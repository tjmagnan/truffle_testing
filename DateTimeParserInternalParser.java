package org.joda.time.format;

class DateTimeParserInternalParser
  implements InternalParser
{
  private final DateTimeParser underlying;
  
  private DateTimeParserInternalParser(DateTimeParser paramDateTimeParser)
  {
    this.underlying = paramDateTimeParser;
  }
  
  static InternalParser of(DateTimeParser paramDateTimeParser)
  {
    if ((paramDateTimeParser instanceof InternalParserDateTimeParser)) {
      paramDateTimeParser = (InternalParser)paramDateTimeParser;
    }
    for (;;)
    {
      return paramDateTimeParser;
      if (paramDateTimeParser == null) {
        paramDateTimeParser = null;
      } else {
        paramDateTimeParser = new DateTimeParserInternalParser(paramDateTimeParser);
      }
    }
  }
  
  public int estimateParsedLength()
  {
    return this.underlying.estimateParsedLength();
  }
  
  DateTimeParser getUnderlying()
  {
    return this.underlying;
  }
  
  public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
  {
    return this.underlying.parseInto(paramDateTimeParserBucket, paramCharSequence.toString(), paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\DateTimeParserInternalParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */