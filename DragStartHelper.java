package android.support.v13.view;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.support.annotation.RequiresApi;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;

@TargetApi(13)
@RequiresApi(13)
public class DragStartHelper
{
  private boolean mDragging;
  private int mLastTouchX;
  private int mLastTouchY;
  private final OnDragStartListener mListener;
  private final View.OnLongClickListener mLongClickListener = new View.OnLongClickListener()
  {
    public boolean onLongClick(View paramAnonymousView)
    {
      return DragStartHelper.this.onLongClick(paramAnonymousView);
    }
  };
  private final View.OnTouchListener mTouchListener = new View.OnTouchListener()
  {
    public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
    {
      return DragStartHelper.this.onTouch(paramAnonymousView, paramAnonymousMotionEvent);
    }
  };
  private final View mView;
  
  public DragStartHelper(View paramView, OnDragStartListener paramOnDragStartListener)
  {
    this.mView = paramView;
    this.mListener = paramOnDragStartListener;
  }
  
  public void attach()
  {
    this.mView.setOnLongClickListener(this.mLongClickListener);
    this.mView.setOnTouchListener(this.mTouchListener);
  }
  
  public void detach()
  {
    this.mView.setOnLongClickListener(null);
    this.mView.setOnTouchListener(null);
  }
  
  public void getTouchPosition(Point paramPoint)
  {
    paramPoint.set(this.mLastTouchX, this.mLastTouchY);
  }
  
  public boolean onLongClick(View paramView)
  {
    return this.mListener.onDragStart(paramView, this);
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    boolean bool2 = false;
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    boolean bool1;
    switch (paramMotionEvent.getAction())
    {
    default: 
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      this.mLastTouchX = i;
      this.mLastTouchY = j;
      bool1 = bool2;
      continue;
      bool1 = bool2;
      if (MotionEventCompat.isFromSource(paramMotionEvent, 8194))
      {
        bool1 = bool2;
        if ((MotionEventCompat.getButtonState(paramMotionEvent) & 0x1) != 0)
        {
          bool1 = bool2;
          if (!this.mDragging) {
            if (this.mLastTouchX == i)
            {
              bool1 = bool2;
              if (this.mLastTouchY == j) {}
            }
            else
            {
              this.mLastTouchX = i;
              this.mLastTouchY = j;
              this.mDragging = this.mListener.onDragStart(paramView, this);
              bool1 = this.mDragging;
              continue;
              this.mDragging = false;
              bool1 = bool2;
            }
          }
        }
      }
    }
  }
  
  public static abstract interface OnDragStartListener
  {
    public abstract boolean onDragStart(View paramView, DragStartHelper paramDragStartHelper);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v13\view\DragStartHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */