package tech.dcube.companion.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PBMoreFragment
  extends Fragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  ListView lstMore;
  private String mParam1;
  private String mParam2;
  
  public static PBMoreFragment newInstance(String paramString1, String paramString2)
  {
    PBMoreFragment localPBMoreFragment = new PBMoreFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBMoreFragment.setArguments(localBundle);
    return localPBMoreFragment;
  }
  
  public void onButtonPressed(Uri paramUri) {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramViewGroup = paramLayoutInflater.inflate(2130968635, paramViewGroup, false);
    this.lstMore = ((ListView)paramViewGroup.findViewById(2131624135));
    paramLayoutInflater = new ArrayAdapter(getActivity().getApplicationContext(), 2130968655, 2131624259, new String[] { "Meters", "Addressbook", "Accounts", "Settings", "Help" });
    this.lstMore.setAdapter(paramLayoutInflater);
    this.lstMore.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong) {}
    });
    return paramViewGroup;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBMoreFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */