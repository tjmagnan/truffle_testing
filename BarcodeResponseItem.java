package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class BarcodeResponseItem
  extends PBObject
  implements Serializable
{
  @Expose
  @SerializedName("height")
  private double height;
  @Expose
  @SerializedName("img_url")
  private String imageUrl;
  @Expose
  @SerializedName("item_id")
  private String itemId;
  @Expose
  @SerializedName("length")
  private double length;
  @Expose
  @SerializedName("manufacturer")
  private String manufacturer;
  @Expose
  @SerializedName("merch_id")
  private String merchId;
  @Expose
  @SerializedName("product_name")
  private String productName;
  @Expose
  @SerializedName("upc")
  private String upc;
  @Expose
  @SerializedName("weight")
  private double weight;
  @Expose
  @SerializedName("width")
  private double width;
  
  public BarcodeResponseItem() {}
  
  public BarcodeResponseItem(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.itemId = paramString1;
    this.merchId = paramString2;
    this.productName = paramString3;
    this.manufacturer = paramString4;
    this.upc = paramString5;
    this.imageUrl = paramString6;
    this.weight = paramInt1;
    this.length = paramInt2;
    this.width = paramInt3;
    this.height = paramInt4;
  }
  
  public double getHeight()
  {
    return this.height;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public String getItemId()
  {
    return this.itemId;
  }
  
  public double getLength()
  {
    return this.length;
  }
  
  public String getManufacturer()
  {
    return this.manufacturer;
  }
  
  public String getMerchId()
  {
    return this.merchId;
  }
  
  public String getProductName()
  {
    return this.productName;
  }
  
  public String getUpc()
  {
    return this.upc;
  }
  
  public double getWeight()
  {
    return this.weight;
  }
  
  public double getWidth()
  {
    return this.width;
  }
  
  public void setHeight(int paramInt)
  {
    this.height = paramInt;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setItemId(String paramString)
  {
    this.itemId = paramString;
  }
  
  public void setLength(int paramInt)
  {
    this.length = paramInt;
  }
  
  public void setManufacturer(String paramString)
  {
    this.manufacturer = paramString;
  }
  
  public void setMerchId(String paramString)
  {
    this.merchId = paramString;
  }
  
  public void setProductName(String paramString)
  {
    this.productName = paramString;
  }
  
  public void setUpc(String paramString)
  {
    this.upc = paramString;
  }
  
  public void setWeight(double paramDouble)
  {
    this.weight = paramDouble;
  }
  
  public void setWidth(int paramInt)
  {
    this.width = paramInt;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\BarcodeResponseItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */