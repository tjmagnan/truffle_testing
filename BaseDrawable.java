package me.zhanghai.android.materialprogressbar;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

abstract class BaseDrawable
  extends Drawable
  implements TintableDrawable
{
  protected int mAlpha = 255;
  protected ColorFilter mColorFilter;
  private DummyConstantState mConstantState = new DummyConstantState(null);
  protected PorterDuffColorFilter mTintFilter;
  protected ColorStateList mTintList;
  protected PorterDuff.Mode mTintMode = PorterDuff.Mode.SRC_IN;
  
  private boolean updateTintFilter()
  {
    boolean bool2 = true;
    boolean bool1 = true;
    if ((this.mTintList == null) || (this.mTintMode == null)) {
      if (this.mTintFilter != null) {
        this.mTintFilter = null;
      }
    }
    for (;;)
    {
      return bool1;
      bool1 = false;
      break;
      this.mTintFilter = new PorterDuffColorFilter(this.mTintList.getColorForState(getState(), 0), this.mTintMode);
      bool1 = bool2;
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    if ((localRect.width() == 0) || (localRect.height() == 0)) {}
    for (;;)
    {
      return;
      int i = paramCanvas.save();
      paramCanvas.translate(localRect.left, localRect.top);
      onDraw(paramCanvas, localRect.width(), localRect.height());
      paramCanvas.restoreToCount(i);
    }
  }
  
  public int getAlpha()
  {
    return this.mAlpha;
  }
  
  public ColorFilter getColorFilter()
  {
    return this.mColorFilter;
  }
  
  protected ColorFilter getColorFilterForDrawing()
  {
    if (this.mColorFilter != null) {}
    for (Object localObject = this.mColorFilter;; localObject = this.mTintFilter) {
      return (ColorFilter)localObject;
    }
  }
  
  public Drawable.ConstantState getConstantState()
  {
    return this.mConstantState;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean isStateful()
  {
    if ((this.mTintList != null) && (this.mTintList.isStateful())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected abstract void onDraw(Canvas paramCanvas, int paramInt1, int paramInt2);
  
  protected boolean onStateChange(int[] paramArrayOfInt)
  {
    return updateTintFilter();
  }
  
  public void setAlpha(int paramInt)
  {
    if (this.mAlpha != paramInt)
    {
      this.mAlpha = paramInt;
      invalidateSelf();
    }
  }
  
  public void setColorFilter(@Nullable ColorFilter paramColorFilter)
  {
    this.mColorFilter = paramColorFilter;
    invalidateSelf();
  }
  
  public void setTint(@ColorInt int paramInt)
  {
    setTintList(ColorStateList.valueOf(paramInt));
  }
  
  public void setTintList(@Nullable ColorStateList paramColorStateList)
  {
    this.mTintList = paramColorStateList;
    if (updateTintFilter()) {
      invalidateSelf();
    }
  }
  
  public void setTintMode(@NonNull PorterDuff.Mode paramMode)
  {
    this.mTintMode = paramMode;
    if (updateTintFilter()) {
      invalidateSelf();
    }
  }
  
  private class DummyConstantState
    extends Drawable.ConstantState
  {
    private DummyConstantState() {}
    
    public int getChangingConfigurations()
    {
      return 0;
    }
    
    @NonNull
    public Drawable newDrawable()
    {
      return BaseDrawable.this;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\BaseDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */