package android.support.transition;

import android.content.Context;
import android.view.ViewGroup;

abstract class SceneStaticsImpl
{
  public abstract SceneImpl getSceneForLayout(ViewGroup paramViewGroup, int paramInt, Context paramContext);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\transition\SceneStaticsImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */