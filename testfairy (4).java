package com.testfairy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.util.HashMap;
import java.util.Map;

public class c
  extends BroadcastReceiver
{
  private static final String a = "health";
  private static final String b = "level";
  private static final String c = "plugged";
  private static final String d = "present";
  private static final String e = "scale";
  private static final String f = "status";
  private static final String g = "technology";
  private static final String h = "temperature";
  private static final String i = "voltage";
  private a j;
  
  public void a(a parama)
  {
    this.j = parama;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = new HashMap(16);
    paramContext.put("health", Integer.valueOf(paramIntent.getIntExtra("health", 0)));
    paramContext.put("level", Integer.valueOf(paramIntent.getIntExtra("level", 0)));
    paramContext.put("plugged", Integer.valueOf(paramIntent.getIntExtra("plugged", 0)));
    paramContext.put("scale", Integer.valueOf(paramIntent.getIntExtra("scale", 0)));
    paramContext.put("status", Integer.valueOf(paramIntent.getIntExtra("status", 0)));
    paramContext.put("temperature", Integer.valueOf(paramIntent.getIntExtra("temperature", 0)));
    paramContext.put("voltage", Integer.valueOf(paramIntent.getIntExtra("voltage", 0)));
    paramIntent = paramIntent.getExtras();
    if (paramIntent != null)
    {
      paramContext.put("present", Boolean.valueOf(paramIntent.getBoolean("present")));
      paramContext.put("technology", paramIntent.getString("technology"));
    }
    this.j.a(paramContext);
  }
  
  public static abstract interface a
  {
    public abstract void a(Map paramMap);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */