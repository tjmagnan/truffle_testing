package com.crashlytics.android.core;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.crashlytics.android.core";
  public static final String ARTIFACT_ID = "crashlytics-core";
  public static final String BUILD_NUMBER = "dev";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final String GROUP = "com.crashlytics.sdk.android";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "2.3.17";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */