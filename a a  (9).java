package com.testfairy.activities.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.EditText;
import android.widget.FrameLayout;
import com.testfairy.FeedbackOptions;

public class i
  extends FrameLayout
{
  public static final int a = 25;
  public static final int b = 25;
  public static final float c = 11.0F;
  private final g d;
  private j e;
  private Bitmap f;
  
  public i(Context paramContext, FeedbackOptions paramFeedbackOptions, String paramString, Bitmap paramBitmap, h paramh)
  {
    super(paramContext);
    this.f = paramBitmap;
    this.e = new j(paramContext, paramString, paramFeedbackOptions, this.f, paramh);
    this.d = new g(paramContext, this.f, paramh);
    addView(this.e);
    addView(this.d);
  }
  
  public EditText a()
  {
    return this.e.a();
  }
  
  public EditText b()
  {
    return this.e.b();
  }
  
  public void c()
  {
    this.d.c();
    this.e.c();
  }
  
  public void d()
  {
    this.e.d();
    this.d.a();
  }
  
  public void e()
  {
    this.d.d();
    this.d.a(this.f);
    this.e.a(this.f);
    d();
  }
  
  public void f()
  {
    this.f = this.d.b();
    this.d.a(this.f);
    this.e.a(this.f);
    d();
  }
  
  public boolean g()
  {
    if (this.d.getVisibility() == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Bitmap h()
  {
    return this.f;
  }
  
  public void i()
  {
    this.e.e();
  }
  
  public void j()
  {
    this.e.f();
  }
  
  public boolean k()
  {
    return this.e.g();
  }
  
  public void l()
  {
    this.e.h();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\a\a\i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */