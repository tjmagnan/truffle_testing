package org.joda.time.convert;

public abstract interface DurationConverter
  extends Converter
{
  public abstract long getDurationMillis(Object paramObject);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\DurationConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */