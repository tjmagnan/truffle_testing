package com.testfairy.p;

import android.graphics.Bitmap;
import java.util.Arrays;

public class e
{
  private int a;
  private int b;
  private int[] c;
  
  public e(Bitmap paramBitmap)
  {
    this.a = paramBitmap.getWidth();
    this.b = paramBitmap.getHeight();
    this.c = new int[this.b];
    int[] arrayOfInt = new int[this.a];
    for (int i = 0; i < this.b; i++)
    {
      paramBitmap.getPixels(arrayOfInt, 0, this.a, 0, i, this.a, 1);
      this.c[i] = Arrays.hashCode(arrayOfInt);
    }
  }
  
  public boolean a(Bitmap paramBitmap)
  {
    boolean bool2 = true;
    boolean bool1;
    if (paramBitmap == null) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (this.a == paramBitmap.getWidth())
      {
        bool1 = bool2;
        if (this.b == paramBitmap.getHeight())
        {
          int[] arrayOfInt = new int[this.a];
          for (int i = 0;; i++)
          {
            if (i >= this.b) {
              break label99;
            }
            paramBitmap.getPixels(arrayOfInt, 0, this.a, 0, i, this.a, 1);
            bool1 = bool2;
            if (Arrays.hashCode(arrayOfInt) != this.c[i]) {
              break;
            }
          }
          label99:
          bool1 = false;
        }
      }
    }
  }
  
  public int hashCode()
  {
    return Arrays.hashCode(this.c);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */