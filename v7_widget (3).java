package android.support.v7.widget;

import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.DrawableContainer.DrawableContainerState;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class DrawableUtils
{
  public static final Rect INSETS_NONE = new Rect();
  private static final String TAG = "DrawableUtils";
  private static final String VECTOR_DRAWABLE_CLAZZ_NAME = "android.graphics.drawable.VectorDrawable";
  private static Class<?> sInsetsClazz;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 18) {}
    try
    {
      sInsetsClazz = Class.forName("android.graphics.Insets");
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;) {}
    }
  }
  
  public static boolean canSafelyMutateDrawable(@NonNull Drawable paramDrawable)
  {
    boolean bool2 = false;
    boolean bool1;
    if ((Build.VERSION.SDK_INT < 15) && ((paramDrawable instanceof InsetDrawable))) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      if (Build.VERSION.SDK_INT < 15)
      {
        bool1 = bool2;
        if ((paramDrawable instanceof GradientDrawable)) {}
      }
      else if (Build.VERSION.SDK_INT < 17)
      {
        bool1 = bool2;
        if ((paramDrawable instanceof LayerDrawable)) {}
      }
      else
      {
        if ((paramDrawable instanceof DrawableContainer))
        {
          paramDrawable = paramDrawable.getConstantState();
          if ((paramDrawable instanceof DrawableContainer.DrawableContainerState))
          {
            paramDrawable = ((DrawableContainer.DrawableContainerState)paramDrawable).getChildren();
            int j = paramDrawable.length;
            for (int i = 0;; i++)
            {
              if (i >= j) {
                break label179;
              }
              bool1 = bool2;
              if (!canSafelyMutateDrawable(paramDrawable[i])) {
                break;
              }
            }
          }
        }
        else
        {
          if ((paramDrawable instanceof android.support.v4.graphics.drawable.DrawableWrapper))
          {
            bool1 = canSafelyMutateDrawable(((android.support.v4.graphics.drawable.DrawableWrapper)paramDrawable).getWrappedDrawable());
            continue;
          }
          if ((paramDrawable instanceof android.support.v7.graphics.drawable.DrawableWrapper))
          {
            bool1 = canSafelyMutateDrawable(((android.support.v7.graphics.drawable.DrawableWrapper)paramDrawable).getWrappedDrawable());
            continue;
          }
          if ((paramDrawable instanceof ScaleDrawable))
          {
            bool1 = canSafelyMutateDrawable(((ScaleDrawable)paramDrawable).getDrawable());
            continue;
          }
        }
        label179:
        bool1 = true;
      }
    }
  }
  
  static void fixDrawable(@NonNull Drawable paramDrawable)
  {
    if ((Build.VERSION.SDK_INT == 21) && ("android.graphics.drawable.VectorDrawable".equals(paramDrawable.getClass().getName()))) {
      fixVectorDrawableTinting(paramDrawable);
    }
  }
  
  private static void fixVectorDrawableTinting(Drawable paramDrawable)
  {
    int[] arrayOfInt = paramDrawable.getState();
    if ((arrayOfInt == null) || (arrayOfInt.length == 0)) {
      paramDrawable.setState(ThemeUtils.CHECKED_STATE_SET);
    }
    for (;;)
    {
      paramDrawable.setState(arrayOfInt);
      return;
      paramDrawable.setState(ThemeUtils.EMPTY_STATE_SET);
    }
  }
  
  public static Rect getOpticalBounds(Drawable paramDrawable)
  {
    if (sInsetsClazz != null) {}
    for (;;)
    {
      Object localObject;
      Rect localRect;
      try
      {
        paramDrawable = DrawableCompat.unwrap(paramDrawable);
        localObject = paramDrawable.getClass().getMethod("getOpticalInsets", new Class[0]).invoke(paramDrawable, new Object[0]);
        if (localObject != null)
        {
          localRect = new android/graphics/Rect;
          localRect.<init>();
          Field[] arrayOfField = sInsetsClazz.getFields();
          int k = arrayOfField.length;
          int j = 0;
          paramDrawable = localRect;
          if (j < k)
          {
            paramDrawable = arrayOfField[j];
            String str = paramDrawable.getName();
            int i = -1;
            switch (str.hashCode())
            {
            default: 
              switch (i)
              {
              default: 
                j++;
              }
              break;
            case 3317767: 
              if (!str.equals("left")) {
                continue;
              }
              i = 0;
              break;
            case 115029: 
              if (!str.equals("top")) {
                continue;
              }
              i = 1;
              break;
            case 108511772: 
              if (!str.equals("right")) {
                continue;
              }
              i = 2;
              break;
            case -1383228885: 
              if (!str.equals("bottom")) {
                continue;
              }
              i = 3;
              continue;
              localRect.left = paramDrawable.getInt(localObject);
              break;
            }
          }
        }
        else
        {
          paramDrawable = INSETS_NONE;
        }
      }
      catch (Exception paramDrawable)
      {
        Log.e("DrawableUtils", "Couldn't obtain the optical insets. Ignoring.");
      }
      return paramDrawable;
      localRect.top = paramDrawable.getInt(localObject);
      continue;
      localRect.right = paramDrawable.getInt(localObject);
      continue;
      localRect.bottom = paramDrawable.getInt(localObject);
    }
  }
  
  static PorterDuff.Mode parseTintMode(int paramInt, PorterDuff.Mode paramMode)
  {
    PorterDuff.Mode localMode = paramMode;
    switch (paramInt)
    {
    default: 
      localMode = paramMode;
    }
    for (;;)
    {
      return localMode;
      localMode = PorterDuff.Mode.SRC_OVER;
      continue;
      localMode = PorterDuff.Mode.SRC_IN;
      continue;
      localMode = PorterDuff.Mode.SRC_ATOP;
      continue;
      localMode = PorterDuff.Mode.MULTIPLY;
      continue;
      localMode = PorterDuff.Mode.SCREEN;
      continue;
      localMode = paramMode;
      if (Build.VERSION.SDK_INT >= 11) {
        localMode = PorterDuff.Mode.valueOf("ADD");
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\DrawableUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */