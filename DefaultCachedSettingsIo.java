package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Kit;

class DefaultCachedSettingsIo
  implements CachedSettingsIo
{
  private final Kit kit;
  
  public DefaultCachedSettingsIo(Kit paramKit)
  {
    this.kit = paramKit;
  }
  
  /* Error */
  public org.json.JSONObject readCachedSettings()
  {
    // Byte code:
    //   0: invokestatic 26	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   3: ldc 28
    //   5: ldc 30
    //   7: invokeinterface 36 3 0
    //   12: aconst_null
    //   13: astore 5
    //   15: aconst_null
    //   16: astore 6
    //   18: aconst_null
    //   19: astore_2
    //   20: aconst_null
    //   21: astore 4
    //   23: aconst_null
    //   24: astore_3
    //   25: aload 6
    //   27: astore_1
    //   28: new 38	java/io/File
    //   31: astore 7
    //   33: aload 6
    //   35: astore_1
    //   36: new 40	io/fabric/sdk/android/services/persistence/FileStoreImpl
    //   39: astore 8
    //   41: aload 6
    //   43: astore_1
    //   44: aload 8
    //   46: aload_0
    //   47: getfield 15	io/fabric/sdk/android/services/settings/DefaultCachedSettingsIo:kit	Lio/fabric/sdk/android/Kit;
    //   50: invokespecial 42	io/fabric/sdk/android/services/persistence/FileStoreImpl:<init>	(Lio/fabric/sdk/android/Kit;)V
    //   53: aload 6
    //   55: astore_1
    //   56: aload 7
    //   58: aload 8
    //   60: invokevirtual 46	io/fabric/sdk/android/services/persistence/FileStoreImpl:getFilesDir	()Ljava/io/File;
    //   63: ldc 48
    //   65: invokespecial 51	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   68: aload 6
    //   70: astore_1
    //   71: aload 7
    //   73: invokevirtual 55	java/io/File:exists	()Z
    //   76: ifeq +41 -> 117
    //   79: aload 6
    //   81: astore_1
    //   82: new 57	java/io/FileInputStream
    //   85: astore_2
    //   86: aload 6
    //   88: astore_1
    //   89: aload_2
    //   90: aload 7
    //   92: invokespecial 60	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   95: aload_2
    //   96: invokestatic 66	io/fabric/sdk/android/services/common/CommonUtils:streamToString	(Ljava/io/InputStream;)Ljava/lang/String;
    //   99: astore_3
    //   100: new 68	org/json/JSONObject
    //   103: astore_1
    //   104: aload_1
    //   105: aload_3
    //   106: invokespecial 71	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   109: aload_2
    //   110: ldc 73
    //   112: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   115: aload_1
    //   116: areturn
    //   117: aload 6
    //   119: astore_1
    //   120: invokestatic 26	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   123: ldc 28
    //   125: ldc 79
    //   127: invokeinterface 36 3 0
    //   132: aload_3
    //   133: astore_1
    //   134: goto -25 -> 109
    //   137: astore_3
    //   138: aload 5
    //   140: astore_2
    //   141: aload_2
    //   142: astore_1
    //   143: invokestatic 26	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   146: ldc 28
    //   148: ldc 81
    //   150: aload_3
    //   151: invokeinterface 85 4 0
    //   156: aload_2
    //   157: ldc 73
    //   159: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   162: aload 4
    //   164: astore_1
    //   165: goto -50 -> 115
    //   168: astore_2
    //   169: aload_1
    //   170: astore_3
    //   171: aload_3
    //   172: ldc 73
    //   174: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   177: aload_2
    //   178: athrow
    //   179: astore_1
    //   180: aload_2
    //   181: astore_3
    //   182: aload_1
    //   183: astore_2
    //   184: goto -13 -> 171
    //   187: astore_3
    //   188: goto -47 -> 141
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	191	0	this	DefaultCachedSettingsIo
    //   27	143	1	localObject1	Object
    //   179	4	1	localObject2	Object
    //   19	138	2	localObject3	Object
    //   168	13	2	localObject4	Object
    //   183	1	2	localObject5	Object
    //   24	109	3	str	String
    //   137	14	3	localException1	Exception
    //   170	12	3	localObject6	Object
    //   187	1	3	localException2	Exception
    //   21	142	4	localObject7	Object
    //   13	126	5	localObject8	Object
    //   16	102	6	localObject9	Object
    //   31	60	7	localFile	java.io.File
    //   39	20	8	localFileStoreImpl	io.fabric.sdk.android.services.persistence.FileStoreImpl
    // Exception table:
    //   from	to	target	type
    //   28	33	137	java/lang/Exception
    //   36	41	137	java/lang/Exception
    //   44	53	137	java/lang/Exception
    //   56	68	137	java/lang/Exception
    //   71	79	137	java/lang/Exception
    //   82	86	137	java/lang/Exception
    //   89	95	137	java/lang/Exception
    //   120	132	137	java/lang/Exception
    //   28	33	168	finally
    //   36	41	168	finally
    //   44	53	168	finally
    //   56	68	168	finally
    //   71	79	168	finally
    //   82	86	168	finally
    //   89	95	168	finally
    //   120	132	168	finally
    //   143	156	168	finally
    //   95	109	179	finally
    //   95	109	187	java/lang/Exception
  }
  
  /* Error */
  public void writeCachedSettings(long paramLong, org.json.JSONObject paramJSONObject)
  {
    // Byte code:
    //   0: invokestatic 26	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   3: ldc 28
    //   5: ldc 89
    //   7: invokeinterface 36 3 0
    //   12: aload_3
    //   13: ifnull +109 -> 122
    //   16: aconst_null
    //   17: astore 7
    //   19: aconst_null
    //   20: astore 6
    //   22: aload 7
    //   24: astore 4
    //   26: aload_3
    //   27: ldc 91
    //   29: lload_1
    //   30: invokevirtual 95	org/json/JSONObject:put	(Ljava/lang/String;J)Lorg/json/JSONObject;
    //   33: pop
    //   34: aload 7
    //   36: astore 4
    //   38: new 97	java/io/FileWriter
    //   41: astore 5
    //   43: aload 7
    //   45: astore 4
    //   47: new 38	java/io/File
    //   50: astore 8
    //   52: aload 7
    //   54: astore 4
    //   56: new 40	io/fabric/sdk/android/services/persistence/FileStoreImpl
    //   59: astore 9
    //   61: aload 7
    //   63: astore 4
    //   65: aload 9
    //   67: aload_0
    //   68: getfield 15	io/fabric/sdk/android/services/settings/DefaultCachedSettingsIo:kit	Lio/fabric/sdk/android/Kit;
    //   71: invokespecial 42	io/fabric/sdk/android/services/persistence/FileStoreImpl:<init>	(Lio/fabric/sdk/android/Kit;)V
    //   74: aload 7
    //   76: astore 4
    //   78: aload 8
    //   80: aload 9
    //   82: invokevirtual 46	io/fabric/sdk/android/services/persistence/FileStoreImpl:getFilesDir	()Ljava/io/File;
    //   85: ldc 48
    //   87: invokespecial 51	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   90: aload 7
    //   92: astore 4
    //   94: aload 5
    //   96: aload 8
    //   98: invokespecial 98	java/io/FileWriter:<init>	(Ljava/io/File;)V
    //   101: aload 5
    //   103: aload_3
    //   104: invokevirtual 102	org/json/JSONObject:toString	()Ljava/lang/String;
    //   107: invokevirtual 105	java/io/FileWriter:write	(Ljava/lang/String;)V
    //   110: aload 5
    //   112: invokevirtual 108	java/io/FileWriter:flush	()V
    //   115: aload 5
    //   117: ldc 110
    //   119: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   122: return
    //   123: astore 5
    //   125: aload 6
    //   127: astore_3
    //   128: aload_3
    //   129: astore 4
    //   131: invokestatic 26	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   134: ldc 28
    //   136: ldc 112
    //   138: aload 5
    //   140: invokeinterface 85 4 0
    //   145: aload_3
    //   146: ldc 110
    //   148: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   151: goto -29 -> 122
    //   154: astore_3
    //   155: aload 4
    //   157: ldc 110
    //   159: invokestatic 77	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   162: aload_3
    //   163: athrow
    //   164: astore_3
    //   165: aload 5
    //   167: astore 4
    //   169: goto -14 -> 155
    //   172: astore_3
    //   173: aload 5
    //   175: astore 4
    //   177: aload_3
    //   178: astore 5
    //   180: aload 4
    //   182: astore_3
    //   183: goto -55 -> 128
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	186	0	this	DefaultCachedSettingsIo
    //   0	186	1	paramLong	long
    //   0	186	3	paramJSONObject	org.json.JSONObject
    //   24	157	4	localObject1	Object
    //   41	75	5	localFileWriter	java.io.FileWriter
    //   123	51	5	localException	Exception
    //   178	1	5	localJSONObject	org.json.JSONObject
    //   20	106	6	localObject2	Object
    //   17	74	7	localObject3	Object
    //   50	47	8	localFile	java.io.File
    //   59	22	9	localFileStoreImpl	io.fabric.sdk.android.services.persistence.FileStoreImpl
    // Exception table:
    //   from	to	target	type
    //   26	34	123	java/lang/Exception
    //   38	43	123	java/lang/Exception
    //   47	52	123	java/lang/Exception
    //   56	61	123	java/lang/Exception
    //   65	74	123	java/lang/Exception
    //   78	90	123	java/lang/Exception
    //   94	101	123	java/lang/Exception
    //   26	34	154	finally
    //   38	43	154	finally
    //   47	52	154	finally
    //   56	61	154	finally
    //   65	74	154	finally
    //   78	90	154	finally
    //   94	101	154	finally
    //   131	145	154	finally
    //   101	115	164	finally
    //   101	115	172	java/lang/Exception
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\DefaultCachedSettingsIo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */