package org.jsoup.parser;

import java.util.ArrayList;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

abstract class TreeBuilder
{
  protected String baseUri;
  protected Token currentToken;
  protected Document doc;
  private Token.EndTag end = new Token.EndTag();
  protected ParseErrorList errors;
  CharacterReader reader;
  protected ParseSettings settings;
  protected ArrayList<Element> stack;
  private Token.StartTag start = new Token.StartTag();
  Tokeniser tokeniser;
  
  protected Element currentElement()
  {
    int i = this.stack.size();
    if (i > 0) {}
    for (Element localElement = (Element)this.stack.get(i - 1);; localElement = null) {
      return localElement;
    }
  }
  
  abstract ParseSettings defaultSettings();
  
  protected void initialiseParse(String paramString1, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    Validate.notNull(paramString1, "String input must not be null");
    Validate.notNull(paramString2, "BaseURI must not be null");
    this.doc = new Document(paramString2);
    this.settings = paramParseSettings;
    this.reader = new CharacterReader(paramString1);
    this.errors = paramParseErrorList;
    this.tokeniser = new Tokeniser(this.reader, paramParseErrorList);
    this.stack = new ArrayList(32);
    this.baseUri = paramString2;
  }
  
  Document parse(String paramString1, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    initialiseParse(paramString1, paramString2, paramParseErrorList, paramParseSettings);
    runParser();
    return this.doc;
  }
  
  protected abstract boolean process(Token paramToken);
  
  protected boolean processEndTag(String paramString)
  {
    if (this.currentToken == this.end) {}
    for (boolean bool = process(new Token.EndTag().name(paramString));; bool = process(this.end.reset().name(paramString))) {
      return bool;
    }
  }
  
  protected boolean processStartTag(String paramString)
  {
    if (this.currentToken == this.start) {}
    for (boolean bool = process(new Token.StartTag().name(paramString));; bool = process(this.start.reset().name(paramString))) {
      return bool;
    }
  }
  
  public boolean processStartTag(String paramString, Attributes paramAttributes)
  {
    if (this.currentToken == this.start) {}
    for (boolean bool = process(new Token.StartTag().nameAttr(paramString, paramAttributes));; bool = process(this.start))
    {
      return bool;
      this.start.reset();
      this.start.nameAttr(paramString, paramAttributes);
    }
  }
  
  protected void runParser()
  {
    Token localToken;
    do
    {
      localToken = this.tokeniser.read();
      process(localToken);
      localToken.reset();
    } while (localToken.type != Token.TokenType.EOF);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\TreeBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */