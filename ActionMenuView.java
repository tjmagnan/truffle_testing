package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuBuilder.ItemInvoker;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView
  extends LinearLayoutCompat
  implements MenuBuilder.ItemInvoker, MenuView
{
  static final int GENERATED_ITEM_PADDING = 4;
  static final int MIN_CELL_SIZE = 56;
  private static final String TAG = "ActionMenuView";
  private MenuPresenter.Callback mActionMenuPresenterCallback;
  private boolean mFormatItems;
  private int mFormatItemsWidth;
  private int mGeneratedItemPadding;
  private MenuBuilder mMenu;
  MenuBuilder.Callback mMenuBuilderCallback;
  private int mMinCellSize;
  OnMenuItemClickListener mOnMenuItemClickListener;
  private Context mPopupContext;
  private int mPopupTheme;
  private ActionMenuPresenter mPresenter;
  private boolean mReserveOverflow;
  
  public ActionMenuView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActionMenuView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setBaselineAligned(false);
    float f = paramContext.getResources().getDisplayMetrics().density;
    this.mMinCellSize = ((int)(56.0F * f));
    this.mGeneratedItemPadding = ((int)(4.0F * f));
    this.mPopupContext = paramContext;
    this.mPopupTheme = 0;
  }
  
  static int measureChildForCells(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int j = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt3) - paramInt4, View.MeasureSpec.getMode(paramInt3));
    ActionMenuItemView localActionMenuItemView;
    if ((paramView instanceof ActionMenuItemView))
    {
      localActionMenuItemView = (ActionMenuItemView)paramView;
      if ((localActionMenuItemView == null) || (!localActionMenuItemView.hasText())) {
        break label182;
      }
      paramInt4 = 1;
      label54:
      int i = 0;
      paramInt3 = i;
      if (paramInt2 > 0) {
        if (paramInt4 != 0)
        {
          paramInt3 = i;
          if (paramInt2 < 2) {}
        }
        else
        {
          paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 * paramInt2, Integer.MIN_VALUE), j);
          i = paramView.getMeasuredWidth();
          paramInt3 = i / paramInt1;
          paramInt2 = paramInt3;
          if (i % paramInt1 != 0) {
            paramInt2 = paramInt3 + 1;
          }
          paramInt3 = paramInt2;
          if (paramInt4 != 0)
          {
            paramInt3 = paramInt2;
            if (paramInt2 < 2) {
              paramInt3 = 2;
            }
          }
        }
      }
      if ((localLayoutParams.isOverflowButton) || (paramInt4 == 0)) {
        break label188;
      }
    }
    label182:
    label188:
    for (boolean bool = true;; bool = false)
    {
      localLayoutParams.expandable = bool;
      localLayoutParams.cellsUsed = paramInt3;
      paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt3 * paramInt1, 1073741824), j);
      return paramInt3;
      localActionMenuItemView = null;
      break;
      paramInt4 = 0;
      break label54;
    }
  }
  
  private void onMeasureExactFormat(int paramInt1, int paramInt2)
  {
    int i8 = View.MeasureSpec.getMode(paramInt2);
    int j = View.MeasureSpec.getSize(paramInt1);
    int i7 = View.MeasureSpec.getSize(paramInt2);
    paramInt1 = getPaddingLeft();
    int i = getPaddingRight();
    int i13 = getPaddingTop() + getPaddingBottom();
    int i9 = getChildMeasureSpec(paramInt2, i13, -2);
    int i10 = j - (paramInt1 + i);
    paramInt2 = i10 / this.mMinCellSize;
    paramInt1 = this.mMinCellSize;
    if (paramInt2 == 0) {
      setMeasuredDimension(i10, 0);
    }
    for (;;)
    {
      return;
      int i11 = this.mMinCellSize + i10 % paramInt1 / paramInt2;
      i = 0;
      int m = 0;
      int k = 0;
      int n = 0;
      j = 0;
      long l1 = 0L;
      int i12 = getChildCount();
      int i1 = 0;
      Object localObject1;
      long l2;
      int i2;
      int i3;
      Object localObject2;
      label269:
      int i4;
      int i5;
      int i6;
      while (i1 < i12)
      {
        localObject1 = getChildAt(i1);
        if (((View)localObject1).getVisibility() == 8)
        {
          l2 = l1;
          i2 = j;
          i1++;
          j = i2;
          l1 = l2;
        }
        else
        {
          boolean bool = localObject1 instanceof ActionMenuItemView;
          i3 = n + 1;
          if (bool) {
            ((View)localObject1).setPadding(this.mGeneratedItemPadding, 0, this.mGeneratedItemPadding, 0);
          }
          localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
          ((LayoutParams)localObject2).expanded = false;
          ((LayoutParams)localObject2).extraPixels = 0;
          ((LayoutParams)localObject2).cellsUsed = 0;
          ((LayoutParams)localObject2).expandable = false;
          ((LayoutParams)localObject2).leftMargin = 0;
          ((LayoutParams)localObject2).rightMargin = 0;
          if ((bool) && (((ActionMenuItemView)localObject1).hasText()))
          {
            bool = true;
            ((LayoutParams)localObject2).preventEdgeOffset = bool;
            if (!((LayoutParams)localObject2).isOverflowButton) {
              break label427;
            }
          }
          label427:
          for (paramInt1 = 1;; paramInt1 = paramInt2)
          {
            int i14 = measureChildForCells((View)localObject1, i11, paramInt1, i9, i13);
            i4 = Math.max(m, i14);
            paramInt1 = k;
            if (((LayoutParams)localObject2).expandable) {
              paramInt1 = k + 1;
            }
            if (((LayoutParams)localObject2).isOverflowButton) {
              j = 1;
            }
            i5 = paramInt2 - i14;
            i6 = Math.max(i, ((View)localObject1).getMeasuredHeight());
            paramInt2 = i5;
            k = paramInt1;
            i2 = j;
            m = i4;
            i = i6;
            l2 = l1;
            n = i3;
            if (i14 != 1) {
              break;
            }
            l2 = l1 | 1 << i1;
            paramInt2 = i5;
            k = paramInt1;
            i2 = j;
            m = i4;
            i = i6;
            n = i3;
            break;
            bool = false;
            break label269;
          }
        }
      }
      long l3;
      if ((j != 0) && (n == 2))
      {
        i1 = 1;
        paramInt1 = 0;
        i2 = paramInt2;
        l2 = l1;
        if (k <= 0) {
          break label635;
        }
        l2 = l1;
        if (i2 <= 0) {
          break label635;
        }
        i3 = Integer.MAX_VALUE;
        l3 = 0L;
        i6 = 0;
        i5 = 0;
        label482:
        if (i5 >= i12) {
          break label617;
        }
        localObject1 = (LayoutParams)getChildAt(i5).getLayoutParams();
        if (((LayoutParams)localObject1).expandable) {
          break label545;
        }
        l2 = l3;
        paramInt2 = i6;
        i4 = i3;
      }
      for (;;)
      {
        i5++;
        i3 = i4;
        i6 = paramInt2;
        l3 = l2;
        break label482;
        i1 = 0;
        break;
        label545:
        if (((LayoutParams)localObject1).cellsUsed < i3)
        {
          i4 = ((LayoutParams)localObject1).cellsUsed;
          l2 = 1 << i5;
          paramInt2 = 1;
        }
        else
        {
          i4 = i3;
          paramInt2 = i6;
          l2 = l3;
          if (((LayoutParams)localObject1).cellsUsed == i3)
          {
            l2 = l3 | 1 << i5;
            paramInt2 = i6 + 1;
            i4 = i3;
          }
        }
      }
      label617:
      l1 |= l3;
      if (i6 > i2)
      {
        l2 = l1;
        label635:
        if ((j != 0) || (n != 1)) {
          break label996;
        }
        j = 1;
        label649:
        paramInt2 = paramInt1;
        if (i2 <= 0) {
          break label1150;
        }
        paramInt2 = paramInt1;
        if (l2 == 0L) {
          break label1150;
        }
        if ((i2 >= n - 1) && (j == 0))
        {
          paramInt2 = paramInt1;
          if (m <= 1) {
            break label1150;
          }
        }
        float f3 = Long.bitCount(l2);
        float f2 = f3;
        if (j == 0)
        {
          float f1 = f3;
          if ((1L & l2) != 0L)
          {
            f1 = f3;
            if (!((LayoutParams)getChildAt(0).getLayoutParams()).preventEdgeOffset) {
              f1 = f3 - 0.5F;
            }
          }
          f2 = f1;
          if ((1 << i12 - 1 & l2) != 0L)
          {
            f2 = f1;
            if (!((LayoutParams)getChildAt(i12 - 1).getLayoutParams()).preventEdgeOffset) {
              f2 = f1 - 0.5F;
            }
          }
        }
        if (f2 <= 0.0F) {
          break label1002;
        }
        j = (int)(i2 * i11 / f2);
        label808:
        k = 0;
        paramInt2 = paramInt1;
        label813:
        if (k >= i12) {
          break label1150;
        }
        if ((1 << k & l2) != 0L) {
          break label1008;
        }
        paramInt1 = paramInt2;
      }
      for (;;)
      {
        k++;
        paramInt2 = paramInt1;
        break label813;
        paramInt1 = 0;
        if (paramInt1 < i12)
        {
          localObject2 = getChildAt(paramInt1);
          localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
          if ((1 << paramInt1 & l3) == 0L)
          {
            paramInt2 = i2;
            l2 = l1;
            if (((LayoutParams)localObject1).cellsUsed == i3 + 1)
            {
              l2 = l1 | 1 << paramInt1;
              paramInt2 = i2;
            }
          }
          for (;;)
          {
            paramInt1++;
            i2 = paramInt2;
            l1 = l2;
            break;
            if ((i1 != 0) && (((LayoutParams)localObject1).preventEdgeOffset) && (i2 == 1)) {
              ((View)localObject2).setPadding(this.mGeneratedItemPadding + i11, 0, this.mGeneratedItemPadding, 0);
            }
            ((LayoutParams)localObject1).cellsUsed += 1;
            ((LayoutParams)localObject1).expanded = true;
            paramInt2 = i2 - 1;
            l2 = l1;
          }
        }
        paramInt1 = 1;
        break;
        label996:
        j = 0;
        break label649;
        label1002:
        j = 0;
        break label808;
        label1008:
        localObject1 = getChildAt(k);
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        if ((localObject1 instanceof ActionMenuItemView))
        {
          ((LayoutParams)localObject2).extraPixels = j;
          ((LayoutParams)localObject2).expanded = true;
          if ((k == 0) && (!((LayoutParams)localObject2).preventEdgeOffset)) {
            ((LayoutParams)localObject2).leftMargin = (-j / 2);
          }
          paramInt1 = 1;
        }
        else if (((LayoutParams)localObject2).isOverflowButton)
        {
          ((LayoutParams)localObject2).extraPixels = j;
          ((LayoutParams)localObject2).expanded = true;
          ((LayoutParams)localObject2).rightMargin = (-j / 2);
          paramInt1 = 1;
        }
        else
        {
          if (k != 0) {
            ((LayoutParams)localObject2).leftMargin = (j / 2);
          }
          paramInt1 = paramInt2;
          if (k != i12 - 1)
          {
            ((LayoutParams)localObject2).rightMargin = (j / 2);
            paramInt1 = paramInt2;
          }
        }
      }
      label1150:
      if (paramInt2 != 0)
      {
        paramInt1 = 0;
        if (paramInt1 < i12)
        {
          localObject1 = getChildAt(paramInt1);
          localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
          if (!((LayoutParams)localObject2).expanded) {}
          for (;;)
          {
            paramInt1++;
            break;
            ((View)localObject1).measure(View.MeasureSpec.makeMeasureSpec(((LayoutParams)localObject2).cellsUsed * i11 + ((LayoutParams)localObject2).extraPixels, 1073741824), i9);
          }
        }
      }
      paramInt1 = i7;
      if (i8 != 1073741824) {
        paramInt1 = i;
      }
      setMeasuredDimension(i10, paramInt1);
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams != null) && ((paramLayoutParams instanceof LayoutParams))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void dismissPopupMenus()
  {
    if (this.mPresenter != null) {
      this.mPresenter.dismissPopupMenus();
    }
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return false;
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    LayoutParams localLayoutParams = new LayoutParams(-2, -2);
    localLayoutParams.gravity = 16;
    return localLayoutParams;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (paramLayoutParams != null) {
      if ((paramLayoutParams instanceof LayoutParams))
      {
        paramLayoutParams = new LayoutParams((LayoutParams)paramLayoutParams);
        localObject = paramLayoutParams;
        if (paramLayoutParams.gravity <= 0) {
          paramLayoutParams.gravity = 16;
        }
      }
    }
    for (Object localObject = paramLayoutParams;; localObject = generateDefaultLayoutParams())
    {
      return (LayoutParams)localObject;
      paramLayoutParams = new LayoutParams(paramLayoutParams);
      break;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public LayoutParams generateOverflowButtonLayoutParams()
  {
    LayoutParams localLayoutParams = generateDefaultLayoutParams();
    localLayoutParams.isOverflowButton = true;
    return localLayoutParams;
  }
  
  public Menu getMenu()
  {
    ActionMenuPresenter localActionMenuPresenter;
    if (this.mMenu == null)
    {
      localObject = getContext();
      this.mMenu = new MenuBuilder((Context)localObject);
      this.mMenu.setCallback(new MenuBuilderCallback());
      this.mPresenter = new ActionMenuPresenter((Context)localObject);
      this.mPresenter.setReserveOverflow(true);
      localActionMenuPresenter = this.mPresenter;
      if (this.mActionMenuPresenterCallback == null) {
        break label109;
      }
    }
    label109:
    for (Object localObject = this.mActionMenuPresenterCallback;; localObject = new ActionMenuPresenterCallback())
    {
      localActionMenuPresenter.setCallback((MenuPresenter.Callback)localObject);
      this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
      this.mPresenter.setMenuView(this);
      return this.mMenu;
    }
  }
  
  @Nullable
  public Drawable getOverflowIcon()
  {
    getMenu();
    return this.mPresenter.getOverflowIcon();
  }
  
  public int getPopupTheme()
  {
    return this.mPopupTheme;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public int getWindowAnimations()
  {
    return 0;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected boolean hasSupportDividerBeforeChildAt(int paramInt)
  {
    boolean bool2;
    if (paramInt == 0) {
      bool2 = false;
    }
    for (;;)
    {
      return bool2;
      View localView1 = getChildAt(paramInt - 1);
      View localView2 = getChildAt(paramInt);
      bool2 = false;
      boolean bool1 = bool2;
      if (paramInt < getChildCount())
      {
        bool1 = bool2;
        if ((localView1 instanceof ActionMenuChildView)) {
          bool1 = false | ((ActionMenuChildView)localView1).needsDividerAfter();
        }
      }
      bool2 = bool1;
      if (paramInt > 0)
      {
        bool2 = bool1;
        if ((localView2 instanceof ActionMenuChildView)) {
          bool2 = bool1 | ((ActionMenuChildView)localView2).needsDividerBefore();
        }
      }
    }
  }
  
  public boolean hideOverflowMenu()
  {
    if ((this.mPresenter != null) && (this.mPresenter.hideOverflowMenu())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void initialize(MenuBuilder paramMenuBuilder)
  {
    this.mMenu = paramMenuBuilder;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean invokeItem(MenuItemImpl paramMenuItemImpl)
  {
    return this.mMenu.performItemAction(paramMenuItemImpl, 0);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean isOverflowMenuShowPending()
  {
    if ((this.mPresenter != null) && (this.mPresenter.isOverflowMenuShowPending())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isOverflowMenuShowing()
  {
    if ((this.mPresenter != null) && (this.mPresenter.isOverflowMenuShowing())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean isOverflowReserved()
  {
    return this.mReserveOverflow;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.mPresenter != null)
    {
      this.mPresenter.updateMenuView(false);
      if (this.mPresenter.isOverflowMenuShowing())
      {
        this.mPresenter.hideOverflowMenu();
        this.mPresenter.showOverflowMenu();
      }
    }
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    dismissPopupMenus();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.mFormatItems) {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    }
    int i2;
    int i1;
    int j;
    int k;
    int i;
    Object localObject1;
    Object localObject2;
    for (;;)
    {
      return;
      i2 = getChildCount();
      i1 = (paramInt4 - paramInt2) / 2;
      int i3 = getDividerWidth();
      j = 0;
      paramInt4 = 0;
      paramInt2 = paramInt3 - paramInt1 - getPaddingRight() - getPaddingLeft();
      k = 0;
      paramBoolean = ViewUtils.isLayoutRtl(this);
      i = 0;
      if (i < i2)
      {
        localObject1 = getChildAt(i);
        if (((View)localObject1).getVisibility() == 8) {}
        for (;;)
        {
          i++;
          break;
          localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
          if (((LayoutParams)localObject2).isOverflowButton)
          {
            m = ((View)localObject1).getMeasuredWidth();
            k = m;
            if (hasSupportDividerBeforeChildAt(i)) {
              k = m + i3;
            }
            int i4 = ((View)localObject1).getMeasuredHeight();
            int n;
            if (paramBoolean)
            {
              n = getPaddingLeft() + ((LayoutParams)localObject2).leftMargin;
              m = n + k;
            }
            for (;;)
            {
              int i5 = i1 - i4 / 2;
              ((View)localObject1).layout(n, i5, m, i5 + i4);
              paramInt2 -= k;
              k = 1;
              break;
              m = getWidth() - getPaddingRight() - ((LayoutParams)localObject2).rightMargin;
              n = m - k;
            }
          }
          int m = ((View)localObject1).getMeasuredWidth() + ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin;
          j += m;
          m = paramInt2 - m;
          paramInt2 = j;
          if (hasSupportDividerBeforeChildAt(i)) {
            paramInt2 = j + i3;
          }
          paramInt4++;
          j = paramInt2;
          paramInt2 = m;
        }
      }
      if ((i2 != 1) || (k != 0)) {
        break;
      }
      localObject1 = getChildAt(0);
      paramInt4 = ((View)localObject1).getMeasuredWidth();
      paramInt2 = ((View)localObject1).getMeasuredHeight();
      paramInt1 = (paramInt3 - paramInt1) / 2 - paramInt4 / 2;
      paramInt3 = i1 - paramInt2 / 2;
      ((View)localObject1).layout(paramInt1, paramInt3, paramInt1 + paramInt4, paramInt3 + paramInt2);
    }
    if (k != 0)
    {
      paramInt1 = 0;
      label379:
      paramInt1 = paramInt4 - paramInt1;
      if (paramInt1 <= 0) {
        break label477;
      }
      paramInt1 = paramInt2 / paramInt1;
      label392:
      paramInt4 = Math.max(0, paramInt1);
      if (!paramBoolean) {
        break label549;
      }
      paramInt3 = getWidth() - getPaddingRight();
      paramInt1 = 0;
      label416:
      if (paramInt1 < i2)
      {
        localObject1 = getChildAt(paramInt1);
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        paramInt2 = paramInt3;
        if (((View)localObject1).getVisibility() != 8) {
          if (!((LayoutParams)localObject2).isOverflowButton) {
            break label482;
          }
        }
      }
    }
    for (paramInt2 = paramInt3;; paramInt2 = paramInt3 - (((LayoutParams)localObject2).leftMargin + i + paramInt4))
    {
      paramInt1++;
      paramInt3 = paramInt2;
      break label416;
      break;
      paramInt1 = 1;
      break label379;
      label477:
      paramInt1 = 0;
      break label392;
      label482:
      paramInt3 -= ((LayoutParams)localObject2).rightMargin;
      i = ((View)localObject1).getMeasuredWidth();
      j = ((View)localObject1).getMeasuredHeight();
      paramInt2 = i1 - j / 2;
      ((View)localObject1).layout(paramInt3 - i, paramInt2, paramInt3, paramInt2 + j);
    }
    label549:
    paramInt3 = getPaddingLeft();
    paramInt1 = 0;
    label557:
    if (paramInt1 < i2)
    {
      localObject2 = getChildAt(paramInt1);
      localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
      paramInt2 = paramInt3;
      if (((View)localObject2).getVisibility() != 8) {
        if (!((LayoutParams)localObject1).isOverflowButton) {
          break label613;
        }
      }
    }
    for (paramInt2 = paramInt3;; paramInt2 = j + (((LayoutParams)localObject1).rightMargin + paramInt2 + paramInt4))
    {
      paramInt1++;
      paramInt3 = paramInt2;
      break label557;
      break;
      label613:
      j = paramInt3 + ((LayoutParams)localObject1).leftMargin;
      paramInt2 = ((View)localObject2).getMeasuredWidth();
      i = ((View)localObject2).getMeasuredHeight();
      paramInt3 = i1 - i / 2;
      ((View)localObject2).layout(j, paramInt3, j + paramInt2, paramInt3 + i);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    boolean bool2 = this.mFormatItems;
    boolean bool1;
    int i;
    int j;
    if (View.MeasureSpec.getMode(paramInt1) == 1073741824)
    {
      bool1 = true;
      this.mFormatItems = bool1;
      if (bool2 != this.mFormatItems) {
        this.mFormatItemsWidth = 0;
      }
      i = View.MeasureSpec.getSize(paramInt1);
      if ((this.mFormatItems) && (this.mMenu != null) && (i != this.mFormatItemsWidth))
      {
        this.mFormatItemsWidth = i;
        this.mMenu.onItemsChanged(true);
      }
      j = getChildCount();
      if ((!this.mFormatItems) || (j <= 0)) {
        break label109;
      }
      onMeasureExactFormat(paramInt1, paramInt2);
    }
    for (;;)
    {
      return;
      bool1 = false;
      break;
      label109:
      for (i = 0; i < j; i++)
      {
        LayoutParams localLayoutParams = (LayoutParams)getChildAt(i).getLayoutParams();
        localLayoutParams.rightMargin = 0;
        localLayoutParams.leftMargin = 0;
      }
      super.onMeasure(paramInt1, paramInt2);
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public MenuBuilder peekMenu()
  {
    return this.mMenu;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setExpandedActionViewsExclusive(boolean paramBoolean)
  {
    this.mPresenter.setExpandedActionViewsExclusive(paramBoolean);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1)
  {
    this.mActionMenuPresenterCallback = paramCallback;
    this.mMenuBuilderCallback = paramCallback1;
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.mOnMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  public void setOverflowIcon(@Nullable Drawable paramDrawable)
  {
    getMenu();
    this.mPresenter.setOverflowIcon(paramDrawable);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setOverflowReserved(boolean paramBoolean)
  {
    this.mReserveOverflow = paramBoolean;
  }
  
  public void setPopupTheme(@StyleRes int paramInt)
  {
    if (this.mPopupTheme != paramInt)
    {
      this.mPopupTheme = paramInt;
      if (paramInt != 0) {
        break label26;
      }
    }
    label26:
    for (this.mPopupContext = getContext();; this.mPopupContext = new ContextThemeWrapper(getContext(), paramInt)) {
      return;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setPresenter(ActionMenuPresenter paramActionMenuPresenter)
  {
    this.mPresenter = paramActionMenuPresenter;
    this.mPresenter.setMenuView(this);
  }
  
  public boolean showOverflowMenu()
  {
    if ((this.mPresenter != null) && (this.mPresenter.showOverflowMenu())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public static abstract interface ActionMenuChildView
  {
    public abstract boolean needsDividerAfter();
    
    public abstract boolean needsDividerBefore();
  }
  
  private class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    ActionMenuPresenterCallback() {}
    
    public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {}
    
    public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
    {
      return false;
    }
  }
  
  public static class LayoutParams
    extends LinearLayoutCompat.LayoutParams
  {
    @ViewDebug.ExportedProperty
    public int cellsUsed;
    @ViewDebug.ExportedProperty
    public boolean expandable;
    boolean expanded;
    @ViewDebug.ExportedProperty
    public int extraPixels;
    @ViewDebug.ExportedProperty
    public boolean isOverflowButton;
    @ViewDebug.ExportedProperty
    public boolean preventEdgeOffset;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.isOverflowButton = false;
    }
    
    LayoutParams(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      super(paramInt2);
      this.isOverflowButton = paramBoolean;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.isOverflowButton = paramLayoutParams.isOverflowButton;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  private class MenuBuilderCallback
    implements MenuBuilder.Callback
  {
    MenuBuilderCallback() {}
    
    public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      if ((ActionMenuView.this.mOnMenuItemClickListener != null) && (ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(paramMenuItem))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public void onMenuModeChange(MenuBuilder paramMenuBuilder)
    {
      if (ActionMenuView.this.mMenuBuilderCallback != null) {
        ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(paramMenuBuilder);
      }
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean onMenuItemClick(MenuItem paramMenuItem);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\ActionMenuView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */