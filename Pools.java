package android.support.constraint.solver;

final class Pools
{
  private static final boolean DEBUG = false;
  
  static abstract interface Pool<T>
  {
    public abstract T acquire();
    
    public abstract boolean release(T paramT);
    
    public abstract void releaseAll(T[] paramArrayOfT, int paramInt);
  }
  
  static class SimplePool<T>
    implements Pools.Pool<T>
  {
    private final Object[] mPool;
    private int mPoolSize;
    
    SimplePool(int paramInt)
    {
      if (paramInt <= 0) {
        throw new IllegalArgumentException("The max pool size must be > 0");
      }
      this.mPool = new Object[paramInt];
    }
    
    private boolean isInPool(T paramT)
    {
      int i = 0;
      if (i < this.mPoolSize) {
        if (this.mPool[i] != paramT) {}
      }
      for (boolean bool = true;; bool = false)
      {
        return bool;
        i++;
        break;
      }
    }
    
    public T acquire()
    {
      Object localObject;
      if (this.mPoolSize > 0)
      {
        int i = this.mPoolSize - 1;
        localObject = this.mPool[i];
        this.mPool[i] = null;
        this.mPoolSize -= 1;
      }
      for (;;)
      {
        return (T)localObject;
        localObject = null;
      }
    }
    
    public boolean release(T paramT)
    {
      if (this.mPoolSize < this.mPool.length)
      {
        this.mPool[this.mPoolSize] = paramT;
        this.mPoolSize += 1;
      }
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public void releaseAll(T[] paramArrayOfT, int paramInt)
    {
      int i = paramInt;
      if (paramInt > paramArrayOfT.length) {
        i = paramArrayOfT.length;
      }
      for (paramInt = 0; paramInt < i; paramInt++)
      {
        T ? = paramArrayOfT[paramInt];
        if (this.mPoolSize < this.mPool.length)
        {
          this.mPool[this.mPoolSize] = ?;
          this.mPoolSize += 1;
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\constraint\solver\Pools.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */