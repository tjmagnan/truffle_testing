package tech.dcube.companion.managers.server;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.backend.PBRequest.PBResponse;
import tech.dcube.companion.managers.server.backend.VolleySingleton;
import tech.dcube.companion.model.BarcodeResponse;
import tech.dcube.companion.model.BarcodeResponse.Catalogue;
import tech.dcube.companion.model.BarcodeResponseItem;
import tech.dcube.companion.model.PBSendProItem;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

class ItemManager
{
  private static final Gson gson = new Gson();
  private static final ItemManager ourInstance = new ItemManager();
  private DataManager dataManager = DataManager.getInstance();
  private Request reqItemSave;
  private Request reqItemUpdate;
  private Request reqItemsList;
  private Request reqScanBarcode;
  
  static ItemManager getInstance()
  {
    return ourInstance;
  }
  
  String getItemFromBarcode(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<BarcodeResponseItem> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_BARCODE_ITEM_SCAN), new Object[] { paramString });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqScanBarcode = ServerManager.createRequest(0, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (BarcodeResponse)ItemManager.gson.fromJson(paramAnonymousPBResponse.body, BarcodeResponse.class);
        if ((paramAnonymousPBResponse != null) && (paramAnonymousPBResponse.getCatalogue() != null)) {
          if ((paramAnonymousPBResponse.getMessage().equalsIgnoreCase("success")) && (paramRequestCompletionHandler != null)) {
            paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse.getCatalogue().getItem(), ItemManager.this.reqScanBarcode.getTag().toString());
          }
        }
        for (;;)
        {
          return;
          if (paramRequestCompletionHandler != null)
          {
            paramAnonymousPBResponse = new VolleyError("Item not available!");
            paramRequestCompletionHandler.onError(paramAnonymousPBResponse, ItemManager.this.reqScanBarcode.getTag().toString());
          }
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ItemManager.ourInstance.getItemFromBarcode(ItemManager.8.this.val$context, ItemManager.8.this.val$UPC, ItemManager.8.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqScanBarcode);
    return this.reqScanBarcode.getTag().toString();
  }
  
  String getItems(final Context paramContext, final ServerManager.RequestCompletionHandler<List<PBSendProItem>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_GET_ITEMS);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqItemsList = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Object localObject = new JsonParser().parse(paramAnonymousPBResponse.body).getAsJsonObject().get("results");
        paramAnonymousPBResponse = new ArrayList();
        if (localObject != null)
        {
          localObject = PBSendProItem.fromJsonArray(PBSendProItem[].class, ((JsonElement)localObject).toString());
          paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          if (localObject != null)
          {
            ItemManager.this.dataManager.getItems().clear();
            ItemManager.this.dataManager.getItems().addAll((Collection)localObject);
            paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ItemManager.this.reqItemsList.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ItemManager.ourInstance.getItems(ItemManager.6.this.val$context, ItemManager.6.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqItemsList);
    return this.reqItemsList.getTag().toString();
  }
  
  String saveItem(final Context paramContext, final PBSendProItem paramPBSendProItem, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_SAVE_ITEM);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqItemSave = ServerManager.createRequest(1, str, "application/json", localHashMap, paramPBSendProItem.toJsonMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (Map)ItemManager.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        boolean bool2 = false;
        boolean bool1 = bool2;
        if (paramAnonymousPBResponse != null)
        {
          bool1 = bool2;
          if (paramAnonymousPBResponse.get("message").equals("Success"))
          {
            bool1 = true;
            paramAnonymousPBResponse = (Map)paramAnonymousPBResponse.get("results");
            if ((paramAnonymousPBResponse != null) && (paramAnonymousPBResponse.get("insertId") != null)) {
              paramPBSendProItem.setId(paramAnonymousPBResponse.get("insertId").toString());
            }
            Log.wtf("Main", "Item: " + paramPBSendProItem);
            ItemManager.this.dataManager.getItems().add(0, paramPBSendProItem);
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool1), ItemManager.this.reqItemSave.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ItemManager.ourInstance.saveItem(ItemManager.4.this.val$context, ItemManager.4.this.val$item, ItemManager.4.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqItemSave);
    return this.reqItemSave.getTag().toString();
  }
  
  String updateItem(final Context paramContext, final PBSendProItem paramPBSendProItem, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_UPDATE_ITEM), new Object[] { paramPBSendProItem.getId() });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqItemUpdate = ServerManager.createRequest(2, str, "application/json", localHashMap, paramPBSendProItem.toJsonMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (Map)ItemManager.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        boolean bool2 = false;
        boolean bool1 = bool2;
        if (paramAnonymousPBResponse != null)
        {
          bool1 = bool2;
          if (paramAnonymousPBResponse.get("message").equals("Success")) {
            bool1 = true;
          }
        }
        Log.wtf("Main", "Item: " + paramPBSendProItem);
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool1), ItemManager.this.reqItemUpdate.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ItemManager.ourInstance.updateItem(ItemManager.2.this.val$context, ItemManager.2.this.val$item, ItemManager.2.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqItemUpdate);
    return this.reqItemUpdate.getTag().toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\ItemManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */