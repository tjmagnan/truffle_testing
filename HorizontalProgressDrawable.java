package me.zhanghai.android.materialprogressbar;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class HorizontalProgressDrawable
  extends BaseProgressLayerDrawable<SingleHorizontalProgressDrawable, HorizontalProgressBackgroundDrawable>
{
  public HorizontalProgressDrawable(Context paramContext)
  {
    super(new Drawable[] { new HorizontalProgressBackgroundDrawable(paramContext), new SingleHorizontalProgressDrawable(paramContext), new SingleHorizontalProgressDrawable(paramContext) }, paramContext);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\HorizontalProgressDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */