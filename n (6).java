package com.testfairy.n;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.g;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class f
  extends b
{
  private String a = null;
  private PackageManager b;
  private ActivityManager c;
  
  public f(com.testfairy.m.b paramb, ActivityManager paramActivityManager, PackageManager paramPackageManager)
  {
    super(paramb);
    this.c = paramActivityManager;
    this.b = paramPackageManager;
  }
  
  private byte[] a(PackageInfo paramPackageInfo)
  {
    localObject = null;
    for (;;)
    {
      try
      {
        localBitmap = com.testfairy.p.b.a(paramPackageInfo.applicationInfo.loadIcon(this.b));
        paramPackageInfo = e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.d(paramPackageInfo, "App icon is " + localBitmap.getWidth() + "x" + localBitmap.getHeight() + " pixels");
        if ((localBitmap.getWidth() <= 128) && (localBitmap.getHeight() <= 128)) {
          continue;
        }
        Log.d(e.a, "Icon is too big, not sending to server");
        paramPackageInfo = (PackageInfo)localObject;
      }
      catch (Throwable paramPackageInfo)
      {
        Bitmap localBitmap;
        paramPackageInfo = (PackageInfo)localObject;
        continue;
      }
      return paramPackageInfo;
      paramPackageInfo = com.testfairy.p.b.b(localBitmap);
    }
  }
  
  private Map c()
  {
    Object localObject1;
    if ((this.c == null) || (this.b == null)) {
      localObject1 = null;
    }
    for (;;)
    {
      return (Map)localObject1;
      try
      {
        localObject1 = ((ActivityManager.RunningTaskInfo)this.c.getRunningTasks(1).get(0)).topActivity.getPackageName();
        PackageInfo localPackageInfo = this.b.getPackageInfo((String)localObject1, 0);
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>(4);
        ((Map)localObject1).put("taskName", localPackageInfo.applicationInfo.loadLabel(this.b).toString());
        ((Map)localObject1).put("packageName", localPackageInfo.applicationInfo.packageName);
        ((Map)localObject1).put("className", localPackageInfo.applicationInfo.className);
      }
      catch (Exception localException)
      {
        Object localObject2 = null;
      }
    }
  }
  
  private String d()
  {
    String str;
    if (this.c == null) {
      str = null;
    }
    for (;;)
    {
      return str;
      try
      {
        str = ((ActivityManager.RunningTaskInfo)this.c.getRunningTasks(1).get(0)).topActivity.getPackageName();
      }
      catch (Exception localException)
      {
        Object localObject = null;
      }
    }
  }
  
  public void a()
  {
    String str = d();
    if (str == null) {}
    for (;;)
    {
      return;
      if ((this.a == null) || (!this.a.equals(str)))
      {
        Object localObject = c();
        if (localObject != null)
        {
          this.a = str;
          localObject = new g(10, (Map)localObject);
          b().a((g)localObject);
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */