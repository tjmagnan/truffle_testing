package com.testfairy.p;

import android.text.TextUtils;
import android.util.Patterns;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class m
{
  public static String a(String paramString)
  {
    try
    {
      paramString = URLEncoder.encode(paramString, "UTF-8");
      return paramString;
    }
    catch (Throwable paramString)
    {
      for (;;)
      {
        paramString = null;
      }
    }
  }
  
  public static String a(List paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    for (int i = 1; localIterator.hasNext(); i = 0)
    {
      paramList = (String)localIterator.next();
      if (i == 0) {
        localStringBuilder.append(',');
      }
      localStringBuilder.append(paramList);
    }
    return localStringBuilder.toString();
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; i < paramArrayOfByte.length; i++) {
      localStringBuffer.append(Integer.toString((paramArrayOfByte[i] & 0xFF) + 256, 16).substring(1));
    }
    return localStringBuffer.toString();
  }
  
  public static String a(String[] paramArrayOfString)
  {
    return a(Arrays.asList(paramArrayOfString));
  }
  
  public static boolean a(CharSequence paramCharSequence)
  {
    if (TextUtils.isEmpty(paramCharSequence)) {}
    for (boolean bool = false;; bool = Patterns.EMAIL_ADDRESS.matcher(paramCharSequence).matches()) {
      return bool;
    }
  }
  
  public static String b(String paramString)
  {
    try
    {
      String str = URLDecoder.decode(paramString.replaceAll("\\\\x([0-9a-fA-F][0-9a-fA-F])", "%$1"), "UTF-8");
      paramString = str;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  public static String b(List paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 1;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      localIterator.next();
      if (i == 0) {
        localStringBuilder.append(',');
      }
      i = 0;
      localStringBuilder.append(paramList.toString());
    }
    return localStringBuilder.toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */