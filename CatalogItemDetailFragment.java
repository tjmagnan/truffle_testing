package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import java.util.List;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBSendProItem;

public class CatalogItemDetailFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  ImageView EditButton;
  private OnCatalogItemListener mListener;
  private String mParam2;
  private int mPosition;
  PBSendProItem pbSendProItem;
  TextView productDesc;
  EditText productHeight;
  EditText productLenght;
  TextView productName;
  EditText productUPC;
  EditText productWeight;
  EditText productWidth;
  
  public static CatalogItemDetailFragment newInstance(int paramInt, String paramString)
  {
    CatalogItemDetailFragment localCatalogItemDetailFragment = new CatalogItemDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putInt("param1", paramInt);
    localBundle.putString("param2", paramString);
    localCatalogItemDetailFragment.setArguments(localBundle);
    return localCatalogItemDetailFragment;
  }
  
  void hideKeyboard()
  {
    UtilManager.hideSoftKeyboard(getActivity());
  }
  
  @OnClick({2131624097})
  public void hideSoftKeyboardOnClick()
  {
    hideKeyboard();
  }
  
  @OnFocusChange({2131624097})
  public void hideSoftKeyboardOnFocus(boolean paramBoolean)
  {
    if (paramBoolean) {}
  }
  
  void launchEditItemFragment()
  {
    FragmentTransaction localFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.add(2131624061, CatalogEditItemFragment.newInstance(this.mPosition, CatalogEditItemFragment.class.getName())).addToBackStack(CatalogEditItemFragment.class.getName());
    localFragmentTransaction.commit();
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnCatalogItemListener))
    {
      this.mListener = ((OnCatalogItemListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnCatalogItemListener");
  }
  
  public void onBackButtonInteraction()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onCatalogItemDetailInteraction("action_back_button", -1);
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    onBackButtonInteraction();
  }
  
  public void onClickTopBarRightButton()
  {
    onShipButtonInteraction();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mPosition = getArguments().getInt("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968627, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Item Details");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Ship");
    this.productName = ((TextView)paramLayoutInflater.findViewById(2131624099));
    this.productDesc = ((TextView)paramLayoutInflater.findViewById(2131624100));
    this.productLenght = ((EditText)paramLayoutInflater.findViewById(2131624101));
    this.productWidth = ((EditText)paramLayoutInflater.findViewById(2131624102));
    this.productHeight = ((EditText)paramLayoutInflater.findViewById(2131624103));
    this.productWeight = ((EditText)paramLayoutInflater.findViewById(2131624104));
    this.productUPC = ((EditText)paramLayoutInflater.findViewById(2131624105));
    this.EditButton = ((ImageView)paramLayoutInflater.findViewById(2131624106));
    updateItemDetailLayout();
    this.EditButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CatalogItemDetailFragment.this.onEditButtonInteraction();
      }
    });
    hideKeyboard();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onEditButtonInteraction()
  {
    hideKeyboard();
    if (this.mListener != null)
    {
      this.mListener.onCatalogItemDetailInteraction("action_edit_button", -1);
      launchEditItemFragment();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    hideKeyboard();
  }
  
  public void onResume()
  {
    super.onResume();
    hideKeyboard();
  }
  
  public void onShipButtonInteraction()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onCatalogItemDetailInteraction("action_ship_button", this.mPosition);
    }
  }
  
  public void updateItemDetailLayout()
  {
    this.pbSendProItem = ((PBSendProItem)DataManager.getInstance().getItems().get(this.mPosition));
    String str5 = this.pbSendProItem.getItemName();
    String str3 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemLengthIn()) });
    String str1 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemWidthIn()) });
    String str6 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemHeightIn()) });
    String str4 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemWeightLb()) });
    String str2 = this.pbSendProItem.getUpc();
    this.productName.setText(str5);
    this.productDesc.setText("Product Description");
    this.productLenght.setText(str3);
    this.productWidth.setText(str1);
    this.productHeight.setText(str6);
    this.productWeight.setText(str4);
    this.productUPC.setText(str2);
    hideKeyboard();
  }
  
  public static abstract interface OnCatalogItemListener
  {
    public abstract void onCatalogItemDetailInteraction(String paramString, int paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\CatalogItemDetailFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */