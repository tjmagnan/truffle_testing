package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.android.volley.VolleyError;
import java.util.List;
import tech.dcube.companion.customListAdapters.RecipientAddressListAdapter;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBAddress;

public class PBRecipientAddressFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  ListView RecipientAddressList;
  RecipientAddressListAdapter addressListAdapter;
  private OnRecipientAddressSelectedListener mListener;
  private String mParam1;
  private String mParam2;
  List<PBAddress> pbAddressList;
  
  public static PBRecipientAddressFragment newInstance(String paramString1, String paramString2)
  {
    PBRecipientAddressFragment localPBRecipientAddressFragment = new PBRecipientAddressFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBRecipientAddressFragment.setArguments(localBundle);
    return localPBRecipientAddressFragment;
  }
  
  void callLoadRecipientAddressesApi()
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local2 = new tech/dcube/companion/fragments/PBRecipientAddressFragment$2;
      local2.<init>(this);
      localServerManager.getAddressBook(localContext, local2);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void callVerifyAddressApi(PBAddress paramPBAddress)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local3 = new tech/dcube/companion/fragments/PBRecipientAddressFragment$3;
      local3.<init>(this);
      localServerManager.verifyAddress(localContext, paramPBAddress, local3);
      return;
    }
    catch (Exception paramPBAddress)
    {
      for (;;)
      {
        paramPBAddress.printStackTrace();
      }
    }
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnRecipientAddressSelectedListener))
    {
      this.mListener = ((OnRecipientAddressSelectedListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnAddCatalogItemListener");
  }
  
  public void onBackButtonPressed()
  {
    if (this.mListener != null) {
      this.mListener.onRecipientAddressButtonInteraction("action_back_button");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    onBackButtonPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968639, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarLeftButtonText("Back");
    setTopBarTitle("Select Recipient");
    hideTopBarRightButton();
    this.RecipientAddressList = ((ListView)paramLayoutInflater.findViewById(2131624157));
    this.pbAddressList = DataManager.getInstance().getAddresses();
    this.addressListAdapter = new RecipientAddressListAdapter(this.pbAddressList, getContext());
    this.RecipientAddressList.setAdapter(this.addressListAdapter);
    this.RecipientAddressList.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = (PBAddress)DataManager.getInstance().getAddresses().get(paramAnonymousInt);
        PBRecipientAddressFragment.this.callVerifyAddressApi(paramAnonymousAdapterView);
      }
    });
    callLoadRecipientAddressesApi();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onVerifiedAddress(PBAddress paramPBAddress)
  {
    if (this.mListener != null) {
      this.mListener.onRecipientAddressSelected(paramPBAddress);
    }
  }
  
  public static abstract interface OnRecipientAddressSelectedListener
  {
    public abstract void onRecipientAddressButtonInteraction(String paramString);
    
    public abstract void onRecipientAddressSelected(PBAddress paramPBAddress);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBRecipientAddressFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */