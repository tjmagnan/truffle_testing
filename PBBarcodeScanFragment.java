package tech.dcube.companion.fragments;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView.ResultHandler;
import tech.dcube.companion.managers.util.UtilManager;

public class PBBarcodeScanFragment
  extends PBTopBarFragment
  implements ZBarScannerView.ResultHandler
{
  private static final int MY_CAMERA_REQUEST_CODE = 100;
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void handleResult(Result paramResult) {}
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  @Nullable
  public View onCreateView(LayoutInflater paramLayoutInflater, @Nullable ViewGroup paramViewGroup, @Nullable Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968624, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Scan Barcode");
    setTopBarLeftButtonText("Cancel");
    hideTopBarRightButton();
    if ((Build.VERSION.SDK_INT >= 23) && (getActivity().checkSelfPermission("android.permission.CAMERA") != 0)) {
      requestPermissions(new String[] { "android.permission.CAMERA" }, 100);
    }
    return paramLayoutInflater;
  }
  
  public void onRequestPermissionsResult(int paramInt, @NonNull String[] paramArrayOfString, @NonNull int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    if ((paramInt == 100) && (paramArrayOfInt[0] != 0))
    {
      displayToast("Camera permission denied");
      onClickTopBarLeftButton();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBBarcodeScanFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */