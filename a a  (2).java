package com.testfairy.activities.a.a;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.widget.Button;

public class b
  extends Button
{
  private static final int a = 4;
  private static final int b = 8;
  private final c c;
  private final int d;
  private GradientDrawable e = null;
  private boolean f;
  
  public b(Context paramContext, int paramInt, boolean paramBoolean, c paramc)
  {
    super(paramContext);
    this.c = paramc;
    this.f = paramBoolean;
    this.d = paramInt;
    if (Build.VERSION.SDK_INT >= 16)
    {
      this.e = new GradientDrawable();
      this.e.setShape(0);
      this.e.setCornerRadius('Ǵ');
      this.e.setColor(paramInt);
      this.e.setSize(500, 500);
      this.e.setStroke(8, -16711936);
      setBackground(this.e);
      a();
    }
  }
  
  public void a()
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      if (!b()) {
        break label32;
      }
      this.f = false;
      this.e.setStroke(8, -16711936);
    }
    for (;;)
    {
      return;
      label32:
      this.f = true;
      this.e.setStroke(0, -16711936);
    }
  }
  
  public boolean b()
  {
    return this.f;
  }
  
  public void setPressed(boolean paramBoolean)
  {
    super.setPressed(paramBoolean);
    if (!paramBoolean) {}
    for (;;)
    {
      return;
      if (this.f) {
        this.c.a(this, this.d);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\a\a\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */