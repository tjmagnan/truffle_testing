package com.android.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.widget.ImageView.ScaleType;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Request.Priority;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyLog;

public class ImageRequest
  extends Request<Bitmap>
{
  public static final float DEFAULT_IMAGE_BACKOFF_MULT = 2.0F;
  public static final int DEFAULT_IMAGE_MAX_RETRIES = 2;
  public static final int DEFAULT_IMAGE_TIMEOUT_MS = 1000;
  private static final Object sDecodeLock = new Object();
  private final Bitmap.Config mDecodeConfig;
  private final Response.Listener<Bitmap> mListener;
  private final int mMaxHeight;
  private final int mMaxWidth;
  private ImageView.ScaleType mScaleType;
  
  @Deprecated
  public ImageRequest(String paramString, Response.Listener<Bitmap> paramListener, int paramInt1, int paramInt2, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    this(paramString, paramListener, paramInt1, paramInt2, ImageView.ScaleType.CENTER_INSIDE, paramConfig, paramErrorListener);
  }
  
  public ImageRequest(String paramString, Response.Listener<Bitmap> paramListener, int paramInt1, int paramInt2, ImageView.ScaleType paramScaleType, Bitmap.Config paramConfig, Response.ErrorListener paramErrorListener)
  {
    super(0, paramString, paramErrorListener);
    setRetryPolicy(new DefaultRetryPolicy(1000, 2, 2.0F));
    this.mListener = paramListener;
    this.mDecodeConfig = paramConfig;
    this.mMaxWidth = paramInt1;
    this.mMaxHeight = paramInt2;
    this.mScaleType = paramScaleType;
  }
  
  private Response<Bitmap> doParse(NetworkResponse paramNetworkResponse)
  {
    Object localObject2 = paramNetworkResponse.data;
    Object localObject1 = new BitmapFactory.Options();
    if ((this.mMaxWidth == 0) && (this.mMaxHeight == 0))
    {
      ((BitmapFactory.Options)localObject1).inPreferredConfig = this.mDecodeConfig;
      localObject1 = BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
      if (localObject1 != null) {
        break label223;
      }
    }
    label223:
    for (paramNetworkResponse = Response.error(new ParseError(paramNetworkResponse));; paramNetworkResponse = Response.success(localObject1, HttpHeaderParser.parseCacheHeaders(paramNetworkResponse)))
    {
      return paramNetworkResponse;
      ((BitmapFactory.Options)localObject1).inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
      int k = ((BitmapFactory.Options)localObject1).outWidth;
      int j = ((BitmapFactory.Options)localObject1).outHeight;
      int m = getResizedDimension(this.mMaxWidth, this.mMaxHeight, k, j, this.mScaleType);
      int i = getResizedDimension(this.mMaxHeight, this.mMaxWidth, j, k, this.mScaleType);
      ((BitmapFactory.Options)localObject1).inJustDecodeBounds = false;
      ((BitmapFactory.Options)localObject1).inSampleSize = findBestSampleSize(k, j, m, i);
      localObject1 = BitmapFactory.decodeByteArray((byte[])localObject2, 0, localObject2.length, (BitmapFactory.Options)localObject1);
      if ((localObject1 != null) && ((((Bitmap)localObject1).getWidth() > m) || (((Bitmap)localObject1).getHeight() > i)))
      {
        localObject2 = Bitmap.createScaledBitmap((Bitmap)localObject1, m, i, true);
        ((Bitmap)localObject1).recycle();
        localObject1 = localObject2;
        break;
      }
      break;
    }
  }
  
  static int findBestSampleSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    double d = Math.min(paramInt1 / paramInt3, paramInt2 / paramInt4);
    for (float f = 1.0F; 2.0F * f <= d; f *= 2.0F) {}
    return (int)f;
  }
  
  private static int getResizedDimension(int paramInt1, int paramInt2, int paramInt3, int paramInt4, ImageView.ScaleType paramScaleType)
  {
    if ((paramInt1 == 0) && (paramInt2 == 0)) {}
    for (;;)
    {
      return paramInt3;
      if (paramScaleType == ImageView.ScaleType.FIT_XY)
      {
        if (paramInt1 != 0) {
          paramInt3 = paramInt1;
        }
      }
      else
      {
        double d;
        if (paramInt1 == 0)
        {
          d = paramInt2 / paramInt4;
          paramInt3 = (int)(paramInt3 * d);
        }
        else if (paramInt2 == 0)
        {
          paramInt3 = paramInt1;
        }
        else
        {
          d = paramInt4 / paramInt3;
          if (paramScaleType == ImageView.ScaleType.CENTER_CROP)
          {
            paramInt3 = paramInt1;
            if (paramInt1 * d < paramInt2) {
              paramInt3 = (int)(paramInt2 / d);
            }
          }
          else
          {
            paramInt3 = paramInt1;
            if (paramInt1 * d > paramInt2) {
              paramInt3 = (int)(paramInt2 / d);
            }
          }
        }
      }
    }
  }
  
  protected void deliverResponse(Bitmap paramBitmap)
  {
    this.mListener.onResponse(paramBitmap);
  }
  
  public Request.Priority getPriority()
  {
    return Request.Priority.LOW;
  }
  
  protected Response<Bitmap> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    synchronized (sDecodeLock)
    {
      try
      {
        Response localResponse = doParse(paramNetworkResponse);
        paramNetworkResponse = localResponse;
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        for (;;)
        {
          VolleyLog.e("Caught OOM for %d byte image, url=%s", new Object[] { Integer.valueOf(paramNetworkResponse.data.length), getUrl() });
          paramNetworkResponse = new com/android/volley/ParseError;
          paramNetworkResponse.<init>(localOutOfMemoryError);
          paramNetworkResponse = Response.error(paramNetworkResponse);
        }
      }
      return paramNetworkResponse;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\toolbox\ImageRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */