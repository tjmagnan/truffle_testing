package com.google.gson.internal;

public abstract interface ObjectConstructor<T>
{
  public abstract T construct();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\google\gson\internal\ObjectConstructor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */