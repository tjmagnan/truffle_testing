package me.dm7.barcodescanner.core;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import java.util.Iterator;
import java.util.List;

public class CameraPreview
  extends SurfaceView
  implements SurfaceHolder.Callback
{
  private static final String TAG = "CameraPreview";
  Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback()
  {
    public void onAutoFocus(boolean paramAnonymousBoolean, Camera paramAnonymousCamera)
    {
      CameraPreview.this.scheduleAutoFocus();
    }
  };
  private Runnable doAutoFocus = new Runnable()
  {
    public void run()
    {
      if ((CameraPreview.this.mCameraWrapper != null) && (CameraPreview.this.mPreviewing) && (CameraPreview.this.mAutoFocus) && (CameraPreview.this.mSurfaceCreated)) {
        CameraPreview.this.safeAutoFocus();
      }
    }
  };
  private boolean mAutoFocus = true;
  private Handler mAutoFocusHandler;
  private CameraWrapper mCameraWrapper;
  private Camera.PreviewCallback mPreviewCallback;
  private boolean mPreviewing = true;
  private boolean mShouldScaleToFill = true;
  private boolean mSurfaceCreated = false;
  
  public CameraPreview(Context paramContext, AttributeSet paramAttributeSet, CameraWrapper paramCameraWrapper, Camera.PreviewCallback paramPreviewCallback)
  {
    super(paramContext, paramAttributeSet);
    init(paramCameraWrapper, paramPreviewCallback);
  }
  
  public CameraPreview(Context paramContext, CameraWrapper paramCameraWrapper, Camera.PreviewCallback paramPreviewCallback)
  {
    super(paramContext);
    init(paramCameraWrapper, paramPreviewCallback);
  }
  
  private void adjustViewSize(Camera.Size paramSize)
  {
    Point localPoint = convertSizeToLandscapeOrientation(new Point(getWidth(), getHeight()));
    float f = paramSize.width / paramSize.height;
    if (localPoint.x / localPoint.y > f) {
      setViewSize((int)(localPoint.y * f), localPoint.y);
    }
    for (;;)
    {
      return;
      setViewSize(localPoint.x, (int)(localPoint.x / f));
    }
  }
  
  private Point convertSizeToLandscapeOrientation(Point paramPoint)
  {
    if (getDisplayOrientation() % 180 == 0) {}
    for (;;)
    {
      return paramPoint;
      paramPoint = new Point(paramPoint.y, paramPoint.x);
    }
  }
  
  private Camera.Size getOptimalPreviewSize()
  {
    Object localObject2;
    if (this.mCameraWrapper == null) {
      localObject2 = null;
    }
    int m;
    Object localObject1;
    do
    {
      double d2;
      for (;;)
      {
        return (Camera.Size)localObject2;
        localObject3 = this.mCameraWrapper.mCamera.getParameters().getSupportedPreviewSizes();
        int k = getWidth();
        int i = getHeight();
        m = i;
        int j = k;
        if (DisplayUtils.getScreenOrientation(getContext()) == 1)
        {
          j = i;
          m = k;
        }
        d2 = j / m;
        if (localObject3 != null) {
          break;
        }
        localObject2 = null;
      }
      localObject1 = null;
      d1 = Double.MAX_VALUE;
      Iterator localIterator = ((List)localObject3).iterator();
      while (localIterator.hasNext())
      {
        localObject2 = (Camera.Size)localIterator.next();
        if ((Math.abs(((Camera.Size)localObject2).width / ((Camera.Size)localObject2).height - d2) <= 0.1D) && (Math.abs(((Camera.Size)localObject2).height - m) < d1))
        {
          localObject1 = localObject2;
          d1 = Math.abs(((Camera.Size)localObject2).height - m);
        }
      }
      localObject2 = localObject1;
    } while (localObject1 != null);
    double d1 = Double.MAX_VALUE;
    Object localObject3 = ((List)localObject3).iterator();
    for (;;)
    {
      localObject2 = localObject1;
      if (!((Iterator)localObject3).hasNext()) {
        break;
      }
      localObject2 = (Camera.Size)((Iterator)localObject3).next();
      if (Math.abs(((Camera.Size)localObject2).height - m) < d1)
      {
        localObject1 = localObject2;
        d1 = Math.abs(((Camera.Size)localObject2).height - m);
      }
    }
  }
  
  private void scheduleAutoFocus()
  {
    this.mAutoFocusHandler.postDelayed(this.doAutoFocus, 1000L);
  }
  
  private void setViewSize(int paramInt1, int paramInt2)
  {
    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
    int i;
    int j;
    float f1;
    if (getDisplayOrientation() % 180 == 0)
    {
      i = paramInt2;
      paramInt2 = paramInt1;
      paramInt1 = i;
      j = paramInt1;
      i = paramInt2;
      if (this.mShouldScaleToFill)
      {
        i = ((View)getParent()).getWidth();
        j = ((View)getParent()).getHeight();
        float f2 = i / paramInt2;
        f1 = j / paramInt1;
        if (f2 <= f1) {
          break label129;
        }
        f1 = f2;
      }
    }
    label129:
    for (;;)
    {
      i = Math.round(paramInt2 * f1);
      j = Math.round(paramInt1 * f1);
      localLayoutParams.width = i;
      localLayoutParams.height = j;
      setLayoutParams(localLayoutParams);
      return;
      break;
    }
  }
  
  public int getDisplayOrientation()
  {
    int i = 0;
    if (this.mCameraWrapper == null) {}
    for (;;)
    {
      return i;
      Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
      if (this.mCameraWrapper.mCameraId == -1)
      {
        Camera.getCameraInfo(0, localCameraInfo);
        label35:
        int j = ((WindowManager)getContext().getSystemService("window")).getDefaultDisplay().getRotation();
        i = 0;
        switch (j)
        {
        }
      }
      for (;;)
      {
        if (localCameraInfo.facing != 1) {
          break label157;
        }
        i = (360 - (localCameraInfo.orientation + i) % 360) % 360;
        break;
        Camera.getCameraInfo(this.mCameraWrapper.mCameraId, localCameraInfo);
        break label35;
        i = 0;
        continue;
        i = 90;
        continue;
        i = 180;
        continue;
        i = 270;
      }
      label157:
      i = (localCameraInfo.orientation - i + 360) % 360;
    }
  }
  
  public void init(CameraWrapper paramCameraWrapper, Camera.PreviewCallback paramPreviewCallback)
  {
    setCamera(paramCameraWrapper, paramPreviewCallback);
    this.mAutoFocusHandler = new Handler();
    getHolder().addCallback(this);
    getHolder().setType(3);
  }
  
  public void safeAutoFocus()
  {
    try
    {
      this.mCameraWrapper.mCamera.autoFocus(this.autoFocusCB);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        scheduleAutoFocus();
      }
    }
  }
  
  public void setAutoFocus(boolean paramBoolean)
  {
    if ((this.mCameraWrapper == null) || (!this.mPreviewing) || (paramBoolean == this.mAutoFocus)) {}
    for (;;)
    {
      return;
      this.mAutoFocus = paramBoolean;
      if (this.mAutoFocus)
      {
        if (this.mSurfaceCreated)
        {
          Log.v("CameraPreview", "Starting autofocus");
          safeAutoFocus();
        }
        else
        {
          scheduleAutoFocus();
        }
      }
      else
      {
        Log.v("CameraPreview", "Cancelling autofocus");
        this.mCameraWrapper.mCamera.cancelAutoFocus();
      }
    }
  }
  
  public void setCamera(CameraWrapper paramCameraWrapper, Camera.PreviewCallback paramPreviewCallback)
  {
    this.mCameraWrapper = paramCameraWrapper;
    this.mPreviewCallback = paramPreviewCallback;
  }
  
  public void setShouldScaleToFill(boolean paramBoolean)
  {
    this.mShouldScaleToFill = paramBoolean;
  }
  
  public void setupCameraParameters()
  {
    Camera.Size localSize = getOptimalPreviewSize();
    Camera.Parameters localParameters = this.mCameraWrapper.mCamera.getParameters();
    localParameters.setPreviewSize(localSize.width, localSize.height);
    this.mCameraWrapper.mCamera.setParameters(localParameters);
    adjustViewSize(localSize);
  }
  
  public void showCameraPreview()
  {
    if (this.mCameraWrapper != null) {}
    try
    {
      getHolder().addCallback(this);
      this.mPreviewing = true;
      setupCameraParameters();
      this.mCameraWrapper.mCamera.setPreviewDisplay(getHolder());
      this.mCameraWrapper.mCamera.setDisplayOrientation(getDisplayOrientation());
      this.mCameraWrapper.mCamera.setOneShotPreviewCallback(this.mPreviewCallback);
      this.mCameraWrapper.mCamera.startPreview();
      if (this.mAutoFocus)
      {
        if (!this.mSurfaceCreated) {
          break label97;
        }
        safeAutoFocus();
      }
      for (;;)
      {
        return;
        label97:
        scheduleAutoFocus();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("CameraPreview", localException.toString(), localException);
      }
    }
  }
  
  public void stopCameraPreview()
  {
    if (this.mCameraWrapper != null) {}
    try
    {
      this.mPreviewing = false;
      getHolder().removeCallback(this);
      this.mCameraWrapper.mCamera.cancelAutoFocus();
      this.mCameraWrapper.mCamera.setOneShotPreviewCallback(null);
      this.mCameraWrapper.mCamera.stopPreview();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("CameraPreview", localException.toString(), localException);
      }
    }
  }
  
  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramSurfaceHolder.getSurface() == null) {}
    for (;;)
    {
      return;
      stopCameraPreview();
      showCameraPreview();
    }
  }
  
  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    this.mSurfaceCreated = true;
  }
  
  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    this.mSurfaceCreated = false;
    stopCameraPreview();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\CameraPreview.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */