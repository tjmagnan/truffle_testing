package tech.dcube.companion.model;

public final class PBSubscriptionTypes
{
  public static final String DEVICE = "DEVICE";
  public static final String FUNDS = "FUNDS";
  public static final String SUPPLY = "SUPPLY";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBSubscriptionTypes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */