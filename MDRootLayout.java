package com.afollestad.materialdialogs.internal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ScrollView;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.R.attr;
import com.afollestad.materialdialogs.R.dimen;
import com.afollestad.materialdialogs.R.id;
import com.afollestad.materialdialogs.R.styleable;
import com.afollestad.materialdialogs.StackingBehavior;
import com.afollestad.materialdialogs.util.DialogUtils;

public class MDRootLayout
  extends ViewGroup
{
  private static final int INDEX_NEGATIVE = 1;
  private static final int INDEX_NEUTRAL = 0;
  private static final int INDEX_POSITIVE = 2;
  private ViewTreeObserver.OnScrollChangedListener bottomOnScrollChangedListener;
  private int buttonBarHeight;
  private GravityEnum buttonGravity = GravityEnum.START;
  private int buttonHorizontalEdgeMargin;
  private int buttonPaddingFull;
  private final MDButton[] buttons = new MDButton[3];
  private View content;
  private Paint dividerPaint;
  private int dividerWidth;
  private boolean drawBottomDivider = false;
  private boolean drawTopDivider = false;
  private boolean isStacked = false;
  private int maxHeight;
  private boolean noTitleNoPadding;
  private int noTitlePaddingFull;
  private boolean reducePaddingNoTitleNoButtons;
  private StackingBehavior stackBehavior = StackingBehavior.ADAPTIVE;
  private View titleBar;
  private ViewTreeObserver.OnScrollChangedListener topOnScrollChangedListener;
  private boolean useFullPadding = true;
  
  public MDRootLayout(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null, 0);
  }
  
  public MDRootLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet, 0);
  }
  
  @TargetApi(11)
  public MDRootLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet, paramInt);
  }
  
  @TargetApi(21)
  public MDRootLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet, paramInt1);
  }
  
  private void addScrollListener(final ViewGroup paramViewGroup, final boolean paramBoolean1, final boolean paramBoolean2)
  {
    if (((!paramBoolean2) && (this.topOnScrollChangedListener == null)) || ((paramBoolean2) && (this.bottomOnScrollChangedListener == null)))
    {
      if ((paramViewGroup instanceof RecyclerView))
      {
        localObject = new RecyclerView.OnScrollListener()
        {
          public void onScrolled(RecyclerView paramAnonymousRecyclerView, int paramAnonymousInt1, int paramAnonymousInt2)
          {
            super.onScrolled(paramAnonymousRecyclerView, paramAnonymousInt1, paramAnonymousInt2);
            boolean bool2 = false;
            paramAnonymousRecyclerView = MDRootLayout.this.buttons;
            paramAnonymousInt2 = paramAnonymousRecyclerView.length;
            for (paramAnonymousInt1 = 0;; paramAnonymousInt1++)
            {
              boolean bool1 = bool2;
              if (paramAnonymousInt1 < paramAnonymousInt2)
              {
                Object localObject = paramAnonymousRecyclerView[paramAnonymousInt1];
                if ((localObject != null) && (((MDButton)localObject).getVisibility() != 8)) {
                  bool1 = true;
                }
              }
              else
              {
                MDRootLayout.this.invalidateDividersForScrollingView(paramViewGroup, paramBoolean1, paramBoolean2, bool1);
                MDRootLayout.this.invalidate();
                return;
              }
            }
          }
        };
        ((RecyclerView)paramViewGroup).addOnScrollListener((RecyclerView.OnScrollListener)localObject);
        ((RecyclerView.OnScrollListener)localObject).onScrolled((RecyclerView)paramViewGroup, 0, 0);
      }
    }
    else {
      return;
    }
    Object localObject = new ViewTreeObserver.OnScrollChangedListener()
    {
      public void onScrollChanged()
      {
        boolean bool2 = false;
        MDButton[] arrayOfMDButton = MDRootLayout.this.buttons;
        int j = arrayOfMDButton.length;
        int i = 0;
        boolean bool1 = bool2;
        if (i < j)
        {
          MDButton localMDButton = arrayOfMDButton[i];
          if ((localMDButton != null) && (localMDButton.getVisibility() != 8)) {
            bool1 = true;
          }
        }
        else
        {
          if (!(paramViewGroup instanceof WebView)) {
            break label96;
          }
          MDRootLayout.this.invalidateDividersForWebView((WebView)paramViewGroup, paramBoolean1, paramBoolean2, bool1);
        }
        for (;;)
        {
          MDRootLayout.this.invalidate();
          return;
          i++;
          break;
          label96:
          MDRootLayout.this.invalidateDividersForScrollingView(paramViewGroup, paramBoolean1, paramBoolean2, bool1);
        }
      }
    };
    if (!paramBoolean2)
    {
      this.topOnScrollChangedListener = ((ViewTreeObserver.OnScrollChangedListener)localObject);
      paramViewGroup.getViewTreeObserver().addOnScrollChangedListener(this.topOnScrollChangedListener);
    }
    for (;;)
    {
      ((ViewTreeObserver.OnScrollChangedListener)localObject).onScrollChanged();
      break;
      this.bottomOnScrollChangedListener = ((ViewTreeObserver.OnScrollChangedListener)localObject);
      paramViewGroup.getViewTreeObserver().addOnScrollChangedListener(this.bottomOnScrollChangedListener);
    }
  }
  
  private static boolean canAdapterViewScroll(AdapterView paramAdapterView)
  {
    boolean bool2 = true;
    boolean bool1;
    if (paramAdapterView.getLastVisiblePosition() == -1)
    {
      bool1 = false;
      return bool1;
    }
    int i;
    if (paramAdapterView.getFirstVisiblePosition() == 0)
    {
      i = 1;
      label24:
      if (paramAdapterView.getLastVisiblePosition() != paramAdapterView.getCount() - 1) {
        break label119;
      }
    }
    label119:
    for (int j = 1;; j = 0)
    {
      bool1 = bool2;
      if (i == 0) {
        break;
      }
      bool1 = bool2;
      if (j == 0) {
        break;
      }
      bool1 = bool2;
      if (paramAdapterView.getChildCount() <= 0) {
        break;
      }
      bool1 = bool2;
      if (paramAdapterView.getChildAt(0).getTop() < paramAdapterView.getPaddingTop()) {
        break;
      }
      bool1 = bool2;
      if (paramAdapterView.getChildAt(paramAdapterView.getChildCount() - 1).getBottom() > paramAdapterView.getHeight() - paramAdapterView.getPaddingBottom()) {
        break;
      }
      bool1 = false;
      break;
      i = 0;
      break label24;
    }
  }
  
  public static boolean canRecyclerViewScroll(RecyclerView paramRecyclerView)
  {
    if ((paramRecyclerView != null) && (paramRecyclerView.getLayoutManager() != null) && (paramRecyclerView.getLayoutManager().canScrollVertically())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private static boolean canScrollViewScroll(ScrollView paramScrollView)
  {
    boolean bool = false;
    if (paramScrollView.getChildCount() == 0) {}
    for (;;)
    {
      return bool;
      int i = paramScrollView.getChildAt(0).getMeasuredHeight();
      if (paramScrollView.getMeasuredHeight() - paramScrollView.getPaddingTop() - paramScrollView.getPaddingBottom() < i) {
        bool = true;
      }
    }
  }
  
  private static boolean canWebViewScroll(WebView paramWebView)
  {
    if (paramWebView.getMeasuredHeight() < paramWebView.getContentHeight() * paramWebView.getScale()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @Nullable
  private static View getBottomView(ViewGroup paramViewGroup)
  {
    Object localObject1;
    if ((paramViewGroup == null) || (paramViewGroup.getChildCount() == 0))
    {
      localObject1 = null;
      return (View)localObject1;
    }
    Object localObject2 = null;
    for (int i = paramViewGroup.getChildCount() - 1;; i--)
    {
      localObject1 = localObject2;
      if (i < 0) {
        break;
      }
      localObject1 = paramViewGroup.getChildAt(i);
      if ((((View)localObject1).getVisibility() == 0) && (((View)localObject1).getBottom() == paramViewGroup.getMeasuredHeight())) {
        break;
      }
    }
  }
  
  @Nullable
  private static View getTopView(ViewGroup paramViewGroup)
  {
    Object localObject1;
    if ((paramViewGroup == null) || (paramViewGroup.getChildCount() == 0))
    {
      localObject1 = null;
      return (View)localObject1;
    }
    Object localObject2 = null;
    for (int i = paramViewGroup.getChildCount() - 1;; i--)
    {
      localObject1 = localObject2;
      if (i < 0) {
        break;
      }
      localObject1 = paramViewGroup.getChildAt(i);
      if ((((View)localObject1).getVisibility() == 0) && (((View)localObject1).getTop() == 0)) {
        break;
      }
    }
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    Resources localResources = paramContext.getResources();
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.MDRootLayout, paramInt, 0);
    this.reducePaddingNoTitleNoButtons = paramAttributeSet.getBoolean(R.styleable.MDRootLayout_md_reduce_padding_no_title_no_buttons, true);
    paramAttributeSet.recycle();
    this.noTitlePaddingFull = localResources.getDimensionPixelSize(R.dimen.md_notitle_vertical_padding);
    this.buttonPaddingFull = localResources.getDimensionPixelSize(R.dimen.md_button_frame_vertical_padding);
    this.buttonHorizontalEdgeMargin = localResources.getDimensionPixelSize(R.dimen.md_button_padding_frame_side);
    this.buttonBarHeight = localResources.getDimensionPixelSize(R.dimen.md_button_height);
    this.dividerPaint = new Paint();
    this.dividerWidth = localResources.getDimensionPixelSize(R.dimen.md_divider_height);
    this.dividerPaint.setColor(DialogUtils.resolveColor(paramContext, R.attr.md_divider_color));
    setWillNotDraw(false);
  }
  
  private void invalidateDividersForScrollingView(ViewGroup paramViewGroup, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    boolean bool = true;
    if ((paramBoolean1) && (paramViewGroup.getChildCount() > 0))
    {
      if ((this.titleBar != null) && (this.titleBar.getVisibility() != 8) && (paramViewGroup.getScrollY() + paramViewGroup.getPaddingTop() > paramViewGroup.getChildAt(0).getTop()))
      {
        paramBoolean1 = true;
        this.drawTopDivider = paramBoolean1;
      }
    }
    else if ((paramBoolean2) && (paramViewGroup.getChildCount() > 0)) {
      if ((!paramBoolean3) || (paramViewGroup.getScrollY() + paramViewGroup.getHeight() - paramViewGroup.getPaddingBottom() >= paramViewGroup.getChildAt(paramViewGroup.getChildCount() - 1).getBottom())) {
        break label120;
      }
    }
    label120:
    for (paramBoolean1 = bool;; paramBoolean1 = false)
    {
      this.drawBottomDivider = paramBoolean1;
      return;
      paramBoolean1 = false;
      break;
    }
  }
  
  private void invalidateDividersForWebView(WebView paramWebView, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    boolean bool = true;
    if (paramBoolean1)
    {
      if ((this.titleBar != null) && (this.titleBar.getVisibility() != 8) && (paramWebView.getScrollY() + paramWebView.getPaddingTop() > 0))
      {
        paramBoolean1 = true;
        this.drawTopDivider = paramBoolean1;
      }
    }
    else if (paramBoolean2) {
      if ((!paramBoolean3) || (paramWebView.getScrollY() + paramWebView.getMeasuredHeight() - paramWebView.getPaddingBottom() >= paramWebView.getContentHeight() * paramWebView.getScale())) {
        break label97;
      }
    }
    label97:
    for (paramBoolean1 = bool;; paramBoolean1 = false)
    {
      this.drawBottomDivider = paramBoolean1;
      return;
      paramBoolean1 = false;
      break;
    }
  }
  
  private void invertGravityIfNecessary()
  {
    if (Build.VERSION.SDK_INT < 17) {}
    for (;;)
    {
      return;
      if (getResources().getConfiguration().getLayoutDirection() == 1) {
        switch (this.buttonGravity)
        {
        default: 
          break;
        case ???: 
          this.buttonGravity = GravityEnum.END;
          break;
        case ???: 
          this.buttonGravity = GravityEnum.START;
        }
      }
    }
  }
  
  private static boolean isVisible(View paramView)
  {
    boolean bool1;
    if ((paramView != null) && (paramView.getVisibility() != 8))
    {
      bool1 = true;
      bool2 = bool1;
      if (bool1)
      {
        bool2 = bool1;
        if ((paramView instanceof MDButton)) {
          if (((MDButton)paramView).getText().toString().trim().length() <= 0) {
            break label60;
          }
        }
      }
    }
    label60:
    for (boolean bool2 = true;; bool2 = false)
    {
      return bool2;
      bool1 = false;
      break;
    }
  }
  
  private void setUpDividersVisibility(final View paramView, final boolean paramBoolean1, final boolean paramBoolean2)
  {
    if (paramView == null) {}
    for (;;)
    {
      return;
      if ((paramView instanceof ScrollView))
      {
        paramView = (ScrollView)paramView;
        if (canScrollViewScroll(paramView))
        {
          addScrollListener(paramView, paramBoolean1, paramBoolean2);
        }
        else
        {
          if (paramBoolean1) {
            this.drawTopDivider = false;
          }
          if (paramBoolean2) {
            this.drawBottomDivider = false;
          }
        }
      }
      else if ((paramView instanceof AdapterView))
      {
        paramView = (AdapterView)paramView;
        if (canAdapterViewScroll(paramView))
        {
          addScrollListener(paramView, paramBoolean1, paramBoolean2);
        }
        else
        {
          if (paramBoolean1) {
            this.drawTopDivider = false;
          }
          if (paramBoolean2) {
            this.drawBottomDivider = false;
          }
        }
      }
      else if ((paramView instanceof WebView))
      {
        paramView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
        {
          public boolean onPreDraw()
          {
            if (paramView.getMeasuredHeight() != 0)
            {
              if (MDRootLayout.canWebViewScroll((WebView)paramView)) {
                break label68;
              }
              if (paramBoolean1) {
                MDRootLayout.access$102(MDRootLayout.this, false);
              }
              if (paramBoolean2) {
                MDRootLayout.access$202(MDRootLayout.this, false);
              }
            }
            for (;;)
            {
              paramView.getViewTreeObserver().removeOnPreDrawListener(this);
              return true;
              label68:
              MDRootLayout.this.addScrollListener((ViewGroup)paramView, paramBoolean1, paramBoolean2);
            }
          }
        });
      }
      else if ((paramView instanceof RecyclerView))
      {
        boolean bool = canRecyclerViewScroll((RecyclerView)paramView);
        if (paramBoolean1) {
          this.drawTopDivider = bool;
        }
        if (paramBoolean2) {
          this.drawBottomDivider = bool;
        }
        if (bool) {
          addScrollListener((ViewGroup)paramView, paramBoolean1, paramBoolean2);
        }
      }
      else if ((paramView instanceof ViewGroup))
      {
        View localView = getTopView((ViewGroup)paramView);
        setUpDividersVisibility(localView, paramBoolean1, paramBoolean2);
        paramView = getBottomView((ViewGroup)paramView);
        if (paramView != localView) {
          setUpDividersVisibility(paramView, false, true);
        }
      }
    }
  }
  
  public void noTitleNoPadding()
  {
    this.noTitleNoPadding = true;
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.content != null)
    {
      int i;
      if (this.drawTopDivider)
      {
        i = this.content.getTop();
        paramCanvas.drawRect(0.0F, i - this.dividerWidth, getMeasuredWidth(), i, this.dividerPaint);
      }
      if (this.drawBottomDivider)
      {
        i = this.content.getBottom();
        paramCanvas.drawRect(0.0F, i, getMeasuredWidth(), this.dividerWidth + i, this.dividerPaint);
      }
    }
  }
  
  public void onFinishInflate()
  {
    super.onFinishInflate();
    int i = 0;
    if (i < getChildCount())
    {
      View localView = getChildAt(i);
      if (localView.getId() == R.id.md_titleFrame) {
        this.titleBar = localView;
      }
      for (;;)
      {
        i++;
        break;
        if (localView.getId() == R.id.md_buttonDefaultNeutral) {
          this.buttons[0] = ((MDButton)localView);
        } else if (localView.getId() == R.id.md_buttonDefaultNegative) {
          this.buttons[1] = ((MDButton)localView);
        } else if (localView.getId() == R.id.md_buttonDefaultPositive) {
          this.buttons[2] = ((MDButton)localView);
        } else {
          this.content = localView;
        }
      }
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (isVisible(this.titleBar))
    {
      i = this.titleBar.getMeasuredHeight();
      this.titleBar.layout(paramInt1, paramInt2, paramInt3, paramInt2 + i);
      i = paramInt2 + i;
    }
    for (;;)
    {
      if (isVisible(this.content)) {
        this.content.layout(paramInt1, i, paramInt3, this.content.getMeasuredHeight() + i);
      }
      if (!this.isStacked) {
        break;
      }
      paramInt2 = paramInt4 - this.buttonPaddingFull;
      MDButton[] arrayOfMDButton = this.buttons;
      j = arrayOfMDButton.length;
      i = 0;
      while (i < j)
      {
        MDButton localMDButton = arrayOfMDButton[i];
        paramInt4 = paramInt2;
        if (isVisible(localMDButton))
        {
          localMDButton.layout(paramInt1, paramInt2 - localMDButton.getMeasuredHeight(), paramInt3, paramInt2);
          paramInt4 = paramInt2 - localMDButton.getMeasuredHeight();
        }
        i++;
        paramInt2 = paramInt4;
      }
      i = paramInt2;
      if (!this.noTitleNoPadding)
      {
        i = paramInt2;
        if (this.useFullPadding) {
          i = paramInt2 + this.noTitlePaddingFull;
        }
      }
    }
    int k = paramInt4;
    if (this.useFullPadding) {
      k = paramInt4 - this.buttonPaddingFull;
    }
    int i1 = k - this.buttonBarHeight;
    int m = this.buttonHorizontalEdgeMargin;
    int j = -1;
    paramInt2 = -1;
    int i = paramInt2;
    paramInt4 = m;
    if (isVisible(this.buttons[2]))
    {
      if (this.buttonGravity == GravityEnum.END)
      {
        paramInt4 = paramInt1 + m;
        int n = paramInt4 + this.buttons[2].getMeasuredWidth();
        i = paramInt2;
        paramInt2 = n;
        this.buttons[2].layout(paramInt4, i1, paramInt2, k);
        paramInt4 = m + this.buttons[2].getMeasuredWidth();
      }
    }
    else
    {
      paramInt2 = j;
      if (isVisible(this.buttons[1]))
      {
        if (this.buttonGravity != GravityEnum.END) {
          break label472;
        }
        paramInt2 = paramInt1 + paramInt4;
        paramInt4 = paramInt2 + this.buttons[1].getMeasuredWidth();
        label359:
        this.buttons[1].layout(paramInt2, i1, paramInt4, k);
        paramInt2 = j;
      }
      if (isVisible(this.buttons[0]))
      {
        if (this.buttonGravity != GravityEnum.END) {
          break label532;
        }
        paramInt2 = paramInt3 - this.buttonHorizontalEdgeMargin;
        paramInt1 = paramInt2 - this.buttons[0].getMeasuredWidth();
      }
    }
    for (;;)
    {
      this.buttons[0].layout(paramInt1, i1, paramInt2, k);
      setUpDividersVisibility(this.content, true, true);
      return;
      paramInt2 = paramInt3 - m;
      paramInt4 = paramInt2 - this.buttons[2].getMeasuredWidth();
      i = paramInt4;
      break;
      label472:
      if (this.buttonGravity == GravityEnum.START)
      {
        paramInt4 = paramInt3 - paramInt4;
        paramInt2 = paramInt4 - this.buttons[1].getMeasuredWidth();
        break label359;
      }
      paramInt2 = paramInt1 + this.buttonHorizontalEdgeMargin;
      paramInt4 = paramInt2 + this.buttons[1].getMeasuredWidth();
      j = paramInt4;
      break label359;
      label532:
      if (this.buttonGravity != GravityEnum.START) {
        break label564;
      }
      paramInt1 += this.buttonHorizontalEdgeMargin;
      paramInt2 = paramInt1 + this.buttons[0].getMeasuredWidth();
    }
    label564:
    if ((paramInt2 == -1) && (i != -1))
    {
      paramInt2 = i - this.buttons[0].getMeasuredWidth();
      paramInt4 = i;
    }
    for (;;)
    {
      paramInt1 = paramInt2;
      paramInt2 = paramInt4;
      break;
      if ((i == -1) && (paramInt2 != -1))
      {
        paramInt4 = paramInt2 + this.buttons[0].getMeasuredWidth();
      }
      else
      {
        paramInt4 = i;
        if (i == -1)
        {
          paramInt2 = (paramInt3 - paramInt1) / 2 - this.buttons[0].getMeasuredWidth() / 2;
          paramInt4 = paramInt2 + this.buttons[0].getMeasuredWidth();
        }
      }
    }
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    int i2 = View.MeasureSpec.getSize(paramInt1);
    int i = View.MeasureSpec.getSize(paramInt2);
    int j = i;
    if (i > this.maxHeight) {
      j = this.maxHeight;
    }
    this.useFullPadding = true;
    i = 0;
    int k = 0;
    if (this.stackBehavior == StackingBehavior.ALWAYS) {
      bool = true;
    }
    int m;
    MDButton[] arrayOfMDButton;
    int n;
    for (i = k;; i = k)
    {
      i1 = 0;
      k = 0;
      this.isStacked = bool;
      m = i;
      if (!bool) {
        break label330;
      }
      arrayOfMDButton = this.buttons;
      i3 = arrayOfMDButton.length;
      n = 0;
      for (;;)
      {
        m = i;
        i1 = k;
        if (n >= i3) {
          break;
        }
        localObject = arrayOfMDButton[n];
        i1 = i;
        m = k;
        if (localObject != null)
        {
          i1 = i;
          m = k;
          if (isVisible((View)localObject))
          {
            ((MDButton)localObject).setStacked(true, false);
            measureChild((View)localObject, paramInt1, paramInt2);
            m = k + ((MDButton)localObject).getMeasuredHeight();
            i1 = 1;
          }
        }
        n++;
        i = i1;
        k = m;
      }
      if (this.stackBehavior != StackingBehavior.NEVER) {
        break;
      }
      bool = false;
    }
    int i1 = 0;
    Object localObject = this.buttons;
    int i3 = localObject.length;
    k = 0;
    while (k < i3)
    {
      arrayOfMDButton = localObject[k];
      n = i1;
      m = i;
      if (arrayOfMDButton != null)
      {
        n = i1;
        m = i;
        if (isVisible(arrayOfMDButton))
        {
          arrayOfMDButton.setStacked(false, false);
          measureChild(arrayOfMDButton, paramInt1, paramInt2);
          n = i1 + arrayOfMDButton.getMeasuredWidth();
          m = 1;
        }
      }
      k++;
      i1 = n;
      i = m;
    }
    if (i1 > i2 - getContext().getResources().getDimensionPixelSize(R.dimen.md_neutral_button_margin) * 2) {}
    for (boolean bool = true;; bool = false) {
      break;
    }
    label330:
    paramInt1 = j;
    k = 0;
    if (m != 0) {
      if (this.isStacked)
      {
        paramInt1 -= i1;
        paramInt2 = 0 + this.buttonPaddingFull * 2;
        k = 0 + this.buttonPaddingFull * 2;
        if (!isVisible(this.titleBar)) {
          break label544;
        }
        this.titleBar.measure(View.MeasureSpec.makeMeasureSpec(i2, 1073741824), 0);
        i = paramInt1 - this.titleBar.getMeasuredHeight();
        n = paramInt2;
        label411:
        paramInt1 = i;
        if (isVisible(this.content))
        {
          this.content.measure(View.MeasureSpec.makeMeasureSpec(i2, 1073741824), View.MeasureSpec.makeMeasureSpec(i - k, Integer.MIN_VALUE));
          if (this.content.getMeasuredHeight() > i - n) {
            break label590;
          }
          if ((this.reducePaddingNoTitleNoButtons) && (!isVisible(this.titleBar)) && (m == 0)) {
            break label569;
          }
          this.useFullPadding = true;
          paramInt1 = i - (this.content.getMeasuredHeight() + n);
        }
      }
    }
    for (;;)
    {
      setMeasuredDimension(i2, j - paramInt1);
      return;
      paramInt1 -= this.buttonBarHeight;
      paramInt2 = 0 + this.buttonPaddingFull * 2;
      break;
      paramInt2 = 0 + this.buttonPaddingFull * 2;
      break;
      label544:
      i = paramInt1;
      n = paramInt2;
      if (this.noTitleNoPadding) {
        break label411;
      }
      n = paramInt2 + this.noTitlePaddingFull;
      i = paramInt1;
      break label411;
      label569:
      this.useFullPadding = false;
      paramInt1 = i - (this.content.getMeasuredHeight() + k);
      continue;
      label590:
      this.useFullPadding = false;
      paramInt1 = 0;
    }
  }
  
  public void setButtonGravity(GravityEnum paramGravityEnum)
  {
    this.buttonGravity = paramGravityEnum;
    invertGravityIfNecessary();
  }
  
  public void setButtonStackedGravity(GravityEnum paramGravityEnum)
  {
    for (MDButton localMDButton : this.buttons) {
      if (localMDButton != null) {
        localMDButton.setStackedGravity(paramGravityEnum);
      }
    }
  }
  
  public void setDividerColor(int paramInt)
  {
    this.dividerPaint.setColor(paramInt);
    invalidate();
  }
  
  public void setMaxHeight(int paramInt)
  {
    this.maxHeight = paramInt;
  }
  
  public void setStackingBehavior(StackingBehavior paramStackingBehavior)
  {
    this.stackBehavior = paramStackingBehavior;
    invalidate();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\internal\MDRootLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */