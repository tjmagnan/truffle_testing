package io.fabric.sdk.android.services.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class AdvertisingInfoServiceStrategy
  implements AdvertisingInfoStrategy
{
  public static final String GOOGLE_PLAY_SERVICES_INTENT = "com.google.android.gms.ads.identifier.service.START";
  public static final String GOOGLE_PLAY_SERVICES_INTENT_PACKAGE_NAME = "com.google.android.gms";
  private static final String GOOGLE_PLAY_SERVICE_PACKAGE_NAME = "com.android.vending";
  private final Context context;
  
  public AdvertisingInfoServiceStrategy(Context paramContext)
  {
    this.context = paramContext.getApplicationContext();
  }
  
  /* Error */
  public AdvertisingInfo getAdvertisingInfo()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: invokestatic 54	android/os/Looper:myLooper	()Landroid/os/Looper;
    //   5: invokestatic 57	android/os/Looper:getMainLooper	()Landroid/os/Looper;
    //   8: if_acmpne +17 -> 25
    //   11: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   14: ldc 65
    //   16: ldc 67
    //   18: invokeinterface 73 3 0
    //   23: aload_2
    //   24: areturn
    //   25: aload_0
    //   26: getfield 39	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy:context	Landroid/content/Context;
    //   29: invokevirtual 77	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   32: ldc 24
    //   34: iconst_0
    //   35: invokevirtual 83	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   38: pop
    //   39: new 10	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingConnection
    //   42: dup
    //   43: aconst_null
    //   44: invokespecial 86	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingConnection:<init>	(Lio/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$1;)V
    //   47: astore 4
    //   49: new 88	android/content/Intent
    //   52: dup
    //   53: ldc 18
    //   55: invokespecial 91	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   58: astore_3
    //   59: aload_3
    //   60: ldc 21
    //   62: invokevirtual 95	android/content/Intent:setPackage	(Ljava/lang/String;)Landroid/content/Intent;
    //   65: pop
    //   66: aload_0
    //   67: getfield 39	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy:context	Landroid/content/Context;
    //   70: aload_3
    //   71: aload 4
    //   73: iconst_1
    //   74: invokevirtual 99	android/content/Context:bindService	(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    //   77: istore_1
    //   78: iload_1
    //   79: ifeq +138 -> 217
    //   82: new 13	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface
    //   85: astore 5
    //   87: aload 5
    //   89: aload 4
    //   91: invokevirtual 103	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingConnection:getBinder	()Landroid/os/IBinder;
    //   94: invokespecial 106	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface:<init>	(Landroid/os/IBinder;)V
    //   97: new 108	io/fabric/sdk/android/services/common/AdvertisingInfo
    //   100: astore_3
    //   101: aload_3
    //   102: aload 5
    //   104: invokevirtual 112	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface:getId	()Ljava/lang/String;
    //   107: aload 5
    //   109: invokevirtual 116	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy$AdvertisingInterface:isLimitAdTrackingEnabled	()Z
    //   112: invokespecial 119	io/fabric/sdk/android/services/common/AdvertisingInfo:<init>	(Ljava/lang/String;Z)V
    //   115: aload_0
    //   116: getfield 39	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy:context	Landroid/content/Context;
    //   119: aload 4
    //   121: invokevirtual 123	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
    //   124: aload_3
    //   125: astore_2
    //   126: goto -103 -> 23
    //   129: astore_3
    //   130: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   133: ldc 65
    //   135: ldc 125
    //   137: invokeinterface 73 3 0
    //   142: goto -119 -> 23
    //   145: astore_3
    //   146: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   149: ldc 65
    //   151: ldc 127
    //   153: aload_3
    //   154: invokeinterface 130 4 0
    //   159: goto -136 -> 23
    //   162: astore_3
    //   163: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   166: ldc 65
    //   168: ldc -124
    //   170: aload_3
    //   171: invokeinterface 135 4 0
    //   176: aload_0
    //   177: getfield 39	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy:context	Landroid/content/Context;
    //   180: aload 4
    //   182: invokevirtual 123	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
    //   185: goto -162 -> 23
    //   188: astore_3
    //   189: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   192: ldc 65
    //   194: ldc -119
    //   196: aload_3
    //   197: invokeinterface 130 4 0
    //   202: goto -179 -> 23
    //   205: astore_3
    //   206: aload_0
    //   207: getfield 39	io/fabric/sdk/android/services/common/AdvertisingInfoServiceStrategy:context	Landroid/content/Context;
    //   210: aload 4
    //   212: invokevirtual 123	android/content/Context:unbindService	(Landroid/content/ServiceConnection;)V
    //   215: aload_3
    //   216: athrow
    //   217: invokestatic 63	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   220: ldc 65
    //   222: ldc -119
    //   224: invokeinterface 73 3 0
    //   229: goto -206 -> 23
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	232	0	this	AdvertisingInfoServiceStrategy
    //   77	2	1	bool	boolean
    //   1	125	2	localObject1	Object
    //   58	67	3	localObject2	Object
    //   129	1	3	localNameNotFoundException	android.content.pm.PackageManager.NameNotFoundException
    //   145	9	3	localException1	Exception
    //   162	9	3	localException2	Exception
    //   188	9	3	localThrowable	Throwable
    //   205	11	3	localObject3	Object
    //   47	164	4	localAdvertisingConnection	AdvertisingConnection
    //   85	23	5	localAdvertisingInterface	AdvertisingInterface
    // Exception table:
    //   from	to	target	type
    //   25	39	129	android/content/pm/PackageManager$NameNotFoundException
    //   25	39	145	java/lang/Exception
    //   82	115	162	java/lang/Exception
    //   66	78	188	java/lang/Throwable
    //   115	124	188	java/lang/Throwable
    //   176	185	188	java/lang/Throwable
    //   206	217	188	java/lang/Throwable
    //   217	229	188	java/lang/Throwable
    //   82	115	205	finally
    //   163	176	205	finally
  }
  
  private static final class AdvertisingConnection
    implements ServiceConnection
  {
    private static final int QUEUE_TIMEOUT_IN_MS = 200;
    private final LinkedBlockingQueue<IBinder> queue = new LinkedBlockingQueue(1);
    private boolean retrieved = false;
    
    public IBinder getBinder()
    {
      if (this.retrieved) {
        Fabric.getLogger().e("Fabric", "getBinder already called");
      }
      this.retrieved = true;
      try
      {
        IBinder localIBinder = (IBinder)this.queue.poll(200L, TimeUnit.MILLISECONDS);
        return localIBinder;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;)
        {
          Object localObject = null;
        }
      }
    }
    
    public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
    {
      try
      {
        this.queue.put(paramIBinder);
        return;
      }
      catch (InterruptedException paramComponentName)
      {
        for (;;) {}
      }
    }
    
    public void onServiceDisconnected(ComponentName paramComponentName)
    {
      this.queue.clear();
    }
  }
  
  private static final class AdvertisingInterface
    implements IInterface
  {
    public static final String ADVERTISING_ID_SERVICE_INTERFACE_TOKEN = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
    private static final int AD_TRANSACTION_CODE_ID = 1;
    private static final int AD_TRANSACTION_CODE_LIMIT_AD_TRACKING = 2;
    private static final int FLAGS_NONE = 0;
    private final IBinder binder;
    
    public AdvertisingInterface(IBinder paramIBinder)
    {
      this.binder = paramIBinder;
    }
    
    public IBinder asBinder()
    {
      return this.binder;
    }
    
    public String getId()
      throws RemoteException
    {
      localParcel2 = Parcel.obtain();
      localParcel1 = Parcel.obtain();
      Object localObject1 = null;
      try
      {
        localParcel2.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
        this.binder.transact(1, localParcel2, localParcel1, 0);
        localParcel1.readException();
        String str = localParcel1.readString();
        localObject1 = str;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Fabric.getLogger().d("Fabric", "Could not get parcel from Google Play Service to capture AdvertisingId");
          localParcel1.recycle();
          localParcel2.recycle();
        }
      }
      finally
      {
        localParcel1.recycle();
        localParcel2.recycle();
      }
      return (String)localObject1;
    }
    
    public boolean isLimitAdTrackingEnabled()
      throws RemoteException
    {
      localParcel2 = Parcel.obtain();
      localParcel1 = Parcel.obtain();
      for (boolean bool = false;; bool = false)
      {
        try
        {
          localParcel2.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
          localParcel2.writeInt(1);
          this.binder.transact(2, localParcel2, localParcel1, 0);
          localParcel1.readException();
          int i = localParcel1.readInt();
          if (i == 0) {
            continue;
          }
          bool = true;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Fabric.getLogger().d("Fabric", "Could not get parcel from Google Play Service to capture Advertising limitAdTracking");
            localParcel1.recycle();
            localParcel2.recycle();
          }
        }
        finally
        {
          localParcel1.recycle();
          localParcel2.recycle();
        }
        return bool;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\common\AdvertisingInfoServiceStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */