package me.dm7.barcodescanner.core;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import java.util.List;

public class CameraUtils
{
  public static Camera getCameraInstance()
  {
    return getCameraInstance(getDefaultCameraId());
  }
  
  public static Camera getCameraInstance(int paramInt)
  {
    Object localObject = null;
    if (paramInt == -1) {}
    for (;;)
    {
      try
      {
        localCamera = Camera.open();
        localObject = localCamera;
      }
      catch (Exception localException)
      {
        Camera localCamera;
        continue;
      }
      return (Camera)localObject;
      localCamera = Camera.open(paramInt);
      localObject = localCamera;
    }
  }
  
  public static int getDefaultCameraId()
  {
    int k = Camera.getNumberOfCameras();
    Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
    int j = -1;
    int i = 0;
    if (i < k)
    {
      j = i;
      Camera.getCameraInfo(i, localCameraInfo);
      if (localCameraInfo.facing != 0) {}
    }
    for (;;)
    {
      return i;
      i++;
      break;
      i = j;
    }
  }
  
  public static boolean isFlashSupported(Camera paramCamera)
  {
    boolean bool;
    if (paramCamera != null)
    {
      paramCamera = paramCamera.getParameters();
      if (paramCamera.getFlashMode() == null) {
        bool = false;
      }
    }
    for (;;)
    {
      return bool;
      paramCamera = paramCamera.getSupportedFlashModes();
      if ((paramCamera == null) || (paramCamera.isEmpty()) || ((paramCamera.size() == 1) && (((String)paramCamera.get(0)).equals("off"))))
      {
        bool = false;
        continue;
        bool = false;
      }
      else
      {
        bool = true;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\CameraUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */