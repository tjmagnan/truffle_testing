package com.testfairy.n;

import android.os.Build.VERSION;
import android.os.StatFs;
import android.util.Log;
import com.testfairy.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e
  extends b
{
  private List a = Arrays.asList(new String[] { "/sdcard", "/mnt/sdcard", "/", "/mnt/extSdCard/" });
  private List b = null;
  
  public e(com.testfairy.m.b paramb)
  {
    super(paramb);
  }
  
  private long a(String paramString)
  {
    for (;;)
    {
      try
      {
        localStatFs = new android/os/StatFs;
        localStatFs.<init>(paramString);
        if (Build.VERSION.SDK_INT < 18) {
          continue;
        }
        l2 = localStatFs.getBlockSizeLong();
        l1 = localStatFs.getAvailableBlocksLong();
        l1 *= l2;
      }
      catch (Throwable paramString)
      {
        StatFs localStatFs;
        long l2;
        int i;
        long l1 = 0L;
        continue;
      }
      return l1;
      l2 = localStatFs.getBlockSize();
      i = localStatFs.getAvailableBlocks();
      l1 = i;
    }
  }
  
  private String a(long paramLong)
  {
    String str = null;
    long l = paramLong;
    if (paramLong >= 1024L)
    {
      str = " KB";
      paramLong /= 1024L;
      l = paramLong;
      if (paramLong >= 1024L)
      {
        str = " MB";
        l = paramLong / 1024L;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder(Long.toString(l));
    for (int i = localStringBuilder.length() - 3; i > 0; i -= 3) {
      localStringBuilder.insert(i, ',');
    }
    if (str != null) {
      localStringBuilder.append(str);
    }
    return localStringBuilder.toString();
  }
  
  private long b(String paramString)
  {
    for (;;)
    {
      try
      {
        localStatFs = new android/os/StatFs;
        localStatFs.<init>(paramString);
        if (Build.VERSION.SDK_INT < 18) {
          continue;
        }
        l1 = localStatFs.getBlockSizeLong();
        l2 = localStatFs.getBlockCountLong();
        l1 = l2 * l1;
      }
      catch (Throwable paramString)
      {
        StatFs localStatFs;
        long l2;
        int i;
        long l1 = 0L;
        continue;
      }
      return l1;
      l1 = localStatFs.getBlockSize();
      i = localStatFs.getBlockCount();
      l2 = i;
    }
  }
  
  private void c()
  {
    this.b = new ArrayList();
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      try
      {
        long l2 = a(str);
        long l1 = b(str);
        if (l1 > 0L)
        {
          List localList = this.b;
          a locala = new com/testfairy/n/e$a;
          locala.<init>(this, str, l1, l2, null);
          localList.add(locala);
        }
      }
      catch (Throwable localThrowable) {}
    }
  }
  
  public void a()
  {
    if (this.b != null) {}
    for (;;)
    {
      return;
      c();
      Object localObject1 = new JSONArray();
      Object localObject2 = this.b.iterator();
      Object localObject3;
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (a)((Iterator)localObject2).next();
        JSONObject localJSONObject = new JSONObject();
        try
        {
          localJSONObject.put("path", ((a)localObject3).a);
          localJSONObject.put("available", ((a)localObject3).c);
          localJSONObject.put("total", ((a)localObject3).b);
          ((JSONArray)localObject1).put(localJSONObject);
        }
        catch (JSONException localJSONException) {}
      }
      localObject2 = new JSONObject();
      try
      {
        ((JSONObject)localObject2).put("disksList", localObject1);
        localObject3 = com.testfairy.e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.d((String)localObject3, "disksList " + ((JSONArray)localObject1).toString());
        localObject1 = new g(21, (JSONObject)localObject2);
        b().a((g)localObject1);
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
    }
  }
  
  private class a
  {
    String a;
    long b;
    long c;
    
    private a(String paramString, long paramLong1, long paramLong2)
    {
      this.a = paramString;
      this.b = paramLong1;
      this.c = paramLong2;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */