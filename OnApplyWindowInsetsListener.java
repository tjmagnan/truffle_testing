package android.support.v4.view;

import android.view.View;

public abstract interface OnApplyWindowInsetsListener
{
  public abstract WindowInsetsCompat onApplyWindowInsets(View paramView, WindowInsetsCompat paramWindowInsetsCompat);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\view\OnApplyWindowInsetsListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */