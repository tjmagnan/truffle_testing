package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class InternalPrinterDateTimePrinter
  implements DateTimePrinter, InternalPrinter
{
  private final InternalPrinter underlying;
  
  private InternalPrinterDateTimePrinter(InternalPrinter paramInternalPrinter)
  {
    this.underlying = paramInternalPrinter;
  }
  
  static DateTimePrinter of(InternalPrinter paramInternalPrinter)
  {
    if ((paramInternalPrinter instanceof DateTimePrinterInternalPrinter)) {
      paramInternalPrinter = ((DateTimePrinterInternalPrinter)paramInternalPrinter).getUnderlying();
    }
    for (;;)
    {
      return paramInternalPrinter;
      if ((paramInternalPrinter instanceof DateTimePrinter)) {
        paramInternalPrinter = (DateTimePrinter)paramInternalPrinter;
      } else if (paramInternalPrinter == null) {
        paramInternalPrinter = null;
      } else {
        paramInternalPrinter = new InternalPrinterDateTimePrinter(paramInternalPrinter);
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (paramObject == this) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject instanceof InternalPrinterDateTimePrinter))
      {
        paramObject = (InternalPrinterDateTimePrinter)paramObject;
        bool = this.underlying.equals(((InternalPrinterDateTimePrinter)paramObject).underlying);
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public int estimatePrintedLength()
  {
    return this.underlying.estimatePrintedLength();
  }
  
  public void printTo(Writer paramWriter, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
    throws IOException
  {
    this.underlying.printTo(paramWriter, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
  }
  
  public void printTo(Writer paramWriter, ReadablePartial paramReadablePartial, Locale paramLocale)
    throws IOException
  {
    this.underlying.printTo(paramWriter, paramReadablePartial, paramLocale);
  }
  
  public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
    throws IOException
  {
    this.underlying.printTo(paramAppendable, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
  }
  
  public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
    throws IOException
  {
    this.underlying.printTo(paramAppendable, paramReadablePartial, paramLocale);
  }
  
  public void printTo(StringBuffer paramStringBuffer, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
  {
    try
    {
      this.underlying.printTo(paramStringBuffer, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
  
  public void printTo(StringBuffer paramStringBuffer, ReadablePartial paramReadablePartial, Locale paramLocale)
  {
    try
    {
      this.underlying.printTo(paramStringBuffer, paramReadablePartial, paramLocale);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\InternalPrinterDateTimePrinter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */