package tech.dcube.companion.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PBTopBarFragment_ViewBinding
  implements Unbinder
{
  private PBTopBarFragment target;
  private View view2131624335;
  private View view2131624337;
  
  @UiThread
  public PBTopBarFragment_ViewBinding(final PBTopBarFragment paramPBTopBarFragment, View paramView)
  {
    this.target = paramPBTopBarFragment;
    paramPBTopBarFragment.topBarTitle = ((TextView)Utils.findRequiredViewAsType(paramView, 2131624336, "field 'topBarTitle'", TextView.class));
    View localView = Utils.findRequiredView(paramView, 2131624335, "field 'topBarLeftButton' and method 'onClickTopBarLeftButton'");
    paramPBTopBarFragment.topBarLeftButton = ((TextView)Utils.castView(localView, 2131624335, "field 'topBarLeftButton'", TextView.class));
    this.view2131624335 = localView;
    localView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBTopBarFragment.onClickTopBarLeftButton();
      }
    });
    paramView = Utils.findRequiredView(paramView, 2131624337, "field 'topBarRightButton' and method 'onClickTopBarRightButton'");
    paramPBTopBarFragment.topBarRightButton = ((TextView)Utils.castView(paramView, 2131624337, "field 'topBarRightButton'", TextView.class));
    this.view2131624337 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBTopBarFragment.onClickTopBarRightButton();
      }
    });
  }
  
  @CallSuper
  public void unbind()
  {
    PBTopBarFragment localPBTopBarFragment = this.target;
    if (localPBTopBarFragment == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    localPBTopBarFragment.topBarTitle = null;
    localPBTopBarFragment.topBarLeftButton = null;
    localPBTopBarFragment.topBarRightButton = null;
    this.view2131624335.setOnClickListener(null);
    this.view2131624335 = null;
    this.view2131624337.setOnClickListener(null);
    this.view2131624337 = null;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBTopBarFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */