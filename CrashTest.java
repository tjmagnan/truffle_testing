package com.crashlytics.android.core;

import android.os.AsyncTask;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;

public class CrashTest
{
  private void privateMethodThatThrowsException(String paramString)
  {
    throw new RuntimeException(paramString);
  }
  
  public void crashAsyncTask(final long paramLong)
  {
    new AsyncTask()
    {
      protected Void doInBackground(Void... paramAnonymousVarArgs)
      {
        try
        {
          Thread.sleep(paramLong);
          CrashTest.this.throwRuntimeException("Background thread crash");
          return null;
        }
        catch (InterruptedException paramAnonymousVarArgs)
        {
          for (;;) {}
        }
      }
    }.execute(new Void[] { (Void)null });
  }
  
  public void indexOutOfBounds()
  {
    int i = new int[2][10];
    Fabric.getLogger().d("CrashlyticsCore", "Out of bounds value: " + i);
  }
  
  public int stackOverflow()
  {
    return stackOverflow() + (int)Math.random();
  }
  
  public void throwFiveChainedExceptions()
  {
    try
    {
      privateMethodThatThrowsException("1");
      return;
    }
    catch (Exception localException3)
    {
      try
      {
        RuntimeException localRuntimeException1 = new java/lang/RuntimeException;
        localRuntimeException1.<init>("2", localException3);
        throw localRuntimeException1;
      }
      catch (Exception localException1)
      {
        try
        {
          RuntimeException localRuntimeException3 = new java/lang/RuntimeException;
          localRuntimeException3.<init>("3", localException1);
          throw localRuntimeException3;
        }
        catch (Exception localException4)
        {
          try
          {
            RuntimeException localRuntimeException2 = new java/lang/RuntimeException;
            localRuntimeException2.<init>("4", localException4);
            throw localRuntimeException2;
          }
          catch (Exception localException2)
          {
            throw new RuntimeException("5", localException2);
          }
        }
      }
    }
  }
  
  public void throwRuntimeException(String paramString)
  {
    throw new RuntimeException(paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\CrashTest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */