package com.testfairy;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.testfairy.h.c;
import com.testfairy.h.f;
import com.testfairy.k.d;
import org.json.JSONObject;

public class j
{
  public static final String a = "testfairy.preferences";
  private static final String b = "testfairy_session.json";
  private final int c;
  private final int d;
  private int e = 0;
  private String f = "";
  private String g;
  private final String h;
  private final String i;
  private final SharedPreferences j;
  
  public j(Context paramContext, JSONObject paramJSONObject, String paramString)
  {
    this.j = paramContext.getApplicationContext().getSharedPreferences("testfairy.preferences", 0);
    this.c = paramJSONObject.optInt("project", -1);
    this.d = paramJSONObject.optInt("build", -1);
    this.h = paramJSONObject.optString("serverEndpoint", "");
    this.i = paramString;
    this.g = i();
    if (paramJSONObject.has("options"))
    {
      this.g = paramJSONObject.getString("options");
      a(this.g);
    }
    this.e = this.j.getInt("testerId", 0);
    if (paramJSONObject.has("credentials"))
    {
      paramContext = paramJSONObject.getJSONObject("credentials");
      this.e = paramContext.getInt("testerId");
      this.f = paramContext.getString("secret");
      a(this.e, this.f);
    }
  }
  
  private String i()
  {
    Object localObject2 = null;
    SharedPreferences localSharedPreferences = j();
    int k = localSharedPreferences.getInt("buildId", -1);
    Object localObject1 = localObject2;
    if (k != -1) {
      if (k == this.d) {
        break label38;
      }
    }
    label38:
    for (localObject1 = localObject2;; localObject1 = localSharedPreferences.getString("options", null)) {
      return (String)localObject1;
    }
  }
  
  private SharedPreferences j()
  {
    return this.j;
  }
  
  public String a()
  {
    return this.g;
  }
  
  public void a(int paramInt)
  {
    SharedPreferences.Editor localEditor = j().edit();
    localEditor.putInt("buildId", paramInt);
    localEditor.commit();
  }
  
  public void a(int paramInt, String paramString)
  {
    SharedPreferences.Editor localEditor = j().edit();
    localEditor.putInt("testerId", paramInt);
    localEditor.putString("secret", paramString);
    localEditor.commit();
  }
  
  public void a(String paramString)
  {
    SharedPreferences.Editor localEditor = j().edit();
    localEditor.putString("options", paramString);
    localEditor.putInt("buildId", this.d);
    localEditor.commit();
  }
  
  public void a(String paramString1, String paramString2)
  {
    String str = "previousSessionToken";
    if (paramString2 != null) {
      str = "previousSessionToken" + paramString2;
    }
    paramString2 = j().edit();
    paramString2.putString(str, paramString1);
    paramString2.commit();
  }
  
  public int b()
  {
    return this.c;
  }
  
  public boolean b(String paramString)
  {
    boolean bool2 = false;
    String[] arrayOfString = a().split(",");
    boolean bool1 = bool2;
    int m;
    if (arrayOfString != null) {
      m = arrayOfString.length;
    }
    for (int k = 0;; k++)
    {
      bool1 = bool2;
      if (k < m)
      {
        if (arrayOfString[k].equals(paramString)) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  public int c()
  {
    return this.d;
  }
  
  public void c(String paramString)
  {
    d locald = new d(this.i);
    f localf = new f();
    localf.a("sessionToken", paramString);
    localf.a("endTime", String.valueOf(System.currentTimeMillis()));
    locald.l(localf, new c());
  }
  
  public int d()
  {
    return this.e;
  }
  
  public String d(String paramString)
  {
    String str1 = "previousSessionToken";
    if (paramString != null) {
      str1 = "previousSessionToken" + paramString;
    }
    String str2 = j().getString(str1, null);
    paramString = j().edit();
    paramString.remove(str1);
    paramString.commit();
    return str2;
  }
  
  public String e()
  {
    return this.f;
  }
  
  public String f()
  {
    return this.h;
  }
  
  public String g()
  {
    return this.i;
  }
  
  public int h()
  {
    return j().getInt("buildId", -1);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */