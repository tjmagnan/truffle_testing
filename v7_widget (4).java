package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.lang.reflect.Field;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class ListViewCompat
  extends ListView
{
  public static final int INVALID_POSITION = -1;
  public static final int NO_POSITION = -1;
  private static final int[] STATE_SET_NOTHING = { 0 };
  private Field mIsChildViewEnabled;
  protected int mMotionPosition;
  int mSelectionBottomPadding = 0;
  int mSelectionLeftPadding = 0;
  int mSelectionRightPadding = 0;
  int mSelectionTopPadding = 0;
  private GateKeeperDrawable mSelector;
  final Rect mSelectorRect = new Rect();
  
  public ListViewCompat(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ListViewCompat(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ListViewCompat(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    try
    {
      this.mIsChildViewEnabled = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
      this.mIsChildViewEnabled.setAccessible(true);
      return;
    }
    catch (NoSuchFieldException paramContext)
    {
      for (;;)
      {
        paramContext.printStackTrace();
      }
    }
  }
  
  protected void dispatchDraw(Canvas paramCanvas)
  {
    drawSelectorCompat(paramCanvas);
    super.dispatchDraw(paramCanvas);
  }
  
  protected void drawSelectorCompat(Canvas paramCanvas)
  {
    if (!this.mSelectorRect.isEmpty())
    {
      Drawable localDrawable = getSelector();
      if (localDrawable != null)
      {
        localDrawable.setBounds(this.mSelectorRect);
        localDrawable.draw(paramCanvas);
      }
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    setSelectorEnabled(true);
    updateSelectorStateCompat();
  }
  
  public int lookForSelectablePosition(int paramInt, boolean paramBoolean)
  {
    int j = -1;
    ListAdapter localListAdapter = getAdapter();
    int i = j;
    if (localListAdapter != null)
    {
      if (!isInTouchMode()) {
        break label29;
      }
      i = j;
    }
    for (;;)
    {
      return i;
      label29:
      int k = localListAdapter.getCount();
      if (!getAdapter().areAllItemsEnabled())
      {
        if (paramBoolean) {
          for (i = Math.max(0, paramInt);; i++)
          {
            paramInt = i;
            if (i >= k) {
              break;
            }
            paramInt = i;
            if (localListAdapter.isEnabled(i)) {
              break;
            }
          }
        }
        for (i = Math.min(paramInt, k - 1);; i--)
        {
          paramInt = i;
          if (i < 0) {
            break;
          }
          paramInt = i;
          if (localListAdapter.isEnabled(i)) {
            break;
          }
        }
        i = j;
        if (paramInt >= 0)
        {
          i = j;
          if (paramInt < k) {
            i = paramInt;
          }
        }
      }
      else
      {
        i = j;
        if (paramInt >= 0)
        {
          i = j;
          if (paramInt < k) {
            i = paramInt;
          }
        }
      }
    }
  }
  
  public int measureHeightOfChildrenCompat(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    paramInt3 = getListPaddingTop();
    paramInt2 = getListPaddingBottom();
    getListPaddingLeft();
    getListPaddingRight();
    int i = getDividerHeight();
    Object localObject = getDivider();
    ListAdapter localListAdapter = getAdapter();
    if (localListAdapter == null) {
      paramInt1 = paramInt3 + paramInt2;
    }
    for (;;)
    {
      return paramInt1;
      paramInt2 = paramInt3 + paramInt2;
      label63:
      int m;
      int i1;
      int k;
      if ((i > 0) && (localObject != null))
      {
        paramInt3 = 0;
        localObject = null;
        m = 0;
        i1 = localListAdapter.getCount();
        k = 0;
      }
      for (;;)
      {
        if (k >= i1) {
          break label308;
        }
        int n = localListAdapter.getItemViewType(k);
        int j = m;
        if (n != m)
        {
          localObject = null;
          j = n;
        }
        View localView = localListAdapter.getView(k, (View)localObject, this);
        ViewGroup.LayoutParams localLayoutParams = localView.getLayoutParams();
        localObject = localLayoutParams;
        if (localLayoutParams == null)
        {
          localObject = generateDefaultLayoutParams();
          localView.setLayoutParams((ViewGroup.LayoutParams)localObject);
        }
        if (((ViewGroup.LayoutParams)localObject).height > 0) {}
        for (m = View.MeasureSpec.makeMeasureSpec(((ViewGroup.LayoutParams)localObject).height, 1073741824);; m = View.MeasureSpec.makeMeasureSpec(0, 0))
        {
          localView.measure(paramInt1, m);
          localView.forceLayout();
          m = paramInt2;
          if (k > 0) {
            m = paramInt2 + i;
          }
          paramInt2 = m + localView.getMeasuredHeight();
          if (paramInt2 < paramInt4) {
            break label270;
          }
          if ((paramInt5 >= 0) && (k > paramInt5) && (paramInt3 > 0))
          {
            paramInt1 = paramInt3;
            if (paramInt2 != paramInt4) {
              break;
            }
          }
          paramInt1 = paramInt4;
          break;
          i = 0;
          break label63;
        }
        label270:
        m = paramInt3;
        if (paramInt5 >= 0)
        {
          m = paramInt3;
          if (k >= paramInt5) {
            m = paramInt2;
          }
        }
        k++;
        localObject = localView;
        paramInt3 = m;
        m = j;
      }
      label308:
      paramInt1 = paramInt2;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getAction())
    {
    }
    for (;;)
    {
      return super.onTouchEvent(paramMotionEvent);
      this.mMotionPosition = pointToPosition((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
    }
  }
  
  protected void positionSelectorCompat(int paramInt, View paramView)
  {
    Rect localRect = this.mSelectorRect;
    localRect.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    localRect.left -= this.mSelectionLeftPadding;
    localRect.top -= this.mSelectionTopPadding;
    localRect.right += this.mSelectionRightPadding;
    localRect.bottom += this.mSelectionBottomPadding;
    for (;;)
    {
      try
      {
        bool = this.mIsChildViewEnabled.getBoolean(this);
        if (paramView.isEnabled() != bool)
        {
          paramView = this.mIsChildViewEnabled;
          if (bool) {
            continue;
          }
          bool = true;
          paramView.set(this, Boolean.valueOf(bool));
          if (paramInt != -1) {
            refreshDrawableState();
          }
        }
        return;
      }
      catch (IllegalAccessException paramView)
      {
        boolean bool;
        paramView.printStackTrace();
        continue;
      }
      bool = false;
    }
  }
  
  protected void positionSelectorLikeFocusCompat(int paramInt, View paramView)
  {
    boolean bool = true;
    Drawable localDrawable = getSelector();
    int i;
    float f2;
    float f1;
    if ((localDrawable != null) && (paramInt != -1))
    {
      i = 1;
      if (i != 0) {
        localDrawable.setVisible(false, false);
      }
      positionSelectorCompat(paramInt, paramView);
      if (i != 0)
      {
        paramView = this.mSelectorRect;
        f2 = paramView.exactCenterX();
        f1 = paramView.exactCenterY();
        if (getVisibility() != 0) {
          break label93;
        }
      }
    }
    for (;;)
    {
      localDrawable.setVisible(bool, false);
      DrawableCompat.setHotspot(localDrawable, f2, f1);
      return;
      i = 0;
      break;
      label93:
      bool = false;
    }
  }
  
  protected void positionSelectorLikeTouchCompat(int paramInt, View paramView, float paramFloat1, float paramFloat2)
  {
    positionSelectorLikeFocusCompat(paramInt, paramView);
    paramView = getSelector();
    if ((paramView != null) && (paramInt != -1)) {
      DrawableCompat.setHotspot(paramView, paramFloat1, paramFloat2);
    }
  }
  
  public void setSelector(Drawable paramDrawable)
  {
    if (paramDrawable != null) {}
    for (Object localObject = new GateKeeperDrawable(paramDrawable);; localObject = null)
    {
      this.mSelector = ((GateKeeperDrawable)localObject);
      super.setSelector(this.mSelector);
      localObject = new Rect();
      if (paramDrawable != null) {
        paramDrawable.getPadding((Rect)localObject);
      }
      this.mSelectionLeftPadding = ((Rect)localObject).left;
      this.mSelectionTopPadding = ((Rect)localObject).top;
      this.mSelectionRightPadding = ((Rect)localObject).right;
      this.mSelectionBottomPadding = ((Rect)localObject).bottom;
      return;
    }
  }
  
  protected void setSelectorEnabled(boolean paramBoolean)
  {
    if (this.mSelector != null) {
      this.mSelector.setEnabled(paramBoolean);
    }
  }
  
  protected boolean shouldShowSelectorCompat()
  {
    if ((touchModeDrawsInPressedStateCompat()) && (isPressed())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected boolean touchModeDrawsInPressedStateCompat()
  {
    return false;
  }
  
  protected void updateSelectorStateCompat()
  {
    Drawable localDrawable = getSelector();
    if ((localDrawable != null) && (shouldShowSelectorCompat())) {
      localDrawable.setState(getDrawableState());
    }
  }
  
  private static class GateKeeperDrawable
    extends DrawableWrapper
  {
    private boolean mEnabled = true;
    
    public GateKeeperDrawable(Drawable paramDrawable)
    {
      super();
    }
    
    public void draw(Canvas paramCanvas)
    {
      if (this.mEnabled) {
        super.draw(paramCanvas);
      }
    }
    
    void setEnabled(boolean paramBoolean)
    {
      this.mEnabled = paramBoolean;
    }
    
    public void setHotspot(float paramFloat1, float paramFloat2)
    {
      if (this.mEnabled) {
        super.setHotspot(paramFloat1, paramFloat2);
      }
    }
    
    public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      if (this.mEnabled) {
        super.setHotspotBounds(paramInt1, paramInt2, paramInt3, paramInt4);
      }
    }
    
    public boolean setState(int[] paramArrayOfInt)
    {
      if (this.mEnabled) {}
      for (boolean bool = super.setState(paramArrayOfInt);; bool = false) {
        return bool;
      }
    }
    
    public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
    {
      if (this.mEnabled) {}
      for (paramBoolean1 = super.setVisible(paramBoolean1, paramBoolean2);; paramBoolean1 = false) {
        return paramBoolean1;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\ListViewCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */