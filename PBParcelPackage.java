package tech.dcube.companion.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PBParcelPackage
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -5715377930603140111L;
  @Expose
  @SerializedName("displayId")
  private String displayId;
  @Expose
  @SerializedName("displayName")
  private String displayName;
  @Expose
  @SerializedName("fixedPrice")
  private Boolean fixedPrice;
  @Expose
  @SerializedName("id")
  private String id;
  @Expose
  @SerializedName("image")
  private String image;
  @Expose
  private List<Double[]> packageDimensions = new ArrayList();
  @Expose
  @SerializedName("price")
  private Double price;
  @Expose
  @SerializedName("serviceName")
  private List<String> serviceName = new ArrayList();
  
  public PBParcelPackage() {}
  
  public PBParcelPackage(String paramString1, String paramString2, String paramString3, String paramString4, Double paramDouble, Boolean paramBoolean, List<Double[]> paramList, List<String> paramList1)
  {
    this.id = paramString1;
    this.displayId = paramString2;
    this.displayName = paramString3;
    this.image = paramString4;
    this.price = paramDouble;
    this.fixedPrice = paramBoolean;
    this.packageDimensions = paramList;
    this.serviceName = paramList1;
  }
  
  public static PBParcelPackage fromJson(String paramString)
  {
    PBParcelPackage localPBParcelPackage = (PBParcelPackage)gson.fromJson(paramString, PBParcelPackage.class);
    if (localPBParcelPackage.packageDimensions == null) {
      localPBParcelPackage.packageDimensions = new ArrayList();
    }
    Object localObject = (Map)gson.fromJson(paramString, Map.class);
    int i;
    if (((Map)localObject).get("dimensions") != null)
    {
      paramString = new Double[3];
      paramString[0] = Double.valueOf(0.0D);
      paramString[1] = Double.valueOf(0.0D);
      paramString[2] = Double.valueOf(0.0D);
      localObject = (List)((Map)localObject).get("dimensions");
      for (i = 0; i < ((List)localObject).size(); i++) {
        paramString[i] = ((Double)((List)localObject).get(i));
      }
      localPBParcelPackage.packageDimensions.add(paramString);
    }
    for (;;)
    {
      return localPBParcelPackage;
      if (((Map)localObject).get("dimensionsMultiple") != null)
      {
        paramString = ((List)((Map)localObject).get("dimensionsMultiple")).iterator();
        while (paramString.hasNext())
        {
          localObject = (List)paramString.next();
          Double[] arrayOfDouble = new Double[3];
          arrayOfDouble[0] = Double.valueOf(0.0D);
          arrayOfDouble[1] = Double.valueOf(0.0D);
          arrayOfDouble[2] = Double.valueOf(0.0D);
          for (i = 0; i < ((List)localObject).size(); i++) {
            arrayOfDouble[i] = ((Double)((List)localObject).get(i));
          }
          localPBParcelPackage.packageDimensions.add(arrayOfDouble);
        }
      }
      else
      {
        localPBParcelPackage.packageDimensions.add(new Double[] { Double.valueOf(0.0D), Double.valueOf(0.0D), Double.valueOf(0.0D) });
      }
    }
  }
  
  public static Map<String, PBParcelPackage> fromJsonArray(String paramString)
  {
    HashMap localHashMap = new HashMap();
    paramString = (Map)gson.fromJson(paramString, Map.class);
    Object localObject1;
    Object localObject2;
    if (paramString.get("ownParcels") != null)
    {
      localObject1 = ((List)paramString.get("ownParcels")).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Map)((Iterator)localObject1).next();
        if (((Map)localObject2).get("items") != null)
        {
          localObject2 = (List)((Map)localObject2).get("items");
          if (localObject2 != null)
          {
            localObject2 = ((List)localObject2).iterator();
            while (((Iterator)localObject2).hasNext())
            {
              Object localObject3 = (Map)((Iterator)localObject2).next();
              localObject3 = fromJson(gson.toJson(localObject3));
              localHashMap.put(((PBParcelPackage)localObject3).displayId, localObject3);
            }
          }
        }
      }
    }
    if (paramString.get("carrierParcels") != null)
    {
      paramString = ((List)paramString.get("carrierParcels")).iterator();
      while (paramString.hasNext())
      {
        localObject1 = (Map)paramString.next();
        if (((Map)localObject1).get("items") != null)
        {
          localObject1 = (List)((Map)localObject1).get("items");
          if (localObject1 != null)
          {
            localObject1 = ((List)localObject1).iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (Map)((Iterator)localObject1).next();
              localObject2 = fromJson(gson.toJson(localObject2));
              localHashMap.put(((PBParcelPackage)localObject2).displayId, localObject2);
            }
          }
        }
      }
    }
    return localHashMap;
  }
  
  public String getDisplayId()
  {
    return this.displayId;
  }
  
  public String getDisplayName()
  {
    return this.displayName;
  }
  
  public Boolean getFixedPrice()
  {
    return this.fixedPrice;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getImage()
  {
    return this.image;
  }
  
  public List<Double[]> getPackageDimensions()
  {
    return this.packageDimensions;
  }
  
  public Double getPrice()
  {
    return this.price;
  }
  
  public List<String> getServiceName()
  {
    return this.serviceName;
  }
  
  public void setDisplayId(String paramString)
  {
    this.displayId = paramString;
  }
  
  public void setDisplayName(String paramString)
  {
    this.displayName = paramString;
  }
  
  public void setFixedPrice(Boolean paramBoolean)
  {
    this.fixedPrice = paramBoolean;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setImage(String paramString)
  {
    this.image = paramString;
  }
  
  public void setPackageDimensions(List<Double[]> paramList)
  {
    this.packageDimensions = paramList;
  }
  
  public void setPrice(Double paramDouble)
  {
    this.price = paramDouble;
  }
  
  public void setServiceName(List<String> paramList)
  {
    this.serviceName = paramList;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBParcelPackage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */