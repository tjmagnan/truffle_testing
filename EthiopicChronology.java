package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableDateTime;
import org.joda.time.field.SkipDateTimeField;

public final class EthiopicChronology
  extends BasicFixedMonthChronology
{
  public static final int EE = 1;
  private static final DateTimeField ERA_FIELD = new BasicSingleEraDateTimeField("EE");
  private static final EthiopicChronology INSTANCE_UTC = getInstance(DateTimeZone.UTC);
  private static final int MAX_YEAR = 292272984;
  private static final int MIN_YEAR = -292269337;
  private static final ConcurrentHashMap<DateTimeZone, EthiopicChronology[]> cCache = new ConcurrentHashMap();
  private static final long serialVersionUID = -5972804258688333942L;
  
  EthiopicChronology(Chronology paramChronology, Object paramObject, int paramInt)
  {
    super(paramChronology, paramObject, paramInt);
  }
  
  public static EthiopicChronology getInstance()
  {
    return getInstance(DateTimeZone.getDefault(), 4);
  }
  
  public static EthiopicChronology getInstance(DateTimeZone paramDateTimeZone)
  {
    return getInstance(paramDateTimeZone, 4);
  }
  
  public static EthiopicChronology getInstance(DateTimeZone paramDateTimeZone, int paramInt)
  {
    Object localObject3 = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localObject3 = DateTimeZone.getDefault();
    }
    paramDateTimeZone = (EthiopicChronology[])cCache.get(localObject3);
    Object localObject1;
    if (paramDateTimeZone == null)
    {
      paramDateTimeZone = new EthiopicChronology[7];
      localObject1 = (EthiopicChronology[])cCache.putIfAbsent(localObject3, paramDateTimeZone);
      if (localObject1 != null) {
        paramDateTimeZone = (DateTimeZone)localObject1;
      }
    }
    for (;;)
    {
      EthiopicChronology localEthiopicChronology = paramDateTimeZone[(paramInt - 1)];
      localObject1 = localEthiopicChronology;
      if (localEthiopicChronology == null)
      {
        localEthiopicChronology = paramDateTimeZone[(paramInt - 1)];
        localObject1 = localEthiopicChronology;
        if (localEthiopicChronology != null) {}
      }
      try
      {
        if (localObject3 == DateTimeZone.UTC)
        {
          localEthiopicChronology = new org/joda/time/chrono/EthiopicChronology;
          localEthiopicChronology.<init>(null, null, paramInt);
          localObject3 = new org/joda/time/DateTime;
          ((DateTime)localObject3).<init>(1, 1, 1, 0, 0, 0, 0, localEthiopicChronology);
          localObject1 = new org/joda/time/chrono/EthiopicChronology;
          ((EthiopicChronology)localObject1).<init>(LimitChronology.getInstance(localEthiopicChronology, (ReadableDateTime)localObject3, null), null, paramInt);
        }
        for (;;)
        {
          paramDateTimeZone[(paramInt - 1)] = localObject1;
          return (EthiopicChronology)localObject1;
          localObject1 = new EthiopicChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, paramInt), (DateTimeZone)localObject3), null, paramInt);
        }
      }
      finally {}
    }
  }
  
  public static EthiopicChronology getInstanceUTC()
  {
    return INSTANCE_UTC;
  }
  
  private Object readResolve()
  {
    Object localObject = getBase();
    if (localObject == null) {}
    for (localObject = getInstance(DateTimeZone.UTC, getMinimumDaysInFirstWeek());; localObject = getInstance(((Chronology)localObject).getZone(), getMinimumDaysInFirstWeek())) {
      return localObject;
    }
  }
  
  protected void assemble(AssembledChronology.Fields paramFields)
  {
    if (getBase() == null)
    {
      super.assemble(paramFields);
      paramFields.year = new SkipDateTimeField(this, paramFields.year);
      paramFields.weekyear = new SkipDateTimeField(this, paramFields.weekyear);
      paramFields.era = ERA_FIELD;
      paramFields.monthOfYear = new BasicMonthOfYearDateTimeField(this, 13);
      paramFields.months = paramFields.monthOfYear.getDurationField();
    }
  }
  
  long calculateFirstDayOfYearMillis(int paramInt)
  {
    int k = paramInt - 1963;
    int i;
    if (k <= 0) {
      i = k + 3 >> 2;
    }
    for (;;)
    {
      long l = k;
      return (i + l * 365L) * 86400000L + 21859200000L;
      int j = k >> 2;
      i = j;
      if (!isLeapYear(paramInt)) {
        i = j + 1;
      }
    }
  }
  
  long getApproxMillisAtEpochDividedByTwo()
  {
    return 30962844000000L;
  }
  
  int getMaxYear()
  {
    return 292272984;
  }
  
  int getMinYear()
  {
    return -292269337;
  }
  
  boolean isLeapDay(long paramLong)
  {
    if ((dayOfMonth().get(paramLong) == 6) && (monthOfYear().isLeap(paramLong))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Chronology withUTC()
  {
    return INSTANCE_UTC;
  }
  
  public Chronology withZone(DateTimeZone paramDateTimeZone)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    if (localDateTimeZone == getZone()) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = getInstance(localDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\EthiopicChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */