package me.dm7.barcodescanner.zbar;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import me.dm7.barcodescanner.core.BarcodeScannerView;
import me.dm7.barcodescanner.core.DisplayUtils;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

public class ZBarScannerView
  extends BarcodeScannerView
{
  private static final String TAG = "ZBarScannerView";
  private List<BarcodeFormat> mFormats;
  private ResultHandler mResultHandler;
  private ImageScanner mScanner;
  
  static
  {
    System.loadLibrary("iconv");
  }
  
  public ZBarScannerView(Context paramContext)
  {
    super(paramContext);
    setupScanner();
  }
  
  public ZBarScannerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setupScanner();
  }
  
  public Collection<BarcodeFormat> getFormats()
  {
    if (this.mFormats == null) {}
    for (List localList = BarcodeFormat.ALL_FORMATS;; localList = this.mFormats) {
      return localList;
    }
  }
  
  public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
  {
    if (this.mResultHandler == null) {}
    for (;;)
    {
      return;
      for (;;)
      {
        Symbol localSymbol;
        try
        {
          Object localObject = paramCamera.getParameters().getPreviewSize();
          int i = ((Camera.Size)localObject).width;
          int j = ((Camera.Size)localObject).height;
          int m = j;
          int k = i;
          localObject = paramArrayOfByte;
          if (DisplayUtils.getScreenOrientation(getContext()) == 1)
          {
            localObject = new byte[paramArrayOfByte.length];
            k = 0;
            if (k < j)
            {
              m = 0;
              if (m < i)
              {
                localObject[(m * j + j - k - 1)] = paramArrayOfByte[(k * i + m)];
                m++;
                continue;
              }
              k++;
              continue;
            }
            k = j;
            m = i;
          }
          paramArrayOfByte = new net/sourceforge/zbar/Image;
          paramArrayOfByte.<init>(k, m, "Y800");
          paramArrayOfByte.setData((byte[])localObject);
          if (this.mScanner.scanImage(paramArrayOfByte) == 0) {
            break label300;
          }
          paramArrayOfByte = this.mScanner.getResults();
          paramCamera = new me/dm7/barcodescanner/zbar/Result;
          paramCamera.<init>();
          localObject = paramArrayOfByte.iterator();
          if (((Iterator)localObject).hasNext())
          {
            localSymbol = (Symbol)((Iterator)localObject).next();
            if (Build.VERSION.SDK_INT < 19) {
              break label291;
            }
            paramArrayOfByte = new java/lang/String;
            paramArrayOfByte.<init>(localSymbol.getDataBytes(), StandardCharsets.UTF_8);
            if (TextUtils.isEmpty(paramArrayOfByte)) {
              continue;
            }
            paramCamera.setContents(paramArrayOfByte);
            paramCamera.setBarcodeFormat(BarcodeFormat.getFormatById(localSymbol.getType()));
          }
          paramArrayOfByte = new android/os/Handler;
          paramArrayOfByte.<init>(Looper.getMainLooper());
          localObject = new me/dm7/barcodescanner/zbar/ZBarScannerView$1;
          ((1)localObject).<init>(this, paramCamera);
          paramArrayOfByte.post((Runnable)localObject);
        }
        catch (RuntimeException paramArrayOfByte)
        {
          Log.e("ZBarScannerView", paramArrayOfByte.toString(), paramArrayOfByte);
        }
        break;
        label291:
        paramArrayOfByte = localSymbol.getData();
      }
      label300:
      paramCamera.setOneShotPreviewCallback(this);
    }
  }
  
  public void resumeCameraPreview(ResultHandler paramResultHandler)
  {
    this.mResultHandler = paramResultHandler;
    super.resumeCameraPreview();
  }
  
  public void setFormats(List<BarcodeFormat> paramList)
  {
    this.mFormats = paramList;
    setupScanner();
  }
  
  public void setResultHandler(ResultHandler paramResultHandler)
  {
    this.mResultHandler = paramResultHandler;
  }
  
  public void setupScanner()
  {
    this.mScanner = new ImageScanner();
    this.mScanner.setConfig(0, 256, 3);
    this.mScanner.setConfig(0, 257, 3);
    this.mScanner.setConfig(0, 0, 0);
    Iterator localIterator = getFormats().iterator();
    while (localIterator.hasNext())
    {
      BarcodeFormat localBarcodeFormat = (BarcodeFormat)localIterator.next();
      this.mScanner.setConfig(localBarcodeFormat.getId(), 0, 1);
    }
  }
  
  public static abstract interface ResultHandler
  {
    public abstract void handleResult(Result paramResult);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\zbar\ZBarScannerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */