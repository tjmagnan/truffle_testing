package tech.dcube.companion.model;

import java.io.Serializable;

public class PBDeviceComponent
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 726607892652668627L;
  private String agentVersion;
  private String deviceVersion;
  private String inkColor;
  private String level;
  private String manufacturerType;
  private String osVersion;
  private String type;
  
  public PBDeviceComponent() {}
  
  public PBDeviceComponent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    this.type = paramString1;
    this.manufacturerType = paramString2;
    this.level = paramString3;
    this.inkColor = paramString4;
    this.agentVersion = paramString5;
    this.deviceVersion = paramString6;
    this.osVersion = paramString7;
  }
  
  public String getAgentVersion()
  {
    return this.agentVersion;
  }
  
  public String getDeviceVersion()
  {
    return this.deviceVersion;
  }
  
  public String getInkColor()
  {
    return this.inkColor;
  }
  
  public String getLevel()
  {
    return this.level;
  }
  
  public String getManufacturerType()
  {
    return this.manufacturerType;
  }
  
  public String getOsVersion()
  {
    return this.osVersion;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public void setAgentVersion(String paramString)
  {
    this.agentVersion = paramString;
  }
  
  public void setDeviceVersion(String paramString)
  {
    this.deviceVersion = paramString;
  }
  
  public void setInkColor(String paramString)
  {
    this.inkColor = paramString;
  }
  
  public void setLevel(String paramString)
  {
    this.level = paramString;
  }
  
  public void setManufacturerType(String paramString)
  {
    this.manufacturerType = paramString;
  }
  
  public void setOsVersion(String paramString)
  {
    this.osVersion = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBDeviceComponent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */