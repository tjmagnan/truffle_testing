package me.dm7.barcodescanner.zbar;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "me.dm7.barcodescanner.zbar";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 193;
  public static final String VERSION_NAME = "1.9.3";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\zbar\BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */