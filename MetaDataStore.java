package com.crashlytics.android.core;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class MetaDataStore
{
  private static final String KEYDATA_SUFFIX = "keys";
  private static final String KEY_USER_EMAIL = "userEmail";
  private static final String KEY_USER_ID = "userId";
  private static final String KEY_USER_NAME = "userName";
  private static final String METADATA_EXT = ".meta";
  private static final String USERDATA_SUFFIX = "user";
  private static final Charset UTF_8 = Charset.forName("UTF-8");
  private final File filesDir;
  
  public MetaDataStore(File paramFile)
  {
    this.filesDir = paramFile;
  }
  
  private File getKeysFileForSession(String paramString)
  {
    return new File(this.filesDir, paramString + "keys" + ".meta");
  }
  
  private File getUserDataFileForSession(String paramString)
  {
    return new File(this.filesDir, paramString + "user" + ".meta");
  }
  
  private static Map<String, String> jsonToKeysData(String paramString)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject(paramString);
    HashMap localHashMap = new HashMap();
    Iterator localIterator = localJSONObject.keys();
    while (localIterator.hasNext())
    {
      paramString = (String)localIterator.next();
      localHashMap.put(paramString, valueOrNull(localJSONObject, paramString));
    }
    return localHashMap;
  }
  
  private static UserMetaData jsonToUserData(String paramString)
    throws JSONException
  {
    paramString = new JSONObject(paramString);
    return new UserMetaData(valueOrNull(paramString, "userId"), valueOrNull(paramString, "userName"), valueOrNull(paramString, "userEmail"));
  }
  
  private static String keysDataToJson(Map<String, String> paramMap)
    throws JSONException
  {
    return new JSONObject(paramMap).toString();
  }
  
  private static String userDataToJson(UserMetaData paramUserMetaData)
    throws JSONException
  {
    new JSONObject() {}.toString();
  }
  
  private static String valueOrNull(JSONObject paramJSONObject, String paramString)
  {
    String str = null;
    if (!paramJSONObject.isNull(paramString)) {
      str = paramJSONObject.optString(paramString, null);
    }
    return str;
  }
  
  /* Error */
  public Map<String, String> readKeyData(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 140	com/crashlytics/android/core/MetaDataStore:getKeysFileForSession	(Ljava/lang/String;)Ljava/io/File;
    //   5: astore 5
    //   7: aload 5
    //   9: invokevirtual 143	java/io/File:exists	()Z
    //   12: ifne +9 -> 21
    //   15: invokestatic 149	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   18: astore_1
    //   19: aload_1
    //   20: areturn
    //   21: aconst_null
    //   22: astore_3
    //   23: aconst_null
    //   24: astore 4
    //   26: aload_3
    //   27: astore_1
    //   28: new 151	java/io/FileInputStream
    //   31: astore_2
    //   32: aload_3
    //   33: astore_1
    //   34: aload_2
    //   35: aload 5
    //   37: invokespecial 153	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   40: aload_2
    //   41: invokestatic 159	io/fabric/sdk/android/services/common/CommonUtils:streamToString	(Ljava/io/InputStream;)Ljava/lang/String;
    //   44: invokestatic 161	com/crashlytics/android/core/MetaDataStore:jsonToKeysData	(Ljava/lang/String;)Ljava/util/Map;
    //   47: astore_1
    //   48: aload_2
    //   49: ldc -93
    //   51: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   54: goto -35 -> 19
    //   57: astore_3
    //   58: aload 4
    //   60: astore_2
    //   61: aload_2
    //   62: astore_1
    //   63: invokestatic 173	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   66: ldc -81
    //   68: ldc -79
    //   70: aload_3
    //   71: invokeinterface 183 4 0
    //   76: aload_2
    //   77: ldc -93
    //   79: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   82: invokestatic 149	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   85: astore_1
    //   86: goto -67 -> 19
    //   89: astore_2
    //   90: aload_1
    //   91: astore_3
    //   92: aload_3
    //   93: ldc -93
    //   95: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   98: aload_2
    //   99: athrow
    //   100: astore_1
    //   101: aload_2
    //   102: astore_3
    //   103: aload_1
    //   104: astore_2
    //   105: goto -13 -> 92
    //   108: astore_3
    //   109: goto -48 -> 61
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	this	MetaDataStore
    //   0	112	1	paramString	String
    //   31	46	2	localObject1	Object
    //   89	13	2	localObject2	Object
    //   104	1	2	str	String
    //   22	11	3	localObject3	Object
    //   57	14	3	localException1	Exception
    //   91	12	3	localObject4	Object
    //   108	1	3	localException2	Exception
    //   24	35	4	localObject5	Object
    //   5	31	5	localFile	File
    // Exception table:
    //   from	to	target	type
    //   28	32	57	java/lang/Exception
    //   34	40	57	java/lang/Exception
    //   28	32	89	finally
    //   34	40	89	finally
    //   63	76	89	finally
    //   40	48	100	finally
    //   40	48	108	java/lang/Exception
  }
  
  /* Error */
  public UserMetaData readUserData(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 186	com/crashlytics/android/core/MetaDataStore:getUserDataFileForSession	(Ljava/lang/String;)Ljava/io/File;
    //   5: astore 5
    //   7: aload 5
    //   9: invokevirtual 143	java/io/File:exists	()Z
    //   12: ifne +9 -> 21
    //   15: getstatic 190	com/crashlytics/android/core/UserMetaData:EMPTY	Lcom/crashlytics/android/core/UserMetaData;
    //   18: astore_1
    //   19: aload_1
    //   20: areturn
    //   21: aconst_null
    //   22: astore_3
    //   23: aconst_null
    //   24: astore 4
    //   26: aload_3
    //   27: astore_1
    //   28: new 151	java/io/FileInputStream
    //   31: astore_2
    //   32: aload_3
    //   33: astore_1
    //   34: aload_2
    //   35: aload 5
    //   37: invokespecial 153	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   40: aload_2
    //   41: invokestatic 159	io/fabric/sdk/android/services/common/CommonUtils:streamToString	(Ljava/io/InputStream;)Ljava/lang/String;
    //   44: invokestatic 192	com/crashlytics/android/core/MetaDataStore:jsonToUserData	(Ljava/lang/String;)Lcom/crashlytics/android/core/UserMetaData;
    //   47: astore_1
    //   48: aload_2
    //   49: ldc -93
    //   51: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   54: goto -35 -> 19
    //   57: astore_3
    //   58: aload 4
    //   60: astore_2
    //   61: aload_2
    //   62: astore_1
    //   63: invokestatic 173	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   66: ldc -81
    //   68: ldc -79
    //   70: aload_3
    //   71: invokeinterface 183 4 0
    //   76: aload_2
    //   77: ldc -93
    //   79: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   82: getstatic 190	com/crashlytics/android/core/UserMetaData:EMPTY	Lcom/crashlytics/android/core/UserMetaData;
    //   85: astore_1
    //   86: goto -67 -> 19
    //   89: astore_3
    //   90: aload_1
    //   91: astore_2
    //   92: aload_2
    //   93: ldc -93
    //   95: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   98: aload_3
    //   99: athrow
    //   100: astore_1
    //   101: aload_1
    //   102: astore_3
    //   103: goto -11 -> 92
    //   106: astore_3
    //   107: goto -46 -> 61
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	this	MetaDataStore
    //   0	110	1	paramString	String
    //   31	62	2	localObject1	Object
    //   22	11	3	localObject2	Object
    //   57	14	3	localException1	Exception
    //   89	10	3	localObject3	Object
    //   102	1	3	str	String
    //   106	1	3	localException2	Exception
    //   24	35	4	localObject4	Object
    //   5	31	5	localFile	File
    // Exception table:
    //   from	to	target	type
    //   28	32	57	java/lang/Exception
    //   34	40	57	java/lang/Exception
    //   28	32	89	finally
    //   34	40	89	finally
    //   63	76	89	finally
    //   40	48	100	finally
    //   40	48	106	java/lang/Exception
  }
  
  /* Error */
  public void writeKeyData(String paramString, Map<String, String> paramMap)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 140	com/crashlytics/android/core/MetaDataStore:getKeysFileForSession	(Ljava/lang/String;)Ljava/io/File;
    //   5: astore 5
    //   7: aconst_null
    //   8: astore_3
    //   9: aconst_null
    //   10: astore 4
    //   12: aload_3
    //   13: astore_1
    //   14: aload_2
    //   15: invokestatic 196	com/crashlytics/android/core/MetaDataStore:keysDataToJson	(Ljava/util/Map;)Ljava/lang/String;
    //   18: astore 7
    //   20: aload_3
    //   21: astore_1
    //   22: new 198	java/io/BufferedWriter
    //   25: astore_2
    //   26: aload_3
    //   27: astore_1
    //   28: new 200	java/io/OutputStreamWriter
    //   31: astore 6
    //   33: aload_3
    //   34: astore_1
    //   35: new 202	java/io/FileOutputStream
    //   38: astore 8
    //   40: aload_3
    //   41: astore_1
    //   42: aload 8
    //   44: aload 5
    //   46: invokespecial 203	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   49: aload_3
    //   50: astore_1
    //   51: aload 6
    //   53: aload 8
    //   55: getstatic 41	com/crashlytics/android/core/MetaDataStore:UTF_8	Ljava/nio/charset/Charset;
    //   58: invokespecial 206	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   61: aload_3
    //   62: astore_1
    //   63: aload_2
    //   64: aload 6
    //   66: invokespecial 209	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   69: aload_2
    //   70: aload 7
    //   72: invokevirtual 214	java/io/Writer:write	(Ljava/lang/String;)V
    //   75: aload_2
    //   76: invokevirtual 217	java/io/Writer:flush	()V
    //   79: aload_2
    //   80: ldc -37
    //   82: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   85: return
    //   86: astore_3
    //   87: aload 4
    //   89: astore_2
    //   90: aload_2
    //   91: astore_1
    //   92: invokestatic 173	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   95: ldc -81
    //   97: ldc -35
    //   99: aload_3
    //   100: invokeinterface 183 4 0
    //   105: aload_2
    //   106: ldc -37
    //   108: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   111: goto -26 -> 85
    //   114: astore_3
    //   115: aload_1
    //   116: astore_2
    //   117: aload_2
    //   118: ldc -37
    //   120: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   123: aload_3
    //   124: athrow
    //   125: astore_1
    //   126: aload_1
    //   127: astore_3
    //   128: goto -11 -> 117
    //   131: astore_3
    //   132: goto -42 -> 90
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	135	0	this	MetaDataStore
    //   0	135	1	paramString	String
    //   0	135	2	paramMap	Map<String, String>
    //   8	54	3	localObject1	Object
    //   86	14	3	localException1	Exception
    //   114	10	3	localObject2	Object
    //   127	1	3	str1	String
    //   131	1	3	localException2	Exception
    //   10	78	4	localObject3	Object
    //   5	40	5	localFile	File
    //   31	34	6	localOutputStreamWriter	java.io.OutputStreamWriter
    //   18	53	7	str2	String
    //   38	16	8	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   14	20	86	java/lang/Exception
    //   22	26	86	java/lang/Exception
    //   28	33	86	java/lang/Exception
    //   35	40	86	java/lang/Exception
    //   42	49	86	java/lang/Exception
    //   51	61	86	java/lang/Exception
    //   63	69	86	java/lang/Exception
    //   14	20	114	finally
    //   22	26	114	finally
    //   28	33	114	finally
    //   35	40	114	finally
    //   42	49	114	finally
    //   51	61	114	finally
    //   63	69	114	finally
    //   92	105	114	finally
    //   69	79	125	finally
    //   69	79	131	java/lang/Exception
  }
  
  /* Error */
  public void writeUserData(String paramString, UserMetaData paramUserMetaData)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 186	com/crashlytics/android/core/MetaDataStore:getUserDataFileForSession	(Ljava/lang/String;)Ljava/io/File;
    //   5: astore 5
    //   7: aconst_null
    //   8: astore_3
    //   9: aconst_null
    //   10: astore 4
    //   12: aload_3
    //   13: astore_1
    //   14: aload_2
    //   15: invokestatic 226	com/crashlytics/android/core/MetaDataStore:userDataToJson	(Lcom/crashlytics/android/core/UserMetaData;)Ljava/lang/String;
    //   18: astore 7
    //   20: aload_3
    //   21: astore_1
    //   22: new 198	java/io/BufferedWriter
    //   25: astore_2
    //   26: aload_3
    //   27: astore_1
    //   28: new 200	java/io/OutputStreamWriter
    //   31: astore 8
    //   33: aload_3
    //   34: astore_1
    //   35: new 202	java/io/FileOutputStream
    //   38: astore 6
    //   40: aload_3
    //   41: astore_1
    //   42: aload 6
    //   44: aload 5
    //   46: invokespecial 203	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   49: aload_3
    //   50: astore_1
    //   51: aload 8
    //   53: aload 6
    //   55: getstatic 41	com/crashlytics/android/core/MetaDataStore:UTF_8	Ljava/nio/charset/Charset;
    //   58: invokespecial 206	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   61: aload_3
    //   62: astore_1
    //   63: aload_2
    //   64: aload 8
    //   66: invokespecial 209	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   69: aload_2
    //   70: aload 7
    //   72: invokevirtual 214	java/io/Writer:write	(Ljava/lang/String;)V
    //   75: aload_2
    //   76: invokevirtual 217	java/io/Writer:flush	()V
    //   79: aload_2
    //   80: ldc -93
    //   82: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   85: return
    //   86: astore_3
    //   87: aload 4
    //   89: astore_2
    //   90: aload_2
    //   91: astore_1
    //   92: invokestatic 173	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   95: ldc -81
    //   97: ldc -28
    //   99: aload_3
    //   100: invokeinterface 183 4 0
    //   105: aload_2
    //   106: ldc -93
    //   108: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   111: goto -26 -> 85
    //   114: astore_2
    //   115: aload_1
    //   116: astore_3
    //   117: aload_3
    //   118: ldc -93
    //   120: invokestatic 167	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   123: aload_2
    //   124: athrow
    //   125: astore_1
    //   126: aload_2
    //   127: astore_3
    //   128: aload_1
    //   129: astore_2
    //   130: goto -13 -> 117
    //   133: astore_3
    //   134: goto -44 -> 90
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	137	0	this	MetaDataStore
    //   0	137	1	paramString	String
    //   0	137	2	paramUserMetaData	UserMetaData
    //   8	54	3	localObject1	Object
    //   86	14	3	localException1	Exception
    //   116	12	3	localObject2	Object
    //   133	1	3	localException2	Exception
    //   10	78	4	localObject3	Object
    //   5	40	5	localFile	File
    //   38	16	6	localFileOutputStream	java.io.FileOutputStream
    //   18	53	7	str	String
    //   31	34	8	localOutputStreamWriter	java.io.OutputStreamWriter
    // Exception table:
    //   from	to	target	type
    //   14	20	86	java/lang/Exception
    //   22	26	86	java/lang/Exception
    //   28	33	86	java/lang/Exception
    //   35	40	86	java/lang/Exception
    //   42	49	86	java/lang/Exception
    //   51	61	86	java/lang/Exception
    //   63	69	86	java/lang/Exception
    //   14	20	114	finally
    //   22	26	114	finally
    //   28	33	114	finally
    //   35	40	114	finally
    //   42	49	114	finally
    //   51	61	114	finally
    //   63	69	114	finally
    //   92	105	114	finally
    //   69	79	125	finally
    //   69	79	133	java/lang/Exception
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\MetaDataStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */