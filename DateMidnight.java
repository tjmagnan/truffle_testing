package org.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Locale;
import org.joda.convert.FromString;
import org.joda.time.base.BaseDateTime;
import org.joda.time.field.AbstractReadableInstantFieldProperty;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

@Deprecated
public final class DateMidnight
  extends BaseDateTime
  implements ReadableDateTime, Serializable
{
  private static final long serialVersionUID = 156371964018738L;
  
  public DateMidnight() {}
  
  public DateMidnight(int paramInt1, int paramInt2, int paramInt3)
  {
    super(paramInt1, paramInt2, paramInt3, 0, 0, 0, 0);
  }
  
  public DateMidnight(int paramInt1, int paramInt2, int paramInt3, Chronology paramChronology)
  {
    super(paramInt1, paramInt2, paramInt3, 0, 0, 0, 0, paramChronology);
  }
  
  public DateMidnight(int paramInt1, int paramInt2, int paramInt3, DateTimeZone paramDateTimeZone)
  {
    super(paramInt1, paramInt2, paramInt3, 0, 0, 0, 0, paramDateTimeZone);
  }
  
  public DateMidnight(long paramLong)
  {
    super(paramLong);
  }
  
  public DateMidnight(long paramLong, Chronology paramChronology)
  {
    super(paramLong, paramChronology);
  }
  
  public DateMidnight(long paramLong, DateTimeZone paramDateTimeZone)
  {
    super(paramLong, paramDateTimeZone);
  }
  
  public DateMidnight(Object paramObject)
  {
    super(paramObject, (Chronology)null);
  }
  
  public DateMidnight(Object paramObject, Chronology paramChronology)
  {
    super(paramObject, DateTimeUtils.getChronology(paramChronology));
  }
  
  public DateMidnight(Object paramObject, DateTimeZone paramDateTimeZone)
  {
    super(paramObject, paramDateTimeZone);
  }
  
  public DateMidnight(Chronology paramChronology)
  {
    super(paramChronology);
  }
  
  public DateMidnight(DateTimeZone paramDateTimeZone)
  {
    super(paramDateTimeZone);
  }
  
  public static DateMidnight now()
  {
    return new DateMidnight();
  }
  
  public static DateMidnight now(Chronology paramChronology)
  {
    if (paramChronology == null) {
      throw new NullPointerException("Chronology must not be null");
    }
    return new DateMidnight(paramChronology);
  }
  
  public static DateMidnight now(DateTimeZone paramDateTimeZone)
  {
    if (paramDateTimeZone == null) {
      throw new NullPointerException("Zone must not be null");
    }
    return new DateMidnight(paramDateTimeZone);
  }
  
  @FromString
  public static DateMidnight parse(String paramString)
  {
    return parse(paramString, ISODateTimeFormat.dateTimeParser().withOffsetParsed());
  }
  
  public static DateMidnight parse(String paramString, DateTimeFormatter paramDateTimeFormatter)
  {
    return paramDateTimeFormatter.parseDateTime(paramString).toDateMidnight();
  }
  
  public Property centuryOfEra()
  {
    return new Property(this, getChronology().centuryOfEra());
  }
  
  protected long checkInstant(long paramLong, Chronology paramChronology)
  {
    return paramChronology.dayOfMonth().roundFloor(paramLong);
  }
  
  public Property dayOfMonth()
  {
    return new Property(this, getChronology().dayOfMonth());
  }
  
  public Property dayOfWeek()
  {
    return new Property(this, getChronology().dayOfWeek());
  }
  
  public Property dayOfYear()
  {
    return new Property(this, getChronology().dayOfYear());
  }
  
  public Property era()
  {
    return new Property(this, getChronology().era());
  }
  
  public DateMidnight minus(long paramLong)
  {
    return withDurationAdded(paramLong, -1);
  }
  
  public DateMidnight minus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, -1);
  }
  
  public DateMidnight minus(ReadablePeriod paramReadablePeriod)
  {
    return withPeriodAdded(paramReadablePeriod, -1);
  }
  
  public DateMidnight minusDays(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().days().subtract(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight minusMonths(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().months().subtract(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight minusWeeks(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().weeks().subtract(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight minusYears(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().years().subtract(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public Property monthOfYear()
  {
    return new Property(this, getChronology().monthOfYear());
  }
  
  public DateMidnight plus(long paramLong)
  {
    return withDurationAdded(paramLong, 1);
  }
  
  public DateMidnight plus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, 1);
  }
  
  public DateMidnight plus(ReadablePeriod paramReadablePeriod)
  {
    return withPeriodAdded(paramReadablePeriod, 1);
  }
  
  public DateMidnight plusDays(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().days().add(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight plusMonths(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().months().add(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight plusWeeks(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().weeks().add(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight plusYears(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateMidnight localDateMidnight = this;; localDateMidnight = withMillis(getChronology().years().add(getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public Property property(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("The DateTimeFieldType must not be null");
    }
    DateTimeField localDateTimeField = paramDateTimeFieldType.getField(getChronology());
    if (!localDateTimeField.isSupported()) {
      throw new IllegalArgumentException("Field '" + paramDateTimeFieldType + "' is not supported");
    }
    return new Property(this, localDateTimeField);
  }
  
  public Interval toInterval()
  {
    Chronology localChronology = getChronology();
    long l = getMillis();
    return new Interval(l, DurationFieldType.days().getField(localChronology).add(l, 1), localChronology);
  }
  
  public LocalDate toLocalDate()
  {
    return new LocalDate(getMillis(), getChronology());
  }
  
  @Deprecated
  public YearMonthDay toYearMonthDay()
  {
    return new YearMonthDay(getMillis(), getChronology());
  }
  
  public Property weekOfWeekyear()
  {
    return new Property(this, getChronology().weekOfWeekyear());
  }
  
  public Property weekyear()
  {
    return new Property(this, getChronology().weekyear());
  }
  
  public DateMidnight withCenturyOfEra(int paramInt)
  {
    return withMillis(getChronology().centuryOfEra().set(getMillis(), paramInt));
  }
  
  public DateMidnight withChronology(Chronology paramChronology)
  {
    if (paramChronology == getChronology()) {}
    for (paramChronology = this;; paramChronology = new DateMidnight(getMillis(), paramChronology)) {
      return paramChronology;
    }
  }
  
  public DateMidnight withDayOfMonth(int paramInt)
  {
    return withMillis(getChronology().dayOfMonth().set(getMillis(), paramInt));
  }
  
  public DateMidnight withDayOfWeek(int paramInt)
  {
    return withMillis(getChronology().dayOfWeek().set(getMillis(), paramInt));
  }
  
  public DateMidnight withDayOfYear(int paramInt)
  {
    return withMillis(getChronology().dayOfYear().set(getMillis(), paramInt));
  }
  
  public DateMidnight withDurationAdded(long paramLong, int paramInt)
  {
    DateMidnight localDateMidnight = this;
    if (paramLong != 0L) {
      if (paramInt != 0) {
        break label19;
      }
    }
    label19:
    for (localDateMidnight = this;; localDateMidnight = withMillis(getChronology().add(getMillis(), paramLong, paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight withDurationAdded(ReadableDuration paramReadableDuration, int paramInt)
  {
    DateMidnight localDateMidnight = this;
    if (paramReadableDuration != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localDateMidnight = this;; localDateMidnight = withDurationAdded(paramReadableDuration.getMillis(), paramInt)) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight withEra(int paramInt)
  {
    return withMillis(getChronology().era().set(getMillis(), paramInt));
  }
  
  public DateMidnight withField(DateTimeFieldType paramDateTimeFieldType, int paramInt)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field must not be null");
    }
    return withMillis(paramDateTimeFieldType.getField(getChronology()).set(getMillis(), paramInt));
  }
  
  public DateMidnight withFieldAdded(DurationFieldType paramDurationFieldType, int paramInt)
  {
    if (paramDurationFieldType == null) {
      throw new IllegalArgumentException("Field must not be null");
    }
    if (paramInt == 0) {}
    for (paramDurationFieldType = this;; paramDurationFieldType = withMillis(paramDurationFieldType.getField(getChronology()).add(getMillis(), paramInt))) {
      return paramDurationFieldType;
    }
  }
  
  public DateMidnight withFields(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {}
    for (paramReadablePartial = this;; paramReadablePartial = withMillis(getChronology().set(paramReadablePartial, getMillis()))) {
      return paramReadablePartial;
    }
  }
  
  public DateMidnight withMillis(long paramLong)
  {
    Object localObject = getChronology();
    paramLong = checkInstant(paramLong, (Chronology)localObject);
    if (paramLong == getMillis()) {}
    for (localObject = this;; localObject = new DateMidnight(paramLong, (Chronology)localObject)) {
      return (DateMidnight)localObject;
    }
  }
  
  public DateMidnight withMonthOfYear(int paramInt)
  {
    return withMillis(getChronology().monthOfYear().set(getMillis(), paramInt));
  }
  
  public DateMidnight withPeriodAdded(ReadablePeriod paramReadablePeriod, int paramInt)
  {
    DateMidnight localDateMidnight = this;
    if (paramReadablePeriod != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localDateMidnight = this;; localDateMidnight = withMillis(getChronology().add(paramReadablePeriod, getMillis(), paramInt))) {
      return localDateMidnight;
    }
  }
  
  public DateMidnight withWeekOfWeekyear(int paramInt)
  {
    return withMillis(getChronology().weekOfWeekyear().set(getMillis(), paramInt));
  }
  
  public DateMidnight withWeekyear(int paramInt)
  {
    return withMillis(getChronology().weekyear().set(getMillis(), paramInt));
  }
  
  public DateMidnight withYear(int paramInt)
  {
    return withMillis(getChronology().year().set(getMillis(), paramInt));
  }
  
  public DateMidnight withYearOfCentury(int paramInt)
  {
    return withMillis(getChronology().yearOfCentury().set(getMillis(), paramInt));
  }
  
  public DateMidnight withYearOfEra(int paramInt)
  {
    return withMillis(getChronology().yearOfEra().set(getMillis(), paramInt));
  }
  
  public DateMidnight withZoneRetainFields(DateTimeZone paramDateTimeZone)
  {
    paramDateTimeZone = DateTimeUtils.getZone(paramDateTimeZone);
    DateTimeZone localDateTimeZone = DateTimeUtils.getZone(getZone());
    if (paramDateTimeZone == localDateTimeZone) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = new DateMidnight(localDateTimeZone.getMillisKeepLocal(paramDateTimeZone, getMillis()), getChronology().withZone(paramDateTimeZone))) {
      return paramDateTimeZone;
    }
  }
  
  public Property year()
  {
    return new Property(this, getChronology().year());
  }
  
  public Property yearOfCentury()
  {
    return new Property(this, getChronology().yearOfCentury());
  }
  
  public Property yearOfEra()
  {
    return new Property(this, getChronology().yearOfEra());
  }
  
  public static final class Property
    extends AbstractReadableInstantFieldProperty
  {
    private static final long serialVersionUID = 257629620L;
    private DateTimeField iField;
    private DateMidnight iInstant;
    
    Property(DateMidnight paramDateMidnight, DateTimeField paramDateTimeField)
    {
      this.iInstant = paramDateMidnight;
      this.iField = paramDateTimeField;
    }
    
    private void readObject(ObjectInputStream paramObjectInputStream)
      throws IOException, ClassNotFoundException
    {
      this.iInstant = ((DateMidnight)paramObjectInputStream.readObject());
      this.iField = ((DateTimeFieldType)paramObjectInputStream.readObject()).getField(this.iInstant.getChronology());
    }
    
    private void writeObject(ObjectOutputStream paramObjectOutputStream)
      throws IOException
    {
      paramObjectOutputStream.writeObject(this.iInstant);
      paramObjectOutputStream.writeObject(this.iField.getType());
    }
    
    public DateMidnight addToCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.add(this.iInstant.getMillis(), paramInt));
    }
    
    public DateMidnight addToCopy(long paramLong)
    {
      return this.iInstant.withMillis(this.iField.add(this.iInstant.getMillis(), paramLong));
    }
    
    public DateMidnight addWrapFieldToCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.addWrapField(this.iInstant.getMillis(), paramInt));
    }
    
    protected Chronology getChronology()
    {
      return this.iInstant.getChronology();
    }
    
    public DateMidnight getDateMidnight()
    {
      return this.iInstant;
    }
    
    public DateTimeField getField()
    {
      return this.iField;
    }
    
    protected long getMillis()
    {
      return this.iInstant.getMillis();
    }
    
    public DateMidnight roundCeilingCopy()
    {
      return this.iInstant.withMillis(this.iField.roundCeiling(this.iInstant.getMillis()));
    }
    
    public DateMidnight roundFloorCopy()
    {
      return this.iInstant.withMillis(this.iField.roundFloor(this.iInstant.getMillis()));
    }
    
    public DateMidnight roundHalfCeilingCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfCeiling(this.iInstant.getMillis()));
    }
    
    public DateMidnight roundHalfEvenCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfEven(this.iInstant.getMillis()));
    }
    
    public DateMidnight roundHalfFloorCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfFloor(this.iInstant.getMillis()));
    }
    
    public DateMidnight setCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.set(this.iInstant.getMillis(), paramInt));
    }
    
    public DateMidnight setCopy(String paramString)
    {
      return setCopy(paramString, null);
    }
    
    public DateMidnight setCopy(String paramString, Locale paramLocale)
    {
      return this.iInstant.withMillis(this.iField.set(this.iInstant.getMillis(), paramString, paramLocale));
    }
    
    public DateMidnight withMaximumValue()
    {
      return setCopy(getMaximumValue());
    }
    
    public DateMidnight withMinimumValue()
    {
      return setCopy(getMinimumValue());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\DateMidnight.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */