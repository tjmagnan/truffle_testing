package io.fabric.sdk.android.services.concurrency.internal;

public abstract interface Backoff
{
  public abstract long getDelayMillis(int paramInt);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\concurrency\internal\Backoff.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */