package org.joda.time.tz;

public class ZoneInfoLogger
{
  static ThreadLocal<Boolean> cVerbose = new ThreadLocal()
  {
    protected Boolean initialValue()
    {
      return Boolean.FALSE;
    }
  };
  
  public static void set(boolean paramBoolean)
  {
    cVerbose.set(Boolean.valueOf(paramBoolean));
  }
  
  public static boolean verbose()
  {
    return ((Boolean)cVerbose.get()).booleanValue();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\ZoneInfoLogger.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */