package org.joda.time;

import java.security.BasicPermission;

public class JodaTimePermission
  extends BasicPermission
{
  private static final long serialVersionUID = 1408944367355875472L;
  
  public JodaTimePermission(String paramString)
  {
    super(paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\JodaTimePermission.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */