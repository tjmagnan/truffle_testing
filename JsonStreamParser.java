package com.google.gson;

import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonReader;
import java.io.EOFException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser
  implements Iterator<JsonElement>
{
  private final Object lock;
  private final JsonReader parser;
  
  public JsonStreamParser(Reader paramReader)
  {
    this.parser = new JsonReader(paramReader);
    this.parser.setLenient(true);
    this.lock = new Object();
  }
  
  public JsonStreamParser(String paramString)
  {
    this(new StringReader(paramString));
  }
  
  /* Error */
  public boolean hasNext()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 28	com/google/gson/JsonStreamParser:lock	Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 22	com/google/gson/JsonStreamParser:parser	Lcom/google/gson/stream/JsonReader;
    //   11: invokevirtual 45	com/google/gson/stream/JsonReader:peek	()Lcom/google/gson/stream/JsonToken;
    //   14: astore_3
    //   15: getstatic 51	com/google/gson/stream/JsonToken:END_DOCUMENT	Lcom/google/gson/stream/JsonToken;
    //   18: astore 4
    //   20: aload_3
    //   21: aload 4
    //   23: if_acmpeq +9 -> 32
    //   26: iconst_1
    //   27: istore_1
    //   28: aload_2
    //   29: monitorexit
    //   30: iload_1
    //   31: ireturn
    //   32: iconst_0
    //   33: istore_1
    //   34: goto -6 -> 28
    //   37: astore 4
    //   39: new 53	com/google/gson/JsonSyntaxException
    //   42: astore_3
    //   43: aload_3
    //   44: aload 4
    //   46: invokespecial 56	com/google/gson/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   49: aload_3
    //   50: athrow
    //   51: astore_3
    //   52: aload_2
    //   53: monitorexit
    //   54: aload_3
    //   55: athrow
    //   56: astore_3
    //   57: new 58	com/google/gson/JsonIOException
    //   60: astore 4
    //   62: aload 4
    //   64: aload_3
    //   65: invokespecial 59	com/google/gson/JsonIOException:<init>	(Ljava/lang/Throwable;)V
    //   68: aload 4
    //   70: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	71	0	this	JsonStreamParser
    //   27	7	1	bool	boolean
    //   14	36	3	localObject2	Object
    //   51	4	3	localObject3	Object
    //   56	9	3	localIOException	java.io.IOException
    //   18	4	4	localJsonToken	com.google.gson.stream.JsonToken
    //   37	8	4	localMalformedJsonException	com.google.gson.stream.MalformedJsonException
    //   60	9	4	localJsonIOException	JsonIOException
    // Exception table:
    //   from	to	target	type
    //   7	20	37	com/google/gson/stream/MalformedJsonException
    //   7	20	51	finally
    //   28	30	51	finally
    //   39	51	51	finally
    //   52	54	51	finally
    //   57	71	51	finally
    //   7	20	56	java/io/IOException
  }
  
  public JsonElement next()
    throws JsonParseException
  {
    if (!hasNext()) {
      throw new NoSuchElementException();
    }
    try
    {
      JsonElement localJsonElement = Streams.parse(this.parser);
      return localJsonElement;
    }
    catch (StackOverflowError localStackOverflowError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localStackOverflowError);
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localOutOfMemoryError);
    }
    catch (JsonParseException localJsonParseException)
    {
      Object localObject = localJsonParseException;
      if ((localJsonParseException.getCause() instanceof EOFException)) {
        localObject = new NoSuchElementException();
      }
      throw ((Throwable)localObject);
    }
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\google\gson\JsonStreamParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */