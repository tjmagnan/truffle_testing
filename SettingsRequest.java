package io.fabric.sdk.android.services.settings;

public class SettingsRequest
{
  public final String advertisingId;
  public final String androidId;
  public final String apiKey;
  public final String buildVersion;
  public final String deviceModel;
  public final String displayVersion;
  public final String iconHash;
  public final String installationId;
  public final String instanceId;
  public final String osBuildVersion;
  public final String osDisplayVersion;
  public final int source;
  
  public SettingsRequest(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, int paramInt, String paramString11)
  {
    this.apiKey = paramString1;
    this.deviceModel = paramString2;
    this.osBuildVersion = paramString3;
    this.osDisplayVersion = paramString4;
    this.advertisingId = paramString5;
    this.installationId = paramString6;
    this.androidId = paramString7;
    this.instanceId = paramString8;
    this.displayVersion = paramString9;
    this.buildVersion = paramString10;
    this.source = paramInt;
    this.iconHash = paramString11;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\SettingsRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */