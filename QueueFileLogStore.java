package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.QueueFile;
import io.fabric.sdk.android.services.common.QueueFile.ElementReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

class QueueFileLogStore
  implements FileLogStore
{
  private QueueFile logFile;
  private final int maxLogSize;
  private final File workingFile;
  
  public QueueFileLogStore(File paramFile, int paramInt)
  {
    this.workingFile = paramFile;
    this.maxLogSize = paramInt;
  }
  
  private void doWriteToLog(long paramLong, String paramString)
  {
    if (this.logFile == null) {}
    for (;;)
    {
      return;
      String str = paramString;
      if (paramString == null) {
        str = "null";
      }
      try
      {
        int i = this.maxLogSize / 4;
        paramString = str;
        if (str.length() > i)
        {
          paramString = new java/lang/StringBuilder;
          paramString.<init>();
          paramString = "..." + str.substring(str.length() - i);
        }
        paramString = paramString.replaceAll("\r", " ").replaceAll("\n", " ");
        paramString = String.format(Locale.US, "%d %s%n", new Object[] { Long.valueOf(paramLong), paramString }).getBytes("UTF-8");
        this.logFile.add(paramString);
        while ((!this.logFile.isEmpty()) && (this.logFile.usedBytes() > this.maxLogSize)) {
          this.logFile.remove();
        }
      }
      catch (IOException paramString)
      {
        Fabric.getLogger().e("CrashlyticsCore", "There was a problem writing to the Crashlytics log.", paramString);
      }
    }
  }
  
  private void openLogFile()
  {
    if (this.logFile == null) {}
    try
    {
      QueueFile localQueueFile = new io/fabric/sdk/android/services/common/QueueFile;
      localQueueFile.<init>(this.workingFile);
      this.logFile = localQueueFile;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Could not open log file: " + this.workingFile, localIOException);
      }
    }
  }
  
  public void closeLogFile()
  {
    CommonUtils.closeOrLog(this.logFile, "There was a problem closing the Crashlytics log file.");
    this.logFile = null;
  }
  
  public void deleteLogFile()
  {
    closeLogFile();
    this.workingFile.delete();
  }
  
  public ByteString getLogAsByteString()
  {
    Object localObject = null;
    if (!this.workingFile.exists()) {}
    for (;;)
    {
      return (ByteString)localObject;
      openLogFile();
      if (this.logFile == null) {
        continue;
      }
      int[] arrayOfInt = new int[1];
      arrayOfInt[0] = 0;
      localObject = new byte[this.logFile.usedBytes()];
      try
      {
        QueueFile localQueueFile = this.logFile;
        QueueFile.ElementReader local1 = new com/crashlytics/android/core/QueueFileLogStore$1;
        local1.<init>(this, (byte[])localObject, arrayOfInt);
        localQueueFile.forEach(local1);
        localObject = ByteString.copyFrom((byte[])localObject, 0, arrayOfInt[0]);
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          Fabric.getLogger().e("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", localIOException);
        }
      }
    }
  }
  
  public void writeToLog(long paramLong, String paramString)
  {
    openLogFile();
    doWriteToLog(paramLong, paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\QueueFileLogStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */