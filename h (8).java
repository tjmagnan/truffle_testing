package com.testfairy.h;

import android.content.Context;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.SyncBasicHttpContext;

public class a
{
  private static final String a = "1.4.1";
  private static final int b = 10;
  private static final int c = 10000;
  private static final int d = 0;
  private static final int e = 8192;
  private static final String f = "Accept-Encoding";
  private static final String g = "gzip";
  private static int h = 10;
  private static int i = 10000;
  private final DefaultHttpClient j;
  private final HttpContext k;
  private ThreadPoolExecutor l;
  private final Map m;
  private final Map n;
  
  public a()
  {
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    ConnManagerParams.setTimeout(localBasicHttpParams, i);
    ConnManagerParams.setMaxConnectionsPerRoute(localBasicHttpParams, new ConnPerRouteBean(h));
    ConnManagerParams.setMaxTotalConnections(localBasicHttpParams, 10);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, i);
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, i);
    HttpConnectionParams.setTcpNoDelay(localBasicHttpParams, true);
    HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, 8192);
    HttpProtocolParams.setVersion(localBasicHttpParams, HttpVersion.HTTP_1_1);
    HttpProtocolParams.setUserAgent(localBasicHttpParams, "TestFairy-Agent-20170625-78d2bc1");
    Object localObject = new SchemeRegistry();
    ((SchemeRegistry)localObject).register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    ((SchemeRegistry)localObject).register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    localObject = new ThreadSafeClientConnManager(localBasicHttpParams, (SchemeRegistry)localObject);
    this.k = new SyncBasicHttpContext(new BasicHttpContext());
    this.j = new DefaultHttpClient((ClientConnectionManager)localObject, localBasicHttpParams);
    this.j.addRequestInterceptor(new HttpRequestInterceptor()
    {
      public void process(HttpRequest paramAnonymousHttpRequest, HttpContext paramAnonymousHttpContext)
      {
        if (!paramAnonymousHttpRequest.containsHeader("Accept-Encoding")) {
          paramAnonymousHttpRequest.addHeader("Accept-Encoding", "gzip");
        }
        Iterator localIterator = a.a(a.this).keySet().iterator();
        while (localIterator.hasNext())
        {
          paramAnonymousHttpContext = (String)localIterator.next();
          paramAnonymousHttpRequest.addHeader(paramAnonymousHttpContext, (String)a.a(a.this).get(paramAnonymousHttpContext));
        }
      }
    });
    this.l = ((ThreadPoolExecutor)Executors.newCachedThreadPool());
    this.m = new WeakHashMap();
    this.n = new HashMap();
  }
  
  private String a(String paramString, f paramf)
  {
    String str = paramString;
    if (paramf != null)
    {
      paramf = paramf.c();
      str = paramString + "?" + paramf;
    }
    return str;
  }
  
  private HttpEntity a(f paramf)
  {
    HttpEntity localHttpEntity = null;
    if (paramf != null) {
      localHttpEntity = paramf.a();
    }
    return localHttpEntity;
  }
  
  private HttpEntityEnclosingRequestBase a(HttpEntityEnclosingRequestBase paramHttpEntityEnclosingRequestBase, HttpEntity paramHttpEntity)
  {
    if (paramHttpEntity != null) {
      paramHttpEntityEnclosingRequestBase.setEntity(paramHttpEntity);
    }
    return paramHttpEntityEnclosingRequestBase;
  }
  
  private void a(DefaultHttpClient paramDefaultHttpClient, HttpContext paramHttpContext, HttpUriRequest paramHttpUriRequest, String paramString, c paramc, Context paramContext)
  {
    if (paramString != null) {
      paramHttpUriRequest.addHeader("Content-Type", paramString);
    }
    paramHttpUriRequest = this.l.submit(new b(paramDefaultHttpClient, paramHttpContext, paramHttpUriRequest, paramc));
    if (paramContext != null)
    {
      paramHttpContext = (List)this.m.get(paramContext);
      paramDefaultHttpClient = paramHttpContext;
      if (paramHttpContext == null)
      {
        paramDefaultHttpClient = new LinkedList();
        this.m.put(paramContext, paramDefaultHttpClient);
      }
      paramDefaultHttpClient.add(new WeakReference(paramHttpUriRequest));
    }
  }
  
  public HttpClient a()
  {
    return this.j;
  }
  
  public void a(int paramInt)
  {
    HttpParams localHttpParams = this.j.getParams();
    ConnManagerParams.setTimeout(localHttpParams, paramInt);
    HttpConnectionParams.setSoTimeout(localHttpParams, paramInt);
    HttpConnectionParams.setConnectionTimeout(localHttpParams, paramInt);
  }
  
  public void a(Context paramContext, String paramString, c paramc)
  {
    a(paramContext, paramString, null, paramc);
  }
  
  public void a(Context paramContext, String paramString, f paramf, c paramc)
  {
    a(this.j, this.k, new HttpGet(a(paramString, paramf)), null, paramc, paramContext);
  }
  
  public void a(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, c paramc)
  {
    a(this.j, this.k, a(new HttpPost(paramString1), paramHttpEntity), paramString2, paramc, paramContext);
  }
  
  public void a(Context paramContext, String paramString, Header[] paramArrayOfHeader, c paramc)
  {
    paramString = new HttpDelete(paramString);
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    a(this.j, this.k, paramString, null, paramc, paramContext);
  }
  
  public void a(Context paramContext, String paramString, Header[] paramArrayOfHeader, f paramf, c paramc)
  {
    paramString = new HttpGet(a(paramString, paramf));
    if (paramArrayOfHeader != null) {
      paramString.setHeaders(paramArrayOfHeader);
    }
    a(this.j, this.k, paramString, null, paramc, paramContext);
  }
  
  public void a(Context paramContext, String paramString1, Header[] paramArrayOfHeader, f paramf, String paramString2, c paramc)
  {
    paramString1 = new HttpPost(paramString1);
    if (paramf != null) {
      paramString1.setEntity(a(paramf));
    }
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    a(this.j, this.k, paramString1, paramString2, paramc, paramContext);
  }
  
  public void a(Context paramContext, String paramString1, Header[] paramArrayOfHeader, HttpEntity paramHttpEntity, String paramString2, c paramc)
  {
    paramString1 = a(new HttpPost(paramString1), paramHttpEntity);
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    a(this.j, this.k, paramString1, paramString2, paramc, paramContext);
  }
  
  public void a(Context paramContext, boolean paramBoolean)
  {
    Object localObject = (List)this.m.get(paramContext);
    if (localObject != null)
    {
      localObject = ((List)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        Future localFuture = (Future)((WeakReference)((Iterator)localObject).next()).get();
        if (localFuture != null) {
          localFuture.cancel(paramBoolean);
        }
      }
    }
    this.m.remove(paramContext);
  }
  
  public void a(String paramString)
  {
    HttpProtocolParams.setUserAgent(this.j.getParams(), paramString);
  }
  
  public void a(String paramString, c paramc)
  {
    a(null, paramString, null, paramc);
  }
  
  public void a(String paramString, f paramf, c paramc)
  {
    a(null, paramString, paramf, paramc);
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.n.put(paramString1, paramString2);
  }
  
  public void a(String paramString1, String paramString2, AuthScope paramAuthScope)
  {
    paramString1 = new UsernamePasswordCredentials(paramString1, paramString2);
    this.j.getCredentialsProvider().setCredentials(paramAuthScope, paramString1);
  }
  
  public void a(ThreadPoolExecutor paramThreadPoolExecutor)
  {
    this.l = paramThreadPoolExecutor;
  }
  
  public void a(SSLSocketFactory paramSSLSocketFactory)
  {
    this.j.getConnectionManager().getSchemeRegistry().register(new Scheme("https", paramSSLSocketFactory, 443));
  }
  
  public HttpContext b()
  {
    return this.k;
  }
  
  public void b(Context paramContext, String paramString, c paramc)
  {
    paramString = new HttpDelete(paramString);
    a(this.j, this.k, paramString, null, paramc, paramContext);
  }
  
  public void b(Context paramContext, String paramString, f paramf, c paramc)
  {
    a(paramContext, paramString, a(paramf), null, paramc);
  }
  
  public void b(Context paramContext, String paramString1, HttpEntity paramHttpEntity, String paramString2, c paramc)
  {
    a(this.j, this.k, a(new HttpPut(paramString1), paramHttpEntity), paramString2, paramc, paramContext);
  }
  
  public void b(Context paramContext, String paramString1, Header[] paramArrayOfHeader, HttpEntity paramHttpEntity, String paramString2, c paramc)
  {
    paramString1 = a(new HttpPut(paramString1), paramHttpEntity);
    if (paramArrayOfHeader != null) {
      paramString1.setHeaders(paramArrayOfHeader);
    }
    a(this.j, this.k, paramString1, paramString2, paramc, paramContext);
  }
  
  public void b(String paramString, c paramc)
  {
    b(null, paramString, null, paramc);
  }
  
  public void b(String paramString, f paramf, c paramc)
  {
    b(null, paramString, paramf, paramc);
  }
  
  public void b(String paramString1, String paramString2)
  {
    a(paramString1, paramString2, AuthScope.ANY);
  }
  
  public void c(Context paramContext, String paramString, f paramf, c paramc)
  {
    b(paramContext, paramString, a(paramf), null, paramc);
  }
  
  public void c(String paramString, c paramc)
  {
    c(null, paramString, null, paramc);
  }
  
  public void c(String paramString, f paramf, c paramc)
  {
    c(null, paramString, paramf, paramc);
  }
  
  public void d(String paramString, c paramc)
  {
    b(null, paramString, paramc);
  }
  
  private static class a
    extends HttpEntityWrapper
  {
    public a(HttpEntity paramHttpEntity)
    {
      super();
    }
    
    public InputStream getContent()
    {
      return new GZIPInputStream(this.wrappedEntity.getContent());
    }
    
    public long getContentLength()
    {
      return -1L;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */