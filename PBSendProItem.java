package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.UUID;

public class PBSendProItem
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -3060517777664416685L;
  @Expose
  @SerializedName("downvotes")
  private int downvotes;
  @Expose
  @SerializedName("id")
  private String id;
  @Expose
  @SerializedName("image_url")
  private String imageUrl;
  @Expose
  @SerializedName("inserted_timestamp")
  private String insertedTimestamp;
  @Expose
  @SerializedName("item_height")
  private double itemHeightIn;
  @Expose
  @SerializedName("item_id")
  private String itemId;
  @Expose
  @SerializedName("item_length")
  private double itemLengthIn;
  @Expose
  @SerializedName("item_name")
  private String itemName;
  @Expose
  @SerializedName("item_weight")
  private double itemWeightLb;
  @Expose
  @SerializedName("item_width")
  private double itemWidthIn;
  @Expose
  @SerializedName("upc")
  private String upc;
  @Expose
  @SerializedName("updated_timestamp")
  private String updatedTimestamp;
  @Expose
  @SerializedName("upvotes")
  private int upvotes;
  @Expose
  @SerializedName("user_id")
  private String userId;
  
  public PBSendProItem() {}
  
  public PBSendProItem(String paramString1, String paramString2, Double paramDouble1, Double paramDouble2, Double paramDouble3, Double paramDouble4, Integer paramInteger1, Integer paramInteger2, String paramString3)
  {
    this.itemId = UUID.randomUUID().toString();
    this.itemName = paramString1;
    this.upc = paramString3;
    this.imageUrl = paramString2;
    this.itemLengthIn = paramDouble1.doubleValue();
    this.itemWidthIn = paramDouble2.doubleValue();
    this.itemHeightIn = paramDouble3.doubleValue();
    this.itemWeightLb = paramDouble4.doubleValue();
    this.upvotes = paramInteger1.intValue();
    this.downvotes = paramInteger2.intValue();
  }
  
  public PBSendProItem(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, Double paramDouble1, Double paramDouble2, Double paramDouble3, Double paramDouble4, Integer paramInteger1, Integer paramInteger2, String paramString7, String paramString8)
  {
    this.id = paramString1;
    this.userId = paramString2;
    this.itemId = paramString3;
    this.itemName = paramString4;
    this.upc = paramString5;
    this.imageUrl = paramString6;
    this.itemLengthIn = paramDouble1.doubleValue();
    this.itemWidthIn = paramDouble2.doubleValue();
    this.itemHeightIn = paramDouble3.doubleValue();
    this.itemWeightLb = paramDouble4.doubleValue();
    this.upvotes = paramInteger1.intValue();
    this.downvotes = paramInteger2.intValue();
    this.insertedTimestamp = paramString7;
    this.updatedTimestamp = paramString8;
  }
  
  public PBSendProItem(BarcodeResponseItem paramBarcodeResponseItem)
  {
    this.itemId = paramBarcodeResponseItem.getItemId();
    this.itemName = paramBarcodeResponseItem.getProductName();
    this.upc = paramBarcodeResponseItem.getUpc();
    this.imageUrl = paramBarcodeResponseItem.getImageUrl();
    this.itemLengthIn = paramBarcodeResponseItem.getLength();
    this.itemWidthIn = paramBarcodeResponseItem.getWidth();
    this.itemHeightIn = paramBarcodeResponseItem.getHeight();
    this.itemWeightLb = paramBarcodeResponseItem.getWeight();
    this.upvotes = 0;
    this.downvotes = 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject == null) || (getClass() != paramObject.getClass()))
      {
        bool = false;
      }
      else
      {
        paramObject = (PBSendProItem)paramObject;
        bool = this.itemId.equals(((PBSendProItem)paramObject).itemId);
      }
    }
  }
  
  public int getDownvotes()
  {
    return this.downvotes;
  }
  
  public String getId()
  {
    return this.id;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public String getInsertedTimestamp()
  {
    return this.insertedTimestamp;
  }
  
  public double getItemHeightIn()
  {
    return this.itemHeightIn;
  }
  
  public String getItemId()
  {
    return this.itemId;
  }
  
  public double getItemLengthIn()
  {
    return this.itemLengthIn;
  }
  
  public String getItemName()
  {
    return this.itemName;
  }
  
  public double getItemWeightLb()
  {
    return this.itemWeightLb;
  }
  
  public double getItemWidthIn()
  {
    return this.itemWidthIn;
  }
  
  public String getUpc()
  {
    return this.upc;
  }
  
  public String getUpdatedTimestamp()
  {
    return this.updatedTimestamp;
  }
  
  public int getUpvotes()
  {
    return this.upvotes;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public int hashCode()
  {
    return this.itemId.hashCode();
  }
  
  public void setDownvotes(int paramInt)
  {
    this.downvotes = paramInt;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setInsertedTimestamp(String paramString)
  {
    this.insertedTimestamp = paramString;
  }
  
  public void setItemHeightIn(double paramDouble)
  {
    this.itemHeightIn = paramDouble;
  }
  
  public void setItemId(String paramString)
  {
    this.itemId = paramString;
  }
  
  public void setItemLengthIn(double paramDouble)
  {
    this.itemLengthIn = paramDouble;
  }
  
  public void setItemName(String paramString)
  {
    this.itemName = paramString;
  }
  
  public void setItemWeightLb(double paramDouble)
  {
    this.itemWeightLb = paramDouble;
  }
  
  public void setItemWidthIn(double paramDouble)
  {
    this.itemWidthIn = paramDouble;
  }
  
  public void setUpc(String paramString)
  {
    this.upc = paramString;
  }
  
  public void setUpdatedTimestamp(String paramString)
  {
    this.updatedTimestamp = paramString;
  }
  
  public void setUpvotes(int paramInt)
  {
    this.upvotes = paramInt;
  }
  
  public void setUserId(String paramString)
  {
    this.userId = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBSendProItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */