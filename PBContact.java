package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBContact
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -8918826371845933774L;
  @Expose
  @SerializedName("archived")
  private Boolean archived = Boolean.valueOf(false);
  @Expose
  @SerializedName("companyName")
  private String companyName = "";
  @Expose
  @SerializedName("createDate")
  private String createDate = "";
  @Expose
  @SerializedName("customerPartnerId")
  private String customerPartnerId = "";
  @Expose
  @SerializedName("fullName")
  private String fullName = "";
  @Expose
  @SerializedName("homePhone")
  private String homePhone = "";
  @Expose
  @SerializedName("id")
  private Integer id = Integer.valueOf(0);
  @Expose
  @SerializedName("idpUserId")
  private String idpUserId;
  @Expose
  @SerializedName("modifyDate")
  private String modifyDate = "";
  @Expose
  @SerializedName("primaryEmail")
  private String primaryEmail = "";
  @Expose
  @SerializedName("shared")
  private Boolean shared = Boolean.valueOf(false);
  @Expose
  @SerializedName("simpleName")
  private String simpleName = "";
  @Expose
  @SerializedName("version")
  private Integer version = Integer.valueOf(0);
  
  public PBContact()
  {
    this.archived = Boolean.valueOf(false);
    this.companyName = "";
    this.createDate = "";
    this.customerPartnerId = "";
    this.fullName = "";
    this.homePhone = "";
    this.id = Integer.valueOf(0);
    this.idpUserId = "";
    this.modifyDate = "";
    this.primaryEmail = "";
    this.shared = Boolean.valueOf(false);
    this.simpleName = "";
    this.version = Integer.valueOf(0);
  }
  
  public PBContact(Boolean paramBoolean1, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Integer paramInteger1, String paramString6, String paramString7, String paramString8, Boolean paramBoolean2, String paramString9, Integer paramInteger2)
  {
    this.archived = paramBoolean1;
    this.companyName = paramString1;
    this.createDate = paramString2;
    this.customerPartnerId = paramString3;
    this.fullName = paramString4;
    this.homePhone = paramString5;
    this.id = paramInteger1;
    this.idpUserId = paramString6;
    this.modifyDate = paramString7;
    this.primaryEmail = paramString8;
    this.shared = paramBoolean2;
    this.simpleName = paramString9;
    this.version = paramInteger2;
  }
  
  public Boolean getArchived()
  {
    return this.archived;
  }
  
  public String getCompanyName()
  {
    return this.companyName;
  }
  
  public String getCreateDate()
  {
    return this.createDate;
  }
  
  public String getCustomerPartnerId()
  {
    return this.customerPartnerId;
  }
  
  public String getFullName()
  {
    return this.fullName;
  }
  
  public String getHomePhone()
  {
    return this.homePhone;
  }
  
  public Integer getId()
  {
    return this.id;
  }
  
  public String getIdpUserId()
  {
    return this.idpUserId;
  }
  
  public String getModifyDate()
  {
    return this.modifyDate;
  }
  
  public String getPrimaryEmail()
  {
    return this.primaryEmail;
  }
  
  public Boolean getShared()
  {
    return this.shared;
  }
  
  public String getSimpleName()
  {
    return this.simpleName;
  }
  
  public Integer getVersion()
  {
    return this.version;
  }
  
  public void setArchived(Boolean paramBoolean)
  {
    this.archived = paramBoolean;
  }
  
  public void setCompanyName(String paramString)
  {
    this.companyName = paramString;
  }
  
  public void setCreateDate(String paramString)
  {
    this.createDate = paramString;
  }
  
  public void setCustomerPartnerId(String paramString)
  {
    this.customerPartnerId = paramString;
  }
  
  public void setFullName(String paramString)
  {
    this.fullName = paramString;
  }
  
  public void setHomePhone(String paramString)
  {
    this.homePhone = paramString;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setIdpUserId(String paramString)
  {
    this.idpUserId = paramString;
  }
  
  public void setModifyDate(String paramString)
  {
    this.modifyDate = paramString;
  }
  
  public void setPrimaryEmail(String paramString)
  {
    this.primaryEmail = paramString;
  }
  
  public void setShared(Boolean paramBoolean)
  {
    this.shared = paramBoolean;
  }
  
  public void setSimpleName(String paramString)
  {
    this.simpleName = paramString;
  }
  
  public void setVersion(Integer paramInteger)
  {
    this.version = paramInteger;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBContact.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */