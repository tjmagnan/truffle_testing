package com.testfairy;

import android.util.Log;
import java.util.HashMap;

public class l
{
  static final int a = 64;
  static final int b = 64;
  static final int c = 1024;
  private final k d;
  private HashMap e = new HashMap();
  private String f = null;
  private boolean g = true;
  
  public l(k paramk)
  {
    this.d = paramk;
  }
  
  public String a()
  {
    return this.f;
  }
  
  public String a(String paramString)
  {
    return (String)this.e.get(paramString);
  }
  
  public void a(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public boolean a(String paramString1, String paramString2)
  {
    boolean bool;
    if ((paramString1 == null) || (paramString2 == null))
    {
      Log.w(e.a, "setAttribute will not accepts null key or value");
      bool = false;
    }
    for (;;)
    {
      return bool;
      if ((this.e.size() >= 64) || (paramString1.length() > 64) || (paramString2.length() > 1024))
      {
        bool = false;
      }
      else if ((this.e.containsKey(paramString1)) && (((String)this.e.get(paramString1)).compareTo(paramString2) == 0))
      {
        bool = false;
      }
      else
      {
        this.e.put(paramString1, paramString2);
        this.d.a(paramString1, paramString2);
        bool = true;
      }
    }
  }
  
  public void b(String paramString)
  {
    a(true);
    this.f = paramString;
    this.d.a(paramString);
  }
  
  public boolean b()
  {
    return this.g;
  }
  
  public void c()
  {
    this.d.a(this.e);
  }
  
  public String toString()
  {
    return "UserId: " + a() + "\n" + "attributes: " + this.e.toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */