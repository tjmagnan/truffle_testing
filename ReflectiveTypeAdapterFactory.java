package com.google.gson.internal.bind;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal..Gson.Types;
import com.google.gson.internal.ConstructorConstructor;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.ObjectConstructor;
import com.google.gson.internal.Primitives;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ReflectiveTypeAdapterFactory
  implements TypeAdapterFactory
{
  private final ConstructorConstructor constructorConstructor;
  private final Excluder excluder;
  private final FieldNamingStrategy fieldNamingPolicy;
  private final JsonAdapterAnnotationTypeAdapterFactory jsonAdapterFactory;
  
  public ReflectiveTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor, FieldNamingStrategy paramFieldNamingStrategy, Excluder paramExcluder, JsonAdapterAnnotationTypeAdapterFactory paramJsonAdapterAnnotationTypeAdapterFactory)
  {
    this.constructorConstructor = paramConstructorConstructor;
    this.fieldNamingPolicy = paramFieldNamingStrategy;
    this.excluder = paramExcluder;
    this.jsonAdapterFactory = paramJsonAdapterAnnotationTypeAdapterFactory;
  }
  
  private BoundField createBoundField(final Gson paramGson, final Field paramField, String paramString, final TypeToken<?> paramTypeToken, boolean paramBoolean1, boolean paramBoolean2)
  {
    final boolean bool2 = Primitives.isPrimitive(paramTypeToken.getRawType());
    final Object localObject = (JsonAdapter)paramField.getAnnotation(JsonAdapter.class);
    TypeAdapter localTypeAdapter = null;
    if (localObject != null) {
      localTypeAdapter = this.jsonAdapterFactory.getTypeAdapter(this.constructorConstructor, paramGson, paramTypeToken, (JsonAdapter)localObject);
    }
    if (localTypeAdapter != null) {}
    for (final boolean bool1 = true;; bool1 = false)
    {
      localObject = localTypeAdapter;
      if (localTypeAdapter == null) {
        localObject = paramGson.getAdapter(paramTypeToken);
      }
      new BoundField(paramString, paramBoolean1, paramBoolean2)
      {
        void read(JsonReader paramAnonymousJsonReader, Object paramAnonymousObject)
          throws IOException, IllegalAccessException
        {
          paramAnonymousJsonReader = localObject.read(paramAnonymousJsonReader);
          if ((paramAnonymousJsonReader != null) || (!bool2)) {
            paramField.set(paramAnonymousObject, paramAnonymousJsonReader);
          }
        }
        
        void write(JsonWriter paramAnonymousJsonWriter, Object paramAnonymousObject)
          throws IOException, IllegalAccessException
        {
          Object localObject = paramField.get(paramAnonymousObject);
          if (bool1) {}
          for (paramAnonymousObject = localObject;; paramAnonymousObject = new TypeAdapterRuntimeTypeWrapper(paramGson, localObject, paramTypeToken.getType()))
          {
            ((TypeAdapter)paramAnonymousObject).write(paramAnonymousJsonWriter, localObject);
            return;
          }
        }
        
        public boolean writeField(Object paramAnonymousObject)
          throws IOException, IllegalAccessException
        {
          boolean bool = false;
          if (!this.serialized) {}
          for (;;)
          {
            return bool;
            if (paramField.get(paramAnonymousObject) != paramAnonymousObject) {
              bool = true;
            }
          }
        }
      };
    }
  }
  
  static boolean excludeField(Field paramField, boolean paramBoolean, Excluder paramExcluder)
  {
    if ((!paramExcluder.excludeClass(paramField.getType(), paramBoolean)) && (!paramExcluder.excludeField(paramField, paramBoolean))) {}
    for (paramBoolean = true;; paramBoolean = false) {
      return paramBoolean;
    }
  }
  
  private Map<String, BoundField> getBoundFields(Gson paramGson, TypeToken<?> paramTypeToken, Class<?> paramClass)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    if (paramClass.isInterface()) {}
    for (;;)
    {
      return localLinkedHashMap;
      Type localType1 = paramTypeToken.getType();
      while (paramClass != Object.class)
      {
        Field[] arrayOfField = paramClass.getDeclaredFields();
        int k = arrayOfField.length;
        int i = 0;
        if (i < k)
        {
          Field localField = arrayOfField[i];
          boolean bool1 = excludeField(localField, true);
          boolean bool2 = excludeField(localField, false);
          if ((!bool1) && (!bool2)) {}
          Object localObject1;
          do
          {
            i++;
            break;
            localField.setAccessible(true);
            Type localType2 = .Gson.Types.resolve(paramTypeToken.getType(), paramClass, localField.getGenericType());
            List localList = getFieldNames(localField);
            localObject1 = null;
            int j = 0;
            while (j < localList.size())
            {
              Object localObject2 = (String)localList.get(j);
              if (j != 0) {
                bool1 = false;
              }
              BoundField localBoundField = (BoundField)localLinkedHashMap.put(localObject2, createBoundField(paramGson, localField, (String)localObject2, TypeToken.get(localType2), bool1, bool2));
              localObject2 = localObject1;
              if (localObject1 == null) {
                localObject2 = localBoundField;
              }
              j++;
              localObject1 = localObject2;
            }
          } while (localObject1 == null);
          throw new IllegalArgumentException(localType1 + " declares multiple JSON fields named " + ((BoundField)localObject1).name);
        }
        paramTypeToken = TypeToken.get(.Gson.Types.resolve(paramTypeToken.getType(), paramClass, paramClass.getGenericSuperclass()));
        paramClass = paramTypeToken.getRawType();
      }
    }
  }
  
  private List<String> getFieldNames(Field paramField)
  {
    Object localObject = (SerializedName)paramField.getAnnotation(SerializedName.class);
    if (localObject == null) {}
    String[] arrayOfString;
    for (paramField = Collections.singletonList(this.fieldNamingPolicy.translateName(paramField));; paramField = Collections.singletonList(paramField))
    {
      return paramField;
      paramField = ((SerializedName)localObject).value();
      arrayOfString = ((SerializedName)localObject).alternate();
      if (arrayOfString.length != 0) {
        break;
      }
    }
    localObject = new ArrayList(arrayOfString.length + 1);
    ((List)localObject).add(paramField);
    int j = arrayOfString.length;
    for (int i = 0;; i++)
    {
      paramField = (Field)localObject;
      if (i >= j) {
        break;
      }
      ((List)localObject).add(arrayOfString[i]);
    }
  }
  
  public <T> TypeAdapter<T> create(Gson paramGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.getRawType();
    if (!Object.class.isAssignableFrom(localClass)) {}
    for (paramGson = null;; paramGson = new Adapter(this.constructorConstructor.get(paramTypeToken), getBoundFields(paramGson, paramTypeToken, localClass))) {
      return paramGson;
    }
  }
  
  public boolean excludeField(Field paramField, boolean paramBoolean)
  {
    return excludeField(paramField, paramBoolean, this.excluder);
  }
  
  public static final class Adapter<T>
    extends TypeAdapter<T>
  {
    private final Map<String, ReflectiveTypeAdapterFactory.BoundField> boundFields;
    private final ObjectConstructor<T> constructor;
    
    Adapter(ObjectConstructor<T> paramObjectConstructor, Map<String, ReflectiveTypeAdapterFactory.BoundField> paramMap)
    {
      this.constructor = paramObjectConstructor;
      this.boundFields = paramMap;
    }
    
    public T read(JsonReader paramJsonReader)
      throws IOException
    {
      if (paramJsonReader.peek() == JsonToken.NULL) {
        paramJsonReader.nextNull();
      }
      Object localObject1;
      for (paramJsonReader = null;; paramJsonReader = (JsonReader)localObject1)
      {
        return paramJsonReader;
        localObject1 = this.constructor.construct();
        try
        {
          paramJsonReader.beginObject();
          for (;;)
          {
            if (!paramJsonReader.hasNext()) {
              break label105;
            }
            localObject2 = paramJsonReader.nextName();
            localObject2 = (ReflectiveTypeAdapterFactory.BoundField)this.boundFields.get(localObject2);
            if ((localObject2 != null) && (((ReflectiveTypeAdapterFactory.BoundField)localObject2).deserialized)) {
              break;
            }
            paramJsonReader.skipValue();
          }
        }
        catch (IllegalStateException paramJsonReader)
        {
          for (;;)
          {
            Object localObject2;
            throw new JsonSyntaxException(paramJsonReader);
            ((ReflectiveTypeAdapterFactory.BoundField)localObject2).read(paramJsonReader, localObject1);
          }
        }
        catch (IllegalAccessException paramJsonReader)
        {
          throw new AssertionError(paramJsonReader);
        }
        label105:
        paramJsonReader.endObject();
      }
    }
    
    public void write(JsonWriter paramJsonWriter, T paramT)
      throws IOException
    {
      if (paramT == null) {
        paramJsonWriter.nullValue();
      }
      for (;;)
      {
        return;
        paramJsonWriter.beginObject();
        try
        {
          Iterator localIterator = this.boundFields.values().iterator();
          while (localIterator.hasNext())
          {
            ReflectiveTypeAdapterFactory.BoundField localBoundField = (ReflectiveTypeAdapterFactory.BoundField)localIterator.next();
            if (localBoundField.writeField(paramT))
            {
              paramJsonWriter.name(localBoundField.name);
              localBoundField.write(paramJsonWriter, paramT);
            }
          }
          paramJsonWriter.endObject();
        }
        catch (IllegalAccessException paramJsonWriter)
        {
          throw new AssertionError(paramJsonWriter);
        }
      }
    }
  }
  
  static abstract class BoundField
  {
    final boolean deserialized;
    final String name;
    final boolean serialized;
    
    protected BoundField(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.name = paramString;
      this.serialized = paramBoolean1;
      this.deserialized = paramBoolean2;
    }
    
    abstract void read(JsonReader paramJsonReader, Object paramObject)
      throws IOException, IllegalAccessException;
    
    abstract void write(JsonWriter paramJsonWriter, Object paramObject)
      throws IOException, IllegalAccessException;
    
    abstract boolean writeField(Object paramObject)
      throws IOException, IllegalAccessException;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\google\gson\internal\bind\ReflectiveTypeAdapterFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */