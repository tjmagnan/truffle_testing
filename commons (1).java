package com.afollestad.materialdialogs.commons;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.afollestad.materialdialogs.commons";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 175;
  public static final String VERSION_NAME = "0.9.4.5";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\commons\BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */