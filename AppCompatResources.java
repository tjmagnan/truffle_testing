package android.support.v7.content.res;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import java.util.WeakHashMap;

public final class AppCompatResources
{
  private static final String LOG_TAG = "AppCompatResources";
  private static final ThreadLocal<TypedValue> TL_TYPED_VALUE = new ThreadLocal();
  private static final Object sColorStateCacheLock = new Object();
  private static final WeakHashMap<Context, SparseArray<ColorStateListCacheEntry>> sColorStateCaches = new WeakHashMap(0);
  
  private static void addColorStateListToCache(@NonNull Context paramContext, @ColorRes int paramInt, @NonNull ColorStateList paramColorStateList)
  {
    synchronized (sColorStateCacheLock)
    {
      Object localObject2 = (SparseArray)sColorStateCaches.get(paramContext);
      Object localObject1 = localObject2;
      if (localObject2 == null)
      {
        localObject1 = new android/util/SparseArray;
        ((SparseArray)localObject1).<init>();
        sColorStateCaches.put(paramContext, localObject1);
      }
      localObject2 = new android/support/v7/content/res/AppCompatResources$ColorStateListCacheEntry;
      ((ColorStateListCacheEntry)localObject2).<init>(paramColorStateList, paramContext.getResources().getConfiguration());
      ((SparseArray)localObject1).append(paramInt, localObject2);
      return;
    }
  }
  
  @Nullable
  private static ColorStateList getCachedColorStateList(@NonNull Context paramContext, @ColorRes int paramInt)
  {
    synchronized (sColorStateCacheLock)
    {
      SparseArray localSparseArray = (SparseArray)sColorStateCaches.get(paramContext);
      if ((localSparseArray != null) && (localSparseArray.size() > 0))
      {
        ColorStateListCacheEntry localColorStateListCacheEntry = (ColorStateListCacheEntry)localSparseArray.get(paramInt);
        if (localColorStateListCacheEntry != null)
        {
          if (localColorStateListCacheEntry.configuration.equals(paramContext.getResources().getConfiguration()))
          {
            paramContext = localColorStateListCacheEntry.value;
            return paramContext;
          }
          localSparseArray.remove(paramInt);
        }
      }
      paramContext = null;
    }
  }
  
  public static ColorStateList getColorStateList(@NonNull Context paramContext, @ColorRes int paramInt)
  {
    Object localObject;
    if (Build.VERSION.SDK_INT >= 23) {
      localObject = paramContext.getColorStateList(paramInt);
    }
    for (;;)
    {
      return (ColorStateList)localObject;
      ColorStateList localColorStateList = getCachedColorStateList(paramContext, paramInt);
      localObject = localColorStateList;
      if (localColorStateList == null)
      {
        localObject = inflateColorStateList(paramContext, paramInt);
        if (localObject != null) {
          addColorStateListToCache(paramContext, paramInt, (ColorStateList)localObject);
        } else {
          localObject = ContextCompat.getColorStateList(paramContext, paramInt);
        }
      }
    }
  }
  
  @Nullable
  public static Drawable getDrawable(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    return AppCompatDrawableManager.get().getDrawable(paramContext, paramInt);
  }
  
  @NonNull
  private static TypedValue getTypedValue()
  {
    TypedValue localTypedValue2 = (TypedValue)TL_TYPED_VALUE.get();
    TypedValue localTypedValue1 = localTypedValue2;
    if (localTypedValue2 == null)
    {
      localTypedValue1 = new TypedValue();
      TL_TYPED_VALUE.set(localTypedValue1);
    }
    return localTypedValue1;
  }
  
  @Nullable
  private static ColorStateList inflateColorStateList(Context paramContext, int paramInt)
  {
    Object localObject = null;
    if (isColorInt(paramContext, paramInt)) {
      paramContext = (Context)localObject;
    }
    for (;;)
    {
      return paramContext;
      Resources localResources = paramContext.getResources();
      XmlResourceParser localXmlResourceParser = localResources.getXml(paramInt);
      try
      {
        paramContext = AppCompatColorStateListInflater.createFromXml(localResources, localXmlResourceParser, paramContext.getTheme());
      }
      catch (Exception paramContext)
      {
        Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", paramContext);
        paramContext = (Context)localObject;
      }
    }
  }
  
  private static boolean isColorInt(@NonNull Context paramContext, @ColorRes int paramInt)
  {
    boolean bool = true;
    paramContext = paramContext.getResources();
    TypedValue localTypedValue = getTypedValue();
    paramContext.getValue(paramInt, localTypedValue, true);
    if ((localTypedValue.type >= 28) && (localTypedValue.type <= 31)) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  private static class ColorStateListCacheEntry
  {
    final Configuration configuration;
    final ColorStateList value;
    
    ColorStateListCacheEntry(@NonNull ColorStateList paramColorStateList, @NonNull Configuration paramConfiguration)
    {
      this.value = paramColorStateList;
      this.configuration = paramConfiguration;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\content\res\AppCompatResources.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */