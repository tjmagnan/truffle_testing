package com.afollestad.materialdialogs;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.util.ArrayList;
import java.util.List;

class DefaultRvAdapter
  extends RecyclerView.Adapter<DefaultVH>
{
  private InternalListCallback callback;
  private final MaterialDialog dialog;
  private final GravityEnum itemGravity;
  @LayoutRes
  private final int layout;
  
  DefaultRvAdapter(MaterialDialog paramMaterialDialog, @LayoutRes int paramInt)
  {
    this.dialog = paramMaterialDialog;
    this.layout = paramInt;
    this.itemGravity = paramMaterialDialog.builder.itemsGravity;
  }
  
  @TargetApi(17)
  private boolean isRTL()
  {
    boolean bool1 = true;
    boolean bool2 = false;
    if (Build.VERSION.SDK_INT < 17) {
      bool1 = bool2;
    }
    while (this.dialog.getBuilder().getContext().getResources().getConfiguration().getLayoutDirection() == 1) {
      return bool1;
    }
    for (;;)
    {
      bool1 = false;
    }
  }
  
  @TargetApi(17)
  private void setupGravity(ViewGroup paramViewGroup)
  {
    ((LinearLayout)paramViewGroup).setGravity(this.itemGravity.getGravityInt() | 0x10);
    CompoundButton localCompoundButton;
    TextView localTextView;
    if (paramViewGroup.getChildCount() == 2)
    {
      if ((this.itemGravity != GravityEnum.END) || (isRTL()) || (!(paramViewGroup.getChildAt(0) instanceof CompoundButton))) {
        break label112;
      }
      localCompoundButton = (CompoundButton)paramViewGroup.getChildAt(0);
      paramViewGroup.removeView(localCompoundButton);
      localTextView = (TextView)paramViewGroup.getChildAt(0);
      paramViewGroup.removeView(localTextView);
      localTextView.setPadding(localTextView.getPaddingRight(), localTextView.getPaddingTop(), localTextView.getPaddingLeft(), localTextView.getPaddingBottom());
      paramViewGroup.addView(localTextView);
      paramViewGroup.addView(localCompoundButton);
    }
    for (;;)
    {
      return;
      label112:
      if ((this.itemGravity == GravityEnum.START) && (isRTL()) && ((paramViewGroup.getChildAt(1) instanceof CompoundButton)))
      {
        localCompoundButton = (CompoundButton)paramViewGroup.getChildAt(1);
        paramViewGroup.removeView(localCompoundButton);
        localTextView = (TextView)paramViewGroup.getChildAt(0);
        paramViewGroup.removeView(localTextView);
        localTextView.setPadding(localTextView.getPaddingRight(), localTextView.getPaddingTop(), localTextView.getPaddingRight(), localTextView.getPaddingBottom());
        paramViewGroup.addView(localCompoundButton);
        paramViewGroup.addView(localTextView);
      }
    }
  }
  
  public int getItemCount()
  {
    if (this.dialog.builder.items != null) {}
    for (int i = this.dialog.builder.items.size();; i = 0) {
      return i;
    }
  }
  
  public void onBindViewHolder(DefaultVH paramDefaultVH, int paramInt)
  {
    View localView = paramDefaultVH.itemView;
    boolean bool2 = DialogUtils.isIn(Integer.valueOf(paramInt), this.dialog.builder.disabledIndices);
    int i;
    Object localObject;
    boolean bool1;
    if (bool2)
    {
      i = DialogUtils.adjustAlpha(this.dialog.builder.itemColor, 0.4F);
      localObject = paramDefaultVH.itemView;
      if (bool2) {
        break label268;
      }
      bool1 = true;
      label60:
      ((View)localObject).setEnabled(bool1);
      switch (this.dialog.listType)
      {
      default: 
        paramDefaultVH.title.setText((CharSequence)this.dialog.builder.items.get(paramInt));
        paramDefaultVH.title.setTextColor(i);
        this.dialog.setTypeface(paramDefaultVH.title, this.dialog.builder.regularFont);
        setupGravity((ViewGroup)localView);
        if (this.dialog.builder.itemIds != null)
        {
          if (paramInt < this.dialog.builder.itemIds.length) {
            localView.setId(this.dialog.builder.itemIds[paramInt]);
          }
        }
        else {
          label211:
          if (Build.VERSION.SDK_INT >= 21)
          {
            paramDefaultVH = (ViewGroup)localView;
            if (paramDefaultVH.getChildCount() == 2)
            {
              if (!(paramDefaultVH.getChildAt(0) instanceof CompoundButton)) {
                break label496;
              }
              paramDefaultVH.getChildAt(0).setBackground(null);
            }
          }
        }
        break;
      }
    }
    for (;;)
    {
      return;
      i = this.dialog.builder.itemColor;
      break;
      label268:
      bool1 = false;
      break label60;
      localObject = (RadioButton)paramDefaultVH.control;
      if (this.dialog.builder.selectedIndex == paramInt)
      {
        bool1 = true;
        label300:
        if (this.dialog.builder.choiceWidgetColor == null) {
          break label359;
        }
        MDTintHelper.setTint((RadioButton)localObject, this.dialog.builder.choiceWidgetColor);
        label328:
        ((RadioButton)localObject).setChecked(bool1);
        if (bool2) {
          break label377;
        }
      }
      label359:
      label377:
      for (bool1 = true;; bool1 = false)
      {
        ((RadioButton)localObject).setEnabled(bool1);
        break;
        bool1 = false;
        break label300;
        MDTintHelper.setTint((RadioButton)localObject, this.dialog.builder.widgetColor);
        break label328;
      }
      localObject = (CheckBox)paramDefaultVH.control;
      bool1 = this.dialog.selectedIndicesList.contains(Integer.valueOf(paramInt));
      if (this.dialog.builder.choiceWidgetColor != null)
      {
        MDTintHelper.setTint((CheckBox)localObject, this.dialog.builder.choiceWidgetColor);
        label438:
        ((CheckBox)localObject).setChecked(bool1);
        if (bool2) {
          break label481;
        }
      }
      label481:
      for (bool1 = true;; bool1 = false)
      {
        ((CheckBox)localObject).setEnabled(bool1);
        break;
        MDTintHelper.setTint((CheckBox)localObject, this.dialog.builder.widgetColor);
        break label438;
      }
      localView.setId(-1);
      break label211;
      label496:
      if ((paramDefaultVH.getChildAt(1) instanceof CompoundButton)) {
        paramDefaultVH.getChildAt(1).setBackground(null);
      }
    }
  }
  
  public DefaultVH onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = LayoutInflater.from(paramViewGroup.getContext()).inflate(this.layout, paramViewGroup, false);
    DialogUtils.setBackgroundCompat(paramViewGroup, this.dialog.getListSelector());
    return new DefaultVH(paramViewGroup, this);
  }
  
  void setCallback(InternalListCallback paramInternalListCallback)
  {
    this.callback = paramInternalListCallback;
  }
  
  static class DefaultVH
    extends RecyclerView.ViewHolder
    implements View.OnClickListener, View.OnLongClickListener
  {
    final DefaultRvAdapter adapter;
    final CompoundButton control;
    final TextView title;
    
    DefaultVH(View paramView, DefaultRvAdapter paramDefaultRvAdapter)
    {
      super();
      this.control = ((CompoundButton)paramView.findViewById(R.id.md_control));
      this.title = ((TextView)paramView.findViewById(R.id.md_title));
      this.adapter = paramDefaultRvAdapter;
      paramView.setOnClickListener(this);
      if (paramDefaultRvAdapter.dialog.builder.listLongCallback != null) {
        paramView.setOnLongClickListener(this);
      }
    }
    
    public void onClick(View paramView)
    {
      if ((this.adapter.callback != null) && (getAdapterPosition() != -1))
      {
        Object localObject2 = null;
        Object localObject1 = localObject2;
        if (this.adapter.dialog.builder.items != null)
        {
          localObject1 = localObject2;
          if (getAdapterPosition() < this.adapter.dialog.builder.items.size()) {
            localObject1 = (CharSequence)this.adapter.dialog.builder.items.get(getAdapterPosition());
          }
        }
        this.adapter.callback.onItemSelected(this.adapter.dialog, paramView, getAdapterPosition(), (CharSequence)localObject1, false);
      }
    }
    
    public boolean onLongClick(View paramView)
    {
      Object localObject1;
      if ((this.adapter.callback != null) && (getAdapterPosition() != -1))
      {
        Object localObject2 = null;
        localObject1 = localObject2;
        if (this.adapter.dialog.builder.items != null)
        {
          localObject1 = localObject2;
          if (getAdapterPosition() < this.adapter.dialog.builder.items.size()) {
            localObject1 = (CharSequence)this.adapter.dialog.builder.items.get(getAdapterPosition());
          }
        }
      }
      for (boolean bool = this.adapter.callback.onItemSelected(this.adapter.dialog, paramView, getAdapterPosition(), (CharSequence)localObject1, true);; bool = false) {
        return bool;
      }
    }
  }
  
  static abstract interface InternalListCallback
  {
    public abstract boolean onItemSelected(MaterialDialog paramMaterialDialog, View paramView, int paramInt, CharSequence paramCharSequence, boolean paramBoolean);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\DefaultRvAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */