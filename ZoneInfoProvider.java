package org.joda.time.tz;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.joda.time.DateTimeZone;

public class ZoneInfoProvider
  implements Provider
{
  private final File iFileDir;
  private final ClassLoader iLoader;
  private final String iResourcePath;
  private final Set<String> iZoneInfoKeys;
  private final Map<String, Object> iZoneInfoMap;
  
  public ZoneInfoProvider(File paramFile)
    throws IOException
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("No file directory provided");
    }
    if (!paramFile.exists()) {
      throw new IOException("File directory doesn't exist: " + paramFile);
    }
    if (!paramFile.isDirectory()) {
      throw new IOException("File doesn't refer to a directory: " + paramFile);
    }
    this.iFileDir = paramFile;
    this.iResourcePath = null;
    this.iLoader = null;
    this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
    this.iZoneInfoKeys = Collections.unmodifiableSortedSet(new TreeSet(this.iZoneInfoMap.keySet()));
  }
  
  public ZoneInfoProvider(String paramString)
    throws IOException
  {
    this(paramString, null, false);
  }
  
  public ZoneInfoProvider(String paramString, ClassLoader paramClassLoader)
    throws IOException
  {
    this(paramString, paramClassLoader, true);
  }
  
  private ZoneInfoProvider(String paramString, ClassLoader paramClassLoader, boolean paramBoolean)
    throws IOException
  {
    if (paramString == null) {
      throw new IllegalArgumentException("No resource path provided");
    }
    String str = paramString;
    if (!paramString.endsWith("/")) {
      str = paramString + '/';
    }
    this.iFileDir = null;
    this.iResourcePath = str;
    paramString = paramClassLoader;
    if (paramClassLoader == null)
    {
      paramString = paramClassLoader;
      if (!paramBoolean) {
        paramString = getClass().getClassLoader();
      }
    }
    this.iLoader = paramString;
    this.iZoneInfoMap = loadZoneInfoMap(openResource("ZoneInfoMap"));
    this.iZoneInfoKeys = Collections.unmodifiableSortedSet(new TreeSet(this.iZoneInfoMap.keySet()));
  }
  
  /* Error */
  private DateTimeZone loadZoneData(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial 74	org/joda/time/tz/ZoneInfoProvider:openResource	(Ljava/lang/String;)Ljava/io/InputStream;
    //   5: astore_3
    //   6: aload_3
    //   7: astore_2
    //   8: aload_3
    //   9: aload_1
    //   10: invokestatic 138	org/joda/time/tz/DateTimeZoneBuilder:readFrom	(Ljava/io/InputStream;Ljava/lang/String;)Lorg/joda/time/DateTimeZone;
    //   13: astore 4
    //   15: aload_3
    //   16: astore_2
    //   17: aload_0
    //   18: getfield 80	org/joda/time/tz/ZoneInfoProvider:iZoneInfoMap	Ljava/util/Map;
    //   21: astore 5
    //   23: aload_3
    //   24: astore_2
    //   25: new 140	java/lang/ref/SoftReference
    //   28: astore 6
    //   30: aload_3
    //   31: astore_2
    //   32: aload 6
    //   34: aload 4
    //   36: invokespecial 143	java/lang/ref/SoftReference:<init>	(Ljava/lang/Object;)V
    //   39: aload_3
    //   40: astore_2
    //   41: aload 5
    //   43: aload_1
    //   44: aload 6
    //   46: invokeinterface 147 3 0
    //   51: pop
    //   52: aload 4
    //   54: astore_1
    //   55: aload_3
    //   56: ifnull +10 -> 66
    //   59: aload_3
    //   60: invokevirtual 152	java/io/InputStream:close	()V
    //   63: aload 4
    //   65: astore_1
    //   66: aload_1
    //   67: areturn
    //   68: astore 4
    //   70: aconst_null
    //   71: astore_3
    //   72: aload_3
    //   73: astore_2
    //   74: aload_0
    //   75: aload 4
    //   77: invokevirtual 156	org/joda/time/tz/ZoneInfoProvider:uncaughtException	(Ljava/lang/Exception;)V
    //   80: aload_3
    //   81: astore_2
    //   82: aload_0
    //   83: getfield 80	org/joda/time/tz/ZoneInfoProvider:iZoneInfoMap	Ljava/util/Map;
    //   86: aload_1
    //   87: invokeinterface 160 2 0
    //   92: pop
    //   93: aload_3
    //   94: ifnull +7 -> 101
    //   97: aload_3
    //   98: invokevirtual 152	java/io/InputStream:close	()V
    //   101: aconst_null
    //   102: astore_1
    //   103: goto -37 -> 66
    //   106: astore_1
    //   107: aconst_null
    //   108: astore_2
    //   109: aload_2
    //   110: ifnull +7 -> 117
    //   113: aload_2
    //   114: invokevirtual 152	java/io/InputStream:close	()V
    //   117: aload_1
    //   118: athrow
    //   119: astore_1
    //   120: aload 4
    //   122: astore_1
    //   123: goto -57 -> 66
    //   126: astore_1
    //   127: goto -26 -> 101
    //   130: astore_2
    //   131: goto -14 -> 117
    //   134: astore_1
    //   135: goto -26 -> 109
    //   138: astore 4
    //   140: goto -68 -> 72
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	143	0	this	ZoneInfoProvider
    //   0	143	1	paramString	String
    //   7	107	2	localInputStream1	InputStream
    //   130	1	2	localIOException1	IOException
    //   5	93	3	localInputStream2	InputStream
    //   13	51	4	localDateTimeZone	DateTimeZone
    //   68	53	4	localIOException2	IOException
    //   138	1	4	localIOException3	IOException
    //   21	21	5	localMap	Map
    //   28	17	6	localSoftReference	SoftReference
    // Exception table:
    //   from	to	target	type
    //   0	6	68	java/io/IOException
    //   0	6	106	finally
    //   59	63	119	java/io/IOException
    //   97	101	126	java/io/IOException
    //   113	117	130	java/io/IOException
    //   8	15	134	finally
    //   17	23	134	finally
    //   25	30	134	finally
    //   32	39	134	finally
    //   41	52	134	finally
    //   74	80	134	finally
    //   82	93	134	finally
    //   8	15	138	java/io/IOException
    //   17	23	138	java/io/IOException
    //   25	30	138	java/io/IOException
    //   32	39	138	java/io/IOException
    //   41	52	138	java/io/IOException
  }
  
  /* Error */
  private static Map<String, Object> loadZoneInfoMap(InputStream paramInputStream)
    throws IOException
  {
    // Byte code:
    //   0: new 162	java/util/concurrent/ConcurrentHashMap
    //   3: dup
    //   4: invokespecial 163	java/util/concurrent/ConcurrentHashMap:<init>	()V
    //   7: astore_1
    //   8: new 165	java/io/DataInputStream
    //   11: dup
    //   12: aload_0
    //   13: invokespecial 168	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   16: astore_2
    //   17: aload_2
    //   18: aload_1
    //   19: invokestatic 172	org/joda/time/tz/ZoneInfoProvider:readZoneInfoMap	(Ljava/io/DataInputStream;Ljava/util/Map;)V
    //   22: aload_2
    //   23: invokevirtual 173	java/io/DataInputStream:close	()V
    //   26: aload_1
    //   27: ldc -81
    //   29: new 140	java/lang/ref/SoftReference
    //   32: dup
    //   33: getstatic 180	org/joda/time/DateTimeZone:UTC	Lorg/joda/time/DateTimeZone;
    //   36: invokespecial 143	java/lang/ref/SoftReference:<init>	(Ljava/lang/Object;)V
    //   39: invokeinterface 147 3 0
    //   44: pop
    //   45: aload_1
    //   46: areturn
    //   47: astore_0
    //   48: aload_2
    //   49: invokevirtual 173	java/io/DataInputStream:close	()V
    //   52: aload_0
    //   53: athrow
    //   54: astore_0
    //   55: goto -29 -> 26
    //   58: astore_1
    //   59: goto -7 -> 52
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	62	0	paramInputStream	InputStream
    //   7	39	1	localConcurrentHashMap	java.util.concurrent.ConcurrentHashMap
    //   58	1	1	localIOException	IOException
    //   16	33	2	localDataInputStream	DataInputStream
    // Exception table:
    //   from	to	target	type
    //   17	22	47	finally
    //   22	26	54	java/io/IOException
    //   48	52	58	java/io/IOException
  }
  
  private InputStream openResource(String paramString)
    throws IOException
  {
    if (this.iFileDir != null) {
      paramString = new FileInputStream(new File(this.iFileDir, paramString));
    }
    final String str;
    do
    {
      return paramString;
      str = this.iResourcePath.concat(paramString);
      localObject = (InputStream)AccessController.doPrivileged(new PrivilegedAction()
      {
        public InputStream run()
        {
          if (ZoneInfoProvider.this.iLoader != null) {}
          for (InputStream localInputStream = ZoneInfoProvider.this.iLoader.getResourceAsStream(str);; localInputStream = ClassLoader.getSystemResourceAsStream(str)) {
            return localInputStream;
          }
        }
      });
      paramString = (String)localObject;
    } while (localObject != null);
    Object localObject = new StringBuilder(40).append("Resource not found: \"").append(str).append("\" ClassLoader: ");
    if (this.iLoader != null) {}
    for (paramString = this.iLoader.toString();; paramString = "system") {
      throw new IOException(paramString);
    }
  }
  
  private static void readZoneInfoMap(DataInputStream paramDataInputStream, Map<String, Object> paramMap)
    throws IOException
  {
    int j = 0;
    int k = paramDataInputStream.readUnsignedShort();
    String[] arrayOfString = new String[k];
    for (int i = 0; i < k; i++) {
      arrayOfString[i] = paramDataInputStream.readUTF().intern();
    }
    k = paramDataInputStream.readUnsignedShort();
    i = j;
    while (i < k) {
      try
      {
        paramMap.put(arrayOfString[paramDataInputStream.readUnsignedShort()], arrayOfString[paramDataInputStream.readUnsignedShort()]);
        i++;
      }
      catch (ArrayIndexOutOfBoundsException paramDataInputStream)
      {
        throw new IOException("Corrupt zone info map");
      }
    }
  }
  
  public Set<String> getAvailableIDs()
  {
    return this.iZoneInfoKeys;
  }
  
  public DateTimeZone getZone(String paramString)
  {
    Object localObject;
    if (paramString == null) {
      localObject = null;
    }
    for (;;)
    {
      return (DateTimeZone)localObject;
      localObject = this.iZoneInfoMap.get(paramString);
      if (localObject == null)
      {
        localObject = null;
      }
      else if ((localObject instanceof SoftReference))
      {
        DateTimeZone localDateTimeZone = (DateTimeZone)((SoftReference)localObject).get();
        localObject = localDateTimeZone;
        if (localDateTimeZone == null) {
          localObject = loadZoneData(paramString);
        }
      }
      else if (paramString.equals(localObject))
      {
        localObject = loadZoneData(paramString);
      }
      else
      {
        localObject = getZone((String)localObject);
      }
    }
  }
  
  protected void uncaughtException(Exception paramException)
  {
    paramException.printStackTrace();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\ZoneInfoProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */