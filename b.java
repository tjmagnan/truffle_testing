package com.testfairy.a;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.testfairy.e;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class b
  implements Application.ActivityLifecycleCallbacks
{
  private final c a;
  private String b;
  private String c;
  private WeakReference d;
  private boolean e = false;
  
  public b(Context paramContext, c paramc)
  {
    this.a = paramc;
    if ((paramContext instanceof Activity)) {
      a((Activity)paramContext);
    }
  }
  
  private static Activity b()
  {
    try
    {
      Object localObject3 = Class.forName("android.app.ActivityThread");
      localObject1 = ((Class)localObject3).getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]);
      localObject3 = ((Class)localObject3).getDeclaredField("mActivities");
      ((Field)localObject3).setAccessible(true);
      localObject3 = ((Map)((Field)localObject3).get(localObject1)).values().iterator();
      Class localClass;
      Field localField;
      do
      {
        if (!((Iterator)localObject3).hasNext()) {
          break;
        }
        localObject1 = ((Iterator)localObject3).next();
        localClass = localObject1.getClass();
        localField = localClass.getDeclaredField("paused");
        localField.setAccessible(true);
      } while (localField.getBoolean(localObject1));
      localObject3 = localClass.getDeclaredField("activity");
      ((Field)localObject3).setAccessible(true);
      localObject1 = (Activity)((Field)localObject3).get(localObject1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = null;
      }
    }
    return (Activity)localObject1;
  }
  
  private void b(Activity paramActivity)
  {
    if (this.e) {
      this.a.a(paramActivity);
    }
    this.e = false;
  }
  
  private void c()
  {
    if (!this.e) {
      this.a.a();
    }
    this.e = true;
  }
  
  private void d()
  {
    if ((this.b != null) && (!this.b.equals(this.c)))
    {
      this.c = this.b;
      this.a.a(this.b);
    }
  }
  
  public Activity a()
  {
    Object localObject = this.d;
    if (localObject != null) {}
    for (localObject = (Activity)((WeakReference)localObject).get();; localObject = b()) {
      return (Activity)localObject;
    }
  }
  
  void a(Activity paramActivity)
  {
    this.d = new WeakReference(paramActivity);
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(Activity paramActivity)
  {
    this.d = null;
    this.a.a(paramActivity);
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    this.b = paramActivity.getClass().getName();
    d();
    a(paramActivity);
    b(paramActivity);
    Log.d(e.a, "onActivityResumed " + this.b);
    this.a.b(paramActivity);
  }
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity) {}
  
  public void onActivityStopped(Activity paramActivity)
  {
    if (a() == null) {
      c();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\a\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */