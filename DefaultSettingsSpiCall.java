package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.AbstractSpiCall;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequest.HttpRequestException;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class DefaultSettingsSpiCall
  extends AbstractSpiCall
  implements SettingsSpiCall
{
  static final String BUILD_VERSION_PARAM = "build_version";
  static final String DISPLAY_VERSION_PARAM = "display_version";
  static final String HEADER_ADVERTISING_TOKEN = "X-CRASHLYTICS-ADVERTISING-TOKEN";
  static final String HEADER_ANDROID_ID = "X-CRASHLYTICS-ANDROID-ID";
  static final String HEADER_DEVICE_MODEL = "X-CRASHLYTICS-DEVICE-MODEL";
  static final String HEADER_INSTALLATION_ID = "X-CRASHLYTICS-INSTALLATION-ID";
  static final String HEADER_OS_BUILD_VERSION = "X-CRASHLYTICS-OS-BUILD-VERSION";
  static final String HEADER_OS_DISPLAY_VERSION = "X-CRASHLYTICS-OS-DISPLAY-VERSION";
  static final String ICON_HASH = "icon_hash";
  static final String INSTANCE_PARAM = "instance";
  static final String SOURCE_PARAM = "source";
  
  public DefaultSettingsSpiCall(Kit paramKit, String paramString1, String paramString2, HttpRequestFactory paramHttpRequestFactory)
  {
    this(paramKit, paramString1, paramString2, paramHttpRequestFactory, HttpMethod.GET);
  }
  
  DefaultSettingsSpiCall(Kit paramKit, String paramString1, String paramString2, HttpRequestFactory paramHttpRequestFactory, HttpMethod paramHttpMethod)
  {
    super(paramKit, paramString1, paramString2, paramHttpRequestFactory, paramHttpMethod);
  }
  
  private HttpRequest applyHeadersTo(HttpRequest paramHttpRequest, SettingsRequest paramSettingsRequest)
  {
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-API-KEY", paramSettingsRequest.apiKey);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-API-CLIENT-VERSION", this.kit.getVersion());
    applyNonNullHeader(paramHttpRequest, "Accept", "application/json");
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-DEVICE-MODEL", paramSettingsRequest.deviceModel);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", paramSettingsRequest.osBuildVersion);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", paramSettingsRequest.osDisplayVersion);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-ADVERTISING-TOKEN", paramSettingsRequest.advertisingId);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-INSTALLATION-ID", paramSettingsRequest.installationId);
    applyNonNullHeader(paramHttpRequest, "X-CRASHLYTICS-ANDROID-ID", paramSettingsRequest.androidId);
    return paramHttpRequest;
  }
  
  private void applyNonNullHeader(HttpRequest paramHttpRequest, String paramString1, String paramString2)
  {
    if (paramString2 != null) {
      paramHttpRequest.header(paramString1, paramString2);
    }
  }
  
  private JSONObject getJsonObjectFrom(String paramString)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = localJSONObject;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Fabric.getLogger().d("Fabric", "Failed to parse settings JSON from " + getUrl(), localException);
        Fabric.getLogger().d("Fabric", "Settings response " + paramString);
        paramString = null;
      }
    }
    return paramString;
  }
  
  private Map<String, String> getQueryParamsFor(SettingsRequest paramSettingsRequest)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("build_version", paramSettingsRequest.buildVersion);
    localHashMap.put("display_version", paramSettingsRequest.displayVersion);
    localHashMap.put("source", Integer.toString(paramSettingsRequest.source));
    if (paramSettingsRequest.iconHash != null) {
      localHashMap.put("icon_hash", paramSettingsRequest.iconHash);
    }
    paramSettingsRequest = paramSettingsRequest.instanceId;
    if (!CommonUtils.isNullOrEmpty(paramSettingsRequest)) {
      localHashMap.put("instance", paramSettingsRequest);
    }
    return localHashMap;
  }
  
  JSONObject handleResponse(HttpRequest paramHttpRequest)
  {
    int i = paramHttpRequest.code();
    Fabric.getLogger().d("Fabric", "Settings result was: " + i);
    if (requestWasSuccessful(i)) {}
    for (paramHttpRequest = getJsonObjectFrom(paramHttpRequest.body());; paramHttpRequest = null)
    {
      return paramHttpRequest;
      Fabric.getLogger().e("Fabric", "Failed to retrieve settings from " + getUrl());
    }
  }
  
  public JSONObject invoke(SettingsRequest paramSettingsRequest)
  {
    Logger localLogger = null;
    Object localObject3 = null;
    localObject1 = localObject3;
    localObject2 = localLogger;
    try
    {
      Map localMap = getQueryParamsFor(paramSettingsRequest);
      localObject1 = localObject3;
      localObject2 = localLogger;
      localObject3 = getHttpRequest(localMap);
      localObject1 = localObject3;
      localObject2 = localObject3;
      paramSettingsRequest = applyHeadersTo((HttpRequest)localObject3, paramSettingsRequest);
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localLogger = Fabric.getLogger();
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localObject3 = new java/lang/StringBuilder;
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      ((StringBuilder)localObject3).<init>();
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localLogger.d("Fabric", "Requesting settings from " + getUrl());
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localLogger = Fabric.getLogger();
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localObject3 = new java/lang/StringBuilder;
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      ((StringBuilder)localObject3).<init>();
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localLogger.d("Fabric", "Settings query params were: " + localMap);
      localObject1 = paramSettingsRequest;
      localObject2 = paramSettingsRequest;
      localObject3 = handleResponse(paramSettingsRequest);
      localObject1 = localObject3;
      localObject2 = localObject1;
      if (paramSettingsRequest != null)
      {
        Fabric.getLogger().d("Fabric", "Settings request ID: " + paramSettingsRequest.header("X-REQUEST-ID"));
        localObject2 = localObject1;
      }
    }
    catch (HttpRequest.HttpRequestException paramSettingsRequest)
    {
      for (;;)
      {
        localObject2 = localObject1;
        Fabric.getLogger().e("Fabric", "Settings request failed.", paramSettingsRequest);
        paramSettingsRequest = null;
        localObject2 = paramSettingsRequest;
        if (localObject1 != null)
        {
          Fabric.getLogger().d("Fabric", "Settings request ID: " + ((HttpRequest)localObject1).header("X-REQUEST-ID"));
          localObject2 = paramSettingsRequest;
        }
      }
    }
    finally
    {
      if (localObject2 == null) {
        break label319;
      }
      Fabric.getLogger().d("Fabric", "Settings request ID: " + ((HttpRequest)localObject2).header("X-REQUEST-ID"));
    }
    return (JSONObject)localObject2;
  }
  
  boolean requestWasSuccessful(int paramInt)
  {
    if ((paramInt == 200) || (paramInt == 201) || (paramInt == 202) || (paramInt == 203)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\DefaultSettingsSpiCall.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */