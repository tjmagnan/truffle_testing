package org.joda.time.tz;

import java.util.SimpleTimeZone;
import java.util.TimeZone;
import org.joda.time.DateTimeZone;

public final class FixedDateTimeZone
  extends DateTimeZone
{
  private static final long serialVersionUID = -3513011772763289092L;
  private final String iNameKey;
  private final int iStandardOffset;
  private final int iWallOffset;
  
  public FixedDateTimeZone(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    super(paramString1);
    this.iNameKey = paramString2;
    this.iWallOffset = paramInt1;
    this.iStandardOffset = paramInt2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if ((paramObject instanceof FixedDateTimeZone))
      {
        paramObject = (FixedDateTimeZone)paramObject;
        if ((!getID().equals(((FixedDateTimeZone)paramObject).getID())) || (this.iStandardOffset != ((FixedDateTimeZone)paramObject).iStandardOffset) || (this.iWallOffset != ((FixedDateTimeZone)paramObject).iWallOffset)) {
          bool = false;
        }
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public String getNameKey(long paramLong)
  {
    return this.iNameKey;
  }
  
  public int getOffset(long paramLong)
  {
    return this.iWallOffset;
  }
  
  public int getOffsetFromLocal(long paramLong)
  {
    return this.iWallOffset;
  }
  
  public int getStandardOffset(long paramLong)
  {
    return this.iStandardOffset;
  }
  
  public int hashCode()
  {
    return getID().hashCode() + this.iStandardOffset * 37 + this.iWallOffset * 31;
  }
  
  public boolean isFixed()
  {
    return true;
  }
  
  public long nextTransition(long paramLong)
  {
    return paramLong;
  }
  
  public long previousTransition(long paramLong)
  {
    return paramLong;
  }
  
  public TimeZone toTimeZone()
  {
    Object localObject = getID();
    if ((((String)localObject).length() == 6) && ((((String)localObject).startsWith("+")) || (((String)localObject).startsWith("-")))) {}
    for (localObject = TimeZone.getTimeZone("GMT" + getID());; localObject = new SimpleTimeZone(this.iWallOffset, getID())) {
      return (TimeZone)localObject;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\FixedDateTimeZone.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */