package me.dm7.barcodescanner.zbar;

public class Result
{
  private BarcodeFormat mBarcodeFormat;
  private String mContents;
  
  public BarcodeFormat getBarcodeFormat()
  {
    return this.mBarcodeFormat;
  }
  
  public String getContents()
  {
    return this.mContents;
  }
  
  public void setBarcodeFormat(BarcodeFormat paramBarcodeFormat)
  {
    this.mBarcodeFormat = paramBarcodeFormat;
  }
  
  public void setContents(String paramString)
  {
    this.mContents = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\zbar\Result.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */