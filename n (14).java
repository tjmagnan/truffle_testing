package com.testfairy.n;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.testfairy.g;
import java.util.HashMap;
import java.util.Map;

public class n
  extends b
{
  private static final String a = "rssi";
  private static final String b = "ssid";
  private WifiManager c;
  private int d = Integer.MAX_VALUE;
  private String e = null;
  
  public n(com.testfairy.m.b paramb, WifiManager paramWifiManager)
  {
    super(paramb);
    this.c = paramWifiManager;
  }
  
  public void a()
  {
    try
    {
      Object localObject1 = this.c.getConnectionInfo();
      int i = ((WifiInfo)localObject1).getRssi();
      Object localObject2 = ((WifiInfo)localObject1).getSSID();
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = "";
      }
      if ((i != this.d) || (!((String)localObject1).equals(this.e)))
      {
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>(2);
        localHashMap.put("rssi", Integer.valueOf(i));
        localHashMap.put("ssid", localObject1);
        localObject2 = new com/testfairy/g;
        ((g)localObject2).<init>(19, localHashMap);
        b().a((g)localObject2);
        this.d = i;
        this.e = ((String)localObject1);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */