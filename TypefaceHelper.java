package com.afollestad.materialdialogs.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.SimpleArrayMap;

public class TypefaceHelper
{
  private static final SimpleArrayMap<String, Typeface> cache = new SimpleArrayMap();
  
  public static Typeface get(Context paramContext, String paramString)
  {
    for (;;)
    {
      synchronized (cache)
      {
        boolean bool = cache.containsKey(paramString);
        if (!bool)
        {
          try
          {
            paramContext = Typeface.createFromAsset(paramContext.getAssets(), String.format("fonts/%s", new Object[] { paramString }));
            cache.put(paramString, paramContext);
          }
          catch (RuntimeException paramContext)
          {
            paramContext = null;
            continue;
          }
          return paramContext;
        }
      }
      paramContext = (Typeface)cache.get(paramString);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\util\TypefaceHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */