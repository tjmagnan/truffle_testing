package org.joda.time.format;

import java.util.Arrays;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.IllegalInstantException;

public class DateTimeParserBucket
{
  private final Chronology iChrono;
  private final Integer iDefaultPivotYear;
  private final int iDefaultYear;
  private final DateTimeZone iDefaultZone;
  private final Locale iLocale;
  private final long iMillis;
  private Integer iOffset;
  private Integer iPivotYear;
  private SavedField[] iSavedFields;
  private int iSavedFieldsCount;
  private boolean iSavedFieldsShared;
  private Object iSavedState;
  private DateTimeZone iZone;
  
  @Deprecated
  public DateTimeParserBucket(long paramLong, Chronology paramChronology, Locale paramLocale)
  {
    this(paramLong, paramChronology, paramLocale, null, 2000);
  }
  
  @Deprecated
  public DateTimeParserBucket(long paramLong, Chronology paramChronology, Locale paramLocale, Integer paramInteger)
  {
    this(paramLong, paramChronology, paramLocale, paramInteger, 2000);
  }
  
  public DateTimeParserBucket(long paramLong, Chronology paramChronology, Locale paramLocale, Integer paramInteger, int paramInt)
  {
    paramChronology = DateTimeUtils.getChronology(paramChronology);
    this.iMillis = paramLong;
    this.iDefaultZone = paramChronology.getZone();
    this.iChrono = paramChronology.withUTC();
    paramChronology = paramLocale;
    if (paramLocale == null) {
      paramChronology = Locale.getDefault();
    }
    this.iLocale = paramChronology;
    this.iDefaultYear = paramInt;
    this.iDefaultPivotYear = paramInteger;
    this.iZone = this.iDefaultZone;
    this.iPivotYear = this.iDefaultPivotYear;
    this.iSavedFields = new SavedField[8];
  }
  
  static int compareReverse(DurationField paramDurationField1, DurationField paramDurationField2)
  {
    int i;
    if ((paramDurationField1 == null) || (!paramDurationField1.isSupported())) {
      if ((paramDurationField2 == null) || (!paramDurationField2.isSupported())) {
        i = 0;
      }
    }
    for (;;)
    {
      return i;
      i = -1;
      continue;
      if ((paramDurationField2 == null) || (!paramDurationField2.isSupported())) {
        i = 1;
      } else {
        i = -paramDurationField1.compareTo(paramDurationField2);
      }
    }
  }
  
  private SavedField obtainSaveField()
  {
    Object localObject2 = this.iSavedFields;
    int j = this.iSavedFieldsCount;
    int i;
    Object localObject1;
    if ((j == localObject2.length) || (this.iSavedFieldsShared)) {
      if (j == localObject2.length)
      {
        i = j * 2;
        localObject1 = new SavedField[i];
        System.arraycopy(localObject2, 0, localObject1, 0, j);
        this.iSavedFields = ((SavedField[])localObject1);
        this.iSavedFieldsShared = false;
      }
    }
    for (;;)
    {
      this.iSavedState = null;
      localObject2 = localObject1[j];
      if (localObject2 == null)
      {
        localObject2 = new SavedField();
        localObject1[j] = localObject2;
      }
      for (localObject1 = localObject2;; localObject1 = localObject2)
      {
        this.iSavedFieldsCount = (j + 1);
        return (SavedField)localObject1;
        i = localObject2.length;
        break;
      }
      localObject1 = localObject2;
    }
  }
  
  private static void sort(SavedField[] paramArrayOfSavedField, int paramInt)
  {
    int i = 0;
    if (paramInt > 10)
    {
      Arrays.sort(paramArrayOfSavedField, 0, paramInt);
      return;
    }
    for (;;)
    {
      i++;
      if (i >= paramInt) {
        break;
      }
      for (int j = i; (j > 0) && (paramArrayOfSavedField[(j - 1)].compareTo(paramArrayOfSavedField[j]) > 0); j--)
      {
        SavedField localSavedField = paramArrayOfSavedField[j];
        paramArrayOfSavedField[j] = paramArrayOfSavedField[(j - 1)];
        paramArrayOfSavedField[(j - 1)] = localSavedField;
      }
    }
  }
  
  public long computeMillis()
  {
    return computeMillis(false, (CharSequence)null);
  }
  
  public long computeMillis(boolean paramBoolean)
  {
    return computeMillis(paramBoolean, (CharSequence)null);
  }
  
  public long computeMillis(boolean paramBoolean, CharSequence paramCharSequence)
  {
    SavedField[] arrayOfSavedField = this.iSavedFields;
    int j = this.iSavedFieldsCount;
    if (this.iSavedFieldsShared)
    {
      arrayOfSavedField = (SavedField[])this.iSavedFields.clone();
      this.iSavedFields = arrayOfSavedField;
      this.iSavedFieldsShared = false;
    }
    sort(arrayOfSavedField, j);
    long l1;
    if (j > 0)
    {
      DurationField localDurationField1 = DurationFieldType.months().getField(this.iChrono);
      localObject2 = DurationFieldType.days().getField(this.iChrono);
      DurationField localDurationField2 = arrayOfSavedField[0].iField.getDurationField();
      if ((compareReverse(localDurationField2, localDurationField1) >= 0) && (compareReverse(localDurationField2, (DurationField)localObject2) <= 0))
      {
        saveField(DateTimeFieldType.year(), this.iDefaultYear);
        l1 = computeMillis(paramBoolean, paramCharSequence);
      }
    }
    int i;
    long l2;
    label259:
    do
    {
      do
      {
        for (;;)
        {
          return l1;
          l1 = this.iMillis;
          i = 0;
          for (;;)
          {
            if (i < j) {}
            try
            {
              l1 = arrayOfSavedField[i].set(l1, paramBoolean);
              i++;
            }
            catch (IllegalFieldValueException localIllegalFieldValueException)
            {
              if (paramCharSequence == null) {
                break label259;
              }
              localIllegalFieldValueException.prependMessage("Cannot parse \"" + paramCharSequence + '"');
              throw localIllegalFieldValueException;
            }
          }
          l2 = l1;
          if (paramBoolean)
          {
            i = 0;
            l2 = l1;
            if (i < j)
            {
              localObject2 = arrayOfSavedField[i];
              if (i == j - 1) {}
              for (paramBoolean = true;; paramBoolean = false)
              {
                l1 = ((SavedField)localObject2).set(l1, paramBoolean);
                i++;
                break;
              }
            }
          }
          if (this.iOffset == null) {
            break;
          }
          l1 = l2 - this.iOffset.intValue();
        }
        l1 = l2;
      } while (this.iZone == null);
      i = this.iZone.getOffsetFromLocal(l2);
      l2 -= i;
      l1 = l2;
    } while (i == this.iZone.getOffset(l2));
    Object localObject2 = "Illegal instant due to time zone offset transition (" + this.iZone + ')';
    Object localObject1 = localObject2;
    if (paramCharSequence != null) {
      localObject1 = "Cannot parse \"" + paramCharSequence + "\": " + (String)localObject2;
    }
    throw new IllegalInstantException((String)localObject1);
  }
  
  public long computeMillis(boolean paramBoolean, String paramString)
  {
    return computeMillis(paramBoolean, paramString);
  }
  
  long doParseMillis(InternalParser paramInternalParser, CharSequence paramCharSequence)
  {
    int j = paramInternalParser.parseInto(this, paramCharSequence, 0);
    int i;
    if (j >= 0)
    {
      i = j;
      if (j >= paramCharSequence.length()) {
        return computeMillis(true, paramCharSequence);
      }
    }
    else
    {
      i = j ^ 0xFFFFFFFF;
    }
    throw new IllegalArgumentException(FormatUtils.createErrorMessage(paramCharSequence.toString(), i));
  }
  
  public Chronology getChronology()
  {
    return this.iChrono;
  }
  
  public Locale getLocale()
  {
    return this.iLocale;
  }
  
  @Deprecated
  public int getOffset()
  {
    if (this.iOffset != null) {}
    for (int i = this.iOffset.intValue();; i = 0) {
      return i;
    }
  }
  
  public Integer getOffsetInteger()
  {
    return this.iOffset;
  }
  
  public Integer getPivotYear()
  {
    return this.iPivotYear;
  }
  
  public DateTimeZone getZone()
  {
    return this.iZone;
  }
  
  public long parseMillis(DateTimeParser paramDateTimeParser, CharSequence paramCharSequence)
  {
    reset();
    return doParseMillis(DateTimeParserInternalParser.of(paramDateTimeParser), paramCharSequence);
  }
  
  public void reset()
  {
    this.iZone = this.iDefaultZone;
    this.iOffset = null;
    this.iPivotYear = this.iDefaultPivotYear;
    this.iSavedFieldsCount = 0;
    this.iSavedFieldsShared = false;
    this.iSavedState = null;
  }
  
  public boolean restoreState(Object paramObject)
  {
    if (((paramObject instanceof SavedState)) && (((SavedState)paramObject).restoreState(this))) {
      this.iSavedState = paramObject;
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void saveField(DateTimeField paramDateTimeField, int paramInt)
  {
    obtainSaveField().init(paramDateTimeField, paramInt);
  }
  
  public void saveField(DateTimeFieldType paramDateTimeFieldType, int paramInt)
  {
    obtainSaveField().init(paramDateTimeFieldType.getField(this.iChrono), paramInt);
  }
  
  public void saveField(DateTimeFieldType paramDateTimeFieldType, String paramString, Locale paramLocale)
  {
    obtainSaveField().init(paramDateTimeFieldType.getField(this.iChrono), paramString, paramLocale);
  }
  
  public Object saveState()
  {
    if (this.iSavedState == null) {
      this.iSavedState = new SavedState();
    }
    return this.iSavedState;
  }
  
  @Deprecated
  public void setOffset(int paramInt)
  {
    this.iSavedState = null;
    this.iOffset = Integer.valueOf(paramInt);
  }
  
  public void setOffset(Integer paramInteger)
  {
    this.iSavedState = null;
    this.iOffset = paramInteger;
  }
  
  @Deprecated
  public void setPivotYear(Integer paramInteger)
  {
    this.iPivotYear = paramInteger;
  }
  
  public void setZone(DateTimeZone paramDateTimeZone)
  {
    this.iSavedState = null;
    this.iZone = paramDateTimeZone;
  }
  
  static class SavedField
    implements Comparable<SavedField>
  {
    DateTimeField iField;
    Locale iLocale;
    String iText;
    int iValue;
    
    public int compareTo(SavedField paramSavedField)
    {
      paramSavedField = paramSavedField.iField;
      int i = DateTimeParserBucket.compareReverse(this.iField.getRangeDurationField(), paramSavedField.getRangeDurationField());
      if (i != 0) {}
      for (;;)
      {
        return i;
        i = DateTimeParserBucket.compareReverse(this.iField.getDurationField(), paramSavedField.getDurationField());
      }
    }
    
    void init(DateTimeField paramDateTimeField, int paramInt)
    {
      this.iField = paramDateTimeField;
      this.iValue = paramInt;
      this.iText = null;
      this.iLocale = null;
    }
    
    void init(DateTimeField paramDateTimeField, String paramString, Locale paramLocale)
    {
      this.iField = paramDateTimeField;
      this.iValue = 0;
      this.iText = paramString;
      this.iLocale = paramLocale;
    }
    
    long set(long paramLong, boolean paramBoolean)
    {
      if (this.iText == null) {}
      for (paramLong = this.iField.setExtended(paramLong, this.iValue);; paramLong = this.iField.set(paramLong, this.iText, this.iLocale))
      {
        long l = paramLong;
        if (paramBoolean) {
          l = this.iField.roundFloor(paramLong);
        }
        return l;
      }
    }
  }
  
  class SavedState
  {
    final Integer iOffset = DateTimeParserBucket.this.iOffset;
    final DateTimeParserBucket.SavedField[] iSavedFields = DateTimeParserBucket.this.iSavedFields;
    final int iSavedFieldsCount = DateTimeParserBucket.this.iSavedFieldsCount;
    final DateTimeZone iZone = DateTimeParserBucket.this.iZone;
    
    SavedState() {}
    
    boolean restoreState(DateTimeParserBucket paramDateTimeParserBucket)
    {
      boolean bool = true;
      if (paramDateTimeParserBucket != DateTimeParserBucket.this) {
        bool = false;
      }
      for (;;)
      {
        return bool;
        DateTimeParserBucket.access$002(paramDateTimeParserBucket, this.iZone);
        DateTimeParserBucket.access$102(paramDateTimeParserBucket, this.iOffset);
        DateTimeParserBucket.access$202(paramDateTimeParserBucket, this.iSavedFields);
        if (this.iSavedFieldsCount < paramDateTimeParserBucket.iSavedFieldsCount) {
          DateTimeParserBucket.access$402(paramDateTimeParserBucket, true);
        }
        DateTimeParserBucket.access$302(paramDateTimeParserBucket, this.iSavedFieldsCount);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\DateTimeParserBucket.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */