package org.jsoup.nodes;

import java.io.IOException;

public class DataNode
  extends Node
{
  private static final String DATA_KEY = "data";
  
  public DataNode(String paramString1, String paramString2)
  {
    super(paramString2);
    this.attributes.put("data", paramString1);
  }
  
  public static DataNode createFromEncoded(String paramString1, String paramString2)
  {
    return new DataNode(Entities.unescape(paramString1), paramString2);
  }
  
  public String getWholeData()
  {
    return this.attributes.get("data");
  }
  
  public String nodeName()
  {
    return "#data";
  }
  
  void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    paramAppendable.append(getWholeData());
  }
  
  void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings) {}
  
  public DataNode setWholeData(String paramString)
  {
    this.attributes.put("data", paramString);
    return this;
  }
  
  public String toString()
  {
    return outerHtml();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\DataNode.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */