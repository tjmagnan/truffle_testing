package org.joda.time;

import java.io.Serializable;
import org.joda.convert.FromString;
import org.joda.time.base.AbstractInstant;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.InstantConverter;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class Instant
  extends AbstractInstant
  implements ReadableInstant, Serializable
{
  private static final long serialVersionUID = 3299096530934209741L;
  private final long iMillis;
  
  public Instant()
  {
    this.iMillis = DateTimeUtils.currentTimeMillis();
  }
  
  public Instant(long paramLong)
  {
    this.iMillis = paramLong;
  }
  
  public Instant(Object paramObject)
  {
    this.iMillis = ConverterManager.getInstance().getInstantConverter(paramObject).getInstantMillis(paramObject, ISOChronology.getInstanceUTC());
  }
  
  public static Instant now()
  {
    return new Instant();
  }
  
  @FromString
  public static Instant parse(String paramString)
  {
    return parse(paramString, ISODateTimeFormat.dateTimeParser());
  }
  
  public static Instant parse(String paramString, DateTimeFormatter paramDateTimeFormatter)
  {
    return paramDateTimeFormatter.parseDateTime(paramString).toInstant();
  }
  
  public Chronology getChronology()
  {
    return ISOChronology.getInstanceUTC();
  }
  
  public long getMillis()
  {
    return this.iMillis;
  }
  
  public Instant minus(long paramLong)
  {
    return withDurationAdded(paramLong, -1);
  }
  
  public Instant minus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, -1);
  }
  
  public Instant plus(long paramLong)
  {
    return withDurationAdded(paramLong, 1);
  }
  
  public Instant plus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, 1);
  }
  
  public DateTime toDateTime()
  {
    return new DateTime(getMillis(), ISOChronology.getInstance());
  }
  
  @Deprecated
  public DateTime toDateTimeISO()
  {
    return toDateTime();
  }
  
  public Instant toInstant()
  {
    return this;
  }
  
  public MutableDateTime toMutableDateTime()
  {
    return new MutableDateTime(getMillis(), ISOChronology.getInstance());
  }
  
  @Deprecated
  public MutableDateTime toMutableDateTimeISO()
  {
    return toMutableDateTime();
  }
  
  public Instant withDurationAdded(long paramLong, int paramInt)
  {
    Instant localInstant = this;
    if (paramLong != 0L) {
      if (paramInt != 0) {
        break label19;
      }
    }
    label19:
    for (localInstant = this;; localInstant = withMillis(getChronology().add(getMillis(), paramLong, paramInt))) {
      return localInstant;
    }
  }
  
  public Instant withDurationAdded(ReadableDuration paramReadableDuration, int paramInt)
  {
    Instant localInstant = this;
    if (paramReadableDuration != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localInstant = this;; localInstant = withDurationAdded(paramReadableDuration.getMillis(), paramInt)) {
      return localInstant;
    }
  }
  
  public Instant withMillis(long paramLong)
  {
    if (paramLong == this.iMillis) {}
    for (Instant localInstant = this;; localInstant = new Instant(paramLong)) {
      return localInstant;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Instant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */