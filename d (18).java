package com.testfairy.d;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.webkit.WebView;
import java.lang.reflect.Method;

public class r
  extends View
{
  private WebView a;
  
  public r(Context paramContext)
  {
    super(paramContext);
  }
  
  void a(WebView paramWebView)
  {
    this.a = paramWebView;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    try
    {
      if (this.a != null)
      {
        Method localMethod = this.a.getClass().getDeclaredMethod("onDraw", new Class[] { Canvas.class });
        localMethod.setAccessible(true);
        localMethod.invoke(this.a, new Object[] { paramCanvas });
      }
      return;
    }
    catch (Exception paramCanvas)
    {
      for (;;) {}
    }
    catch (OutOfMemoryError paramCanvas)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */