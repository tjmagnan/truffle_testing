package me.dm7.barcodescanner.zbar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BarcodeFormat
{
  public static final List<BarcodeFormat> ALL_FORMATS;
  public static final BarcodeFormat CODABAR;
  public static final BarcodeFormat CODE128;
  public static final BarcodeFormat CODE39;
  public static final BarcodeFormat CODE93;
  public static final BarcodeFormat DATABAR;
  public static final BarcodeFormat DATABAR_EXP;
  public static final BarcodeFormat EAN13;
  public static final BarcodeFormat EAN8;
  public static final BarcodeFormat I25;
  public static final BarcodeFormat ISBN10;
  public static final BarcodeFormat ISBN13;
  public static final BarcodeFormat NONE = new BarcodeFormat(0, "NONE");
  public static final BarcodeFormat PARTIAL = new BarcodeFormat(1, "PARTIAL");
  public static final BarcodeFormat PDF417;
  public static final BarcodeFormat QRCODE;
  public static final BarcodeFormat UPCA;
  public static final BarcodeFormat UPCE;
  private int mId;
  private String mName;
  
  static
  {
    EAN8 = new BarcodeFormat(8, "EAN8");
    UPCE = new BarcodeFormat(9, "UPCE");
    ISBN10 = new BarcodeFormat(10, "ISBN10");
    UPCA = new BarcodeFormat(12, "UPCA");
    EAN13 = new BarcodeFormat(13, "EAN13");
    ISBN13 = new BarcodeFormat(14, "ISBN13");
    I25 = new BarcodeFormat(25, "I25");
    DATABAR = new BarcodeFormat(34, "DATABAR");
    DATABAR_EXP = new BarcodeFormat(35, "DATABAR_EXP");
    CODABAR = new BarcodeFormat(38, "CODABAR");
    CODE39 = new BarcodeFormat(39, "CODE39");
    PDF417 = new BarcodeFormat(57, "PDF417");
    QRCODE = new BarcodeFormat(64, "QRCODE");
    CODE93 = new BarcodeFormat(93, "CODE93");
    CODE128 = new BarcodeFormat(128, "CODE128");
    ALL_FORMATS = new ArrayList();
    ALL_FORMATS.add(PARTIAL);
    ALL_FORMATS.add(EAN8);
    ALL_FORMATS.add(UPCE);
    ALL_FORMATS.add(ISBN10);
    ALL_FORMATS.add(UPCA);
    ALL_FORMATS.add(EAN13);
    ALL_FORMATS.add(ISBN13);
    ALL_FORMATS.add(I25);
    ALL_FORMATS.add(DATABAR);
    ALL_FORMATS.add(DATABAR_EXP);
    ALL_FORMATS.add(CODABAR);
    ALL_FORMATS.add(CODE39);
    ALL_FORMATS.add(PDF417);
    ALL_FORMATS.add(QRCODE);
    ALL_FORMATS.add(CODE93);
    ALL_FORMATS.add(CODE128);
  }
  
  public BarcodeFormat(int paramInt, String paramString)
  {
    this.mId = paramInt;
    this.mName = paramString;
  }
  
  public static BarcodeFormat getFormatById(int paramInt)
  {
    Iterator localIterator = ALL_FORMATS.iterator();
    BarcodeFormat localBarcodeFormat;
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      localBarcodeFormat = (BarcodeFormat)localIterator.next();
    } while (localBarcodeFormat.getId() != paramInt);
    for (;;)
    {
      return localBarcodeFormat;
      localBarcodeFormat = NONE;
    }
  }
  
  public int getId()
  {
    return this.mId;
  }
  
  public String getName()
  {
    return this.mName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\zbar\BarcodeFormat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */