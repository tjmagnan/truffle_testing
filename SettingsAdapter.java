package tech.dcube.companion.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.util.List;
import tech.dcube.companion.fragments.SettingsFragment;

public class SettingsAdapter
  extends FragmentStatePagerAdapter
{
  private List<SettingsFragment> fragments;
  
  public SettingsAdapter(FragmentManager paramFragmentManager, List<SettingsFragment> paramList)
  {
    super(paramFragmentManager);
    this.fragments = paramList;
  }
  
  public int getCount()
  {
    return this.fragments.size();
  }
  
  public Fragment getItem(int paramInt)
  {
    return (Fragment)this.fragments.get(paramInt);
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    return ((SettingsFragment)this.fragments.get(paramInt)).mMeterName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\adapters\SettingsAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */