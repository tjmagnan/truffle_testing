package com.testfairy.b;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class c
{
  private static final int a = 524288;
  private static final byte[] b = { 82, 73, 70, 70, 48, 48, 48, 48, 87, 65, 86, 69, 102, 109, 116, 32, 16, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 16, 0, 100, 97, 116, 97, 0, 0, 0, 0 };
  private int c = 0;
  private final List d = Collections.synchronizedList(new ArrayList());
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private float i;
  private a j = null;
  private final b.a k;
  
  public c(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat)
  {
    this.e = paramInt1;
    this.f = paramInt2;
    this.g = paramInt3;
    this.h = paramInt4;
    this.i = paramFloat;
    this.k = new b.a();
    this.k.a = paramInt1;
    this.k.b = paramInt1;
    this.k.d = paramInt2;
    this.k.c = paramInt3;
    if (paramInt1 > 22050) {
      this.k.b = (paramInt1 >> 2);
    }
    for (;;)
    {
      this.j = b.a(this.k);
      return;
      if (paramInt1 > 11025) {
        this.k.b = (paramInt1 >> 1);
      }
    }
  }
  
  public int a()
  {
    return this.k.a;
  }
  
  public void a(float paramFloat)
  {
    this.i = paramFloat;
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (this.c < 524288)
    {
      paramArrayOfByte = this.j.a(paramArrayOfByte, paramInt1, paramInt2);
      this.d.add(paramArrayOfByte);
      paramInt1 = this.c;
      this.c = (paramArrayOfByte.length + paramInt1);
    }
  }
  
  public void a(short[] paramArrayOfShort, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = new byte[paramInt2 * 2];
    int n = 0;
    int m = 0;
    while (n < paramInt2)
    {
      int i1 = m + 1;
      arrayOfByte[m] = ((byte)(paramArrayOfShort[paramInt1] & 0xFF));
      m = i1 + 1;
      arrayOfByte[i1] = ((byte)(paramArrayOfShort[paramInt1] >> 8 & 0xFF));
      n++;
      paramInt1++;
    }
    a(arrayOfByte, 0, paramInt2 * 2);
  }
  
  public int b()
  {
    return this.k.b;
  }
  
  public int c()
  {
    return this.f;
  }
  
  public int d()
  {
    return this.g;
  }
  
  public int e()
  {
    return this.h;
  }
  
  public int f()
  {
    return this.c;
  }
  
  public float g()
  {
    return this.i;
  }
  
  public void h()
  {
    synchronized (this.d)
    {
      this.d.clear();
      this.c = 0;
      return;
    }
  }
  
  public float i()
  {
    int n = f();
    int m = n;
    if (d() == 2) {
      m = n >> 1;
    }
    n = m;
    if (c() == 16) {
      n = m >> 1;
    }
    float f1 = b();
    return n / f1;
  }
  
  public byte[] j()
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    localByteArrayOutputStream.write(b);
    synchronized (this.d)
    {
      Iterator localIterator = this.d.iterator();
      if (localIterator.hasNext())
      {
        byte[] arrayOfByte = (byte[])localIterator.next();
        localByteArrayOutputStream.write(arrayOfByte, 0, arrayOfByte.length);
      }
    }
    ??? = localByteArrayOutputStream.toByteArray();
    int i1 = this.k.b;
    int m = this.k.b * this.k.d * this.k.c / 8;
    int n = ???.length + b.length - 4;
    ???[4] = ((byte)(n & 0xFF));
    ???[5] = ((byte)(n >> 8 & 0xFF));
    ???[6] = ((byte)(n >> 16 & 0xFF));
    ???[7] = ((byte)(n >> 24 & 0xFF));
    ???[22] = ((byte)(this.g & 0xFF));
    ???[24] = ((byte)(i1 & 0xFF));
    ???[25] = ((byte)(i1 >> 8 & 0xFF));
    ???[26] = ((byte)(i1 >> 16 & 0xFF));
    ???[27] = ((byte)(i1 >> 24 & 0xFF));
    ???[28] = ((byte)(m & 0xFF));
    ???[29] = ((byte)(m >> 8 & 0xFF));
    ???[30] = ((byte)(m >> 16 & 0xFF));
    ???[31] = ((byte)(m >> 24 & 0xFF));
    ???[34] = ((byte)(this.f & 0xFF));
    ???[35] = ((byte)(this.f >> 8 & 0xFF));
    ???[40] = ((byte)(???.length & 0xFF));
    ???[41] = ((byte)(???.length >> 8 & 0xFF));
    ???[42] = ((byte)(???.length >> 16 & 0xFF));
    ???[43] = ((byte)(???.length >> 24 & 0xFF));
    return (byte[])???;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\b\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */