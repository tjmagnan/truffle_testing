package com.testfairy.activities.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class g
  extends LinearLayout
{
  public static final String a = "drawing_board";
  private final ImageView b;
  private d c;
  private final FrameLayout d;
  private final f e;
  
  public g(Context paramContext, Bitmap paramBitmap, final h paramh)
  {
    super(paramContext);
    setOrientation(1);
    this.e = new f(paramContext, new e()
    {
      public void a() {}
      
      public void a(int paramAnonymousInt)
      {
        g.a(g.this).a(paramAnonymousInt);
      }
      
      public void b()
      {
        paramh.b();
      }
      
      public void c()
      {
        paramh.a();
      }
    });
    addView(this.e);
    this.d = new FrameLayout(paramContext);
    this.d.setContentDescription("drawing_board");
    this.b = new ImageView(paramContext);
    this.b.setImageBitmap(paramBitmap);
    this.b.setAdjustViewBounds(true);
    this.d.addView(this.b, new LinearLayout.LayoutParams(-2, -2));
    setBackgroundColor(-7829368);
    a();
    this.c = new d(paramContext);
    this.d.addView(this.c);
    this.b.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
    {
      boolean a = true;
      
      public void onLayoutChange(View paramAnonymousView, final int paramAnonymousInt1, final int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
      {
        paramAnonymousInt2 = paramAnonymousView.getWidth();
        paramAnonymousInt1 = paramAnonymousView.getHeight();
        if ((this.a) && (paramAnonymousInt2 > 0) && (paramAnonymousInt1 > 0))
        {
          this.a = false;
          g.a(g.this).a(paramAnonymousInt2, paramAnonymousInt1);
          g.a(g.this).post(new Runnable()
          {
            public void run()
            {
              Log.d(com.testfairy.e.a, "run: set new LayoutParams");
              g.a(g.this).getLayoutParams().width = paramAnonymousInt2;
              g.a(g.this).getLayoutParams().height = paramAnonymousInt1;
              g.a(g.this).requestLayout();
            }
          });
        }
      }
    });
    paramContext = new LinearLayout.LayoutParams(-2, -2);
    paramContext.gravity = 17;
    paramContext.setMargins(25, 25, 25, 25);
    addView(this.d, paramContext);
  }
  
  public void a()
  {
    setVisibility(8);
  }
  
  public void a(Bitmap paramBitmap)
  {
    this.b.setImageBitmap(paramBitmap);
  }
  
  public Bitmap b()
  {
    this.d.destroyDrawingCache();
    this.d.buildDrawingCache();
    return Bitmap.createBitmap(this.d.getDrawingCache());
  }
  
  public void c()
  {
    setVisibility(0);
  }
  
  public void d()
  {
    this.c.a();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\a\a\g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */