package tech.dcube.companion;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import tech.dcube.companion.fragments.PBTopBarFragment;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBShipment;

public class ShippingSummaryFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private OnFragmentInteractionListener mListener;
  private String mParam1;
  private String mParam2;
  LinearLayout shipItemLayout;
  
  public static ShippingSummaryFragment newInstance(String paramString1, String paramString2)
  {
    ShippingSummaryFragment localShippingSummaryFragment = new ShippingSummaryFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localShippingSummaryFragment.setArguments(localBundle);
    return localShippingSummaryFragment;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnFragmentInteractionListener))
    {
      this.mListener = ((OnFragmentInteractionListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnFragmentInteractionListener");
  }
  
  public void onButtonPressed(Uri paramUri)
  {
    if (this.mListener != null) {
      this.mListener.onFragmentInteraction(paramUri);
    }
  }
  
  public void onClickTopBarLeftButton() {}
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968649, paramViewGroup, false);
    this.shipItemLayout = ((LinearLayout)paramLayoutInflater.findViewById(2131624182));
    if (DataManager.getInstance().getCurrentShipment().isManualShipment()) {
      this.shipItemLayout.setVisibility(8);
    }
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Summary");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Cancel");
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public static abstract interface OnFragmentInteractionListener
  {
    public abstract void onFragmentInteraction(Uri paramUri);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\ShippingSummaryFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */