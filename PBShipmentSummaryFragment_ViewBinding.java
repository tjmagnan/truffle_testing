package tech.dcube.companion.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PBShipmentSummaryFragment_ViewBinding
  extends PBTopBarFragment_ViewBinding
{
  private PBShipmentSummaryFragment target;
  private View view2131624189;
  
  @UiThread
  public PBShipmentSummaryFragment_ViewBinding(final PBShipmentSummaryFragment paramPBShipmentSummaryFragment, View paramView)
  {
    super(paramPBShipmentSummaryFragment, paramView);
    this.target = paramPBShipmentSummaryFragment;
    paramView = Utils.findRequiredView(paramView, 2131624189, "method 'payWithSendPro'");
    this.view2131624189 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBShipmentSummaryFragment.payWithSendPro();
      }
    });
  }
  
  public void unbind()
  {
    if (this.target == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    this.view2131624189.setOnClickListener(null);
    this.view2131624189 = null;
    super.unbind();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBShipmentSummaryFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */