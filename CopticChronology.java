package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeZone;
import org.joda.time.field.SkipDateTimeField;

public final class CopticChronology
  extends BasicFixedMonthChronology
{
  public static final int AM = 1;
  private static final DateTimeField ERA_FIELD = new BasicSingleEraDateTimeField("AM");
  private static final CopticChronology INSTANCE_UTC = getInstance(DateTimeZone.UTC);
  private static final int MAX_YEAR = 292272708;
  private static final int MIN_YEAR = -292269337;
  private static final ConcurrentHashMap<DateTimeZone, CopticChronology[]> cCache = new ConcurrentHashMap();
  private static final long serialVersionUID = -5972804258688333942L;
  
  CopticChronology(Chronology paramChronology, Object paramObject, int paramInt)
  {
    super(paramChronology, paramObject, paramInt);
  }
  
  public static CopticChronology getInstance()
  {
    return getInstance(DateTimeZone.getDefault(), 4);
  }
  
  public static CopticChronology getInstance(DateTimeZone paramDateTimeZone)
  {
    return getInstance(paramDateTimeZone, 4);
  }
  
  public static CopticChronology getInstance(DateTimeZone paramDateTimeZone, int paramInt)
  {
    Object localObject3 = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localObject3 = DateTimeZone.getDefault();
    }
    paramDateTimeZone = (CopticChronology[])cCache.get(localObject3);
    Object localObject1;
    if (paramDateTimeZone == null)
    {
      localObject1 = new CopticChronology[7];
      paramDateTimeZone = (CopticChronology[])cCache.putIfAbsent(localObject3, localObject1);
      if (paramDateTimeZone == null) {}
    }
    for (;;)
    {
      DateTime localDateTime = paramDateTimeZone[(paramInt - 1)];
      localObject1 = localDateTime;
      if (localDateTime == null)
      {
        localDateTime = paramDateTimeZone[(paramInt - 1)];
        localObject1 = localDateTime;
        if (localDateTime != null) {}
      }
      try
      {
        if (localObject3 == DateTimeZone.UTC)
        {
          localObject3 = new org/joda/time/chrono/CopticChronology;
          ((CopticChronology)localObject3).<init>(null, null, paramInt);
          localDateTime = new org/joda/time/DateTime;
          localDateTime.<init>(1, 1, 1, 0, 0, 0, 0, (Chronology)localObject3);
          localObject1 = new org/joda/time/chrono/CopticChronology;
          ((CopticChronology)localObject1).<init>(LimitChronology.getInstance((Chronology)localObject3, localDateTime, null), null, paramInt);
        }
        for (;;)
        {
          paramDateTimeZone[(paramInt - 1)] = localObject1;
          return (CopticChronology)localObject1;
          localObject1 = new CopticChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, paramInt), (DateTimeZone)localObject3), null, paramInt);
        }
        paramDateTimeZone = (DateTimeZone)localObject2;
      }
      finally {}
    }
  }
  
  public static CopticChronology getInstanceUTC()
  {
    return INSTANCE_UTC;
  }
  
  private Object readResolve()
  {
    Object localObject = getBase();
    int j = getMinimumDaysInFirstWeek();
    int i = j;
    if (j == 0) {
      i = 4;
    }
    if (localObject == null) {}
    for (localObject = getInstance(DateTimeZone.UTC, i);; localObject = getInstance(((Chronology)localObject).getZone(), i)) {
      return localObject;
    }
  }
  
  protected void assemble(AssembledChronology.Fields paramFields)
  {
    if (getBase() == null)
    {
      super.assemble(paramFields);
      paramFields.year = new SkipDateTimeField(this, paramFields.year);
      paramFields.weekyear = new SkipDateTimeField(this, paramFields.weekyear);
      paramFields.era = ERA_FIELD;
      paramFields.monthOfYear = new BasicMonthOfYearDateTimeField(this, 13);
      paramFields.months = paramFields.monthOfYear.getDurationField();
    }
  }
  
  long calculateFirstDayOfYearMillis(int paramInt)
  {
    int k = paramInt - 1687;
    int i;
    if (k <= 0) {
      i = k + 3 >> 2;
    }
    for (;;)
    {
      long l = k;
      return (i + l * 365L) * 86400000L + 21859200000L;
      int j = k >> 2;
      i = j;
      if (!isLeapYear(paramInt)) {
        i = j + 1;
      }
    }
  }
  
  long getApproxMillisAtEpochDividedByTwo()
  {
    return 26607895200000L;
  }
  
  int getMaxYear()
  {
    return 292272708;
  }
  
  int getMinYear()
  {
    return -292269337;
  }
  
  boolean isLeapDay(long paramLong)
  {
    if ((dayOfMonth().get(paramLong) == 6) && (monthOfYear().isLeap(paramLong))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Chronology withUTC()
  {
    return INSTANCE_UTC;
  }
  
  public Chronology withZone(DateTimeZone paramDateTimeZone)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    if (localDateTimeZone == getZone()) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = getInstance(localDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\CopticChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */