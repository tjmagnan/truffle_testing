package com.google.gson.internal.bind.util;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class ISO8601Utils
{
  private static final TimeZone TIMEZONE_UTC = TimeZone.getTimeZone("UTC");
  private static final String UTC_ID = "UTC";
  
  private static boolean checkOffset(String paramString, int paramInt, char paramChar)
  {
    if ((paramInt < paramString.length()) && (paramString.charAt(paramInt) == paramChar)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static String format(Date paramDate)
  {
    return format(paramDate, false, TIMEZONE_UTC);
  }
  
  public static String format(Date paramDate, boolean paramBoolean)
  {
    return format(paramDate, paramBoolean, TIMEZONE_UTC);
  }
  
  public static String format(Date paramDate, boolean paramBoolean, TimeZone paramTimeZone)
  {
    GregorianCalendar localGregorianCalendar = new GregorianCalendar(paramTimeZone, Locale.US);
    localGregorianCalendar.setTime(paramDate);
    int k = "yyyy-MM-ddThh:mm:ss".length();
    int i;
    int j;
    label51:
    char c;
    if (paramBoolean)
    {
      i = ".sss".length();
      if (paramTimeZone.getRawOffset() != 0) {
        break label320;
      }
      j = "Z".length();
      paramDate = new StringBuilder(k + i + j);
      padInt(paramDate, localGregorianCalendar.get(1), "yyyy".length());
      paramDate.append('-');
      padInt(paramDate, localGregorianCalendar.get(2) + 1, "MM".length());
      paramDate.append('-');
      padInt(paramDate, localGregorianCalendar.get(5), "dd".length());
      paramDate.append('T');
      padInt(paramDate, localGregorianCalendar.get(11), "hh".length());
      paramDate.append(':');
      padInt(paramDate, localGregorianCalendar.get(12), "mm".length());
      paramDate.append(':');
      padInt(paramDate, localGregorianCalendar.get(13), "ss".length());
      if (paramBoolean)
      {
        paramDate.append('.');
        padInt(paramDate, localGregorianCalendar.get(14), "sss".length());
      }
      k = paramTimeZone.getOffset(localGregorianCalendar.getTimeInMillis());
      if (k == 0) {
        break label336;
      }
      j = Math.abs(k / 60000 / 60);
      i = Math.abs(k / 60000 % 60);
      if (k >= 0) {
        break label330;
      }
      c = '-';
      label274:
      paramDate.append(c);
      padInt(paramDate, j, "hh".length());
      paramDate.append(':');
      padInt(paramDate, i, "mm".length());
    }
    for (;;)
    {
      return paramDate.toString();
      i = 0;
      break;
      label320:
      j = "+hh:mm".length();
      break label51;
      label330:
      c = '+';
      break label274;
      label336:
      paramDate.append('Z');
    }
  }
  
  private static int indexOfNonDigit(String paramString, int paramInt)
  {
    if (paramInt < paramString.length())
    {
      int j = paramString.charAt(paramInt);
      i = paramInt;
      if (j >= 48) {
        if (j <= 57) {
          break label32;
        }
      }
    }
    for (int i = paramInt;; i = paramString.length())
    {
      return i;
      label32:
      paramInt++;
      break;
    }
  }
  
  private static void padInt(StringBuilder paramStringBuilder, int paramInt1, int paramInt2)
  {
    String str = Integer.toString(paramInt1);
    for (paramInt1 = paramInt2 - str.length(); paramInt1 > 0; paramInt1--) {
      paramStringBuilder.append('0');
    }
    paramStringBuilder.append(str);
  }
  
  public static Date parse(String paramString, ParsePosition paramParsePosition)
    throws ParseException
  {
    try
    {
      i = paramParsePosition.getIndex();
      j = i + 4;
      i6 = parseInt(paramString, i, j);
      i = j;
      if (checkOffset(paramString, j, '-')) {
        i = j + 1;
      }
      j = i + 2;
      i7 = parseInt(paramString, i, j);
      if (!checkOffset(paramString, j, '-')) {
        break label1028;
      }
      i = j + 1;
    }
    catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
    {
      int i6;
      int i7;
      int i8;
      boolean bool;
      Object localObject1;
      int i5;
      String str;
      while (paramString == null)
      {
        paramString = null;
        str = localIndexOutOfBoundsException.getMessage();
        if (str != null)
        {
          localObject3 = str;
          if (!str.isEmpty()) {}
        }
        else
        {
          localObject3 = "(" + localIndexOutOfBoundsException.getClass().getName() + ")";
        }
        paramString = new ParseException("Failed to parse date [" + paramString + "]: " + (String)localObject3, paramParsePosition.getIndex());
        paramString.initCause(localIndexOutOfBoundsException);
        throw paramString;
        j *= 10;
        continue;
        j *= 100;
      }
      c = paramString.charAt(i);
      if (c != 'Z') {
        break label692;
      }
      Object localObject2 = TIMEZONE_UTC;
      i++;
      do
      {
        do
        {
          localObject3 = new java/util/GregorianCalendar;
          ((GregorianCalendar)localObject3).<init>((TimeZone)localObject2);
          ((Calendar)localObject3).setLenient(false);
          ((Calendar)localObject3).set(1, i6);
          ((Calendar)localObject3).set(2, i7 - 1);
          ((Calendar)localObject3).set(5, i8);
          ((Calendar)localObject3).set(11, j);
          ((Calendar)localObject3).set(12, m);
          ((Calendar)localObject3).set(13, n);
          ((Calendar)localObject3).set(14, i1);
          paramParsePosition.setIndex(i);
          localObject2 = ((Calendar)localObject3).getTime();
          paramString = (String)localObject2;
          break;
          if ((c != '+') && (c != '-')) {
            break label928;
          }
          localObject2 = paramString.substring(i);
          if (((String)localObject2).length() >= 5) {}
          for (;;)
          {
            k = i + ((String)localObject2).length();
            if ((!"+0000".equals(localObject2)) && (!"+00:00".equals(localObject2))) {
              break label790;
            }
            localObject2 = TIMEZONE_UTC;
            i = k;
            break;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject2 = (String)localObject2 + "00";
          }
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          str = "GMT" + (String)localObject2;
          localObject3 = TimeZone.getTimeZone(str);
          localObject4 = ((TimeZone)localObject3).getID();
          i = k;
          localObject2 = localObject3;
        } while (((String)localObject4).equals(str));
        i = k;
        localObject2 = localObject3;
      } while (((String)localObject4).replace(":", "").equals(str));
      Object localObject4 = new java/lang/IndexOutOfBoundsException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((IndexOutOfBoundsException)localObject4).<init>("Mismatching time zone indicator: " + str + " given, resolves to " + ((TimeZone)localObject3).getID());
      throw ((Throwable)localObject4);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      char c;
      for (;;) {}
      Object localObject3 = new java/lang/IndexOutOfBoundsException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      ((IndexOutOfBoundsException)localObject3).<init>("Invalid time zone indicator '" + c + "'");
      throw ((Throwable)localObject3);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        int i2;
        int k;
        int i4;
        int i3;
        label692:
        label790:
        label928:
        continue;
        paramString = '"' + paramString + "'";
        continue;
        label1003:
        int j = i2;
        int i1 = i4;
        int m = i3;
        int n = k;
        continue;
        label1022:
        int i = j;
        continue;
        label1028:
        i = j;
      }
    }
    i2 = i + 2;
    i8 = parseInt(paramString, i, i2);
    j = 0;
    m = 0;
    k = 0;
    i4 = 0;
    bool = checkOffset(paramString, i2, 'T');
    if ((!bool) && (paramString.length() <= i2))
    {
      localObject1 = new java/util/GregorianCalendar;
      ((GregorianCalendar)localObject1).<init>(i6, i7 - 1, i8);
      paramParsePosition.setIndex(i2);
      localObject1 = ((Calendar)localObject1).getTime();
      paramString = (String)localObject1;
      return paramString;
    }
    i1 = i4;
    i = i2;
    n = k;
    if (bool)
    {
      i = i2 + 1;
      j = i + 2;
      i2 = parseInt(paramString, i, j);
      i = j;
      if (checkOffset(paramString, j, ':')) {
        i = j + 1;
      }
      j = i + 2;
      i3 = parseInt(paramString, i, j);
      if (!checkOffset(paramString, j, ':')) {
        break label1022;
      }
      i = j + 1;
      if (paramString.length() <= i) {
        break label1003;
      }
      j = paramString.charAt(i);
      if ((j == 90) || (j == 43) || (j == 45)) {
        break label1003;
      }
      i5 = i + 2;
      i = parseInt(paramString, i, i5);
      k = i;
      if (i > 59)
      {
        k = i;
        if (i < 63) {
          k = 59;
        }
      }
      j = i2;
      i1 = i4;
      m = i3;
      i = i5;
      n = k;
      if (checkOffset(paramString, i5, '.'))
      {
        n = i5 + 1;
        i = indexOfNonDigit(paramString, n + 1);
        m = Math.min(i, n + 3);
        j = parseInt(paramString, n, m);
      }
    }
    switch (m - n)
    {
    default: 
      n = k;
      m = i3;
      i1 = j;
      j = i2;
      if (paramString.length() <= i)
      {
        localObject1 = new java/lang/IllegalArgumentException;
        ((IllegalArgumentException)localObject1).<init>("No time zone indicator");
        throw ((Throwable)localObject1);
      }
      break;
    }
  }
  
  private static int parseInt(String paramString, int paramInt1, int paramInt2)
    throws NumberFormatException
  {
    if ((paramInt1 < 0) || (paramInt2 > paramString.length()) || (paramInt1 > paramInt2)) {
      throw new NumberFormatException(paramString);
    }
    int j = 0;
    int i;
    if (paramInt1 < paramInt2)
    {
      i = paramInt1 + 1;
      j = Character.digit(paramString.charAt(paramInt1), 10);
      if (j < 0) {
        throw new NumberFormatException("Invalid number: " + paramString.substring(paramInt1, paramInt2));
      }
      j = -j;
    }
    for (;;)
    {
      if (i < paramInt2)
      {
        int k = Character.digit(paramString.charAt(i), 10);
        if (k < 0) {
          throw new NumberFormatException("Invalid number: " + paramString.substring(paramInt1, paramInt2));
        }
        j = j * 10 - k;
        i++;
      }
      else
      {
        return -j;
        i = paramInt1;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\google\gson\internal\bind\util\ISO8601Utils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */