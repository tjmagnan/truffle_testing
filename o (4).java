package com.testfairy.o;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.g;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class d
  extends b
{
  public d(com.testfairy.m.b paramb)
  {
    super(paramb);
  }
  
  private String a(Object paramObject)
  {
    Class localClass;
    if (paramObject != null) {
      localClass = paramObject.getClass();
    }
    for (;;)
    {
      try
      {
        paramObject = (String)localClass.getDeclaredMethod("getLabel", null).invoke(paramObject, new Object[0]);
        return (String)paramObject;
      }
      catch (Throwable paramObject)
      {
        Log.w(e.a, (Throwable)paramObject);
      }
      paramObject = null;
    }
  }
  
  public void a(String paramString)
  {
    HashMap localHashMap1 = new HashMap(2);
    HashMap localHashMap2 = new HashMap(1);
    localHashMap2.put("actionName", paramString);
    localHashMap1.put("data", localHashMap2);
    localHashMap1.put("source", "newrelic");
    try
    {
      paramString = new com/testfairy/g;
      paramString.<init>(22, localHashMap1);
      this.a.a(paramString);
      return;
    }
    catch (Exception paramString)
    {
      for (;;) {}
    }
  }
  
  public void a(String paramString1, String paramString2, int paramInt, double paramDouble1, double paramDouble2, Object paramObject1, Object paramObject2)
  {
    paramObject1 = a(paramObject1);
    String str = a(paramObject2);
    paramObject2 = new HashMap(2);
    HashMap localHashMap = new HashMap(7);
    localHashMap.put("name", paramString1);
    localHashMap.put("category", paramString2);
    localHashMap.put("count", Integer.valueOf(paramInt));
    localHashMap.put("totalValue", Double.valueOf(paramDouble1));
    localHashMap.put("exclusiveValue", Double.valueOf(paramDouble2));
    localHashMap.put("countUnit", paramObject1);
    localHashMap.put("valueUnit", str);
    ((Map)paramObject2).put("data", localHashMap);
    ((Map)paramObject2).put("source", "newrelic");
    try
    {
      paramString1 = new com/testfairy/g;
      paramString1.<init>(22, (Map)paramObject2);
      this.a.a(paramString1);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\o\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */