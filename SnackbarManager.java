package android.support.design.widget;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager
{
  private static final int LONG_DURATION_MS = 2750;
  static final int MSG_TIMEOUT = 0;
  private static final int SHORT_DURATION_MS = 1500;
  private static SnackbarManager sSnackbarManager;
  private SnackbarRecord mCurrentSnackbar;
  private final Handler mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback()
  {
    public boolean handleMessage(Message paramAnonymousMessage)
    {
      switch (paramAnonymousMessage.what)
      {
      }
      for (boolean bool = false;; bool = true)
      {
        return bool;
        SnackbarManager.this.handleTimeout((SnackbarManager.SnackbarRecord)paramAnonymousMessage.obj);
      }
    }
  });
  private final Object mLock = new Object();
  private SnackbarRecord mNextSnackbar;
  
  private boolean cancelSnackbarLocked(SnackbarRecord paramSnackbarRecord, int paramInt)
  {
    Callback localCallback = (Callback)paramSnackbarRecord.callback.get();
    if (localCallback != null)
    {
      this.mHandler.removeCallbacksAndMessages(paramSnackbarRecord);
      localCallback.dismiss(paramInt);
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  static SnackbarManager getInstance()
  {
    if (sSnackbarManager == null) {
      sSnackbarManager = new SnackbarManager();
    }
    return sSnackbarManager;
  }
  
  private boolean isCurrentSnackbarLocked(Callback paramCallback)
  {
    if ((this.mCurrentSnackbar != null) && (this.mCurrentSnackbar.isSnackbar(paramCallback))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private boolean isNextSnackbarLocked(Callback paramCallback)
  {
    if ((this.mNextSnackbar != null) && (this.mNextSnackbar.isSnackbar(paramCallback))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private void scheduleTimeoutLocked(SnackbarRecord paramSnackbarRecord)
  {
    if (paramSnackbarRecord.duration == -2) {
      return;
    }
    int i = 2750;
    if (paramSnackbarRecord.duration > 0) {
      i = paramSnackbarRecord.duration;
    }
    for (;;)
    {
      this.mHandler.removeCallbacksAndMessages(paramSnackbarRecord);
      this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 0, paramSnackbarRecord), i);
      break;
      if (paramSnackbarRecord.duration == -1) {
        i = 1500;
      }
    }
  }
  
  private void showNextSnackbarLocked()
  {
    if (this.mNextSnackbar != null)
    {
      this.mCurrentSnackbar = this.mNextSnackbar;
      this.mNextSnackbar = null;
      Callback localCallback = (Callback)this.mCurrentSnackbar.callback.get();
      if (localCallback == null) {
        break label45;
      }
      localCallback.show();
    }
    for (;;)
    {
      return;
      label45:
      this.mCurrentSnackbar = null;
    }
  }
  
  public void dismiss(Callback paramCallback, int paramInt)
  {
    synchronized (this.mLock)
    {
      if (isCurrentSnackbarLocked(paramCallback)) {
        cancelSnackbarLocked(this.mCurrentSnackbar, paramInt);
      }
      while (!isNextSnackbarLocked(paramCallback)) {
        return;
      }
      cancelSnackbarLocked(this.mNextSnackbar, paramInt);
    }
  }
  
  void handleTimeout(SnackbarRecord paramSnackbarRecord)
  {
    synchronized (this.mLock)
    {
      if ((this.mCurrentSnackbar == paramSnackbarRecord) || (this.mNextSnackbar == paramSnackbarRecord)) {
        cancelSnackbarLocked(paramSnackbarRecord, 2);
      }
      return;
    }
  }
  
  public boolean isCurrent(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      boolean bool = isCurrentSnackbarLocked(paramCallback);
      return bool;
    }
  }
  
  public boolean isCurrentOrNext(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      if ((isCurrentSnackbarLocked(paramCallback)) || (isNextSnackbarLocked(paramCallback)))
      {
        bool = true;
        return bool;
      }
      boolean bool = false;
    }
  }
  
  public void onDismissed(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      if (isCurrentSnackbarLocked(paramCallback))
      {
        this.mCurrentSnackbar = null;
        if (this.mNextSnackbar != null) {
          showNextSnackbarLocked();
        }
      }
      return;
    }
  }
  
  public void onShown(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      if (isCurrentSnackbarLocked(paramCallback)) {
        scheduleTimeoutLocked(this.mCurrentSnackbar);
      }
      return;
    }
  }
  
  public void pauseTimeout(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      if ((isCurrentSnackbarLocked(paramCallback)) && (!this.mCurrentSnackbar.paused))
      {
        this.mCurrentSnackbar.paused = true;
        this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
      }
      return;
    }
  }
  
  public void restoreTimeoutIfPaused(Callback paramCallback)
  {
    synchronized (this.mLock)
    {
      if ((isCurrentSnackbarLocked(paramCallback)) && (this.mCurrentSnackbar.paused))
      {
        this.mCurrentSnackbar.paused = false;
        scheduleTimeoutLocked(this.mCurrentSnackbar);
      }
      return;
    }
  }
  
  public void show(int paramInt, Callback paramCallback)
  {
    for (;;)
    {
      synchronized (this.mLock)
      {
        if (isCurrentSnackbarLocked(paramCallback))
        {
          this.mCurrentSnackbar.duration = paramInt;
          this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
          scheduleTimeoutLocked(this.mCurrentSnackbar);
          return;
        }
        if (isNextSnackbarLocked(paramCallback))
        {
          this.mNextSnackbar.duration = paramInt;
          if ((this.mCurrentSnackbar == null) || (!cancelSnackbarLocked(this.mCurrentSnackbar, 4))) {
            break label111;
          }
        }
      }
      SnackbarRecord localSnackbarRecord = new android/support/design/widget/SnackbarManager$SnackbarRecord;
      localSnackbarRecord.<init>(paramInt, paramCallback);
      this.mNextSnackbar = localSnackbarRecord;
      continue;
      label111:
      this.mCurrentSnackbar = null;
      showNextSnackbarLocked();
    }
  }
  
  static abstract interface Callback
  {
    public abstract void dismiss(int paramInt);
    
    public abstract void show();
  }
  
  private static class SnackbarRecord
  {
    final WeakReference<SnackbarManager.Callback> callback;
    int duration;
    boolean paused;
    
    SnackbarRecord(int paramInt, SnackbarManager.Callback paramCallback)
    {
      this.callback = new WeakReference(paramCallback);
      this.duration = paramInt;
    }
    
    boolean isSnackbar(SnackbarManager.Callback paramCallback)
    {
      if ((paramCallback != null) && (this.callback.get() == paramCallback)) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\design\widget\SnackbarManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */