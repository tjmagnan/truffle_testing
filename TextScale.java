package android.support.design.internal;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.support.annotation.RequiresApi;
import android.support.transition.Transition;
import android.support.transition.TransitionValues;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.Map;

@TargetApi(14)
@RequiresApi(14)
public class TextScale
  extends Transition
{
  private static final String PROPNAME_SCALE = "android:textscale:scale";
  
  private void captureValues(TransitionValues paramTransitionValues)
  {
    if ((paramTransitionValues.view instanceof TextView))
    {
      TextView localTextView = (TextView)paramTransitionValues.view;
      paramTransitionValues.values.put("android:textscale:scale", Float.valueOf(localTextView.getScaleX()));
    }
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues)
  {
    captureValues(paramTransitionValues);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues)
  {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2)
  {
    Object localObject = null;
    paramViewGroup = (ViewGroup)localObject;
    if (paramTransitionValues1 != null)
    {
      paramViewGroup = (ViewGroup)localObject;
      if (paramTransitionValues2 != null)
      {
        paramViewGroup = (ViewGroup)localObject;
        if ((paramTransitionValues1.view instanceof TextView))
        {
          if ((paramTransitionValues2.view instanceof TextView)) {
            break label45;
          }
          paramViewGroup = (ViewGroup)localObject;
        }
      }
    }
    return paramViewGroup;
    label45:
    final TextView localTextView = (TextView)paramTransitionValues2.view;
    paramViewGroup = paramTransitionValues1.values;
    paramTransitionValues1 = paramTransitionValues2.values;
    float f1;
    if (paramViewGroup.get("android:textscale:scale") != null)
    {
      f1 = ((Float)paramViewGroup.get("android:textscale:scale")).floatValue();
      label91:
      if (paramTransitionValues1.get("android:textscale:scale") == null) {
        break label169;
      }
    }
    label169:
    for (float f2 = ((Float)paramTransitionValues1.get("android:textscale:scale")).floatValue();; f2 = 1.0F)
    {
      paramViewGroup = (ViewGroup)localObject;
      if (f1 == f2) {
        break;
      }
      paramViewGroup = ValueAnimator.ofFloat(new float[] { f1, f2 });
      paramViewGroup.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
      {
        public void onAnimationUpdate(ValueAnimator paramAnonymousValueAnimator)
        {
          float f = ((Float)paramAnonymousValueAnimator.getAnimatedValue()).floatValue();
          localTextView.setScaleX(f);
          localTextView.setScaleY(f);
        }
      });
      break;
      f1 = 1.0F;
      break label91;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\design\internal\TextScale.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */