package com.crashlytics.android.core;

import android.app.Activity;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.Crash.FatalException;
import io.fabric.sdk.android.services.common.Crash.LoggedException;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.persistence.FileStore;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import io.fabric.sdk.android.services.settings.FeaturesSettingsData;
import io.fabric.sdk.android.services.settings.PromptSettingsData;
import io.fabric.sdk.android.services.settings.SessionSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsController
{
  private static final int ANALYZER_VERSION = 1;
  static final FilenameFilter ANY_SESSION_FILENAME_FILTER;
  private static final String COLLECT_CUSTOM_KEYS = "com.crashlytics.CollectCustomKeys";
  private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  private static final String EVENT_TYPE_CRASH = "crash";
  private static final String EVENT_TYPE_LOGGED = "error";
  static final String FATAL_SESSION_DIR = "fatal-sessions";
  private static final String GENERATOR_FORMAT = "Crashlytics Android SDK/%s";
  private static final String[] INITIAL_SESSION_PART_TAGS = { "SessionUser", "SessionApp", "SessionOS", "SessionDevice" };
  static final String INVALID_CLS_CACHE_DIR = "invalidClsFiles";
  static final Comparator<File> LARGEST_FILE_NAME_FIRST;
  static final int MAX_INVALID_SESSIONS = 4;
  private static final int MAX_LOCAL_LOGGED_EXCEPTIONS = 64;
  static final int MAX_OPEN_SESSIONS = 8;
  static final int MAX_STACK_SIZE = 1024;
  static final String NONFATAL_SESSION_DIR = "nonfatal-sessions";
  static final int NUM_STACK_REPETITIONS_ALLOWED = 10;
  private static final Map<String, String> SEND_AT_CRASHTIME_HEADER;
  static final String SESSION_APP_TAG = "SessionApp";
  static final String SESSION_BEGIN_TAG = "BeginSession";
  static final String SESSION_DEVICE_TAG = "SessionDevice";
  static final String SESSION_EVENT_MISSING_BINARY_IMGS_TAG = "SessionMissingBinaryImages";
  static final String SESSION_FATAL_TAG = "SessionCrash";
  static final FilenameFilter SESSION_FILE_FILTER = new FilenameFilter()
  {
    public boolean accept(File paramAnonymousFile, String paramAnonymousString)
    {
      if ((paramAnonymousString.length() == ".cls".length() + 35) && (paramAnonymousString.endsWith(".cls"))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  };
  private static final Pattern SESSION_FILE_PATTERN;
  private static final int SESSION_ID_LENGTH = 35;
  static final String SESSION_NON_FATAL_TAG = "SessionEvent";
  static final String SESSION_OS_TAG = "SessionOS";
  static final String SESSION_USER_TAG = "SessionUser";
  private static final boolean SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT = false;
  static final Comparator<File> SMALLEST_FILE_NAME_FIRST;
  private final AppData appData;
  private final CrashlyticsBackgroundWorker backgroundWorker;
  private CrashlyticsUncaughtExceptionHandler crashHandler;
  private final CrashlyticsCore crashlyticsCore;
  private final DevicePowerStateListener devicePowerStateListener;
  private final AtomicInteger eventCounter = new AtomicInteger(0);
  private final FileStore fileStore;
  private final ReportUploader.HandlingExceptionCheck handlingExceptionCheck;
  private final HttpRequestFactory httpRequestFactory;
  private final IdManager idManager;
  private final LogFileDirectoryProvider logFileDirectoryProvider;
  private final LogFileManager logFileManager;
  private final PreferenceManager preferenceManager;
  private final ReportUploader.ReportFilesProvider reportFilesProvider;
  private final StackTraceTrimmingStrategy stackTraceTrimmingStrategy;
  private final String unityVersion;
  
  static
  {
    LARGEST_FILE_NAME_FIRST = new Comparator()
    {
      public int compare(File paramAnonymousFile1, File paramAnonymousFile2)
      {
        return paramAnonymousFile2.getName().compareTo(paramAnonymousFile1.getName());
      }
    };
    SMALLEST_FILE_NAME_FIRST = new Comparator()
    {
      public int compare(File paramAnonymousFile1, File paramAnonymousFile2)
      {
        return paramAnonymousFile1.getName().compareTo(paramAnonymousFile2.getName());
      }
    };
    ANY_SESSION_FILENAME_FILTER = new FilenameFilter()
    {
      public boolean accept(File paramAnonymousFile, String paramAnonymousString)
      {
        return CrashlyticsController.SESSION_FILE_PATTERN.matcher(paramAnonymousString).matches();
      }
    };
    SESSION_FILE_PATTERN = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    SEND_AT_CRASHTIME_HEADER = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
  }
  
  CrashlyticsController(CrashlyticsCore paramCrashlyticsCore, CrashlyticsBackgroundWorker paramCrashlyticsBackgroundWorker, HttpRequestFactory paramHttpRequestFactory, IdManager paramIdManager, PreferenceManager paramPreferenceManager, FileStore paramFileStore, AppData paramAppData, UnityVersionProvider paramUnityVersionProvider)
  {
    this.crashlyticsCore = paramCrashlyticsCore;
    this.backgroundWorker = paramCrashlyticsBackgroundWorker;
    this.httpRequestFactory = paramHttpRequestFactory;
    this.idManager = paramIdManager;
    this.preferenceManager = paramPreferenceManager;
    this.fileStore = paramFileStore;
    this.appData = paramAppData;
    this.unityVersion = paramUnityVersionProvider.getUnityVersion();
    paramCrashlyticsCore = paramCrashlyticsCore.getContext();
    this.logFileDirectoryProvider = new LogFileDirectoryProvider(paramFileStore);
    this.logFileManager = new LogFileManager(paramCrashlyticsCore, this.logFileDirectoryProvider);
    this.reportFilesProvider = new ReportUploaderFilesProvider(null);
    this.handlingExceptionCheck = new ReportUploaderHandlingExceptionCheck(null);
    this.devicePowerStateListener = new DevicePowerStateListener(paramCrashlyticsCore);
    this.stackTraceTrimmingStrategy = new MiddleOutFallbackStrategy(1024, new StackTraceTrimmingStrategy[] { new RemoveRepeatsStrategy(10) });
  }
  
  private void closeOpenSessions(File[] paramArrayOfFile, int paramInt1, int paramInt2)
  {
    Fabric.getLogger().d("CrashlyticsCore", "Closing open sessions.");
    while (paramInt1 < paramArrayOfFile.length)
    {
      File localFile = paramArrayOfFile[paramInt1];
      String str = getSessionIdFromSessionFile(localFile);
      Fabric.getLogger().d("CrashlyticsCore", "Closing session: " + str);
      writeSessionPartsToSessionFile(localFile, str, paramInt2);
      paramInt1++;
    }
  }
  
  private void closeWithoutRenamingOrLog(ClsFileOutputStream paramClsFileOutputStream)
  {
    if (paramClsFileOutputStream == null) {}
    for (;;)
    {
      return;
      try
      {
        paramClsFileOutputStream.closeInProgressStream();
      }
      catch (IOException paramClsFileOutputStream)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Error closing session file stream in the presence of an exception", paramClsFileOutputStream);
      }
    }
  }
  
  private static void copyToCodedOutputStream(InputStream paramInputStream, CodedOutputStream paramCodedOutputStream, int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = new byte[paramInt];
    paramInt = 0;
    while (paramInt < arrayOfByte.length)
    {
      int i = paramInputStream.read(arrayOfByte, paramInt, arrayOfByte.length - paramInt);
      if (i < 0) {
        break;
      }
      paramInt += i;
    }
    paramCodedOutputStream.writeRawBytes(arrayOfByte);
  }
  
  private void deleteSessionPartFilesFor(String paramString)
  {
    paramString = listSessionPartFilesFor(paramString);
    int j = paramString.length;
    for (int i = 0; i < j; i++) {
      paramString[i].delete();
    }
  }
  
  private void doCloseSessions(SessionSettingsData paramSessionSettingsData, boolean paramBoolean)
    throws Exception
  {
    int i;
    File[] arrayOfFile;
    if (paramBoolean)
    {
      i = 1;
      trimOpenSessions(i + 8);
      arrayOfFile = listSortedSessionBeginFiles();
      if (arrayOfFile.length > i) {
        break label47;
      }
      Fabric.getLogger().d("CrashlyticsCore", "No open sessions to be closed.");
    }
    for (;;)
    {
      return;
      i = 0;
      break;
      label47:
      writeSessionUser(getSessionIdFromSessionFile(arrayOfFile[i]));
      if (paramSessionSettingsData == null) {
        Fabric.getLogger().d("CrashlyticsCore", "Unable to close session. Settings are not loaded.");
      } else {
        closeOpenSessions(arrayOfFile, i, paramSessionSettingsData.maxCustomExceptionEvents);
      }
    }
  }
  
  private void doOpenSession()
    throws Exception
  {
    Date localDate = new Date();
    String str = new CLSUUID(this.idManager).toString();
    Fabric.getLogger().d("CrashlyticsCore", "Opening a new session with ID " + str);
    writeBeginSession(str, localDate);
    writeSessionApp(str);
    writeSessionOS(str);
    writeSessionDevice(str);
    this.logFileManager.setCurrentSession(str);
  }
  
  /* Error */
  private void doWriteExternalCrashEvent(SessionEventData paramSessionEventData)
    throws IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 10
    //   3: aconst_null
    //   4: astore 8
    //   6: aconst_null
    //   7: astore 11
    //   9: aconst_null
    //   10: astore 9
    //   12: aconst_null
    //   13: astore 6
    //   15: aconst_null
    //   16: astore 7
    //   18: aload 11
    //   20: astore_3
    //   21: aload 10
    //   23: astore 4
    //   25: aload_0
    //   26: invokespecial 485	com/crashlytics/android/core/CrashlyticsController:getPreviousSessionId	()Ljava/lang/String;
    //   29: astore 12
    //   31: aload 12
    //   33: ifnonnull +40 -> 73
    //   36: aload 11
    //   38: astore_3
    //   39: aload 10
    //   41: astore 4
    //   43: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   46: ldc_w 367
    //   49: ldc_w 487
    //   52: aconst_null
    //   53: invokeinterface 408 4 0
    //   58: aconst_null
    //   59: ldc_w 489
    //   62: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   65: aconst_null
    //   66: ldc_w 497
    //   69: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   72: return
    //   73: aload 11
    //   75: astore_3
    //   76: aload 10
    //   78: astore 4
    //   80: aload 12
    //   82: getstatic 507	java/util/Locale:US	Ljava/util/Locale;
    //   85: ldc_w 509
    //   88: iconst_2
    //   89: anewarray 4	java/lang/Object
    //   92: dup
    //   93: iconst_0
    //   94: aload_1
    //   95: getfield 515	com/crashlytics/android/core/internal/models/SessionEventData:signal	Lcom/crashlytics/android/core/internal/models/SignalData;
    //   98: getfield 520	com/crashlytics/android/core/internal/models/SignalData:code	Ljava/lang/String;
    //   101: aastore
    //   102: dup
    //   103: iconst_1
    //   104: aload_1
    //   105: getfield 515	com/crashlytics/android/core/internal/models/SessionEventData:signal	Lcom/crashlytics/android/core/internal/models/SignalData;
    //   108: getfield 523	com/crashlytics/android/core/internal/models/SignalData:name	Ljava/lang/String;
    //   111: aastore
    //   112: invokestatic 527	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   115: invokestatic 530	com/crashlytics/android/core/CrashlyticsController:recordFatalExceptionAnswersEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   118: aload 11
    //   120: astore_3
    //   121: aload 10
    //   123: astore 4
    //   125: aload_1
    //   126: getfield 534	com/crashlytics/android/core/internal/models/SessionEventData:binaryImages	[Lcom/crashlytics/android/core/internal/models/BinaryImageData;
    //   129: ifnull +229 -> 358
    //   132: aload 11
    //   134: astore_3
    //   135: aload 10
    //   137: astore 4
    //   139: aload_1
    //   140: getfield 534	com/crashlytics/android/core/internal/models/SessionEventData:binaryImages	[Lcom/crashlytics/android/core/internal/models/BinaryImageData;
    //   143: arraylength
    //   144: ifle +214 -> 358
    //   147: iconst_1
    //   148: istore_2
    //   149: iload_2
    //   150: ifeq +213 -> 363
    //   153: ldc -128
    //   155: astore 5
    //   157: aload 11
    //   159: astore_3
    //   160: aload 10
    //   162: astore 4
    //   164: aload_0
    //   165: invokevirtual 538	com/crashlytics/android/core/CrashlyticsController:getFilesDir	()Ljava/io/File;
    //   168: astore 13
    //   170: aload 11
    //   172: astore_3
    //   173: aload 10
    //   175: astore 4
    //   177: new 381	java/lang/StringBuilder
    //   180: astore 14
    //   182: aload 11
    //   184: astore_3
    //   185: aload 10
    //   187: astore 4
    //   189: aload 14
    //   191: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   194: aload 11
    //   196: astore_3
    //   197: aload 10
    //   199: astore 4
    //   201: new 399	com/crashlytics/android/core/ClsFileOutputStream
    //   204: dup
    //   205: aload 13
    //   207: aload 14
    //   209: aload 12
    //   211: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: aload 5
    //   216: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   222: invokespecial 541	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   225: astore 5
    //   227: aload 9
    //   229: astore_3
    //   230: aload 6
    //   232: astore 4
    //   234: aload 5
    //   236: invokestatic 545	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   239: astore 6
    //   241: aload 6
    //   243: astore_3
    //   244: aload 6
    //   246: astore 4
    //   248: new 547	com/crashlytics/android/core/MetaDataStore
    //   251: astore 7
    //   253: aload 6
    //   255: astore_3
    //   256: aload 6
    //   258: astore 4
    //   260: aload 7
    //   262: aload_0
    //   263: invokevirtual 538	com/crashlytics/android/core/CrashlyticsController:getFilesDir	()Ljava/io/File;
    //   266: invokespecial 550	com/crashlytics/android/core/MetaDataStore:<init>	(Ljava/io/File;)V
    //   269: aload 6
    //   271: astore_3
    //   272: aload 6
    //   274: astore 4
    //   276: aload 7
    //   278: aload 12
    //   280: invokevirtual 554	com/crashlytics/android/core/MetaDataStore:readKeyData	(Ljava/lang/String;)Ljava/util/Map;
    //   283: astore 8
    //   285: aload 6
    //   287: astore_3
    //   288: aload 6
    //   290: astore 4
    //   292: new 264	com/crashlytics/android/core/LogFileManager
    //   295: astore 7
    //   297: aload 6
    //   299: astore_3
    //   300: aload 6
    //   302: astore 4
    //   304: aload 7
    //   306: aload_0
    //   307: getfield 231	com/crashlytics/android/core/CrashlyticsController:crashlyticsCore	Lcom/crashlytics/android/core/CrashlyticsCore;
    //   310: invokevirtual 257	com/crashlytics/android/core/CrashlyticsCore:getContext	()Landroid/content/Context;
    //   313: aload_0
    //   314: getfield 262	com/crashlytics/android/core/CrashlyticsController:logFileDirectoryProvider	Lcom/crashlytics/android/core/CrashlyticsController$LogFileDirectoryProvider;
    //   317: aload 12
    //   319: invokespecial 557	com/crashlytics/android/core/LogFileManager:<init>	(Landroid/content/Context;Lcom/crashlytics/android/core/LogFileManager$DirectoryProvider;Ljava/lang/String;)V
    //   322: aload 6
    //   324: astore_3
    //   325: aload 6
    //   327: astore 4
    //   329: aload_1
    //   330: aload 7
    //   332: aload 8
    //   334: aload 6
    //   336: invokestatic 563	com/crashlytics/android/core/NativeCrashWriter:writeNativeCrash	(Lcom/crashlytics/android/core/internal/models/SessionEventData;Lcom/crashlytics/android/core/LogFileManager;Ljava/util/Map;Lcom/crashlytics/android/core/CodedOutputStream;)V
    //   339: aload 6
    //   341: ldc_w 489
    //   344: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   347: aload 5
    //   349: ldc_w 497
    //   352: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   355: goto -283 -> 72
    //   358: iconst_0
    //   359: istore_2
    //   360: goto -211 -> 149
    //   363: ldc 125
    //   365: astore 5
    //   367: goto -210 -> 157
    //   370: astore 6
    //   372: aload 8
    //   374: astore_1
    //   375: aload 7
    //   377: astore 5
    //   379: aload 5
    //   381: astore_3
    //   382: aload_1
    //   383: astore 4
    //   385: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   388: ldc_w 367
    //   391: ldc_w 565
    //   394: aload 6
    //   396: invokeinterface 408 4 0
    //   401: aload 5
    //   403: ldc_w 489
    //   406: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   409: aload_1
    //   410: ldc_w 497
    //   413: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   416: goto -344 -> 72
    //   419: astore_1
    //   420: aload_3
    //   421: ldc_w 489
    //   424: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   427: aload 4
    //   429: ldc_w 497
    //   432: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   435: aload_1
    //   436: athrow
    //   437: astore_1
    //   438: aload 5
    //   440: astore 4
    //   442: goto -22 -> 420
    //   445: astore 6
    //   447: aload 5
    //   449: astore_1
    //   450: aload 4
    //   452: astore 5
    //   454: goto -75 -> 379
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	457	0	this	CrashlyticsController
    //   0	457	1	paramSessionEventData	SessionEventData
    //   148	212	2	i	int
    //   20	401	3	localObject1	Object
    //   23	428	4	localObject2	Object
    //   155	298	5	localObject3	Object
    //   13	327	6	localCodedOutputStream	CodedOutputStream
    //   370	25	6	localException1	Exception
    //   445	1	6	localException2	Exception
    //   16	360	7	localObject4	Object
    //   4	369	8	localMap	Map
    //   10	218	9	localObject5	Object
    //   1	197	10	localObject6	Object
    //   7	188	11	localObject7	Object
    //   29	289	12	str	String
    //   168	38	13	localFile	File
    //   180	28	14	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   25	31	370	java/lang/Exception
    //   43	58	370	java/lang/Exception
    //   80	118	370	java/lang/Exception
    //   125	132	370	java/lang/Exception
    //   139	147	370	java/lang/Exception
    //   164	170	370	java/lang/Exception
    //   177	182	370	java/lang/Exception
    //   189	194	370	java/lang/Exception
    //   201	227	370	java/lang/Exception
    //   25	31	419	finally
    //   43	58	419	finally
    //   80	118	419	finally
    //   125	132	419	finally
    //   139	147	419	finally
    //   164	170	419	finally
    //   177	182	419	finally
    //   189	194	419	finally
    //   201	227	419	finally
    //   385	401	419	finally
    //   234	241	437	finally
    //   248	253	437	finally
    //   260	269	437	finally
    //   276	285	437	finally
    //   292	297	437	finally
    //   304	322	437	finally
    //   329	339	437	finally
    //   234	241	445	java/lang/Exception
    //   248	253	445	java/lang/Exception
    //   260	269	445	java/lang/Exception
    //   276	285	445	java/lang/Exception
    //   292	297	445	java/lang/Exception
    //   304	322	445	java/lang/Exception
    //   329	339	445	java/lang/Exception
  }
  
  /* Error */
  private void doWriteNonFatal(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 303	com/crashlytics/android/core/CrashlyticsController:getCurrentSessionId	()Ljava/lang/String;
    //   4: astore 13
    //   6: aload 13
    //   8: ifnonnull +19 -> 27
    //   11: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   14: ldc_w 367
    //   17: ldc_w 567
    //   20: aconst_null
    //   21: invokeinterface 408 4 0
    //   26: return
    //   27: aload 13
    //   29: aload_3
    //   30: invokevirtual 571	java/lang/Object:getClass	()Ljava/lang/Class;
    //   33: invokevirtual 576	java/lang/Class:getName	()Ljava/lang/String;
    //   36: invokestatic 579	com/crashlytics/android/core/CrashlyticsController:recordLoggedExceptionAnswersEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   39: aconst_null
    //   40: astore 10
    //   42: aconst_null
    //   43: astore 8
    //   45: aconst_null
    //   46: astore 11
    //   48: aconst_null
    //   49: astore 12
    //   51: aconst_null
    //   52: astore 9
    //   54: aconst_null
    //   55: astore 7
    //   57: aload 11
    //   59: astore 4
    //   61: aload 10
    //   63: astore 5
    //   65: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   68: astore 6
    //   70: aload 11
    //   72: astore 4
    //   74: aload 10
    //   76: astore 5
    //   78: new 381	java/lang/StringBuilder
    //   81: astore 14
    //   83: aload 11
    //   85: astore 4
    //   87: aload 10
    //   89: astore 5
    //   91: aload 14
    //   93: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   96: aload 11
    //   98: astore 4
    //   100: aload 10
    //   102: astore 5
    //   104: aload 6
    //   106: ldc_w 367
    //   109: aload 14
    //   111: ldc_w 581
    //   114: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: aload_3
    //   118: invokevirtual 584	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   121: ldc_w 586
    //   124: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: aload_2
    //   128: invokevirtual 589	java/lang/Thread:getName	()Ljava/lang/String;
    //   131: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   134: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   137: invokeinterface 375 3 0
    //   142: aload 11
    //   144: astore 4
    //   146: aload 10
    //   148: astore 5
    //   150: aload_0
    //   151: getfield 229	com/crashlytics/android/core/CrashlyticsController:eventCounter	Ljava/util/concurrent/atomic/AtomicInteger;
    //   154: invokevirtual 593	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
    //   157: invokestatic 597	io/fabric/sdk/android/services/common/CommonUtils:padWithZerosToMaxIntWidth	(I)Ljava/lang/String;
    //   160: astore 6
    //   162: aload 11
    //   164: astore 4
    //   166: aload 10
    //   168: astore 5
    //   170: new 381	java/lang/StringBuilder
    //   173: astore 14
    //   175: aload 11
    //   177: astore 4
    //   179: aload 10
    //   181: astore 5
    //   183: aload 14
    //   185: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   188: aload 11
    //   190: astore 4
    //   192: aload 10
    //   194: astore 5
    //   196: aload 14
    //   198: aload 13
    //   200: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: ldc -120
    //   205: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: aload 6
    //   210: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   216: astore 14
    //   218: aload 11
    //   220: astore 4
    //   222: aload 10
    //   224: astore 5
    //   226: new 399	com/crashlytics/android/core/ClsFileOutputStream
    //   229: astore 6
    //   231: aload 11
    //   233: astore 4
    //   235: aload 10
    //   237: astore 5
    //   239: aload 6
    //   241: aload_0
    //   242: invokevirtual 538	com/crashlytics/android/core/CrashlyticsController:getFilesDir	()Ljava/io/File;
    //   245: aload 14
    //   247: invokespecial 541	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   250: aload 12
    //   252: astore 4
    //   254: aload 9
    //   256: astore 5
    //   258: aload 6
    //   260: invokestatic 545	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   263: astore 7
    //   265: aload 7
    //   267: astore 4
    //   269: aload 7
    //   271: astore 5
    //   273: aload_0
    //   274: aload 7
    //   276: aload_1
    //   277: aload_2
    //   278: aload_3
    //   279: ldc 83
    //   281: iconst_0
    //   282: invokespecial 601	com/crashlytics/android/core/CrashlyticsController:writeSessionEvent	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   285: aload 7
    //   287: ldc_w 603
    //   290: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   293: aload 6
    //   295: ldc_w 605
    //   298: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   301: aload_0
    //   302: aload 13
    //   304: bipush 64
    //   306: invokespecial 609	com/crashlytics/android/core/CrashlyticsController:trimSessionEventFiles	(Ljava/lang/String;I)V
    //   309: goto -283 -> 26
    //   312: astore_1
    //   313: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   316: ldc_w 367
    //   319: ldc_w 611
    //   322: aload_1
    //   323: invokeinterface 408 4 0
    //   328: goto -302 -> 26
    //   331: astore_3
    //   332: aload 8
    //   334: astore_1
    //   335: aload 7
    //   337: astore_2
    //   338: aload_2
    //   339: astore 4
    //   341: aload_1
    //   342: astore 5
    //   344: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   347: ldc_w 367
    //   350: ldc_w 613
    //   353: aload_3
    //   354: invokeinterface 408 4 0
    //   359: aload_2
    //   360: ldc_w 603
    //   363: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   366: aload_1
    //   367: ldc_w 605
    //   370: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   373: goto -72 -> 301
    //   376: astore_1
    //   377: aload 4
    //   379: ldc_w 603
    //   382: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   385: aload 5
    //   387: ldc_w 605
    //   390: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   393: aload_1
    //   394: athrow
    //   395: astore_1
    //   396: aload 6
    //   398: astore 5
    //   400: goto -23 -> 377
    //   403: astore_3
    //   404: aload 6
    //   406: astore_1
    //   407: aload 5
    //   409: astore_2
    //   410: goto -72 -> 338
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	413	0	this	CrashlyticsController
    //   0	413	1	paramDate	Date
    //   0	413	2	paramThread	Thread
    //   0	413	3	paramThrowable	Throwable
    //   59	319	4	localObject1	Object
    //   63	345	5	localObject2	Object
    //   68	337	6	localObject3	Object
    //   55	281	7	localCodedOutputStream	CodedOutputStream
    //   43	290	8	localObject4	Object
    //   52	203	9	localObject5	Object
    //   40	196	10	localObject6	Object
    //   46	186	11	localObject7	Object
    //   49	202	12	localObject8	Object
    //   4	299	13	str	String
    //   81	165	14	localObject9	Object
    // Exception table:
    //   from	to	target	type
    //   301	309	312	java/lang/Exception
    //   65	70	331	java/lang/Exception
    //   78	83	331	java/lang/Exception
    //   91	96	331	java/lang/Exception
    //   104	142	331	java/lang/Exception
    //   150	162	331	java/lang/Exception
    //   170	175	331	java/lang/Exception
    //   183	188	331	java/lang/Exception
    //   196	218	331	java/lang/Exception
    //   226	231	331	java/lang/Exception
    //   239	250	331	java/lang/Exception
    //   65	70	376	finally
    //   78	83	376	finally
    //   91	96	376	finally
    //   104	142	376	finally
    //   150	162	376	finally
    //   170	175	376	finally
    //   183	188	376	finally
    //   196	218	376	finally
    //   226	231	376	finally
    //   239	250	376	finally
    //   344	359	376	finally
    //   258	265	395	finally
    //   273	285	395	finally
    //   258	265	403	java/lang/Exception
    //   273	285	403	java/lang/Exception
  }
  
  private File[] ensureFileArrayNotNull(File[] paramArrayOfFile)
  {
    File[] arrayOfFile = paramArrayOfFile;
    if (paramArrayOfFile == null) {
      arrayOfFile = new File[0];
    }
    return arrayOfFile;
  }
  
  private CreateReportSpiCall getCreateReportSpiCall(String paramString)
  {
    String str = CommonUtils.getStringsFileValue(this.crashlyticsCore.getContext(), "com.crashlytics.ApiEndpoint");
    return new DefaultCreateReportSpiCall(this.crashlyticsCore, str, paramString, this.httpRequestFactory);
  }
  
  private String getCurrentSessionId()
  {
    Object localObject = listSortedSessionBeginFiles();
    if (localObject.length > 0) {}
    for (localObject = getSessionIdFromSessionFile(localObject[0]);; localObject = null) {
      return (String)localObject;
    }
  }
  
  private String getPreviousSessionId()
  {
    Object localObject = listSortedSessionBeginFiles();
    if (localObject.length > 1) {}
    for (localObject = getSessionIdFromSessionFile(localObject[1]);; localObject = null) {
      return (String)localObject;
    }
  }
  
  static String getSessionIdFromSessionFile(File paramFile)
  {
    return paramFile.getName().substring(0, 35);
  }
  
  private File[] getTrimmedNonFatalFiles(String paramString, File[] paramArrayOfFile, int paramInt)
  {
    File[] arrayOfFile = paramArrayOfFile;
    if (paramArrayOfFile.length > paramInt)
    {
      Fabric.getLogger().d("CrashlyticsCore", String.format(Locale.US, "Trimming down to %d logged exceptions.", new Object[] { Integer.valueOf(paramInt) }));
      trimSessionEventFiles(paramString, paramInt);
      arrayOfFile = listFilesMatching(new FileNameContainsFilter(paramString + "SessionEvent"));
    }
    return arrayOfFile;
  }
  
  private UserMetaData getUserMetaData(String paramString)
  {
    if (isHandlingException()) {}
    for (paramString = new UserMetaData(this.crashlyticsCore.getUserIdentifier(), this.crashlyticsCore.getUserName(), this.crashlyticsCore.getUserEmail());; paramString = new MetaDataStore(getFilesDir()).readUserData(paramString)) {
      return paramString;
    }
  }
  
  private File[] listFiles(File paramFile)
  {
    return ensureFileArrayNotNull(paramFile.listFiles());
  }
  
  private File[] listFilesMatching(File paramFile, FilenameFilter paramFilenameFilter)
  {
    return ensureFileArrayNotNull(paramFile.listFiles(paramFilenameFilter));
  }
  
  private File[] listFilesMatching(FilenameFilter paramFilenameFilter)
  {
    return listFilesMatching(getFilesDir(), paramFilenameFilter);
  }
  
  private File[] listSessionPartFilesFor(String paramString)
  {
    return listFilesMatching(new SessionPartFileFilter(paramString));
  }
  
  private File[] listSortedSessionBeginFiles()
  {
    File[] arrayOfFile = listSessionBeginFiles();
    Arrays.sort(arrayOfFile, LARGEST_FILE_NAME_FIRST);
    return arrayOfFile;
  }
  
  private static void recordFatalExceptionAnswersEvent(String paramString1, String paramString2)
  {
    Answers localAnswers = (Answers)Fabric.getKit(Answers.class);
    if (localAnswers == null) {
      Fabric.getLogger().d("CrashlyticsCore", "Answers is not available");
    }
    for (;;)
    {
      return;
      localAnswers.onException(new Crash.FatalException(paramString1, paramString2));
    }
  }
  
  private static void recordLoggedExceptionAnswersEvent(String paramString1, String paramString2)
  {
    Answers localAnswers = (Answers)Fabric.getKit(Answers.class);
    if (localAnswers == null) {
      Fabric.getLogger().d("CrashlyticsCore", "Answers is not available");
    }
    for (;;)
    {
      return;
      localAnswers.onException(new Crash.LoggedException(paramString1, paramString2));
    }
  }
  
  private void retainSessions(File[] paramArrayOfFile, Set<String> paramSet)
  {
    int j = paramArrayOfFile.length;
    for (int i = 0;; i++)
    {
      File localFile;
      String str;
      Matcher localMatcher;
      if (i < j)
      {
        localFile = paramArrayOfFile[i];
        str = localFile.getName();
        localMatcher = SESSION_FILE_PATTERN.matcher(str);
        if (!localMatcher.matches())
        {
          Fabric.getLogger().d("CrashlyticsCore", "Deleting unknown file: " + str);
          localFile.delete();
        }
      }
      else
      {
        return;
      }
      if (!paramSet.contains(localMatcher.group(1)))
      {
        Fabric.getLogger().d("CrashlyticsCore", "Trimming session file: " + str);
        localFile.delete();
      }
    }
  }
  
  private void sendSessionReports(SettingsData paramSettingsData)
  {
    if (paramSettingsData == null) {
      Fabric.getLogger().w("CrashlyticsCore", "Cannot send reports. Settings are unavailable.");
    }
    for (;;)
    {
      return;
      Context localContext = this.crashlyticsCore.getContext();
      paramSettingsData = getCreateReportSpiCall(paramSettingsData.appData.reportsUrl);
      ReportUploader localReportUploader = new ReportUploader(this.appData.apiKey, paramSettingsData, this.reportFilesProvider, this.handlingExceptionCheck);
      paramSettingsData = listCompleteSessionFiles();
      int j = paramSettingsData.length;
      for (int i = 0; i < j; i++)
      {
        SessionReport localSessionReport = new SessionReport(paramSettingsData[i], SEND_AT_CRASHTIME_HEADER);
        this.backgroundWorker.submit(new SendReportRunnable(localContext, localSessionReport, localReportUploader));
      }
    }
  }
  
  private boolean shouldPromptUserBeforeSendingCrashReports(SettingsData paramSettingsData)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramSettingsData == null) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (paramSettingsData.featuresData.promptEnabled)
      {
        bool1 = bool2;
        if (!this.preferenceManager.shouldAlwaysSendReports()) {
          bool1 = true;
        }
      }
    }
  }
  
  /* Error */
  private void synthesizeSessionFile(File paramFile1, String paramString, File[] paramArrayOfFile, File paramFile2)
  {
    // Byte code:
    //   0: aload 4
    //   2: ifnull +342 -> 344
    //   5: iconst_1
    //   6: istore 5
    //   8: iload 5
    //   10: ifeq +340 -> 350
    //   13: aload_0
    //   14: invokevirtual 797	com/crashlytics/android/core/CrashlyticsController:getFatalSessionFilesDir	()Ljava/io/File;
    //   17: astore 9
    //   19: aload 9
    //   21: invokevirtual 800	java/io/File:exists	()Z
    //   24: ifne +9 -> 33
    //   27: aload 9
    //   29: invokevirtual 803	java/io/File:mkdirs	()Z
    //   32: pop
    //   33: aconst_null
    //   34: astore 13
    //   36: aconst_null
    //   37: astore 11
    //   39: aconst_null
    //   40: astore 14
    //   42: aconst_null
    //   43: astore 15
    //   45: aconst_null
    //   46: astore 12
    //   48: aconst_null
    //   49: astore 10
    //   51: aload 14
    //   53: astore 6
    //   55: aload 13
    //   57: astore 7
    //   59: new 399	com/crashlytics/android/core/ClsFileOutputStream
    //   62: astore 8
    //   64: aload 14
    //   66: astore 6
    //   68: aload 13
    //   70: astore 7
    //   72: aload 8
    //   74: aload 9
    //   76: aload_2
    //   77: invokespecial 541	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   80: aload 15
    //   82: astore 6
    //   84: aload 12
    //   86: astore 7
    //   88: aload 8
    //   90: invokestatic 545	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   93: astore 9
    //   95: aload 9
    //   97: astore 6
    //   99: aload 9
    //   101: astore 7
    //   103: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   106: astore 10
    //   108: aload 9
    //   110: astore 6
    //   112: aload 9
    //   114: astore 7
    //   116: new 381	java/lang/StringBuilder
    //   119: astore 11
    //   121: aload 9
    //   123: astore 6
    //   125: aload 9
    //   127: astore 7
    //   129: aload 11
    //   131: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   134: aload 9
    //   136: astore 6
    //   138: aload 9
    //   140: astore 7
    //   142: aload 10
    //   144: ldc_w 367
    //   147: aload 11
    //   149: ldc_w 805
    //   152: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: aload_2
    //   156: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   162: invokeinterface 375 3 0
    //   167: aload 9
    //   169: astore 6
    //   171: aload 9
    //   173: astore 7
    //   175: aload 9
    //   177: aload_1
    //   178: invokestatic 809	com/crashlytics/android/core/CrashlyticsController:writeToCosFromFile	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/io/File;)V
    //   181: aload 9
    //   183: astore 6
    //   185: aload 9
    //   187: astore 7
    //   189: new 457	java/util/Date
    //   192: astore_1
    //   193: aload 9
    //   195: astore 6
    //   197: aload 9
    //   199: astore 7
    //   201: aload_1
    //   202: invokespecial 458	java/util/Date:<init>	()V
    //   205: aload 9
    //   207: astore 6
    //   209: aload 9
    //   211: astore 7
    //   213: aload 9
    //   215: iconst_4
    //   216: aload_1
    //   217: invokevirtual 813	java/util/Date:getTime	()J
    //   220: ldc2_w 814
    //   223: ldiv
    //   224: invokevirtual 819	com/crashlytics/android/core/CodedOutputStream:writeUInt64	(IJ)V
    //   227: aload 9
    //   229: astore 6
    //   231: aload 9
    //   233: astore 7
    //   235: aload 9
    //   237: iconst_5
    //   238: iload 5
    //   240: invokevirtual 823	com/crashlytics/android/core/CodedOutputStream:writeBool	(IZ)V
    //   243: aload 9
    //   245: astore 6
    //   247: aload 9
    //   249: astore 7
    //   251: aload 9
    //   253: bipush 11
    //   255: iconst_1
    //   256: invokevirtual 827	com/crashlytics/android/core/CodedOutputStream:writeUInt32	(II)V
    //   259: aload 9
    //   261: astore 6
    //   263: aload 9
    //   265: astore 7
    //   267: aload 9
    //   269: bipush 12
    //   271: iconst_3
    //   272: invokevirtual 830	com/crashlytics/android/core/CodedOutputStream:writeEnum	(II)V
    //   275: aload 9
    //   277: astore 6
    //   279: aload 9
    //   281: astore 7
    //   283: aload_0
    //   284: aload 9
    //   286: aload_2
    //   287: invokespecial 834	com/crashlytics/android/core/CrashlyticsController:writeInitialPartsTo	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/lang/String;)V
    //   290: aload 9
    //   292: astore 6
    //   294: aload 9
    //   296: astore 7
    //   298: aload 9
    //   300: aload_3
    //   301: aload_2
    //   302: invokestatic 838	com/crashlytics/android/core/CrashlyticsController:writeNonFatalEventsTo	(Lcom/crashlytics/android/core/CodedOutputStream;[Ljava/io/File;Ljava/lang/String;)V
    //   305: iload 5
    //   307: ifeq +18 -> 325
    //   310: aload 9
    //   312: astore 6
    //   314: aload 9
    //   316: astore 7
    //   318: aload 9
    //   320: aload 4
    //   322: invokestatic 809	com/crashlytics/android/core/CrashlyticsController:writeToCosFromFile	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/io/File;)V
    //   325: aload 9
    //   327: ldc_w 840
    //   330: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   333: iconst_0
    //   334: ifeq +25 -> 359
    //   337: aload_0
    //   338: aload 8
    //   340: invokespecial 842	com/crashlytics/android/core/CrashlyticsController:closeWithoutRenamingOrLog	(Lcom/crashlytics/android/core/ClsFileOutputStream;)V
    //   343: return
    //   344: iconst_0
    //   345: istore 5
    //   347: goto -339 -> 8
    //   350: aload_0
    //   351: invokevirtual 845	com/crashlytics/android/core/CrashlyticsController:getNonFatalSessionFilesDir	()Ljava/io/File;
    //   354: astore 9
    //   356: goto -337 -> 19
    //   359: aload 8
    //   361: ldc_w 847
    //   364: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   367: goto -24 -> 343
    //   370: astore 4
    //   372: aload 11
    //   374: astore_3
    //   375: aload 10
    //   377: astore_1
    //   378: aload_1
    //   379: astore 6
    //   381: aload_3
    //   382: astore 7
    //   384: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   387: astore 8
    //   389: aload_1
    //   390: astore 6
    //   392: aload_3
    //   393: astore 7
    //   395: new 381	java/lang/StringBuilder
    //   398: astore 9
    //   400: aload_1
    //   401: astore 6
    //   403: aload_3
    //   404: astore 7
    //   406: aload 9
    //   408: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   411: aload_1
    //   412: astore 6
    //   414: aload_3
    //   415: astore 7
    //   417: aload 8
    //   419: ldc_w 367
    //   422: aload 9
    //   424: ldc_w 849
    //   427: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   430: aload_2
    //   431: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   434: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   437: aload 4
    //   439: invokeinterface 408 4 0
    //   444: aload_1
    //   445: ldc_w 840
    //   448: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   451: iconst_1
    //   452: ifeq +11 -> 463
    //   455: aload_0
    //   456: aload_3
    //   457: invokespecial 842	com/crashlytics/android/core/CrashlyticsController:closeWithoutRenamingOrLog	(Lcom/crashlytics/android/core/ClsFileOutputStream;)V
    //   460: goto -117 -> 343
    //   463: aload_3
    //   464: ldc_w 847
    //   467: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   470: goto -127 -> 343
    //   473: astore_1
    //   474: aload 6
    //   476: ldc_w 840
    //   479: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   482: iconst_0
    //   483: ifeq +11 -> 494
    //   486: aload_0
    //   487: aload 7
    //   489: invokespecial 842	com/crashlytics/android/core/CrashlyticsController:closeWithoutRenamingOrLog	(Lcom/crashlytics/android/core/ClsFileOutputStream;)V
    //   492: aload_1
    //   493: athrow
    //   494: aload 7
    //   496: ldc_w 847
    //   499: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   502: goto -10 -> 492
    //   505: astore_1
    //   506: aload 8
    //   508: astore 7
    //   510: goto -36 -> 474
    //   513: astore 4
    //   515: aload 8
    //   517: astore_3
    //   518: aload 7
    //   520: astore_1
    //   521: goto -143 -> 378
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	524	0	this	CrashlyticsController
    //   0	524	1	paramFile1	File
    //   0	524	2	paramString	String
    //   0	524	3	paramArrayOfFile	File[]
    //   0	524	4	paramFile2	File
    //   6	340	5	bool	boolean
    //   53	422	6	localObject1	Object
    //   57	462	7	localObject2	Object
    //   62	454	8	localObject3	Object
    //   17	406	9	localObject4	Object
    //   49	327	10	localLogger	Logger
    //   37	336	11	localStringBuilder	StringBuilder
    //   46	39	12	localObject5	Object
    //   34	35	13	localObject6	Object
    //   40	25	14	localObject7	Object
    //   43	38	15	localObject8	Object
    // Exception table:
    //   from	to	target	type
    //   59	64	370	java/lang/Exception
    //   72	80	370	java/lang/Exception
    //   59	64	473	finally
    //   72	80	473	finally
    //   384	389	473	finally
    //   395	400	473	finally
    //   406	411	473	finally
    //   417	444	473	finally
    //   88	95	505	finally
    //   103	108	505	finally
    //   116	121	505	finally
    //   129	134	505	finally
    //   142	167	505	finally
    //   175	181	505	finally
    //   189	193	505	finally
    //   201	205	505	finally
    //   213	227	505	finally
    //   235	243	505	finally
    //   251	259	505	finally
    //   267	275	505	finally
    //   283	290	505	finally
    //   298	305	505	finally
    //   318	325	505	finally
    //   88	95	513	java/lang/Exception
    //   103	108	513	java/lang/Exception
    //   116	121	513	java/lang/Exception
    //   129	134	513	java/lang/Exception
    //   142	167	513	java/lang/Exception
    //   175	181	513	java/lang/Exception
    //   189	193	513	java/lang/Exception
    //   201	205	513	java/lang/Exception
    //   213	227	513	java/lang/Exception
    //   235	243	513	java/lang/Exception
    //   251	259	513	java/lang/Exception
    //   267	275	513	java/lang/Exception
    //   283	290	513	java/lang/Exception
    //   298	305	513	java/lang/Exception
    //   318	325	513	java/lang/Exception
  }
  
  private void trimInvalidSessionFiles()
  {
    File localFile = getInvalidFilesDir();
    if (!localFile.exists()) {}
    for (;;)
    {
      return;
      File[] arrayOfFile = listFilesMatching(localFile, new InvalidPartFileFilter());
      Arrays.sort(arrayOfFile, Collections.reverseOrder());
      HashSet localHashSet = new HashSet();
      for (int i = 0; (i < arrayOfFile.length) && (localHashSet.size() < 4); i++) {
        localHashSet.add(getSessionIdFromSessionFile(arrayOfFile[i]));
      }
      retainSessions(listFiles(localFile), localHashSet);
    }
  }
  
  private void trimOpenSessions(int paramInt)
  {
    HashSet localHashSet = new HashSet();
    File[] arrayOfFile = listSortedSessionBeginFiles();
    int i = Math.min(paramInt, arrayOfFile.length);
    for (paramInt = 0; paramInt < i; paramInt++) {
      localHashSet.add(getSessionIdFromSessionFile(arrayOfFile[paramInt]));
    }
    this.logFileManager.discardOldLogFiles(localHashSet);
    retainSessions(listFilesMatching(new AnySessionPartFileFilter(null)), localHashSet);
  }
  
  private void trimSessionEventFiles(String paramString, int paramInt)
  {
    Utils.capFileCount(getFilesDir(), new FileNameContainsFilter(paramString + "SessionEvent"), paramInt, SMALLEST_FILE_NAME_FIRST);
  }
  
  private void writeBeginSession(String paramString, Date paramDate)
    throws Exception
  {
    CodedOutputStream localCodedOutputStream = null;
    Object localObject2 = null;
    Object localObject1 = null;
    try
    {
      localClsFileOutputStream = new com/crashlytics/android/core/ClsFileOutputStream;
      File localFile = getFilesDir();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localClsFileOutputStream.<init>(localFile, paramString + "BeginSession");
      localObject1 = localObject2;
      CommonUtils.flushOrLog((Flushable)localObject1, "Failed to flush to session begin file.");
    }
    finally
    {
      try
      {
        localCodedOutputStream = CodedOutputStream.newInstance(localClsFileOutputStream);
        localObject1 = localCodedOutputStream;
        SessionProtobufHelper.writeBeginSession(localCodedOutputStream, paramString, String.format(Locale.US, "Crashlytics Android SDK/%s", new Object[] { this.crashlyticsCore.getVersion() }), paramDate.getTime() / 1000L);
        CommonUtils.flushOrLog(localCodedOutputStream, "Failed to flush to session begin file.");
        CommonUtils.closeOrLog(localClsFileOutputStream, "Failed to close begin session file.");
        return;
      }
      finally
      {
        for (;;)
        {
          ClsFileOutputStream localClsFileOutputStream;
          paramDate = localClsFileOutputStream;
        }
      }
      paramString = finally;
      paramDate = localCodedOutputStream;
    }
    CommonUtils.closeOrLog(paramDate, "Failed to close begin session file.");
    throw paramString;
  }
  
  /* Error */
  private void writeFatal(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: aconst_null
    //   4: astore 8
    //   6: aconst_null
    //   7: astore 11
    //   9: aconst_null
    //   10: astore 10
    //   12: aconst_null
    //   13: astore 9
    //   15: aconst_null
    //   16: astore 7
    //   18: aload 11
    //   20: astore 4
    //   22: aload 6
    //   24: astore 5
    //   26: aload_0
    //   27: invokespecial 303	com/crashlytics/android/core/CrashlyticsController:getCurrentSessionId	()Ljava/lang/String;
    //   30: astore 13
    //   32: aload 13
    //   34: ifnonnull +41 -> 75
    //   37: aload 11
    //   39: astore 4
    //   41: aload 6
    //   43: astore 5
    //   45: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   48: ldc_w 367
    //   51: ldc_w 902
    //   54: aconst_null
    //   55: invokeinterface 408 4 0
    //   60: aconst_null
    //   61: ldc_w 489
    //   64: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   67: aconst_null
    //   68: ldc_w 497
    //   71: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   74: return
    //   75: aload 11
    //   77: astore 4
    //   79: aload 6
    //   81: astore 5
    //   83: aload 13
    //   85: aload_3
    //   86: invokevirtual 571	java/lang/Object:getClass	()Ljava/lang/Class;
    //   89: invokevirtual 576	java/lang/Class:getName	()Ljava/lang/String;
    //   92: invokestatic 530	com/crashlytics/android/core/CrashlyticsController:recordFatalExceptionAnswersEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   95: aload 11
    //   97: astore 4
    //   99: aload 6
    //   101: astore 5
    //   103: aload_0
    //   104: invokevirtual 538	com/crashlytics/android/core/CrashlyticsController:getFilesDir	()Ljava/io/File;
    //   107: astore 12
    //   109: aload 11
    //   111: astore 4
    //   113: aload 6
    //   115: astore 5
    //   117: new 381	java/lang/StringBuilder
    //   120: astore 14
    //   122: aload 11
    //   124: astore 4
    //   126: aload 6
    //   128: astore 5
    //   130: aload 14
    //   132: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   135: aload 11
    //   137: astore 4
    //   139: aload 6
    //   141: astore 5
    //   143: new 399	com/crashlytics/android/core/ClsFileOutputStream
    //   146: dup
    //   147: aload 12
    //   149: aload 14
    //   151: aload 13
    //   153: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: ldc -128
    //   158: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   161: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   164: invokespecial 541	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   167: astore 6
    //   169: aload 10
    //   171: astore 4
    //   173: aload 9
    //   175: astore 5
    //   177: aload 6
    //   179: invokestatic 545	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   182: astore 7
    //   184: aload 7
    //   186: astore 4
    //   188: aload 7
    //   190: astore 5
    //   192: aload_0
    //   193: aload 7
    //   195: aload_1
    //   196: aload_2
    //   197: aload_3
    //   198: ldc 80
    //   200: iconst_1
    //   201: invokespecial 601	com/crashlytics/android/core/CrashlyticsController:writeSessionEvent	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   204: aload 7
    //   206: ldc_w 489
    //   209: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   212: aload 6
    //   214: ldc_w 497
    //   217: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   220: goto -146 -> 74
    //   223: astore_3
    //   224: aload 8
    //   226: astore_2
    //   227: aload 7
    //   229: astore_1
    //   230: aload_1
    //   231: astore 4
    //   233: aload_2
    //   234: astore 5
    //   236: invokestatic 365	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   239: ldc_w 367
    //   242: ldc_w 904
    //   245: aload_3
    //   246: invokeinterface 408 4 0
    //   251: aload_1
    //   252: ldc_w 489
    //   255: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   258: aload_2
    //   259: ldc_w 497
    //   262: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   265: goto -191 -> 74
    //   268: astore_1
    //   269: aload 4
    //   271: ldc_w 489
    //   274: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   277: aload 5
    //   279: ldc_w 497
    //   282: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   285: aload_1
    //   286: athrow
    //   287: astore_1
    //   288: aload 6
    //   290: astore 5
    //   292: goto -23 -> 269
    //   295: astore_3
    //   296: aload 6
    //   298: astore_2
    //   299: aload 5
    //   301: astore_1
    //   302: goto -72 -> 230
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	305	0	this	CrashlyticsController
    //   0	305	1	paramDate	Date
    //   0	305	2	paramThread	Thread
    //   0	305	3	paramThrowable	Throwable
    //   20	250	4	localObject1	Object
    //   24	276	5	localObject2	Object
    //   1	296	6	localClsFileOutputStream	ClsFileOutputStream
    //   16	212	7	localCodedOutputStream	CodedOutputStream
    //   4	221	8	localObject3	Object
    //   13	161	9	localObject4	Object
    //   10	160	10	localObject5	Object
    //   7	129	11	localObject6	Object
    //   107	41	12	localFile	File
    //   30	122	13	str	String
    //   120	30	14	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   26	32	223	java/lang/Exception
    //   45	60	223	java/lang/Exception
    //   83	95	223	java/lang/Exception
    //   103	109	223	java/lang/Exception
    //   117	122	223	java/lang/Exception
    //   130	135	223	java/lang/Exception
    //   143	169	223	java/lang/Exception
    //   26	32	268	finally
    //   45	60	268	finally
    //   83	95	268	finally
    //   103	109	268	finally
    //   117	122	268	finally
    //   130	135	268	finally
    //   143	169	268	finally
    //   236	251	268	finally
    //   177	184	287	finally
    //   192	204	287	finally
    //   177	184	295	java/lang/Exception
    //   192	204	295	java/lang/Exception
  }
  
  private void writeInitialPartsTo(CodedOutputStream paramCodedOutputStream, String paramString)
    throws IOException
  {
    String[] arrayOfString = INITIAL_SESSION_PART_TAGS;
    int j = arrayOfString.length;
    int i = 0;
    if (i < j)
    {
      String str = arrayOfString[i];
      File[] arrayOfFile = listFilesMatching(new FileNameContainsFilter(paramString + str));
      if (arrayOfFile.length == 0) {
        Fabric.getLogger().e("CrashlyticsCore", "Can't find " + str + " data for session ID " + paramString, null);
      }
      for (;;)
      {
        i++;
        break;
        Fabric.getLogger().d("CrashlyticsCore", "Collecting " + str + " data for session ID " + paramString);
        writeToCosFromFile(paramCodedOutputStream, arrayOfFile[0]);
      }
    }
  }
  
  private static void writeNonFatalEventsTo(CodedOutputStream paramCodedOutputStream, File[] paramArrayOfFile, String paramString)
  {
    int i = 0;
    Arrays.sort(paramArrayOfFile, CommonUtils.FILE_MODIFIED_COMPARATOR);
    int j = paramArrayOfFile.length;
    for (;;)
    {
      if (i < j)
      {
        File localFile = paramArrayOfFile[i];
        try
        {
          Fabric.getLogger().d("CrashlyticsCore", String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", new Object[] { paramString, localFile.getName() }));
          writeToCosFromFile(paramCodedOutputStream, localFile);
          i++;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Fabric.getLogger().e("CrashlyticsCore", "Error writting non-fatal to session.", localException);
          }
        }
      }
    }
  }
  
  private void writeSessionApp(String paramString)
    throws Exception
  {
    String str1 = null;
    String str2 = null;
    localObject1 = null;
    try
    {
      localObject2 = new com/crashlytics/android/core/ClsFileOutputStream;
      Object localObject4 = getFilesDir();
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      ((ClsFileOutputStream)localObject2).<init>((File)localObject4, paramString + "SessionApp");
      paramString = str2;
      int i;
      CommonUtils.flushOrLog((Flushable)localObject1, "Failed to flush to session app file.");
    }
    finally
    {
      try
      {
        localObject1 = CodedOutputStream.newInstance((OutputStream)localObject2);
        paramString = (String)localObject1;
        str1 = this.idManager.getAppIdentifier();
        paramString = (String)localObject1;
        localObject5 = this.appData.versionCode;
        paramString = (String)localObject1;
        str2 = this.appData.versionName;
        paramString = (String)localObject1;
        localObject4 = this.idManager.getAppInstallIdentifier();
        paramString = (String)localObject1;
        i = DeliveryMechanism.determineFrom(this.appData.installerPackageName).getId();
        paramString = (String)localObject1;
        SessionProtobufHelper.writeSessionApp((CodedOutputStream)localObject1, str1, this.appData.apiKey, (String)localObject5, str2, (String)localObject4, i, this.unityVersion);
        CommonUtils.flushOrLog((Flushable)localObject1, "Failed to flush to session app file.");
        CommonUtils.closeOrLog((Closeable)localObject2, "Failed to close session app file.");
        return;
      }
      finally
      {
        for (;;)
        {
          Object localObject2;
          localObject1 = paramString;
          paramString = (String)localObject3;
        }
      }
      paramString = finally;
      localObject2 = str1;
    }
    CommonUtils.closeOrLog((Closeable)localObject2, "Failed to close session app file.");
    throw paramString;
  }
  
  private void writeSessionDevice(String paramString)
    throws Exception
  {
    String str = null;
    CodedOutputStream localCodedOutputStream = null;
    Context localContext = null;
    try
    {
      localObject1 = new com/crashlytics/android/core/ClsFileOutputStream;
      Object localObject4 = getFilesDir();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      ((ClsFileOutputStream)localObject1).<init>((File)localObject4, paramString + "SessionDevice");
      paramString = localCodedOutputStream;
      int j;
      int i;
      long l1;
      long l2;
      long l3;
      boolean bool;
      int k;
      CommonUtils.flushOrLog(paramString, "Failed to flush session device info.");
    }
    finally
    {
      try
      {
        localCodedOutputStream = CodedOutputStream.newInstance((OutputStream)localObject1);
        paramString = localCodedOutputStream;
        localContext = this.crashlyticsCore.getContext();
        paramString = localCodedOutputStream;
        localObject4 = new android/os/StatFs;
        paramString = localCodedOutputStream;
        ((StatFs)localObject4).<init>(Environment.getDataDirectory().getPath());
        paramString = localCodedOutputStream;
        str = this.idManager.getDeviceUUID();
        paramString = localCodedOutputStream;
        j = CommonUtils.getCpuArchitectureInt();
        paramString = localCodedOutputStream;
        i = Runtime.getRuntime().availableProcessors();
        paramString = localCodedOutputStream;
        l1 = CommonUtils.getTotalRamInBytes();
        paramString = localCodedOutputStream;
        l2 = ((StatFs)localObject4).getBlockCount();
        paramString = localCodedOutputStream;
        l3 = ((StatFs)localObject4).getBlockSize();
        paramString = localCodedOutputStream;
        bool = CommonUtils.isEmulator(localContext);
        paramString = localCodedOutputStream;
        localObject4 = this.idManager.getDeviceIdentifiers();
        paramString = localCodedOutputStream;
        k = CommonUtils.getDeviceState(localContext);
        paramString = localCodedOutputStream;
        SessionProtobufHelper.writeSessionDevice(localCodedOutputStream, str, j, Build.MODEL, i, l1, l2 * l3, bool, (Map)localObject4, k, Build.MANUFACTURER, Build.PRODUCT);
        CommonUtils.flushOrLog(localCodedOutputStream, "Failed to flush session device info.");
        CommonUtils.closeOrLog((Closeable)localObject1, "Failed to close session device file.");
        return;
      }
      finally
      {
        Object localObject1;
        for (;;) {}
      }
      localObject2 = finally;
      localObject1 = str;
      paramString = localContext;
    }
    CommonUtils.closeOrLog((Closeable)localObject1, "Failed to close session device file.");
    throw ((Throwable)localObject2);
  }
  
  private void writeSessionEvent(CodedOutputStream paramCodedOutputStream, Date paramDate, Thread paramThread, Throwable paramThrowable, String paramString, boolean paramBoolean)
    throws Exception
  {
    TrimmedThrowableData localTrimmedThrowableData = new TrimmedThrowableData(paramThrowable, this.stackTraceTrimmingStrategy);
    Object localObject = this.crashlyticsCore.getContext();
    long l1 = paramDate.getTime() / 1000L;
    Float localFloat = CommonUtils.getBatteryLevel((Context)localObject);
    int j = CommonUtils.getBatteryVelocity((Context)localObject, this.devicePowerStateListener.isPowerConnected());
    boolean bool = CommonUtils.getProximitySensorEnabled((Context)localObject);
    int k = ((Context)localObject).getResources().getConfiguration().orientation;
    long l3 = CommonUtils.getTotalRamInBytes();
    long l2 = CommonUtils.calculateFreeRamInBytes((Context)localObject);
    long l4 = CommonUtils.calculateUsedDiskSpaceInBytes(Environment.getDataDirectory().getPath());
    ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = CommonUtils.getAppProcessInfo(((Context)localObject).getPackageName(), (Context)localObject);
    LinkedList localLinkedList = new LinkedList();
    StackTraceElement[] arrayOfStackTraceElement = localTrimmedThrowableData.stacktrace;
    String str2 = this.appData.buildId;
    String str1 = this.idManager.getAppIdentifier();
    if (paramBoolean)
    {
      paramThrowable = Thread.getAllStackTraces();
      paramDate = new Thread[paramThrowable.size()];
      int i = 0;
      Iterator localIterator = paramThrowable.entrySet().iterator();
      for (;;)
      {
        paramThrowable = paramDate;
        if (!localIterator.hasNext()) {
          break;
        }
        paramThrowable = (Map.Entry)localIterator.next();
        paramDate[i] = ((Thread)paramThrowable.getKey());
        localLinkedList.add(this.stackTraceTrimmingStrategy.getTrimmedStackTrace((StackTraceElement[])paramThrowable.getValue()));
        i++;
      }
    }
    paramThrowable = new Thread[0];
    if (!CommonUtils.getBooleanResourceValue((Context)localObject, "com.crashlytics.CollectCustomKeys", true)) {
      paramDate = new TreeMap();
    }
    for (;;)
    {
      SessionProtobufHelper.writeSessionEvent(paramCodedOutputStream, l1, paramString, localTrimmedThrowableData, paramThread, arrayOfStackTraceElement, paramThrowable, localLinkedList, paramDate, this.logFileManager, localRunningAppProcessInfo, k, str1, str2, localFloat, j, bool, l3 - l2, l4);
      return;
      localObject = this.crashlyticsCore.getAttributes();
      paramDate = (Date)localObject;
      if (localObject != null)
      {
        paramDate = (Date)localObject;
        if (((Map)localObject).size() > 1) {
          paramDate = new TreeMap((Map)localObject);
        }
      }
    }
  }
  
  private void writeSessionOS(String paramString)
    throws Exception
  {
    Object localObject5 = null;
    CodedOutputStream localCodedOutputStream = null;
    Object localObject4 = null;
    try
    {
      localObject3 = new com/crashlytics/android/core/ClsFileOutputStream;
      File localFile = getFilesDir();
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      ((ClsFileOutputStream)localObject3).<init>(localFile, paramString + "SessionOS");
      paramString = localCodedOutputStream;
      CommonUtils.flushOrLog(paramString, "Failed to flush to session OS file.");
    }
    finally
    {
      try
      {
        localCodedOutputStream = CodedOutputStream.newInstance((OutputStream)localObject3);
        paramString = localCodedOutputStream;
        SessionProtobufHelper.writeSessionOS(localCodedOutputStream, CommonUtils.isRooted(this.crashlyticsCore.getContext()));
        CommonUtils.flushOrLog(localCodedOutputStream, "Failed to flush to session OS file.");
        CommonUtils.closeOrLog((Closeable)localObject3, "Failed to close session OS file.");
        return;
      }
      finally
      {
        Object localObject3;
        for (;;) {}
      }
      localObject1 = finally;
      localObject3 = localObject5;
      paramString = (String)localObject4;
    }
    CommonUtils.closeOrLog((Closeable)localObject3, "Failed to close session OS file.");
    throw ((Throwable)localObject1);
  }
  
  private void writeSessionPartsToSessionFile(File paramFile, String paramString, int paramInt)
  {
    Fabric.getLogger().d("CrashlyticsCore", "Collecting session parts for ID " + paramString);
    Object localObject = listFilesMatching(new FileNameContainsFilter(paramString + "SessionCrash"));
    boolean bool1;
    boolean bool2;
    if ((localObject != null) && (localObject.length > 0))
    {
      bool1 = true;
      Fabric.getLogger().d("CrashlyticsCore", String.format(Locale.US, "Session %s has fatal exception: %s", new Object[] { paramString, Boolean.valueOf(bool1) }));
      File[] arrayOfFile = listFilesMatching(new FileNameContainsFilter(paramString + "SessionEvent"));
      if ((arrayOfFile == null) || (arrayOfFile.length <= 0)) {
        break label279;
      }
      bool2 = true;
      label159:
      Fabric.getLogger().d("CrashlyticsCore", String.format(Locale.US, "Session %s has non-fatal exceptions: %s", new Object[] { paramString, Boolean.valueOf(bool2) }));
      if ((!bool1) && (!bool2)) {
        break label291;
      }
      arrayOfFile = getTrimmedNonFatalFiles(paramString, arrayOfFile, paramInt);
      if (!bool1) {
        break label285;
      }
      localObject = localObject[0];
      label226:
      synthesizeSessionFile(paramFile, paramString, arrayOfFile, (File)localObject);
    }
    for (;;)
    {
      Fabric.getLogger().d("CrashlyticsCore", "Removing session part files for ID " + paramString);
      deleteSessionPartFilesFor(paramString);
      return;
      bool1 = false;
      break;
      label279:
      bool2 = false;
      break label159;
      label285:
      localObject = null;
      break label226;
      label291:
      Fabric.getLogger().d("CrashlyticsCore", "No events present for session ID " + paramString);
    }
  }
  
  /* Error */
  private void writeSessionUser(String paramString)
    throws Exception
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 6
    //   6: aconst_null
    //   7: astore_3
    //   8: new 399	com/crashlytics/android/core/ClsFileOutputStream
    //   11: astore 4
    //   13: aload_0
    //   14: invokevirtual 538	com/crashlytics/android/core/CrashlyticsController:getFilesDir	()Ljava/io/File;
    //   17: astore 7
    //   19: new 381	java/lang/StringBuilder
    //   22: astore 8
    //   24: aload 8
    //   26: invokespecial 382	java/lang/StringBuilder:<init>	()V
    //   29: aload 4
    //   31: aload 7
    //   33: aload 8
    //   35: aload_1
    //   36: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: ldc -114
    //   41: invokevirtual 388	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: invokevirtual 391	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   47: invokespecial 541	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   50: aload 6
    //   52: astore_3
    //   53: aload 4
    //   55: invokestatic 545	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   58: astore 5
    //   60: aload 5
    //   62: astore_3
    //   63: aload_0
    //   64: aload_1
    //   65: invokespecial 1164	com/crashlytics/android/core/CrashlyticsController:getUserMetaData	(Ljava/lang/String;)Lcom/crashlytics/android/core/UserMetaData;
    //   68: astore_1
    //   69: aload 5
    //   71: astore_3
    //   72: aload_1
    //   73: invokevirtual 1167	com/crashlytics/android/core/UserMetaData:isEmpty	()Z
    //   76: istore_2
    //   77: iload_2
    //   78: ifeq +20 -> 98
    //   81: aload 5
    //   83: ldc_w 1169
    //   86: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   89: aload 4
    //   91: ldc_w 1171
    //   94: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   97: return
    //   98: aload 5
    //   100: astore_3
    //   101: aload 5
    //   103: aload_1
    //   104: getfield 1174	com/crashlytics/android/core/UserMetaData:id	Ljava/lang/String;
    //   107: aload_1
    //   108: getfield 1175	com/crashlytics/android/core/UserMetaData:name	Ljava/lang/String;
    //   111: aload_1
    //   112: getfield 1178	com/crashlytics/android/core/UserMetaData:email	Ljava/lang/String;
    //   115: invokestatic 1181	com/crashlytics/android/core/SessionProtobufHelper:writeSessionUser	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   118: aload 5
    //   120: ldc_w 1169
    //   123: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   126: aload 4
    //   128: ldc_w 1171
    //   131: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   134: goto -37 -> 97
    //   137: astore_1
    //   138: aload 5
    //   140: astore 4
    //   142: aload_3
    //   143: ldc_w 1169
    //   146: invokestatic 495	io/fabric/sdk/android/services/common/CommonUtils:flushOrLog	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   149: aload 4
    //   151: ldc_w 1171
    //   154: invokestatic 501	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   157: aload_1
    //   158: athrow
    //   159: astore_1
    //   160: goto -18 -> 142
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	163	0	this	CrashlyticsController
    //   0	163	1	paramString	String
    //   76	2	2	bool	boolean
    //   7	136	3	localObject1	Object
    //   11	139	4	localObject2	Object
    //   1	138	5	localCodedOutputStream	CodedOutputStream
    //   4	47	6	localObject3	Object
    //   17	15	7	localFile	File
    //   22	12	8	localStringBuilder	StringBuilder
    // Exception table:
    //   from	to	target	type
    //   8	50	137	finally
    //   53	60	159	finally
    //   63	69	159	finally
    //   72	77	159	finally
    //   101	118	159	finally
  }
  
  private static void writeToCosFromFile(CodedOutputStream paramCodedOutputStream, File paramFile)
    throws IOException
  {
    if (!paramFile.exists())
    {
      Fabric.getLogger().e("CrashlyticsCore", "Tried to include a file that doesn't exist: " + paramFile.getName(), null);
      label42:
      return;
    }
    Object localObject = null;
    try
    {
      localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile);
    }
    finally
    {
      try
      {
        copyToCodedOutputStream(localFileInputStream, paramCodedOutputStream, (int)paramFile.length());
        CommonUtils.closeOrLog(localFileInputStream, "Failed to close file input stream.");
        break label42;
      }
      finally
      {
        FileInputStream localFileInputStream;
        paramFile = localFileInputStream;
      }
      paramCodedOutputStream = finally;
      paramFile = (File)localObject;
      CommonUtils.closeOrLog(paramFile, "Failed to close file input stream.");
      throw paramCodedOutputStream;
    }
  }
  
  void cacheKeyData(final Map<String, String> paramMap)
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Void call()
        throws Exception
      {
        String str = CrashlyticsController.this.getCurrentSessionId();
        new MetaDataStore(CrashlyticsController.this.getFilesDir()).writeKeyData(str, paramMap);
        return null;
      }
    });
  }
  
  void cacheUserData(final String paramString1, final String paramString2, final String paramString3)
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Void call()
        throws Exception
      {
        String str = CrashlyticsController.this.getCurrentSessionId();
        new MetaDataStore(CrashlyticsController.this.getFilesDir()).writeUserData(str, new UserMetaData(paramString1, paramString2, paramString3));
        return null;
      }
    });
  }
  
  void cleanInvalidTempFiles()
  {
    this.backgroundWorker.submit(new Runnable()
    {
      public void run()
      {
        CrashlyticsController.this.doCleanInvalidTempFiles(CrashlyticsController.this.listFilesMatching(new CrashlyticsController.InvalidPartFileFilter()));
      }
    });
  }
  
  void doCleanInvalidTempFiles(File[] paramArrayOfFile)
  {
    int j = 0;
    final Object localObject = new HashSet();
    int k = paramArrayOfFile.length;
    File localFile;
    for (int i = 0; i < k; i++)
    {
      localFile = paramArrayOfFile[i];
      Fabric.getLogger().d("CrashlyticsCore", "Found invalid session part file: " + localFile);
      ((Set)localObject).add(getSessionIdFromSessionFile(localFile));
    }
    if (((Set)localObject).isEmpty()) {}
    for (;;)
    {
      return;
      paramArrayOfFile = getInvalidFilesDir();
      if (!paramArrayOfFile.exists()) {
        paramArrayOfFile.mkdir();
      }
      localObject = listFilesMatching(new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          boolean bool = false;
          if (paramAnonymousString.length() < 35) {}
          for (;;)
          {
            return bool;
            bool = localObject.contains(paramAnonymousString.substring(0, 35));
          }
        }
      });
      k = localObject.length;
      for (i = j; i < k; i++)
      {
        localFile = localObject[i];
        Fabric.getLogger().d("CrashlyticsCore", "Moving session file: " + localFile);
        if (!localFile.renameTo(new File(paramArrayOfFile, localFile.getName())))
        {
          Fabric.getLogger().d("CrashlyticsCore", "Could not move session file. Deleting " + localFile);
          localFile.delete();
        }
      }
      trimInvalidSessionFiles();
    }
  }
  
  void doCloseSessions(SessionSettingsData paramSessionSettingsData)
    throws Exception
  {
    doCloseSessions(paramSessionSettingsData, false);
  }
  
  void enableExceptionHandling(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    openSession();
    this.crashHandler = new CrashlyticsUncaughtExceptionHandler(new CrashlyticsUncaughtExceptionHandler.CrashListener()
    {
      public void onUncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
      {
        CrashlyticsController.this.handleUncaughtException(paramAnonymousThread, paramAnonymousThrowable);
      }
    }, paramUncaughtExceptionHandler);
    Thread.setDefaultUncaughtExceptionHandler(this.crashHandler);
  }
  
  boolean finalizeSessions(final SessionSettingsData paramSessionSettingsData)
  {
    ((Boolean)this.backgroundWorker.submitAndWait(new Callable()
    {
      public Boolean call()
        throws Exception
      {
        if (CrashlyticsController.this.isHandlingException()) {
          Fabric.getLogger().d("CrashlyticsCore", "Skipping session finalization because a crash has already occurred.");
        }
        for (Boolean localBoolean = Boolean.FALSE;; localBoolean = Boolean.TRUE)
        {
          return localBoolean;
          Fabric.getLogger().d("CrashlyticsCore", "Finalizing previously open sessions.");
          CrashlyticsController.this.doCloseSessions(paramSessionSettingsData, true);
          Fabric.getLogger().d("CrashlyticsCore", "Closed all previously open sessions");
        }
      }
    })).booleanValue();
  }
  
  File getFatalSessionFilesDir()
  {
    return new File(getFilesDir(), "fatal-sessions");
  }
  
  File getFilesDir()
  {
    return this.fileStore.getFilesDir();
  }
  
  File getInvalidFilesDir()
  {
    return new File(getFilesDir(), "invalidClsFiles");
  }
  
  File getNonFatalSessionFilesDir()
  {
    return new File(getFilesDir(), "nonfatal-sessions");
  }
  
  void handleUncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    try
    {
      Object localObject1 = Fabric.getLogger();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((Logger)localObject1).d("CrashlyticsCore", "Crashlytics is handling uncaught exception \"" + paramThrowable + "\" from thread " + paramThread.getName());
      this.devicePowerStateListener.dispose();
      localObject1 = new java/util/Date;
      ((Date)localObject1).<init>();
      localObject2 = this.backgroundWorker;
      Callable local6 = new com/crashlytics/android/core/CrashlyticsController$6;
      local6.<init>(this, (Date)localObject1, paramThread, paramThrowable);
      ((CrashlyticsBackgroundWorker)localObject2).submitAndWait(local6);
      return;
    }
    finally
    {
      paramThread = finally;
      throw paramThread;
    }
  }
  
  boolean hasOpenSession()
  {
    if (listSessionBeginFiles().length > 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean isHandlingException()
  {
    if ((this.crashHandler != null) && (this.crashHandler.isHandlingException())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  File[] listCompleteSessionFiles()
  {
    LinkedList localLinkedList = new LinkedList();
    Collections.addAll(localLinkedList, listFilesMatching(getFatalSessionFilesDir(), SESSION_FILE_FILTER));
    Collections.addAll(localLinkedList, listFilesMatching(getNonFatalSessionFilesDir(), SESSION_FILE_FILTER));
    Collections.addAll(localLinkedList, listFilesMatching(getFilesDir(), SESSION_FILE_FILTER));
    return (File[])localLinkedList.toArray(new File[localLinkedList.size()]);
  }
  
  File[] listSessionBeginFiles()
  {
    return listFilesMatching(new FileNameContainsFilter("BeginSession"));
  }
  
  void openSession()
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Void call()
        throws Exception
      {
        CrashlyticsController.this.doOpenSession();
        return null;
      }
    });
  }
  
  void submitAllReports(float paramFloat, SettingsData paramSettingsData)
  {
    if (paramSettingsData == null)
    {
      Fabric.getLogger().w("CrashlyticsCore", "Could not send reports. Settings are not available.");
      return;
    }
    CreateReportSpiCall localCreateReportSpiCall = getCreateReportSpiCall(paramSettingsData.appData.reportsUrl);
    if (shouldPromptUserBeforeSendingCrashReports(paramSettingsData)) {}
    for (paramSettingsData = new PrivacyDialogCheck(this.crashlyticsCore, this.preferenceManager, paramSettingsData.promptData);; paramSettingsData = new ReportUploader.AlwaysSendCheck())
    {
      new ReportUploader(this.appData.apiKey, localCreateReportSpiCall, this.reportFilesProvider, this.handlingExceptionCheck).uploadReports(paramFloat, paramSettingsData);
      break;
    }
  }
  
  void trimSessionFiles(int paramInt)
  {
    int i = paramInt - Utils.capFileCount(getFatalSessionFilesDir(), paramInt, SMALLEST_FILE_NAME_FIRST);
    paramInt = Utils.capFileCount(getNonFatalSessionFilesDir(), i, SMALLEST_FILE_NAME_FIRST);
    Utils.capFileCount(getFilesDir(), SESSION_FILE_FILTER, i - paramInt, SMALLEST_FILE_NAME_FIRST);
  }
  
  void writeExternalCrashEvent(final SessionEventData paramSessionEventData)
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Void call()
        throws Exception
      {
        if (!CrashlyticsController.this.isHandlingException()) {
          CrashlyticsController.this.doWriteExternalCrashEvent(paramSessionEventData);
        }
        return null;
      }
    });
  }
  
  void writeNonFatalException(final Thread paramThread, final Throwable paramThrowable)
  {
    final Date localDate = new Date();
    this.backgroundWorker.submit(new Runnable()
    {
      public void run()
      {
        if (!CrashlyticsController.this.isHandlingException()) {
          CrashlyticsController.this.doWriteNonFatal(localDate, paramThread, paramThrowable);
        }
      }
    });
  }
  
  void writeToLog(final long paramLong, String paramString)
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Void call()
        throws Exception
      {
        if (!CrashlyticsController.this.isHandlingException()) {
          CrashlyticsController.this.logFileManager.writeToLog(paramLong, this.val$msg);
        }
        return null;
      }
    });
  }
  
  private static class AnySessionPartFileFilter
    implements FilenameFilter
  {
    public boolean accept(File paramFile, String paramString)
    {
      if ((!CrashlyticsController.SESSION_FILE_FILTER.accept(paramFile, paramString)) && (CrashlyticsController.SESSION_FILE_PATTERN.matcher(paramString).matches())) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
  
  static class FileNameContainsFilter
    implements FilenameFilter
  {
    private final String string;
    
    public FileNameContainsFilter(String paramString)
    {
      this.string = paramString;
    }
    
    public boolean accept(File paramFile, String paramString)
    {
      if ((paramString.contains(this.string)) && (!paramString.endsWith(".cls_temp"))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
  
  static class InvalidPartFileFilter
    implements FilenameFilter
  {
    public boolean accept(File paramFile, String paramString)
    {
      if ((ClsFileOutputStream.TEMP_FILENAME_FILTER.accept(paramFile, paramString)) || (paramString.contains("SessionMissingBinaryImages"))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
  
  private static final class LogFileDirectoryProvider
    implements LogFileManager.DirectoryProvider
  {
    private static final String LOG_FILES_DIR = "log-files";
    private final FileStore rootFileStore;
    
    public LogFileDirectoryProvider(FileStore paramFileStore)
    {
      this.rootFileStore = paramFileStore;
    }
    
    public File getLogFileDir()
    {
      File localFile = new File(this.rootFileStore.getFilesDir(), "log-files");
      if (!localFile.exists()) {
        localFile.mkdirs();
      }
      return localFile;
    }
  }
  
  private static final class PrivacyDialogCheck
    implements ReportUploader.SendCheck
  {
    private final Kit kit;
    private final PreferenceManager preferenceManager;
    private final PromptSettingsData promptData;
    
    public PrivacyDialogCheck(Kit paramKit, PreferenceManager paramPreferenceManager, PromptSettingsData paramPromptSettingsData)
    {
      this.kit = paramKit;
      this.preferenceManager = paramPreferenceManager;
      this.promptData = paramPromptSettingsData;
    }
    
    public boolean canSendReports()
    {
      Activity localActivity = this.kit.getFabric().getCurrentActivity();
      if ((localActivity == null) || (localActivity.isFinishing())) {}
      final Object localObject;
      for (boolean bool = true;; bool = ((CrashPromptDialog)localObject).getOptIn())
      {
        return bool;
        localObject = new CrashPromptDialog.AlwaysSendCallback()
        {
          public void sendUserReportsWithoutPrompting(boolean paramAnonymousBoolean)
          {
            CrashlyticsController.PrivacyDialogCheck.this.preferenceManager.setShouldAlwaysSendReports(paramAnonymousBoolean);
          }
        };
        localObject = CrashPromptDialog.create(localActivity, this.promptData, (CrashPromptDialog.AlwaysSendCallback)localObject);
        localActivity.runOnUiThread(new Runnable()
        {
          public void run()
          {
            localObject.show();
          }
        });
        Fabric.getLogger().d("CrashlyticsCore", "Waiting for user opt-in.");
        ((CrashPromptDialog)localObject).await();
      }
    }
  }
  
  private final class ReportUploaderFilesProvider
    implements ReportUploader.ReportFilesProvider
  {
    private ReportUploaderFilesProvider() {}
    
    public File[] getCompleteSessionFiles()
    {
      return CrashlyticsController.this.listCompleteSessionFiles();
    }
    
    public File[] getInvalidSessionFiles()
    {
      return CrashlyticsController.this.getInvalidFilesDir().listFiles();
    }
  }
  
  private final class ReportUploaderHandlingExceptionCheck
    implements ReportUploader.HandlingExceptionCheck
  {
    private ReportUploaderHandlingExceptionCheck() {}
    
    public boolean isHandlingException()
    {
      return CrashlyticsController.this.isHandlingException();
    }
  }
  
  private static final class SendReportRunnable
    implements Runnable
  {
    private final Context context;
    private final Report report;
    private final ReportUploader reportUploader;
    
    public SendReportRunnable(Context paramContext, Report paramReport, ReportUploader paramReportUploader)
    {
      this.context = paramContext;
      this.report = paramReport;
      this.reportUploader = paramReportUploader;
    }
    
    public void run()
    {
      if (!CommonUtils.canTryConnection(this.context)) {}
      for (;;)
      {
        return;
        Fabric.getLogger().d("CrashlyticsCore", "Attempting to send crash report at time of crash...");
        this.reportUploader.forceUpload(this.report);
      }
    }
  }
  
  static class SessionPartFileFilter
    implements FilenameFilter
  {
    private final String sessionId;
    
    public SessionPartFileFilter(String paramString)
    {
      this.sessionId = paramString;
    }
    
    public boolean accept(File paramFile, String paramString)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramString.equals(this.sessionId + ".cls")) {
        bool1 = bool2;
      }
      for (;;)
      {
        return bool1;
        bool1 = bool2;
        if (paramString.contains(this.sessionId))
        {
          bool1 = bool2;
          if (!paramString.endsWith(".cls_temp")) {
            bool1 = true;
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\CrashlyticsController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */