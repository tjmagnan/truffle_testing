package android.support.v7.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Scroller;

public abstract class SnapHelper
  extends RecyclerView.OnFlingListener
{
  static final float MILLISECONDS_PER_INCH = 100.0F;
  private Scroller mGravityScroller;
  RecyclerView mRecyclerView;
  private final RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener()
  {
    boolean mScrolled = false;
    
    public void onScrollStateChanged(RecyclerView paramAnonymousRecyclerView, int paramAnonymousInt)
    {
      super.onScrollStateChanged(paramAnonymousRecyclerView, paramAnonymousInt);
      if ((paramAnonymousInt == 0) && (this.mScrolled))
      {
        this.mScrolled = false;
        SnapHelper.this.snapToTargetExistingView();
      }
    }
    
    public void onScrolled(RecyclerView paramAnonymousRecyclerView, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      if ((paramAnonymousInt1 != 0) || (paramAnonymousInt2 != 0)) {
        this.mScrolled = true;
      }
    }
  };
  
  private void destroyCallbacks()
  {
    this.mRecyclerView.removeOnScrollListener(this.mScrollListener);
    this.mRecyclerView.setOnFlingListener(null);
  }
  
  private void setupCallbacks()
    throws IllegalStateException
  {
    if (this.mRecyclerView.getOnFlingListener() != null) {
      throw new IllegalStateException("An instance of OnFlingListener already set.");
    }
    this.mRecyclerView.addOnScrollListener(this.mScrollListener);
    this.mRecyclerView.setOnFlingListener(this);
  }
  
  private boolean snapFromFling(@NonNull RecyclerView.LayoutManager paramLayoutManager, int paramInt1, int paramInt2)
  {
    boolean bool2 = false;
    boolean bool1;
    if (!(paramLayoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      LinearSmoothScroller localLinearSmoothScroller = createSnapScroller(paramLayoutManager);
      bool1 = bool2;
      if (localLinearSmoothScroller != null)
      {
        paramInt1 = findTargetSnapPosition(paramLayoutManager, paramInt1, paramInt2);
        bool1 = bool2;
        if (paramInt1 != -1)
        {
          localLinearSmoothScroller.setTargetPosition(paramInt1);
          paramLayoutManager.startSmoothScroll(localLinearSmoothScroller);
          bool1 = true;
        }
      }
    }
  }
  
  public void attachToRecyclerView(@Nullable RecyclerView paramRecyclerView)
    throws IllegalStateException
  {
    if (this.mRecyclerView == paramRecyclerView) {}
    for (;;)
    {
      return;
      if (this.mRecyclerView != null) {
        destroyCallbacks();
      }
      this.mRecyclerView = paramRecyclerView;
      if (this.mRecyclerView != null)
      {
        setupCallbacks();
        this.mGravityScroller = new Scroller(this.mRecyclerView.getContext(), new DecelerateInterpolator());
        snapToTargetExistingView();
      }
    }
  }
  
  @Nullable
  public abstract int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager paramLayoutManager, @NonNull View paramView);
  
  public int[] calculateScrollDistance(int paramInt1, int paramInt2)
  {
    this.mGravityScroller.fling(0, 0, paramInt1, paramInt2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
    return new int[] { this.mGravityScroller.getFinalX(), this.mGravityScroller.getFinalY() };
  }
  
  @Nullable
  protected LinearSmoothScroller createSnapScroller(RecyclerView.LayoutManager paramLayoutManager)
  {
    if (!(paramLayoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {}
    for (paramLayoutManager = null;; paramLayoutManager = new LinearSmoothScroller(this.mRecyclerView.getContext())
        {
          protected float calculateSpeedPerPixel(DisplayMetrics paramAnonymousDisplayMetrics)
          {
            return 100.0F / paramAnonymousDisplayMetrics.densityDpi;
          }
          
          protected void onTargetFound(View paramAnonymousView, RecyclerView.State paramAnonymousState, RecyclerView.SmoothScroller.Action paramAnonymousAction)
          {
            paramAnonymousView = SnapHelper.this.calculateDistanceToFinalSnap(SnapHelper.this.mRecyclerView.getLayoutManager(), paramAnonymousView);
            int i = paramAnonymousView[0];
            int k = paramAnonymousView[1];
            int j = calculateTimeForDeceleration(Math.max(Math.abs(i), Math.abs(k)));
            if (j > 0) {
              paramAnonymousAction.update(i, k, j, this.mDecelerateInterpolator);
            }
          }
        }) {
      return paramLayoutManager;
    }
  }
  
  @Nullable
  public abstract View findSnapView(RecyclerView.LayoutManager paramLayoutManager);
  
  public abstract int findTargetSnapPosition(RecyclerView.LayoutManager paramLayoutManager, int paramInt1, int paramInt2);
  
  public boolean onFling(int paramInt1, int paramInt2)
  {
    boolean bool2 = false;
    RecyclerView.LayoutManager localLayoutManager = this.mRecyclerView.getLayoutManager();
    boolean bool1;
    if (localLayoutManager == null) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (this.mRecyclerView.getAdapter() != null)
      {
        int i = this.mRecyclerView.getMinFlingVelocity();
        if (Math.abs(paramInt2) <= i)
        {
          bool1 = bool2;
          if (Math.abs(paramInt1) <= i) {}
        }
        else
        {
          bool1 = bool2;
          if (snapFromFling(localLayoutManager, paramInt1, paramInt2)) {
            bool1 = true;
          }
        }
      }
    }
  }
  
  void snapToTargetExistingView()
  {
    if (this.mRecyclerView == null) {}
    for (;;)
    {
      return;
      RecyclerView.LayoutManager localLayoutManager = this.mRecyclerView.getLayoutManager();
      if (localLayoutManager != null)
      {
        Object localObject = findSnapView(localLayoutManager);
        if (localObject != null)
        {
          localObject = calculateDistanceToFinalSnap(localLayoutManager, (View)localObject);
          if ((localObject[0] != 0) || (localObject[1] != 0)) {
            this.mRecyclerView.smoothScrollBy(localObject[0], localObject[1]);
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\SnapHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */