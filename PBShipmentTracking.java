package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBShipmentTracking
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 9156489425865348169L;
  @Expose
  @SerializedName("carrier")
  private String carrier;
  @Expose
  @SerializedName("company_name")
  private Object companyName;
  @Expose
  @SerializedName("display_field")
  private String displayField;
  @Expose
  @SerializedName("estimated_delivery")
  private String estimatedDelivery;
  @Expose
  @SerializedName("iso_country")
  private String isoCountry;
  @Expose
  @SerializedName("recipient_name")
  private Object recipientName;
  private PBShipmentTrackingDetail shipmentDetail;
  @Expose
  @SerializedName("source")
  private String source;
  @Expose
  @SerializedName("tracking_id")
  private String trackingId;
  @Expose
  @SerializedName("updated_timestamp")
  private String updatedTimestamp;
  @Expose
  @SerializedName("user_id")
  private String userId;
  
  public PBShipmentTracking() {}
  
  public PBShipmentTracking(String paramString1, String paramString2, String paramString3, Object paramObject1, Object paramObject2, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    this.trackingId = paramString1;
    this.source = paramString2;
    this.carrier = paramString3;
    this.recipientName = paramObject1;
    this.companyName = paramObject2;
    this.isoCountry = paramString4;
    this.estimatedDelivery = paramString5;
    this.updatedTimestamp = paramString6;
    this.userId = paramString7;
  }
  
  public String getCarrier()
  {
    return this.carrier;
  }
  
  public Object getCompanyName()
  {
    return this.companyName;
  }
  
  public String getDisplayField()
  {
    return this.displayField;
  }
  
  public String getEstimatedDelivery()
  {
    return this.estimatedDelivery;
  }
  
  public String getIsoCountry()
  {
    return this.isoCountry;
  }
  
  public Object getRecipientName()
  {
    return this.recipientName;
  }
  
  public PBShipmentTrackingDetail getShipmentDetail()
  {
    return this.shipmentDetail;
  }
  
  public String getSource()
  {
    return this.source;
  }
  
  public String getTrackingId()
  {
    return this.trackingId;
  }
  
  public String getUpdatedTimestamp()
  {
    return this.updatedTimestamp;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public void setCarrier(String paramString)
  {
    this.carrier = paramString;
  }
  
  public void setCompanyName(Object paramObject)
  {
    this.companyName = paramObject;
  }
  
  public void setDisplayField(String paramString)
  {
    this.displayField = paramString;
  }
  
  public void setEstimatedDelivery(String paramString)
  {
    this.estimatedDelivery = paramString;
  }
  
  public void setIsoCountry(String paramString)
  {
    this.isoCountry = paramString;
  }
  
  public void setRecipientName(Object paramObject)
  {
    this.recipientName = paramObject;
  }
  
  public void setShipmentDetail(PBShipmentTrackingDetail paramPBShipmentTrackingDetail)
  {
    this.shipmentDetail = paramPBShipmentTrackingDetail;
  }
  
  public void setSource(String paramString)
  {
    this.source = paramString;
  }
  
  public void setTrackingId(String paramString)
  {
    this.trackingId = paramString;
  }
  
  public void setUpdatedTimestamp(String paramString)
  {
    this.updatedTimestamp = paramString;
  }
  
  public void setUserId(String paramString)
  {
    this.userId = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentTracking.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */