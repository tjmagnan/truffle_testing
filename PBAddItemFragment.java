package tech.dcube.companion.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;

public class PBAddItemFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private String mParam1;
  private String mParam2;
  
  public static PBAddItemFragment newInstance(String paramString1, String paramString2)
  {
    PBAddItemFragment localPBAddItemFragment = new PBAddItemFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBAddItemFragment.setArguments(localBundle);
    return localPBAddItemFragment;
  }
  
  public void onClickTopBarLeftButton() {}
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968630, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Add Item");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Done");
    return paramLayoutInflater;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBAddItemFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */