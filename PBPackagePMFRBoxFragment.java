package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBParcelPackage;
import tech.dcube.companion.model.PBShipmentPackage;

public class PBPackagePMFRBoxFragment
  extends PBTopBarFragment
  implements View.OnClickListener
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  public static final String LARGE_PMFR_BOX = "LFRB";
  public static final String MEDIUM_PMFR_BOX = "FRB";
  public static final String SMALL_PMFR_BOX = "SFRB";
  LinearLayout LargeFlatRateBox;
  LinearLayout MediumFlatRateBox;
  TextView Rate_LargeFlatRateBox;
  TextView Rate_MediumFlatRateBox;
  TextView Rate_SmallFlatRateBox;
  LinearLayout SmallFlatRateBox;
  private OnPackagePMFRBoxListener mListener;
  private String mParam1;
  private String mParam2;
  
  public static PBPackagePMFRBoxFragment newInstance(String paramString1, String paramString2)
  {
    PBPackagePMFRBoxFragment localPBPackagePMFRBoxFragment = new PBPackagePMFRBoxFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBPackagePMFRBoxFragment.setArguments(localBundle);
    return localPBPackagePMFRBoxFragment;
  }
  
  void hideAllPMFRBoxes()
  {
    this.LargeFlatRateBox.setVisibility(8);
    this.MediumFlatRateBox.setVisibility(8);
    this.SmallFlatRateBox.setVisibility(8);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnPackagePMFRBoxListener))
    {
      this.mListener = ((OnPackagePMFRBoxListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnPackagePMFRBoxListener");
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      onPMFRBoxSelected("LFRB");
      continue;
      onPMFRBoxSelected("FRB");
      continue;
      onPMFRBoxSelected("SFRB");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968636, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Select Packaging");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    this.LargeFlatRateBox = ((LinearLayout)paramLayoutInflater.findViewById(2131624136));
    this.MediumFlatRateBox = ((LinearLayout)paramLayoutInflater.findViewById(2131624138));
    this.SmallFlatRateBox = ((LinearLayout)paramLayoutInflater.findViewById(2131624140));
    this.Rate_LargeFlatRateBox = ((TextView)paramLayoutInflater.findViewById(2131624137));
    this.Rate_MediumFlatRateBox = ((TextView)paramLayoutInflater.findViewById(2131624139));
    this.Rate_SmallFlatRateBox = ((TextView)paramLayoutInflater.findViewById(2131624141));
    this.LargeFlatRateBox.setOnClickListener(this);
    this.MediumFlatRateBox.setOnClickListener(this);
    this.SmallFlatRateBox.setOnClickListener(this);
    hideAllPMFRBoxes();
    showAvailablePMFRBoxes();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPMFRBoxSelected(String paramString)
  {
    if (this.mListener != null)
    {
      Object localObject2 = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.FLAT_RATE_BOX);
      Object localObject1 = new PBShipmentPackage();
      Iterator localIterator = ((List)localObject2).iterator();
      while (localIterator.hasNext())
      {
        localObject2 = (PBShipmentPackage)localIterator.next();
        if (((PBShipmentPackage)localObject2).getPackageId().equals(paramString)) {
          localObject1 = localObject2;
        }
      }
      this.mListener.onPMFRBoxSelected((PBShipmentPackage)localObject1);
    }
  }
  
  void showAvailablePMFRBoxes()
  {
    hideAllPMFRBoxes();
    Object localObject = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.FLAT_RATE_BOX);
    int k = 0;
    int j = 0;
    int i = 0;
    boolean bool = DataManager.getInstance().isPackagesV0Success();
    Iterator localIterator = ((List)localObject).iterator();
    while (localIterator.hasNext())
    {
      localObject = (PBShipmentPackage)localIterator.next();
      if (((PBShipmentPackage)localObject).getPackageId().equals("LFRB")) {
        k = 1;
      } else if (((PBShipmentPackage)localObject).getPackageId().equals("FRB")) {
        j = 1;
      } else if (((PBShipmentPackage)localObject).getPackageId().equals("SFRB")) {
        i = 1;
      }
    }
    if (k != 0)
    {
      this.LargeFlatRateBox.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("LFRB")).getPrice().doubleValue()) });
        this.Rate_LargeFlatRateBox.setText((CharSequence)localObject);
      }
    }
    if (j != 0)
    {
      this.MediumFlatRateBox.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("FRB")).getPrice().doubleValue()) });
        this.Rate_MediumFlatRateBox.setText((CharSequence)localObject);
      }
    }
    if (i != 0)
    {
      this.SmallFlatRateBox.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("SFRB")).getPrice().doubleValue()) });
        this.Rate_SmallFlatRateBox.setText((CharSequence)localObject);
      }
    }
  }
  
  public static abstract interface OnPackagePMFRBoxListener
  {
    public abstract void onPMFRBoxSelected(PBShipmentPackage paramPBShipmentPackage);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBPackagePMFRBoxFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */