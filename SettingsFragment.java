package tech.dcube.companion.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.android.volley.VolleyError;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;

public class SettingsFragment
  extends Fragment
{
  private static final String ARG_METER_MODEL = "meter_model";
  private static final String ARG_METER_NAME = "meter_name";
  private static final String ARG_METER_POSITION = "meter_position";
  private static final String ARG_METER_SERIAL_NUMBER = "meter_serial_number";
  private static final String SETTINGS_FUNDS_LOW = "_funds_low";
  private static final String SETTINGS_LOW_FUND_WARNING_AMOUNT = "_low_fund_warning_amount";
  private static final String SETTINGS_LOW_INK_LEVEL = "_low_ink_level";
  private static final String SETTINGS_METER_ERROR = "_low_ink_level";
  private static final String SETTINGS_SOFTWARE_RATE_UPDATES = "_software_rate_updates";
  private static final String SHARED_PREFERENCES_KEY = "COMPANION_APP_SPREF_KEY_";
  String low_fund_warning_amount = "0.00";
  boolean low_ink_level = true;
  Switch mFundLowSwitch;
  LinearLayout mFundsLowAmntLL;
  TextView mFundsLowTitleTV;
  EditText mFundsLowerLimitTV;
  private OnSettingsInteractionListener mListener;
  Switch mLowInkLevelSwitch;
  Switch mMeterErrorSwitch;
  private String mMeterModel;
  TextView mMeterModelTV;
  public String mMeterName;
  EditText mMeterNameTV;
  TextView mMeterSerialNoTV;
  private String mMeterSerialNumber;
  private int mPosition;
  Switch mSoftwareRateUpdateSwitch;
  boolean meter_error = true;
  boolean meter_funds_low = true;
  SharedPreferences sharedPreferences;
  boolean software_rate_update = true;
  
  public static SettingsFragment newInstance(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    SettingsFragment localSettingsFragment = new SettingsFragment();
    localSettingsFragment.mMeterName = paramString1;
    Bundle localBundle = new Bundle();
    localBundle.putInt("meter_position", paramInt);
    localBundle.putString("meter_name", paramString1);
    localBundle.putString("meter_model", paramString2);
    localBundle.putString("meter_serial_number", paramString3);
    localSettingsFragment.setArguments(localBundle);
    return localSettingsFragment;
  }
  
  void addCheckBoxListeners()
  {
    this.mFundLowSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          SettingsFragment.this.callAddMeterSubscriptionApi("FUNDS");
        }
        for (;;)
        {
          return;
          SettingsFragment.this.callRemoveMeterSubscriptionApi("FUNDS");
        }
      }
    });
    this.mLowInkLevelSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          SettingsFragment.this.callAddMeterSubscriptionApi("SUPPLY");
        }
        for (;;)
        {
          return;
          SettingsFragment.this.callRemoveMeterSubscriptionApi("SUPPLY");
        }
      }
    });
    this.mMeterErrorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean) {
          SettingsFragment.this.callAddMeterSubscriptionApi("DEVICE");
        }
        for (;;)
        {
          return;
          SettingsFragment.this.callRemoveMeterSubscriptionApi("DEVICE");
        }
      }
    });
    this.mSoftwareRateUpdateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        SettingsFragment.this.software_rate_update = paramAnonymousBoolean;
        SettingsFragment.this.saveSoftwareRateUpdatesPreference(SettingsFragment.this.mMeterModel, SettingsFragment.this.mMeterSerialNumber);
      }
    });
  }
  
  void callAddMeterSubscriptionApi(String paramString)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local12 = new tech/dcube/companion/fragments/SettingsFragment$12;
      local12.<init>(this);
      localServerManager.addMeterSubscriptions(localContext, paramString, local12);
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
        Log.wtf("Home: ", "Add Subscriptions: Failed");
        updateUILayout();
      }
    }
  }
  
  void callGetMeterSubscriptionApi()
  {
    showLoadingDialog();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local11 = new tech/dcube/companion/fragments/SettingsFragment$11;
      local11.<init>(this);
      localServerManager.getMeterSubscriptions(localContext, local11);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        Log.wtf("Home: ", "Get Subscriptions: Failed");
        updateUILayout();
        hideLoadingDialog();
      }
    }
  }
  
  void callRemoveMeterSubscriptionApi(String paramString)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local13 = new tech/dcube/companion/fragments/SettingsFragment$13;
      local13.<init>(this);
      localServerManager.removeMeterSubscriptions(localContext, paramString, local13);
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
        Log.wtf("Home: ", "Remove Subscriptions: Failed");
        updateUILayout();
      }
    }
  }
  
  void getLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.low_ink_level = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  void getMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.meter_error = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  void getMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    this.meter_funds_low = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  void getPreferences()
  {
    String str1 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_ink_level";
    String str2 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_funds_low";
    String str5 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_ink_level";
    String str4 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_software_rate_updates";
    String str3 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_fund_warning_amount";
    this.low_ink_level = this.sharedPreferences.getBoolean(str1, true);
    this.meter_funds_low = this.sharedPreferences.getBoolean(str2, true);
    this.meter_error = this.sharedPreferences.getBoolean(str5, true);
    this.software_rate_update = this.sharedPreferences.getBoolean(str4, true);
    this.low_fund_warning_amount = this.sharedPreferences.getString(str3, "0.00");
  }
  
  void getSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.software_rate_update = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  void hideLoadingDialog()
  {
    if (this.mListener != null) {
      this.mListener.onSettingsShowLoadingDialog(false);
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnSettingsInteractionListener))
    {
      this.mListener = ((OnSettingsInteractionListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnFragmentInteractionListener");
  }
  
  public void onButtonPressed(Uri paramUri)
  {
    if (this.mListener != null) {}
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mPosition = getArguments().getInt("meter_position");
      this.mMeterName = getArguments().getString("meter_name");
      this.mMeterModel = getArguments().getString("meter_model");
      this.mMeterSerialNumber = getArguments().getString("meter_serial_number");
    }
    this.sharedPreferences = getActivity().getSharedPreferences("COMPANION_APP_SPREF_KEY_", 0);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968647, paramViewGroup, false);
    this.mMeterNameTV = ((EditText)paramLayoutInflater.findViewById(2131624203));
    this.mMeterModelTV = ((TextView)paramLayoutInflater.findViewById(2131624199));
    this.mMeterSerialNoTV = ((TextView)paramLayoutInflater.findViewById(2131624200));
    this.mFundLowSwitch = ((Switch)paramLayoutInflater.findViewById(2131624209));
    this.mLowInkLevelSwitch = ((Switch)paramLayoutInflater.findViewById(2131624206));
    this.mMeterErrorSwitch = ((Switch)paramLayoutInflater.findViewById(2131624216));
    this.mSoftwareRateUpdateSwitch = ((Switch)paramLayoutInflater.findViewById(2131624219));
    this.mFundsLowTitleTV = ((TextView)paramLayoutInflater.findViewById(2131624211));
    this.mFundsLowerLimitTV = ((EditText)paramLayoutInflater.findViewById(2131624213));
    this.mFundsLowAmntLL = ((LinearLayout)paramLayoutInflater.findViewById(2131624210));
    this.mMeterNameTV.setText(this.mMeterName);
    getSoftwareRateUpdatesPreference(this.mMeterModel, this.mMeterSerialNumber);
    setProperUIValues();
    setListeners();
    this.mMeterModelTV.setText("Model " + this.mMeterModel);
    this.mMeterSerialNoTV.setText("Serial Number " + this.mMeterSerialNumber);
    callGetMeterSubscriptionApi();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onLowFundsSwitched()
  {
    if (this.mListener != null)
    {
      String str2 = this.mMeterModel;
      String str1 = this.mMeterSerialNumber;
      String str3 = this.mMeterName;
      int i = this.mPosition;
      this.mMeterNameTV.getText().toString();
      str3 = this.mFundsLowerLimitTV.getText().toString();
      boolean bool = this.mFundLowSwitch.isChecked();
      if (bool) {
        this.mListener.onMeterSettingsLowFundsChanged(str2, str1, str3, bool);
      }
    }
  }
  
  public void onMeterNickNameChanged()
  {
    if (this.mListener != null)
    {
      String str3 = this.mMeterModel;
      String str4 = this.mMeterSerialNumber;
      String str2 = this.mMeterName;
      int i = this.mPosition;
      String str1 = this.mMeterNameTV.getText().toString();
      this.mListener.onMeterSettingsNameChanged(i, str3, str4, str2, str1);
    }
  }
  
  void removeCheckBoxListeners()
  {
    this.mFundLowSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean) {}
    });
    this.mLowInkLevelSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean) {}
    });
    this.mMeterErrorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean) {}
    });
    this.mSoftwareRateUpdateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean) {}
    });
  }
  
  void saveLowFundWarningPreferences()
  {
    String str2 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_fund_warning_amount";
    SharedPreferences.Editor localEditor = this.sharedPreferences.edit();
    Object localObject = null;
    try
    {
      String str1 = this.mFundsLowerLimitTV.getText().toString();
      localObject = str1;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    localEditor.putString(str2, (String)localObject);
    localEditor.commit();
  }
  
  void saveLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString2 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString1 = this.sharedPreferences.edit();
    paramString1.putBoolean(paramString2, this.low_ink_level);
    paramString1.commit();
  }
  
  void saveMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString2 = this.sharedPreferences.edit();
    paramString2.putBoolean(paramString1, this.meter_error);
    paramString2.commit();
  }
  
  void saveMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    paramString2 = this.sharedPreferences.edit();
    paramString2.putBoolean(paramString1, this.meter_funds_low);
    paramString2.commit();
  }
  
  void savePreferences()
  {
    String str1 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_ink_level";
    String str4 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_funds_low";
    String str2 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_low_ink_level";
    String str3 = this.mMeterModel + "_" + this.mMeterSerialNumber + "_software_rate_updates";
    SharedPreferences.Editor localEditor = this.sharedPreferences.edit();
    localEditor.putBoolean(str1, this.low_ink_level);
    localEditor.putBoolean(str4, this.meter_funds_low);
    localEditor.putBoolean(str2, this.meter_error);
    localEditor.putBoolean(str3, this.software_rate_update);
    localEditor.commit();
  }
  
  void saveSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    paramString2 = this.sharedPreferences.edit();
    paramString2.putBoolean(paramString1, this.software_rate_update);
    paramString2.commit();
  }
  
  void setListeners()
  {
    this.mMeterNameTV.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        if (!paramAnonymousBoolean) {
          SettingsFragment.this.onMeterNickNameChanged();
        }
      }
    });
    this.mFundsLowerLimitTV.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        if (!paramAnonymousBoolean) {
          SettingsFragment.this.onLowFundsSwitched();
        }
      }
    });
    addCheckBoxListeners();
  }
  
  void setProperUIValues()
  {
    this.low_ink_level = ((Boolean)DataManager.getInstance().getMeterSubscriptions().get("SUPPLY")).booleanValue();
    this.meter_funds_low = ((Boolean)DataManager.getInstance().getMeterSubscriptions().get("FUNDS")).booleanValue();
    this.meter_error = ((Boolean)DataManager.getInstance().getMeterSubscriptions().get("DEVICE")).booleanValue();
    this.mLowInkLevelSwitch.setChecked(this.low_ink_level);
    this.mMeterErrorSwitch.setChecked(this.meter_error);
    this.mSoftwareRateUpdateSwitch.setChecked(this.software_rate_update);
    this.mFundLowSwitch.setChecked(this.meter_funds_low);
    if (this.meter_funds_low)
    {
      this.mFundsLowAmntLL.setVisibility(0);
      this.mFundsLowTitleTV.setVisibility(0);
      this.mFundsLowerLimitTV.setFocusable(true);
    }
    for (;;)
    {
      Log.d("Meter Settings", this.mMeterModel + this.mMeterSerialNumber + this.low_fund_warning_amount);
      this.mFundsLowerLimitTV.setText(this.low_fund_warning_amount);
      return;
      this.mFundsLowAmntLL.setVisibility(8);
      this.mFundsLowTitleTV.setVisibility(8);
      this.mFundsLowerLimitTV.setFocusable(false);
    }
  }
  
  void showLoadingDialog()
  {
    if (this.mListener != null) {
      this.mListener.onSettingsShowLoadingDialog(true);
    }
  }
  
  void updateUILayout()
  {
    removeCheckBoxListeners();
    setProperUIValues();
    addCheckBoxListeners();
  }
  
  public static abstract interface OnSettingsInteractionListener
  {
    public abstract void onMeterSettingsLowFundsChanged(String paramString1, String paramString2, String paramString3, boolean paramBoolean);
    
    public abstract void onMeterSettingsNameChanged(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4);
    
    public abstract void onSettingsShowLoadingDialog(boolean paramBoolean);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\SettingsFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */