package com.testfairy.n;

import android.telephony.TelephonyManager;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.g;
import java.util.HashMap;
import java.util.Map;

public class m
  extends b
{
  private static final String d = "simOperatorName";
  private static final String e = "networkOperatorName";
  private String a = null;
  private String b = null;
  private TelephonyManager c;
  
  public m(com.testfairy.m.b paramb, TelephonyManager paramTelephonyManager)
  {
    super(paramb);
    this.c = paramTelephonyManager;
  }
  
  public void a()
  {
    try
    {
      String str = this.c.getSimOperatorName();
      Object localObject1 = this.c.getNetworkOperatorName();
      if ((str != null) && (localObject1 != null) && ((!str.equals(this.a)) || (!((String)localObject1).equals(this.b))))
      {
        Object localObject2 = e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.v((String)localObject2, "Operator: '" + (String)localObject1 + "', SIM: '" + str + "'");
        this.a = str;
        this.b = ((String)localObject1);
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>(2);
        ((Map)localObject2).put("simOperatorName", str);
        ((Map)localObject2).put("networkOperatorName", localObject1);
        localObject1 = new com/testfairy/g;
        ((g)localObject1).<init>(17, (Map)localObject2);
        b().a((g)localObject1);
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */