package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.ButterKnife;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBWeightRules;

public class PBPackageNormalPackageFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String NORMAL_PACKAGE = "PKG";
  TextView MaxWeight;
  TextView MinWeight;
  EditText Weight_LBS;
  EditText Weight_OZ;
  private OnPackageNormalPackageSelectListener mListener;
  private String mParam1;
  private String mParam2;
  PBShipmentPackage mShipmentPackage;
  double maxWeight_KG;
  double maxWeight_LBS;
  double minWeight_KG;
  double minWeight_LBS;
  
  public static PBPackageNormalPackageFragment newInstance(String paramString1, String paramString2)
  {
    PBPackageNormalPackageFragment localPBPackageNormalPackageFragment = new PBPackageNormalPackageFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBPackageNormalPackageFragment.setArguments(localBundle);
    return localPBPackageNormalPackageFragment;
  }
  
  boolean checkWeightLimits()
  {
    double d1 = 0.0D;
    double d2 = 0.0D;
    if (!this.Weight_LBS.getText().toString().equals("")) {
      d1 = Double.parseDouble(this.Weight_LBS.getText().toString());
    }
    if (!this.Weight_OZ.getText().toString().equals("")) {
      d2 = Double.parseDouble(this.Weight_OZ.getText().toString());
    }
    d1 = UtilManager.weightLBtoKG(d1) + UtilManager.weightOZtoKG(d2);
    PBWeightRules localPBWeightRules = this.mShipmentPackage.getWeightRules();
    if ((d1 >= this.minWeight_KG) && (d1 <= this.maxWeight_KG))
    {
      localPBWeightRules.setWeight(d1);
      this.mShipmentPackage.setWeightRules(localPBWeightRules);
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      displayToast("Weight is not in limits");
    }
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnPackageNormalPackageSelectListener))
    {
      this.mListener = ((OnPackageNormalPackageSelectListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnPackageNormalPackageSelectListener");
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    onNextButtonInteraction();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968642, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Select Packaging");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Next");
    this.Weight_LBS = ((EditText)paramLayoutInflater.findViewById(2131624164));
    this.Weight_OZ = ((EditText)paramLayoutInflater.findViewById(2131624165));
    this.MinWeight = ((TextView)paramLayoutInflater.findViewById(2131624166));
    this.MaxWeight = ((TextView)paramLayoutInflater.findViewById(2131624167));
    paramViewGroup = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.PACKAGE);
    this.mShipmentPackage = new PBShipmentPackage();
    paramViewGroup = paramViewGroup.iterator();
    while (paramViewGroup.hasNext())
    {
      paramBundle = (PBShipmentPackage)paramViewGroup.next();
      if (paramBundle.getPackageId().equals("PKG")) {
        this.mShipmentPackage = paramBundle;
      }
    }
    paramViewGroup = this.mShipmentPackage.getWeightRules();
    this.maxWeight_KG = paramViewGroup.getMaxWeightAllowedKg();
    this.minWeight_KG = paramViewGroup.getMinWeightAllowedKg();
    this.minWeight_LBS = UtilManager.weightKGtoLBS(this.minWeight_KG);
    this.maxWeight_LBS = UtilManager.weightKGtoLBS(this.maxWeight_KG);
    this.MinWeight.setText(String.format("min weight: %.02f lbs", new Object[] { Double.valueOf(this.minWeight_LBS) }));
    this.MaxWeight.setText(String.format("max weight: %.02f lbs", new Object[] { Double.valueOf(this.maxWeight_LBS) }));
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onNextButtonInteraction()
  {
    if ((this.mListener != null) && (checkWeightLimits())) {
      this.mListener.onPackageNormalSelected("action_next_button", this.mShipmentPackage);
    }
  }
  
  public static abstract interface OnPackageNormalPackageSelectListener
  {
    public abstract void onPackageNormalSelected(String paramString, PBShipmentPackage paramPBShipmentPackage);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBPackageNormalPackageFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */