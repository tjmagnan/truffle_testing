package com.testfairy.f;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.m.b;
import java.io.File;
import java.io.FilenameFilter;
import org.json.JSONObject;

public class c
{
  private static boolean a = false;
  private Thread.UncaughtExceptionHandler b = null;
  private Thread.UncaughtExceptionHandler c = null;
  private final b d;
  private final com.testfairy.a.a e;
  private final com.testfairy.k.d f;
  private final Thread.UncaughtExceptionHandler g = new Thread.UncaughtExceptionHandler()
  {
    public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
    {
      c.a(c.this, paramAnonymousThread, paramAnonymousThrowable);
    }
  };
  
  public c(b paramb, com.testfairy.a.a parama)
  {
    this(paramb, parama, null);
  }
  
  public c(b paramb, com.testfairy.a.a parama, com.testfairy.k.d paramd)
  {
    this.d = paramb;
    this.e = parama;
    this.f = paramd;
  }
  
  private static void a(String paramString, Throwable paramThrowable)
  {
    Log.e(e.a, paramString, paramThrowable);
  }
  
  private void a(Thread paramThread, Throwable paramThrowable)
  {
    if (a)
    {
      this.c.uncaughtException(paramThread, paramThrowable);
      return;
    }
    a = true;
    if (!this.e.f())
    {
      Log.v(e.a, "Uncaught exception before session started");
      c(paramThread, paramThrowable);
    }
    for (;;)
    {
      this.b.uncaughtException(paramThread, paramThrowable);
      break;
      b(paramThread, paramThrowable);
    }
  }
  
  public static boolean a()
  {
    return a;
  }
  
  private void b(Thread paramThread, Throwable paramThrowable)
  {
    try
    {
      f localf = new com/testfairy/f/f;
      localf.<init>(paramThread, paramThrowable, this.e.d(), this.e.b(), this.e.c(), this.f);
      localf.start();
      localf.join();
      this.d.a();
      return;
    }
    catch (InterruptedException paramThread)
    {
      for (;;)
      {
        Log.v(e.a, "InterruptException during a crash", paramThread);
      }
    }
  }
  
  private void c(String paramString)
  {
    try
    {
      Object localObject1 = e.a;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Log.v((String)localObject1, "Sending " + paramString);
      localObject1 = new java/io/File;
      ((File)localObject1).<init>(paramString);
      localObject2 = com.testfairy.p.d.a((File)localObject1);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      d("Read this from file: '" + (String)localObject2 + "'");
      localObject3 = new org/json/JSONObject;
      ((JSONObject)localObject3).<init>((String)localObject2);
      if (!((JSONObject)localObject3).has("sessionToken")) {
        d("NO SESSION TOKEN!");
      }
      for (;;)
      {
        return;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        d("Sending crash report with sessionToken " + ((JSONObject)localObject3).getString("sessionToken"));
        localObject2 = new com/testfairy/h/f;
        ((com.testfairy.h.f)localObject2).<init>();
        ((com.testfairy.h.f)localObject2).a("sessionToken", ((JSONObject)localObject3).getString("sessionToken"));
        ((com.testfairy.h.f)localObject2).a("data", ((JSONObject)localObject3).getString("data"));
        localObject3 = new com/testfairy/f/c$2;
        ((2)localObject3).<init>(this, (File)localObject1);
        if (this.f != null) {
          break;
        }
        localObject1 = new com/testfairy/k/d;
        ((com.testfairy.k.d)localObject1).<init>(this.e.b().f());
        ((com.testfairy.k.d)localObject1).c((com.testfairy.h.f)localObject2, (com.testfairy.h.c)localObject3);
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2;
        Object localObject3;
        a("Could not read stack trace data from " + paramString, localException);
        continue;
        this.f.c((com.testfairy.h.f)localObject2, (com.testfairy.h.c)localObject3);
      }
    }
  }
  
  private void c(Thread paramThread, Throwable paramThrowable)
  {
    try
    {
      a locala = new com/testfairy/f/a;
      locala.<init>(paramThread, paramThrowable, this.e.b(), this.e.e(), this.f);
      locala.start();
      locala.join();
      return;
    }
    catch (Throwable paramThread)
    {
      for (;;)
      {
        Log.v(e.a, "Exception within a crash, how unfortunate", paramThread);
      }
    }
  }
  
  private static void d(String paramString)
  {
    Log.v(e.a, paramString);
  }
  
  public void a(String paramString)
  {
    try
    {
      Object localObject1 = e.a;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Log.v((String)localObject1, "Absolute path: " + paramString);
      if (paramString != null)
      {
        localObject1 = new java/io/File;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((File)localObject1).<init>(paramString + "/");
        localObject2 = new com/testfairy/p/j;
        ((com.testfairy.p.j)localObject2).<init>("testfairy-stacktrace");
        localObject1 = ((File)localObject1).list((FilenameFilter)localObject2);
        int j;
        int i;
        if ((localObject1 != null) && (localObject1.length > 0))
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          d("Found " + localObject1.length + " traces");
          j = localObject1.length;
          i = 0;
        }
        while (i < j)
        {
          localObject2 = localObject1[i];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          c(paramString + "/" + (String)localObject2);
          i++;
          continue;
          d("No previous stack traces found");
        }
      }
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        a("Can't send old exceptions", paramString);
      }
    }
  }
  
  public void b()
  {
    if (Thread.getDefaultUncaughtExceptionHandler() != this.g)
    {
      this.c = this.b;
      this.b = Thread.getDefaultUncaughtExceptionHandler();
      Thread.setDefaultUncaughtExceptionHandler(this.g);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */