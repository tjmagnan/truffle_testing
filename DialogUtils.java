package com.afollestad.materialdialogs.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ArrayRes;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;

public class DialogUtils
{
  @ColorInt
  public static int adjustAlpha(@ColorInt int paramInt, float paramFloat)
  {
    return Color.argb(Math.round(Color.alpha(paramInt) * paramFloat), Color.red(paramInt), Color.green(paramInt), Color.blue(paramInt));
  }
  
  public static ColorStateList getActionTextColorStateList(Context paramContext, @ColorRes int paramInt)
  {
    TypedValue localTypedValue = new TypedValue();
    paramContext.getResources().getValue(paramInt, localTypedValue, true);
    if ((localTypedValue.type >= 28) && (localTypedValue.type <= 31)) {
      paramContext = getActionTextStateList(paramContext, localTypedValue.data);
    }
    for (;;)
    {
      return paramContext;
      if (Build.VERSION.SDK_INT <= 22) {
        paramContext = paramContext.getResources().getColorStateList(paramInt);
      } else {
        paramContext = paramContext.getColorStateList(paramInt);
      }
    }
  }
  
  public static ColorStateList getActionTextStateList(Context paramContext, int paramInt)
  {
    int j = resolveColor(paramContext, 16842806);
    int i = paramInt;
    if (paramInt == 0) {
      i = j;
    }
    paramContext = new int[] { -16842910 };
    int[] arrayOfInt = new int[0];
    paramInt = adjustAlpha(i, 0.4F);
    return new ColorStateList(new int[][] { paramContext, arrayOfInt }, new int[] { paramInt, i });
  }
  
  @ColorInt
  public static int getColor(Context paramContext, @ColorRes int paramInt)
  {
    return ContextCompat.getColor(paramContext, paramInt);
  }
  
  public static int[] getColorArray(@NonNull Context paramContext, @ArrayRes int paramInt)
  {
    if (paramInt == 0) {
      paramContext = null;
    }
    for (;;)
    {
      return paramContext;
      TypedArray localTypedArray = paramContext.getResources().obtainTypedArray(paramInt);
      paramContext = new int[localTypedArray.length()];
      for (paramInt = 0; paramInt < localTypedArray.length(); paramInt++) {
        paramContext[paramInt] = localTypedArray.getColor(paramInt, 0);
      }
      localTypedArray.recycle();
    }
  }
  
  @ColorInt
  public static int getDisabledColor(Context paramContext)
  {
    if (isColorDark(resolveColor(paramContext, 16842806))) {}
    for (int i = -16777216;; i = -1) {
      return adjustAlpha(i, 0.3F);
    }
  }
  
  private static int gravityEnumToAttrInt(GravityEnum paramGravityEnum)
  {
    int i;
    switch (paramGravityEnum)
    {
    default: 
      i = 0;
    }
    for (;;)
    {
      return i;
      i = 1;
      continue;
      i = 2;
    }
  }
  
  public static void hideKeyboard(@NonNull DialogInterface paramDialogInterface, @NonNull MaterialDialog.Builder paramBuilder)
  {
    paramDialogInterface = (MaterialDialog)paramDialogInterface;
    if (paramDialogInterface.getInputEditText() == null) {}
    label67:
    for (;;)
    {
      return;
      paramBuilder = (InputMethodManager)paramBuilder.getContext().getSystemService("input_method");
      if (paramBuilder != null)
      {
        View localView = paramDialogInterface.getCurrentFocus();
        if (localView != null) {}
        for (paramDialogInterface = localView.getWindowToken();; paramDialogInterface = paramDialogInterface.getView().getWindowToken())
        {
          if (paramDialogInterface == null) {
            break label67;
          }
          paramBuilder.hideSoftInputFromWindow(paramDialogInterface, 0);
          break;
        }
      }
    }
  }
  
  public static boolean isColorDark(@ColorInt int paramInt)
  {
    if (1.0D - (0.299D * Color.red(paramInt) + 0.587D * Color.green(paramInt) + 0.114D * Color.blue(paramInt)) / 255.0D >= 0.5D) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static <T> boolean isIn(@NonNull T paramT, @Nullable T[] paramArrayOfT)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramArrayOfT != null)
    {
      if (paramArrayOfT.length == 0) {
        bool1 = bool2;
      }
    }
    else {
      return bool1;
    }
    int j = paramArrayOfT.length;
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= j) {
        break;
      }
      if (paramArrayOfT[i].equals(paramT))
      {
        bool1 = true;
        break;
      }
    }
  }
  
  /* Error */
  public static ColorStateList resolveActionTextColorStateList(Context paramContext, @AttrRes int paramInt, ColorStateList paramColorStateList)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 205	android/content/Context:getTheme	()Landroid/content/res/Resources$Theme;
    //   4: iconst_1
    //   5: newarray <illegal type>
    //   7: dup
    //   8: iconst_0
    //   9: iload_1
    //   10: iastore
    //   11: invokevirtual 211	android/content/res/Resources$Theme:obtainStyledAttributes	([I)Landroid/content/res/TypedArray;
    //   14: astore_3
    //   15: aload_3
    //   16: iconst_0
    //   17: invokevirtual 215	android/content/res/TypedArray:peekValue	(I)Landroid/util/TypedValue;
    //   20: astore 4
    //   22: aload 4
    //   24: ifnonnull +9 -> 33
    //   27: aload_3
    //   28: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   31: aload_2
    //   32: areturn
    //   33: aload 4
    //   35: getfield 65	android/util/TypedValue:type	I
    //   38: bipush 28
    //   40: if_icmplt +30 -> 70
    //   43: aload 4
    //   45: getfield 65	android/util/TypedValue:type	I
    //   48: bipush 31
    //   50: if_icmpgt +20 -> 70
    //   53: aload_0
    //   54: aload 4
    //   56: getfield 68	android/util/TypedValue:data	I
    //   59: invokestatic 71	com/afollestad/materialdialogs/util/DialogUtils:getActionTextStateList	(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    //   62: astore_2
    //   63: aload_3
    //   64: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   67: goto -36 -> 31
    //   70: aload_3
    //   71: iconst_0
    //   72: invokevirtual 216	android/content/res/TypedArray:getColorStateList	(I)Landroid/content/res/ColorStateList;
    //   75: astore_0
    //   76: aload_0
    //   77: ifnull +12 -> 89
    //   80: aload_3
    //   81: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   84: aload_0
    //   85: astore_2
    //   86: goto -55 -> 31
    //   89: aload_3
    //   90: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   93: goto -62 -> 31
    //   96: astore_0
    //   97: aload_3
    //   98: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   101: aload_0
    //   102: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	paramContext	Context
    //   0	103	1	paramInt	int
    //   0	103	2	paramColorStateList	ColorStateList
    //   14	84	3	localTypedArray	TypedArray
    //   20	35	4	localTypedValue	TypedValue
    // Exception table:
    //   from	to	target	type
    //   15	22	96	finally
    //   33	63	96	finally
    //   70	76	96	finally
  }
  
  public static boolean resolveBoolean(Context paramContext, @AttrRes int paramInt)
  {
    return resolveBoolean(paramContext, paramInt, false);
  }
  
  public static boolean resolveBoolean(Context paramContext, @AttrRes int paramInt, boolean paramBoolean)
  {
    paramContext = paramContext.getTheme().obtainStyledAttributes(new int[] { paramInt });
    try
    {
      paramBoolean = paramContext.getBoolean(0, paramBoolean);
      return paramBoolean;
    }
    finally
    {
      paramContext.recycle();
    }
  }
  
  @ColorInt
  public static int resolveColor(Context paramContext, @AttrRes int paramInt)
  {
    return resolveColor(paramContext, paramInt, 0);
  }
  
  @ColorInt
  public static int resolveColor(Context paramContext, @AttrRes int paramInt1, int paramInt2)
  {
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { paramInt1 });
    try
    {
      paramInt1 = localTypedArray.getColor(0, paramInt2);
      return paramInt1;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  public static int resolveDimension(Context paramContext, @AttrRes int paramInt)
  {
    return resolveDimension(paramContext, paramInt, -1);
  }
  
  private static int resolveDimension(Context paramContext, @AttrRes int paramInt1, int paramInt2)
  {
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { paramInt1 });
    try
    {
      paramInt1 = localTypedArray.getDimensionPixelSize(0, paramInt2);
      return paramInt1;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  public static Drawable resolveDrawable(Context paramContext, @AttrRes int paramInt)
  {
    return resolveDrawable(paramContext, paramInt, null);
  }
  
  private static Drawable resolveDrawable(Context paramContext, @AttrRes int paramInt, Drawable paramDrawable)
  {
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { paramInt });
    try
    {
      paramContext = localTypedArray.getDrawable(0);
      Object localObject = paramContext;
      if (paramContext == null)
      {
        localObject = paramContext;
        if (paramDrawable != null) {
          localObject = paramDrawable;
        }
      }
      return (Drawable)localObject;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  /* Error */
  public static GravityEnum resolveGravityEnum(Context paramContext, @AttrRes int paramInt, GravityEnum paramGravityEnum)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 205	android/content/Context:getTheme	()Landroid/content/res/Resources$Theme;
    //   4: iconst_1
    //   5: newarray <illegal type>
    //   7: dup
    //   8: iconst_0
    //   9: iload_1
    //   10: iastore
    //   11: invokevirtual 211	android/content/res/Resources$Theme:obtainStyledAttributes	([I)Landroid/content/res/TypedArray;
    //   14: astore_3
    //   15: aload_3
    //   16: iconst_0
    //   17: aload_2
    //   18: invokestatic 247	com/afollestad/materialdialogs/util/DialogUtils:gravityEnumToAttrInt	(Lcom/afollestad/materialdialogs/GravityEnum;)I
    //   21: invokevirtual 250	android/content/res/TypedArray:getInt	(II)I
    //   24: tableswitch	default:+24->48, 1:+34->58, 2:+45->69
    //   48: getstatic 254	com/afollestad/materialdialogs/GravityEnum:START	Lcom/afollestad/materialdialogs/GravityEnum;
    //   51: astore_0
    //   52: aload_3
    //   53: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   56: aload_0
    //   57: areturn
    //   58: getstatic 257	com/afollestad/materialdialogs/GravityEnum:CENTER	Lcom/afollestad/materialdialogs/GravityEnum;
    //   61: astore_0
    //   62: aload_3
    //   63: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   66: goto -10 -> 56
    //   69: getstatic 260	com/afollestad/materialdialogs/GravityEnum:END	Lcom/afollestad/materialdialogs/GravityEnum;
    //   72: astore_0
    //   73: aload_3
    //   74: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   77: goto -21 -> 56
    //   80: astore_0
    //   81: aload_3
    //   82: invokevirtual 122	android/content/res/TypedArray:recycle	()V
    //   85: aload_0
    //   86: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	87	0	paramContext	Context
    //   0	87	1	paramInt	int
    //   0	87	2	paramGravityEnum	GravityEnum
    //   14	68	3	localTypedArray	TypedArray
    // Exception table:
    //   from	to	target	type
    //   15	48	80	finally
    //   48	52	80	finally
    //   58	62	80	finally
    //   69	73	80	finally
  }
  
  public static String resolveString(Context paramContext, @AttrRes int paramInt)
  {
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(paramInt, localTypedValue, true);
    return (String)localTypedValue.string;
  }
  
  public static void setBackgroundCompat(View paramView, Drawable paramDrawable)
  {
    if (Build.VERSION.SDK_INT < 16) {
      paramView.setBackgroundDrawable(paramDrawable);
    }
    for (;;)
    {
      return;
      paramView.setBackground(paramDrawable);
    }
  }
  
  public static void showKeyboard(@NonNull DialogInterface paramDialogInterface, @NonNull final MaterialDialog.Builder paramBuilder)
  {
    paramDialogInterface = (MaterialDialog)paramDialogInterface;
    if (paramDialogInterface.getInputEditText() == null) {}
    for (;;)
    {
      return;
      paramDialogInterface.getInputEditText().post(new Runnable()
      {
        public void run()
        {
          this.val$dialog.getInputEditText().requestFocus();
          InputMethodManager localInputMethodManager = (InputMethodManager)paramBuilder.getContext().getSystemService("input_method");
          if (localInputMethodManager != null) {
            localInputMethodManager.showSoftInput(this.val$dialog.getInputEditText(), 1);
          }
        }
      });
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\util\DialogUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */