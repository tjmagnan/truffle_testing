package com.afollestad.materialdialogs.color;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.afollestad.materialdialogs.Theme;
import com.afollestad.materialdialogs.commons.R.attr;
import com.afollestad.materialdialogs.commons.R.dimen;
import com.afollestad.materialdialogs.commons.R.drawable;
import com.afollestad.materialdialogs.commons.R.id;
import com.afollestad.materialdialogs.commons.R.layout;
import com.afollestad.materialdialogs.commons.R.string;
import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;

public class ColorChooserDialog
  extends DialogFragment
  implements View.OnClickListener, View.OnLongClickListener
{
  public static final String TAG_ACCENT = "[MD_COLOR_CHOOSER]";
  public static final String TAG_CUSTOM = "[MD_COLOR_CHOOSER]";
  public static final String TAG_PRIMARY = "[MD_COLOR_CHOOSER]";
  private ColorCallback callback;
  private int circleSize;
  private View colorChooserCustomFrame;
  @Nullable
  private int[][] colorsSub;
  private int[] colorsTop;
  private EditText customColorHex;
  private View customColorIndicator;
  private SeekBar.OnSeekBarChangeListener customColorRgbListener;
  private TextWatcher customColorTextWatcher;
  private SeekBar customSeekA;
  private TextView customSeekAValue;
  private SeekBar customSeekB;
  private TextView customSeekBValue;
  private SeekBar customSeekG;
  private TextView customSeekGValue;
  private SeekBar customSeekR;
  private TextView customSeekRValue;
  private GridView grid;
  private int selectedCustomColor;
  
  private void dismissIfNecessary(AppCompatActivity paramAppCompatActivity, String paramString)
  {
    paramString = paramAppCompatActivity.getSupportFragmentManager().findFragmentByTag(paramString);
    if (paramString != null)
    {
      ((DialogFragment)paramString).dismiss();
      paramAppCompatActivity.getSupportFragmentManager().beginTransaction().remove(paramString).commit();
    }
  }
  
  private void findSubIndexForColor(int paramInt1, int paramInt2)
  {
    if ((this.colorsSub == null) || (this.colorsSub.length - 1 < paramInt1)) {}
    label53:
    for (;;)
    {
      return;
      int[] arrayOfInt = this.colorsSub[paramInt1];
      for (paramInt1 = 0;; paramInt1++)
      {
        if (paramInt1 >= arrayOfInt.length) {
          break label53;
        }
        if (arrayOfInt[paramInt1] == paramInt2)
        {
          subIndex(paramInt1);
          break;
        }
      }
    }
  }
  
  @Nullable
  public static ColorChooserDialog findVisible(@NonNull AppCompatActivity paramAppCompatActivity, String paramString)
  {
    paramAppCompatActivity = paramAppCompatActivity.getSupportFragmentManager().findFragmentByTag(paramString);
    if ((paramAppCompatActivity != null) && ((paramAppCompatActivity instanceof ColorChooserDialog))) {}
    for (paramAppCompatActivity = (ColorChooserDialog)paramAppCompatActivity;; paramAppCompatActivity = null) {
      return paramAppCompatActivity;
    }
  }
  
  private void generateColors()
  {
    Builder localBuilder = getBuilder();
    if (localBuilder.colorsTop != null)
    {
      this.colorsTop = localBuilder.colorsTop;
      this.colorsSub = localBuilder.colorsSub;
    }
    for (;;)
    {
      return;
      if (localBuilder.accentMode)
      {
        this.colorsTop = ColorPalette.ACCENT_COLORS;
        this.colorsSub = ColorPalette.ACCENT_COLORS_SUB;
      }
      else
      {
        this.colorsTop = ColorPalette.PRIMARY_COLORS;
        this.colorsSub = ColorPalette.PRIMARY_COLORS_SUB;
      }
    }
  }
  
  private Builder getBuilder()
  {
    if ((getArguments() == null) || (!getArguments().containsKey("builder"))) {}
    for (Builder localBuilder = null;; localBuilder = (Builder)getArguments().getSerializable("builder")) {
      return localBuilder;
    }
  }
  
  @ColorInt
  private int getSelectedColor()
  {
    int j;
    if ((this.colorChooserCustomFrame != null) && (this.colorChooserCustomFrame.getVisibility() == 0))
    {
      j = this.selectedCustomColor;
      return j;
    }
    int i = 0;
    if (subIndex() > -1) {
      i = this.colorsSub[topIndex()][subIndex()];
    }
    for (;;)
    {
      j = i;
      if (i != 0) {
        break;
      }
      i = 0;
      if (Build.VERSION.SDK_INT >= 21) {
        i = DialogUtils.resolveColor(getActivity(), 16843829);
      }
      j = DialogUtils.resolveColor(getActivity(), R.attr.colorAccent, i);
      break;
      if (topIndex() > -1) {
        i = this.colorsTop[topIndex()];
      }
    }
  }
  
  private void invalidate()
  {
    if (this.grid.getAdapter() == null)
    {
      this.grid.setAdapter(new ColorGridAdapter());
      this.grid.setSelector(ResourcesCompat.getDrawable(getResources(), R.drawable.md_transparent, null));
    }
    for (;;)
    {
      if (getDialog() != null) {
        getDialog().setTitle(getTitle());
      }
      return;
      ((BaseAdapter)this.grid.getAdapter()).notifyDataSetChanged();
    }
  }
  
  private void invalidateDynamicButtonColors()
  {
    MaterialDialog localMaterialDialog = (MaterialDialog)getDialog();
    if (localMaterialDialog == null) {}
    for (;;)
    {
      return;
      if (getBuilder().dynamicButtonColor)
      {
        int j = getSelectedColor();
        int i;
        if (Color.alpha(j) >= 64)
        {
          i = j;
          if (Color.red(j) > 247)
          {
            i = j;
            if (Color.green(j) > 247)
            {
              i = j;
              if (Color.blue(j) <= 247) {}
            }
          }
        }
        else
        {
          i = Color.parseColor("#DEDEDE");
        }
        if (getBuilder().dynamicButtonColor)
        {
          localMaterialDialog.getActionButton(DialogAction.POSITIVE).setTextColor(i);
          localMaterialDialog.getActionButton(DialogAction.NEGATIVE).setTextColor(i);
          localMaterialDialog.getActionButton(DialogAction.NEUTRAL).setTextColor(i);
        }
        if (this.customSeekR != null)
        {
          if (this.customSeekA.getVisibility() == 0) {
            MDTintHelper.setTint(this.customSeekA, i);
          }
          MDTintHelper.setTint(this.customSeekR, i);
          MDTintHelper.setTint(this.customSeekG, i);
          MDTintHelper.setTint(this.customSeekB, i);
        }
      }
    }
  }
  
  private void isInSub(boolean paramBoolean)
  {
    getArguments().putBoolean("in_sub", paramBoolean);
  }
  
  private boolean isInSub()
  {
    return getArguments().getBoolean("in_sub", false);
  }
  
  private int subIndex()
  {
    int i = -1;
    if (this.colorsSub == null) {}
    for (;;)
    {
      return i;
      i = getArguments().getInt("sub_index", -1);
    }
  }
  
  private void subIndex(int paramInt)
  {
    if (this.colorsSub == null) {}
    for (;;)
    {
      return;
      getArguments().putInt("sub_index", paramInt);
    }
  }
  
  private void toggleCustom(MaterialDialog paramMaterialDialog)
  {
    MaterialDialog localMaterialDialog = paramMaterialDialog;
    if (paramMaterialDialog == null) {
      localMaterialDialog = (MaterialDialog)getDialog();
    }
    if (this.grid.getVisibility() == 0)
    {
      localMaterialDialog.setTitle(getBuilder().customBtn);
      localMaterialDialog.setActionButton(DialogAction.NEUTRAL, getBuilder().presetsBtn);
      localMaterialDialog.setActionButton(DialogAction.NEGATIVE, getBuilder().cancelBtn);
      this.grid.setVisibility(4);
      this.colorChooserCustomFrame.setVisibility(0);
      this.customColorTextWatcher = new TextWatcher()
      {
        public void afterTextChanged(Editable paramAnonymousEditable) {}
        
        public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3) {}
        
        public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
        {
          try
          {
            ColorChooserDialog localColorChooserDialog = ColorChooserDialog.this;
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            ColorChooserDialog.access$902(localColorChooserDialog, Color.parseColor("#" + paramAnonymousCharSequence.toString()));
            ColorChooserDialog.this.customColorIndicator.setBackgroundColor(ColorChooserDialog.this.selectedCustomColor);
            if (ColorChooserDialog.this.customSeekA.getVisibility() == 0)
            {
              paramAnonymousInt1 = Color.alpha(ColorChooserDialog.this.selectedCustomColor);
              ColorChooserDialog.this.customSeekA.setProgress(paramAnonymousInt1);
              ColorChooserDialog.this.customSeekAValue.setText(String.format(Locale.US, "%d", new Object[] { Integer.valueOf(paramAnonymousInt1) }));
            }
            if (ColorChooserDialog.this.customSeekA.getVisibility() == 0)
            {
              paramAnonymousInt1 = Color.alpha(ColorChooserDialog.this.selectedCustomColor);
              ColorChooserDialog.this.customSeekA.setProgress(paramAnonymousInt1);
            }
            paramAnonymousInt1 = Color.red(ColorChooserDialog.this.selectedCustomColor);
            ColorChooserDialog.this.customSeekR.setProgress(paramAnonymousInt1);
            paramAnonymousInt1 = Color.green(ColorChooserDialog.this.selectedCustomColor);
            ColorChooserDialog.this.customSeekG.setProgress(paramAnonymousInt1);
            paramAnonymousInt1 = Color.blue(ColorChooserDialog.this.selectedCustomColor);
            ColorChooserDialog.this.customSeekB.setProgress(paramAnonymousInt1);
            ColorChooserDialog.this.isInSub(false);
            ColorChooserDialog.this.topIndex(-1);
            ColorChooserDialog.this.subIndex(-1);
            ColorChooserDialog.this.invalidateDynamicButtonColors();
            return;
          }
          catch (IllegalArgumentException paramAnonymousCharSequence)
          {
            for (;;)
            {
              ColorChooserDialog.access$902(ColorChooserDialog.this, -16777216);
            }
          }
        }
      };
      this.customColorHex.addTextChangedListener(this.customColorTextWatcher);
      this.customColorRgbListener = new SeekBar.OnSeekBarChangeListener()
      {
        @SuppressLint({"DefaultLocale"})
        public void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
        {
          if (paramAnonymousBoolean)
          {
            if (!ColorChooserDialog.this.getBuilder().allowUserCustomAlpha) {
              break label228;
            }
            paramAnonymousInt = Color.argb(ColorChooserDialog.this.customSeekA.getProgress(), ColorChooserDialog.this.customSeekR.getProgress(), ColorChooserDialog.this.customSeekG.getProgress(), ColorChooserDialog.this.customSeekB.getProgress());
            ColorChooserDialog.this.customColorHex.setText(String.format("%08X", new Object[] { Integer.valueOf(paramAnonymousInt) }));
          }
          for (;;)
          {
            ColorChooserDialog.this.customSeekAValue.setText(String.format("%d", new Object[] { Integer.valueOf(ColorChooserDialog.this.customSeekA.getProgress()) }));
            ColorChooserDialog.this.customSeekRValue.setText(String.format("%d", new Object[] { Integer.valueOf(ColorChooserDialog.this.customSeekR.getProgress()) }));
            ColorChooserDialog.this.customSeekGValue.setText(String.format("%d", new Object[] { Integer.valueOf(ColorChooserDialog.this.customSeekG.getProgress()) }));
            ColorChooserDialog.this.customSeekBValue.setText(String.format("%d", new Object[] { Integer.valueOf(ColorChooserDialog.this.customSeekB.getProgress()) }));
            return;
            label228:
            paramAnonymousInt = Color.rgb(ColorChooserDialog.this.customSeekR.getProgress(), ColorChooserDialog.this.customSeekG.getProgress(), ColorChooserDialog.this.customSeekB.getProgress());
            ColorChooserDialog.this.customColorHex.setText(String.format("%06X", new Object[] { Integer.valueOf(0xFFFFFF & paramAnonymousInt) }));
          }
        }
        
        public void onStartTrackingTouch(SeekBar paramAnonymousSeekBar) {}
        
        public void onStopTrackingTouch(SeekBar paramAnonymousSeekBar) {}
      };
      this.customSeekR.setOnSeekBarChangeListener(this.customColorRgbListener);
      this.customSeekG.setOnSeekBarChangeListener(this.customColorRgbListener);
      this.customSeekB.setOnSeekBarChangeListener(this.customColorRgbListener);
      if (this.customSeekA.getVisibility() == 0)
      {
        this.customSeekA.setOnSeekBarChangeListener(this.customColorRgbListener);
        this.customColorHex.setText(String.format("%08X", new Object[] { Integer.valueOf(this.selectedCustomColor) }));
      }
      for (;;)
      {
        return;
        this.customColorHex.setText(String.format("%06X", new Object[] { Integer.valueOf(0xFFFFFF & this.selectedCustomColor) }));
      }
    }
    localMaterialDialog.setTitle(getBuilder().title);
    localMaterialDialog.setActionButton(DialogAction.NEUTRAL, getBuilder().customBtn);
    if (isInSub()) {
      localMaterialDialog.setActionButton(DialogAction.NEGATIVE, getBuilder().backBtn);
    }
    for (;;)
    {
      this.grid.setVisibility(0);
      this.colorChooserCustomFrame.setVisibility(8);
      this.customColorHex.removeTextChangedListener(this.customColorTextWatcher);
      this.customColorTextWatcher = null;
      this.customSeekR.setOnSeekBarChangeListener(null);
      this.customSeekG.setOnSeekBarChangeListener(null);
      this.customSeekB.setOnSeekBarChangeListener(null);
      this.customColorRgbListener = null;
      break;
      localMaterialDialog.setActionButton(DialogAction.NEGATIVE, getBuilder().cancelBtn);
    }
  }
  
  private int topIndex()
  {
    return getArguments().getInt("top_index", -1);
  }
  
  private void topIndex(int paramInt)
  {
    if (paramInt > -1) {
      findSubIndexForColor(paramInt, this.colorsTop[paramInt]);
    }
    getArguments().putInt("top_index", paramInt);
  }
  
  @StringRes
  public int getTitle()
  {
    Builder localBuilder = getBuilder();
    if (isInSub()) {}
    for (int i = localBuilder.titleSub;; i = localBuilder.title)
    {
      int j = i;
      if (i == 0) {
        j = localBuilder.title;
      }
      return j;
    }
  }
  
  public boolean isAccentMode()
  {
    return getBuilder().accentMode;
  }
  
  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    if (!(paramActivity instanceof ColorCallback)) {
      throw new IllegalStateException("ColorChooserDialog needs to be shown from an Activity implementing ColorCallback.");
    }
    this.callback = ((ColorCallback)paramActivity);
  }
  
  public void onClick(View paramView)
  {
    int i;
    MaterialDialog localMaterialDialog;
    if (paramView.getTag() != null)
    {
      i = Integer.parseInt(((String)paramView.getTag()).split(":")[0]);
      localMaterialDialog = (MaterialDialog)getDialog();
      paramView = getBuilder();
      if (!isInSub()) {
        break label75;
      }
      subIndex(i);
    }
    for (;;)
    {
      if (paramView.allowUserCustom) {
        this.selectedCustomColor = getSelectedColor();
      }
      invalidateDynamicButtonColors();
      invalidate();
      return;
      label75:
      topIndex(i);
      if ((this.colorsSub != null) && (i < this.colorsSub.length))
      {
        localMaterialDialog.setActionButton(DialogAction.NEGATIVE, paramView.backBtn);
        isInSub(true);
      }
    }
  }
  
  @NonNull
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    if ((getArguments() == null) || (!getArguments().containsKey("builder"))) {
      throw new IllegalStateException("ColorChooserDialog should be created using its Builder interface.");
    }
    generateColors();
    int j = 0;
    int i = 0;
    int m;
    label61:
    label137:
    MaterialDialog localMaterialDialog;
    if (paramBundle != null) {
      if (!paramBundle.getBoolean("in_custom", false))
      {
        i = 1;
        m = getSelectedColor();
        this.circleSize = getResources().getDimensionPixelSize(R.dimen.md_colorchooser_circlesize);
        paramBundle = getBuilder();
        Object localObject = new MaterialDialog.Builder(getActivity()).title(getTitle()).autoDismiss(false).customView(R.layout.md_dialog_colorchooser, false).negativeText(paramBundle.cancelBtn).positiveText(paramBundle.doneBtn);
        if (!paramBundle.allowUserCustom) {
          break label726;
        }
        j = paramBundle.customBtn;
        localObject = ((MaterialDialog.Builder)localObject).neutralText(j).typeface(paramBundle.mediumFont, paramBundle.regularFont).onPositive(new MaterialDialog.SingleButtonCallback()
        {
          public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
          {
            ColorChooserDialog.this.callback.onColorSelection(ColorChooserDialog.this, ColorChooserDialog.this.getSelectedColor());
            ColorChooserDialog.this.dismiss();
          }
        }).onNegative(new MaterialDialog.SingleButtonCallback()
        {
          public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
          {
            if (ColorChooserDialog.this.isInSub())
            {
              paramAnonymousMaterialDialog.setActionButton(DialogAction.NEGATIVE, ColorChooserDialog.this.getBuilder().cancelBtn);
              ColorChooserDialog.this.isInSub(false);
              ColorChooserDialog.this.subIndex(-1);
              ColorChooserDialog.this.invalidate();
            }
            for (;;)
            {
              return;
              paramAnonymousMaterialDialog.cancel();
            }
          }
        }).onNeutral(new MaterialDialog.SingleButtonCallback()
        {
          public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
          {
            ColorChooserDialog.this.toggleCustom(paramAnonymousMaterialDialog);
          }
        }).showListener(new DialogInterface.OnShowListener()
        {
          public void onShow(DialogInterface paramAnonymousDialogInterface)
          {
            ColorChooserDialog.this.invalidateDynamicButtonColors();
          }
        });
        if (paramBundle.theme != null) {
          ((MaterialDialog.Builder)localObject).theme(paramBundle.theme);
        }
        localMaterialDialog = ((MaterialDialog.Builder)localObject).build();
        localObject = localMaterialDialog.getCustomView();
        this.grid = ((GridView)((View)localObject).findViewById(R.id.md_grid));
        if (paramBundle.allowUserCustom)
        {
          this.selectedCustomColor = m;
          this.colorChooserCustomFrame = ((View)localObject).findViewById(R.id.md_colorChooserCustomFrame);
          this.customColorHex = ((EditText)((View)localObject).findViewById(R.id.md_hexInput));
          this.customColorIndicator = ((View)localObject).findViewById(R.id.md_colorIndicator);
          this.customSeekA = ((SeekBar)((View)localObject).findViewById(R.id.md_colorA));
          this.customSeekAValue = ((TextView)((View)localObject).findViewById(R.id.md_colorAValue));
          this.customSeekR = ((SeekBar)((View)localObject).findViewById(R.id.md_colorR));
          this.customSeekRValue = ((TextView)((View)localObject).findViewById(R.id.md_colorRValue));
          this.customSeekG = ((SeekBar)((View)localObject).findViewById(R.id.md_colorG));
          this.customSeekGValue = ((TextView)((View)localObject).findViewById(R.id.md_colorGValue));
          this.customSeekB = ((SeekBar)((View)localObject).findViewById(R.id.md_colorB));
          this.customSeekBValue = ((TextView)((View)localObject).findViewById(R.id.md_colorBValue));
          if (paramBundle.allowUserCustomAlpha) {
            break label731;
          }
          ((View)localObject).findViewById(R.id.md_colorALabel).setVisibility(8);
          this.customSeekA.setVisibility(8);
          this.customSeekAValue.setVisibility(8);
          this.customColorHex.setHint("2196F3");
          this.customColorHex.setFilters(new InputFilter[] { new InputFilter.LengthFilter(6) });
        }
      }
    }
    for (;;)
    {
      if (i == 0) {
        toggleCustom(localMaterialDialog);
      }
      invalidate();
      return localMaterialDialog;
      i = 0;
      break;
      if (getBuilder().setPreselectionColor)
      {
        int i1 = getBuilder().preselectColor;
        m = i1;
        if (i1 == 0) {
          break label61;
        }
        int n = 0;
        label542:
        i = j;
        m = i1;
        if (n >= this.colorsTop.length) {
          break label61;
        }
        if (this.colorsTop[n] == i1)
        {
          i = 1;
          topIndex(n);
          if (getBuilder().accentMode)
          {
            subIndex(2);
            m = i1;
            break label61;
          }
          if (this.colorsSub != null)
          {
            findSubIndexForColor(n, i1);
            m = i1;
            break label61;
          }
          subIndex(5);
          m = i1;
          break label61;
        }
        int k = j;
        if (this.colorsSub != null) {}
        for (i = 0;; i++)
        {
          k = j;
          if (i < this.colorsSub[n].length)
          {
            if (this.colorsSub[n][i] == i1)
            {
              k = 1;
              topIndex(n);
              subIndex(i);
            }
          }
          else
          {
            i = k;
            m = i1;
            if (k != 0) {
              break;
            }
            n++;
            j = k;
            break label542;
          }
        }
      }
      m = -16777216;
      i = 1;
      break label61;
      label726:
      j = 0;
      break label137;
      label731:
      this.customColorHex.setHint("FF2196F3");
      this.customColorHex.setFilters(new InputFilter[] { new InputFilter.LengthFilter(8) });
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    if (this.callback != null) {
      this.callback.onColorChooserDismissed(this);
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    if (paramView.getTag() != null)
    {
      int i = Integer.parseInt(((String)paramView.getTag()).split(":")[1]);
      ((CircleView)paramView).showHint(i);
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("top_index", topIndex());
    paramBundle.putBoolean("in_sub", isInSub());
    paramBundle.putInt("sub_index", subIndex());
    if ((this.colorChooserCustomFrame != null) && (this.colorChooserCustomFrame.getVisibility() == 0)) {}
    for (boolean bool = true;; bool = false)
    {
      paramBundle.putBoolean("in_custom", bool);
      return;
    }
  }
  
  @NonNull
  public ColorChooserDialog show(AppCompatActivity paramAppCompatActivity)
  {
    Builder localBuilder = getBuilder();
    if (localBuilder.colorsTop != null) {}
    for (;;)
    {
      dismissIfNecessary(paramAppCompatActivity, "[MD_COLOR_CHOOSER]");
      show(paramAppCompatActivity.getSupportFragmentManager(), "[MD_COLOR_CHOOSER]");
      return this;
      if (localBuilder.accentMode) {}
    }
  }
  
  public String tag()
  {
    Object localObject = getBuilder();
    if (((Builder)localObject).tag != null) {}
    for (localObject = ((Builder)localObject).tag;; localObject = super.getTag()) {
      return (String)localObject;
    }
  }
  
  public static class Builder
    implements Serializable
  {
    boolean accentMode = false;
    boolean allowUserCustom = true;
    boolean allowUserCustomAlpha = true;
    @StringRes
    int backBtn = R.string.md_back_label;
    @StringRes
    int cancelBtn = R.string.md_cancel_label;
    @Nullable
    int[][] colorsSub;
    @Nullable
    int[] colorsTop;
    @NonNull
    final transient AppCompatActivity context;
    @StringRes
    int customBtn = R.string.md_custom_label;
    @StringRes
    int doneBtn = R.string.md_done_label;
    boolean dynamicButtonColor = true;
    @Nullable
    String mediumFont;
    @ColorInt
    int preselectColor;
    @StringRes
    int presetsBtn = R.string.md_presets_label;
    @Nullable
    String regularFont;
    boolean setPreselectionColor = false;
    @Nullable
    String tag;
    @Nullable
    Theme theme;
    @StringRes
    final int title;
    @StringRes
    int titleSub;
    
    public <ActivityType extends AppCompatActivity,  extends ColorChooserDialog.ColorCallback> Builder(@NonNull ActivityType paramActivityType, @StringRes int paramInt)
    {
      this.context = paramActivityType;
      this.title = paramInt;
    }
    
    @NonNull
    public Builder accentMode(boolean paramBoolean)
    {
      this.accentMode = paramBoolean;
      return this;
    }
    
    @NonNull
    public Builder allowUserColorInput(boolean paramBoolean)
    {
      this.allowUserCustom = paramBoolean;
      return this;
    }
    
    @NonNull
    public Builder allowUserColorInputAlpha(boolean paramBoolean)
    {
      this.allowUserCustomAlpha = paramBoolean;
      return this;
    }
    
    @NonNull
    public Builder backButton(@StringRes int paramInt)
    {
      this.backBtn = paramInt;
      return this;
    }
    
    @NonNull
    public ColorChooserDialog build()
    {
      ColorChooserDialog localColorChooserDialog = new ColorChooserDialog();
      Bundle localBundle = new Bundle();
      localBundle.putSerializable("builder", this);
      localColorChooserDialog.setArguments(localBundle);
      return localColorChooserDialog;
    }
    
    @NonNull
    public Builder cancelButton(@StringRes int paramInt)
    {
      this.cancelBtn = paramInt;
      return this;
    }
    
    @NonNull
    public Builder customButton(@StringRes int paramInt)
    {
      this.customBtn = paramInt;
      return this;
    }
    
    @NonNull
    public Builder customColors(@ArrayRes int paramInt, @Nullable int[][] paramArrayOfInt)
    {
      this.colorsTop = DialogUtils.getColorArray(this.context, paramInt);
      this.colorsSub = paramArrayOfInt;
      return this;
    }
    
    @NonNull
    public Builder customColors(@NonNull int[] paramArrayOfInt, @Nullable int[][] paramArrayOfInt1)
    {
      this.colorsTop = paramArrayOfInt;
      this.colorsSub = paramArrayOfInt1;
      return this;
    }
    
    @NonNull
    public Builder doneButton(@StringRes int paramInt)
    {
      this.doneBtn = paramInt;
      return this;
    }
    
    @NonNull
    public Builder dynamicButtonColor(boolean paramBoolean)
    {
      this.dynamicButtonColor = paramBoolean;
      return this;
    }
    
    @NonNull
    public Builder preselect(@ColorInt int paramInt)
    {
      this.preselectColor = paramInt;
      this.setPreselectionColor = true;
      return this;
    }
    
    @NonNull
    public Builder presetsButton(@StringRes int paramInt)
    {
      this.presetsBtn = paramInt;
      return this;
    }
    
    @NonNull
    public ColorChooserDialog show()
    {
      ColorChooserDialog localColorChooserDialog = build();
      localColorChooserDialog.show(this.context);
      return localColorChooserDialog;
    }
    
    @NonNull
    public Builder tag(@Nullable String paramString)
    {
      this.tag = paramString;
      return this;
    }
    
    @NonNull
    public Builder theme(@NonNull Theme paramTheme)
    {
      this.theme = paramTheme;
      return this;
    }
    
    @NonNull
    public Builder titleSub(@StringRes int paramInt)
    {
      this.titleSub = paramInt;
      return this;
    }
    
    @NonNull
    public Builder typeface(@Nullable String paramString1, @Nullable String paramString2)
    {
      this.mediumFont = paramString1;
      this.regularFont = paramString2;
      return this;
    }
  }
  
  public static abstract interface ColorCallback
  {
    public abstract void onColorChooserDismissed(@NonNull ColorChooserDialog paramColorChooserDialog);
    
    public abstract void onColorSelection(@NonNull ColorChooserDialog paramColorChooserDialog, @ColorInt int paramInt);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorChooserTag {}
  
  private class ColorGridAdapter
    extends BaseAdapter
  {
    ColorGridAdapter() {}
    
    public int getCount()
    {
      if (ColorChooserDialog.this.isInSub()) {}
      for (int i = ColorChooserDialog.this.colorsSub[ColorChooserDialog.this.topIndex()].length;; i = ColorChooserDialog.this.colorsTop.length) {
        return i;
      }
    }
    
    public Object getItem(int paramInt)
    {
      if (ColorChooserDialog.this.isInSub()) {}
      for (Integer localInteger = Integer.valueOf(ColorChooserDialog.this.colorsSub[ColorChooserDialog.this.topIndex()][paramInt]);; localInteger = Integer.valueOf(ColorChooserDialog.this.colorsTop[paramInt])) {
        return localInteger;
      }
    }
    
    public long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    @SuppressLint({"DefaultLocale"})
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      paramViewGroup = paramView;
      if (paramView == null)
      {
        paramViewGroup = new CircleView(ColorChooserDialog.this.getContext());
        paramViewGroup.setLayoutParams(new AbsListView.LayoutParams(ColorChooserDialog.this.circleSize, ColorChooserDialog.this.circleSize));
      }
      paramView = (CircleView)paramViewGroup;
      int i;
      if (ColorChooserDialog.this.isInSub())
      {
        i = ColorChooserDialog.this.colorsSub[ColorChooserDialog.this.topIndex()][paramInt];
        paramView.setBackgroundColor(i);
        if (!ColorChooserDialog.this.isInSub()) {
          break label182;
        }
        if (ColorChooserDialog.this.subIndex() != paramInt) {
          break label176;
        }
      }
      label176:
      for (boolean bool = true;; bool = false)
      {
        paramView.setSelected(bool);
        paramView.setTag(String.format("%d:%d", new Object[] { Integer.valueOf(paramInt), Integer.valueOf(i) }));
        paramView.setOnClickListener(ColorChooserDialog.this);
        paramView.setOnLongClickListener(ColorChooserDialog.this);
        return paramViewGroup;
        i = ColorChooserDialog.this.colorsTop[paramInt];
        break;
      }
      label182:
      if (ColorChooserDialog.this.topIndex() == paramInt) {}
      for (bool = true;; bool = false)
      {
        paramView.setSelected(bool);
        break;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\color\ColorChooserDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */