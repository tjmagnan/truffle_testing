package com.testfairy.e;

public class b
  implements a
{
  private int a = 80;
  private float b = 1.0F;
  private boolean c = false;
  private boolean d = false;
  private boolean e = false;
  private boolean f = false;
  private boolean g = false;
  private boolean h = false;
  private boolean i = false;
  private boolean j = false;
  private boolean k = false;
  private boolean l = false;
  private boolean m = false;
  private boolean n = false;
  private boolean o = false;
  private boolean p = false;
  private boolean q = false;
  private boolean r = false;
  private boolean s = false;
  private boolean t = false;
  private boolean u = false;
  private boolean v = false;
  private boolean w = false;
  private long x = 900000L;
  private long y = 1000L;
  
  public void a(float paramFloat)
  {
    this.b = paramFloat;
  }
  
  public void a(int paramInt)
  {
    this.a = paramInt;
  }
  
  public void a(long paramLong)
  {
    this.x = paramLong;
  }
  
  public void a(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }
  
  public boolean a()
  {
    return this.c;
  }
  
  public void b(long paramLong)
  {
    this.y = paramLong;
  }
  
  public void b(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }
  
  public boolean b()
  {
    return this.h;
  }
  
  public void c(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }
  
  public boolean c()
  {
    return this.j;
  }
  
  public void d(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  public boolean d()
  {
    return this.k;
  }
  
  public void e(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public boolean e()
  {
    return this.n;
  }
  
  public void f(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public boolean f()
  {
    return this.o;
  }
  
  public void g(boolean paramBoolean)
  {
    this.i = paramBoolean;
  }
  
  public boolean g()
  {
    return this.d;
  }
  
  public void h(boolean paramBoolean)
  {
    this.j = paramBoolean;
  }
  
  public boolean h()
  {
    return this.l;
  }
  
  public void i(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  public boolean i()
  {
    return this.m;
  }
  
  public void j(boolean paramBoolean)
  {
    this.l = paramBoolean;
  }
  
  public boolean j()
  {
    return this.i;
  }
  
  public void k(boolean paramBoolean)
  {
    this.m = paramBoolean;
  }
  
  public boolean k()
  {
    return this.e;
  }
  
  public void l(boolean paramBoolean)
  {
    this.n = paramBoolean;
  }
  
  public boolean l()
  {
    return this.f;
  }
  
  public void m(boolean paramBoolean)
  {
    this.o = paramBoolean;
  }
  
  public boolean m()
  {
    return this.g;
  }
  
  public void n(boolean paramBoolean)
  {
    this.p = paramBoolean;
  }
  
  public boolean n()
  {
    return this.p;
  }
  
  public void o(boolean paramBoolean)
  {
    this.q = paramBoolean;
  }
  
  public boolean o()
  {
    return this.q;
  }
  
  public void p(boolean paramBoolean)
  {
    this.r = paramBoolean;
  }
  
  public boolean p()
  {
    return this.r;
  }
  
  public void q(boolean paramBoolean)
  {
    this.s = paramBoolean;
  }
  
  public boolean q()
  {
    return this.s;
  }
  
  public void r(boolean paramBoolean)
  {
    this.t = paramBoolean;
  }
  
  public boolean r()
  {
    return this.t;
  }
  
  public void s(boolean paramBoolean)
  {
    this.u = paramBoolean;
  }
  
  public boolean s()
  {
    return this.w;
  }
  
  public void t(boolean paramBoolean)
  {
    this.v = paramBoolean;
  }
  
  public boolean t()
  {
    return this.u;
  }
  
  public String toString()
  {
    return String.format("Options{jpegQuality=%d, logcatEnabled=%b, memoryEnabled=%b, videoEnabled=%b, videoOnlyWifiEnabled=%b, dataOnlyWifiEnabled=%b, cpuEnabled=%b, phoneSignalEnabled=%b, gpsEnabled=%b, wifiEnabled=%b, microphoneEnabled=%b, batteryEnabled=%b, networkEnabled=%b, sessionLengthCap=%d,screenshotInterval=%d, shakeForFeedback=%b, hideUserInput=%b, recordOnBackground=%b, anonymousEnabled=%b, testerEmailVerifyNeeded=%b, anrEnabled=%b, recordAudioEnabled=%b, userInteractionEnabled=%b}", new Object[] { Integer.valueOf(this.a), Boolean.valueOf(this.c), Boolean.valueOf(this.d), Boolean.valueOf(this.e), Boolean.valueOf(this.f), Boolean.valueOf(this.g), Boolean.valueOf(this.h), Boolean.valueOf(this.i), Boolean.valueOf(this.j), Boolean.valueOf(this.k), Boolean.valueOf(this.l), Boolean.valueOf(this.n), Boolean.valueOf(this.o), Long.valueOf(this.x), Long.valueOf(this.y), Boolean.valueOf(this.p), Boolean.valueOf(this.r), Boolean.valueOf(this.s), Boolean.valueOf(this.t), Boolean.valueOf(this.u), Boolean.valueOf(this.v), Boolean.valueOf(this.m), Boolean.valueOf(this.w) });
  }
  
  public int u()
  {
    return this.a;
  }
  
  public void u(boolean paramBoolean)
  {
    this.w = paramBoolean;
  }
  
  public float v()
  {
    return this.b;
  }
  
  public long w()
  {
    return this.y;
  }
  
  public long x()
  {
    return this.x;
  }
  
  public boolean y()
  {
    return this.v;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\e\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */