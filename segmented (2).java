package info.hoang8f.android.segmented;

public final class R
{
  public static final class attr
  {
    public static final int sc_border_width = 2130772328;
    public static final int sc_checked_text_color = 2130772330;
    public static final int sc_corner_radius = 2130772327;
    public static final int sc_tint_color = 2130772329;
  }
  
  public static final class color
  {
    public static final int radio_button_selected_color = 2131492959;
    public static final int radio_button_unselected_color = 2131492960;
  }
  
  public static final class dimen
  {
    public static final int radio_button_conner_radius = 2131230973;
    public static final int radio_button_stroke_border = 2131230974;
  }
  
  public static final class drawable
  {
    public static final int button_text_color = 2130837603;
    public static final int radio_checked = 2130837691;
    public static final int radio_unchecked = 2130837692;
  }
  
  public static final class string
  {
    public static final int app_name = 2131165223;
  }
  
  public static final class style
  {
    public static final int RadioButton = 2131362016;
  }
  
  public static final class styleable
  {
    public static final int[] SegmentedGroup = { 2130772327, 2130772328, 2130772329, 2130772330 };
    public static final int SegmentedGroup_sc_border_width = 1;
    public static final int SegmentedGroup_sc_checked_text_color = 3;
    public static final int SegmentedGroup_sc_corner_radius = 0;
    public static final int SegmentedGroup_sc_tint_color = 2;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\info\hoang8f\android\segmented\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */