package tech.dcube.companion.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class CatalogEditItemFragment_ViewBinding
  extends PBTopBarFragment_ViewBinding
{
  private CatalogEditItemFragment target;
  private View view2131624097;
  
  @UiThread
  public CatalogEditItemFragment_ViewBinding(final CatalogEditItemFragment paramCatalogEditItemFragment, View paramView)
  {
    super(paramCatalogEditItemFragment, paramView);
    this.target = paramCatalogEditItemFragment;
    paramView = Utils.findRequiredView(paramView, 2131624097, "method 'hideSoftKeyboardOnClick' and method 'hideSoftKeyboardOnFocus'");
    this.view2131624097 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramCatalogEditItemFragment.hideSoftKeyboardOnClick();
      }
    });
    paramView.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        paramCatalogEditItemFragment.hideSoftKeyboardOnFocus(paramAnonymousBoolean);
      }
    });
  }
  
  public void unbind()
  {
    if (this.target == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    this.view2131624097.setOnClickListener(null);
    this.view2131624097.setOnFocusChangeListener(null);
    this.view2131624097 = null;
    super.unbind();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\CatalogEditItemFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */