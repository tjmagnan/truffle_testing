package com.testfairy;

import java.io.Serializable;

public class FeedbackOptions
  implements Serializable
{
  private final boolean a;
  private final boolean b;
  private final transient Callback c;
  private final String d;
  
  private FeedbackOptions(boolean paramBoolean1, boolean paramBoolean2, Callback paramCallback, String paramString)
  {
    this.a = paramBoolean1;
    this.b = paramBoolean2;
    this.c = paramCallback;
    this.d = paramString;
  }
  
  public boolean a()
  {
    return this.a;
  }
  
  public boolean b()
  {
    return this.b;
  }
  
  public Callback c()
  {
    return this.c;
  }
  
  public String d()
  {
    return this.d;
  }
  
  public static class Builder
  {
    private boolean a = true;
    private boolean b = false;
    private FeedbackOptions.Callback c = new FeedbackOptions.Callback()
    {
      public void onFeedbackCancelled() {}
      
      public void onFeedbackFailed(int paramAnonymousInt, FeedbackContent paramAnonymousFeedbackContent) {}
      
      public void onFeedbackSent(FeedbackContent paramAnonymousFeedbackContent) {}
    };
    private String d;
    
    public FeedbackOptions build()
    {
      return new FeedbackOptions(this.a, this.b, this.c, this.d, null);
    }
    
    public Builder setBrowserUrl(String paramString)
    {
      String str = paramString;
      if (!paramString.startsWith("http://"))
      {
        str = paramString;
        if (!paramString.startsWith("https://")) {
          str = "http://" + paramString;
        }
      }
      this.d = str;
      return this;
    }
    
    public Builder setCallback(FeedbackOptions.Callback paramCallback)
    {
      this.c = paramCallback;
      return this;
    }
    
    public Builder setEmailFieldVisible(boolean paramBoolean)
    {
      this.a = paramBoolean;
      return this;
    }
    
    public Builder setEmailMandatory(boolean paramBoolean)
    {
      if (paramBoolean) {
        setEmailFieldVisible(true);
      }
      this.b = paramBoolean;
      return this;
    }
  }
  
  public static abstract interface Callback
  {
    public static final int REASON_NETWORK_ERROR = 1;
    
    public abstract void onFeedbackCancelled();
    
    public abstract void onFeedbackFailed(int paramInt, FeedbackContent paramFeedbackContent);
    
    public abstract void onFeedbackSent(FeedbackContent paramFeedbackContent);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\FeedbackOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */