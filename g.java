package com.testfairy;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class g
{
  public static final int A = 1;
  public static final int B = 2;
  public static final int C = 3;
  public static final int D = 4;
  public static final int E = 5;
  public static final int F = 7;
  public static final int G = 9;
  public static final int H = 10;
  public static final int I = 11;
  public static final int J = 12;
  public static final int K = 13;
  public static final int L = 14;
  public static final int M = 15;
  public static final int N = 16;
  public static final int O = 17;
  public static final int P = 18;
  public static final int Q = 19;
  public static final int R = 20;
  private static int X = 1;
  public static final int a = 0;
  public static final int b = 1;
  public static final int c = 2;
  public static final int d = 3;
  public static final int e = 5;
  public static final int f = 6;
  public static final int g = 7;
  public static final int h = 8;
  public static final int i = 9;
  public static final int j = 10;
  public static final int k = 11;
  public static final int l = 12;
  public static final int m = 13;
  public static final int n = 14;
  public static final int o = 15;
  public static final int p = 16;
  public static final int q = 17;
  public static final int r = 18;
  public static final int s = 19;
  public static final int t = 20;
  public static final int u = 21;
  public static final int v = 22;
  public static final int w = 23;
  public static final int x = 24;
  public static final int y = 25;
  public static final int z = 26;
  private Map S = new HashMap() {};
  private int T = b();
  private int U;
  private double V = System.currentTimeMillis();
  private JSONObject W;
  
  public g(int paramInt)
  {
    this.U = paramInt;
    this.W = null;
  }
  
  public g(int paramInt, Map paramMap)
  {
    this(paramInt);
    this.W = new JSONObject(paramMap);
  }
  
  public g(int paramInt, JSONObject paramJSONObject)
  {
    this(paramInt);
    this.W = paramJSONObject;
  }
  
  private int b()
  {
    try
    {
      int i1 = X;
      X = i1 + 1;
      return i1;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public JSONObject a()
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("id", this.T);
    localJSONObject.put("type", this.U);
    localJSONObject.put("ts", this.V);
    if (this.W != null) {
      localJSONObject.put("data", this.W);
    }
    return localJSONObject;
  }
  
  public void a(double paramDouble)
  {
    this.V = paramDouble;
  }
  
  protected void a(JSONObject paramJSONObject)
  {
    this.W = paramJSONObject;
  }
  
  public String toString()
  {
    return "Event #" + this.T + " type: " + (String)this.S.get(Integer.valueOf(this.U));
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */