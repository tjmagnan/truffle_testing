package org.joda.time;

import java.io.Serializable;
import java.util.Comparator;
import org.joda.time.convert.ConverterManager;
import org.joda.time.convert.InstantConverter;

public class DateTimeComparator
  implements Comparator<Object>, Serializable
{
  private static final DateTimeComparator ALL_INSTANCE = new DateTimeComparator(null, null);
  private static final DateTimeComparator DATE_INSTANCE = new DateTimeComparator(DateTimeFieldType.dayOfYear(), null);
  private static final DateTimeComparator TIME_INSTANCE = new DateTimeComparator(null, DateTimeFieldType.dayOfYear());
  private static final long serialVersionUID = -6097339773320178364L;
  private final DateTimeFieldType iLowerLimit;
  private final DateTimeFieldType iUpperLimit;
  
  protected DateTimeComparator(DateTimeFieldType paramDateTimeFieldType1, DateTimeFieldType paramDateTimeFieldType2)
  {
    this.iLowerLimit = paramDateTimeFieldType1;
    this.iUpperLimit = paramDateTimeFieldType2;
  }
  
  public static DateTimeComparator getDateOnlyInstance()
  {
    return DATE_INSTANCE;
  }
  
  public static DateTimeComparator getInstance()
  {
    return ALL_INSTANCE;
  }
  
  public static DateTimeComparator getInstance(DateTimeFieldType paramDateTimeFieldType)
  {
    return getInstance(paramDateTimeFieldType, null);
  }
  
  public static DateTimeComparator getInstance(DateTimeFieldType paramDateTimeFieldType1, DateTimeFieldType paramDateTimeFieldType2)
  {
    if ((paramDateTimeFieldType1 == null) && (paramDateTimeFieldType2 == null)) {
      paramDateTimeFieldType1 = ALL_INSTANCE;
    }
    for (;;)
    {
      return paramDateTimeFieldType1;
      if ((paramDateTimeFieldType1 == DateTimeFieldType.dayOfYear()) && (paramDateTimeFieldType2 == null)) {
        paramDateTimeFieldType1 = DATE_INSTANCE;
      } else if ((paramDateTimeFieldType1 == null) && (paramDateTimeFieldType2 == DateTimeFieldType.dayOfYear())) {
        paramDateTimeFieldType1 = TIME_INSTANCE;
      } else {
        paramDateTimeFieldType1 = new DateTimeComparator(paramDateTimeFieldType1, paramDateTimeFieldType2);
      }
    }
  }
  
  public static DateTimeComparator getTimeOnlyInstance()
  {
    return TIME_INSTANCE;
  }
  
  private Object readResolve()
  {
    return getInstance(this.iLowerLimit, this.iUpperLimit);
  }
  
  public int compare(Object paramObject1, Object paramObject2)
  {
    Object localObject = ConverterManager.getInstance().getInstantConverter(paramObject1);
    Chronology localChronology = ((InstantConverter)localObject).getChronology(paramObject1, (Chronology)null);
    long l4 = ((InstantConverter)localObject).getInstantMillis(paramObject1, localChronology);
    int i;
    if (paramObject1 == paramObject2) {
      i = 0;
    }
    for (;;)
    {
      return i;
      paramObject1 = ConverterManager.getInstance().getInstantConverter(paramObject2);
      localObject = ((InstantConverter)paramObject1).getChronology(paramObject2, (Chronology)null);
      long l3 = ((InstantConverter)paramObject1).getInstantMillis(paramObject2, (Chronology)localObject);
      long l1 = l3;
      long l2 = l4;
      if (this.iLowerLimit != null)
      {
        l2 = this.iLowerLimit.getField(localChronology).roundFloor(l4);
        l1 = this.iLowerLimit.getField((Chronology)localObject).roundFloor(l3);
      }
      l4 = l1;
      l3 = l2;
      if (this.iUpperLimit != null)
      {
        l3 = this.iUpperLimit.getField(localChronology).remainder(l2);
        l4 = this.iUpperLimit.getField((Chronology)localObject).remainder(l1);
      }
      if (l3 < l4) {
        i = -1;
      } else if (l3 > l4) {
        i = 1;
      } else {
        i = 0;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if ((paramObject instanceof DateTimeComparator))
    {
      paramObject = (DateTimeComparator)paramObject;
      if (this.iLowerLimit != ((DateTimeComparator)paramObject).getLowerLimit())
      {
        bool1 = bool2;
        if (this.iLowerLimit != null)
        {
          bool1 = bool2;
          if (!this.iLowerLimit.equals(((DateTimeComparator)paramObject).getLowerLimit())) {}
        }
      }
      else if (this.iUpperLimit != ((DateTimeComparator)paramObject).getUpperLimit())
      {
        bool1 = bool2;
        if (this.iUpperLimit != null)
        {
          bool1 = bool2;
          if (!this.iUpperLimit.equals(((DateTimeComparator)paramObject).getUpperLimit())) {}
        }
      }
      else
      {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public DateTimeFieldType getLowerLimit()
  {
    return this.iLowerLimit;
  }
  
  public DateTimeFieldType getUpperLimit()
  {
    return this.iUpperLimit;
  }
  
  public int hashCode()
  {
    int j = 0;
    int i;
    if (this.iLowerLimit == null)
    {
      i = 0;
      if (this.iUpperLimit != null) {
        break label36;
      }
    }
    for (;;)
    {
      return i + j * 123;
      i = this.iLowerLimit.hashCode();
      break;
      label36:
      j = this.iUpperLimit.hashCode();
    }
  }
  
  public String toString()
  {
    if (this.iLowerLimit == this.iUpperLimit)
    {
      localStringBuilder = new StringBuilder().append("DateTimeComparator[");
      if (this.iLowerLimit == null) {}
      for (str = "";; str = this.iLowerLimit.getName())
      {
        str = str + "]";
        return str;
      }
    }
    StringBuilder localStringBuilder = new StringBuilder().append("DateTimeComparator[");
    if (this.iLowerLimit == null)
    {
      str = "";
      label84:
      localStringBuilder = localStringBuilder.append(str).append("-");
      if (this.iUpperLimit != null) {
        break label133;
      }
    }
    label133:
    for (String str = "";; str = this.iUpperLimit.getName())
    {
      str = str + "]";
      break;
      str = this.iLowerLimit.getName();
      break label84;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\DateTimeComparator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */