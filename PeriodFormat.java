package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

public class PeriodFormat
{
  private static final String BUNDLE_NAME = "org.joda.time.format.messages";
  private static final ConcurrentMap<Locale, PeriodFormatter> FORMATTERS = new ConcurrentHashMap();
  
  private static PeriodFormatter buildNonRegExFormatter(ResourceBundle paramResourceBundle, Locale paramLocale)
  {
    String[] arrayOfString = retrieveVariants(paramResourceBundle);
    return new PeriodFormatterBuilder().appendYears().appendSuffix(paramResourceBundle.getString("PeriodFormat.year"), paramResourceBundle.getString("PeriodFormat.years")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendMonths().appendSuffix(paramResourceBundle.getString("PeriodFormat.month"), paramResourceBundle.getString("PeriodFormat.months")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendWeeks().appendSuffix(paramResourceBundle.getString("PeriodFormat.week"), paramResourceBundle.getString("PeriodFormat.weeks")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendDays().appendSuffix(paramResourceBundle.getString("PeriodFormat.day"), paramResourceBundle.getString("PeriodFormat.days")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendHours().appendSuffix(paramResourceBundle.getString("PeriodFormat.hour"), paramResourceBundle.getString("PeriodFormat.hours")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendMinutes().appendSuffix(paramResourceBundle.getString("PeriodFormat.minute"), paramResourceBundle.getString("PeriodFormat.minutes")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendSeconds().appendSuffix(paramResourceBundle.getString("PeriodFormat.second"), paramResourceBundle.getString("PeriodFormat.seconds")).appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString).appendMillis().appendSuffix(paramResourceBundle.getString("PeriodFormat.millisecond"), paramResourceBundle.getString("PeriodFormat.milliseconds")).toFormatter().withLocale(paramLocale);
  }
  
  private static PeriodFormatter buildRegExFormatter(ResourceBundle paramResourceBundle, Locale paramLocale)
  {
    String[] arrayOfString = retrieveVariants(paramResourceBundle);
    String str = paramResourceBundle.getString("PeriodFormat.regex.separator");
    PeriodFormatterBuilder localPeriodFormatterBuilder = new PeriodFormatterBuilder();
    localPeriodFormatterBuilder.appendYears();
    if (containsKey(paramResourceBundle, "PeriodFormat.years.regex"))
    {
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.years.regex").split(str), paramResourceBundle.getString("PeriodFormat.years.list").split(str));
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendMonths();
      if (!containsKey(paramResourceBundle, "PeriodFormat.months.regex")) {
        break label495;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.months.regex").split(str), paramResourceBundle.getString("PeriodFormat.months.list").split(str));
      label118:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendWeeks();
      if (!containsKey(paramResourceBundle, "PeriodFormat.weeks.regex")) {
        break label515;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.weeks.regex").split(str), paramResourceBundle.getString("PeriodFormat.weeks.list").split(str));
      label176:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendDays();
      if (!containsKey(paramResourceBundle, "PeriodFormat.days.regex")) {
        break label535;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.days.regex").split(str), paramResourceBundle.getString("PeriodFormat.days.list").split(str));
      label234:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendHours();
      if (!containsKey(paramResourceBundle, "PeriodFormat.hours.regex")) {
        break label555;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.hours.regex").split(str), paramResourceBundle.getString("PeriodFormat.hours.list").split(str));
      label292:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendMinutes();
      if (!containsKey(paramResourceBundle, "PeriodFormat.minutes.regex")) {
        break label575;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.minutes.regex").split(str), paramResourceBundle.getString("PeriodFormat.minutes.list").split(str));
      label350:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendSeconds();
      if (!containsKey(paramResourceBundle, "PeriodFormat.seconds.regex")) {
        break label595;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.seconds.regex").split(str), paramResourceBundle.getString("PeriodFormat.seconds.list").split(str));
      label408:
      localPeriodFormatterBuilder.appendSeparator(paramResourceBundle.getString("PeriodFormat.commaspace"), paramResourceBundle.getString("PeriodFormat.spaceandspace"), arrayOfString);
      localPeriodFormatterBuilder.appendMillis();
      if (!containsKey(paramResourceBundle, "PeriodFormat.milliseconds.regex")) {
        break label615;
      }
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.milliseconds.regex").split(str), paramResourceBundle.getString("PeriodFormat.milliseconds.list").split(str));
    }
    for (;;)
    {
      return localPeriodFormatterBuilder.toFormatter().withLocale(paramLocale);
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.year"), paramResourceBundle.getString("PeriodFormat.years"));
      break;
      label495:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.month"), paramResourceBundle.getString("PeriodFormat.months"));
      break label118;
      label515:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.week"), paramResourceBundle.getString("PeriodFormat.weeks"));
      break label176;
      label535:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.day"), paramResourceBundle.getString("PeriodFormat.days"));
      break label234;
      label555:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.hour"), paramResourceBundle.getString("PeriodFormat.hours"));
      break label292;
      label575:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.minute"), paramResourceBundle.getString("PeriodFormat.minutes"));
      break label350;
      label595:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.second"), paramResourceBundle.getString("PeriodFormat.seconds"));
      break label408;
      label615:
      localPeriodFormatterBuilder.appendSuffix(paramResourceBundle.getString("PeriodFormat.millisecond"), paramResourceBundle.getString("PeriodFormat.milliseconds"));
    }
  }
  
  private static PeriodFormatter buildWordBased(Locale paramLocale)
  {
    ResourceBundle localResourceBundle = ResourceBundle.getBundle("org.joda.time.format.messages", paramLocale);
    if (containsKey(localResourceBundle, "PeriodFormat.regex.separator")) {}
    for (paramLocale = buildRegExFormatter(localResourceBundle, paramLocale);; paramLocale = buildNonRegExFormatter(localResourceBundle, paramLocale)) {
      return paramLocale;
    }
  }
  
  private static boolean containsKey(ResourceBundle paramResourceBundle, String paramString)
  {
    paramResourceBundle = paramResourceBundle.getKeys();
    do
    {
      if (!paramResourceBundle.hasMoreElements()) {
        break;
      }
    } while (!((String)paramResourceBundle.nextElement()).equals(paramString));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static PeriodFormatter getDefault()
  {
    return wordBased(Locale.ENGLISH);
  }
  
  private static String[] retrieveVariants(ResourceBundle paramResourceBundle)
  {
    return new String[] { paramResourceBundle.getString("PeriodFormat.space"), paramResourceBundle.getString("PeriodFormat.comma"), paramResourceBundle.getString("PeriodFormat.commandand"), paramResourceBundle.getString("PeriodFormat.commaspaceand") };
  }
  
  public static PeriodFormatter wordBased()
  {
    return wordBased(Locale.getDefault());
  }
  
  public static PeriodFormatter wordBased(Locale paramLocale)
  {
    PeriodFormatter localPeriodFormatter = (PeriodFormatter)FORMATTERS.get(paramLocale);
    Object localObject = localPeriodFormatter;
    if (localPeriodFormatter == null)
    {
      localObject = new DynamicWordBased(buildWordBased(paramLocale));
      localObject = new PeriodFormatter((PeriodPrinter)localObject, (PeriodParser)localObject, paramLocale, null);
      paramLocale = (PeriodFormatter)FORMATTERS.putIfAbsent(paramLocale, localObject);
      if (paramLocale == null) {
        break label65;
      }
      localObject = paramLocale;
    }
    label65:
    for (;;)
    {
      return (PeriodFormatter)localObject;
    }
  }
  
  static class DynamicWordBased
    implements PeriodPrinter, PeriodParser
  {
    private final PeriodFormatter iFormatter;
    
    DynamicWordBased(PeriodFormatter paramPeriodFormatter)
    {
      this.iFormatter = paramPeriodFormatter;
    }
    
    private PeriodParser getParser(Locale paramLocale)
    {
      if ((paramLocale != null) && (!paramLocale.equals(this.iFormatter.getLocale()))) {}
      for (paramLocale = PeriodFormat.wordBased(paramLocale).getParser();; paramLocale = this.iFormatter.getParser()) {
        return paramLocale;
      }
    }
    
    private PeriodPrinter getPrinter(Locale paramLocale)
    {
      if ((paramLocale != null) && (!paramLocale.equals(this.iFormatter.getLocale()))) {}
      for (paramLocale = PeriodFormat.wordBased(paramLocale).getPrinter();; paramLocale = this.iFormatter.getPrinter()) {
        return paramLocale;
      }
    }
    
    public int calculatePrintedLength(ReadablePeriod paramReadablePeriod, Locale paramLocale)
    {
      return getPrinter(paramLocale).calculatePrintedLength(paramReadablePeriod, paramLocale);
    }
    
    public int countFieldsToPrint(ReadablePeriod paramReadablePeriod, int paramInt, Locale paramLocale)
    {
      return getPrinter(paramLocale).countFieldsToPrint(paramReadablePeriod, paramInt, paramLocale);
    }
    
    public int parseInto(ReadWritablePeriod paramReadWritablePeriod, String paramString, int paramInt, Locale paramLocale)
    {
      return getParser(paramLocale).parseInto(paramReadWritablePeriod, paramString, paramInt, paramLocale);
    }
    
    public void printTo(Writer paramWriter, ReadablePeriod paramReadablePeriod, Locale paramLocale)
      throws IOException
    {
      getPrinter(paramLocale).printTo(paramWriter, paramReadablePeriod, paramLocale);
    }
    
    public void printTo(StringBuffer paramStringBuffer, ReadablePeriod paramReadablePeriod, Locale paramLocale)
    {
      getPrinter(paramLocale).printTo(paramStringBuffer, paramReadablePeriod, paramLocale);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\PeriodFormat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */