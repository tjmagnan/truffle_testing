package com.crashlytics.android.core.internal.models;

public class SignalData
{
  public final String code;
  public final long faultAddress;
  public final String name;
  
  public SignalData(String paramString1, String paramString2, long paramLong)
  {
    this.name = paramString1;
    this.code = paramString2;
    this.faultAddress = paramLong;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\internal\models\SignalData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */