package org.joda.time;

import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Days
  extends BaseSingleFieldPeriod
{
  public static final Days FIVE;
  public static final Days FOUR;
  public static final Days MAX_VALUE = new Days(Integer.MAX_VALUE);
  public static final Days MIN_VALUE = new Days(Integer.MIN_VALUE);
  public static final Days ONE;
  private static final PeriodFormatter PARSER = ISOPeriodFormat.standard().withParseType(PeriodType.days());
  public static final Days SEVEN;
  public static final Days SIX;
  public static final Days THREE;
  public static final Days TWO;
  public static final Days ZERO = new Days(0);
  private static final long serialVersionUID = 87525275727380865L;
  
  static
  {
    ONE = new Days(1);
    TWO = new Days(2);
    THREE = new Days(3);
    FOUR = new Days(4);
    FIVE = new Days(5);
    SIX = new Days(6);
    SEVEN = new Days(7);
  }
  
  private Days(int paramInt)
  {
    super(paramInt);
  }
  
  public static Days days(int paramInt)
  {
    Days localDays;
    switch (paramInt)
    {
    default: 
      localDays = new Days(paramInt);
    }
    for (;;)
    {
      return localDays;
      localDays = ZERO;
      continue;
      localDays = ONE;
      continue;
      localDays = TWO;
      continue;
      localDays = THREE;
      continue;
      localDays = FOUR;
      continue;
      localDays = FIVE;
      continue;
      localDays = SIX;
      continue;
      localDays = SEVEN;
      continue;
      localDays = MAX_VALUE;
      continue;
      localDays = MIN_VALUE;
    }
  }
  
  public static Days daysBetween(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    return days(BaseSingleFieldPeriod.between(paramReadableInstant1, paramReadableInstant2, DurationFieldType.days()));
  }
  
  public static Days daysBetween(ReadablePartial paramReadablePartial1, ReadablePartial paramReadablePartial2)
  {
    if (((paramReadablePartial1 instanceof LocalDate)) && ((paramReadablePartial2 instanceof LocalDate))) {}
    for (paramReadablePartial1 = days(DateTimeUtils.getChronology(paramReadablePartial1.getChronology()).days().getDifference(((LocalDate)paramReadablePartial2).getLocalMillis(), ((LocalDate)paramReadablePartial1).getLocalMillis()));; paramReadablePartial1 = days(BaseSingleFieldPeriod.between(paramReadablePartial1, paramReadablePartial2, ZERO))) {
      return paramReadablePartial1;
    }
  }
  
  public static Days daysIn(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (paramReadableInterval = ZERO;; paramReadableInterval = days(BaseSingleFieldPeriod.between(paramReadableInterval.getStart(), paramReadableInterval.getEnd(), DurationFieldType.days()))) {
      return paramReadableInterval;
    }
  }
  
  @FromString
  public static Days parseDays(String paramString)
  {
    if (paramString == null) {}
    for (paramString = ZERO;; paramString = days(PARSER.parsePeriod(paramString).getDays())) {
      return paramString;
    }
  }
  
  private Object readResolve()
  {
    return days(getValue());
  }
  
  public static Days standardDaysIn(ReadablePeriod paramReadablePeriod)
  {
    return days(BaseSingleFieldPeriod.standardPeriodIn(paramReadablePeriod, 86400000L));
  }
  
  public Days dividedBy(int paramInt)
  {
    if (paramInt == 1) {}
    for (Days localDays = this;; localDays = days(getValue() / paramInt)) {
      return localDays;
    }
  }
  
  public int getDays()
  {
    return getValue();
  }
  
  public DurationFieldType getFieldType()
  {
    return DurationFieldType.days();
  }
  
  public PeriodType getPeriodType()
  {
    return PeriodType.days();
  }
  
  public boolean isGreaterThan(Days paramDays)
  {
    boolean bool = true;
    if (paramDays == null) {
      if (getValue() <= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() <= paramDays.getValue()) {
        bool = false;
      }
    }
  }
  
  public boolean isLessThan(Days paramDays)
  {
    boolean bool = true;
    if (paramDays == null) {
      if (getValue() >= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() >= paramDays.getValue()) {
        bool = false;
      }
    }
  }
  
  public Days minus(int paramInt)
  {
    return plus(FieldUtils.safeNegate(paramInt));
  }
  
  public Days minus(Days paramDays)
  {
    if (paramDays == null) {}
    for (paramDays = this;; paramDays = minus(paramDays.getValue())) {
      return paramDays;
    }
  }
  
  public Days multipliedBy(int paramInt)
  {
    return days(FieldUtils.safeMultiply(getValue(), paramInt));
  }
  
  public Days negated()
  {
    return days(FieldUtils.safeNegate(getValue()));
  }
  
  public Days plus(int paramInt)
  {
    if (paramInt == 0) {}
    for (Days localDays = this;; localDays = days(FieldUtils.safeAdd(getValue(), paramInt))) {
      return localDays;
    }
  }
  
  public Days plus(Days paramDays)
  {
    if (paramDays == null) {}
    for (paramDays = this;; paramDays = plus(paramDays.getValue())) {
      return paramDays;
    }
  }
  
  public Duration toStandardDuration()
  {
    return new Duration(getValue() * 86400000L);
  }
  
  public Hours toStandardHours()
  {
    return Hours.hours(FieldUtils.safeMultiply(getValue(), 24));
  }
  
  public Minutes toStandardMinutes()
  {
    return Minutes.minutes(FieldUtils.safeMultiply(getValue(), 1440));
  }
  
  public Seconds toStandardSeconds()
  {
    return Seconds.seconds(FieldUtils.safeMultiply(getValue(), 86400));
  }
  
  public Weeks toStandardWeeks()
  {
    return Weeks.weeks(getValue() / 7);
  }
  
  @ToString
  public String toString()
  {
    return "P" + String.valueOf(getValue()) + "D";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Days.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */