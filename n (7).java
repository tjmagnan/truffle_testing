package com.testfairy.n;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class g
{
  private final Set a = new HashSet();
  
  public g()
  {
    Object localObject = new HashMap(32);
    ((Map)localObject).put("com.flurry.android.FlurryAgent", "flurry");
    ((Map)localObject).put("com.flurry.android.FlurryAds", "flurry-ads");
    ((Map)localObject).put("com.facebook.LoginActivity", "facebook");
    ((Map)localObject).put("com.facebook.android.FbDialog", "facebook");
    ((Map)localObject).put("com.crittercism.app.Crittercism", "crittercism");
    ((Map)localObject).put("net.hockeyapp.android.UpdateActivity", "hockeyapp");
    ((Map)localObject).put("roboguice.activity.RoboActivity", "roboguice");
    ((Map)localObject).put("com.unity3d.player.UnityPlayerProxyActivity", "unity");
    ((Map)localObject).put("com.adobe.air.AndroidActivityWrapper", "adobe-air");
    ((Map)localObject).put("com.ansca.corona.CoronaActivity", "corona");
    ((Map)localObject).put("org.cocos2dx.lib.Cocos2dxActivity", "cocos2dx");
    ((Map)localObject).put("com.ironsource.mobilcore.MobileCore", "mobilecore");
    ((Map)localObject).put("com.applovin.sdk.AppLovinSdk", "applovin");
    ((Map)localObject).put("org.fmod.FMODAudioDevice", "fmod");
    ((Map)localObject).put("com.appbrain.AppBrain", "appbrain");
    ((Map)localObject).put("com.appbrain.AppBrainActivity", "appbrain");
    ((Map)localObject).put("org.andengine.ui.activity.BaseActivity", "andengine");
    ((Map)localObject).put("com.mongodb.Mongo", "mongodb");
    ((Map)localObject).put("com.tapjoy.TapjoyConnect", "tapjoy");
    ((Map)localObject).put("com.urbanairship.UAirship", "urbanairship");
    ((Map)localObject).put("com.zong.android.engine.ui.ZongUI", "zong");
    ((Map)localObject).put("com.admob.android.ads.AdView", "admob");
    ((Map)localObject).put("com.chartboost.sdk.Chartboost", "chartboost");
    ((Map)localObject).put("android.support.v4.app.Fragment", "supportv4");
    ((Map)localObject).put("android.support.v4.view.PagerAdapter", "supportv4");
    ((Map)localObject).put("com.uservoice.uservoicesdk.UserVoice", "uservoice");
    ((Map)localObject).put("com.crashlytics.android.Crashlytics", "crashlytics");
    ((Map)localObject).put("com.mixpanel.android.mpmetrics.MixpanelAPI", "mixpanel");
    ((Map)localObject).put("com.gryphonet.appright.AppRight", "appright");
    ((Map)localObject).put("com.droidwatcher.nativehandler.NativeDebuggerService", "droidwatcher");
    ((Map)localObject).put("org.acra.ACRA", "acra");
    ((Map)localObject).put("com.actionbarsherlock.app.SherlockListActivity", "actionbar-sherlock");
    ((Map)localObject).put("com.mobileapptracker.MobileAppTracker", "mobile-app-tracker");
    ((Map)localObject).put("org.videolan.libvlc.LibVLC", "libvlc");
    ((Map)localObject).put("org.msgpack.MessagePack", "msgpack");
    ((Map)localObject).put("com.testflightapp.lib.TestFlight", "testflight");
    ((Map)localObject).put("butterknife.ButterKnife", "butterknife");
    ((Map)localObject).put("retrofit.RequestBuilder", "retrofit");
    ((Map)localObject).put("uk.co.senab.photoview.PhotoView", "photoview");
    ((Map)localObject).put("com.newrelic.agent.android.NewRelic", "new-relic");
    ((Map)localObject).put("com.google.android.apps.analytics.GoogleAnalyticsTracker", "google-analytics");
    ((Map)localObject).put("com.google.android.apps.analytics.AnalyticsReceiver", "google-analytics");
    ((Map)localObject).put("Lcom/google/android/gms/maps/GoogleMap;", "google-maps");
    ((Map)localObject).put("com.appsee.Appsee", "appsee");
    ((Map)localObject).put("com.crashlytics.android.Crashlytics", "crashlytics");
    ((Map)localObject).put("io.fabric.sdk.android.Fabric", "fabric");
    ((Map)localObject).put("io.intercom.android.sdk.Intercom", "intercom");
    ((Map)localObject).put("com.adobe.fre.FREExtension", "adobe-air");
    ((Map)localObject).put("org.appcelerator.kroll.KrollModule", "titanium");
    ((Map)localObject).put("com.localytics.android.Localytics", "localytics");
    ((Map)localObject).put("com.google.firebase.FirebaseApp", "firebase");
    ((Map)localObject).put("org.apache.cordova.CordovaPlugin", "cordova");
    ((Map)localObject).put("com.testfairy.TestFairy", "testfairy-sdk");
    localObject = ((Map)localObject).entrySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      Map.Entry localEntry = (Map.Entry)((Iterator)localObject).next();
      try
      {
        Class.forName((String)localEntry.getKey());
        this.a.add(localEntry.getValue());
      }
      catch (Throwable localThrowable) {}
    }
  }
  
  public Set a()
  {
    return this.a;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */