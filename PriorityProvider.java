package io.fabric.sdk.android.services.concurrency;

public abstract interface PriorityProvider<T>
  extends Comparable<T>
{
  public abstract Priority getPriority();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\concurrency\PriorityProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */