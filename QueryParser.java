package org.jsoup.select;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.parser.TokenQueue;

public class QueryParser
{
  private static final String[] AttributeEvals = { "=", "!=", "^=", "$=", "*=", "~=" };
  private static final Pattern NTH_AB = Pattern.compile("((\\+|-)?(\\d+)?)n(\\s*(\\+|-)?\\s*\\d+)?", 2);
  private static final Pattern NTH_B = Pattern.compile("(\\+|-)?(\\d+)");
  private static final String[] combinators = { ",", ">", "+", "~", " " };
  private List<Evaluator> evals = new ArrayList();
  private String query;
  private TokenQueue tq;
  
  private QueryParser(String paramString)
  {
    this.query = paramString;
    this.tq = new TokenQueue(paramString);
  }
  
  private void allElements()
  {
    this.evals.add(new Evaluator.AllElements());
  }
  
  private void byAttribute()
  {
    TokenQueue localTokenQueue = new TokenQueue(this.tq.chompBalanced('[', ']'));
    String str = localTokenQueue.consumeToAny(AttributeEvals);
    Validate.notEmpty(str);
    localTokenQueue.consumeWhitespace();
    if (localTokenQueue.isEmpty()) {
      if (str.startsWith("^")) {
        this.evals.add(new Evaluator.AttributeStarting(str.substring(1)));
      }
    }
    for (;;)
    {
      return;
      this.evals.add(new Evaluator.Attribute(str));
      continue;
      if (localTokenQueue.matchChomp("="))
      {
        this.evals.add(new Evaluator.AttributeWithValue(str, localTokenQueue.remainder()));
      }
      else if (localTokenQueue.matchChomp("!="))
      {
        this.evals.add(new Evaluator.AttributeWithValueNot(str, localTokenQueue.remainder()));
      }
      else if (localTokenQueue.matchChomp("^="))
      {
        this.evals.add(new Evaluator.AttributeWithValueStarting(str, localTokenQueue.remainder()));
      }
      else if (localTokenQueue.matchChomp("$="))
      {
        this.evals.add(new Evaluator.AttributeWithValueEnding(str, localTokenQueue.remainder()));
      }
      else if (localTokenQueue.matchChomp("*="))
      {
        this.evals.add(new Evaluator.AttributeWithValueContaining(str, localTokenQueue.remainder()));
      }
      else
      {
        if (!localTokenQueue.matchChomp("~=")) {
          break;
        }
        this.evals.add(new Evaluator.AttributeWithValueMatching(str, Pattern.compile(localTokenQueue.remainder())));
      }
    }
    throw new Selector.SelectorParseException("Could not parse attribute query '%s': unexpected token at '%s'", new Object[] { this.query, localTokenQueue.remainder() });
  }
  
  private void byClass()
  {
    String str = this.tq.consumeCssIdentifier();
    Validate.notEmpty(str);
    this.evals.add(new Evaluator.Class(str.trim()));
  }
  
  private void byId()
  {
    String str = this.tq.consumeCssIdentifier();
    Validate.notEmpty(str);
    this.evals.add(new Evaluator.Id(str));
  }
  
  private void byTag()
  {
    String str2 = this.tq.consumeElementSelector();
    Validate.notEmpty(str2);
    if (str2.startsWith("*|")) {
      this.evals.add(new CombiningEvaluator.Or(new Evaluator[] { new Evaluator.Tag(str2.trim().toLowerCase()), new Evaluator.TagEndsWith(str2.replace("*|", ":").trim().toLowerCase()) }));
    }
    for (;;)
    {
      return;
      String str1 = str2;
      if (str2.contains("|")) {
        str1 = str2.replace("|", ":");
      }
      this.evals.add(new Evaluator.Tag(str1.trim()));
    }
  }
  
  private void combinator(char paramChar)
  {
    this.tq.consumeWhitespace();
    Evaluator localEvaluator2 = parse(consumeSubQuery());
    int j = 0;
    Object localObject3;
    Object localObject1;
    int i;
    Object localObject2;
    if (this.evals.size() == 1)
    {
      Evaluator localEvaluator1 = (Evaluator)this.evals.get(0);
      localObject3 = localEvaluator1;
      localObject1 = localEvaluator1;
      i = j;
      localObject2 = localObject3;
      if ((localObject3 instanceof CombiningEvaluator.Or))
      {
        localObject1 = localEvaluator1;
        i = j;
        localObject2 = localObject3;
        if (paramChar != ',')
        {
          localObject1 = ((CombiningEvaluator.Or)localEvaluator1).rightMostEvaluator();
          i = 1;
          localObject2 = localObject3;
        }
      }
      this.evals.clear();
      if (paramChar != '>') {
        break label195;
      }
      localObject1 = new CombiningEvaluator.And(new Evaluator[] { localEvaluator2, new StructuralEvaluator.ImmediateParent((Evaluator)localObject1) });
      label146:
      if (i == 0) {
        break label405;
      }
      ((CombiningEvaluator.Or)localObject2).replaceRightMostEvaluator((Evaluator)localObject1);
    }
    for (;;)
    {
      this.evals.add(localObject2);
      return;
      localObject1 = new CombiningEvaluator.And(this.evals);
      localObject2 = localObject1;
      i = j;
      break;
      label195:
      if (paramChar == ' ')
      {
        localObject1 = new CombiningEvaluator.And(new Evaluator[] { localEvaluator2, new StructuralEvaluator.Parent((Evaluator)localObject1) });
        break label146;
      }
      if (paramChar == '+')
      {
        localObject1 = new CombiningEvaluator.And(new Evaluator[] { localEvaluator2, new StructuralEvaluator.ImmediatePreviousSibling((Evaluator)localObject1) });
        break label146;
      }
      if (paramChar == '~')
      {
        localObject1 = new CombiningEvaluator.And(new Evaluator[] { localEvaluator2, new StructuralEvaluator.PreviousSibling((Evaluator)localObject1) });
        break label146;
      }
      if (paramChar == ',')
      {
        if ((localObject1 instanceof CombiningEvaluator.Or))
        {
          localObject1 = (CombiningEvaluator.Or)localObject1;
          ((CombiningEvaluator.Or)localObject1).add(localEvaluator2);
        }
        for (;;)
        {
          break;
          localObject3 = new CombiningEvaluator.Or();
          ((CombiningEvaluator.Or)localObject3).add((Evaluator)localObject1);
          ((CombiningEvaluator.Or)localObject3).add(localEvaluator2);
          localObject1 = localObject3;
        }
      }
      throw new Selector.SelectorParseException("Unknown combinator: " + paramChar, new Object[0]);
      label405:
      localObject2 = localObject1;
    }
  }
  
  private int consumeIndex()
  {
    String str = this.tq.chompTo(")").trim();
    Validate.isTrue(StringUtil.isNumeric(str), "Index must be numeric");
    return Integer.parseInt(str);
  }
  
  private String consumeSubQuery()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (;;)
    {
      if (!this.tq.isEmpty())
      {
        if (this.tq.matches("("))
        {
          localStringBuilder.append("(").append(this.tq.chompBalanced('(', ')')).append(")");
          continue;
        }
        if (this.tq.matches("["))
        {
          localStringBuilder.append("[").append(this.tq.chompBalanced('[', ']')).append("]");
          continue;
        }
        if (!this.tq.matchesAny(combinators)) {}
      }
      else
      {
        return localStringBuilder.toString();
      }
      localStringBuilder.append(this.tq.consume());
    }
  }
  
  private void contains(boolean paramBoolean)
  {
    TokenQueue localTokenQueue = this.tq;
    String str;
    if (paramBoolean)
    {
      str = ":containsOwn";
      localTokenQueue.consume(str);
      str = TokenQueue.unescape(this.tq.chompBalanced('(', ')'));
      Validate.notEmpty(str, ":contains(text) query must not be empty");
      if (!paramBoolean) {
        break label70;
      }
      this.evals.add(new Evaluator.ContainsOwnText(str));
    }
    for (;;)
    {
      return;
      str = ":contains";
      break;
      label70:
      this.evals.add(new Evaluator.ContainsText(str));
    }
  }
  
  private void containsData()
  {
    this.tq.consume(":containsData");
    String str = TokenQueue.unescape(this.tq.chompBalanced('(', ')'));
    Validate.notEmpty(str, ":containsData(text) query must not be empty");
    this.evals.add(new Evaluator.ContainsData(str));
  }
  
  private void cssNthChild(boolean paramBoolean1, boolean paramBoolean2)
  {
    int j = 0;
    int i = 1;
    String str = this.tq.chompTo(")").trim().toLowerCase();
    Matcher localMatcher1 = NTH_AB.matcher(str);
    Matcher localMatcher2 = NTH_B.matcher(str);
    if ("odd".equals(str))
    {
      i = 2;
      j = 1;
      if (!paramBoolean2) {
        break label250;
      }
      if (!paramBoolean1) {
        break label227;
      }
      this.evals.add(new Evaluator.IsNthLastOfType(i, j));
    }
    for (;;)
    {
      return;
      if ("even".equals(str))
      {
        i = 2;
        j = 0;
        break;
      }
      if (localMatcher1.matches())
      {
        if (localMatcher1.group(3) != null) {
          i = Integer.parseInt(localMatcher1.group(1).replaceFirst("^\\+", ""));
        }
        if (localMatcher1.group(4) != null) {
          j = Integer.parseInt(localMatcher1.group(4).replaceFirst("^\\+", ""));
        }
        break;
      }
      if (localMatcher2.matches())
      {
        i = 0;
        j = Integer.parseInt(localMatcher2.group().replaceFirst("^\\+", ""));
        break;
      }
      throw new Selector.SelectorParseException("Could not parse nth-index '%s': unexpected format", new Object[] { str });
      label227:
      this.evals.add(new Evaluator.IsNthOfType(i, j));
      continue;
      label250:
      if (paramBoolean1) {
        this.evals.add(new Evaluator.IsNthLastChild(i, j));
      } else {
        this.evals.add(new Evaluator.IsNthChild(i, j));
      }
    }
  }
  
  private void findElements()
  {
    if (this.tq.matchChomp("#")) {
      byId();
    }
    for (;;)
    {
      return;
      if (this.tq.matchChomp("."))
      {
        byClass();
      }
      else if ((this.tq.matchesWord()) || (this.tq.matches("*|")))
      {
        byTag();
      }
      else if (this.tq.matches("["))
      {
        byAttribute();
      }
      else if (this.tq.matchChomp("*"))
      {
        allElements();
      }
      else if (this.tq.matchChomp(":lt("))
      {
        indexLessThan();
      }
      else if (this.tq.matchChomp(":gt("))
      {
        indexGreaterThan();
      }
      else if (this.tq.matchChomp(":eq("))
      {
        indexEquals();
      }
      else if (this.tq.matches(":has("))
      {
        has();
      }
      else if (this.tq.matches(":contains("))
      {
        contains(false);
      }
      else if (this.tq.matches(":containsOwn("))
      {
        contains(true);
      }
      else if (this.tq.matches(":containsData("))
      {
        containsData();
      }
      else if (this.tq.matches(":matches("))
      {
        matches(false);
      }
      else if (this.tq.matches(":matchesOwn("))
      {
        matches(true);
      }
      else if (this.tq.matches(":not("))
      {
        not();
      }
      else if (this.tq.matchChomp(":nth-child("))
      {
        cssNthChild(false, false);
      }
      else if (this.tq.matchChomp(":nth-last-child("))
      {
        cssNthChild(true, false);
      }
      else if (this.tq.matchChomp(":nth-of-type("))
      {
        cssNthChild(false, true);
      }
      else if (this.tq.matchChomp(":nth-last-of-type("))
      {
        cssNthChild(true, true);
      }
      else if (this.tq.matchChomp(":first-child"))
      {
        this.evals.add(new Evaluator.IsFirstChild());
      }
      else if (this.tq.matchChomp(":last-child"))
      {
        this.evals.add(new Evaluator.IsLastChild());
      }
      else if (this.tq.matchChomp(":first-of-type"))
      {
        this.evals.add(new Evaluator.IsFirstOfType());
      }
      else if (this.tq.matchChomp(":last-of-type"))
      {
        this.evals.add(new Evaluator.IsLastOfType());
      }
      else if (this.tq.matchChomp(":only-child"))
      {
        this.evals.add(new Evaluator.IsOnlyChild());
      }
      else if (this.tq.matchChomp(":only-of-type"))
      {
        this.evals.add(new Evaluator.IsOnlyOfType());
      }
      else if (this.tq.matchChomp(":empty"))
      {
        this.evals.add(new Evaluator.IsEmpty());
      }
      else
      {
        if (!this.tq.matchChomp(":root")) {
          break;
        }
        this.evals.add(new Evaluator.IsRoot());
      }
    }
    throw new Selector.SelectorParseException("Could not parse query '%s': unexpected token at '%s'", new Object[] { this.query, this.tq.remainder() });
  }
  
  private void has()
  {
    this.tq.consume(":has");
    String str = this.tq.chompBalanced('(', ')');
    Validate.notEmpty(str, ":has(el) subselect must not be empty");
    this.evals.add(new StructuralEvaluator.Has(parse(str)));
  }
  
  private void indexEquals()
  {
    this.evals.add(new Evaluator.IndexEquals(consumeIndex()));
  }
  
  private void indexGreaterThan()
  {
    this.evals.add(new Evaluator.IndexGreaterThan(consumeIndex()));
  }
  
  private void indexLessThan()
  {
    this.evals.add(new Evaluator.IndexLessThan(consumeIndex()));
  }
  
  private void matches(boolean paramBoolean)
  {
    TokenQueue localTokenQueue = this.tq;
    String str;
    if (paramBoolean)
    {
      str = ":matchesOwn";
      localTokenQueue.consume(str);
      str = this.tq.chompBalanced('(', ')');
      Validate.notEmpty(str, ":matches(regex) query must not be empty");
      if (!paramBoolean) {
        break label70;
      }
      this.evals.add(new Evaluator.MatchesOwn(Pattern.compile(str)));
    }
    for (;;)
    {
      return;
      str = ":matches";
      break;
      label70:
      this.evals.add(new Evaluator.Matches(Pattern.compile(str)));
    }
  }
  
  private void not()
  {
    this.tq.consume(":not");
    String str = this.tq.chompBalanced('(', ')');
    Validate.notEmpty(str, ":not(selector) subselect must not be empty");
    this.evals.add(new StructuralEvaluator.Not(parse(str)));
  }
  
  public static Evaluator parse(String paramString)
  {
    return new QueryParser(paramString).parse();
  }
  
  Evaluator parse()
  {
    this.tq.consumeWhitespace();
    if (this.tq.matchesAny(combinators))
    {
      this.evals.add(new StructuralEvaluator.Root());
      combinator(this.tq.consume());
    }
    while (!this.tq.isEmpty())
    {
      boolean bool = this.tq.consumeWhitespace();
      if (this.tq.matchesAny(combinators))
      {
        combinator(this.tq.consume());
        continue;
        findElements();
      }
      else if (bool)
      {
        combinator(' ');
      }
      else
      {
        findElements();
      }
    }
    if (this.evals.size() == 1) {}
    for (Object localObject = (Evaluator)this.evals.get(0);; localObject = new CombiningEvaluator.And(this.evals)) {
      return (Evaluator)localObject;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\select\QueryParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */