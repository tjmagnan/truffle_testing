package org.jsoup.helper;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public class W3CDom
{
  protected DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  
  public String asString(org.w3c.dom.Document paramDocument)
  {
    try
    {
      DOMSource localDOMSource = new javax/xml/transform/dom/DOMSource;
      localDOMSource.<init>(paramDocument);
      paramDocument = new java/io/StringWriter;
      paramDocument.<init>();
      StreamResult localStreamResult = new javax/xml/transform/stream/StreamResult;
      localStreamResult.<init>(paramDocument);
      TransformerFactory.newInstance().newTransformer().transform(localDOMSource, localStreamResult);
      paramDocument = paramDocument.toString();
      return paramDocument;
    }
    catch (TransformerException paramDocument)
    {
      throw new IllegalStateException(paramDocument);
    }
  }
  
  public void convert(org.jsoup.nodes.Document paramDocument, org.w3c.dom.Document paramDocument1)
  {
    if (!StringUtil.isBlank(paramDocument.location())) {
      paramDocument1.setDocumentURI(paramDocument.location());
    }
    paramDocument = paramDocument.child(0);
    new NodeTraversor(new W3CBuilder(paramDocument1)).traverse(paramDocument);
  }
  
  public org.w3c.dom.Document fromJsoup(org.jsoup.nodes.Document paramDocument)
  {
    Validate.notNull(paramDocument);
    try
    {
      this.factory.setNamespaceAware(true);
      org.w3c.dom.Document localDocument = this.factory.newDocumentBuilder().newDocument();
      convert(paramDocument, localDocument);
      return localDocument;
    }
    catch (ParserConfigurationException paramDocument)
    {
      throw new IllegalStateException(paramDocument);
    }
  }
  
  protected static class W3CBuilder
    implements NodeVisitor
  {
    private static final String xmlnsKey = "xmlns";
    private static final String xmlnsPrefix = "xmlns:";
    private org.w3c.dom.Element dest;
    private final org.w3c.dom.Document doc;
    private final HashMap<String, String> namespaces = new HashMap();
    
    public W3CBuilder(org.w3c.dom.Document paramDocument)
    {
      this.doc = paramDocument;
    }
    
    private void copyAttributes(org.jsoup.nodes.Node paramNode, org.w3c.dom.Element paramElement)
    {
      Iterator localIterator = paramNode.attributes().iterator();
      while (localIterator.hasNext())
      {
        Attribute localAttribute = (Attribute)localIterator.next();
        paramNode = localAttribute.getKey().replaceAll("[^-a-zA-Z0-9_:.]", "");
        if (paramNode.matches("[a-zA-Z_:]{1}[-a-zA-Z0-9_:.]*")) {
          paramElement.setAttribute(paramNode, localAttribute.getValue());
        }
      }
    }
    
    private String updateNamespaces(org.jsoup.nodes.Element paramElement)
    {
      Iterator localIterator = paramElement.attributes().iterator();
      if (localIterator.hasNext())
      {
        Attribute localAttribute = (Attribute)localIterator.next();
        String str = localAttribute.getKey();
        if (str.equals("xmlns")) {}
        for (str = "";; str = str.substring("xmlns:".length()))
        {
          this.namespaces.put(str, localAttribute.getValue());
          break;
          if (!str.startsWith("xmlns:")) {
            break;
          }
        }
      }
      int i = paramElement.tagName().indexOf(":");
      if (i > 0) {}
      for (paramElement = paramElement.tagName().substring(0, i);; paramElement = "") {
        return paramElement;
      }
    }
    
    public void head(org.jsoup.nodes.Node paramNode, int paramInt)
    {
      Object localObject;
      if ((paramNode instanceof org.jsoup.nodes.Element))
      {
        paramNode = (org.jsoup.nodes.Element)paramNode;
        localObject = updateNamespaces(paramNode);
        localObject = (String)this.namespaces.get(localObject);
        localObject = this.doc.createElementNS((String)localObject, paramNode.tagName());
        copyAttributes(paramNode, (org.w3c.dom.Element)localObject);
        if (this.dest == null)
        {
          this.doc.appendChild((org.w3c.dom.Node)localObject);
          this.dest = ((org.w3c.dom.Element)localObject);
        }
      }
      for (;;)
      {
        return;
        this.dest.appendChild((org.w3c.dom.Node)localObject);
        break;
        if ((paramNode instanceof TextNode))
        {
          paramNode = (TextNode)paramNode;
          paramNode = this.doc.createTextNode(paramNode.getWholeText());
          this.dest.appendChild(paramNode);
        }
        else if ((paramNode instanceof Comment))
        {
          paramNode = (Comment)paramNode;
          paramNode = this.doc.createComment(paramNode.getData());
          this.dest.appendChild(paramNode);
        }
        else if ((paramNode instanceof DataNode))
        {
          paramNode = (DataNode)paramNode;
          paramNode = this.doc.createTextNode(paramNode.getWholeData());
          this.dest.appendChild(paramNode);
        }
      }
    }
    
    public void tail(org.jsoup.nodes.Node paramNode, int paramInt)
    {
      if (((paramNode instanceof org.jsoup.nodes.Element)) && ((this.dest.getParentNode() instanceof org.w3c.dom.Element))) {
        this.dest = ((org.w3c.dom.Element)this.dest.getParentNode());
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\helper\W3CDom.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */