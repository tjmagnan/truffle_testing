package tech.dcube.companion.fragments;

import android.support.v4.app.Fragment;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;

public abstract class PBTopBarFragment
  extends Fragment
{
  @BindView(2131624335)
  TextView topBarLeftButton;
  @BindView(2131624337)
  TextView topBarRightButton;
  @BindView(2131624336)
  TextView topBarTitle;
  
  protected void hideTopBarLeftButton()
  {
    this.topBarLeftButton.setVisibility(4);
  }
  
  protected void hideTopBarRightButton()
  {
    this.topBarRightButton.setVisibility(4);
  }
  
  @OnClick({2131624335})
  public abstract void onClickTopBarLeftButton();
  
  @OnClick({2131624337})
  public abstract void onClickTopBarRightButton();
  
  protected void setTopBarLeftButtonText(String paramString)
  {
    this.topBarLeftButton.setText(paramString);
  }
  
  protected void setTopBarLeftButtonTextSize(float paramFloat)
  {
    this.topBarLeftButton.setTextSize(2, paramFloat);
  }
  
  protected void setTopBarRightButtonText(String paramString)
  {
    this.topBarRightButton.setText(paramString);
  }
  
  protected void setTopBarRightButtonTextSize(float paramFloat)
  {
    this.topBarRightButton.setTextSize(2, paramFloat);
  }
  
  protected void setTopBarTitle(String paramString)
  {
    this.topBarTitle.setText(paramString);
  }
  
  protected void showTopBarLeftButton()
  {
    this.topBarLeftButton.setVisibility(0);
  }
  
  protected void showTopBarRightButton()
  {
    this.topBarRightButton.setVisibility(0);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBTopBarFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */