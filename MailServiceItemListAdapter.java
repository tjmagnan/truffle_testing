package tech.dcube.companion.customListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import tech.dcube.companion.model.PBShipmentService;

public class MailServiceItemListAdapter
  extends ArrayAdapter<PBShipmentService>
{
  private List<PBShipmentService> dataSet;
  private int lastPosition = -1;
  Context mContext;
  
  public MailServiceItemListAdapter(List<PBShipmentService> paramList, Context paramContext)
  {
    super(paramContext, 2130968654, paramList);
    this.dataSet = paramList;
    this.mContext = paramContext;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    PBShipmentService localPBShipmentService = (PBShipmentService)getItem(paramInt);
    Object localObject2;
    Object localObject1;
    if (paramView == null)
    {
      localObject2 = new ViewHolder(null);
      paramView = LayoutInflater.from(getContext()).inflate(2130968654, paramViewGroup, false);
      ((ViewHolder)localObject2).MailClassName = ((TextView)paramView.findViewById(2131624224));
      ((ViewHolder)localObject2).MailClassCharge = ((TextView)paramView.findViewById(2131624225));
      localObject1 = paramView;
      paramView.setTag(localObject2);
      paramViewGroup = (ViewGroup)localObject2;
      localObject2 = this.mContext;
      if (paramInt <= this.lastPosition) {
        break label202;
      }
    }
    label202:
    for (int i = 2131034132;; i = 2131034129)
    {
      ((View)localObject1).startAnimation(AnimationUtils.loadAnimation((Context)localObject2, i));
      this.lastPosition = paramInt;
      localObject1 = localPBShipmentService.getServiceName();
      double d = localPBShipmentService.getTotalPackageCharge().doubleValue();
      localObject2 = "$" + String.format("%.02f", new Object[] { Double.valueOf(d) });
      paramViewGroup.MailClassName.setText((CharSequence)localObject1);
      paramViewGroup.MailClassCharge.setText((CharSequence)localObject2);
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      localObject1 = paramView;
      break;
    }
  }
  
  public void updateData(List<PBShipmentService> paramList)
  {
    this.dataSet.clear();
    this.dataSet.addAll(paramList);
    notifyDataSetChanged();
  }
  
  private static class ViewHolder
  {
    TextView MailClassCharge;
    TextView MailClassDesc;
    TextView MailClassName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\customListAdapters\MailServiceItemListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */