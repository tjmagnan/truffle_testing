package android.support.design.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

class ViewOffsetBehavior<V extends View>
  extends CoordinatorLayout.Behavior<V>
{
  private int mTempLeftRightOffset = 0;
  private int mTempTopBottomOffset = 0;
  private ViewOffsetHelper mViewOffsetHelper;
  
  public ViewOffsetBehavior() {}
  
  public ViewOffsetBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public int getLeftAndRightOffset()
  {
    if (this.mViewOffsetHelper != null) {}
    for (int i = this.mViewOffsetHelper.getLeftAndRightOffset();; i = 0) {
      return i;
    }
  }
  
  public int getTopAndBottomOffset()
  {
    if (this.mViewOffsetHelper != null) {}
    for (int i = this.mViewOffsetHelper.getTopAndBottomOffset();; i = 0) {
      return i;
    }
  }
  
  protected void layoutChild(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt)
  {
    paramCoordinatorLayout.onLayoutChild(paramV, paramInt);
  }
  
  public boolean onLayoutChild(CoordinatorLayout paramCoordinatorLayout, V paramV, int paramInt)
  {
    layoutChild(paramCoordinatorLayout, paramV, paramInt);
    if (this.mViewOffsetHelper == null) {
      this.mViewOffsetHelper = new ViewOffsetHelper(paramV);
    }
    this.mViewOffsetHelper.onViewLayout();
    if (this.mTempTopBottomOffset != 0)
    {
      this.mViewOffsetHelper.setTopAndBottomOffset(this.mTempTopBottomOffset);
      this.mTempTopBottomOffset = 0;
    }
    if (this.mTempLeftRightOffset != 0)
    {
      this.mViewOffsetHelper.setLeftAndRightOffset(this.mTempLeftRightOffset);
      this.mTempLeftRightOffset = 0;
    }
    return true;
  }
  
  public boolean setLeftAndRightOffset(int paramInt)
  {
    if (this.mViewOffsetHelper != null) {}
    for (boolean bool = this.mViewOffsetHelper.setLeftAndRightOffset(paramInt);; bool = false)
    {
      return bool;
      this.mTempLeftRightOffset = paramInt;
    }
  }
  
  public boolean setTopAndBottomOffset(int paramInt)
  {
    if (this.mViewOffsetHelper != null) {}
    for (boolean bool = this.mViewOffsetHelper.setTopAndBottomOffset(paramInt);; bool = false)
    {
      return bool;
      this.mTempTopBottomOffset = paramInt;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\design\widget\ViewOffsetBehavior.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */