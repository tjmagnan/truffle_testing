package com.testfairy.n;

import android.os.Process;
import com.testfairy.g;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class d
  extends b
{
  private static final int a = 1024;
  private long b = 0L;
  private long c = 0L;
  private String d;
  
  public d(com.testfairy.m.b paramb)
  {
    super(paramb);
    int i = Process.myPid();
    this.d = ("/proc/" + i + "/stat");
  }
  
  public void a()
  {
    try
    {
      Object localObject1 = new java/io/BufferedReader;
      Object localObject2 = new java/io/FileReader;
      ((FileReader)localObject2).<init>(this.d);
      ((BufferedReader)localObject1).<init>((Reader)localObject2, 1024);
      localObject2 = ((BufferedReader)localObject1).readLine();
      ((BufferedReader)localObject1).close();
      localObject1 = ((String)localObject2).split(" ");
      long l2 = Long.parseLong(localObject1[13]);
      long l1 = Long.parseLong(localObject1[14]);
      long l3 = Long.parseLong(localObject1[19]);
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>(4);
      ((HashMap)localObject1).put("utime", Long.valueOf(l2 - this.b));
      ((HashMap)localObject1).put("stime", Long.valueOf(l1 - this.c));
      ((HashMap)localObject1).put("threads", Long.valueOf(l3));
      this.b = l2;
      this.c = l1;
      localObject2 = new com/testfairy/g;
      ((g)localObject2).<init>(14, (Map)localObject1);
      b().a((g)localObject2);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */