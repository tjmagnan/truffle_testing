package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class ShipmentTransaction
  implements Serializable
{
  private static final long serialVersionUID = -3600571777646416865L;
  @Expose
  @SerializedName("id")
  private String id;
  @Expose
  @SerializedName("image_url")
  private String imageUrl;
  @Expose
  @SerializedName("item_height")
  private double itemHeightIn;
  @Expose
  @SerializedName("item_id")
  private String itemId;
  @Expose
  @SerializedName("item_length")
  private double itemLengthIn;
  @Expose
  @SerializedName("item_name")
  private String itemName;
  @Expose
  @SerializedName("item_weight")
  private double itemWeightLb;
  @Expose
  @SerializedName("item_width")
  private double itemWidthIn;
  @Expose
  @SerializedName("manufacturer")
  private String manufacturer;
  private PBPackageType packageType;
  private PBAddress recipientAddress;
  @Expose
  @SerializedName("upc")
  private String upc;
  @Expose
  @SerializedName("user_id")
  private String userId;
  
  public String getId()
  {
    return this.id;
  }
  
  public String getImageUrl()
  {
    return this.imageUrl;
  }
  
  public double getItemHeightIn()
  {
    return this.itemHeightIn;
  }
  
  public String getItemId()
  {
    return this.itemId;
  }
  
  public double getItemLengthIn()
  {
    return this.itemLengthIn;
  }
  
  public String getItemName()
  {
    return this.itemName;
  }
  
  public double getItemWeightLb()
  {
    return this.itemWeightLb;
  }
  
  public double getItemWidthIn()
  {
    return this.itemWidthIn;
  }
  
  public String getManufacturer()
  {
    return this.manufacturer;
  }
  
  public PBPackageType getPackageType()
  {
    return this.packageType;
  }
  
  public PBAddress getRecipientAddress()
  {
    return this.recipientAddress;
  }
  
  public String getUpc()
  {
    return this.upc;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public void setId(String paramString)
  {
    this.id = paramString;
  }
  
  public void setImageUrl(String paramString)
  {
    this.imageUrl = paramString;
  }
  
  public void setItemHeightIn(double paramDouble)
  {
    this.itemHeightIn = paramDouble;
  }
  
  public void setItemId(String paramString)
  {
    this.itemId = paramString;
  }
  
  public void setItemLengthIn(double paramDouble)
  {
    this.itemLengthIn = paramDouble;
  }
  
  public void setItemName(String paramString)
  {
    this.itemName = paramString;
  }
  
  public void setItemWeightLb(double paramDouble)
  {
    this.itemWeightLb = paramDouble;
  }
  
  public void setItemWidthIn(double paramDouble)
  {
    this.itemWidthIn = paramDouble;
  }
  
  public void setManufacturer(String paramString)
  {
    this.manufacturer = paramString;
  }
  
  public void setPackageType(PBPackageType paramPBPackageType)
  {
    this.packageType = paramPBPackageType;
  }
  
  public void setRecipientAddress(PBAddress paramPBAddress)
  {
    this.recipientAddress = paramPBAddress;
  }
  
  public void setUpc(String paramString)
  {
    this.upc = paramString;
  }
  
  public void setUserId(String paramString)
  {
    this.userId = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\ShipmentTransaction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */