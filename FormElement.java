package org.jsoup.nodes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jsoup.Connection;
import org.jsoup.Connection.KeyVal;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection.KeyVal;
import org.jsoup.helper.Validate;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

public class FormElement
  extends Element
{
  private final Elements elements = new Elements();
  
  public FormElement(Tag paramTag, String paramString, Attributes paramAttributes)
  {
    super(paramTag, paramString, paramAttributes);
  }
  
  public FormElement addElement(Element paramElement)
  {
    this.elements.add(paramElement);
    return this;
  }
  
  public Elements elements()
  {
    return this.elements;
  }
  
  public List<Connection.KeyVal> formData()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.elements.iterator();
    while (localIterator.hasNext())
    {
      Object localObject1 = (Element)localIterator.next();
      if ((((Element)localObject1).tag().isFormSubmittable()) && (!((Element)localObject1).hasAttr("disabled")))
      {
        String str = ((Element)localObject1).attr("name");
        if (str.length() != 0)
        {
          Object localObject2 = ((Element)localObject1).attr("type");
          if ("select".equals(((Element)localObject1).tagName()))
          {
            localObject2 = ((Element)localObject1).select("option[selected]");
            int i = 0;
            localObject2 = ((Elements)localObject2).iterator();
            while (((Iterator)localObject2).hasNext())
            {
              localArrayList.add(HttpConnection.KeyVal.create(str, ((Element)((Iterator)localObject2).next()).val()));
              i = 1;
            }
            if (i == 0)
            {
              localObject1 = ((Element)localObject1).select("option").first();
              if (localObject1 != null) {
                localArrayList.add(HttpConnection.KeyVal.create(str, ((Element)localObject1).val()));
              }
            }
          }
          else if (("checkbox".equalsIgnoreCase((String)localObject2)) || ("radio".equalsIgnoreCase((String)localObject2)))
          {
            if (((Element)localObject1).hasAttr("checked"))
            {
              if (((Element)localObject1).val().length() > 0) {}
              for (localObject1 = ((Element)localObject1).val();; localObject1 = "on")
              {
                localArrayList.add(HttpConnection.KeyVal.create(str, (String)localObject1));
                break;
              }
            }
          }
          else
          {
            localArrayList.add(HttpConnection.KeyVal.create(str, ((Element)localObject1).val()));
          }
        }
      }
    }
    return localArrayList;
  }
  
  public Connection submit()
  {
    String str;
    if (hasAttr("action"))
    {
      str = absUrl("action");
      Validate.notEmpty(str, "Could not determine a form action URL for submit. Ensure you set a base URI when parsing.");
      if (!attr("method").toUpperCase().equals("POST")) {
        break label71;
      }
    }
    label71:
    for (Connection.Method localMethod = Connection.Method.POST;; localMethod = Connection.Method.GET)
    {
      return Jsoup.connect(str).data(formData()).method(localMethod);
      str = baseUri();
      break;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\FormElement.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */