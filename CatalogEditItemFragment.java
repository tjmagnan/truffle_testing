package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import com.android.volley.VolleyError;
import java.util.List;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBSendProItem;

public class CatalogEditItemFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  double _height = 0.0D;
  double _length = 0.0D;
  String _productDesc = "";
  String _productName = "";
  String _productUPC = "";
  double _weight = 0.0D;
  double _width = 0.0D;
  private OnEditCatalogListener mListener;
  private String mParam2;
  private int mPosition;
  PBSendProItem pbSendProItem;
  EditText productDesc;
  EditText productHeight;
  EditText productLenght;
  EditText productName;
  EditText productUPC;
  EditText productWeight;
  EditText productWidth;
  
  public static CatalogEditItemFragment newInstance(int paramInt, String paramString)
  {
    CatalogEditItemFragment localCatalogEditItemFragment = new CatalogEditItemFragment();
    Bundle localBundle = new Bundle();
    localBundle.putInt("param1", paramInt);
    localBundle.putString("param2", paramString);
    localCatalogEditItemFragment.setArguments(localBundle);
    return localCatalogEditItemFragment;
  }
  
  void callAddtoCatalogApi() {}
  
  boolean checkEditInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput()) && (checkLengthInput()) && (checkWidthInput()) && (checkHeightInput()) && (checkWeightInput()) && (checkUPCInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkEditNameInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkHeightInput()
  {
    bool = false;
    try
    {
      this._height = Double.parseDouble(this.productHeight.getText().toString());
      if (this._height > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._height = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkLengthInput()
  {
    bool = false;
    try
    {
      this._length = Double.parseDouble(this.productLenght.getText().toString());
      if (this._length > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._length = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkProductDescInput()
  {
    this._productDesc = this.productDesc.getText().toString();
    return true;
  }
  
  boolean checkProductNameInput()
  {
    if (!this.productName.getText().toString().equals(""))
    {
      this._productName = this.productName.getText().toString();
      if (!this._productName.equals("")) {
        break label57;
      }
    }
    label57:
    for (boolean bool = false;; bool = true)
    {
      return bool;
      this._productName = "";
      break;
    }
  }
  
  boolean checkUPCInput()
  {
    if (this.productUPC.getText().toString() != "") {
      this._productUPC = this.productUPC.getText().toString();
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      this._productUPC = "";
    }
  }
  
  boolean checkWeightInput()
  {
    bool = false;
    try
    {
      this._weight = Double.parseDouble(this.productWeight.getText().toString());
      if (this._weight > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._weight = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkWidthInput()
  {
    bool = false;
    try
    {
      this._width = Double.parseDouble(this.productWidth.getText().toString());
      if (this._width > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._width = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  void hideKeyboard()
  {
    UtilManager.hideSoftKeyboard(getActivity());
  }
  
  @OnClick({2131624097})
  public void hideSoftKeyboardOnClick()
  {
    hideKeyboard();
  }
  
  @OnFocusChange({2131624097})
  public void hideSoftKeyboardOnFocus(boolean paramBoolean)
  {
    if (paramBoolean) {}
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnEditCatalogListener))
    {
      this.mListener = ((OnEditCatalogListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnAddCatalogItemListener");
  }
  
  public void onCancelButtonPressed()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onEditCatalogInteraction("action_cancel_button", this.mPosition);
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    onCancelButtonPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    if (!checkEditNameInputs()) {
      displayToast("Product Name can't be Empty");
    }
    for (;;)
    {
      return;
      if (!checkEditInputs())
      {
        displayToast("Dimensions and Weight can't be Zero");
      }
      else
      {
        this.pbSendProItem.setItemName(this._productName);
        this.pbSendProItem.setItemLengthIn(this._length);
        this.pbSendProItem.setItemWidthIn(this._width);
        this.pbSendProItem.setItemHeightIn(this._height);
        this.pbSendProItem.setItemWeightLb(this._weight);
        this.pbSendProItem.setUpc(this._productUPC);
        try
        {
          ServerManager localServerManager = ServerManager.getInstance();
          Context localContext = getContext();
          PBSendProItem localPBSendProItem = this.pbSendProItem;
          ServerManager.RequestCompletionHandler local1 = new tech/dcube/companion/fragments/CatalogEditItemFragment$1;
          local1.<init>(this);
          localServerManager.updateItem(localContext, localPBSendProItem, local1);
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
        }
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mPosition = getArguments().getInt("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968626, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Edit Item");
    setTopBarLeftButtonText("Cancel");
    setTopBarRightButtonText("Save");
    this.productName = ((EditText)paramLayoutInflater.findViewById(2131624099));
    this.productDesc = ((EditText)paramLayoutInflater.findViewById(2131624100));
    this.productLenght = ((EditText)paramLayoutInflater.findViewById(2131624101));
    this.productWidth = ((EditText)paramLayoutInflater.findViewById(2131624102));
    this.productHeight = ((EditText)paramLayoutInflater.findViewById(2131624103));
    this.productWeight = ((EditText)paramLayoutInflater.findViewById(2131624104));
    this.productUPC = ((EditText)paramLayoutInflater.findViewById(2131624105));
    updateEditItemLayout();
    hideKeyboard();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPause()
  {
    super.onPause();
    hideKeyboard();
  }
  
  public void onResume()
  {
    super.onResume();
    hideKeyboard();
  }
  
  public void onSaveButtonPressed()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onEditCatalogInteraction("action_save_button", this.mPosition);
    }
  }
  
  public void updateEditItemLayout()
  {
    this.pbSendProItem = ((PBSendProItem)DataManager.getInstance().getItems().get(this.mPosition));
    String str4 = this.pbSendProItem.getItemName();
    String str1 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemLengthIn()) });
    String str5 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemWidthIn()) });
    String str6 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemHeightIn()) });
    String str2 = String.format("%.01f", new Object[] { Double.valueOf(this.pbSendProItem.getItemWeightLb()) });
    String str3 = this.pbSendProItem.getUpc();
    this.productName.setText(str4);
    this.productDesc.setText("Product Description");
    this.productLenght.setText(str1);
    this.productWidth.setText(str5);
    this.productHeight.setText(str6);
    this.productWeight.setText(str2);
    this.productUPC.setText(str3);
  }
  
  public static abstract interface OnEditCatalogListener
  {
    public abstract void onEditCatalogInteraction(String paramString, int paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\CatalogEditItemFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */