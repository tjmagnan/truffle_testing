package io.keen.client.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import io.keen.client.java.KeenNetworkStatusHandler;

public class AndroidNetworkStatusHandler
  implements KeenNetworkStatusHandler
{
  private final Context context;
  
  public AndroidNetworkStatusHandler(Context paramContext)
  {
    this.context = paramContext;
  }
  
  public boolean isNetworkConnected()
  {
    boolean bool = true;
    int i;
    if (this.context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0)
    {
      i = 1;
      if (i != 0) {
        break label27;
      }
    }
    for (;;)
    {
      return bool;
      i = 0;
      break;
      label27:
      NetworkInfo localNetworkInfo = ((ConnectivityManager)this.context.getSystemService("connectivity")).getActiveNetworkInfo();
      if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected())) {
        bool = false;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\keen\client\android\AndroidNetworkStatusHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */