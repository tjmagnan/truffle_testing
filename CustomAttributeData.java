package com.crashlytics.android.core.internal.models;

public class CustomAttributeData
{
  public final String key;
  public final String value;
  
  public CustomAttributeData(String paramString1, String paramString2)
  {
    this.key = paramString1;
    this.value = paramString2;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\internal\models\CustomAttributeData.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */