package io.keen.client.android;

import android.os.Build.VERSION;
import io.keen.client.java.KeenJsonHandler;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class AndroidJsonHandler
  implements KeenJsonHandler
{
  private static final int COPY_BUFFER_SIZE = 4096;
  private boolean isWrapNestedMapsAndCollections;
  private JsonObjectManager jsonObjectManager;
  
  public AndroidJsonHandler()
  {
    if (Build.VERSION.SDK_INT < 19) {}
    for (boolean bool = true;; bool = false)
    {
      this.isWrapNestedMapsAndCollections = bool;
      this.jsonObjectManager = null;
      return;
    }
  }
  
  private JSONArray convertCollectionToJSONArray(Collection paramCollection)
    throws IOException
  {
    if ((this.isWrapNestedMapsAndCollections) && (requiresWrap(paramCollection)))
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = paramCollection.iterator();
      paramCollection = localArrayList;
      if (localIterator.hasNext())
      {
        Object localObject = localIterator.next();
        paramCollection = (Collection)localObject;
        if ((localObject instanceof Map)) {
          paramCollection = convertMapToJSONObject((Map)localObject);
        }
        for (;;)
        {
          localArrayList.add(paramCollection);
          break;
          if ((localObject instanceof Collection)) {
            paramCollection = convertCollectionToJSONArray((Collection)localObject);
          }
        }
      }
    }
    return getJsonObjectManager().newArray(paramCollection);
  }
  
  private JSONObject convertMapToJSONObject(Map paramMap)
    throws IOException
  {
    Object localObject1;
    if ((this.isWrapNestedMapsAndCollections) && (requiresWrap(paramMap)))
    {
      HashMap localHashMap = new HashMap();
      Iterator localIterator = paramMap.keySet().iterator();
      localObject1 = localHashMap;
      if (localIterator.hasNext())
      {
        Object localObject3 = localIterator.next();
        Object localObject2 = paramMap.get(localObject3);
        localObject1 = localObject2;
        if ((localObject2 instanceof Map)) {
          localObject1 = convertMapToJSONObject((Map)localObject2);
        }
        for (;;)
        {
          localHashMap.put(localObject3, localObject1);
          break;
          if ((localObject2 instanceof Collection)) {
            localObject1 = convertCollectionToJSONArray((Collection)localObject2);
          } else if ((localObject2 instanceof Object[])) {
            localObject1 = convertCollectionToJSONArray(Arrays.asList((Object[])localObject2));
          }
        }
      }
    }
    else
    {
      localObject1 = paramMap;
    }
    return getJsonObjectManager().newObject((Map)localObject1);
  }
  
  private JsonObjectManager getJsonObjectManager()
  {
    if (this.jsonObjectManager == null) {
      this.jsonObjectManager = new AndroidJsonObjectManager(null);
    }
    return this.jsonObjectManager;
  }
  
  /* Error */
  private static String readerToString(Reader paramReader)
    throws IOException
  {
    // Byte code:
    //   0: new 121	java/io/StringWriter
    //   3: dup
    //   4: invokespecial 122	java/io/StringWriter:<init>	()V
    //   7: astore_2
    //   8: sipush 4096
    //   11: newarray <illegal type>
    //   13: astore_3
    //   14: aload_0
    //   15: aload_3
    //   16: invokevirtual 128	java/io/Reader:read	([C)I
    //   19: istore_1
    //   20: iload_1
    //   21: iconst_m1
    //   22: if_icmpne +14 -> 36
    //   25: aload_2
    //   26: invokevirtual 132	java/io/StringWriter:toString	()Ljava/lang/String;
    //   29: astore_2
    //   30: aload_0
    //   31: invokevirtual 135	java/io/Reader:close	()V
    //   34: aload_2
    //   35: areturn
    //   36: aload_2
    //   37: aload_3
    //   38: iconst_0
    //   39: iload_1
    //   40: invokevirtual 139	java/io/StringWriter:write	([CII)V
    //   43: goto -29 -> 14
    //   46: astore_2
    //   47: aload_0
    //   48: invokevirtual 135	java/io/Reader:close	()V
    //   51: aload_2
    //   52: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	53	0	paramReader	Reader
    //   19	21	1	i	int
    //   7	30	2	localObject1	Object
    //   46	6	2	localObject2	Object
    //   13	25	3	arrayOfChar	char[]
    // Exception table:
    //   from	to	target	type
    //   8	14	46	finally
    //   14	20	46	finally
    //   25	30	46	finally
    //   36	43	46	finally
  }
  
  private static boolean requiresWrap(Collection paramCollection)
  {
    Iterator localIterator = paramCollection.iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      paramCollection = localIterator.next();
    } while ((!(paramCollection instanceof Collection)) && (!(paramCollection instanceof Map)));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private static boolean requiresWrap(Map paramMap)
  {
    Iterator localIterator = paramMap.values().iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      paramMap = localIterator.next();
    } while ((!(paramMap instanceof Collection)) && (!(paramMap instanceof Map)) && (!(paramMap instanceof Object[])));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Map<String, Object> readJson(Reader paramReader)
    throws IOException
  {
    if (paramReader == null) {
      throw new IllegalArgumentException("Reader must not be null");
    }
    paramReader = readerToString(paramReader);
    Object localObject;
    try
    {
      localObject = JsonHelper.fromJson(getJsonObjectManager().newTokener(paramReader).nextValue());
      paramReader = null;
      if (localObject == null)
      {
        paramReader = new java/lang/IllegalArgumentException;
        paramReader.<init>("Empty reader or ill-formatted JSON encountered.");
        throw paramReader;
      }
    }
    catch (JSONException paramReader)
    {
      throw new IOException(paramReader);
    }
    if ((localObject instanceof Map)) {
      paramReader = (Map)localObject;
    }
    for (;;)
    {
      return paramReader;
      if ((localObject instanceof List))
      {
        paramReader = new java/util/LinkedHashMap;
        paramReader.<init>();
        paramReader.put("io.keen.client.java.__fake_root", localObject);
      }
    }
  }
  
  protected void setJsonObjectManager(JsonObjectManager paramJsonObjectManager)
  {
    this.jsonObjectManager = paramJsonObjectManager;
  }
  
  public void setWrapNestedMapsAndCollections(boolean paramBoolean)
  {
    this.isWrapNestedMapsAndCollections = paramBoolean;
  }
  
  public void writeJson(Writer paramWriter, Map<String, ?> paramMap)
    throws IOException
  {
    if (paramWriter == null) {
      throw new IllegalArgumentException("Writer must not be null");
    }
    paramMap = convertMapToJSONObject(paramMap);
    paramWriter.write(getJsonObjectManager().stringify(paramMap));
    paramWriter.close();
  }
  
  private static class AndroidJsonObjectManager
    implements AndroidJsonHandler.JsonObjectManager
  {
    public JSONArray newArray(Collection<?> paramCollection)
    {
      return new JSONArray(paramCollection);
    }
    
    public JSONObject newObject(Map<String, ?> paramMap)
    {
      return new JSONObject(paramMap);
    }
    
    public JSONTokener newTokener(String paramString)
    {
      return new JSONTokener(paramString);
    }
    
    public String stringify(JSONObject paramJSONObject)
    {
      return paramJSONObject.toString();
    }
  }
  
  protected static abstract interface JsonObjectManager
  {
    public abstract JSONArray newArray(Collection<?> paramCollection);
    
    public abstract JSONObject newObject(Map<String, ?> paramMap);
    
    public abstract JSONTokener newTokener(String paramString);
    
    public abstract String stringify(JSONObject paramJSONObject);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\keen\client\android\AndroidJsonHandler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */