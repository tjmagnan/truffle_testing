package org.jsoup.nodes;

public class BooleanAttribute
  extends Attribute
{
  public BooleanAttribute(String paramString)
  {
    super(paramString, "");
  }
  
  protected boolean isBooleanAttribute()
  {
    return true;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\BooleanAttribute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */