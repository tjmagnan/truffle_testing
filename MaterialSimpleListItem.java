package com.afollestad.materialdialogs.simplelist;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import com.afollestad.materialdialogs.util.DialogUtils;

public class MaterialSimpleListItem
{
  private final Builder builder;
  
  private MaterialSimpleListItem(Builder paramBuilder)
  {
    this.builder = paramBuilder;
  }
  
  @ColorInt
  public int getBackgroundColor()
  {
    return this.builder.backgroundColor;
  }
  
  public CharSequence getContent()
  {
    return this.builder.content;
  }
  
  public Drawable getIcon()
  {
    return this.builder.icon;
  }
  
  public int getIconPadding()
  {
    return this.builder.iconPadding;
  }
  
  public long getId()
  {
    return this.builder.id;
  }
  
  @Nullable
  public Object getTag()
  {
    return this.builder.tag;
  }
  
  public String toString()
  {
    if (getContent() != null) {}
    for (String str = getContent().toString();; str = "(no content)") {
      return str;
    }
  }
  
  public static class Builder
  {
    int backgroundColor;
    protected CharSequence content;
    private final Context context;
    protected Drawable icon;
    int iconPadding;
    protected long id;
    Object tag;
    
    public Builder(Context paramContext)
    {
      this.context = paramContext;
      this.backgroundColor = Color.parseColor("#BCBCBC");
    }
    
    public Builder backgroundColor(@ColorInt int paramInt)
    {
      this.backgroundColor = paramInt;
      return this;
    }
    
    public Builder backgroundColorAttr(@AttrRes int paramInt)
    {
      return backgroundColor(DialogUtils.resolveColor(this.context, paramInt));
    }
    
    public Builder backgroundColorRes(@ColorRes int paramInt)
    {
      return backgroundColor(DialogUtils.getColor(this.context, paramInt));
    }
    
    public MaterialSimpleListItem build()
    {
      return new MaterialSimpleListItem(this, null);
    }
    
    public Builder content(@StringRes int paramInt)
    {
      return content(this.context.getString(paramInt));
    }
    
    public Builder content(CharSequence paramCharSequence)
    {
      this.content = paramCharSequence;
      return this;
    }
    
    public Builder icon(@DrawableRes int paramInt)
    {
      return icon(ContextCompat.getDrawable(this.context, paramInt));
    }
    
    public Builder icon(Drawable paramDrawable)
    {
      this.icon = paramDrawable;
      return this;
    }
    
    public Builder iconPadding(@IntRange(from=0L, to=2147483647L) int paramInt)
    {
      this.iconPadding = paramInt;
      return this;
    }
    
    public Builder iconPaddingDp(@IntRange(from=0L, to=2147483647L) int paramInt)
    {
      this.iconPadding = ((int)TypedValue.applyDimension(1, paramInt, this.context.getResources().getDisplayMetrics()));
      return this;
    }
    
    public Builder iconPaddingRes(@DimenRes int paramInt)
    {
      return iconPadding(this.context.getResources().getDimensionPixelSize(paramInt));
    }
    
    public Builder id(long paramLong)
    {
      this.id = paramLong;
      return this;
    }
    
    public Builder tag(@Nullable Object paramObject)
    {
      this.tag = paramObject;
      return this;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\simplelist\MaterialSimpleListItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */