package com.testfairy.d;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import com.testfairy.g;
import com.testfairy.h.c;
import com.testfairy.n;
import com.testfairy.p.r;
import com.testfairy.p.s;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.WeakHashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class q
  extends TimerTask
{
  private static final int b = 0;
  private static final Set y = new HashSet(8);
  private long A;
  private final f B = new f(new h()
  {
    public SurfaceHolder a(SurfaceHolder paramAnonymousSurfaceHolder)
    {
      return null;
    }
    
    public void a(Bitmap paramAnonymousBitmap, com.testfairy.p.q paramAnonymousq)
    {
      a(paramAnonymousBitmap, paramAnonymousq, null);
    }
    
    public void a(Bitmap paramAnonymousBitmap, com.testfairy.p.q paramAnonymousq, List paramAnonymousList)
    {
      q.a(q.this, paramAnonymousBitmap);
      q.a(q.this, paramAnonymousList);
      q.a(q.this, paramAnonymousq);
      q.a(q.this).post(q.this.a);
      q.b(q.this).c().a(paramAnonymousBitmap);
    }
    
    public void a(d paramAnonymousd, long paramAnonymousLong)
    {
      q.a(q.this, paramAnonymousLong);
      q.d(q.this).a(q.c(q.this));
      q.b(q.this).c().a(paramAnonymousd);
      q.a(q.this, null);
      q.a(q.this, paramAnonymousd);
      q.a(q.this, null);
      q.a(q.this, null);
      q.a(q.this).post(q.this.a);
    }
  });
  public Runnable a = new Runnable()
  {
    private ByteArrayOutputStream b = new ByteArrayOutputStream();
    private final WeakHashMap c = new WeakHashMap(2);
    
    private void a(String paramAnonymousString, Bitmap paramAnonymousBitmap, com.testfairy.p.q paramAnonymousq)
    {
      if (com.testfairy.p.b.a(paramAnonymousBitmap))
      {
        Log.d(com.testfairy.e.a, n.ad);
        q.a(q.this, null);
        q.a(q.this, null);
        q.a(q.this, false);
        q.b(q.this, System.currentTimeMillis());
      }
      for (;;)
      {
        return;
        try
        {
          long l = System.currentTimeMillis();
          this.b.reset();
          int i = q.j(q.this).u();
          Bitmap.createScaledBitmap(paramAnonymousBitmap, (int)(paramAnonymousBitmap.getWidth() * q.j(q.this).v()), (int)(paramAnonymousBitmap.getHeight() * q.j(q.this).v()), true).compress(Bitmap.CompressFormat.JPEG, i, this.b);
          q.k(q.this);
          if (paramAnonymousq != null)
          {
            paramAnonymousBitmap = new java/util/HashMap;
            paramAnonymousBitmap.<init>(2);
            paramAnonymousBitmap.put("seq", Integer.valueOf(q.l(q.this)));
            paramAnonymousBitmap.put("viewNode", paramAnonymousq);
            paramAnonymousq = new com/testfairy/g;
            paramAnonymousq.<init>(23, paramAnonymousBitmap);
            q.m(q.this).a(paramAnonymousq);
          }
          paramAnonymousBitmap = new java/util/HashMap;
          paramAnonymousBitmap.<init>(8);
          paramAnonymousBitmap.put("sessionToken", q.b(q.this).c().a());
          paramAnonymousBitmap.put("seq", String.valueOf(q.l(q.this)));
          paramAnonymousBitmap.put("timestamp", String.valueOf(l));
          paramAnonymousBitmap.put(n.af, paramAnonymousString);
          paramAnonymousBitmap.put("type", String.valueOf(0));
          paramAnonymousBitmap.put("lastScreenshotTime", String.valueOf(q.c(q.this)));
          paramAnonymousBitmap.put("interval", String.valueOf(q.d(q.this).a()));
          if ((q.n(q.this) != null) && (q.n(q.this).size() > 0)) {
            a(q.n(q.this), paramAnonymousBitmap);
          }
          q.a locala = new com/testfairy/d/q$a;
          locala.<init>(q.this, this.b.size());
          paramAnonymousq = this.b.toByteArray();
          q localq = q.this;
          paramAnonymousString = new com/testfairy/d/i;
          paramAnonymousString.<init>(paramAnonymousBitmap, paramAnonymousq, locala);
          q.a(localq, paramAnonymousString);
          q.o(q.this).a(paramAnonymousBitmap, paramAnonymousq, locala);
        }
        catch (Throwable paramAnonymousString)
        {
          Log.e(com.testfairy.e.a, n.V);
          q.p(q.this);
        }
      }
    }
    
    private void a(List paramAnonymousList, Map paramAnonymousMap)
    {
      JSONArray localJSONArray1 = null;
      Object localObject2 = r.a(((a)paramAnonymousList.get(0)).c(), WebView.class);
      Object localObject1 = localJSONArray1;
      if (localObject2 != null)
      {
        localObject1 = localJSONArray1;
        if (((List)localObject2).size() > 0)
        {
          localObject2 = (WebView)((List)localObject2).get(0);
          localObject1 = localJSONArray1;
          if (this.c.containsKey(localObject2)) {
            localObject1 = (String)this.c.get(localObject2);
          }
        }
      }
      localObject2 = new JSONObject();
      JSONArray localJSONArray2 = new JSONArray();
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("network", ((a)paramAnonymousList.get(0)).a());
      localJSONArray1 = new JSONArray();
      localJSONArray1.put(((a)paramAnonymousList.get(0)).b().left);
      localJSONArray1.put(((a)paramAnonymousList.get(0)).b().top);
      localJSONArray1.put(((a)paramAnonymousList.get(0)).b().right);
      localJSONArray1.put(((a)paramAnonymousList.get(0)).b().bottom);
      localJSONObject.put("rect", localJSONArray1);
      if (localObject1 != null) {
        localJSONObject.put("destinationUrl", localObject1);
      }
      localJSONArray2.put(localJSONObject);
      ((JSONObject)localObject2).put("ads", localJSONArray2);
      paramAnonymousMap.put("extra", Uri.encode(((JSONObject)localObject2).toString()));
    }
    
    public void run()
    {
      if (q.f(q.this) != null)
      {
        q.f(q.this).a = com.testfairy.p.b.a(q.f(q.this).a(), q.f(q.this).b(), q.f(q.this).c());
        q.a(q.this, Bitmap.createBitmap(q.f(q.this).a, q.f(q.this).b(), q.f(q.this).c(), Bitmap.Config.ARGB_8888));
      }
      if (q.g(q.this) == null) {
        q.a(q.this, false);
      }
      for (;;)
      {
        return;
        if ((q.h(q.this) != null) && (!q.h(q.this).a(q.g(q.this))))
        {
          q.a(q.this, null);
          q.a(q.this, null);
          q.a(q.this, false);
          q.b(q.this, System.currentTimeMillis());
        }
        else
        {
          q.a(q.this, new com.testfairy.p.e(q.g(q.this)));
          String str = "NONE";
          if (q.b(q.this).j() != null) {
            str = q.b(q.this).j();
          }
          a(str, q.g(q.this), q.i(q.this));
        }
      }
    }
  };
  private final long c = 150L;
  private final com.testfairy.a.b d;
  private final HandlerThread e;
  private boolean f = false;
  private final com.testfairy.m.b g;
  private final com.testfairy.e.a h;
  private final b i;
  private com.testfairy.k.d j;
  private i k;
  private final com.testfairy.a.a l;
  private final Handler m;
  private boolean n = false;
  private boolean o = false;
  private long p = 0L;
  private boolean q = false;
  private int r = 0;
  private Bitmap s = null;
  private d t = null;
  private com.testfairy.p.q u = null;
  private List v = null;
  private com.testfairy.p.e w = null;
  private Class x = null;
  private b z;
  
  public q(com.testfairy.m.b paramb, com.testfairy.e.a parama, com.testfairy.a.a parama1, com.testfairy.a.b paramb1, b paramb2)
  {
    try
    {
      this.x = Class.forName(n.aD);
      Log.d(com.testfairy.e.a, "unityPlayerClass found was found.");
      this.g = paramb;
      this.h = parama;
      this.l = parama1;
      this.d = paramb1;
      this.i = paramb2;
      this.z = new b(parama.w());
      a(parama1.c().b());
      this.B.a(this.h.p());
      this.e = new HandlerThread("testfairy-screenshot-handler", 1);
      this.e.start();
      this.m = new Handler(this.e.getLooper());
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;)
      {
        this.x = null;
      }
    }
  }
  
  private int a(List paramList)
  {
    paramList = paramList.iterator();
    int i1 = 0;
    if (paramList.hasNext())
    {
      View localView = (View)paramList.next();
      if (i1 >= localView.getHeight()) {
        break label46;
      }
      i1 = localView.getHeight();
    }
    label46:
    for (;;)
    {
      break;
      return i1;
    }
  }
  
  private List a(View[] paramArrayOfView)
  {
    paramArrayOfView = (View[])Arrays.copyOf(paramArrayOfView, paramArrayOfView.length);
    if (Build.VERSION.SDK_INT >= 21) {
      Arrays.sort(paramArrayOfView, new Comparator()
      {
        public int a(View paramAnonymousView1, View paramAnonymousView2)
        {
          return Float.compare(paramAnonymousView1.getZ(), paramAnonymousView2.getZ());
        }
      });
    }
    ArrayList localArrayList = new ArrayList();
    int i2 = paramArrayOfView.length;
    for (int i1 = 0; i1 < i2; i1++)
    {
      View localView = paramArrayOfView[i1];
      if (localView.isShown()) {
        localArrayList.add(localView);
      }
    }
    return localArrayList;
  }
  
  public static void a(View paramView)
  {
    if (paramView.getId() == -1) {
      paramView.setId(r.a());
    }
    a(Integer.valueOf(paramView.getId()));
  }
  
  private void a(i parami)
  {
    try
    {
      Log.d(com.testfairy.e.a, "Resending previous screenshot");
      this.j.a(parami.a, parami.b, parami.c);
      return;
    }
    catch (Throwable parami)
    {
      for (;;)
      {
        Log.e(com.testfairy.e.a, n.V);
        c();
      }
    }
  }
  
  public static void a(Integer paramInteger)
  {
    y.add(paramInteger);
  }
  
  private void a(final List paramList, final Set paramSet)
  {
    final int i1 = a(paramList);
    final int i2 = b(paramList);
    new Handler(Looper.getMainLooper()).post(new Runnable()
    {
      public void run()
      {
        long l = System.currentTimeMillis();
        q.e(q.this).a(i2, i1, paramSet, paramList);
        q.a(q.this, System.currentTimeMillis() - l);
        q.d(q.this).a(q.c(q.this));
      }
    });
  }
  
  private int b(List paramList)
  {
    Iterator localIterator = paramList.iterator();
    int i1 = 0;
    if (localIterator.hasNext())
    {
      paramList = (View)localIterator.next();
      if (i1 >= paramList.getWidth()) {
        break label46;
      }
      i1 = paramList.getWidth();
    }
    label46:
    for (;;)
    {
      break;
      return i1;
    }
  }
  
  private View b(View[] paramArrayOfView)
  {
    Object localObject = this.d.a();
    if (localObject != null)
    {
      localObject = ((Activity)localObject).getWindow().getDecorView();
      if (!((View)localObject).isShown()) {}
    }
    for (paramArrayOfView = (View[])localObject;; paramArrayOfView = paramArrayOfView[0]) {
      return paramArrayOfView;
    }
  }
  
  private void b()
  {
    this.q = true;
    this.o = false;
    this.p = System.currentTimeMillis();
  }
  
  private boolean b(View paramView)
  {
    boolean bool;
    if (this.x == null) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      paramView = (ArrayList)r.a(paramView, this.x);
      if (paramView.isEmpty())
      {
        bool = false;
      }
      else
      {
        paramView = (View)paramView.get(0);
        this.B.a(paramView);
        bool = true;
      }
    }
  }
  
  private void c()
  {
    this.q = false;
    this.k = null;
    this.s = null;
    this.o = false;
    this.p = System.currentTimeMillis();
  }
  
  public void a()
  {
    this.e.quit();
  }
  
  public void a(Bitmap paramBitmap)
  {
    this.s = paramBitmap;
    this.m.post(this.a);
  }
  
  public void a(com.testfairy.k.d paramd)
  {
    this.j = paramd;
  }
  
  public void a(boolean paramBoolean)
  {
    this.n = paramBoolean;
  }
  
  public void run()
  {
    if (this.f) {}
    for (;;)
    {
      return;
      if (this.j == null) {
        Log.d(com.testfairy.e.a, "restClient is not set!!");
      } else if (!this.n) {
        if ((this.h.l()) && (!com.testfairy.k.b.a())) {
          this.i.a();
        } else if ((!com.testfairy.f.d.a()) && (this.l.f()) && (!this.o) && (System.currentTimeMillis() - this.p >= this.z.a())) {
          if ((this.q) && (this.k != null))
          {
            this.o = true;
            a(this.k);
          }
          else
          {
            View[] arrayOfView = s.a();
            if ((arrayOfView != null) && (arrayOfView.length != 0))
            {
              List localList = a(arrayOfView);
              this.o = true;
              if (!b(b(arrayOfView))) {
                a(localList, y);
              }
            }
          }
        }
      }
    }
  }
  
  private class a
    extends c
  {
    private int b;
    
    public a(int paramInt)
    {
      this.b = paramInt;
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      q.q(q.this).a(this.b);
      q.p(q.this);
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      Log.d(com.testfairy.e.a, "Failed to send screenshot # " + q.l(q.this) + ", Message: " + paramThrowable.getMessage());
      q.q(q.this).a();
      q.r(q.this);
    }
  }
  
  public static abstract interface b
  {
    public abstract void a();
    
    public abstract void a(int paramInt);
    
    public abstract void a(long paramLong);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */