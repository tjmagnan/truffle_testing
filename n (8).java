package com.testfairy.n;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.g;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class h
  extends b
{
  private static final String a = "lat";
  private static final String b = "long";
  private static final String c = "accuracy";
  private static final String d = "satCount";
  private LocationManager e;
  private GpsStatus f = null;
  private double g = 0.0D;
  private double h = 0.0D;
  private float i = 0.0F;
  private int j = 0;
  private double k = 0.0D;
  private double l = 0.0D;
  private float m = 0.0F;
  private int n = 0;
  private boolean o = false;
  private GpsStatus.Listener p = new GpsStatus.Listener()
  {
    private int a(Iterable paramAnonymousIterable)
    {
      Iterator localIterator = paramAnonymousIterable.iterator();
      for (int i = 0; localIterator.hasNext(); i++) {
        paramAnonymousIterable = (GpsSatellite)localIterator.next();
      }
      return i;
    }
    
    public void onGpsStatusChanged(int paramAnonymousInt)
    {
      if (paramAnonymousInt == 4) {}
      try
      {
        h.a(h.this, h.b(h.this).getGpsStatus(h.a(h.this)));
        h.a(h.this, a(h.a(h.this).getSatellites()));
        return;
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
    }
  };
  private boolean q = false;
  
  public h(com.testfairy.m.b paramb, LocationManager paramLocationManager)
  {
    super(paramb);
    this.e = paramLocationManager;
  }
  
  private void c()
  {
    if (!this.o)
    {
      this.e.addGpsStatusListener(this.p);
      this.o = true;
    }
  }
  
  public void a()
  {
    int i2 = 1;
    int i1 = 0;
    if (this.k != this.g)
    {
      this.k = this.g;
      i1 = 1;
    }
    if (this.l != this.h)
    {
      this.l = this.h;
      i1 = 1;
    }
    if (this.m != this.i)
    {
      this.m = this.i;
      i1 = 1;
    }
    if (this.n != this.j)
    {
      this.n = this.j;
      i1 = i2;
    }
    for (;;)
    {
      if (i1 != 0)
      {
        Object localObject = new HashMap(4);
        ((Map)localObject).put("lat", Double.valueOf(this.k));
        ((Map)localObject).put("long", Double.valueOf(this.l));
        ((Map)localObject).put("accuracy", Double.valueOf(this.m));
        ((Map)localObject).put("satCount", Double.valueOf(this.n));
        localObject = new g(6, (Map)localObject);
        b().a((g)localObject);
      }
      return;
    }
  }
  
  public void a(Location paramLocation)
  {
    for (;;)
    {
      try
      {
        this.g = paramLocation.getLatitude();
        this.h = paramLocation.getLongitude();
        if (paramLocation.hasAccuracy())
        {
          f1 = paramLocation.getAccuracy();
          this.i = f1;
          c();
          return;
        }
      }
      catch (Exception paramLocation)
      {
        float f1;
        if (this.q) {
          continue;
        }
        Log.e(e.a, "Error in onLocationChange", paramLocation);
        this.q = true;
        continue;
      }
      f1 = 0.0F;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */