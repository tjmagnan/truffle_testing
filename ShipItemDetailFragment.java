package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.CatalogItemDataModel;
import tech.dcube.companion.model.PBSendProItem;
import tech.dcube.companion.model.PBShipment;

public class ShipItemDetailFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "name";
  private static final String ARG_PARAM10 = "upcFound";
  private static final String ARG_PARAM2 = "description";
  private static final String ARG_PARAM3 = "lenght";
  private static final String ARG_PARAM4 = "width";
  private static final String ARG_PARAM5 = "height";
  private static final String ARG_PARAM6 = "weight";
  private static final String ARG_PARAM7 = "upc";
  private static final String ARG_PARAM8 = "tag";
  private static final String ARG_PARAM9 = "img_url";
  double _height = 0.0D;
  double _length = 0.0D;
  String _productDesc = "";
  String _productName = "";
  String _productUPC = "";
  double _weight = 0.0D;
  double _width = 0.0D;
  Switch addItemToCatalog;
  CatalogItemDataModel catalogItemDataModel;
  private String mImg_url;
  private OnShipItemDetailListener mListener;
  private String mProductDesc;
  private Double mProductHeight;
  private Double mProductLenght;
  private String mProductName;
  private String mProductUPC;
  private Double mProductWeight;
  private Double mProductWidth;
  private boolean mUPCFound = true;
  EditText productDesc;
  EditText productHeight;
  EditText productLenght;
  EditText productName;
  EditText productUPC;
  EditText productWeight;
  EditText productWidth;
  private String tag = getClass().getName();
  
  public static ShipItemDetailFragment newInstance(String paramString1, String paramString2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, String paramString3, String paramString4, boolean paramBoolean, String paramString5)
  {
    ShipItemDetailFragment localShipItemDetailFragment = new ShipItemDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("name", paramString1);
    localBundle.putString("description", paramString2);
    localBundle.putDouble("lenght", paramDouble1);
    localBundle.putDouble("width", paramDouble2);
    localBundle.putDouble("height", paramDouble3);
    localBundle.putDouble("weight", paramDouble4);
    localBundle.putString("upc", paramString3);
    localBundle.putString("tag", paramString5);
    localBundle.putString("img_url", paramString4);
    localBundle.putBoolean("upcFound", paramBoolean);
    localShipItemDetailFragment.setArguments(localBundle);
    return localShipItemDetailFragment;
  }
  
  boolean checkEditInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput()) && (checkLengthInput()) && (checkWidthInput()) && (checkHeightInput()) && (checkWeightInput()) && (checkUPCInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkEditNameInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkHeightInput()
  {
    bool = false;
    try
    {
      this._height = Double.parseDouble(this.productHeight.getText().toString());
      if (this._height > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._height = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkLengthInput()
  {
    bool = false;
    try
    {
      this._length = Double.parseDouble(this.productLenght.getText().toString());
      if (this._length > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._length = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkProductDescInput()
  {
    this._productDesc = this.productDesc.getText().toString();
    return true;
  }
  
  boolean checkProductNameInput()
  {
    if (!this.productName.getText().toString().equals(""))
    {
      this._productName = this.productName.getText().toString();
      if (!this._productName.equals("")) {
        break label57;
      }
    }
    label57:
    for (boolean bool = false;; bool = true)
    {
      return bool;
      this._productName = "";
      break;
    }
  }
  
  boolean checkUPCInput()
  {
    if (this.productUPC.getText().toString() != "") {
      this._productUPC = this.productUPC.getText().toString();
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      this._productUPC = "";
    }
  }
  
  boolean checkWeightInput()
  {
    bool = false;
    try
    {
      this._weight = Double.parseDouble(this.productWeight.getText().toString());
      if (this._weight > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._weight = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkWidthInput()
  {
    bool = false;
    try
    {
      this._width = Double.parseDouble(this.productWidth.getText().toString());
      if (this._width > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._width = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  void hideKeyboard()
  {
    UtilManager.hideSoftKeyboard(getActivity());
  }
  
  @OnClick({2131624182})
  public void hideSoftKeyboardOnClick()
  {
    hideKeyboard();
  }
  
  @OnFocusChange({2131624182})
  public void hideSoftKeyboardOnFocus(boolean paramBoolean)
  {
    if (paramBoolean) {}
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnShipItemDetailListener))
    {
      this.mListener = ((OnShipItemDetailListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnShipItemDetailListener");
  }
  
  public void onClickTopBarLeftButton()
  {
    onReScanBarcodeClicked();
  }
  
  public void onClickTopBarRightButton()
  {
    if (!checkEditNameInputs()) {
      displayToast("Product Name can't be Empty");
    }
    for (;;)
    {
      return;
      if (!checkEditInputs())
      {
        displayToast("Dimensions and Weight can't be Zero");
      }
      else
      {
        PBSendProItem localPBSendProItem = DataManager.getInstance().getCurrentShipment().getItem();
        localPBSendProItem.setItemName(this._productName);
        localPBSendProItem.setItemLengthIn(this._length);
        localPBSendProItem.setItemWidthIn(this._width);
        localPBSendProItem.setItemHeightIn(this._height);
        localPBSendProItem.setItemWeightLb(this._weight);
        localPBSendProItem.setUpc(this._productUPC);
        onShipButtonClicked();
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mProductName = getArguments().getString("name");
      this.mProductDesc = getArguments().getString("description");
      this.mProductLenght = Double.valueOf(getArguments().getDouble("lenght"));
      this.mProductWidth = Double.valueOf(getArguments().getDouble("width"));
      this.mProductHeight = Double.valueOf(getArguments().getDouble("height"));
      this.mProductWeight = Double.valueOf(getArguments().getDouble("weight"));
      this.mProductUPC = getArguments().getString("upc");
      this.tag = getArguments().getString("tag");
      this.mImg_url = getArguments().getString("img_url");
      this.mUPCFound = getArguments().getBoolean("upcFound");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968648, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Item Details");
    setTopBarLeftButtonText("Rescan");
    setTopBarRightButtonText("Ship");
    this.productName = ((EditText)paramLayoutInflater.findViewById(2131624099));
    this.productDesc = ((EditText)paramLayoutInflater.findViewById(2131624100));
    this.productLenght = ((EditText)paramLayoutInflater.findViewById(2131624101));
    this.productWidth = ((EditText)paramLayoutInflater.findViewById(2131624102));
    this.productHeight = ((EditText)paramLayoutInflater.findViewById(2131624103));
    this.productWeight = ((EditText)paramLayoutInflater.findViewById(2131624104));
    this.productUPC = ((EditText)paramLayoutInflater.findViewById(2131624105));
    this.addItemToCatalog = ((Switch)paramLayoutInflater.findViewById(2131624220));
    if (this.mUPCFound)
    {
      paramViewGroup = DataManager.getInstance().getCurrentShipment().getItem();
      this.mProductName = paramViewGroup.getItemName();
      this.mProductDesc = "";
      this.mProductUPC = paramViewGroup.getUpc();
      this.mImg_url = paramViewGroup.getImageUrl();
      this.mProductLenght = Double.valueOf(paramViewGroup.getItemLengthIn());
      this.mProductWidth = Double.valueOf(paramViewGroup.getItemWidthIn());
      this.mProductHeight = Double.valueOf(paramViewGroup.getItemHeightIn());
      this.mProductWeight = Double.valueOf(paramViewGroup.getItemWeightLb());
      this.productName.setText(this.mProductName);
      if (this.mProductDesc != null) {
        this.productDesc.setText(this.mProductDesc);
      }
      this.productLenght.setText(String.format("%.01f", new Object[] { this.mProductLenght }));
      this.productWidth.setText(String.format("%.01f", new Object[] { this.mProductWidth }));
      this.productHeight.setText(String.format("%.01f", new Object[] { this.mProductHeight }));
      this.productWeight.setText(String.format("%.01f", new Object[] { this.mProductWeight }));
      this.productUPC.setText(this.mProductUPC);
    }
    for (;;)
    {
      if (!this.mUPCFound)
      {
        this.productName.setInputType(1);
        this.productDesc.setInputType(1);
        this.productLenght.setInputType(8194);
        this.productWidth.setInputType(8194);
        this.productHeight.setInputType(8194);
        this.productWeight.setInputType(8194);
        this.productUPC.setInputType(2);
      }
      hideKeyboard();
      return paramLayoutInflater;
      this.mProductName = "";
      this.mProductDesc = "";
      this.mImg_url = "";
      this.mProductLenght = Double.valueOf(0.0D);
      this.mProductWidth = Double.valueOf(0.0D);
      this.mProductHeight = Double.valueOf(0.0D);
      this.mProductWeight = Double.valueOf(0.0D);
      this.productUPC.setText(this.mProductUPC);
    }
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPause()
  {
    super.onPause();
    hideKeyboard();
  }
  
  public void onReScanBarcodeClicked()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onShipItemDetailInteraction("action_rescan_barcode", false);
    }
  }
  
  public void onResume()
  {
    super.onResume();
    hideKeyboard();
  }
  
  public void onShipButtonClicked()
  {
    hideKeyboard();
    if (this.mListener != null)
    {
      boolean bool = this.addItemToCatalog.isChecked();
      this.mListener.onShipItemDetailInteraction("action_ship", bool);
    }
  }
  
  public static abstract interface OnShipItemDetailListener
  {
    public abstract void onShipItemDetailInteraction(String paramString, boolean paramBoolean);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\ShipItemDetailFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */