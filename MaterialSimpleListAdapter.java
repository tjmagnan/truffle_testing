package com.afollestad.materialdialogs.simplelist;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.commons.R.layout;
import com.afollestad.materialdialogs.internal.MDAdapter;
import java.util.ArrayList;
import java.util.List;

public class MaterialSimpleListAdapter
  extends RecyclerView.Adapter<SimpleListVH>
  implements MDAdapter
{
  private Callback callback;
  private MaterialDialog dialog;
  private List<MaterialSimpleListItem> items = new ArrayList(4);
  
  public MaterialSimpleListAdapter(Callback paramCallback)
  {
    this.callback = paramCallback;
  }
  
  public void add(MaterialSimpleListItem paramMaterialSimpleListItem)
  {
    this.items.add(paramMaterialSimpleListItem);
    notifyItemInserted(this.items.size() - 1);
  }
  
  public void clear()
  {
    this.items.clear();
    notifyDataSetChanged();
  }
  
  public MaterialSimpleListItem getItem(int paramInt)
  {
    return (MaterialSimpleListItem)this.items.get(paramInt);
  }
  
  public int getItemCount()
  {
    return this.items.size();
  }
  
  public void onBindViewHolder(SimpleListVH paramSimpleListVH, int paramInt)
  {
    MaterialSimpleListItem localMaterialSimpleListItem;
    if (this.dialog != null)
    {
      localMaterialSimpleListItem = (MaterialSimpleListItem)this.items.get(paramInt);
      if (localMaterialSimpleListItem.getIcon() == null) {
        break label129;
      }
      paramSimpleListVH.icon.setImageDrawable(localMaterialSimpleListItem.getIcon());
      paramSimpleListVH.icon.setPadding(localMaterialSimpleListItem.getIconPadding(), localMaterialSimpleListItem.getIconPadding(), localMaterialSimpleListItem.getIconPadding(), localMaterialSimpleListItem.getIconPadding());
      paramSimpleListVH.icon.getBackground().setColorFilter(localMaterialSimpleListItem.getBackgroundColor(), PorterDuff.Mode.SRC_ATOP);
    }
    for (;;)
    {
      paramSimpleListVH.title.setTextColor(this.dialog.getBuilder().getItemColor());
      paramSimpleListVH.title.setText(localMaterialSimpleListItem.getContent());
      this.dialog.setTypeface(paramSimpleListVH.title, this.dialog.getBuilder().getRegularFont());
      return;
      label129:
      paramSimpleListVH.icon.setVisibility(8);
    }
  }
  
  public SimpleListVH onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new SimpleListVH(LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.md_simplelist_item, paramViewGroup, false), this);
  }
  
  public void setDialog(MaterialDialog paramMaterialDialog)
  {
    this.dialog = paramMaterialDialog;
  }
  
  public static abstract interface Callback
  {
    public abstract void onMaterialListItemSelected(MaterialDialog paramMaterialDialog, int paramInt, MaterialSimpleListItem paramMaterialSimpleListItem);
  }
  
  static class SimpleListVH
    extends RecyclerView.ViewHolder
    implements View.OnClickListener
  {
    final MaterialSimpleListAdapter adapter;
    final ImageView icon;
    final TextView title;
    
    SimpleListVH(View paramView, MaterialSimpleListAdapter paramMaterialSimpleListAdapter)
    {
      super();
      this.icon = ((ImageView)paramView.findViewById(16908294));
      this.title = ((TextView)paramView.findViewById(16908310));
      this.adapter = paramMaterialSimpleListAdapter;
      paramView.setOnClickListener(this);
    }
    
    public void onClick(View paramView)
    {
      if (this.adapter.callback != null) {
        this.adapter.callback.onMaterialListItemSelected(this.adapter.dialog, getAdapterPosition(), this.adapter.getItem(getAdapterPosition()));
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\simplelist\MaterialSimpleListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */