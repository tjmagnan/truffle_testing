package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;

@TargetApi(14)
@RequiresApi(14)
class ChangeBoundsPort
  extends TransitionPort
{
  private static final String LOG_TAG = "ChangeBounds";
  private static final String PROPNAME_BOUNDS = "android:changeBounds:bounds";
  private static final String PROPNAME_PARENT = "android:changeBounds:parent";
  private static final String PROPNAME_WINDOW_X = "android:changeBounds:windowX";
  private static final String PROPNAME_WINDOW_Y = "android:changeBounds:windowY";
  private static RectEvaluator sRectEvaluator = new RectEvaluator();
  private static final String[] sTransitionProperties = { "android:changeBounds:bounds", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY" };
  boolean mReparent = false;
  boolean mResizeClip = false;
  int[] tempLocation = new int[2];
  
  private void captureValues(TransitionValues paramTransitionValues)
  {
    View localView = paramTransitionValues.view;
    paramTransitionValues.values.put("android:changeBounds:bounds", new Rect(localView.getLeft(), localView.getTop(), localView.getRight(), localView.getBottom()));
    paramTransitionValues.values.put("android:changeBounds:parent", paramTransitionValues.view.getParent());
    paramTransitionValues.view.getLocationInWindow(this.tempLocation);
    paramTransitionValues.values.put("android:changeBounds:windowX", Integer.valueOf(this.tempLocation[0]));
    paramTransitionValues.values.put("android:changeBounds:windowY", Integer.valueOf(this.tempLocation[1]));
  }
  
  public void captureEndValues(TransitionValues paramTransitionValues)
  {
    captureValues(paramTransitionValues);
  }
  
  public void captureStartValues(TransitionValues paramTransitionValues)
  {
    captureValues(paramTransitionValues);
  }
  
  public Animator createAnimator(final ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, final TransitionValues paramTransitionValues2)
  {
    if ((paramTransitionValues1 == null) || (paramTransitionValues2 == null)) {}
    Object localObject2;
    ViewGroup localViewGroup;
    for (paramViewGroup = null;; paramViewGroup = null)
    {
      return paramViewGroup;
      localObject2 = paramTransitionValues1.values;
      localObject1 = paramTransitionValues2.values;
      localObject2 = (ViewGroup)((Map)localObject2).get("android:changeBounds:parent");
      localViewGroup = (ViewGroup)((Map)localObject1).get("android:changeBounds:parent");
      if ((localObject2 != null) && (localViewGroup != null)) {
        break;
      }
    }
    final Object localObject1 = paramTransitionValues2.view;
    int i;
    label96:
    int i10;
    int i9;
    int i7;
    int i8;
    int i6;
    int i5;
    int i3;
    int i4;
    int i2;
    int n;
    int i1;
    int m;
    int k;
    int j;
    if ((localObject2 == localViewGroup) || (((ViewGroup)localObject2).getId() == localViewGroup.getId()))
    {
      i = 1;
      if ((this.mReparent) && (i == 0)) {
        break label913;
      }
      paramViewGroup = (Rect)paramTransitionValues1.values.get("android:changeBounds:bounds");
      paramTransitionValues1 = (Rect)paramTransitionValues2.values.get("android:changeBounds:bounds");
      i10 = paramViewGroup.left;
      i9 = paramTransitionValues1.left;
      i7 = paramViewGroup.top;
      i8 = paramTransitionValues1.top;
      i6 = paramViewGroup.right;
      i5 = paramTransitionValues1.right;
      i3 = paramViewGroup.bottom;
      i4 = paramTransitionValues1.bottom;
      i2 = i6 - i10;
      n = i3 - i7;
      i1 = i5 - i9;
      m = i4 - i8;
      k = 0;
      j = 0;
      i = k;
      if (i2 != 0)
      {
        i = k;
        if (n != 0)
        {
          i = k;
          if (i1 != 0)
          {
            i = k;
            if (m != 0)
            {
              if (i10 != i9) {
                j = 0 + 1;
              }
              i = j;
              if (i7 != i8) {
                i = j + 1;
              }
              j = i;
              if (i6 != i5) {
                j = i + 1;
              }
              i = j;
              if (i3 != i4) {
                i = j + 1;
              }
            }
          }
        }
      }
      if (i <= 0) {
        break label1222;
      }
      if (this.mResizeClip) {
        break label586;
      }
      paramViewGroup = new PropertyValuesHolder[i];
      if (i10 != i9) {
        ((View)localObject1).setLeft(i10);
      }
      if (i7 != i8) {
        ((View)localObject1).setTop(i7);
      }
      if (i6 != i5) {
        ((View)localObject1).setRight(i6);
      }
      if (i3 != i4) {
        ((View)localObject1).setBottom(i3);
      }
      if (i10 == i9) {
        break label1239;
      }
      j = 0 + 1;
      paramViewGroup[0] = PropertyValuesHolder.ofInt("left", new int[] { i10, i9 });
    }
    for (;;)
    {
      i = j;
      if (i7 != i8)
      {
        paramViewGroup[j] = PropertyValuesHolder.ofInt("top", new int[] { i7, i8 });
        i = j + 1;
      }
      j = i;
      if (i6 != i5)
      {
        paramViewGroup[i] = PropertyValuesHolder.ofInt("right", new int[] { i6, i5 });
        j = i + 1;
      }
      if (i3 != i4) {
        paramViewGroup[j] = PropertyValuesHolder.ofInt("bottom", new int[] { i3, i4 });
      }
      for (;;)
      {
        paramTransitionValues1 = ObjectAnimator.ofPropertyValuesHolder(localObject1, paramViewGroup);
        paramViewGroup = paramTransitionValues1;
        if (!(((View)localObject1).getParent() instanceof ViewGroup)) {
          break;
        }
        paramViewGroup = (ViewGroup)((View)localObject1).getParent();
        addListener(new TransitionPort.TransitionListenerAdapter()
        {
          boolean mCanceled = false;
          
          public void onTransitionCancel(TransitionPort paramAnonymousTransitionPort)
          {
            this.mCanceled = true;
          }
          
          public void onTransitionEnd(TransitionPort paramAnonymousTransitionPort)
          {
            if (!this.mCanceled) {}
          }
          
          public void onTransitionPause(TransitionPort paramAnonymousTransitionPort) {}
          
          public void onTransitionResume(TransitionPort paramAnonymousTransitionPort) {}
        });
        paramViewGroup = paramTransitionValues1;
        break;
        i = 0;
        break label96;
        label586:
        if (i2 != i1) {
          ((View)localObject1).setRight(Math.max(i2, i1) + i9);
        }
        if (n != m) {
          ((View)localObject1).setBottom(Math.max(n, m) + i8);
        }
        if (i10 != i9) {
          ((View)localObject1).setTranslationX(i10 - i9);
        }
        if (i7 != i8) {
          ((View)localObject1).setTranslationY(i7 - i8);
        }
        float f2 = i9 - i10;
        float f1 = i8 - i7;
        k = i1 - i2;
        i3 = m - n;
        j = 0;
        if (f2 != 0.0F) {
          j = 0 + 1;
        }
        i = j;
        if (f1 != 0.0F) {
          i = j + 1;
        }
        if (k == 0)
        {
          j = i;
          if (i3 == 0) {}
        }
        else
        {
          j = i + 1;
        }
        paramViewGroup = new PropertyValuesHolder[j];
        if (f2 != 0.0F)
        {
          i = 0 + 1;
          paramViewGroup[0] = PropertyValuesHolder.ofFloat("translationX", new float[] { ((View)localObject1).getTranslationX(), 0.0F });
        }
        for (;;)
        {
          if (f1 != 0.0F) {
            paramViewGroup[i] = PropertyValuesHolder.ofFloat("translationY", new float[] { ((View)localObject1).getTranslationY(), 0.0F });
          }
          for (;;)
          {
            if ((k != 0) || (i3 != 0))
            {
              new Rect(0, 0, i2, n);
              new Rect(0, 0, i1, m);
            }
            paramViewGroup = ObjectAnimator.ofPropertyValuesHolder(localObject1, paramViewGroup);
            if ((((View)localObject1).getParent() instanceof ViewGroup))
            {
              paramTransitionValues1 = (ViewGroup)((View)localObject1).getParent();
              addListener(new TransitionPort.TransitionListenerAdapter()
              {
                boolean mCanceled = false;
                
                public void onTransitionCancel(TransitionPort paramAnonymousTransitionPort)
                {
                  this.mCanceled = true;
                }
                
                public void onTransitionEnd(TransitionPort paramAnonymousTransitionPort)
                {
                  if (!this.mCanceled) {}
                }
                
                public void onTransitionPause(TransitionPort paramAnonymousTransitionPort) {}
                
                public void onTransitionResume(TransitionPort paramAnonymousTransitionPort) {}
              });
            }
            paramViewGroup.addListener(new AnimatorListenerAdapter()
            {
              public void onAnimationEnd(Animator paramAnonymousAnimator) {}
            });
            break;
            label913:
            i = ((Integer)paramTransitionValues1.values.get("android:changeBounds:windowX")).intValue();
            m = ((Integer)paramTransitionValues1.values.get("android:changeBounds:windowY")).intValue();
            j = ((Integer)paramTransitionValues2.values.get("android:changeBounds:windowX")).intValue();
            k = ((Integer)paramTransitionValues2.values.get("android:changeBounds:windowY")).intValue();
            if ((i != j) || (m != k))
            {
              paramViewGroup.getLocationInWindow(this.tempLocation);
              paramTransitionValues1 = Bitmap.createBitmap(((View)localObject1).getWidth(), ((View)localObject1).getHeight(), Bitmap.Config.ARGB_8888);
              ((View)localObject1).draw(new Canvas(paramTransitionValues1));
              paramTransitionValues2 = new BitmapDrawable(paramTransitionValues1);
              ((View)localObject1).setVisibility(4);
              ViewOverlay.createFrom(paramViewGroup).add(paramTransitionValues2);
              localObject2 = new Rect(i - this.tempLocation[0], m - this.tempLocation[1], i - this.tempLocation[0] + ((View)localObject1).getWidth(), m - this.tempLocation[1] + ((View)localObject1).getHeight());
              paramTransitionValues1 = new Rect(j - this.tempLocation[0], k - this.tempLocation[1], j - this.tempLocation[0] + ((View)localObject1).getWidth(), k - this.tempLocation[1] + ((View)localObject1).getHeight());
              paramTransitionValues1 = ObjectAnimator.ofObject(paramTransitionValues2, "bounds", sRectEvaluator, new Object[] { localObject2, paramTransitionValues1 });
              paramTransitionValues1.addListener(new AnimatorListenerAdapter()
              {
                public void onAnimationEnd(Animator paramAnonymousAnimator)
                {
                  ViewOverlay.createFrom(paramViewGroup).remove(paramTransitionValues2);
                  localObject1.setVisibility(0);
                }
              });
              paramViewGroup = paramTransitionValues1;
              break;
            }
            label1222:
            paramViewGroup = null;
            break;
          }
          i = 0;
        }
      }
      label1239:
      j = 0;
    }
  }
  
  public String[] getTransitionProperties()
  {
    return sTransitionProperties;
  }
  
  public void setReparent(boolean paramBoolean)
  {
    this.mReparent = paramBoolean;
  }
  
  public void setResizeClip(boolean paramBoolean)
  {
    this.mResizeClip = paramBoolean;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\transition\ChangeBoundsPort.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */