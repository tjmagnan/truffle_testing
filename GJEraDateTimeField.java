package org.joda.time.chrono;

import java.util.Locale;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;
import org.joda.time.field.BaseDateTimeField;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.UnsupportedDurationField;

final class GJEraDateTimeField
  extends BaseDateTimeField
{
  private static final long serialVersionUID = 4240986525305515528L;
  private final BasicChronology iChronology;
  
  GJEraDateTimeField(BasicChronology paramBasicChronology)
  {
    super(DateTimeFieldType.era());
    this.iChronology = paramBasicChronology;
  }
  
  private Object readResolve()
  {
    return this.iChronology.era();
  }
  
  public int get(long paramLong)
  {
    if (this.iChronology.getYear(paramLong) <= 0) {}
    for (int i = 0;; i = 1) {
      return i;
    }
  }
  
  public String getAsText(int paramInt, Locale paramLocale)
  {
    return GJLocaleSymbols.forLocale(paramLocale).eraValueToText(paramInt);
  }
  
  public DurationField getDurationField()
  {
    return UnsupportedDurationField.getInstance(DurationFieldType.eras());
  }
  
  public int getMaximumTextLength(Locale paramLocale)
  {
    return GJLocaleSymbols.forLocale(paramLocale).getEraMaxTextLength();
  }
  
  public int getMaximumValue()
  {
    return 1;
  }
  
  public int getMinimumValue()
  {
    return 0;
  }
  
  public DurationField getRangeDurationField()
  {
    return null;
  }
  
  public boolean isLenient()
  {
    return false;
  }
  
  public long roundCeiling(long paramLong)
  {
    if (get(paramLong) == 0) {}
    for (paramLong = this.iChronology.setYear(0L, 1);; paramLong = Long.MAX_VALUE) {
      return paramLong;
    }
  }
  
  public long roundFloor(long paramLong)
  {
    if (get(paramLong) == 1) {}
    for (paramLong = this.iChronology.setYear(0L, 1);; paramLong = Long.MIN_VALUE) {
      return paramLong;
    }
  }
  
  public long roundHalfCeiling(long paramLong)
  {
    return roundFloor(paramLong);
  }
  
  public long roundHalfEven(long paramLong)
  {
    return roundFloor(paramLong);
  }
  
  public long roundHalfFloor(long paramLong)
  {
    return roundFloor(paramLong);
  }
  
  public long set(long paramLong, int paramInt)
  {
    FieldUtils.verifyValueBounds(this, paramInt, 0, 1);
    long l = paramLong;
    if (get(paramLong) != paramInt)
    {
      paramInt = this.iChronology.getYear(paramLong);
      l = this.iChronology.setYear(paramLong, -paramInt);
    }
    return l;
  }
  
  public long set(long paramLong, String paramString, Locale paramLocale)
  {
    return set(paramLong, GJLocaleSymbols.forLocale(paramLocale).eraTextToValue(paramString));
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\GJEraDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */