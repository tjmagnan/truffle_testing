package io.fabric.sdk.android;

public class InitializationException
  extends RuntimeException
{
  public InitializationException(String paramString)
  {
    super(paramString);
  }
  
  public InitializationException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\InitializationException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */