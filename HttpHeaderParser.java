package com.android.volley.toolbox;

import com.android.volley.Cache.Entry;
import com.android.volley.NetworkResponse;
import java.util.Date;
import java.util.Map;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;

public class HttpHeaderParser
{
  public static Cache.Entry parseCacheHeaders(NetworkResponse paramNetworkResponse)
  {
    long l10 = System.currentTimeMillis();
    Map localMap = paramNetworkResponse.headers;
    long l3 = 0L;
    long l6 = 0L;
    long l7 = 0L;
    long l8 = 0L;
    long l9 = 0L;
    l2 = 0L;
    l1 = 0L;
    int j = 0;
    int m = 0;
    int i = 0;
    Object localObject = (String)localMap.get("Date");
    if (localObject != null) {
      l3 = parseDateAsEpoch((String)localObject);
    }
    localObject = (String)localMap.get("Cache-Control");
    l5 = l2;
    l4 = l1;
    int k;
    String str;
    if (localObject != null)
    {
      int n = 1;
      localObject = ((String)localObject).split(",");
      k = 0;
      j = n;
      m = i;
      l5 = l2;
      l4 = l1;
      if (k < localObject.length)
      {
        str = localObject[k].trim();
        if ((str.equals("no-cache")) || (str.equals("no-store")))
        {
          paramNetworkResponse = null;
          label158:
          return paramNetworkResponse;
        }
        if (!str.startsWith("max-age=")) {}
      }
    }
    for (;;)
    {
      try
      {
        l5 = Long.parseLong(str.substring(8));
        l4 = l1;
        k++;
      }
      catch (Exception localException2)
      {
        l5 = l2;
        l4 = l1;
        continue;
      }
      l2 = l5;
      l1 = l4;
      break;
      if (str.startsWith("stale-while-revalidate=")) {}
      try
      {
        l4 = Long.parseLong(str.substring(23));
        l5 = l2;
      }
      catch (Exception localException1)
      {
        l5 = l2;
        l4 = l1;
      }
      if (!str.equals("must-revalidate"))
      {
        l5 = l2;
        l4 = l1;
        if (!str.equals("proxy-revalidate")) {}
      }
      else
      {
        i = 1;
        l5 = l2;
        l4 = l1;
        continue;
        localObject = (String)localMap.get("Expires");
        if (localObject != null) {
          l7 = parseDateAsEpoch((String)localObject);
        }
        localObject = (String)localMap.get("Last-Modified");
        if (localObject != null) {
          l6 = parseDateAsEpoch((String)localObject);
        }
        str = (String)localMap.get("ETag");
        if (j != 0)
        {
          l2 = l10 + 1000L * l5;
          if (m != 0)
          {
            l1 = l2;
            localObject = new Cache.Entry();
            ((Cache.Entry)localObject).data = paramNetworkResponse.data;
            ((Cache.Entry)localObject).etag = str;
            ((Cache.Entry)localObject).softTtl = l2;
            ((Cache.Entry)localObject).ttl = l1;
            ((Cache.Entry)localObject).serverDate = l3;
            ((Cache.Entry)localObject).lastModified = l6;
            ((Cache.Entry)localObject).responseHeaders = localMap;
            paramNetworkResponse = (NetworkResponse)localObject;
            break label158;
          }
          l1 = l2 + 1000L * l4;
          continue;
        }
        l1 = l9;
        l2 = l8;
        if (l3 > 0L)
        {
          l1 = l9;
          l2 = l8;
          if (l7 >= l3)
          {
            l2 = l10 + (l7 - l3);
            l1 = l2;
          }
        }
      }
    }
  }
  
  public static String parseCharset(Map<String, String> paramMap)
  {
    return parseCharset(paramMap, "ISO-8859-1");
  }
  
  public static String parseCharset(Map<String, String> paramMap, String paramString)
  {
    Object localObject = (String)paramMap.get("Content-Type");
    paramMap = paramString;
    if (localObject != null) {
      localObject = ((String)localObject).split(";");
    }
    for (int i = 1;; i++)
    {
      paramMap = paramString;
      if (i < localObject.length)
      {
        paramMap = localObject[i].trim().split("=");
        if ((paramMap.length == 2) && (paramMap[0].equals("charset"))) {
          paramMap = paramMap[1];
        }
      }
      else
      {
        return paramMap;
      }
    }
  }
  
  public static long parseDateAsEpoch(String paramString)
  {
    try
    {
      l = DateUtils.parseDate(paramString).getTime();
      return l;
    }
    catch (DateParseException paramString)
    {
      for (;;)
      {
        long l = 0L;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\toolbox\HttpHeaderParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */