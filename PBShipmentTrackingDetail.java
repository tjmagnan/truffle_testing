package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PBShipmentTrackingDetail
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 2968044433933447795L;
  @Expose
  @SerializedName("deliveryDate")
  private Date deliveryDate;
  @Expose
  @SerializedName("estimatedDeliveryDate")
  private Date estimatedDeliveryDate;
  @Expose
  @SerializedName("lastPackageStatusDate")
  private Date lastPackageStatusDate;
  @Expose
  @SerializedName("packageIdentifier")
  private String packageIdentifier;
  @Expose
  @SerializedName("packageStatus")
  private String packageStatus;
  @Expose
  @SerializedName("recipientAddress")
  private PBShipmentAddress recipientAddress;
  @Expose
  @SerializedName("senderAddress")
  private PBShipmentAddress senderAddress;
  @Expose
  @SerializedName("serviceCode")
  private String serviceCode;
  @Expose
  @SerializedName("serviceName")
  private String serviceName;
  @Expose
  @SerializedName("shipDate")
  private Date shipDate;
  @Expose
  @SerializedName("trackingDetailsList")
  private List<PBTrackingDetails> trackingDetails = new ArrayList();
  @Expose
  @SerializedName("trackingNumber")
  private String trackingNumber;
  
  public PBShipmentTrackingDetail() {}
  
  public PBShipmentTrackingDetail(Date paramDate1, Date paramDate2, Date paramDate3, String paramString1, String paramString2, PBShipmentAddress paramPBShipmentAddress1, PBShipmentAddress paramPBShipmentAddress2, String paramString3, String paramString4, Date paramDate4, List<PBTrackingDetails> paramList, String paramString5)
  {
    this.deliveryDate = paramDate1;
    this.estimatedDeliveryDate = paramDate2;
    this.lastPackageStatusDate = paramDate3;
    this.packageIdentifier = paramString1;
    this.packageStatus = paramString2;
    this.recipientAddress = paramPBShipmentAddress1;
    this.senderAddress = paramPBShipmentAddress2;
    this.serviceCode = paramString3;
    this.serviceName = paramString4;
    this.shipDate = paramDate4;
    this.trackingDetails = paramList;
    this.trackingNumber = paramString5;
  }
  
  public Date getDeliveryDate()
  {
    return this.deliveryDate;
  }
  
  public Date getEstimatedDeliveryDate()
  {
    return this.estimatedDeliveryDate;
  }
  
  public Date getLastPackageStatusDate()
  {
    return this.lastPackageStatusDate;
  }
  
  public String getPackageIdentifier()
  {
    return this.packageIdentifier;
  }
  
  public String getPackageStatus()
  {
    return this.packageStatus;
  }
  
  public PBShipmentAddress getRecipientAddress()
  {
    return this.recipientAddress;
  }
  
  public PBShipmentAddress getSenderAddress()
  {
    return this.senderAddress;
  }
  
  public String getServiceCode()
  {
    return this.serviceCode;
  }
  
  public String getServiceName()
  {
    return this.serviceName;
  }
  
  public Date getShipDate()
  {
    return this.shipDate;
  }
  
  public List<PBTrackingDetails> getTrackingDetails()
  {
    return this.trackingDetails;
  }
  
  public String getTrackingNumber()
  {
    return this.trackingNumber;
  }
  
  public void setDeliveryDate(Date paramDate)
  {
    this.deliveryDate = paramDate;
  }
  
  public void setEstimatedDeliveryDate(Date paramDate)
  {
    this.estimatedDeliveryDate = paramDate;
  }
  
  public void setLastPackageStatusDate(Date paramDate)
  {
    this.lastPackageStatusDate = paramDate;
  }
  
  public void setPackageIdentifier(String paramString)
  {
    this.packageIdentifier = paramString;
  }
  
  public void setPackageStatus(String paramString)
  {
    this.packageStatus = paramString;
  }
  
  public void setRecipientAddress(PBShipmentAddress paramPBShipmentAddress)
  {
    this.recipientAddress = paramPBShipmentAddress;
  }
  
  public void setSenderAddress(PBShipmentAddress paramPBShipmentAddress)
  {
    this.senderAddress = paramPBShipmentAddress;
  }
  
  public void setServiceCode(String paramString)
  {
    this.serviceCode = paramString;
  }
  
  public void setServiceName(String paramString)
  {
    this.serviceName = paramString;
  }
  
  public void setShipDate(Date paramDate)
  {
    this.shipDate = paramDate;
  }
  
  public void setTrackingDetails(List<PBTrackingDetails> paramList)
  {
    this.trackingDetails = paramList;
  }
  
  public void setTrackingNumber(String paramString)
  {
    this.trackingNumber = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentTrackingDetail.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */