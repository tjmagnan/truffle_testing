package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.Cache;
import com.android.volley.Cache.Entry;
import com.android.volley.VolleyLog;
import java.io.EOFException;
import java.io.File;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DiskBasedCache
  implements Cache
{
  private static final int CACHE_MAGIC = 538247942;
  private static final int DEFAULT_DISK_USAGE_BYTES = 5242880;
  private static final float HYSTERESIS_FACTOR = 0.9F;
  private final Map<String, CacheHeader> mEntries = new LinkedHashMap(16, 0.75F, true);
  private final int mMaxCacheSizeInBytes;
  private final File mRootDirectory;
  private long mTotalSize = 0L;
  
  public DiskBasedCache(File paramFile)
  {
    this(paramFile, 5242880);
  }
  
  public DiskBasedCache(File paramFile, int paramInt)
  {
    this.mRootDirectory = paramFile;
    this.mMaxCacheSizeInBytes = paramInt;
  }
  
  private String getFilenameForKey(String paramString)
  {
    int i = paramString.length() / 2;
    String str = String.valueOf(String.valueOf(paramString.substring(0, i).hashCode()));
    paramString = String.valueOf(String.valueOf(paramString.substring(i).hashCode()));
    if (paramString.length() != 0) {}
    for (paramString = str.concat(paramString);; paramString = new String(str)) {
      return paramString;
    }
  }
  
  private void pruneIfNeeded(int paramInt)
  {
    if (this.mTotalSize + paramInt < this.mMaxCacheSizeInBytes) {
      return;
    }
    if (VolleyLog.DEBUG) {
      VolleyLog.v("Pruning old cache entries.", new Object[0]);
    }
    long l1 = this.mTotalSize;
    int i = 0;
    long l2 = SystemClock.elapsedRealtime();
    Iterator localIterator = this.mEntries.entrySet().iterator();
    label61:
    int j = i;
    CacheHeader localCacheHeader;
    if (localIterator.hasNext())
    {
      localCacheHeader = (CacheHeader)((Map.Entry)localIterator.next()).getValue();
      if (!getFileForKey(localCacheHeader.key).delete()) {
        break label205;
      }
      this.mTotalSize -= localCacheHeader.size;
    }
    for (;;)
    {
      localIterator.remove();
      j = i + 1;
      i = j;
      if ((float)(this.mTotalSize + paramInt) >= this.mMaxCacheSizeInBytes * 0.9F) {
        break label61;
      }
      if (!VolleyLog.DEBUG) {
        break;
      }
      VolleyLog.v("pruned %d files, %d bytes, %d ms", new Object[] { Integer.valueOf(j), Long.valueOf(this.mTotalSize - l1), Long.valueOf(SystemClock.elapsedRealtime() - l2) });
      break;
      label205:
      VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", new Object[] { localCacheHeader.key, getFilenameForKey(localCacheHeader.key) });
    }
  }
  
  private void putEntry(String paramString, CacheHeader paramCacheHeader)
  {
    if (!this.mEntries.containsKey(paramString)) {}
    CacheHeader localCacheHeader;
    for (this.mTotalSize += paramCacheHeader.size;; this.mTotalSize += paramCacheHeader.size - localCacheHeader.size)
    {
      this.mEntries.put(paramString, paramCacheHeader);
      return;
      localCacheHeader = (CacheHeader)this.mEntries.get(paramString);
    }
  }
  
  private static int read(InputStream paramInputStream)
    throws IOException
  {
    int i = paramInputStream.read();
    if (i == -1) {
      throw new EOFException();
    }
    return i;
  }
  
  static int readInt(InputStream paramInputStream)
    throws IOException
  {
    return 0x0 | read(paramInputStream) << 0 | read(paramInputStream) << 8 | read(paramInputStream) << 16 | read(paramInputStream) << 24;
  }
  
  static long readLong(InputStream paramInputStream)
    throws IOException
  {
    return 0L | (read(paramInputStream) & 0xFF) << 0 | (read(paramInputStream) & 0xFF) << 8 | (read(paramInputStream) & 0xFF) << 16 | (read(paramInputStream) & 0xFF) << 24 | (read(paramInputStream) & 0xFF) << 32 | (read(paramInputStream) & 0xFF) << 40 | (read(paramInputStream) & 0xFF) << 48 | (read(paramInputStream) & 0xFF) << 56;
  }
  
  static String readString(InputStream paramInputStream)
    throws IOException
  {
    return new String(streamToBytes(paramInputStream, (int)readLong(paramInputStream)), "UTF-8");
  }
  
  static Map<String, String> readStringStringMap(InputStream paramInputStream)
    throws IOException
  {
    int j = readInt(paramInputStream);
    if (j == 0) {}
    for (Object localObject = Collections.emptyMap();; localObject = new HashMap(j)) {
      for (int i = 0; i < j; i++) {
        ((Map)localObject).put(readString(paramInputStream).intern(), readString(paramInputStream).intern());
      }
    }
    return (Map<String, String>)localObject;
  }
  
  private void removeEntry(String paramString)
  {
    CacheHeader localCacheHeader = (CacheHeader)this.mEntries.get(paramString);
    if (localCacheHeader != null)
    {
      this.mTotalSize -= localCacheHeader.size;
      this.mEntries.remove(paramString);
    }
  }
  
  private static byte[] streamToBytes(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    while (i < paramInt)
    {
      int j = paramInputStream.read(arrayOfByte, i, paramInt - i);
      if (j == -1) {
        break;
      }
      i += j;
    }
    if (i != paramInt) {
      throw new IOException(50 + "Expected " + paramInt + " bytes, read " + i + " bytes");
    }
    return arrayOfByte;
  }
  
  static void writeInt(OutputStream paramOutputStream, int paramInt)
    throws IOException
  {
    paramOutputStream.write(paramInt >> 0 & 0xFF);
    paramOutputStream.write(paramInt >> 8 & 0xFF);
    paramOutputStream.write(paramInt >> 16 & 0xFF);
    paramOutputStream.write(paramInt >> 24 & 0xFF);
  }
  
  static void writeLong(OutputStream paramOutputStream, long paramLong)
    throws IOException
  {
    paramOutputStream.write((byte)(int)(paramLong >>> 0));
    paramOutputStream.write((byte)(int)(paramLong >>> 8));
    paramOutputStream.write((byte)(int)(paramLong >>> 16));
    paramOutputStream.write((byte)(int)(paramLong >>> 24));
    paramOutputStream.write((byte)(int)(paramLong >>> 32));
    paramOutputStream.write((byte)(int)(paramLong >>> 40));
    paramOutputStream.write((byte)(int)(paramLong >>> 48));
    paramOutputStream.write((byte)(int)(paramLong >>> 56));
  }
  
  static void writeString(OutputStream paramOutputStream, String paramString)
    throws IOException
  {
    paramString = paramString.getBytes("UTF-8");
    writeLong(paramOutputStream, paramString.length);
    paramOutputStream.write(paramString, 0, paramString.length);
  }
  
  static void writeStringStringMap(Map<String, String> paramMap, OutputStream paramOutputStream)
    throws IOException
  {
    if (paramMap != null)
    {
      writeInt(paramOutputStream, paramMap.size());
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)paramMap.next();
        writeString(paramOutputStream, (String)localEntry.getKey());
        writeString(paramOutputStream, (String)localEntry.getValue());
      }
    }
    writeInt(paramOutputStream, 0);
  }
  
  public void clear()
  {
    try
    {
      File[] arrayOfFile = this.mRootDirectory.listFiles();
      if (arrayOfFile != null)
      {
        int j = arrayOfFile.length;
        for (int i = 0; i < j; i++) {
          arrayOfFile[i].delete();
        }
      }
      this.mEntries.clear();
      this.mTotalSize = 0L;
      VolleyLog.d("Cache cleared.", new Object[0]);
      return;
    }
    finally {}
  }
  
  /* Error */
  public Cache.Entry get(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aload_0
    //   4: monitorenter
    //   5: aload_0
    //   6: getfield 47	com/android/volley/toolbox/DiskBasedCache:mEntries	Ljava/util/Map;
    //   9: aload_1
    //   10: invokeinterface 182 2 0
    //   15: checkcast 10	com/android/volley/toolbox/DiskBasedCache$CacheHeader
    //   18: astore 9
    //   20: aload 9
    //   22: ifnonnull +10 -> 32
    //   25: aload 5
    //   27: astore_1
    //   28: aload_0
    //   29: monitorexit
    //   30: aload_1
    //   31: areturn
    //   32: aload_0
    //   33: aload_1
    //   34: invokevirtual 138	com/android/volley/toolbox/DiskBasedCache:getFileForKey	(Ljava/lang/String;)Ljava/io/File;
    //   37: astore 7
    //   39: aconst_null
    //   40: astore 4
    //   42: aconst_null
    //   43: astore 6
    //   45: aload 4
    //   47: astore_2
    //   48: new 13	com/android/volley/toolbox/DiskBasedCache$CountingInputStream
    //   51: astore_3
    //   52: aload 4
    //   54: astore_2
    //   55: new 306	java/io/BufferedInputStream
    //   58: astore 8
    //   60: aload 4
    //   62: astore_2
    //   63: new 308	java/io/FileInputStream
    //   66: astore 10
    //   68: aload 4
    //   70: astore_2
    //   71: aload 10
    //   73: aload 7
    //   75: invokespecial 310	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   78: aload 4
    //   80: astore_2
    //   81: aload 8
    //   83: aload 10
    //   85: invokespecial 313	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   88: aload 4
    //   90: astore_2
    //   91: aload_3
    //   92: aload 8
    //   94: aconst_null
    //   95: invokespecial 316	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:<init>	(Ljava/io/InputStream;Lcom/android/volley/toolbox/DiskBasedCache$1;)V
    //   98: aload_3
    //   99: invokestatic 320	com/android/volley/toolbox/DiskBasedCache$CacheHeader:readHeader	(Ljava/io/InputStream;)Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;
    //   102: pop
    //   103: aload 9
    //   105: aload_3
    //   106: aload 7
    //   108: invokevirtual 322	java/io/File:length	()J
    //   111: aload_3
    //   112: invokestatic 326	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:access$100	(Lcom/android/volley/toolbox/DiskBasedCache$CountingInputStream;)I
    //   115: i2l
    //   116: lsub
    //   117: l2i
    //   118: invokestatic 209	com/android/volley/toolbox/DiskBasedCache:streamToBytes	(Ljava/io/InputStream;I)[B
    //   121: invokevirtual 330	com/android/volley/toolbox/DiskBasedCache$CacheHeader:toCacheEntry	([B)Lcom/android/volley/Cache$Entry;
    //   124: astore_2
    //   125: aload_3
    //   126: ifnull +7 -> 133
    //   129: aload_3
    //   130: invokevirtual 333	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   133: aload_2
    //   134: astore_1
    //   135: goto -107 -> 28
    //   138: astore_1
    //   139: aload 5
    //   141: astore_1
    //   142: goto -114 -> 28
    //   145: astore 4
    //   147: aload 6
    //   149: astore_3
    //   150: aload_3
    //   151: astore_2
    //   152: ldc_w 335
    //   155: iconst_2
    //   156: anewarray 4	java/lang/Object
    //   159: dup
    //   160: iconst_0
    //   161: aload 7
    //   163: invokevirtual 338	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   166: aastore
    //   167: dup
    //   168: iconst_1
    //   169: aload 4
    //   171: invokevirtual 339	java/io/IOException:toString	()Ljava/lang/String;
    //   174: aastore
    //   175: invokestatic 168	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   178: aload_3
    //   179: astore_2
    //   180: aload_0
    //   181: aload_1
    //   182: invokevirtual 341	com/android/volley/toolbox/DiskBasedCache:remove	(Ljava/lang/String;)V
    //   185: aload 5
    //   187: astore_1
    //   188: aload_3
    //   189: ifnull -161 -> 28
    //   192: aload_3
    //   193: invokevirtual 333	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   196: aload 5
    //   198: astore_1
    //   199: goto -171 -> 28
    //   202: astore_1
    //   203: aload 5
    //   205: astore_1
    //   206: goto -178 -> 28
    //   209: astore_1
    //   210: aload_2
    //   211: ifnull +7 -> 218
    //   214: aload_2
    //   215: invokevirtual 333	com/android/volley/toolbox/DiskBasedCache$CountingInputStream:close	()V
    //   218: aload_1
    //   219: athrow
    //   220: astore_1
    //   221: aload_0
    //   222: monitorexit
    //   223: aload_1
    //   224: athrow
    //   225: astore_1
    //   226: aload 5
    //   228: astore_1
    //   229: goto -201 -> 28
    //   232: astore_1
    //   233: aload_3
    //   234: astore_2
    //   235: goto -25 -> 210
    //   238: astore_2
    //   239: aload_2
    //   240: astore 4
    //   242: goto -92 -> 150
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	245	0	this	DiskBasedCache
    //   0	245	1	paramString	String
    //   47	188	2	localObject1	Object
    //   238	2	2	localIOException1	IOException
    //   51	183	3	localObject2	Object
    //   40	49	4	localObject3	Object
    //   145	25	4	localIOException2	IOException
    //   240	1	4	localObject4	Object
    //   1	226	5	localObject5	Object
    //   43	105	6	localObject6	Object
    //   37	125	7	localFile	File
    //   58	35	8	localBufferedInputStream	java.io.BufferedInputStream
    //   18	86	9	localCacheHeader	CacheHeader
    //   66	18	10	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   129	133	138	java/io/IOException
    //   48	52	145	java/io/IOException
    //   55	60	145	java/io/IOException
    //   63	68	145	java/io/IOException
    //   71	78	145	java/io/IOException
    //   81	88	145	java/io/IOException
    //   91	98	145	java/io/IOException
    //   192	196	202	java/io/IOException
    //   48	52	209	finally
    //   55	60	209	finally
    //   63	68	209	finally
    //   71	78	209	finally
    //   81	88	209	finally
    //   91	98	209	finally
    //   152	178	209	finally
    //   180	185	209	finally
    //   5	20	220	finally
    //   32	39	220	finally
    //   129	133	220	finally
    //   192	196	220	finally
    //   214	218	220	finally
    //   218	220	220	finally
    //   214	218	225	java/io/IOException
    //   98	125	232	finally
    //   98	125	238	java/io/IOException
  }
  
  public File getFileForKey(String paramString)
  {
    return new File(this.mRootDirectory, getFilenameForKey(paramString));
  }
  
  /* Error */
  public void initialize()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   6: invokevirtual 348	java/io/File:exists	()Z
    //   9: ifne +36 -> 45
    //   12: aload_0
    //   13: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   16: invokevirtual 351	java/io/File:mkdirs	()Z
    //   19: ifne +23 -> 42
    //   22: ldc_w 353
    //   25: iconst_1
    //   26: anewarray 4	java/lang/Object
    //   29: dup
    //   30: iconst_0
    //   31: aload_0
    //   32: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   35: invokevirtual 338	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   38: aastore
    //   39: invokestatic 356	com/android/volley/VolleyLog:e	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   42: aload_0
    //   43: monitorexit
    //   44: return
    //   45: aload_0
    //   46: getfield 51	com/android/volley/toolbox/DiskBasedCache:mRootDirectory	Ljava/io/File;
    //   49: invokevirtual 299	java/io/File:listFiles	()[Ljava/io/File;
    //   52: astore 7
    //   54: aload 7
    //   56: ifnull -14 -> 42
    //   59: aload 7
    //   61: arraylength
    //   62: istore_2
    //   63: iconst_0
    //   64: istore_1
    //   65: iload_1
    //   66: iload_2
    //   67: if_icmpge -25 -> 42
    //   70: aload 7
    //   72: iload_1
    //   73: aaload
    //   74: astore 8
    //   76: aconst_null
    //   77: astore 6
    //   79: aconst_null
    //   80: astore 5
    //   82: aload 6
    //   84: astore_3
    //   85: new 306	java/io/BufferedInputStream
    //   88: astore 4
    //   90: aload 6
    //   92: astore_3
    //   93: new 308	java/io/FileInputStream
    //   96: astore 9
    //   98: aload 6
    //   100: astore_3
    //   101: aload 9
    //   103: aload 8
    //   105: invokespecial 310	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   108: aload 6
    //   110: astore_3
    //   111: aload 4
    //   113: aload 9
    //   115: invokespecial 313	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   118: aload 4
    //   120: invokestatic 320	com/android/volley/toolbox/DiskBasedCache$CacheHeader:readHeader	(Ljava/io/InputStream;)Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;
    //   123: astore_3
    //   124: aload_3
    //   125: aload 8
    //   127: invokevirtual 322	java/io/File:length	()J
    //   130: putfield 146	com/android/volley/toolbox/DiskBasedCache$CacheHeader:size	J
    //   133: aload_0
    //   134: aload_3
    //   135: getfield 134	com/android/volley/toolbox/DiskBasedCache$CacheHeader:key	Ljava/lang/String;
    //   138: aload_3
    //   139: invokespecial 358	com/android/volley/toolbox/DiskBasedCache:putEntry	(Ljava/lang/String;Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;)V
    //   142: aload 4
    //   144: ifnull +8 -> 152
    //   147: aload 4
    //   149: invokevirtual 359	java/io/BufferedInputStream:close	()V
    //   152: iinc 1 1
    //   155: goto -90 -> 65
    //   158: astore_3
    //   159: goto -7 -> 152
    //   162: astore_3
    //   163: aload 5
    //   165: astore 4
    //   167: aload 8
    //   169: ifnull +12 -> 181
    //   172: aload 4
    //   174: astore_3
    //   175: aload 8
    //   177: invokevirtual 143	java/io/File:delete	()Z
    //   180: pop
    //   181: aload 4
    //   183: ifnull -31 -> 152
    //   186: aload 4
    //   188: invokevirtual 359	java/io/BufferedInputStream:close	()V
    //   191: goto -39 -> 152
    //   194: astore_3
    //   195: goto -43 -> 152
    //   198: astore 4
    //   200: aload_3
    //   201: astore 5
    //   203: aload 5
    //   205: ifnull +8 -> 213
    //   208: aload 5
    //   210: invokevirtual 359	java/io/BufferedInputStream:close	()V
    //   213: aload 4
    //   215: athrow
    //   216: astore_3
    //   217: aload_0
    //   218: monitorexit
    //   219: aload_3
    //   220: athrow
    //   221: astore_3
    //   222: goto -9 -> 213
    //   225: astore_3
    //   226: aload 4
    //   228: astore 5
    //   230: aload_3
    //   231: astore 4
    //   233: goto -30 -> 203
    //   236: astore_3
    //   237: goto -70 -> 167
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	240	0	this	DiskBasedCache
    //   64	89	1	i	int
    //   62	6	2	j	int
    //   84	55	3	localObject1	Object
    //   158	1	3	localIOException1	IOException
    //   162	1	3	localIOException2	IOException
    //   174	1	3	localObject2	Object
    //   194	7	3	localIOException3	IOException
    //   216	4	3	localObject3	Object
    //   221	1	3	localIOException4	IOException
    //   225	6	3	localObject4	Object
    //   236	1	3	localIOException5	IOException
    //   88	99	4	localObject5	Object
    //   198	29	4	localObject6	Object
    //   231	1	4	localObject7	Object
    //   80	149	5	localObject8	Object
    //   77	32	6	localObject9	Object
    //   52	19	7	arrayOfFile	File[]
    //   74	102	8	localFile	File
    //   96	18	9	localFileInputStream	java.io.FileInputStream
    // Exception table:
    //   from	to	target	type
    //   147	152	158	java/io/IOException
    //   85	90	162	java/io/IOException
    //   93	98	162	java/io/IOException
    //   101	108	162	java/io/IOException
    //   111	118	162	java/io/IOException
    //   186	191	194	java/io/IOException
    //   85	90	198	finally
    //   93	98	198	finally
    //   101	108	198	finally
    //   111	118	198	finally
    //   175	181	198	finally
    //   2	42	216	finally
    //   45	54	216	finally
    //   59	63	216	finally
    //   147	152	216	finally
    //   186	191	216	finally
    //   208	213	216	finally
    //   213	216	216	finally
    //   208	213	221	java/io/IOException
    //   118	142	225	finally
    //   118	142	236	java/io/IOException
  }
  
  public void invalidate(String paramString, boolean paramBoolean)
  {
    try
    {
      Cache.Entry localEntry = get(paramString);
      if (localEntry != null)
      {
        localEntry.softTtl = 0L;
        if (paramBoolean) {
          localEntry.ttl = 0L;
        }
        put(paramString, localEntry);
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public void put(String paramString, Cache.Entry paramEntry)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: getfield 378	com/android/volley/Cache$Entry:data	[B
    //   7: arraylength
    //   8: invokespecial 380	com/android/volley/toolbox/DiskBasedCache:pruneIfNeeded	(I)V
    //   11: aload_0
    //   12: aload_1
    //   13: invokevirtual 138	com/android/volley/toolbox/DiskBasedCache:getFileForKey	(Ljava/lang/String;)Ljava/io/File;
    //   16: astore_3
    //   17: new 382	java/io/BufferedOutputStream
    //   20: astore 4
    //   22: new 384	java/io/FileOutputStream
    //   25: astore 5
    //   27: aload 5
    //   29: aload_3
    //   30: invokespecial 385	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   33: aload 4
    //   35: aload 5
    //   37: invokespecial 388	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   40: new 10	com/android/volley/toolbox/DiskBasedCache$CacheHeader
    //   43: astore 5
    //   45: aload 5
    //   47: aload_1
    //   48: aload_2
    //   49: invokespecial 390	com/android/volley/toolbox/DiskBasedCache$CacheHeader:<init>	(Ljava/lang/String;Lcom/android/volley/Cache$Entry;)V
    //   52: aload 5
    //   54: aload 4
    //   56: invokevirtual 394	com/android/volley/toolbox/DiskBasedCache$CacheHeader:writeHeader	(Ljava/io/OutputStream;)Z
    //   59: ifne +63 -> 122
    //   62: aload 4
    //   64: invokevirtual 395	java/io/BufferedOutputStream:close	()V
    //   67: ldc_w 397
    //   70: iconst_1
    //   71: anewarray 4	java/lang/Object
    //   74: dup
    //   75: iconst_0
    //   76: aload_3
    //   77: invokevirtual 338	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   80: aastore
    //   81: invokestatic 168	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   84: new 186	java/io/IOException
    //   87: astore_1
    //   88: aload_1
    //   89: invokespecial 398	java/io/IOException:<init>	()V
    //   92: aload_1
    //   93: athrow
    //   94: astore_1
    //   95: aload_3
    //   96: invokevirtual 143	java/io/File:delete	()Z
    //   99: ifne +20 -> 119
    //   102: ldc_w 400
    //   105: iconst_1
    //   106: anewarray 4	java/lang/Object
    //   109: dup
    //   110: iconst_0
    //   111: aload_3
    //   112: invokevirtual 338	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   115: aastore
    //   116: invokestatic 168	com/android/volley/VolleyLog:d	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   119: aload_0
    //   120: monitorexit
    //   121: return
    //   122: aload 4
    //   124: aload_2
    //   125: getfield 378	com/android/volley/Cache$Entry:data	[B
    //   128: invokevirtual 403	java/io/BufferedOutputStream:write	([B)V
    //   131: aload 4
    //   133: invokevirtual 395	java/io/BufferedOutputStream:close	()V
    //   136: aload_0
    //   137: aload_1
    //   138: aload 5
    //   140: invokespecial 358	com/android/volley/toolbox/DiskBasedCache:putEntry	(Ljava/lang/String;Lcom/android/volley/toolbox/DiskBasedCache$CacheHeader;)V
    //   143: goto -24 -> 119
    //   146: astore_1
    //   147: aload_0
    //   148: monitorexit
    //   149: aload_1
    //   150: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	DiskBasedCache
    //   0	151	1	paramString	String
    //   0	151	2	paramEntry	Cache.Entry
    //   16	96	3	localFile	File
    //   20	112	4	localBufferedOutputStream	java.io.BufferedOutputStream
    //   25	114	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   17	94	94	java/io/IOException
    //   122	143	94	java/io/IOException
    //   2	17	146	finally
    //   17	94	146	finally
    //   95	119	146	finally
    //   122	143	146	finally
  }
  
  public void remove(String paramString)
  {
    try
    {
      boolean bool = getFileForKey(paramString).delete();
      removeEntry(paramString);
      if (!bool) {
        VolleyLog.d("Could not delete cache entry for key=%s, filename=%s", new Object[] { paramString, getFilenameForKey(paramString) });
      }
      return;
    }
    finally {}
  }
  
  static class CacheHeader
  {
    public String etag;
    public String key;
    public long lastModified;
    public Map<String, String> responseHeaders;
    public long serverDate;
    public long size;
    public long softTtl;
    public long ttl;
    
    private CacheHeader() {}
    
    public CacheHeader(String paramString, Cache.Entry paramEntry)
    {
      this.key = paramString;
      this.size = paramEntry.data.length;
      this.etag = paramEntry.etag;
      this.serverDate = paramEntry.serverDate;
      this.lastModified = paramEntry.lastModified;
      this.ttl = paramEntry.ttl;
      this.softTtl = paramEntry.softTtl;
      this.responseHeaders = paramEntry.responseHeaders;
    }
    
    public static CacheHeader readHeader(InputStream paramInputStream)
      throws IOException
    {
      CacheHeader localCacheHeader = new CacheHeader();
      if (DiskBasedCache.readInt(paramInputStream) != 538247942) {
        throw new IOException();
      }
      localCacheHeader.key = DiskBasedCache.readString(paramInputStream);
      localCacheHeader.etag = DiskBasedCache.readString(paramInputStream);
      if (localCacheHeader.etag.equals("")) {
        localCacheHeader.etag = null;
      }
      localCacheHeader.serverDate = DiskBasedCache.readLong(paramInputStream);
      localCacheHeader.lastModified = DiskBasedCache.readLong(paramInputStream);
      localCacheHeader.ttl = DiskBasedCache.readLong(paramInputStream);
      localCacheHeader.softTtl = DiskBasedCache.readLong(paramInputStream);
      localCacheHeader.responseHeaders = DiskBasedCache.readStringStringMap(paramInputStream);
      return localCacheHeader;
    }
    
    public Cache.Entry toCacheEntry(byte[] paramArrayOfByte)
    {
      Cache.Entry localEntry = new Cache.Entry();
      localEntry.data = paramArrayOfByte;
      localEntry.etag = this.etag;
      localEntry.serverDate = this.serverDate;
      localEntry.lastModified = this.lastModified;
      localEntry.ttl = this.ttl;
      localEntry.softTtl = this.softTtl;
      localEntry.responseHeaders = this.responseHeaders;
      return localEntry;
    }
    
    public boolean writeHeader(OutputStream paramOutputStream)
    {
      bool = true;
      for (;;)
      {
        try
        {
          DiskBasedCache.writeInt(paramOutputStream, 538247942);
          DiskBasedCache.writeString(paramOutputStream, this.key);
          if (this.etag != null) {
            continue;
          }
          str = "";
          DiskBasedCache.writeString(paramOutputStream, str);
          DiskBasedCache.writeLong(paramOutputStream, this.serverDate);
          DiskBasedCache.writeLong(paramOutputStream, this.lastModified);
          DiskBasedCache.writeLong(paramOutputStream, this.ttl);
          DiskBasedCache.writeLong(paramOutputStream, this.softTtl);
          DiskBasedCache.writeStringStringMap(this.responseHeaders, paramOutputStream);
          paramOutputStream.flush();
        }
        catch (IOException paramOutputStream)
        {
          String str;
          VolleyLog.d("%s", new Object[] { paramOutputStream.toString() });
          bool = false;
          continue;
        }
        return bool;
        str = this.etag;
      }
    }
  }
  
  private static class CountingInputStream
    extends FilterInputStream
  {
    private int bytesRead = 0;
    
    private CountingInputStream(InputStream paramInputStream)
    {
      super();
    }
    
    public int read()
      throws IOException
    {
      int i = super.read();
      if (i != -1) {
        this.bytesRead += 1;
      }
      return i;
    }
    
    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      paramInt1 = super.read(paramArrayOfByte, paramInt1, paramInt2);
      if (paramInt1 != -1) {
        this.bytesRead += paramInt1;
      }
      return paramInt1;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\toolbox\DiskBasedCache.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */