package io.fabric.sdk.android.services.common;

public abstract interface AdvertisingInfoStrategy
{
  public abstract AdvertisingInfo getAdvertisingInfo();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\common\AdvertisingInfoStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */