package org.jsoup.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class HtmlTreeBuilder
  extends TreeBuilder
{
  private static final String[] TagSearchButton;
  private static final String[] TagSearchEndTags;
  private static final String[] TagSearchList;
  private static final String[] TagSearchSelectScope;
  private static final String[] TagSearchSpecial;
  private static final String[] TagSearchTableScope;
  public static final String[] TagsSearchInScope;
  private boolean baseUriSetFromDoc = false;
  private Element contextElement;
  private Token.EndTag emptyEnd = new Token.EndTag();
  private FormElement formElement;
  private ArrayList<Element> formattingElements = new ArrayList();
  private boolean fosterInserts = false;
  private boolean fragmentParsing = false;
  private boolean framesetOk = true;
  private Element headElement;
  private HtmlTreeBuilderState originalState;
  private List<String> pendingTableCharacters = new ArrayList();
  private String[] specificScopeTarget = { null };
  private HtmlTreeBuilderState state;
  
  static
  {
    if (!HtmlTreeBuilder.class.desiredAssertionStatus()) {}
    for (boolean bool = true;; bool = false)
    {
      $assertionsDisabled = bool;
      TagsSearchInScope = new String[] { "applet", "caption", "html", "table", "td", "th", "marquee", "object" };
      TagSearchList = new String[] { "ol", "ul" };
      TagSearchButton = new String[] { "button" };
      TagSearchTableScope = new String[] { "html", "table" };
      TagSearchSelectScope = new String[] { "optgroup", "option" };
      TagSearchEndTags = new String[] { "dd", "dt", "li", "option", "optgroup", "p", "rp", "rt" };
      TagSearchSpecial = new String[] { "address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", "body", "br", "button", "caption", "center", "col", "colgroup", "command", "dd", "details", "dir", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe", "img", "input", "isindex", "li", "link", "listing", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", "p", "param", "plaintext", "pre", "script", "section", "select", "style", "summary", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "ul", "wbr", "xmp" };
      return;
    }
  }
  
  private void clearStackToContext(String... paramVarArgs)
  {
    for (int i = this.stack.size() - 1;; i--)
    {
      if (i >= 0)
      {
        Element localElement = (Element)this.stack.get(i);
        if ((!StringUtil.in(localElement.nodeName(), paramVarArgs)) && (!localElement.nodeName().equals("html"))) {}
      }
      else
      {
        return;
      }
      this.stack.remove(i);
    }
  }
  
  private boolean inSpecificScope(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2)
  {
    this.specificScopeTarget[0] = paramString;
    return inSpecificScope(this.specificScopeTarget, paramArrayOfString1, paramArrayOfString2);
  }
  
  private boolean inSpecificScope(String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3)
  {
    boolean bool2 = false;
    int i = this.stack.size() - 1;
    String str;
    boolean bool1;
    if (i >= 0)
    {
      str = ((Element)this.stack.get(i)).nodeName();
      if (StringUtil.in(str, paramArrayOfString1)) {
        bool1 = true;
      }
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (!StringUtil.in(str, paramArrayOfString2)) {
        if (paramArrayOfString3 != null)
        {
          bool1 = bool2;
          if (StringUtil.in(str, paramArrayOfString3)) {}
        }
        else
        {
          i--;
          break;
          Validate.fail("Should not be reachable");
          bool1 = bool2;
        }
      }
    }
  }
  
  private void insertNode(Node paramNode)
  {
    if (this.stack.size() == 0) {
      this.doc.appendChild(paramNode);
    }
    for (;;)
    {
      if (((paramNode instanceof Element)) && (((Element)paramNode).tag().isFormListed()) && (this.formElement != null)) {
        this.formElement.addElement((Element)paramNode);
      }
      return;
      if (isFosterInserts()) {
        insertInFosterParent(paramNode);
      } else {
        currentElement().appendChild(paramNode);
      }
    }
  }
  
  private boolean isElementInQueue(ArrayList<Element> paramArrayList, Element paramElement)
  {
    int i = paramArrayList.size() - 1;
    if (i >= 0) {
      if ((Element)paramArrayList.get(i) != paramElement) {}
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i--;
      break;
    }
  }
  
  private boolean isSameFormattingElement(Element paramElement1, Element paramElement2)
  {
    if ((paramElement1.nodeName().equals(paramElement2.nodeName())) && (paramElement1.attributes().equals(paramElement2.attributes()))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private void replaceInQueue(ArrayList<Element> paramArrayList, Element paramElement1, Element paramElement2)
  {
    int i = paramArrayList.lastIndexOf(paramElement1);
    if (i != -1) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool);
      paramArrayList.set(i, paramElement2);
      return;
    }
  }
  
  Element aboveOnStack(Element paramElement)
  {
    assert (onStack(paramElement));
    int i = this.stack.size() - 1;
    if (i >= 0) {
      if ((Element)this.stack.get(i) != paramElement) {}
    }
    for (paramElement = (Element)this.stack.get(i - 1);; paramElement = null)
    {
      return paramElement;
      i--;
      break;
    }
  }
  
  void clearFormattingElementsToLastMarker()
  {
    while ((!this.formattingElements.isEmpty()) && (removeLastFormattingElement() != null)) {}
  }
  
  void clearStackToTableBodyContext()
  {
    clearStackToContext(new String[] { "tbody", "tfoot", "thead" });
  }
  
  void clearStackToTableContext()
  {
    clearStackToContext(new String[] { "table" });
  }
  
  void clearStackToTableRowContext()
  {
    clearStackToContext(new String[] { "tr" });
  }
  
  ParseSettings defaultSettings()
  {
    return ParseSettings.htmlDefault;
  }
  
  void error(HtmlTreeBuilderState paramHtmlTreeBuilderState)
  {
    if (this.errors.canAddError()) {
      this.errors.add(new ParseError(this.reader.pos(), "Unexpected token [%s] when in state [%s]", new Object[] { this.currentToken.tokenType(), paramHtmlTreeBuilderState }));
    }
  }
  
  void framesetOk(boolean paramBoolean)
  {
    this.framesetOk = paramBoolean;
  }
  
  boolean framesetOk()
  {
    return this.framesetOk;
  }
  
  void generateImpliedEndTags()
  {
    generateImpliedEndTags(null);
  }
  
  void generateImpliedEndTags(String paramString)
  {
    while ((paramString != null) && (!currentElement().nodeName().equals(paramString)) && (StringUtil.in(currentElement().nodeName(), TagSearchEndTags))) {
      pop();
    }
  }
  
  Element getActiveFormattingElement(String paramString)
  {
    for (int i = this.formattingElements.size() - 1;; i--)
    {
      Element localElement2;
      Element localElement1;
      if (i >= 0)
      {
        localElement2 = (Element)this.formattingElements.get(i);
        if (localElement2 != null) {}
      }
      else
      {
        localElement1 = null;
      }
      do
      {
        return localElement1;
        localElement1 = localElement2;
      } while (localElement2.nodeName().equals(paramString));
    }
  }
  
  String getBaseUri()
  {
    return this.baseUri;
  }
  
  Document getDocument()
  {
    return this.doc;
  }
  
  FormElement getFormElement()
  {
    return this.formElement;
  }
  
  Element getFromStack(String paramString)
  {
    int i = this.stack.size() - 1;
    Element localElement;
    if (i >= 0)
    {
      localElement = (Element)this.stack.get(i);
      if (!localElement.nodeName().equals(paramString)) {}
    }
    for (paramString = localElement;; paramString = null)
    {
      return paramString;
      i--;
      break;
    }
  }
  
  Element getHeadElement()
  {
    return this.headElement;
  }
  
  List<String> getPendingTableCharacters()
  {
    return this.pendingTableCharacters;
  }
  
  ArrayList<Element> getStack()
  {
    return this.stack;
  }
  
  boolean inButtonScope(String paramString)
  {
    return inScope(paramString, TagSearchButton);
  }
  
  boolean inListItemScope(String paramString)
  {
    return inScope(paramString, TagSearchList);
  }
  
  boolean inScope(String paramString)
  {
    return inScope(paramString, null);
  }
  
  boolean inScope(String paramString, String[] paramArrayOfString)
  {
    return inSpecificScope(paramString, TagsSearchInScope, paramArrayOfString);
  }
  
  boolean inScope(String[] paramArrayOfString)
  {
    return inSpecificScope(paramArrayOfString, TagsSearchInScope, null);
  }
  
  boolean inSelectScope(String paramString)
  {
    boolean bool2 = false;
    int i = this.stack.size() - 1;
    String str;
    boolean bool1;
    if (i >= 0)
    {
      str = ((Element)this.stack.get(i)).nodeName();
      if (str.equals(paramString)) {
        bool1 = true;
      }
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (StringUtil.in(str, TagSearchSelectScope))
      {
        i--;
        break;
        Validate.fail("Should not be reachable");
        bool1 = bool2;
      }
    }
  }
  
  boolean inTableScope(String paramString)
  {
    return inSpecificScope(paramString, TagSearchTableScope, null);
  }
  
  Element insert(Token.StartTag paramStartTag)
  {
    if (paramStartTag.isSelfClosing())
    {
      paramStartTag = insertEmpty(paramStartTag);
      this.stack.add(paramStartTag);
      this.tokeniser.transition(TokeniserState.Data);
      this.tokeniser.emit(this.emptyEnd.reset().name(paramStartTag.tagName()));
    }
    for (;;)
    {
      return paramStartTag;
      paramStartTag = new Element(Tag.valueOf(paramStartTag.name(), this.settings), this.baseUri, this.settings.normalizeAttributes(paramStartTag.attributes));
      insert(paramStartTag);
    }
  }
  
  void insert(Element paramElement)
  {
    insertNode(paramElement);
    this.stack.add(paramElement);
  }
  
  void insert(Token.Character paramCharacter)
  {
    String str = currentElement().tagName();
    if ((str.equals("script")) || (str.equals("style"))) {}
    for (paramCharacter = new DataNode(paramCharacter.getData(), this.baseUri);; paramCharacter = new TextNode(paramCharacter.getData(), this.baseUri))
    {
      currentElement().appendChild(paramCharacter);
      return;
    }
  }
  
  void insert(Token.Comment paramComment)
  {
    insertNode(new Comment(paramComment.getData(), this.baseUri));
  }
  
  Element insertEmpty(Token.StartTag paramStartTag)
  {
    Tag localTag = Tag.valueOf(paramStartTag.name(), this.settings);
    Element localElement = new Element(localTag, this.baseUri, paramStartTag.attributes);
    insertNode(localElement);
    if (paramStartTag.isSelfClosing())
    {
      if (!localTag.isKnownTag()) {
        break label64;
      }
      if (localTag.isSelfClosing()) {
        this.tokeniser.acknowledgeSelfClosingFlag();
      }
    }
    for (;;)
    {
      return localElement;
      label64:
      localTag.setSelfClosing();
      this.tokeniser.acknowledgeSelfClosingFlag();
    }
  }
  
  FormElement insertForm(Token.StartTag paramStartTag, boolean paramBoolean)
  {
    paramStartTag = new FormElement(Tag.valueOf(paramStartTag.name(), this.settings), this.baseUri, paramStartTag.attributes);
    setFormElement(paramStartTag);
    insertNode(paramStartTag);
    if (paramBoolean) {
      this.stack.add(paramStartTag);
    }
    return paramStartTag;
  }
  
  void insertInFosterParent(Node paramNode)
  {
    Element localElement2 = getFromStack("table");
    int i = 0;
    Element localElement1;
    if (localElement2 != null) {
      if (localElement2.parent() != null)
      {
        localElement1 = localElement2.parent();
        i = 1;
        if (i == 0) {
          break label73;
        }
        Validate.notNull(localElement2);
        localElement2.before(paramNode);
      }
    }
    for (;;)
    {
      return;
      localElement1 = aboveOnStack(localElement2);
      break;
      localElement1 = (Element)this.stack.get(0);
      break;
      label73:
      localElement1.appendChild(paramNode);
    }
  }
  
  void insertMarkerToFormattingElements()
  {
    this.formattingElements.add(null);
  }
  
  void insertOnStackAfter(Element paramElement1, Element paramElement2)
  {
    int i = this.stack.lastIndexOf(paramElement1);
    if (i != -1) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool);
      this.stack.add(i + 1, paramElement2);
      return;
    }
  }
  
  Element insertStartTag(String paramString)
  {
    paramString = new Element(Tag.valueOf(paramString, this.settings), this.baseUri);
    insert(paramString);
    return paramString;
  }
  
  boolean isFosterInserts()
  {
    return this.fosterInserts;
  }
  
  boolean isFragmentParsing()
  {
    return this.fragmentParsing;
  }
  
  boolean isInActiveFormattingElements(Element paramElement)
  {
    return isElementInQueue(this.formattingElements, paramElement);
  }
  
  boolean isSpecial(Element paramElement)
  {
    return StringUtil.in(paramElement.nodeName(), TagSearchSpecial);
  }
  
  Element lastFormattingElement()
  {
    if (this.formattingElements.size() > 0) {}
    for (Element localElement = (Element)this.formattingElements.get(this.formattingElements.size() - 1);; localElement = null) {
      return localElement;
    }
  }
  
  void markInsertionMode()
  {
    this.originalState = this.state;
  }
  
  void maybeSetBaseUri(Element paramElement)
  {
    if (this.baseUriSetFromDoc) {}
    for (;;)
    {
      return;
      paramElement = paramElement.absUrl("href");
      if (paramElement.length() != 0)
      {
        this.baseUri = paramElement;
        this.baseUriSetFromDoc = true;
        this.doc.setBaseUri(paramElement);
      }
    }
  }
  
  void newPendingTableCharacters()
  {
    this.pendingTableCharacters = new ArrayList();
  }
  
  boolean onStack(Element paramElement)
  {
    return isElementInQueue(this.stack, paramElement);
  }
  
  HtmlTreeBuilderState originalState()
  {
    return this.originalState;
  }
  
  Document parse(String paramString1, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    this.state = HtmlTreeBuilderState.Initial;
    this.baseUriSetFromDoc = false;
    return super.parse(paramString1, paramString2, paramParseErrorList, paramParseSettings);
  }
  
  List<Node> parseFragment(String paramString1, Element paramElement, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    this.state = HtmlTreeBuilderState.Initial;
    initialiseParse(paramString1, paramString2, paramParseErrorList, paramParseSettings);
    this.contextElement = paramElement;
    this.fragmentParsing = true;
    paramString1 = null;
    if (paramElement != null)
    {
      if (paramElement.ownerDocument() != null) {
        this.doc.quirksMode(paramElement.ownerDocument().quirksMode());
      }
      paramString1 = paramElement.tagName();
      if (StringUtil.in(paramString1, new String[] { "title", "textarea" }))
      {
        this.tokeniser.transition(TokeniserState.Rcdata);
        paramString2 = new Element(Tag.valueOf("html", paramParseSettings), paramString2);
        this.doc.appendChild(paramString2);
        this.stack.add(paramString2);
        resetInsertionMode();
        paramString1 = paramElement.parents();
        paramString1.add(0, paramElement);
        paramParseErrorList = paramString1.iterator();
        do
        {
          paramString1 = paramString2;
          if (!paramParseErrorList.hasNext()) {
            break;
          }
          paramString1 = (Element)paramParseErrorList.next();
        } while (!(paramString1 instanceof FormElement));
        this.formElement = ((FormElement)paramString1);
        paramString1 = paramString2;
      }
    }
    else
    {
      runParser();
      if ((paramElement == null) || (paramString1 == null)) {
        break label333;
      }
    }
    label333:
    for (paramString1 = paramString1.childNodes();; paramString1 = this.doc.childNodes())
    {
      return paramString1;
      if (StringUtil.in(paramString1, new String[] { "iframe", "noembed", "noframes", "style", "xmp" }))
      {
        this.tokeniser.transition(TokeniserState.Rawtext);
        break;
      }
      if (paramString1.equals("script"))
      {
        this.tokeniser.transition(TokeniserState.ScriptData);
        break;
      }
      if (paramString1.equals("noscript"))
      {
        this.tokeniser.transition(TokeniserState.Data);
        break;
      }
      if (paramString1.equals("plaintext"))
      {
        this.tokeniser.transition(TokeniserState.Data);
        break;
      }
      this.tokeniser.transition(TokeniserState.Data);
      break;
    }
  }
  
  Element pop()
  {
    int i = this.stack.size();
    return (Element)this.stack.remove(i - 1);
  }
  
  void popStackToBefore(String paramString)
  {
    for (int i = this.stack.size() - 1;; i--)
    {
      if ((i < 0) || (((Element)this.stack.get(i)).nodeName().equals(paramString))) {
        return;
      }
      this.stack.remove(i);
    }
  }
  
  void popStackToClose(String paramString)
  {
    for (int i = this.stack.size() - 1;; i--) {
      if (i >= 0)
      {
        Element localElement = (Element)this.stack.get(i);
        this.stack.remove(i);
        if (!localElement.nodeName().equals(paramString)) {}
      }
      else
      {
        return;
      }
    }
  }
  
  void popStackToClose(String... paramVarArgs)
  {
    for (int i = this.stack.size() - 1;; i--) {
      if (i >= 0)
      {
        Element localElement = (Element)this.stack.get(i);
        this.stack.remove(i);
        if (!StringUtil.in(localElement.nodeName(), paramVarArgs)) {}
      }
      else
      {
        return;
      }
    }
  }
  
  protected boolean process(Token paramToken)
  {
    this.currentToken = paramToken;
    return this.state.process(paramToken, this);
  }
  
  boolean process(Token paramToken, HtmlTreeBuilderState paramHtmlTreeBuilderState)
  {
    this.currentToken = paramToken;
    return paramHtmlTreeBuilderState.process(paramToken, this);
  }
  
  void push(Element paramElement)
  {
    this.stack.add(paramElement);
  }
  
  void pushActiveFormattingElements(Element paramElement)
  {
    int k = 0;
    int i = this.formattingElements.size() - 1;
    for (;;)
    {
      Element localElement;
      if (i >= 0)
      {
        localElement = (Element)this.formattingElements.get(i);
        if (localElement != null) {
          break label45;
        }
      }
      label45:
      int j;
      for (;;)
      {
        this.formattingElements.add(paramElement);
        return;
        j = k;
        if (isSameFormattingElement(paramElement, localElement)) {
          j = k + 1;
        }
        if (j != 3) {
          break;
        }
        this.formattingElements.remove(i);
      }
      i--;
      k = j;
    }
  }
  
  void reconstructFormattingElements()
  {
    Object localObject = lastFormattingElement();
    if ((localObject == null) || (onStack((Element)localObject))) {
      return;
    }
    int n = this.formattingElements.size();
    int i = n - 1;
    int m = 0;
    label38:
    int j;
    if (i == 0) {
      j = 1;
    }
    for (;;)
    {
      int k = i;
      if (j == 0)
      {
        localObject = this.formattingElements;
        k = i + 1;
        localObject = (Element)((ArrayList)localObject).get(k);
      }
      Validate.notNull(localObject);
      j = 0;
      Element localElement = insertStartTag(((Element)localObject).nodeName());
      localElement.attributes().addAll(((Element)localObject).attributes());
      this.formattingElements.set(k, localElement);
      i = k;
      if (k == n - 1)
      {
        break;
        localObject = this.formattingElements;
        k = i - 1;
        localElement = (Element)((ArrayList)localObject).get(k);
        localObject = localElement;
        i = k;
        j = m;
        if (localElement != null)
        {
          localObject = localElement;
          i = k;
          if (!onStack(localElement)) {
            break label38;
          }
          localObject = localElement;
          i = k;
          j = m;
        }
      }
    }
  }
  
  void removeFromActiveFormattingElements(Element paramElement)
  {
    for (int i = this.formattingElements.size() - 1;; i--) {
      if (i >= 0)
      {
        if ((Element)this.formattingElements.get(i) == paramElement) {
          this.formattingElements.remove(i);
        }
      }
      else {
        return;
      }
    }
  }
  
  boolean removeFromStack(Element paramElement)
  {
    int i = this.stack.size() - 1;
    if (i >= 0) {
      if ((Element)this.stack.get(i) == paramElement) {
        this.stack.remove(i);
      }
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i--;
      break;
    }
  }
  
  Element removeLastFormattingElement()
  {
    int i = this.formattingElements.size();
    if (i > 0) {}
    for (Element localElement = (Element)this.formattingElements.remove(i - 1);; localElement = null) {
      return localElement;
    }
  }
  
  void replaceActiveFormattingElement(Element paramElement1, Element paramElement2)
  {
    replaceInQueue(this.formattingElements, paramElement1, paramElement2);
  }
  
  void replaceOnStack(Element paramElement1, Element paramElement2)
  {
    replaceInQueue(this.stack, paramElement1, paramElement2);
  }
  
  void resetInsertionMode()
  {
    int j = 0;
    for (int i = this.stack.size() - 1;; i--)
    {
      Object localObject;
      if (i >= 0)
      {
        localObject = (Element)this.stack.get(i);
        if (i == 0)
        {
          j = 1;
          localObject = this.contextElement;
        }
        localObject = ((Element)localObject).nodeName();
        if (!"select".equals(localObject)) {
          break label61;
        }
        transition(HtmlTreeBuilderState.InSelect);
      }
      for (;;)
      {
        return;
        label61:
        if (("td".equals(localObject)) || (("th".equals(localObject)) && (j == 0)))
        {
          transition(HtmlTreeBuilderState.InCell);
        }
        else if ("tr".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InRow);
        }
        else if (("tbody".equals(localObject)) || ("thead".equals(localObject)) || ("tfoot".equals(localObject)))
        {
          transition(HtmlTreeBuilderState.InTableBody);
        }
        else if ("caption".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InCaption);
        }
        else if ("colgroup".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InColumnGroup);
        }
        else if ("table".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InTable);
        }
        else if ("head".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InBody);
        }
        else if ("body".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InBody);
        }
        else if ("frameset".equals(localObject))
        {
          transition(HtmlTreeBuilderState.InFrameset);
        }
        else if ("html".equals(localObject))
        {
          transition(HtmlTreeBuilderState.BeforeHead);
        }
        else
        {
          if (j == 0) {
            break;
          }
          transition(HtmlTreeBuilderState.InBody);
        }
      }
    }
  }
  
  void setFormElement(FormElement paramFormElement)
  {
    this.formElement = paramFormElement;
  }
  
  void setFosterInserts(boolean paramBoolean)
  {
    this.fosterInserts = paramBoolean;
  }
  
  void setHeadElement(Element paramElement)
  {
    this.headElement = paramElement;
  }
  
  void setPendingTableCharacters(List<String> paramList)
  {
    this.pendingTableCharacters = paramList;
  }
  
  HtmlTreeBuilderState state()
  {
    return this.state;
  }
  
  public String toString()
  {
    return "TreeBuilder{currentToken=" + this.currentToken + ", state=" + this.state + ", currentElement=" + currentElement() + '}';
  }
  
  void transition(HtmlTreeBuilderState paramHtmlTreeBuilderState)
  {
    this.state = paramHtmlTreeBuilderState;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\HtmlTreeBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */