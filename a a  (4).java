package com.testfairy.activities.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class d
  extends View
  implements View.OnTouchListener
{
  private int[] a = { 10, 15, 40 };
  private Path b = new Path();
  private Paint c = new Paint();
  private Paint d;
  private Canvas e;
  private Bitmap f;
  
  public d(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public d(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public d(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.c.setAntiAlias(true);
    this.c.setDither(true);
    this.c.setColor(-16777216);
    this.c.setStyle(Paint.Style.STROKE);
    this.c.setStrokeJoin(Paint.Join.ROUND);
    this.c.setStrokeCap(Paint.Cap.ROUND);
    this.c.setStrokeWidth(this.a[1]);
    this.d = new Paint(4);
    setFocusable(true);
    setFocusableInTouchMode(true);
    setOnTouchListener(this);
  }
  
  public void a()
  {
    if (this.e == null) {}
    for (;;)
    {
      return;
      this.e.drawColor(0, PorterDuff.Mode.CLEAR);
      invalidate();
    }
  }
  
  public void a(int paramInt)
  {
    this.c.setColor(paramInt);
  }
  
  void a(int paramInt1, int paramInt2)
  {
    this.f = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
    this.e = new Canvas(this.f);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.c.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }
    for (;;)
    {
      return;
      this.c.setXfermode(null);
    }
  }
  
  public void b(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > this.a.length)) {}
    for (;;)
    {
      return;
      this.c.setStrokeWidth(this.a[paramInt]);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.f == null) {}
    for (;;)
    {
      return;
      paramCanvas.drawBitmap(this.f, 0.0F, 0.0F, this.d);
      paramCanvas.drawPath(this.b, this.c);
    }
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    boolean bool = false;
    if (this.e == null) {}
    float f2;
    float f1;
    for (;;)
    {
      return bool;
      f2 = paramMotionEvent.getX();
      f1 = paramMotionEvent.getY();
      switch (paramMotionEvent.getAction())
      {
      }
    }
    this.b.moveTo(f2, f1);
    for (;;)
    {
      invalidate();
      bool = true;
      break;
      this.b.lineTo(f2, f1);
      this.e.drawPath(this.b, this.c);
      continue;
      this.e.drawPath(this.b, this.c);
      this.b.reset();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\a\a\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */