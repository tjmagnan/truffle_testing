package com.testfairy.b;

public class b
{
  private static final a a = new a()
  {
    public byte[] a(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      byte[] arrayOfByte = new byte[paramAnonymousInt2];
      System.arraycopy(paramAnonymousArrayOfByte, paramAnonymousInt1, arrayOfByte, 0, paramAnonymousInt2);
      return arrayOfByte;
    }
  };
  private static final a b = new a()
  {
    public byte[] a(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      int j = 0;
      Object localObject;
      if (paramAnonymousInt2 == 0)
      {
        localObject = new byte[0];
        return (byte[])localObject;
      }
      byte[] arrayOfByte = new byte[paramAnonymousInt2 >> 2];
      int m;
      for (int i = paramAnonymousInt1;; i = m + 1 + 6)
      {
        localObject = arrayOfByte;
        if (i >= paramAnonymousInt1 + paramAnonymousInt2 - 2 - 1) {
          break;
        }
        int k = j + 1;
        m = i + 1;
        arrayOfByte[j] = paramAnonymousArrayOfByte[i];
        j = k + 1;
        arrayOfByte[k] = paramAnonymousArrayOfByte[m];
      }
    }
  };
  private static final a c = new a()
  {
    public byte[] a(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      int j = 0;
      Object localObject;
      if (paramAnonymousInt2 == 0)
      {
        localObject = new byte[0];
        return (byte[])localObject;
      }
      byte[] arrayOfByte = new byte[paramAnonymousInt2 >> 2];
      int i = paramAnonymousInt1;
      for (;;)
      {
        localObject = arrayOfByte;
        if (i >= paramAnonymousInt1 + paramAnonymousInt2 - 2 - 1) {
          break;
        }
        arrayOfByte[j] = paramAnonymousArrayOfByte[i];
        i = i + 1 + 3;
        j++;
      }
    }
  };
  private static final a d = new a()
  {
    public byte[] a(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      int j = 0;
      Object localObject;
      if (paramAnonymousInt2 == 0)
      {
        localObject = new byte[0];
        return (byte[])localObject;
      }
      byte[] arrayOfByte = new byte[paramAnonymousInt2 >> 1];
      int k;
      for (int i = paramAnonymousInt1;; i = k + 1 + 2)
      {
        localObject = arrayOfByte;
        if (i >= paramAnonymousInt1 + paramAnonymousInt2 - 2 - 1) {
          break;
        }
        int m = j + 1;
        k = i + 1;
        arrayOfByte[j] = paramAnonymousArrayOfByte[i];
        j = m + 1;
        arrayOfByte[m] = paramAnonymousArrayOfByte[k];
      }
    }
  };
  private static final a e = new a()
  {
    public byte[] a(byte[] paramAnonymousArrayOfByte, int paramAnonymousInt1, int paramAnonymousInt2)
    {
      int j = 0;
      Object localObject;
      if (paramAnonymousInt2 == 0)
      {
        localObject = new byte[0];
        return (byte[])localObject;
      }
      byte[] arrayOfByte = new byte[paramAnonymousInt2 >> 1];
      int i = paramAnonymousInt1;
      for (;;)
      {
        localObject = arrayOfByte;
        if (i >= paramAnonymousInt1 + paramAnonymousInt2 - 2 - 1) {
          break;
        }
        arrayOfByte[j] = paramAnonymousArrayOfByte[i];
        i = i + 1 + 1;
        j++;
      }
    }
  };
  
  public static a a(a parama)
  {
    if (parama.a == parama.b << 2) {
      if ((parama.c == 1) && (parama.d == 16)) {
        parama = b;
      }
    }
    for (;;)
    {
      return parama;
      if ((parama.c == 1) && (parama.d == 8))
      {
        parama = c;
      }
      else
      {
        if (parama.a == parama.b << 1)
        {
          if ((parama.c == 1) && (parama.d == 16))
          {
            parama = d;
            continue;
          }
          if ((parama.c == 1) && (parama.d == 8))
          {
            parama = e;
            continue;
          }
        }
        parama = a;
      }
    }
  }
  
  public static class a
  {
    public int a;
    public int b;
    public int c;
    public int d;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\b\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */