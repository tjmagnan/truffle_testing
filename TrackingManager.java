package tech.dcube.companion.managers.server;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.backend.PBRequest.PBResponse;
import tech.dcube.companion.managers.server.backend.VolleySingleton;
import tech.dcube.companion.model.PBShipmentTracking;
import tech.dcube.companion.model.PBShipmentTrackingDetail;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

class TrackingManager
{
  private static final Gson gson = new Gson();
  private static final TrackingManager ourInstance = new TrackingManager();
  private DataManager dataManager = DataManager.getInstance();
  private Request reqTracking;
  private Request reqTrackingDetail;
  private Request reqTrackingList;
  
  static TrackingManager getInstance()
  {
    return ourInstance;
  }
  
  String getTrackingDetail(final Context paramContext, final PBShipmentTracking paramPBShipmentTracking, final ServerManager.RequestCompletionHandler<PBShipmentTrackingDetail> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_TRACKING_DETAIL), new Object[] { paramPBShipmentTracking.getTrackingId() });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqTrackingDetail = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (PBShipmentTrackingDetail)PBShipmentTrackingDetail.fromJson(PBShipmentTrackingDetail.class, paramAnonymousPBResponse.body);
        paramPBShipmentTracking.setShipmentDetail(paramAnonymousPBResponse);
        Log.wtf("Main", "Details: " + paramAnonymousPBResponse);
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, TrackingManager.this.reqTrackingDetail.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            TrackingManager.ourInstance.getTrackingDetail(TrackingManager.4.this.val$context, TrackingManager.4.this.val$tracking, TrackingManager.4.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqTrackingDetail);
    return this.reqTrackingDetail.getTag().toString();
  }
  
  String getTrackingsList(final Context paramContext, final ServerManager.RequestCompletionHandler<List<PBShipmentTracking>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_TRACKING_LIST);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqTrackingList = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Object localObject = new JsonParser().parse(paramAnonymousPBResponse.body).getAsJsonObject().get("results");
        paramAnonymousPBResponse = new ArrayList();
        if (localObject != null)
        {
          localObject = PBShipmentTracking.fromJsonArray(PBShipmentTracking[].class, ((JsonElement)localObject).toString());
          paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          if (localObject != null)
          {
            TrackingManager.this.dataManager.getTrackings().clear();
            TrackingManager.this.dataManager.getTrackings().addAll((Collection)localObject);
            paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, TrackingManager.this.reqTrackingList.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            TrackingManager.ourInstance.getTrackingsList(TrackingManager.6.this.val$context, TrackingManager.6.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqTrackingList);
    return this.reqTrackingList.getTag().toString();
  }
  
  String trackShipmentById(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<PBShipmentTracking> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_TRACKING_BY_ID), new Object[] { paramString });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqTracking = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Object localObject1 = new JsonParser().parse(paramAnonymousPBResponse.body).getAsJsonObject().get("results");
        paramAnonymousPBResponse = new ArrayList();
        if (localObject1 != null)
        {
          localObject1 = PBShipmentTracking.fromJsonArray(PBShipmentTracking[].class, ((JsonElement)localObject1).toString());
          paramAnonymousPBResponse = (PBRequest.PBResponse)localObject1;
          if (localObject1 == null) {}
        }
        Object localObject2 = null;
        localObject1 = localObject2;
        if (paramAnonymousPBResponse != null)
        {
          localObject1 = localObject2;
          if (paramAnonymousPBResponse.size() > 0) {
            localObject1 = (PBShipmentTracking)paramAnonymousPBResponse.get(0);
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(localObject1, TrackingManager.this.reqTracking.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            TrackingManager.ourInstance.trackShipmentById(TrackingManager.2.this.val$context, TrackingManager.2.this.val$trackingId, TrackingManager.2.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqTracking);
    return this.reqTracking.getTag().toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\TrackingManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */