package tech.dcube.companion.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.PrintAttributes.Builder;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentAdapter.LayoutResultCallback;
import android.print.PrintDocumentInfo.Builder;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPrintLabel;

public class PBReceiptShowFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private String Receipt_PDF_url;
  OnShipmentReceiptListener mListener;
  String pdfUrl = "http://www.orimi.com/pdf-test.pdf";
  private String tag;
  @BindView(2131624196)
  WebView webView;
  
  private void createWebPrintJob(WebView paramWebView)
  {
    if (Build.VERSION.SDK_INT >= 19)
    {
      PrintManager localPrintManager = (PrintManager)getActivity().getSystemService("print");
      paramWebView = paramWebView.createPrintDocumentAdapter();
      localPrintManager.print(getString(2131165223) + " Document", paramWebView, new PrintAttributes.Builder().build());
    }
    for (;;)
    {
      return;
      displayToast("It seems, printing is not availble");
    }
  }
  
  public static PBReceiptShowFragment newInstance(String paramString1, String paramString2)
  {
    PBReceiptShowFragment localPBReceiptShowFragment = new PBReceiptShowFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBReceiptShowFragment.setArguments(localBundle);
    return localPBReceiptShowFragment;
  }
  
  private void sharePdfUrl(String paramString1, String paramString2)
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    localIntent.putExtra("android.intent.extra.SUBJECT", paramString1);
    localIntent.putExtra("android.intent.extra.TEXT", paramString2);
    getActivity().startActivity(Intent.createChooser(localIntent, "Share"));
  }
  
  void createGoogleDocsPrintIntent()
  {
    String str = this.Receipt_PDF_url;
    str = "http://docs.google.com/viewer?url=" + str;
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setDataAndType(Uri.parse(str), "text/html");
    startActivity(localIntent);
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  void downloadAndPrintPDF() {}
  
  void downloadPDFfromWeb()
  {
    String str = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(Calendar.getInstance().getTime());
    str = "Receipt " + str + ".pdf";
    new DownloadFile(null).execute(new String[] { this.Receipt_PDF_url, str });
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnShipmentReceiptListener))
    {
      this.mListener = ((OnShipmentReceiptListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnShipmentReceiptListener");
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    onShipmentDoneInteraction();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.Receipt_PDF_url = getArguments().getString("param1");
      this.tag = getArguments().getString("param2");
    }
  }
  
  @SuppressLint({"SetJavaScriptEnabled"})
  @Nullable
  public View onCreateView(LayoutInflater paramLayoutInflater, @Nullable ViewGroup paramViewGroup, @Nullable Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968646, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("LabelPrint");
    setTopBarLeftButtonText("Back");
    hideTopBarLeftButton();
    setTopBarRightButtonText("Done");
    paramViewGroup = DataManager.getInstance().getCurrentShipment().getPrintLabel();
    try
    {
      this.Receipt_PDF_url = paramViewGroup.getLabelUrl();
      this.webView.getSettings().setJavaScriptEnabled(true);
      this.webView.clearCache(true);
      this.webView.loadUrl("https://docs.google.com/viewer?url=" + this.Receipt_PDF_url);
      return paramLayoutInflater;
    }
    catch (Exception paramViewGroup)
    {
      for (;;)
      {
        paramViewGroup.printStackTrace();
        this.Receipt_PDF_url = this.pdfUrl;
        displayToast("Print Label Error");
      }
    }
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onShipmentDoneInteraction()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentReceipt("action_done_button");
    }
  }
  
  @OnClick({2131624197})
  public void print()
  {
    createGoogleDocsPrintIntent();
  }
  
  void printDownloadedPDF(final String paramString)
  {
    ((PrintManager)getActivity().getSystemService("print")).print(getString(2131165223) + " Document", new PrintDocumentAdapter()new PrintAttributes.Builder
    {
      public void onLayout(PrintAttributes paramAnonymousPrintAttributes1, PrintAttributes paramAnonymousPrintAttributes2, CancellationSignal paramAnonymousCancellationSignal, PrintDocumentAdapter.LayoutResultCallback paramAnonymousLayoutResultCallback, Bundle paramAnonymousBundle)
      {
        if (paramAnonymousCancellationSignal.isCanceled()) {
          paramAnonymousLayoutResultCallback.onLayoutCancelled();
        }
        for (;;)
        {
          return;
          paramAnonymousLayoutResultCallback.onLayoutFinished(new PrintDocumentInfo.Builder(paramString).setContentType(0).build(), true);
        }
      }
      
      /* Error */
      public void onWrite(android.print.PageRange[] paramAnonymousArrayOfPageRange, android.os.ParcelFileDescriptor paramAnonymousParcelFileDescriptor, CancellationSignal paramAnonymousCancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback paramAnonymousWriteResultCallback)
      {
        // Byte code:
        //   0: aconst_null
        //   1: astore 10
        //   3: aconst_null
        //   4: astore 9
        //   6: aconst_null
        //   7: astore_3
        //   8: aconst_null
        //   9: astore 8
        //   11: aconst_null
        //   12: astore 6
        //   14: aconst_null
        //   15: astore 7
        //   17: new 62	java/io/FileInputStream
        //   20: astore_1
        //   21: aload_1
        //   22: aload_0
        //   23: getfield 19	tech/dcube/companion/fragments/PBReceiptShowFragment$1:val$fileName	Ljava/lang/String;
        //   26: invokespecial 63	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
        //   29: new 65	java/io/FileOutputStream
        //   32: astore_3
        //   33: aload_3
        //   34: aload_2
        //   35: invokevirtual 71	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //   38: invokespecial 74	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //   41: sipush 1024
        //   44: newarray <illegal type>
        //   46: astore_2
        //   47: aload_1
        //   48: aload_2
        //   49: invokevirtual 80	java/io/InputStream:read	([B)I
        //   52: istore 5
        //   54: iload 5
        //   56: ifle +26 -> 82
        //   59: aload_3
        //   60: aload_2
        //   61: iconst_0
        //   62: iload 5
        //   64: invokevirtual 86	java/io/OutputStream:write	([BII)V
        //   67: goto -20 -> 47
        //   70: astore_2
        //   71: aload_3
        //   72: astore_2
        //   73: aload_1
        //   74: invokevirtual 89	java/io/InputStream:close	()V
        //   77: aload_2
        //   78: invokevirtual 90	java/io/OutputStream:close	()V
        //   81: return
        //   82: aload 4
        //   84: iconst_1
        //   85: anewarray 92	android/print/PageRange
        //   88: dup
        //   89: iconst_0
        //   90: getstatic 96	android/print/PageRange:ALL_PAGES	Landroid/print/PageRange;
        //   93: aastore
        //   94: invokevirtual 102	android/print/PrintDocumentAdapter$WriteResultCallback:onWriteFinished	([Landroid/print/PageRange;)V
        //   97: aload_1
        //   98: invokevirtual 89	java/io/InputStream:close	()V
        //   101: aload_3
        //   102: invokevirtual 90	java/io/OutputStream:close	()V
        //   105: goto -24 -> 81
        //   108: astore_1
        //   109: aload_1
        //   110: invokevirtual 105	java/io/IOException:printStackTrace	()V
        //   113: goto -32 -> 81
        //   116: astore_1
        //   117: aload_1
        //   118: invokevirtual 105	java/io/IOException:printStackTrace	()V
        //   121: goto -40 -> 81
        //   124: astore_1
        //   125: aload 8
        //   127: astore_3
        //   128: aload 10
        //   130: astore_1
        //   131: aload_1
        //   132: invokevirtual 89	java/io/InputStream:close	()V
        //   135: aload_3
        //   136: invokevirtual 90	java/io/OutputStream:close	()V
        //   139: goto -58 -> 81
        //   142: astore_1
        //   143: aload_1
        //   144: invokevirtual 105	java/io/IOException:printStackTrace	()V
        //   147: goto -66 -> 81
        //   150: astore_2
        //   151: aload 6
        //   153: astore_3
        //   154: aload 9
        //   156: astore_1
        //   157: aload_1
        //   158: invokevirtual 89	java/io/InputStream:close	()V
        //   161: aload_3
        //   162: invokevirtual 90	java/io/OutputStream:close	()V
        //   165: aload_2
        //   166: athrow
        //   167: astore_1
        //   168: aload_1
        //   169: invokevirtual 105	java/io/IOException:printStackTrace	()V
        //   172: goto -7 -> 165
        //   175: astore_2
        //   176: aload 6
        //   178: astore_3
        //   179: goto -22 -> 157
        //   182: astore_2
        //   183: goto -26 -> 157
        //   186: astore_2
        //   187: aload 8
        //   189: astore_3
        //   190: goto -59 -> 131
        //   193: astore_2
        //   194: goto -63 -> 131
        //   197: astore_1
        //   198: aload_3
        //   199: astore_1
        //   200: aload 7
        //   202: astore_2
        //   203: goto -130 -> 73
        //   206: astore_2
        //   207: aload 7
        //   209: astore_2
        //   210: goto -137 -> 73
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	213	0	this	1
        //   0	213	1	paramAnonymousArrayOfPageRange	android.print.PageRange[]
        //   0	213	2	paramAnonymousParcelFileDescriptor	android.os.ParcelFileDescriptor
        //   0	213	3	paramAnonymousCancellationSignal	CancellationSignal
        //   0	213	4	paramAnonymousWriteResultCallback	android.print.PrintDocumentAdapter.WriteResultCallback
        //   52	11	5	i	int
        //   12	165	6	localObject1	Object
        //   15	193	7	localObject2	Object
        //   9	179	8	localObject3	Object
        //   4	151	9	localObject4	Object
        //   1	128	10	localObject5	Object
        // Exception table:
        //   from	to	target	type
        //   41	47	70	java/io/FileNotFoundException
        //   47	54	70	java/io/FileNotFoundException
        //   59	67	70	java/io/FileNotFoundException
        //   82	97	70	java/io/FileNotFoundException
        //   97	105	108	java/io/IOException
        //   73	81	116	java/io/IOException
        //   17	29	124	java/lang/Exception
        //   131	139	142	java/io/IOException
        //   17	29	150	finally
        //   157	165	167	java/io/IOException
        //   29	41	175	finally
        //   41	47	182	finally
        //   47	54	182	finally
        //   59	67	182	finally
        //   82	97	182	finally
        //   29	41	186	java/lang/Exception
        //   41	47	193	java/lang/Exception
        //   47	54	193	java/lang/Exception
        //   59	67	193	java/lang/Exception
        //   82	97	193	java/lang/Exception
        //   17	29	197	java/io/FileNotFoundException
        //   29	41	206	java/io/FileNotFoundException
      }
    }, new PrintAttributes.Builder().build());
  }
  
  @OnClick({2131624198})
  public void share()
  {
    sharePdfUrl("Companion Shipping Receipt", this.Receipt_PDF_url);
  }
  
  private class DownloadFile
    extends AsyncTask<String, Void, Void>
  {
    private DownloadFile() {}
    
    protected Void doInBackground(String... paramVarArgs)
    {
      String str = paramVarArgs[0];
      paramVarArgs = paramVarArgs[1];
      File localFile = new File(Environment.getExternalStorageDirectory().toString(), "Companion App");
      localFile.mkdir();
      paramVarArgs = new File(localFile, paramVarArgs);
      try
      {
        paramVarArgs.createNewFile();
        PBReceiptShowFragment.FileDownloader.downloadFile(str, paramVarArgs);
        return null;
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localIOException.printStackTrace();
        }
      }
    }
  }
  
  public static class FileDownloader
  {
    private static final int MEGABYTE = 1048576;
    
    /* Error */
    public static void downloadFile(String paramString, File paramFile)
    {
      // Byte code:
      //   0: new 25	java/net/URL
      //   3: astore_3
      //   4: aload_3
      //   5: aload_0
      //   6: invokespecial 28	java/net/URL:<init>	(Ljava/lang/String;)V
      //   9: aload_3
      //   10: invokevirtual 32	java/net/URL:openConnection	()Ljava/net/URLConnection;
      //   13: checkcast 34	java/net/HttpURLConnection
      //   16: astore 4
      //   18: aload 4
      //   20: invokevirtual 37	java/net/HttpURLConnection:connect	()V
      //   23: aload 4
      //   25: invokevirtual 41	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
      //   28: astore_0
      //   29: new 43	java/io/FileOutputStream
      //   32: astore_3
      //   33: aload_3
      //   34: aload_1
      //   35: invokespecial 46	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
      //   38: aload 4
      //   40: invokevirtual 50	java/net/HttpURLConnection:getContentLength	()I
      //   43: pop
      //   44: ldc 10
      //   46: newarray <illegal type>
      //   48: astore_1
      //   49: aload_0
      //   50: aload_1
      //   51: invokevirtual 56	java/io/InputStream:read	([B)I
      //   54: istore_2
      //   55: iload_2
      //   56: ifle +19 -> 75
      //   59: aload_3
      //   60: aload_1
      //   61: iconst_0
      //   62: iload_2
      //   63: invokevirtual 60	java/io/FileOutputStream:write	([BII)V
      //   66: goto -17 -> 49
      //   69: astore_0
      //   70: aload_0
      //   71: invokevirtual 63	java/io/FileNotFoundException:printStackTrace	()V
      //   74: return
      //   75: aload_3
      //   76: invokevirtual 66	java/io/FileOutputStream:close	()V
      //   79: goto -5 -> 74
      //   82: astore_0
      //   83: aload_0
      //   84: invokevirtual 67	java/net/MalformedURLException:printStackTrace	()V
      //   87: goto -13 -> 74
      //   90: astore_0
      //   91: aload_0
      //   92: invokevirtual 68	java/io/IOException:printStackTrace	()V
      //   95: goto -21 -> 74
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	98	0	paramString	String
      //   0	98	1	paramFile	File
      //   54	9	2	i	int
      //   3	73	3	localObject	Object
      //   16	23	4	localHttpURLConnection	java.net.HttpURLConnection
      // Exception table:
      //   from	to	target	type
      //   0	49	69	java/io/FileNotFoundException
      //   49	55	69	java/io/FileNotFoundException
      //   59	66	69	java/io/FileNotFoundException
      //   75	79	69	java/io/FileNotFoundException
      //   0	49	82	java/net/MalformedURLException
      //   49	55	82	java/net/MalformedURLException
      //   59	66	82	java/net/MalformedURLException
      //   75	79	82	java/net/MalformedURLException
      //   0	49	90	java/io/IOException
      //   49	55	90	java/io/IOException
      //   59	66	90	java/io/IOException
      //   75	79	90	java/io/IOException
    }
  }
  
  public static abstract interface OnShipmentReceiptListener
  {
    public abstract void onShipmentReceipt(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBReceiptShowFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */