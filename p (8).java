package com.testfairy.p;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class h
{
  public static String a(Context paramContext)
  {
    Object localObject = null;
    try
    {
      paramContext = paramContext.getPackageManager().getInstallerPackageName(paramContext.getPackageName());
      return paramContext;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        paramContext = (Context)localObject;
      }
    }
  }
  
  public static String a(PackageManager paramPackageManager, String paramString)
  {
    Object localObject = null;
    try
    {
      paramPackageManager = paramPackageManager.getApplicationLabel(paramPackageManager.getApplicationInfo(paramString, 0)).toString();
      return paramPackageManager;
    }
    catch (Throwable paramPackageManager)
    {
      for (;;)
      {
        paramPackageManager = (PackageManager)localObject;
      }
    }
  }
  
  public static boolean a(Context paramContext, Class paramClass)
  {
    paramClass = new Intent(paramContext, paramClass);
    if (paramContext.getPackageManager().queryIntentActivities(paramClass, 65536).size() > 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static int b(Context paramContext)
  {
    int j = 0;
    try
    {
      i = paramContext.getPackageManager().getPackageInfo(e(paramContext), 0).versionCode;
      return i;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        int i = j;
      }
    }
  }
  
  public static String c(Context paramContext)
  {
    Object localObject = null;
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(e(paramContext), 0).versionName;
      return paramContext;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        paramContext = (Context)localObject;
      }
    }
  }
  
  public static List d(Context paramContext)
  {
    String[] arrayOfString = new String[0];
    try
    {
      paramContext = paramContext.getPackageManager().getPackageInfo(e(paramContext), 4096).requestedPermissions;
      return Arrays.asList(paramContext);
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        paramContext = arrayOfString;
      }
    }
  }
  
  public static String e(Context paramContext)
  {
    return paramContext.getApplicationContext().getPackageName();
  }
  
  public static Drawable f(Context paramContext)
  {
    Object localObject2 = null;
    Object localObject3 = paramContext.getPackageManager();
    localObject1 = localObject2;
    try
    {
      paramContext = ((PackageManager)localObject3).getApplicationInfo(e(paramContext), 0);
      localObject1 = localObject2;
      Resources localResources = ((PackageManager)localObject3).getResourcesForApplication(paramContext);
      localObject1 = localObject2;
      Configuration localConfiguration2 = localResources.getConfiguration();
      localObject1 = localObject2;
      Configuration localConfiguration1 = new android/content/res/Configuration;
      localObject1 = localObject2;
      localConfiguration1.<init>(localConfiguration2);
      localObject1 = localObject2;
      DisplayMetrics localDisplayMetrics = localResources.getDisplayMetrics();
      localObject1 = localObject2;
      localObject3 = localResources.getDisplayMetrics();
      localObject1 = localObject2;
      localDisplayMetrics.densityDpi = 320;
      localObject1 = localObject2;
      localResources.updateConfiguration(localConfiguration2, localDisplayMetrics);
      localObject1 = localObject2;
      paramContext = localResources.getDrawable(paramContext.icon);
      localObject1 = paramContext;
      localResources.updateConfiguration(localConfiguration1, (DisplayMetrics)localObject3);
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      for (;;)
      {
        paramContext = (Context)localObject1;
      }
    }
    return paramContext;
  }
  
  public static List g(Context paramContext)
  {
    localArrayList = new ArrayList();
    try
    {
      String str = e(paramContext);
      paramContext = paramContext.getPackageManager().getPackageInfo(str, 1).activities;
      int j = paramContext.length;
      for (int i = 0; i < j; i++) {
        localArrayList.add(paramContext[i].name);
      }
      return localArrayList;
    }
    catch (Exception paramContext) {}
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */