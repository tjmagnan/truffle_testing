package com.afollestad.materialdialogs;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.afollestad.materialdialogs.internal.MDAdapter;
import com.afollestad.materialdialogs.internal.MDButton;
import com.afollestad.materialdialogs.internal.MDRootLayout;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import me.zhanghai.android.materialprogressbar.HorizontalProgressDrawable;
import me.zhanghai.android.materialprogressbar.IndeterminateCircularProgressDrawable;
import me.zhanghai.android.materialprogressbar.IndeterminateHorizontalProgressDrawable;

class DialogInit
{
  private static void fixCanvasScalingWhenHardwareAccelerated(ProgressBar paramProgressBar)
  {
    if ((Build.VERSION.SDK_INT < 18) && (paramProgressBar.isHardwareAccelerated()) && (paramProgressBar.getLayerType() != 1)) {
      paramProgressBar.setLayerType(1, null);
    }
  }
  
  @LayoutRes
  static int getInflateLayout(MaterialDialog.Builder paramBuilder)
  {
    int i;
    if (paramBuilder.customView != null) {
      i = R.layout.md_dialog_custom;
    }
    for (;;)
    {
      return i;
      if ((paramBuilder.items != null) || (paramBuilder.adapter != null))
      {
        if (paramBuilder.checkBoxPrompt != null) {
          i = R.layout.md_dialog_list_check;
        } else {
          i = R.layout.md_dialog_list;
        }
      }
      else if (paramBuilder.progress > -2) {
        i = R.layout.md_dialog_progress;
      } else if (paramBuilder.indeterminateProgress)
      {
        if (paramBuilder.indeterminateIsHorizontalProgress) {
          i = R.layout.md_dialog_progress_indeterminate_horizontal;
        } else {
          i = R.layout.md_dialog_progress_indeterminate;
        }
      }
      else if (paramBuilder.inputCallback != null)
      {
        if (paramBuilder.checkBoxPrompt != null) {
          i = R.layout.md_dialog_input_check;
        } else {
          i = R.layout.md_dialog_input;
        }
      }
      else if (paramBuilder.checkBoxPrompt != null) {
        i = R.layout.md_dialog_basic_check;
      } else {
        i = R.layout.md_dialog_basic;
      }
    }
  }
  
  @StyleRes
  static int getTheme(@NonNull MaterialDialog.Builder paramBuilder)
  {
    Object localObject = paramBuilder.context;
    int i = R.attr.md_dark_theme;
    boolean bool;
    if (paramBuilder.theme == Theme.DARK)
    {
      bool = true;
      bool = DialogUtils.resolveBoolean((Context)localObject, i, bool);
      if (!bool) {
        break label56;
      }
      localObject = Theme.DARK;
      label36:
      paramBuilder.theme = ((Theme)localObject);
      if (!bool) {
        break label63;
      }
    }
    label56:
    label63:
    for (i = R.style.MD_Dark;; i = R.style.MD_Light)
    {
      return i;
      bool = false;
      break;
      localObject = Theme.LIGHT;
      break label36;
    }
  }
  
  @UiThread
  public static void init(MaterialDialog paramMaterialDialog)
  {
    MaterialDialog.Builder localBuilder = paramMaterialDialog.builder;
    paramMaterialDialog.setCancelable(localBuilder.cancelable);
    paramMaterialDialog.setCanceledOnTouchOutside(localBuilder.canceledOnTouchOutside);
    if (localBuilder.backgroundColor == 0) {
      localBuilder.backgroundColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_background_color, DialogUtils.resolveColor(paramMaterialDialog.getContext(), R.attr.colorBackgroundFloating));
    }
    if (localBuilder.backgroundColor != 0)
    {
      localObject1 = new GradientDrawable();
      ((GradientDrawable)localObject1).setCornerRadius(localBuilder.context.getResources().getDimension(R.dimen.md_bg_corner_radius));
      ((GradientDrawable)localObject1).setColor(localBuilder.backgroundColor);
      paramMaterialDialog.getWindow().setBackgroundDrawable((Drawable)localObject1);
    }
    if (!localBuilder.positiveColorSet) {
      localBuilder.positiveColor = DialogUtils.resolveActionTextColorStateList(localBuilder.context, R.attr.md_positive_color, localBuilder.positiveColor);
    }
    if (!localBuilder.neutralColorSet) {
      localBuilder.neutralColor = DialogUtils.resolveActionTextColorStateList(localBuilder.context, R.attr.md_neutral_color, localBuilder.neutralColor);
    }
    if (!localBuilder.negativeColorSet) {
      localBuilder.negativeColor = DialogUtils.resolveActionTextColorStateList(localBuilder.context, R.attr.md_negative_color, localBuilder.negativeColor);
    }
    if (!localBuilder.widgetColorSet) {
      localBuilder.widgetColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_widget_color, localBuilder.widgetColor);
    }
    int i;
    if (!localBuilder.titleColorSet)
    {
      i = DialogUtils.resolveColor(paramMaterialDialog.getContext(), 16842806);
      localBuilder.titleColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_title_color, i);
    }
    if (!localBuilder.contentColorSet)
    {
      i = DialogUtils.resolveColor(paramMaterialDialog.getContext(), 16842808);
      localBuilder.contentColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_content_color, i);
    }
    if (!localBuilder.itemColorSet) {
      localBuilder.itemColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_item_color, localBuilder.contentColor);
    }
    paramMaterialDialog.title = ((TextView)paramMaterialDialog.view.findViewById(R.id.md_title));
    paramMaterialDialog.icon = ((ImageView)paramMaterialDialog.view.findViewById(R.id.md_icon));
    paramMaterialDialog.titleFrame = paramMaterialDialog.view.findViewById(R.id.md_titleFrame);
    paramMaterialDialog.content = ((TextView)paramMaterialDialog.view.findViewById(R.id.md_content));
    paramMaterialDialog.recyclerView = ((RecyclerView)paramMaterialDialog.view.findViewById(R.id.md_contentRecyclerView));
    paramMaterialDialog.checkBoxPrompt = ((CheckBox)paramMaterialDialog.view.findViewById(R.id.md_promptCheckbox));
    paramMaterialDialog.positiveButton = ((MDButton)paramMaterialDialog.view.findViewById(R.id.md_buttonDefaultPositive));
    paramMaterialDialog.neutralButton = ((MDButton)paramMaterialDialog.view.findViewById(R.id.md_buttonDefaultNeutral));
    paramMaterialDialog.negativeButton = ((MDButton)paramMaterialDialog.view.findViewById(R.id.md_buttonDefaultNegative));
    if ((localBuilder.inputCallback != null) && (localBuilder.positiveText == null)) {
      localBuilder.positiveText = localBuilder.context.getText(17039370);
    }
    Object localObject1 = paramMaterialDialog.positiveButton;
    label550:
    label572:
    label678:
    int j;
    label911:
    label983:
    label1061:
    boolean bool1;
    label1222:
    label1571:
    label1591:
    FrameLayout localFrameLayout;
    Object localObject2;
    int k;
    if (localBuilder.positiveText != null)
    {
      i = 0;
      ((MDButton)localObject1).setVisibility(i);
      localObject1 = paramMaterialDialog.neutralButton;
      if (localBuilder.neutralText == null) {
        break label2033;
      }
      i = 0;
      ((MDButton)localObject1).setVisibility(i);
      localObject1 = paramMaterialDialog.negativeButton;
      if (localBuilder.negativeText == null) {
        break label2039;
      }
      i = 0;
      ((MDButton)localObject1).setVisibility(i);
      paramMaterialDialog.positiveButton.setFocusable(true);
      paramMaterialDialog.neutralButton.setFocusable(true);
      paramMaterialDialog.negativeButton.setFocusable(true);
      if (localBuilder.positiveFocus) {
        paramMaterialDialog.positiveButton.requestFocus();
      }
      if (localBuilder.neutralFocus) {
        paramMaterialDialog.neutralButton.requestFocus();
      }
      if (localBuilder.negativeFocus) {
        paramMaterialDialog.negativeButton.requestFocus();
      }
      if (localBuilder.icon == null) {
        break label2045;
      }
      paramMaterialDialog.icon.setVisibility(0);
      paramMaterialDialog.icon.setImageDrawable(localBuilder.icon);
      j = localBuilder.maxIconSize;
      i = j;
      if (j == -1) {
        i = DialogUtils.resolveDimension(localBuilder.context, R.attr.md_icon_max_size);
      }
      if ((localBuilder.limitIconToDefaultSize) || (DialogUtils.resolveBoolean(localBuilder.context, R.attr.md_icon_limit_icon_to_default_size))) {
        i = localBuilder.context.getResources().getDimensionPixelSize(R.dimen.md_icon_max_size);
      }
      if (i > -1)
      {
        paramMaterialDialog.icon.setAdjustViewBounds(true);
        paramMaterialDialog.icon.setMaxHeight(i);
        paramMaterialDialog.icon.setMaxWidth(i);
        paramMaterialDialog.icon.requestLayout();
      }
      if (!localBuilder.dividerColorSet)
      {
        i = DialogUtils.resolveColor(paramMaterialDialog.getContext(), R.attr.md_divider);
        localBuilder.dividerColor = DialogUtils.resolveColor(localBuilder.context, R.attr.md_divider_color, i);
      }
      paramMaterialDialog.view.setDividerColor(localBuilder.dividerColor);
      if (paramMaterialDialog.title != null)
      {
        paramMaterialDialog.setTypeface(paramMaterialDialog.title, localBuilder.mediumFont);
        paramMaterialDialog.title.setTextColor(localBuilder.titleColor);
        paramMaterialDialog.title.setGravity(localBuilder.titleGravity.getGravityInt());
        if (Build.VERSION.SDK_INT >= 17) {
          paramMaterialDialog.title.setTextAlignment(localBuilder.titleGravity.getTextAlignment());
        }
        if (localBuilder.title != null) {
          break label2095;
        }
        paramMaterialDialog.titleFrame.setVisibility(8);
      }
      if (paramMaterialDialog.content != null)
      {
        paramMaterialDialog.content.setMovementMethod(new LinkMovementMethod());
        paramMaterialDialog.setTypeface(paramMaterialDialog.content, localBuilder.regularFont);
        paramMaterialDialog.content.setLineSpacing(0.0F, localBuilder.contentLineSpacingMultiplier);
        if (localBuilder.linkColor != null) {
          break label2118;
        }
        paramMaterialDialog.content.setLinkTextColor(DialogUtils.resolveColor(paramMaterialDialog.getContext(), 16842806));
        paramMaterialDialog.content.setTextColor(localBuilder.contentColor);
        paramMaterialDialog.content.setGravity(localBuilder.contentGravity.getGravityInt());
        if (Build.VERSION.SDK_INT >= 17) {
          paramMaterialDialog.content.setTextAlignment(localBuilder.contentGravity.getTextAlignment());
        }
        if (localBuilder.content == null) {
          break label2133;
        }
        paramMaterialDialog.content.setText(localBuilder.content);
        paramMaterialDialog.content.setVisibility(0);
      }
      if (paramMaterialDialog.checkBoxPrompt != null)
      {
        paramMaterialDialog.checkBoxPrompt.setText(localBuilder.checkBoxPrompt);
        paramMaterialDialog.checkBoxPrompt.setChecked(localBuilder.checkBoxPromptInitiallyChecked);
        paramMaterialDialog.checkBoxPrompt.setOnCheckedChangeListener(localBuilder.checkBoxPromptListener);
        paramMaterialDialog.setTypeface(paramMaterialDialog.checkBoxPrompt, localBuilder.regularFont);
        paramMaterialDialog.checkBoxPrompt.setTextColor(localBuilder.contentColor);
        MDTintHelper.setTint(paramMaterialDialog.checkBoxPrompt, localBuilder.widgetColor);
      }
      paramMaterialDialog.view.setButtonGravity(localBuilder.buttonsGravity);
      paramMaterialDialog.view.setButtonStackedGravity(localBuilder.btnStackedGravity);
      paramMaterialDialog.view.setStackingBehavior(localBuilder.stackingBehavior);
      if (Build.VERSION.SDK_INT < 14) {
        break label2145;
      }
      boolean bool2 = DialogUtils.resolveBoolean(localBuilder.context, 16843660, true);
      bool1 = bool2;
      if (bool2) {
        bool1 = DialogUtils.resolveBoolean(localBuilder.context, R.attr.textAllCaps, true);
      }
      localObject1 = paramMaterialDialog.positiveButton;
      paramMaterialDialog.setTypeface((TextView)localObject1, localBuilder.mediumFont);
      ((MDButton)localObject1).setAllCapsCompat(bool1);
      ((MDButton)localObject1).setText(localBuilder.positiveText);
      ((MDButton)localObject1).setTextColor(localBuilder.positiveColor);
      paramMaterialDialog.positiveButton.setStackedSelector(paramMaterialDialog.getButtonSelector(DialogAction.POSITIVE, true));
      paramMaterialDialog.positiveButton.setDefaultSelector(paramMaterialDialog.getButtonSelector(DialogAction.POSITIVE, false));
      paramMaterialDialog.positiveButton.setTag(DialogAction.POSITIVE);
      paramMaterialDialog.positiveButton.setOnClickListener(paramMaterialDialog);
      paramMaterialDialog.positiveButton.setVisibility(0);
      localObject1 = paramMaterialDialog.negativeButton;
      paramMaterialDialog.setTypeface((TextView)localObject1, localBuilder.mediumFont);
      ((MDButton)localObject1).setAllCapsCompat(bool1);
      ((MDButton)localObject1).setText(localBuilder.negativeText);
      ((MDButton)localObject1).setTextColor(localBuilder.negativeColor);
      paramMaterialDialog.negativeButton.setStackedSelector(paramMaterialDialog.getButtonSelector(DialogAction.NEGATIVE, true));
      paramMaterialDialog.negativeButton.setDefaultSelector(paramMaterialDialog.getButtonSelector(DialogAction.NEGATIVE, false));
      paramMaterialDialog.negativeButton.setTag(DialogAction.NEGATIVE);
      paramMaterialDialog.negativeButton.setOnClickListener(paramMaterialDialog);
      paramMaterialDialog.negativeButton.setVisibility(0);
      localObject1 = paramMaterialDialog.neutralButton;
      paramMaterialDialog.setTypeface((TextView)localObject1, localBuilder.mediumFont);
      ((MDButton)localObject1).setAllCapsCompat(bool1);
      ((MDButton)localObject1).setText(localBuilder.neutralText);
      ((MDButton)localObject1).setTextColor(localBuilder.neutralColor);
      paramMaterialDialog.neutralButton.setStackedSelector(paramMaterialDialog.getButtonSelector(DialogAction.NEUTRAL, true));
      paramMaterialDialog.neutralButton.setDefaultSelector(paramMaterialDialog.getButtonSelector(DialogAction.NEUTRAL, false));
      paramMaterialDialog.neutralButton.setTag(DialogAction.NEUTRAL);
      paramMaterialDialog.neutralButton.setOnClickListener(paramMaterialDialog);
      paramMaterialDialog.neutralButton.setVisibility(0);
      if (localBuilder.listCallbackMultiChoice != null) {
        paramMaterialDialog.selectedIndicesList = new ArrayList();
      }
      if (paramMaterialDialog.recyclerView != null)
      {
        if (localBuilder.adapter != null) {
          break label2223;
        }
        if (localBuilder.listCallbackSingleChoice == null) {
          break label2162;
        }
        paramMaterialDialog.listType = MaterialDialog.ListType.SINGLE;
        localBuilder.adapter = new DefaultRvAdapter(paramMaterialDialog, MaterialDialog.ListType.getLayoutForType(paramMaterialDialog.listType));
      }
      setupProgressDialog(paramMaterialDialog);
      setupInputDialog(paramMaterialDialog);
      if (localBuilder.customView != null)
      {
        ((MDRootLayout)paramMaterialDialog.view.findViewById(R.id.md_root)).noTitleNoPadding();
        localFrameLayout = (FrameLayout)paramMaterialDialog.view.findViewById(R.id.md_customViewFrame);
        paramMaterialDialog.customViewFrame = localFrameLayout;
        localObject2 = localBuilder.customView;
        if (((View)localObject2).getParent() != null) {
          ((ViewGroup)((View)localObject2).getParent()).removeView((View)localObject2);
        }
        localObject1 = localObject2;
        if (localBuilder.wrapCustomViewInScroll)
        {
          Resources localResources = paramMaterialDialog.getContext().getResources();
          i = localResources.getDimensionPixelSize(R.dimen.md_dialog_frame_margin);
          localObject1 = new ScrollView(paramMaterialDialog.getContext());
          j = localResources.getDimensionPixelSize(R.dimen.md_content_padding_top);
          k = localResources.getDimensionPixelSize(R.dimen.md_content_padding_bottom);
          ((ScrollView)localObject1).setClipToPadding(false);
          if (!(localObject2 instanceof EditText)) {
            break label2251;
          }
          ((ScrollView)localObject1).setPadding(i, j, i, k);
        }
      }
    }
    for (;;)
    {
      ((ScrollView)localObject1).addView((View)localObject2, new FrameLayout.LayoutParams(-1, -2));
      localFrameLayout.addView((View)localObject1, new ViewGroup.LayoutParams(-1, -2));
      if (localBuilder.showListener != null) {
        paramMaterialDialog.setOnShowListener(localBuilder.showListener);
      }
      if (localBuilder.cancelListener != null) {
        paramMaterialDialog.setOnCancelListener(localBuilder.cancelListener);
      }
      if (localBuilder.dismissListener != null) {
        paramMaterialDialog.setOnDismissListener(localBuilder.dismissListener);
      }
      if (localBuilder.keyListener != null) {
        paramMaterialDialog.setOnKeyListener(localBuilder.keyListener);
      }
      paramMaterialDialog.setOnShowListenerInternal();
      paramMaterialDialog.invalidateList();
      paramMaterialDialog.setViewInternal(paramMaterialDialog.view);
      paramMaterialDialog.checkIfListInitScroll();
      localObject1 = paramMaterialDialog.getWindow().getWindowManager().getDefaultDisplay();
      localObject2 = new Point();
      ((Display)localObject1).getSize((Point)localObject2);
      int n = ((Point)localObject2).x;
      int m = ((Point)localObject2).y;
      i = localBuilder.context.getResources().getDimensionPixelSize(R.dimen.md_dialog_vertical_margin);
      k = localBuilder.context.getResources().getDimensionPixelSize(R.dimen.md_dialog_horizontal_margin);
      j = localBuilder.context.getResources().getDimensionPixelSize(R.dimen.md_dialog_max_width);
      paramMaterialDialog.view.setMaxHeight(m - i * 2);
      localObject1 = new WindowManager.LayoutParams();
      ((WindowManager.LayoutParams)localObject1).copyFrom(paramMaterialDialog.getWindow().getAttributes());
      ((WindowManager.LayoutParams)localObject1).width = Math.min(j, n - k * 2);
      paramMaterialDialog.getWindow().setAttributes((WindowManager.LayoutParams)localObject1);
      return;
      i = 8;
      break;
      label2033:
      i = 8;
      break label550;
      label2039:
      i = 8;
      break label572;
      label2045:
      localObject1 = DialogUtils.resolveDrawable(localBuilder.context, R.attr.md_icon);
      if (localObject1 != null)
      {
        paramMaterialDialog.icon.setVisibility(0);
        paramMaterialDialog.icon.setImageDrawable((Drawable)localObject1);
        break label678;
      }
      paramMaterialDialog.icon.setVisibility(8);
      break label678;
      label2095:
      paramMaterialDialog.title.setText(localBuilder.title);
      paramMaterialDialog.titleFrame.setVisibility(0);
      break label911;
      label2118:
      paramMaterialDialog.content.setLinkTextColor(localBuilder.linkColor);
      break label983;
      label2133:
      paramMaterialDialog.content.setVisibility(8);
      break label1061;
      label2145:
      bool1 = DialogUtils.resolveBoolean(localBuilder.context, R.attr.textAllCaps, true);
      break label1222;
      label2162:
      if (localBuilder.listCallbackMultiChoice != null)
      {
        paramMaterialDialog.listType = MaterialDialog.ListType.MULTI;
        if (localBuilder.selectedIndices == null) {
          break label1571;
        }
        paramMaterialDialog.selectedIndicesList = new ArrayList(Arrays.asList(localBuilder.selectedIndices));
        localBuilder.selectedIndices = null;
        break label1571;
      }
      paramMaterialDialog.listType = MaterialDialog.ListType.REGULAR;
      break label1571;
      label2223:
      if (!(localBuilder.adapter instanceof MDAdapter)) {
        break label1591;
      }
      ((MDAdapter)localBuilder.adapter).setDialog(paramMaterialDialog);
      break label1591;
      label2251:
      ((ScrollView)localObject1).setPadding(0, j, 0, k);
      ((View)localObject2).setPadding(i, 0, i, 0);
    }
  }
  
  private static void setupInputDialog(MaterialDialog paramMaterialDialog)
  {
    MaterialDialog.Builder localBuilder = paramMaterialDialog.builder;
    paramMaterialDialog.input = ((EditText)paramMaterialDialog.view.findViewById(16908297));
    if (paramMaterialDialog.input == null) {}
    for (;;)
    {
      return;
      paramMaterialDialog.setTypeface(paramMaterialDialog.input, localBuilder.regularFont);
      if (localBuilder.inputPrefill != null) {
        paramMaterialDialog.input.setText(localBuilder.inputPrefill);
      }
      paramMaterialDialog.setInternalInputCallback();
      paramMaterialDialog.input.setHint(localBuilder.inputHint);
      paramMaterialDialog.input.setSingleLine();
      paramMaterialDialog.input.setTextColor(localBuilder.contentColor);
      paramMaterialDialog.input.setHintTextColor(DialogUtils.adjustAlpha(localBuilder.contentColor, 0.3F));
      MDTintHelper.setTint(paramMaterialDialog.input, paramMaterialDialog.builder.widgetColor);
      if (localBuilder.inputType != -1)
      {
        paramMaterialDialog.input.setInputType(localBuilder.inputType);
        if ((localBuilder.inputType != 144) && ((localBuilder.inputType & 0x80) == 128)) {
          paramMaterialDialog.input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
      }
      paramMaterialDialog.inputMinMax = ((TextView)paramMaterialDialog.view.findViewById(R.id.md_minMax));
      if ((localBuilder.inputMinLength > 0) || (localBuilder.inputMaxLength > -1))
      {
        int i = paramMaterialDialog.input.getText().toString().length();
        if (!localBuilder.inputAllowEmpty) {}
        for (boolean bool = true;; bool = false)
        {
          paramMaterialDialog.invalidateInputMinMaxIndicator(i, bool);
          break;
        }
      }
      paramMaterialDialog.inputMinMax.setVisibility(8);
      paramMaterialDialog.inputMinMax = null;
    }
  }
  
  private static void setupProgressDialog(MaterialDialog paramMaterialDialog)
  {
    Object localObject1 = paramMaterialDialog.builder;
    if ((((MaterialDialog.Builder)localObject1).indeterminateProgress) || (((MaterialDialog.Builder)localObject1).progress > -2))
    {
      paramMaterialDialog.progressBar = ((ProgressBar)paramMaterialDialog.view.findViewById(16908301));
      if (paramMaterialDialog.progressBar != null) {}
    }
    label104:
    label139:
    label452:
    label457:
    label469:
    label475:
    for (;;)
    {
      return;
      Object localObject2;
      boolean bool;
      if (Build.VERSION.SDK_INT >= 14) {
        if (((MaterialDialog.Builder)localObject1).indeterminateProgress) {
          if (((MaterialDialog.Builder)localObject1).indeterminateIsHorizontalProgress)
          {
            localObject2 = new IndeterminateHorizontalProgressDrawable(((MaterialDialog.Builder)localObject1).getContext());
            ((IndeterminateHorizontalProgressDrawable)localObject2).setTint(((MaterialDialog.Builder)localObject1).widgetColor);
            paramMaterialDialog.progressBar.setProgressDrawable((Drawable)localObject2);
            paramMaterialDialog.progressBar.setIndeterminateDrawable((Drawable)localObject2);
            if ((!((MaterialDialog.Builder)localObject1).indeterminateProgress) || (((MaterialDialog.Builder)localObject1).indeterminateIsHorizontalProgress))
            {
              localObject2 = paramMaterialDialog.progressBar;
              if ((!((MaterialDialog.Builder)localObject1).indeterminateProgress) || (!((MaterialDialog.Builder)localObject1).indeterminateIsHorizontalProgress)) {
                break label452;
              }
              bool = true;
              ((ProgressBar)localObject2).setIndeterminate(bool);
              paramMaterialDialog.progressBar.setProgress(0);
              paramMaterialDialog.progressBar.setMax(((MaterialDialog.Builder)localObject1).progressMax);
              paramMaterialDialog.progressLabel = ((TextView)paramMaterialDialog.view.findViewById(R.id.md_label));
              if (paramMaterialDialog.progressLabel != null)
              {
                paramMaterialDialog.progressLabel.setTextColor(((MaterialDialog.Builder)localObject1).contentColor);
                paramMaterialDialog.setTypeface(paramMaterialDialog.progressLabel, ((MaterialDialog.Builder)localObject1).mediumFont);
                paramMaterialDialog.progressLabel.setText(((MaterialDialog.Builder)localObject1).progressPercentFormat.format(0L));
              }
              paramMaterialDialog.progressMinMax = ((TextView)paramMaterialDialog.view.findViewById(R.id.md_minMax));
              if (paramMaterialDialog.progressMinMax == null) {
                break label469;
              }
              paramMaterialDialog.progressMinMax.setTextColor(((MaterialDialog.Builder)localObject1).contentColor);
              paramMaterialDialog.setTypeface(paramMaterialDialog.progressMinMax, ((MaterialDialog.Builder)localObject1).regularFont);
              if (!((MaterialDialog.Builder)localObject1).showMinMax) {
                break label457;
              }
              paramMaterialDialog.progressMinMax.setVisibility(0);
              paramMaterialDialog.progressMinMax.setText(String.format(((MaterialDialog.Builder)localObject1).progressNumberFormat, new Object[] { Integer.valueOf(0), Integer.valueOf(((MaterialDialog.Builder)localObject1).progressMax) }));
              localObject1 = (ViewGroup.MarginLayoutParams)paramMaterialDialog.progressBar.getLayoutParams();
              ((ViewGroup.MarginLayoutParams)localObject1).leftMargin = 0;
              ((ViewGroup.MarginLayoutParams)localObject1).rightMargin = 0;
            }
          }
        }
      }
      for (;;)
      {
        if (paramMaterialDialog.progressBar == null) {
          break label475;
        }
        fixCanvasScalingWhenHardwareAccelerated(paramMaterialDialog.progressBar);
        break;
        localObject2 = new IndeterminateCircularProgressDrawable(((MaterialDialog.Builder)localObject1).getContext());
        ((IndeterminateCircularProgressDrawable)localObject2).setTint(((MaterialDialog.Builder)localObject1).widgetColor);
        paramMaterialDialog.progressBar.setProgressDrawable((Drawable)localObject2);
        paramMaterialDialog.progressBar.setIndeterminateDrawable((Drawable)localObject2);
        break label104;
        localObject2 = new HorizontalProgressDrawable(((MaterialDialog.Builder)localObject1).getContext());
        ((HorizontalProgressDrawable)localObject2).setTint(((MaterialDialog.Builder)localObject1).widgetColor);
        paramMaterialDialog.progressBar.setProgressDrawable((Drawable)localObject2);
        paramMaterialDialog.progressBar.setIndeterminateDrawable((Drawable)localObject2);
        break label104;
        MDTintHelper.setTint(paramMaterialDialog.progressBar, ((MaterialDialog.Builder)localObject1).widgetColor);
        break label104;
        bool = false;
        break label139;
        paramMaterialDialog.progressMinMax.setVisibility(8);
        continue;
        ((MaterialDialog.Builder)localObject1).showMinMax = false;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\DialogInit.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */