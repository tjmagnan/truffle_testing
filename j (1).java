package com.testfairy.j;

import android.util.Log;
import com.testfairy.e;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class a
  extends c
{
  protected static final String a = "testfairy-logfile-reader";
  public static final String b = "/dev/log/main";
  private static final int j = 20;
  private static final int k = 4096;
  private static final String[] o = { "V", "V", "V", "D", "I", "W", "E", "A" };
  private FileInputStream l;
  private int m = 0;
  private boolean n = false;
  private TimerTask p = new TimerTask()
  {
    public void run()
    {
      if (a.a(a.this) == 0)
      {
        Log.d(e.a, "Logfile watchdog woke up, and no logs were read");
        a.this.a();
      }
      try
      {
        a.b(a.this).close();
        if (!a.c(a.this))
        {
          a.a(a.this, true);
          a.this.d.a();
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
    }
  };
  
  public a(b paramb)
  {
    super(paramb);
    setName("testfairy-logfile-reader");
  }
  
  private int b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2) {
      if (paramArrayOfByte[paramInt1] != 0) {}
    }
    for (;;)
    {
      return paramInt1;
      paramInt1++;
      break;
      paramInt1 = -1;
    }
  }
  
  c.a a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    c.a locala = new c.a();
    int i = paramArrayOfByte[4] & 0xFF | (paramArrayOfByte[5] & 0xFF) << 8 | (paramArrayOfByte[6] & 0xFF) << 16 | (paramArrayOfByte[7] & 0xFF) << 24;
    if (i != paramInt2) {}
    for (paramArrayOfByte = null;; paramArrayOfByte = locala)
    {
      return paramArrayOfByte;
      locala.e = String.valueOf(i);
      locala.a = ((paramArrayOfByte[12] & 0xFF | (paramArrayOfByte[13] & 0xFF) << 8 | (paramArrayOfByte[14] & 0xFF) << 16 | (paramArrayOfByte[15] & 0xFF) << 24) * 1000L + (paramArrayOfByte[16] & 0xFF | (paramArrayOfByte[17] & 0xFF) << 8 | (paramArrayOfByte[18] & 0xFF) << 16 | (paramArrayOfByte[19] & 0xFF) << 24) / 1000000L);
      paramInt2 = paramArrayOfByte[0] & 0xFF | (paramArrayOfByte[1] & 0xFF) << 8;
      int i2 = paramArrayOfByte[20];
      int i1 = b(paramArrayOfByte, 21, paramInt2 + 20);
      i = b(paramArrayOfByte, i1 + 1, paramInt2 + 20);
      paramInt2 = i;
      if (i == -1) {
        paramInt2 = paramInt1 - 1;
      }
      paramInt1 = paramInt2;
      if (paramInt2 > 0)
      {
        paramInt1 = paramInt2;
        if (paramArrayOfByte[(paramInt2 - 1)] == 10) {
          paramInt1 = paramInt2 - 1;
        }
      }
      locala.c = new String(paramArrayOfByte, 21, i1 - 21);
      locala.d = new String(paramArrayOfByte, i1 + 1, paramInt1 - (i1 + 1));
      paramArrayOfByte = "D";
      if (i2 <= 7) {
        paramArrayOfByte = o[i2];
      }
      locala.b = paramArrayOfByte;
    }
  }
  
  public void run()
  {
    Object localObject2 = null;
    for (;;)
    {
      try
      {
        arrayOfByte = new byte['က'];
        localObject1 = new java/io/FileInputStream;
        ((FileInputStream)localObject1).<init>("/dev/log/main");
        this.l = ((FileInputStream)localObject1);
        localObject1 = new java/util/Timer;
        ((Timer)localObject1).<init>("testfairy-log-watchdog");
        try
        {
          ((Timer)localObject1).schedule(this.p, 2000L);
          if (!this.g) {
            if (this.f)
            {
              Thread.sleep(1000L);
              continue;
              if (this.l == null) {}
            }
          }
        }
        catch (Throwable localThrowable2) {}
      }
      catch (Throwable localThrowable1)
      {
        byte[] arrayOfByte;
        Object localObject1;
        int i;
        Object localObject3;
        IOException localIOException1 = localIOException3;
        continue;
      }
      try
      {
        this.l.close();
        if (localObject1 != null)
        {
          this.p.cancel();
          ((Timer)localObject1).cancel();
        }
        if (!this.n)
        {
          this.n = true;
          this.d.a();
        }
        return;
        i = this.l.read(arrayOfByte);
        if (i < 0)
        {
          Log.e(e.a, "Can't read from Android log file ");
          localObject3 = this.l;
          if (localObject3 == null) {}
        }
        try
        {
          this.l.close();
          this.p.cancel();
          ((Timer)localObject1).cancel();
          continue;
          this.m += 1;
          localObject3 = a(arrayOfByte, i, this.i);
          if (localObject3 == null) {
            continue;
          }
          this.d.a(((c.a)localObject3).a, ((c.a)localObject3).b, ((c.a)localObject3).c, ((c.a)localObject3).d);
        }
        catch (IOException localIOException2)
        {
          Log.e(e.a, "IOException", localIOException2);
        }
      }
      catch (IOException localIOException3)
      {
        Log.e(e.a, "IOException", localIOException3);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\j\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */