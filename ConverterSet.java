package org.joda.time.convert;

class ConverterSet
{
  private final Converter[] iConverters;
  private Entry[] iSelectEntries;
  
  ConverterSet(Converter[] paramArrayOfConverter)
  {
    this.iConverters = paramArrayOfConverter;
    this.iSelectEntries = new Entry[16];
  }
  
  private static Converter selectSlow(ConverterSet paramConverterSet, Class<?> paramClass)
  {
    Object localObject4 = null;
    Object localObject2 = paramConverterSet.iConverters;
    int i = localObject2.length;
    int j = i;
    Object localObject1 = paramConverterSet;
    int k = j - 1;
    Class localClass;
    if (k >= 0)
    {
      paramConverterSet = localObject2[k];
      localClass = paramConverterSet.getSupportedType();
      if (localClass != paramClass) {}
    }
    for (;;)
    {
      return paramConverterSet;
      Object localObject3;
      if (localClass != null)
      {
        j = i;
        localObject3 = localObject2;
        paramConverterSet = (ConverterSet)localObject1;
        if (paramClass != null)
        {
          j = i;
          localObject3 = localObject2;
          paramConverterSet = (ConverterSet)localObject1;
          if (localClass.isAssignableFrom(paramClass)) {}
        }
      }
      else
      {
        paramConverterSet = ((ConverterSet)localObject1).remove(k, null);
        localObject3 = paramConverterSet.iConverters;
        j = localObject3.length;
      }
      i = j;
      localObject2 = localObject3;
      localObject1 = paramConverterSet;
      j = k;
      break;
      paramConverterSet = (ConverterSet)localObject4;
      if (paramClass != null)
      {
        paramConverterSet = (ConverterSet)localObject4;
        if (i != 0) {
          if (i == 1)
          {
            paramConverterSet = localObject2[0];
          }
          else
          {
            paramConverterSet = (ConverterSet)localObject1;
            j = i;
            localObject1 = localObject2;
            for (;;)
            {
              i--;
              if (i < 0) {
                break;
              }
              localObject2 = localObject1[i].getSupportedType();
              k = j;
              for (;;)
              {
                int m = k - 1;
                if (m < 0) {
                  break;
                }
                k = m;
                if (m != i)
                {
                  k = m;
                  if (localObject1[m].getSupportedType().isAssignableFrom((Class)localObject2))
                  {
                    paramConverterSet = paramConverterSet.remove(m, null);
                    localObject1 = paramConverterSet.iConverters;
                    j = localObject1.length;
                    i = j - 1;
                    k = m;
                  }
                }
              }
            }
            if (j != 1) {
              break label265;
            }
            paramConverterSet = localObject1[0];
          }
        }
      }
    }
    label265:
    localObject2 = new StringBuilder();
    ((StringBuilder)localObject2).append("Unable to find best converter for type \"");
    ((StringBuilder)localObject2).append(paramClass.getName());
    ((StringBuilder)localObject2).append("\" from remaining set: ");
    i = 0;
    if (i < j)
    {
      paramConverterSet = localObject1[i];
      paramClass = paramConverterSet.getSupportedType();
      ((StringBuilder)localObject2).append(paramConverterSet.getClass().getName());
      ((StringBuilder)localObject2).append('[');
      if (paramClass == null) {}
      for (paramConverterSet = null;; paramConverterSet = paramClass.getName())
      {
        ((StringBuilder)localObject2).append(paramConverterSet);
        ((StringBuilder)localObject2).append("], ");
        i++;
        break;
      }
    }
    throw new IllegalStateException(((StringBuilder)localObject2).toString());
  }
  
  ConverterSet add(Converter paramConverter, Converter[] paramArrayOfConverter)
  {
    Converter[] arrayOfConverter1 = this.iConverters;
    int k = arrayOfConverter1.length;
    int i = 0;
    Converter localConverter;
    if (i < k)
    {
      localConverter = arrayOfConverter1[i];
      if (paramConverter.equals(localConverter))
      {
        paramConverter = this;
        if (paramArrayOfConverter != null)
        {
          paramArrayOfConverter[0] = null;
          paramConverter = this;
        }
      }
    }
    for (;;)
    {
      return paramConverter;
      Converter[] arrayOfConverter2;
      if (paramConverter.getSupportedType() == localConverter.getSupportedType())
      {
        arrayOfConverter2 = new Converter[k];
        int j = 0;
        if (j < k)
        {
          if (j != i) {
            arrayOfConverter2[j] = arrayOfConverter1[j];
          }
          for (;;)
          {
            j++;
            break;
            arrayOfConverter2[j] = paramConverter;
          }
        }
        if (paramArrayOfConverter != null) {
          paramArrayOfConverter[0] = localConverter;
        }
        paramConverter = new ConverterSet(arrayOfConverter2);
      }
      else
      {
        i++;
        break;
        arrayOfConverter2 = new Converter[k + 1];
        System.arraycopy(arrayOfConverter1, 0, arrayOfConverter2, 0, k);
        arrayOfConverter2[k] = paramConverter;
        if (paramArrayOfConverter != null) {
          paramArrayOfConverter[0] = null;
        }
        paramConverter = new ConverterSet(arrayOfConverter2);
      }
    }
  }
  
  void copyInto(Converter[] paramArrayOfConverter)
  {
    System.arraycopy(this.iConverters, 0, paramArrayOfConverter, 0, this.iConverters.length);
  }
  
  ConverterSet remove(int paramInt, Converter[] paramArrayOfConverter)
  {
    Converter[] arrayOfConverter = this.iConverters;
    int m = arrayOfConverter.length;
    if (paramInt >= m) {
      throw new IndexOutOfBoundsException();
    }
    if (paramArrayOfConverter != null) {
      paramArrayOfConverter[0] = arrayOfConverter[paramInt];
    }
    paramArrayOfConverter = new Converter[m - 1];
    int j = 0;
    int i = 0;
    if (j < m)
    {
      if (j == paramInt) {
        break label93;
      }
      int k = i + 1;
      paramArrayOfConverter[i] = arrayOfConverter[j];
      i = k;
    }
    label93:
    for (;;)
    {
      j++;
      break;
      return new ConverterSet(paramArrayOfConverter);
    }
  }
  
  ConverterSet remove(Converter paramConverter, Converter[] paramArrayOfConverter)
  {
    Converter[] arrayOfConverter = this.iConverters;
    int j = arrayOfConverter.length;
    int i = 0;
    if (i < j) {
      if (paramConverter.equals(arrayOfConverter[i])) {
        paramConverter = remove(i, paramArrayOfConverter);
      }
    }
    for (;;)
    {
      return paramConverter;
      i++;
      break;
      paramConverter = this;
      if (paramArrayOfConverter != null)
      {
        paramArrayOfConverter[0] = null;
        paramConverter = this;
      }
    }
  }
  
  Converter select(Class<?> paramClass)
    throws IllegalStateException
  {
    Object localObject1 = this.iSelectEntries;
    int m = localObject1.length;
    int i;
    if (paramClass == null) {
      i = 0;
    }
    label139:
    label244:
    for (;;)
    {
      Converter localConverter = localObject1[i];
      if (localConverter != null) {
        if (localConverter.iType == paramClass) {
          paramClass = localConverter.iConverter;
        }
      }
      for (;;)
      {
        return paramClass;
        i = paramClass.hashCode() & m - 1;
        break;
        i++;
        if (i < m) {
          break label244;
        }
        i = 0;
        break;
        localConverter = selectSlow(this, paramClass);
        Object localObject2 = new Entry(paramClass, localConverter);
        paramClass = (Entry[])((Entry[])localObject1).clone();
        paramClass[i] = localObject2;
        for (i = 0;; i++)
        {
          if (i >= m) {
            break label139;
          }
          if (paramClass[i] == null)
          {
            this.iSelectEntries = paramClass;
            paramClass = localConverter;
            break;
          }
        }
        int n = m << 1;
        localObject2 = new Entry[n];
        for (int j = 0; j < m; j++)
        {
          Object localObject3 = paramClass[j];
          localObject1 = ((Entry)localObject3).iType;
          if (localObject1 == null) {
            i = 0;
          }
          while (localObject2[i] != null)
          {
            int k = i + 1;
            i = k;
            if (k >= n)
            {
              i = 0;
              continue;
              i = localObject1.hashCode() & n - 1;
            }
          }
          localObject2[i] = localObject3;
        }
        this.iSelectEntries = ((Entry[])localObject2);
        paramClass = localConverter;
      }
    }
  }
  
  int size()
  {
    return this.iConverters.length;
  }
  
  static class Entry
  {
    final Converter iConverter;
    final Class<?> iType;
    
    Entry(Class<?> paramClass, Converter paramConverter)
    {
      this.iType = paramClass;
      this.iConverter = paramConverter;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\ConverterSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */