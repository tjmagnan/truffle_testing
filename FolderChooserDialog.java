package com.afollestad.materialdialogs.folderselector;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.InputCallback;
import com.afollestad.materialdialogs.MaterialDialog.ListCallback;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.afollestad.materialdialogs.commons.R.string;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FolderChooserDialog
  extends DialogFragment
  implements MaterialDialog.ListCallback
{
  private static final String DEFAULT_TAG = "[MD_FOLDER_SELECTOR]";
  private FolderCallback callback;
  private boolean canGoUp = false;
  private File[] parentContents;
  private File parentFolder;
  
  private void checkIfCanGoUp()
  {
    for (boolean bool = true;; bool = false) {
      try
      {
        if (this.parentFolder.getPath().split("/").length > 1)
        {
          this.canGoUp = bool;
          return;
        }
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        for (;;)
        {
          this.canGoUp = false;
        }
      }
    }
  }
  
  private void createNewFolder()
  {
    new MaterialDialog.Builder(getActivity()).title(getBuilder().newFolderButton).input(0, 0, false, new MaterialDialog.InputCallback()
    {
      public void onInput(@NonNull MaterialDialog paramAnonymousMaterialDialog, CharSequence paramAnonymousCharSequence)
      {
        paramAnonymousMaterialDialog = new File(FolderChooserDialog.this.parentFolder, paramAnonymousCharSequence.toString());
        if (!paramAnonymousMaterialDialog.mkdir())
        {
          paramAnonymousMaterialDialog = "Unable to create folder " + paramAnonymousMaterialDialog.getAbsolutePath() + ", make sure you have the WRITE_EXTERNAL_STORAGE permission or root permissions.";
          Toast.makeText(FolderChooserDialog.this.getActivity(), paramAnonymousMaterialDialog, 1).show();
        }
        for (;;)
        {
          return;
          FolderChooserDialog.this.reload();
        }
      }
    }).show();
  }
  
  @NonNull
  private Builder getBuilder()
  {
    return (Builder)getArguments().getSerializable("builder");
  }
  
  private void reload()
  {
    this.parentContents = listFiles();
    MaterialDialog localMaterialDialog = (MaterialDialog)getDialog();
    localMaterialDialog.setTitle(this.parentFolder.getAbsolutePath());
    getArguments().putString("current_path", this.parentFolder.getAbsolutePath());
    localMaterialDialog.setItems((CharSequence[])getContentsArray());
  }
  
  String[] getContentsArray()
  {
    int i = 1;
    Object localObject;
    if (this.parentContents == null) {
      if (this.canGoUp)
      {
        localObject = new String[1];
        localObject[0] = getBuilder().goUpLabel;
      }
    }
    String[] arrayOfString;
    label82:
    do
    {
      for (;;)
      {
        return (String[])localObject;
        localObject = new String[0];
      }
      j = this.parentContents.length;
      if (!this.canGoUp) {
        break;
      }
      arrayOfString = new String[i + j];
      if (this.canGoUp) {
        arrayOfString[0] = getBuilder().goUpLabel;
      }
      i = 0;
      localObject = arrayOfString;
    } while (i >= this.parentContents.length);
    if (this.canGoUp) {}
    for (int j = i + 1;; j = i)
    {
      arrayOfString[j] = this.parentContents[i].getName();
      i++;
      break label82;
      i = 0;
      break;
    }
  }
  
  File[] listFiles()
  {
    Object localObject = null;
    File[] arrayOfFile = this.parentFolder.listFiles();
    ArrayList localArrayList = new ArrayList();
    if (arrayOfFile != null)
    {
      int j = arrayOfFile.length;
      for (int i = 0; i < j; i++)
      {
        localObject = arrayOfFile[i];
        if (((File)localObject).isDirectory()) {
          localArrayList.add(localObject);
        }
      }
      Collections.sort(localArrayList, new FolderSorter(null));
      localObject = (File[])localArrayList.toArray(new File[localArrayList.size()]);
    }
    return (File[])localObject;
  }
  
  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    this.callback = ((FolderCallback)paramActivity);
  }
  
  @NonNull
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    if ((Build.VERSION.SDK_INT >= 23) && (ActivityCompat.checkSelfPermission(getActivity(), "android.permission.READ_EXTERNAL_STORAGE") != 0)) {}
    for (paramBundle = new MaterialDialog.Builder(getActivity()).title(R.string.md_error_label).content(R.string.md_storage_perm_error).positiveText(17039370).build();; paramBundle = paramBundle.build())
    {
      return paramBundle;
      if ((getArguments() == null) || (!getArguments().containsKey("builder"))) {
        throw new IllegalStateException("You must create a FolderChooserDialog using the Builder.");
      }
      if (!getArguments().containsKey("current_path")) {
        getArguments().putString("current_path", getBuilder().initialPath);
      }
      this.parentFolder = new File(getArguments().getString("current_path"));
      checkIfCanGoUp();
      this.parentContents = listFiles();
      paramBundle = new MaterialDialog.Builder(getActivity()).typeface(getBuilder().mediumFont, getBuilder().regularFont).title(this.parentFolder.getAbsolutePath()).items((CharSequence[])getContentsArray()).itemsCallback(this).onPositive(new MaterialDialog.SingleButtonCallback()
      {
        public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
        {
          paramAnonymousMaterialDialog.dismiss();
          FolderChooserDialog.this.callback.onFolderSelection(FolderChooserDialog.this, FolderChooserDialog.this.parentFolder);
        }
      }).onNegative(new MaterialDialog.SingleButtonCallback()
      {
        public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
        {
          paramAnonymousMaterialDialog.dismiss();
        }
      }).autoDismiss(false).positiveText(getBuilder().chooseButton).negativeText(getBuilder().cancelButton);
      if (getBuilder().allowNewFolder)
      {
        paramBundle.neutralText(getBuilder().newFolderButton);
        paramBundle.onNeutral(new MaterialDialog.SingleButtonCallback()
        {
          public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
          {
            FolderChooserDialog.this.createNewFolder();
          }
        });
      }
      if ("/".equals(getBuilder().initialPath)) {
        this.canGoUp = false;
      }
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    if (this.callback != null) {
      this.callback.onFolderChooserDismissed(this);
    }
  }
  
  public void onSelection(MaterialDialog paramMaterialDialog, View paramView, int paramInt, CharSequence paramCharSequence)
  {
    boolean bool = true;
    if ((this.canGoUp) && (paramInt == 0))
    {
      this.parentFolder = this.parentFolder.getParentFile();
      if (this.parentFolder.getAbsolutePath().equals("/storage/emulated")) {
        this.parentFolder = this.parentFolder.getParentFile();
      }
      if (this.parentFolder.getParent() != null) {
        this.canGoUp = bool;
      }
    }
    for (;;)
    {
      reload();
      return;
      bool = false;
      break;
      paramMaterialDialog = this.parentContents;
      int i = paramInt;
      if (this.canGoUp) {
        i = paramInt - 1;
      }
      this.parentFolder = paramMaterialDialog[i];
      this.canGoUp = true;
      if (this.parentFolder.getAbsolutePath().equals("/storage/emulated")) {
        this.parentFolder = Environment.getExternalStorageDirectory();
      }
    }
  }
  
  public void show(FragmentActivity paramFragmentActivity)
  {
    String str = getBuilder().tag;
    Fragment localFragment = paramFragmentActivity.getSupportFragmentManager().findFragmentByTag(str);
    if (localFragment != null)
    {
      ((DialogFragment)localFragment).dismiss();
      paramFragmentActivity.getSupportFragmentManager().beginTransaction().remove(localFragment).commit();
    }
    show(paramFragmentActivity.getSupportFragmentManager(), str);
  }
  
  public static class Builder
    implements Serializable
  {
    boolean allowNewFolder;
    @StringRes
    int cancelButton;
    @StringRes
    int chooseButton;
    @NonNull
    final transient AppCompatActivity context;
    String goUpLabel;
    String initialPath;
    @Nullable
    String mediumFont;
    @StringRes
    int newFolderButton;
    @Nullable
    String regularFont;
    String tag;
    
    public <ActivityType extends AppCompatActivity,  extends FolderChooserDialog.FolderCallback> Builder(@NonNull ActivityType paramActivityType)
    {
      this.context = paramActivityType;
      this.chooseButton = R.string.md_choose_label;
      this.cancelButton = 17039360;
      this.goUpLabel = "...";
      this.initialPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    }
    
    @NonNull
    public Builder allowNewFolder(boolean paramBoolean, @StringRes int paramInt)
    {
      this.allowNewFolder = paramBoolean;
      int i = paramInt;
      if (paramInt == 0) {
        i = R.string.new_folder;
      }
      this.newFolderButton = i;
      return this;
    }
    
    @NonNull
    public FolderChooserDialog build()
    {
      FolderChooserDialog localFolderChooserDialog = new FolderChooserDialog();
      Bundle localBundle = new Bundle();
      localBundle.putSerializable("builder", this);
      localFolderChooserDialog.setArguments(localBundle);
      return localFolderChooserDialog;
    }
    
    @NonNull
    public Builder cancelButton(@StringRes int paramInt)
    {
      this.cancelButton = paramInt;
      return this;
    }
    
    @NonNull
    public Builder chooseButton(@StringRes int paramInt)
    {
      this.chooseButton = paramInt;
      return this;
    }
    
    @NonNull
    public Builder goUpLabel(String paramString)
    {
      this.goUpLabel = paramString;
      return this;
    }
    
    @NonNull
    public Builder initialPath(@Nullable String paramString)
    {
      String str = paramString;
      if (paramString == null) {
        str = File.separator;
      }
      this.initialPath = str;
      return this;
    }
    
    @NonNull
    public FolderChooserDialog show()
    {
      FolderChooserDialog localFolderChooserDialog = build();
      localFolderChooserDialog.show(this.context);
      return localFolderChooserDialog;
    }
    
    @NonNull
    public Builder tag(@Nullable String paramString)
    {
      String str = paramString;
      if (paramString == null) {
        str = "[MD_FOLDER_SELECTOR]";
      }
      this.tag = str;
      return this;
    }
    
    @NonNull
    public Builder typeface(@Nullable String paramString1, @Nullable String paramString2)
    {
      this.mediumFont = paramString1;
      this.regularFont = paramString2;
      return this;
    }
  }
  
  public static abstract interface FolderCallback
  {
    public abstract void onFolderChooserDismissed(@NonNull FolderChooserDialog paramFolderChooserDialog);
    
    public abstract void onFolderSelection(@NonNull FolderChooserDialog paramFolderChooserDialog, @NonNull File paramFile);
  }
  
  private static class FolderSorter
    implements Comparator<File>
  {
    public int compare(File paramFile1, File paramFile2)
    {
      return paramFile1.getName().compareTo(paramFile2.getName());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\folderselector\FolderChooserDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */