package tech.dcube.companion.managers.server.backend;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class GenericRequest<T>
  extends JsonRequest<T>
{
  private final Class<T> clazz;
  private final Gson gson = new Gson();
  private final Map<String, String> headers;
  private boolean muteRequest = false;
  
  private GenericRequest(int paramInt, Class<T> paramClass, String paramString1, String paramString2, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, Map<String, String> paramMap)
  {
    super(paramInt, paramString1, paramString2, paramListener, paramErrorListener);
    this.clazz = paramClass;
    this.headers = paramMap;
    configureRequest();
  }
  
  public GenericRequest(int paramInt, String paramString, Class<T> paramClass, Object paramObject, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener)
  {
    this(paramInt, paramClass, paramString, new Gson().toJson(paramObject), paramListener, paramErrorListener, new HashMap());
  }
  
  public GenericRequest(int paramInt, String paramString, Class<T> paramClass, Object paramObject, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, Map<String, String> paramMap)
  {
    this(paramInt, paramClass, paramString, new Gson().toJson(paramObject), paramListener, paramErrorListener, paramMap);
  }
  
  public GenericRequest(int paramInt, String paramString, Class<T> paramClass, Object paramObject, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, Map<String, String> paramMap, boolean paramBoolean)
  {
    this(paramInt, paramClass, paramString, new Gson().toJson(paramObject), paramListener, paramErrorListener, paramMap);
    this.muteRequest = paramBoolean;
  }
  
  public GenericRequest(int paramInt, String paramString, Class<T> paramClass, Object paramObject, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, boolean paramBoolean)
  {
    this(paramInt, paramClass, paramString, new Gson().toJson(paramObject), paramListener, paramErrorListener, new HashMap());
    this.muteRequest = paramBoolean;
  }
  
  public GenericRequest(int paramInt, String paramString1, Class<T> paramClass, String paramString2, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener)
  {
    this(paramInt, paramClass, paramString1, paramString2, paramListener, paramErrorListener, new HashMap());
  }
  
  public GenericRequest(int paramInt, String paramString1, Class<T> paramClass, String paramString2, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, boolean paramBoolean)
  {
    this(paramInt, paramClass, paramString1, paramString2, paramListener, paramErrorListener, new HashMap());
    this.muteRequest = paramBoolean;
  }
  
  public GenericRequest(String paramString, Class<T> paramClass, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener)
  {
    this(0, paramString, paramClass, "", paramListener, paramErrorListener);
  }
  
  public GenericRequest(String paramString, Class<T> paramClass, Response.Listener<T> paramListener, Response.ErrorListener paramErrorListener, Map<String, String> paramMap)
  {
    this(0, paramClass, paramString, "", paramListener, paramErrorListener, paramMap);
  }
  
  private void configureRequest() {}
  
  public Map<String, String> getHeaders()
    throws AuthFailureError
  {
    if (this.headers != null) {}
    for (Map localMap = this.headers;; localMap = super.getHeaders()) {
      return localMap;
    }
  }
  
  protected Response<T> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    Object localObject2 = null;
    Object localObject1;
    if (this.muteRequest)
    {
      localObject1 = localObject2;
      if (paramNetworkResponse.statusCode >= 200)
      {
        localObject1 = localObject2;
        if (paramNetworkResponse.statusCode <= 299) {
          localObject1 = Response.success(null, HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
        }
      }
    }
    for (;;)
    {
      return (Response<T>)localObject1;
      try
      {
        localObject1 = new java/lang/String;
        ((String)localObject1).<init>(paramNetworkResponse.data, HttpHeaderParser.parseCharset(paramNetworkResponse.headers));
        localObject1 = Response.success(this.gson.fromJson((String)localObject1, this.clazz), HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
      }
      catch (UnsupportedEncodingException paramNetworkResponse)
      {
        localObject1 = Response.error(new ParseError(paramNetworkResponse));
      }
      catch (JsonSyntaxException paramNetworkResponse)
      {
        localObject1 = Response.error(new ParseError(paramNetworkResponse));
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\backend\GenericRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */