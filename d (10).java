package com.testfairy.d;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import com.testfairy.d;

public class j
  extends View
{
  private d a;
  
  public j(Context paramContext)
  {
    super(paramContext);
  }
  
  public void a(d paramd)
  {
    this.a = paramd;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.a != null) {
      this.a.a(paramCanvas);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */