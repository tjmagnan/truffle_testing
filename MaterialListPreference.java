package com.afollestad.materialdialogs.prefs;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.preference.ListPreference;
import android.preference.Preference.BaseSavedState;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.ListCallbackSingleChoice;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import java.lang.reflect.Field;

public class MaterialListPreference
  extends ListPreference
{
  private Context context;
  private MaterialDialog dialog;
  
  public MaterialListPreference(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null);
  }
  
  public MaterialListPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet);
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet)
  {
    this.context = paramContext;
    PrefUtil.setLayoutResource(paramContext, this, paramAttributeSet);
    if (Build.VERSION.SDK_INT <= 10) {
      setWidgetLayoutResource(0);
    }
  }
  
  public Dialog getDialog()
  {
    return this.dialog;
  }
  
  public RecyclerView getRecyclerView()
  {
    if (getDialog() == null) {}
    for (RecyclerView localRecyclerView = null;; localRecyclerView = ((MaterialDialog)getDialog()).getRecyclerView()) {
      return localRecyclerView;
    }
  }
  
  public void onActivityDestroy()
  {
    super.onActivityDestroy();
    if ((this.dialog != null) && (this.dialog.isShowing())) {
      this.dialog.dismiss();
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    PrefUtil.unregisterOnActivityDestroyListener(this, this);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class))) {
      super.onRestoreInstanceState(paramParcelable);
    }
    for (;;)
    {
      return;
      paramParcelable = (SavedState)paramParcelable;
      super.onRestoreInstanceState(paramParcelable.getSuperState());
      if (paramParcelable.isDialogShowing) {
        showDialog(paramParcelable.dialogBundle);
      }
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Object localObject = super.onSaveInstanceState();
    Dialog localDialog = getDialog();
    if ((localDialog == null) || (!localDialog.isShowing())) {}
    for (;;)
    {
      return (Parcelable)localObject;
      localObject = new SavedState((Parcelable)localObject);
      ((SavedState)localObject).isDialogShowing = true;
      ((SavedState)localObject).dialogBundle = localDialog.onSaveInstanceState();
    }
  }
  
  public void setEntries(CharSequence[] paramArrayOfCharSequence)
  {
    super.setEntries(paramArrayOfCharSequence);
    if (this.dialog != null) {
      this.dialog.setItems(paramArrayOfCharSequence);
    }
  }
  
  protected void showDialog(Bundle paramBundle)
  {
    if ((getEntries() == null) || (getEntryValues() == null)) {
      throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
    }
    int i = findIndexOfValue(getValue());
    MaterialDialog.Builder localBuilder = new MaterialDialog.Builder(this.context).title(getDialogTitle()).icon(getDialogIcon()).dismissListener(this).onAny(new MaterialDialog.SingleButtonCallback()
    {
      public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
      {
        switch (MaterialListPreference.3.$SwitchMap$com$afollestad$materialdialogs$DialogAction[paramAnonymousDialogAction.ordinal()])
        {
        default: 
          MaterialListPreference.this.onClick(paramAnonymousMaterialDialog, -1);
        }
        for (;;)
        {
          return;
          MaterialListPreference.this.onClick(paramAnonymousMaterialDialog, -3);
          continue;
          MaterialListPreference.this.onClick(paramAnonymousMaterialDialog, -2);
        }
      }
    }).negativeText(getNegativeButtonText()).items(getEntries()).autoDismiss(true).itemsCallbackSingleChoice(i, new MaterialDialog.ListCallbackSingleChoice()
    {
      public boolean onSelection(MaterialDialog paramAnonymousMaterialDialog, View paramAnonymousView, int paramAnonymousInt, CharSequence paramAnonymousCharSequence)
      {
        MaterialListPreference.this.onClick(null, -1);
        if ((paramAnonymousInt >= 0) && (MaterialListPreference.this.getEntryValues() != null)) {}
        try
        {
          paramAnonymousMaterialDialog = ListPreference.class.getDeclaredField("mClickedDialogEntryIndex");
          paramAnonymousMaterialDialog.setAccessible(true);
          paramAnonymousMaterialDialog.set(MaterialListPreference.this, Integer.valueOf(paramAnonymousInt));
          return true;
        }
        catch (Exception paramAnonymousMaterialDialog)
        {
          for (;;)
          {
            paramAnonymousMaterialDialog.printStackTrace();
          }
        }
      }
    });
    View localView = onCreateDialogView();
    if (localView != null)
    {
      onBindDialogView(localView);
      localBuilder.customView(localView, false);
    }
    for (;;)
    {
      PrefUtil.registerOnActivityDestroyListener(this, this);
      this.dialog = localBuilder.build();
      if (paramBundle != null) {
        this.dialog.onRestoreInstanceState(paramBundle);
      }
      onClick(this.dialog, -2);
      this.dialog.show();
      return;
      localBuilder.content(getDialogMessage());
    }
  }
  
  private static class SavedState
    extends Preference.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public MaterialListPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
      {
        return new MaterialListPreference.SavedState(paramAnonymousParcel);
      }
      
      public MaterialListPreference.SavedState[] newArray(int paramAnonymousInt)
      {
        return new MaterialListPreference.SavedState[paramAnonymousInt];
      }
    };
    Bundle dialogBundle;
    boolean isDialogShowing;
    
    SavedState(Parcel paramParcel)
    {
      super();
      if (paramParcel.readInt() == 1) {}
      for (;;)
      {
        this.isDialogShowing = bool;
        this.dialogBundle = paramParcel.readBundle();
        return;
        bool = false;
      }
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(@NonNull Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      if (this.isDialogShowing) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        paramParcel.writeBundle(this.dialogBundle);
        return;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\prefs\MaterialListPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */