package com.afollestad.materialdialogs.color;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.FloatRange;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.afollestad.materialdialogs.util.DialogUtils;

public class CircleView
  extends FrameLayout
{
  private final int borderWidthLarge;
  private final int borderWidthSmall;
  private final Paint innerPaint;
  private final Paint outerPaint;
  private boolean selected;
  private final Paint whitePaint;
  
  public CircleView(Context paramContext)
  {
    this(paramContext, null, 0);
  }
  
  public CircleView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public CircleView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = getResources();
    this.borderWidthSmall = ((int)TypedValue.applyDimension(1, 3.0F, paramContext.getDisplayMetrics()));
    this.borderWidthLarge = ((int)TypedValue.applyDimension(1, 5.0F, paramContext.getDisplayMetrics()));
    this.whitePaint = new Paint();
    this.whitePaint.setAntiAlias(true);
    this.whitePaint.setColor(-1);
    this.innerPaint = new Paint();
    this.innerPaint.setAntiAlias(true);
    this.outerPaint = new Paint();
    this.outerPaint.setAntiAlias(true);
    update(-12303292);
    setWillNotDraw(false);
  }
  
  private Drawable createSelector(int paramInt)
  {
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new OvalShape());
    localShapeDrawable.getPaint().setColor(translucentColor(shiftColorUp(paramInt)));
    StateListDrawable localStateListDrawable = new StateListDrawable();
    localStateListDrawable.addState(new int[] { 16842919 }, localShapeDrawable);
    return localStateListDrawable;
  }
  
  @ColorInt
  public static int shiftColor(@ColorInt int paramInt, @FloatRange(from=0.0D, to=2.0D) float paramFloat)
  {
    if (paramFloat == 1.0F) {}
    for (;;)
    {
      return paramInt;
      float[] arrayOfFloat = new float[3];
      Color.colorToHSV(paramInt, arrayOfFloat);
      arrayOfFloat[2] *= paramFloat;
      paramInt = Color.HSVToColor(arrayOfFloat);
    }
  }
  
  @ColorInt
  public static int shiftColorDown(@ColorInt int paramInt)
  {
    return shiftColor(paramInt, 0.9F);
  }
  
  @ColorInt
  public static int shiftColorUp(@ColorInt int paramInt)
  {
    return shiftColor(paramInt, 1.1F);
  }
  
  @ColorInt
  private static int translucentColor(int paramInt)
  {
    return Color.argb(Math.round(Color.alpha(paramInt) * 0.7F), Color.red(paramInt), Color.green(paramInt), Color.blue(paramInt));
  }
  
  private void update(@ColorInt int paramInt)
  {
    this.innerPaint.setColor(paramInt);
    this.outerPaint.setColor(shiftColorDown(paramInt));
    Drawable localDrawable = createSelector(paramInt);
    if (Build.VERSION.SDK_INT >= 21)
    {
      int[] arrayOfInt = { 16842919 };
      paramInt = shiftColorUp(paramInt);
      setForeground(new RippleDrawable(new ColorStateList(new int[][] { arrayOfInt }, new int[] { paramInt }), localDrawable, null));
    }
    for (;;)
    {
      return;
      setForeground(localDrawable);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int j = getMeasuredWidth() / 2;
    if (this.selected)
    {
      int k = j - this.borderWidthLarge;
      int i = this.borderWidthSmall;
      paramCanvas.drawCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, j, this.outerPaint);
      paramCanvas.drawCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, k, this.whitePaint);
      paramCanvas.drawCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, k - i, this.innerPaint);
    }
    for (;;)
    {
      return;
      paramCanvas.drawCircle(getMeasuredWidth() / 2, getMeasuredHeight() / 2, j, this.innerPaint);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt1);
    setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
  }
  
  @Deprecated
  public void setActivated(boolean paramBoolean)
  {
    throw new IllegalStateException("Cannot use setActivated() on CircleView.");
  }
  
  @Deprecated
  public void setBackground(Drawable paramDrawable)
  {
    throw new IllegalStateException("Cannot use setBackground() on CircleView.");
  }
  
  public void setBackgroundColor(@ColorInt int paramInt)
  {
    update(paramInt);
    requestLayout();
    invalidate();
  }
  
  @Deprecated
  public void setBackgroundDrawable(Drawable paramDrawable)
  {
    throw new IllegalStateException("Cannot use setBackgroundDrawable() on CircleView.");
  }
  
  public void setBackgroundResource(@ColorRes int paramInt)
  {
    setBackgroundColor(DialogUtils.getColor(getContext(), paramInt));
  }
  
  public void setSelected(boolean paramBoolean)
  {
    this.selected = paramBoolean;
    requestLayout();
    invalidate();
  }
  
  public void showHint(int paramInt)
  {
    int[] arrayOfInt = new int[2];
    Rect localRect = new Rect();
    getLocationOnScreen(arrayOfInt);
    getWindowVisibleDisplayFrame(localRect);
    Object localObject = getContext();
    int i = getWidth();
    int n = getHeight();
    int k = arrayOfInt[1];
    int m = n / 2;
    int j = arrayOfInt[0] + i / 2;
    i = j;
    if (ViewCompat.getLayoutDirection(this) == 0) {
      i = ((Context)localObject).getResources().getDisplayMetrics().widthPixels - j;
    }
    localObject = Toast.makeText((Context)localObject, String.format("#%06X", new Object[] { Integer.valueOf(0xFFFFFF & paramInt) }), 0);
    if (k + m < localRect.height()) {
      ((Toast)localObject).setGravity(8388661, i, arrayOfInt[1] + n - localRect.top);
    }
    for (;;)
    {
      ((Toast)localObject).show();
      return;
      ((Toast)localObject).setGravity(81, 0, n);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\color\CircleView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */