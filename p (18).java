package com.testfairy.p;

import android.opengl.GLSurfaceView;
import android.os.Build.VERSION;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class r
{
  private static final AtomicInteger a = new AtomicInteger(1);
  
  public static int a()
  {
    int j;
    if (Build.VERSION.SDK_INT < 17) {
      do
      {
        j = a.get();
        int k = j + 1;
        i = k;
        if (k > 16777215) {
          i = 1;
        }
      } while (!a.compareAndSet(j, i));
    }
    for (int i = j;; i = View.generateViewId()) {
      return i;
    }
  }
  
  public static View a(View paramView, int paramInt1, int paramInt2)
  {
    View localView;
    if (!b(paramView, paramInt1, paramInt2)) {
      localView = null;
    }
    do
    {
      return localView;
      localView = paramView;
    } while (!(paramView instanceof ViewGroup));
    ViewGroup localViewGroup = (ViewGroup)paramView;
    int j = localViewGroup.getChildCount();
    for (int i = 0;; i++)
    {
      localView = paramView;
      if (i >= j) {
        break;
      }
      localView = localViewGroup.getChildAt(i);
      if (b(localView, paramInt1, paramInt2))
      {
        localView = a(localView, paramInt1, paramInt2);
        break;
      }
    }
  }
  
  private static q a(View paramView, Set paramSet)
  {
    q localq = new q();
    localq.a(paramView.getClass().toString());
    localq.e(paramView.getVisibility());
    Object localObject = new int[2];
    paramView.getLocationOnScreen((int[])localObject);
    localq.a(localObject[0]);
    localq.b(localObject[1]);
    localq.d(paramView.getMeasuredHeight());
    localq.c(paramView.getMeasuredWidth());
    if (paramView.getContentDescription() == null)
    {
      localObject = "";
      localq.c((String)localObject);
      localq.f(paramView.getId());
      if (Build.VERSION.SDK_INT < 14) {
        break label248;
      }
    }
    label248:
    for (float f = paramView.getAlpha();; f = 1.0F)
    {
      localq.a(f);
      localq.a(paramView.isFocusable());
      localq.b(paramView.isEnabled());
      if ((paramView instanceof TextView))
      {
        localObject = (TextView)paramView;
        if ((!paramSet.contains(Integer.valueOf(paramView.getId()))) && ((((TextView)localObject).getInputType() & 0x81) != 129)) {
          break label253;
        }
        localq.b("*****");
      }
      if (!(paramView instanceof ViewGroup)) {
        break label290;
      }
      paramView = (ViewGroup)paramView;
      for (int i = 0; i < paramView.getChildCount(); i++) {
        localq.a(a(paramView.getChildAt(i), paramSet));
      }
      localObject = paramView.getContentDescription().toString();
      break;
    }
    label253:
    if (((TextView)localObject).getText() == null) {}
    for (localObject = "";; localObject = ((TextView)localObject).getText().toString())
    {
      localq.b((String)localObject);
      break;
    }
    label290:
    return localq;
  }
  
  public static q a(List paramList, Set paramSet, int paramInt1, int paramInt2)
  {
    q localq = new q();
    localq.a("app");
    localq.c(paramInt1);
    localq.d(paramInt2);
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      View localView = (View)paramList.next();
      try
      {
        localq.a(a(localView, paramSet));
      }
      catch (Exception localException) {}
    }
    return localq;
  }
  
  public static String a(View paramView)
  {
    return b(paramView, 0);
  }
  
  public static List a(View paramView, int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    a(paramView, paramInt, localArrayList);
    return localArrayList;
  }
  
  private static List a(View paramView, int paramInt, List paramList)
  {
    if (paramView.getId() == paramInt) {
      paramList.add(paramView);
    }
    if ((paramView instanceof ViewGroup))
    {
      paramView = (ViewGroup)paramView;
      for (int i = 0; i < paramView.getChildCount(); i++) {
        a(paramView.getChildAt(i), paramInt, paramList);
      }
    }
    return paramList;
  }
  
  public static List a(View paramView, Class paramClass)
  {
    return a(paramView, paramClass, new ArrayList());
  }
  
  private static List a(View paramView, Class paramClass, List paramList)
  {
    if (paramClass.isInstance(paramView)) {
      paramList.add(paramView);
    }
    if ((paramView instanceof ViewGroup))
    {
      paramView = (ViewGroup)paramView;
      int j = paramView.getChildCount();
      for (int i = 0; i < j; i++) {
        a(paramView.getChildAt(i), paramClass, paramList);
      }
    }
    return paramList;
  }
  
  public static List a(View paramView, String paramString)
  {
    return a(paramView, paramString, new ArrayList());
  }
  
  private static List a(View paramView, String paramString, List paramList)
  {
    if (paramView.getClass().getName().startsWith(paramString)) {
      paramList.add(paramView);
    }
    if ((paramView instanceof ViewGroup))
    {
      paramView = (ViewGroup)paramView;
      int j = paramView.getChildCount();
      for (int i = 0; i < j; i++) {
        a(paramView.getChildAt(i), paramString, paramList);
      }
    }
    return paramList;
  }
  
  public static boolean a(View paramView1, View paramView2)
  {
    boolean bool2 = true;
    boolean bool1;
    if (paramView1 == paramView2) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      if ((paramView1 instanceof ViewGroup))
      {
        paramView1 = (ViewGroup)paramView1;
        int j = paramView1.getChildCount();
        for (int i = 0;; i++)
        {
          if (i >= j) {
            break label61;
          }
          bool1 = bool2;
          if (a(paramView1.getChildAt(i), paramView2)) {
            break;
          }
        }
      }
      label61:
      bool1 = false;
    }
  }
  
  private static String b(View paramView, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder(32);
    while (localStringBuilder.length() < paramInt) {
      localStringBuilder.append(' ');
    }
    localStringBuilder.append(paramView.getClass().toString());
    localStringBuilder.append(" #");
    localStringBuilder.append(paramView.getId());
    localStringBuilder.append(' ');
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    Object localObject1;
    if ((paramView instanceof TextView)) {
      localObject1 = (TextView)paramView;
    }
    for (Object localObject2 = "" + " TextView.text='" + ((TextView)localObject1).getText() + "'";; localObject2 = "")
    {
      localObject1 = localObject2;
      if (paramView.willNotCacheDrawing()) {
        localObject1 = (String)localObject2 + " willNotCacheDrawing";
      }
      localObject2 = localObject1;
      if (paramView.willNotDraw()) {
        localObject2 = (String)localObject1 + " willNotDraw";
      }
      localObject1 = localObject2;
      if (paramView.getVisibility() != 0) {
        localObject1 = (String)localObject2 + " visibility=" + paramView.getVisibility();
      }
      localObject2 = localObject1;
      if (!paramView.isShown()) {
        localObject2 = (String)localObject1 + " !isShown";
      }
      if ((paramView instanceof GLSurfaceView)) {
        localStringBuilder.append(" GLSurfaceView");
      }
      if ((paramView instanceof SurfaceView))
      {
        localStringBuilder.append(" SurfaceView");
        if (paramView.getClass().getName().startsWith("maps.")) {
          localStringBuilder.append(" GoogleMaps");
        }
      }
      if ((Build.VERSION.SDK_INT >= 14) && ((paramView instanceof TextureView))) {
        localStringBuilder.append(" TextureView");
      }
      localStringBuilder.append(" (" + arrayOfInt[0] + "," + arrayOfInt[1] + " " + paramView.getMeasuredWidth() + "x" + paramView.getMeasuredHeight() + ")" + (String)localObject2);
      if ((paramView instanceof WebView))
      {
        localObject1 = (WebView)paramView;
        localStringBuilder.append(" url: ");
        localStringBuilder.append(((WebView)localObject1).getUrl());
      }
      localStringBuilder.append("\n");
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        for (int i = 0; i < paramView.getChildCount(); i++) {
          localStringBuilder.append(b(paramView.getChildAt(i), paramInt + 4));
        }
      }
      return localStringBuilder.toString();
    }
  }
  
  public static List b(View paramView)
  {
    ArrayList localArrayList1 = new ArrayList();
    if (paramView.isShown())
    {
      localArrayList1.add(paramView.getClass().getName());
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        ArrayList localArrayList2 = new ArrayList();
        int i = 0;
        if (i < paramView.getChildCount())
        {
          View localView = paramView.getChildAt(i);
          if (localView.isShown())
          {
            if (!(localView instanceof ViewGroup)) {
              break label94;
            }
            localArrayList2.add(a(localView));
          }
          for (;;)
          {
            i++;
            break;
            label94:
            localArrayList2.add(localView.getClass().getName());
          }
        }
        localArrayList1.add(localArrayList2);
      }
    }
    return localArrayList1;
  }
  
  private static boolean b(View paramView, int paramInt1, int paramInt2)
  {
    boolean bool = true;
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    if ((paramInt1 >= arrayOfInt[0]) && (paramInt2 >= arrayOfInt[1]))
    {
      int i = paramView.getMeasuredWidth();
      int j = paramView.getMeasuredHeight();
      if ((paramInt1 >= i + arrayOfInt[0]) || (paramInt2 >= arrayOfInt[1] + j)) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public static int c(View paramView)
  {
    ViewGroup localViewGroup = (ViewGroup)paramView.getParent();
    int j = localViewGroup.getChildCount();
    int i = 0;
    if (i < j) {
      if (localViewGroup.getChildAt(i) != paramView) {}
    }
    for (;;)
    {
      return i;
      i++;
      break;
      i = 0;
    }
  }
  
  public static View d(View paramView)
  {
    while ((paramView.getRootView() != null) && (paramView != paramView.getRootView())) {
      paramView = paramView.getRootView();
    }
    return paramView;
  }
  
  public static String e(View paramView)
  {
    Object localObject1 = null;
    if (paramView == null)
    {
      paramView = (View)localObject1;
      return paramView;
    }
    Object localObject2;
    if ((paramView instanceof TextView))
    {
      localObject2 = ((TextView)paramView).getText();
      localObject1 = localObject2;
      if (localObject2 != null)
      {
        localObject1 = localObject2;
        if (((CharSequence)localObject2).length() != 0) {}
      }
    }
    for (localObject1 = null;; localObject1 = null)
    {
      localObject2 = localObject1;
      if (localObject1 == null)
      {
        paramView = paramView.getContentDescription();
        localObject2 = paramView;
        if (paramView != null)
        {
          localObject2 = paramView;
          if (paramView.length() == 0) {
            localObject2 = null;
          }
        }
      }
      paramView = (String)localObject2;
      break;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */