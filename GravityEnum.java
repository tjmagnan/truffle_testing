package com.afollestad.materialdialogs;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build.VERSION;

public enum GravityEnum
{
  private static final boolean HAS_RTL;
  
  static
  {
    boolean bool = true;
    START = new GravityEnum("START", 0);
    CENTER = new GravityEnum("CENTER", 1);
    END = new GravityEnum("END", 2);
    $VALUES = new GravityEnum[] { START, CENTER, END };
    if (Build.VERSION.SDK_INT >= 17) {}
    for (;;)
    {
      HAS_RTL = bool;
      return;
      bool = false;
    }
  }
  
  private GravityEnum() {}
  
  @SuppressLint({"RtlHardcoded"})
  public int getGravityInt()
  {
    int i;
    switch (this)
    {
    default: 
      throw new IllegalStateException("Invalid gravity constant");
    case ???: 
      if (HAS_RTL) {
        i = 8388611;
      }
      break;
    }
    for (;;)
    {
      return i;
      i = 3;
      continue;
      i = 1;
      continue;
      if (HAS_RTL) {
        i = 8388613;
      } else {
        i = 5;
      }
    }
  }
  
  @TargetApi(17)
  public int getTextAlignment()
  {
    int i;
    switch (this)
    {
    default: 
      i = 5;
    }
    for (;;)
    {
      return i;
      i = 4;
      continue;
      i = 6;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\GravityEnum.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */