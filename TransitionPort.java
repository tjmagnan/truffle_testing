package android.support.transition;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.support.annotation.RequiresApi;
import android.support.annotation.RestrictTo;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@TargetApi(14)
@RequiresApi(14)
abstract class TransitionPort
  implements Cloneable
{
  static final boolean DBG = false;
  private static final String LOG_TAG = "Transition";
  private static ThreadLocal<ArrayMap<Animator, AnimationInfo>> sRunningAnimators = new ThreadLocal();
  ArrayList<Animator> mAnimators = new ArrayList();
  boolean mCanRemoveViews = false;
  ArrayList<Animator> mCurrentAnimators = new ArrayList();
  long mDuration = -1L;
  private TransitionValuesMaps mEndValues = new TransitionValuesMaps();
  private boolean mEnded = false;
  TimeInterpolator mInterpolator = null;
  ArrayList<TransitionListener> mListeners = null;
  private String mName = getClass().getName();
  int mNumInstances = 0;
  TransitionSetPort mParent = null;
  boolean mPaused = false;
  ViewGroup mSceneRoot = null;
  long mStartDelay = -1L;
  private TransitionValuesMaps mStartValues = new TransitionValuesMaps();
  ArrayList<View> mTargetChildExcludes = null;
  ArrayList<View> mTargetExcludes = null;
  ArrayList<Integer> mTargetIdChildExcludes = null;
  ArrayList<Integer> mTargetIdExcludes = null;
  ArrayList<Integer> mTargetIds = new ArrayList();
  ArrayList<Class> mTargetTypeChildExcludes = null;
  ArrayList<Class> mTargetTypeExcludes = null;
  ArrayList<View> mTargets = new ArrayList();
  
  private void captureHierarchy(View paramView, boolean paramBoolean)
  {
    if (paramView == null) {}
    for (;;)
    {
      return;
      int i = 0;
      if ((paramView.getParent() instanceof ListView)) {
        i = 1;
      }
      if ((i == 0) || (((ListView)paramView.getParent()).getAdapter().hasStableIds()))
      {
        int j = -1;
        long l = -1L;
        if (i == 0) {
          j = paramView.getId();
        }
        for (;;)
        {
          if (((this.mTargetIdExcludes != null) && (this.mTargetIdExcludes.contains(Integer.valueOf(j)))) || ((this.mTargetExcludes != null) && (this.mTargetExcludes.contains(paramView)))) {
            break label177;
          }
          if ((this.mTargetTypeExcludes == null) || (paramView == null)) {
            break label179;
          }
          int m = this.mTargetTypeExcludes.size();
          for (int k = 0;; k++)
          {
            if (k >= m) {
              break label179;
            }
            if (((Class)this.mTargetTypeExcludes.get(k)).isInstance(paramView)) {
              break;
            }
          }
          localObject = (ListView)paramView.getParent();
          l = ((ListView)localObject).getItemIdAtPosition(((ListView)localObject).getPositionForView(paramView));
        }
        label177:
        continue;
        label179:
        Object localObject = new TransitionValues();
        ((TransitionValues)localObject).view = paramView;
        if (paramBoolean)
        {
          captureStartValues((TransitionValues)localObject);
          label204:
          if (!paramBoolean) {
            break label370;
          }
          if (i != 0) {
            break label353;
          }
          this.mStartValues.viewValues.put(paramView, localObject);
          if (j >= 0) {
            this.mStartValues.idValues.put(j, localObject);
          }
        }
        for (;;)
        {
          if ((!(paramView instanceof ViewGroup)) || ((this.mTargetIdChildExcludes != null) && (this.mTargetIdChildExcludes.contains(Integer.valueOf(j)))) || ((this.mTargetChildExcludes != null) && (this.mTargetChildExcludes.contains(paramView)))) {
            break label425;
          }
          if ((this.mTargetTypeChildExcludes == null) || (paramView == null)) {
            break label427;
          }
          j = this.mTargetTypeChildExcludes.size();
          for (i = 0;; i++)
          {
            if (i >= j) {
              break label427;
            }
            if (((Class)this.mTargetTypeChildExcludes.get(i)).isInstance(paramView)) {
              break;
            }
          }
          captureEndValues((TransitionValues)localObject);
          break label204;
          label353:
          this.mStartValues.itemIdValues.put(l, localObject);
          continue;
          label370:
          if (i == 0)
          {
            this.mEndValues.viewValues.put(paramView, localObject);
            if (j >= 0) {
              this.mEndValues.idValues.put(j, localObject);
            }
          }
          else
          {
            this.mEndValues.itemIdValues.put(l, localObject);
          }
        }
        label425:
        continue;
        label427:
        paramView = (ViewGroup)paramView;
        for (i = 0; i < paramView.getChildCount(); i++) {
          captureHierarchy(paramView.getChildAt(i), paramBoolean);
        }
      }
    }
  }
  
  private ArrayList<Integer> excludeId(ArrayList<Integer> paramArrayList, int paramInt, boolean paramBoolean)
  {
    Object localObject = paramArrayList;
    if (paramInt > 0) {
      if (!paramBoolean) {
        break label24;
      }
    }
    label24:
    for (localObject = ArrayListManager.add(paramArrayList, Integer.valueOf(paramInt));; localObject = ArrayListManager.remove(paramArrayList, Integer.valueOf(paramInt))) {
      return (ArrayList<Integer>)localObject;
    }
  }
  
  private ArrayList<Class> excludeType(ArrayList<Class> paramArrayList, Class paramClass, boolean paramBoolean)
  {
    Object localObject = paramArrayList;
    if (paramClass != null) {
      if (!paramBoolean) {
        break label21;
      }
    }
    label21:
    for (localObject = ArrayListManager.add(paramArrayList, paramClass);; localObject = ArrayListManager.remove(paramArrayList, paramClass)) {
      return (ArrayList<Class>)localObject;
    }
  }
  
  private ArrayList<View> excludeView(ArrayList<View> paramArrayList, View paramView, boolean paramBoolean)
  {
    Object localObject = paramArrayList;
    if (paramView != null) {
      if (!paramBoolean) {
        break label21;
      }
    }
    label21:
    for (localObject = ArrayListManager.add(paramArrayList, paramView);; localObject = ArrayListManager.remove(paramArrayList, paramView)) {
      return (ArrayList<View>)localObject;
    }
  }
  
  private static ArrayMap<Animator, AnimationInfo> getRunningAnimators()
  {
    ArrayMap localArrayMap2 = (ArrayMap)sRunningAnimators.get();
    ArrayMap localArrayMap1 = localArrayMap2;
    if (localArrayMap2 == null)
    {
      localArrayMap1 = new ArrayMap();
      sRunningAnimators.set(localArrayMap1);
    }
    return localArrayMap1;
  }
  
  private void runAnimator(Animator paramAnimator, final ArrayMap<Animator, AnimationInfo> paramArrayMap)
  {
    if (paramAnimator != null)
    {
      paramAnimator.addListener(new AnimatorListenerAdapter()
      {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
          paramArrayMap.remove(paramAnonymousAnimator);
          TransitionPort.this.mCurrentAnimators.remove(paramAnonymousAnimator);
        }
        
        public void onAnimationStart(Animator paramAnonymousAnimator)
        {
          TransitionPort.this.mCurrentAnimators.add(paramAnonymousAnimator);
        }
      });
      animate(paramAnimator);
    }
  }
  
  public TransitionPort addListener(TransitionListener paramTransitionListener)
  {
    if (this.mListeners == null) {
      this.mListeners = new ArrayList();
    }
    this.mListeners.add(paramTransitionListener);
    return this;
  }
  
  public TransitionPort addTarget(int paramInt)
  {
    if (paramInt > 0) {
      this.mTargetIds.add(Integer.valueOf(paramInt));
    }
    return this;
  }
  
  public TransitionPort addTarget(View paramView)
  {
    this.mTargets.add(paramView);
    return this;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void animate(Animator paramAnimator)
  {
    if (paramAnimator == null) {
      end();
    }
    for (;;)
    {
      return;
      if (getDuration() >= 0L) {
        paramAnimator.setDuration(getDuration());
      }
      if (getStartDelay() >= 0L) {
        paramAnimator.setStartDelay(getStartDelay());
      }
      if (getInterpolator() != null) {
        paramAnimator.setInterpolator(getInterpolator());
      }
      paramAnimator.addListener(new AnimatorListenerAdapter()
      {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
          TransitionPort.this.end();
          paramAnonymousAnimator.removeListener(this);
        }
      });
      paramAnimator.start();
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void cancel()
  {
    for (int i = this.mCurrentAnimators.size() - 1; i >= 0; i--) {
      ((Animator)this.mCurrentAnimators.get(i)).cancel();
    }
    if ((this.mListeners != null) && (this.mListeners.size() > 0))
    {
      ArrayList localArrayList = (ArrayList)this.mListeners.clone();
      int j = localArrayList.size();
      for (i = 0; i < j; i++) {
        ((TransitionListener)localArrayList.get(i)).onTransitionCancel(this);
      }
    }
  }
  
  public abstract void captureEndValues(TransitionValues paramTransitionValues);
  
  public abstract void captureStartValues(TransitionValues paramTransitionValues);
  
  void captureValues(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    clearValues(paramBoolean);
    if ((this.mTargetIds.size() > 0) || (this.mTargets.size() > 0))
    {
      int i;
      Object localObject;
      if (this.mTargetIds.size() > 0)
      {
        i = 0;
        if (i < this.mTargetIds.size())
        {
          int j = ((Integer)this.mTargetIds.get(i)).intValue();
          localObject = paramViewGroup.findViewById(j);
          TransitionValues localTransitionValues;
          if (localObject != null)
          {
            localTransitionValues = new TransitionValues();
            localTransitionValues.view = ((View)localObject);
            if (!paramBoolean) {
              break label147;
            }
            captureStartValues(localTransitionValues);
            label103:
            if (!paramBoolean) {
              break label156;
            }
            this.mStartValues.viewValues.put(localObject, localTransitionValues);
            if (j >= 0) {
              this.mStartValues.idValues.put(j, localTransitionValues);
            }
          }
          for (;;)
          {
            i++;
            break;
            label147:
            captureEndValues(localTransitionValues);
            break label103;
            label156:
            this.mEndValues.viewValues.put(localObject, localTransitionValues);
            if (j >= 0) {
              this.mEndValues.idValues.put(j, localTransitionValues);
            }
          }
        }
      }
      if (this.mTargets.size() > 0)
      {
        i = 0;
        if (i < this.mTargets.size())
        {
          paramViewGroup = (View)this.mTargets.get(i);
          if (paramViewGroup != null)
          {
            localObject = new TransitionValues();
            ((TransitionValues)localObject).view = paramViewGroup;
            if (!paramBoolean) {
              break label281;
            }
            captureStartValues((TransitionValues)localObject);
            label257:
            if (!paramBoolean) {
              break label290;
            }
            this.mStartValues.viewValues.put(paramViewGroup, localObject);
          }
          for (;;)
          {
            i++;
            break;
            label281:
            captureEndValues((TransitionValues)localObject);
            break label257;
            label290:
            this.mEndValues.viewValues.put(paramViewGroup, localObject);
          }
        }
      }
    }
    else
    {
      captureHierarchy(paramViewGroup, paramBoolean);
    }
  }
  
  void clearValues(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mStartValues.viewValues.clear();
      this.mStartValues.idValues.clear();
      this.mStartValues.itemIdValues.clear();
    }
    for (;;)
    {
      return;
      this.mEndValues.viewValues.clear();
      this.mEndValues.idValues.clear();
      this.mEndValues.itemIdValues.clear();
    }
  }
  
  public TransitionPort clone()
  {
    Object localObject1 = null;
    try
    {
      TransitionPort localTransitionPort = (TransitionPort)super.clone();
      localObject1 = localTransitionPort;
      Object localObject2 = new java/util/ArrayList;
      localObject1 = localTransitionPort;
      ((ArrayList)localObject2).<init>();
      localObject1 = localTransitionPort;
      localTransitionPort.mAnimators = ((ArrayList)localObject2);
      localObject1 = localTransitionPort;
      localObject2 = new android/support/transition/TransitionValuesMaps;
      localObject1 = localTransitionPort;
      ((TransitionValuesMaps)localObject2).<init>();
      localObject1 = localTransitionPort;
      localTransitionPort.mStartValues = ((TransitionValuesMaps)localObject2);
      localObject1 = localTransitionPort;
      localObject2 = new android/support/transition/TransitionValuesMaps;
      localObject1 = localTransitionPort;
      ((TransitionValuesMaps)localObject2).<init>();
      localObject1 = localTransitionPort;
      localTransitionPort.mEndValues = ((TransitionValuesMaps)localObject2);
      localObject1 = localTransitionPort;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      for (;;) {}
    }
    return (TransitionPort)localObject1;
  }
  
  public Animator createAnimator(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, TransitionValues paramTransitionValues2)
  {
    return null;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void createAnimators(ViewGroup paramViewGroup, TransitionValuesMaps paramTransitionValuesMaps1, TransitionValuesMaps paramTransitionValuesMaps2)
  {
    Object localObject7 = new ArrayMap(paramTransitionValuesMaps2.viewValues);
    Object localObject6 = new SparseArray(paramTransitionValuesMaps2.idValues.size());
    for (int i = 0; i < paramTransitionValuesMaps2.idValues.size(); i++) {
      ((SparseArray)localObject6).put(paramTransitionValuesMaps2.idValues.keyAt(i), paramTransitionValuesMaps2.idValues.valueAt(i));
    }
    Object localObject5 = new LongSparseArray(paramTransitionValuesMaps2.itemIdValues.size());
    for (i = 0; i < paramTransitionValuesMaps2.itemIdValues.size(); i++) {
      ((LongSparseArray)localObject5).put(paramTransitionValuesMaps2.itemIdValues.keyAt(i), paramTransitionValuesMaps2.itemIdValues.valueAt(i));
    }
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList1 = new ArrayList();
    Iterator localIterator1 = paramTransitionValuesMaps1.viewValues.keySet().iterator();
    Object localObject1;
    label245:
    label334:
    Object localObject4;
    Object localObject3;
    long l;
    while (localIterator1.hasNext())
    {
      View localView = (View)localIterator1.next();
      localObject1 = null;
      i = 0;
      if ((localView.getParent() instanceof ListView)) {
        i = 1;
      }
      if (i == 0)
      {
        i = localView.getId();
        if (paramTransitionValuesMaps1.viewValues.get(localView) != null)
        {
          localObject2 = (TransitionValues)paramTransitionValuesMaps1.viewValues.get(localView);
          if (paramTransitionValuesMaps2.viewValues.get(localView) == null) {
            break label334;
          }
          localObject1 = (TransitionValues)paramTransitionValuesMaps2.viewValues.get(localView);
          ((ArrayMap)localObject7).remove(localView);
        }
        for (;;)
        {
          ((SparseArray)localObject6).remove(i);
          if (!isValidTarget(localView, i)) {
            break;
          }
          localArrayList2.add(localObject2);
          localArrayList1.add(localObject1);
          break;
          localObject2 = (TransitionValues)paramTransitionValuesMaps1.idValues.get(i);
          break label245;
          if (i != -1)
          {
            localObject4 = (TransitionValues)paramTransitionValuesMaps2.idValues.get(i);
            localObject3 = null;
            Iterator localIterator2 = ((ArrayMap)localObject7).keySet().iterator();
            while (localIterator2.hasNext())
            {
              localObject1 = (View)localIterator2.next();
              if (((View)localObject1).getId() == i) {
                localObject3 = localObject1;
              }
            }
            localObject1 = localObject4;
            if (localObject3 != null)
            {
              ((ArrayMap)localObject7).remove(localObject3);
              localObject1 = localObject4;
            }
          }
        }
      }
      localObject1 = (ListView)localView.getParent();
      if (((ListView)localObject1).getAdapter().hasStableIds())
      {
        l = ((ListView)localObject1).getItemIdAtPosition(((ListView)localObject1).getPositionForView(localView));
        localObject1 = (TransitionValues)paramTransitionValuesMaps1.itemIdValues.get(l);
        ((LongSparseArray)localObject5).remove(l);
        localArrayList2.add(localObject1);
        localArrayList1.add(null);
      }
    }
    int j = paramTransitionValuesMaps1.itemIdValues.size();
    for (i = 0; i < j; i++)
    {
      l = paramTransitionValuesMaps1.itemIdValues.keyAt(i);
      if (isValidTarget(null, l))
      {
        localObject1 = (TransitionValues)paramTransitionValuesMaps1.itemIdValues.get(l);
        localObject2 = (TransitionValues)paramTransitionValuesMaps2.itemIdValues.get(l);
        ((LongSparseArray)localObject5).remove(l);
        localArrayList2.add(localObject1);
        localArrayList1.add(localObject2);
      }
    }
    Object localObject2 = ((ArrayMap)localObject7).keySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (View)((Iterator)localObject2).next();
      i = ((View)localObject3).getId();
      if (isValidTarget((View)localObject3, i))
      {
        if (paramTransitionValuesMaps1.viewValues.get(localObject3) != null) {}
        for (localObject1 = (TransitionValues)paramTransitionValuesMaps1.viewValues.get(localObject3);; localObject1 = (TransitionValues)paramTransitionValuesMaps1.idValues.get(i))
        {
          localObject3 = (TransitionValues)((ArrayMap)localObject7).get(localObject3);
          ((SparseArray)localObject6).remove(i);
          localArrayList2.add(localObject1);
          localArrayList1.add(localObject3);
          break;
        }
      }
    }
    j = ((SparseArray)localObject6).size();
    int k;
    for (i = 0; i < j; i++)
    {
      k = ((SparseArray)localObject6).keyAt(i);
      if (isValidTarget(null, k))
      {
        localObject2 = (TransitionValues)paramTransitionValuesMaps1.idValues.get(k);
        localObject1 = (TransitionValues)((SparseArray)localObject6).get(k);
        localArrayList2.add(localObject2);
        localArrayList1.add(localObject1);
      }
    }
    j = ((LongSparseArray)localObject5).size();
    for (i = 0; i < j; i++)
    {
      l = ((LongSparseArray)localObject5).keyAt(i);
      localObject1 = (TransitionValues)paramTransitionValuesMaps1.itemIdValues.get(l);
      localObject2 = (TransitionValues)((LongSparseArray)localObject5).get(l);
      localArrayList2.add(localObject1);
      localArrayList1.add(localObject2);
    }
    localObject6 = getRunningAnimators();
    i = 0;
    if (i < localArrayList2.size())
    {
      localObject1 = (TransitionValues)localArrayList2.get(i);
      paramTransitionValuesMaps1 = (TransitionValues)localArrayList1.get(i);
      if (((localObject1 != null) || (paramTransitionValuesMaps1 != null)) && ((localObject1 == null) || (!((TransitionValues)localObject1).equals(paramTransitionValuesMaps1))))
      {
        localObject3 = createAnimator(paramViewGroup, (TransitionValues)localObject1, paramTransitionValuesMaps1);
        if (localObject3 != null)
        {
          localObject5 = null;
          if (paramTransitionValuesMaps1 == null) {
            break label1291;
          }
          localObject4 = paramTransitionValuesMaps1.view;
          localObject7 = getTransitionProperties();
          localObject2 = localObject3;
          paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5;
          localObject1 = localObject4;
          if (localObject4 != null)
          {
            localObject2 = localObject3;
            paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5;
            localObject1 = localObject4;
            if (localObject7 != null)
            {
              localObject2 = localObject3;
              paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5;
              localObject1 = localObject4;
              if (localObject7.length > 0)
              {
                localObject5 = new TransitionValues();
                ((TransitionValues)localObject5).view = ((View)localObject4);
                paramTransitionValuesMaps1 = (TransitionValues)paramTransitionValuesMaps2.viewValues.get(localObject4);
                if (paramTransitionValuesMaps1 != null) {
                  for (j = 0; j < localObject7.length; j++) {
                    ((TransitionValues)localObject5).values.put(localObject7[j], paramTransitionValuesMaps1.values.get(localObject7[j]));
                  }
                }
                k = ((ArrayMap)localObject6).size();
                j = 0;
                label1135:
                localObject2 = localObject3;
                paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5;
                localObject1 = localObject4;
                if (j < k)
                {
                  paramTransitionValuesMaps1 = (AnimationInfo)((ArrayMap)localObject6).get((Animator)((ArrayMap)localObject6).keyAt(j));
                  if ((paramTransitionValuesMaps1.values == null) || (paramTransitionValuesMaps1.view != localObject4) || (((paramTransitionValuesMaps1.name != null) || (getName() != null)) && ((!paramTransitionValuesMaps1.name.equals(getName())) || (!paramTransitionValuesMaps1.values.equals(localObject5))))) {
                    break label1285;
                  }
                  localObject2 = null;
                  localObject1 = localObject4;
                }
              }
            }
          }
        }
      }
      for (paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5;; paramTransitionValuesMaps1 = (TransitionValuesMaps)localObject5)
      {
        if (localObject2 != null)
        {
          ((ArrayMap)localObject6).put(localObject2, new AnimationInfo((View)localObject1, getName(), WindowIdPort.getWindowId(paramViewGroup), paramTransitionValuesMaps1));
          this.mAnimators.add(localObject2);
        }
        i++;
        break;
        label1285:
        j++;
        break label1135;
        label1291:
        localObject1 = ((TransitionValues)localObject1).view;
        localObject2 = localObject3;
      }
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void end()
  {
    this.mNumInstances -= 1;
    if (this.mNumInstances == 0)
    {
      Object localObject;
      if ((this.mListeners != null) && (this.mListeners.size() > 0))
      {
        localObject = (ArrayList)this.mListeners.clone();
        int j = ((ArrayList)localObject).size();
        for (i = 0; i < j; i++) {
          ((TransitionListener)((ArrayList)localObject).get(i)).onTransitionEnd(this);
        }
      }
      for (int i = 0; i < this.mStartValues.itemIdValues.size(); i++) {
        localObject = ((TransitionValues)this.mStartValues.itemIdValues.valueAt(i)).view;
      }
      for (i = 0; i < this.mEndValues.itemIdValues.size(); i++) {
        localObject = ((TransitionValues)this.mEndValues.itemIdValues.valueAt(i)).view;
      }
      this.mEnded = true;
    }
  }
  
  public TransitionPort excludeChildren(int paramInt, boolean paramBoolean)
  {
    this.mTargetIdChildExcludes = excludeId(this.mTargetIdChildExcludes, paramInt, paramBoolean);
    return this;
  }
  
  public TransitionPort excludeChildren(View paramView, boolean paramBoolean)
  {
    this.mTargetChildExcludes = excludeView(this.mTargetChildExcludes, paramView, paramBoolean);
    return this;
  }
  
  public TransitionPort excludeChildren(Class paramClass, boolean paramBoolean)
  {
    this.mTargetTypeChildExcludes = excludeType(this.mTargetTypeChildExcludes, paramClass, paramBoolean);
    return this;
  }
  
  public TransitionPort excludeTarget(int paramInt, boolean paramBoolean)
  {
    this.mTargetIdExcludes = excludeId(this.mTargetIdExcludes, paramInt, paramBoolean);
    return this;
  }
  
  public TransitionPort excludeTarget(View paramView, boolean paramBoolean)
  {
    this.mTargetExcludes = excludeView(this.mTargetExcludes, paramView, paramBoolean);
    return this;
  }
  
  public TransitionPort excludeTarget(Class paramClass, boolean paramBoolean)
  {
    this.mTargetTypeExcludes = excludeType(this.mTargetTypeExcludes, paramClass, paramBoolean);
    return this;
  }
  
  public long getDuration()
  {
    return this.mDuration;
  }
  
  public TimeInterpolator getInterpolator()
  {
    return this.mInterpolator;
  }
  
  public String getName()
  {
    return this.mName;
  }
  
  public long getStartDelay()
  {
    return this.mStartDelay;
  }
  
  public List<Integer> getTargetIds()
  {
    return this.mTargetIds;
  }
  
  public List<View> getTargets()
  {
    return this.mTargets;
  }
  
  public String[] getTransitionProperties()
  {
    return null;
  }
  
  public TransitionValues getTransitionValues(View paramView, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getTransitionValues(paramView, paramBoolean);
      return (TransitionValues)localObject;
    }
    if (paramBoolean) {}
    for (TransitionValuesMaps localTransitionValuesMaps = this.mStartValues;; localTransitionValuesMaps = this.mEndValues)
    {
      TransitionValues localTransitionValues = (TransitionValues)localTransitionValuesMaps.viewValues.get(paramView);
      localObject = localTransitionValues;
      if (localTransitionValues != null) {
        break;
      }
      int i = paramView.getId();
      if (i >= 0) {
        localTransitionValues = (TransitionValues)localTransitionValuesMaps.idValues.get(i);
      }
      localObject = localTransitionValues;
      if (localTransitionValues != null) {
        break;
      }
      localObject = localTransitionValues;
      if (!(paramView.getParent() instanceof ListView)) {
        break;
      }
      localObject = (ListView)paramView.getParent();
      long l = ((ListView)localObject).getItemIdAtPosition(((ListView)localObject).getPositionForView(paramView));
      localObject = (TransitionValues)localTransitionValuesMaps.itemIdValues.get(l);
      break;
    }
  }
  
  boolean isValidTarget(View paramView, long paramLong)
  {
    boolean bool;
    if ((this.mTargetIdExcludes != null) && (this.mTargetIdExcludes.contains(Integer.valueOf((int)paramLong)))) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      if ((this.mTargetExcludes != null) && (this.mTargetExcludes.contains(paramView)))
      {
        bool = false;
      }
      else
      {
        int i;
        if ((this.mTargetTypeExcludes != null) && (paramView != null))
        {
          int j = this.mTargetTypeExcludes.size();
          for (i = 0;; i++)
          {
            if (i >= j) {
              break label113;
            }
            if (((Class)this.mTargetTypeExcludes.get(i)).isInstance(paramView))
            {
              bool = false;
              break;
            }
          }
        }
        label113:
        if ((this.mTargetIds.size() == 0) && (this.mTargets.size() == 0))
        {
          bool = true;
        }
        else
        {
          if (this.mTargetIds.size() > 0) {
            for (i = 0;; i++)
            {
              if (i >= this.mTargetIds.size()) {
                break label197;
              }
              if (((Integer)this.mTargetIds.get(i)).intValue() == paramLong)
              {
                bool = true;
                break;
              }
            }
          }
          label197:
          if ((paramView != null) && (this.mTargets.size() > 0)) {
            for (i = 0;; i++)
            {
              if (i >= this.mTargets.size()) {
                break label251;
              }
              if (this.mTargets.get(i) == paramView)
              {
                bool = true;
                break;
              }
            }
          }
          label251:
          bool = false;
        }
      }
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void pause(View paramView)
  {
    if (!this.mEnded)
    {
      ArrayMap localArrayMap = getRunningAnimators();
      int i = localArrayMap.size();
      paramView = WindowIdPort.getWindowId(paramView);
      i--;
      while (i >= 0)
      {
        AnimationInfo localAnimationInfo = (AnimationInfo)localArrayMap.valueAt(i);
        if ((localAnimationInfo.view != null) && (paramView.equals(localAnimationInfo.windowId))) {
          ((Animator)localArrayMap.keyAt(i)).cancel();
        }
        i--;
      }
      if ((this.mListeners != null) && (this.mListeners.size() > 0))
      {
        paramView = (ArrayList)this.mListeners.clone();
        int j = paramView.size();
        for (i = 0; i < j; i++) {
          ((TransitionListener)paramView.get(i)).onTransitionPause(this);
        }
      }
      this.mPaused = true;
    }
  }
  
  void playTransition(ViewGroup paramViewGroup)
  {
    ArrayMap localArrayMap = getRunningAnimators();
    int i = localArrayMap.size() - 1;
    if (i >= 0)
    {
      Animator localAnimator = (Animator)localArrayMap.keyAt(i);
      Object localObject1;
      if (localAnimator != null)
      {
        localObject1 = (AnimationInfo)localArrayMap.get(localAnimator);
        if ((localObject1 != null) && (((AnimationInfo)localObject1).view != null) && (((AnimationInfo)localObject1).view.getContext() == paramViewGroup.getContext()))
        {
          int k = 0;
          TransitionValues localTransitionValues = ((AnimationInfo)localObject1).values;
          Object localObject3 = ((AnimationInfo)localObject1).view;
          if (this.mEndValues.viewValues == null) {
            break label285;
          }
          localObject1 = (TransitionValues)this.mEndValues.viewValues.get(localObject3);
          label117:
          Object localObject2 = localObject1;
          if (localObject1 == null) {
            localObject2 = (TransitionValues)this.mEndValues.idValues.get(((View)localObject3).getId());
          }
          int j = k;
          if (localTransitionValues != null)
          {
            j = k;
            if (localObject2 != null)
            {
              localObject3 = localTransitionValues.values.keySet().iterator();
              Object localObject4;
              do
              {
                j = k;
                if (!((Iterator)localObject3).hasNext()) {
                  break;
                }
                localObject4 = (String)((Iterator)localObject3).next();
                localObject1 = localTransitionValues.values.get(localObject4);
                localObject4 = ((TransitionValues)localObject2).values.get(localObject4);
              } while ((localObject1 == null) || (localObject4 == null) || (localObject1.equals(localObject4)));
              j = 1;
            }
          }
          if (j != 0)
          {
            if ((!localAnimator.isRunning()) && (!localAnimator.isStarted())) {
              break label291;
            }
            localAnimator.cancel();
          }
        }
      }
      for (;;)
      {
        i--;
        break;
        label285:
        localObject1 = null;
        break label117;
        label291:
        localArrayMap.remove(localAnimator);
      }
    }
    createAnimators(paramViewGroup, this.mStartValues, this.mEndValues);
    runAnimators();
  }
  
  public TransitionPort removeListener(TransitionListener paramTransitionListener)
  {
    if (this.mListeners == null) {}
    for (;;)
    {
      return this;
      this.mListeners.remove(paramTransitionListener);
      if (this.mListeners.size() == 0) {
        this.mListeners = null;
      }
    }
  }
  
  public TransitionPort removeTarget(int paramInt)
  {
    if (paramInt > 0) {
      this.mTargetIds.remove(Integer.valueOf(paramInt));
    }
    return this;
  }
  
  public TransitionPort removeTarget(View paramView)
  {
    if (paramView != null) {
      this.mTargets.remove(paramView);
    }
    return this;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void resume(View paramView)
  {
    if (this.mPaused)
    {
      if (!this.mEnded)
      {
        ArrayMap localArrayMap = getRunningAnimators();
        int i = localArrayMap.size();
        paramView = WindowIdPort.getWindowId(paramView);
        i--;
        while (i >= 0)
        {
          AnimationInfo localAnimationInfo = (AnimationInfo)localArrayMap.valueAt(i);
          if ((localAnimationInfo.view != null) && (paramView.equals(localAnimationInfo.windowId))) {
            ((Animator)localArrayMap.keyAt(i)).end();
          }
          i--;
        }
        if ((this.mListeners != null) && (this.mListeners.size() > 0))
        {
          paramView = (ArrayList)this.mListeners.clone();
          int j = paramView.size();
          for (i = 0; i < j; i++) {
            ((TransitionListener)paramView.get(i)).onTransitionResume(this);
          }
        }
      }
      this.mPaused = false;
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void runAnimators()
  {
    start();
    ArrayMap localArrayMap = getRunningAnimators();
    Iterator localIterator = this.mAnimators.iterator();
    while (localIterator.hasNext())
    {
      Animator localAnimator = (Animator)localIterator.next();
      if (localArrayMap.containsKey(localAnimator))
      {
        start();
        runAnimator(localAnimator, localArrayMap);
      }
    }
    this.mAnimators.clear();
    end();
  }
  
  void setCanRemoveViews(boolean paramBoolean)
  {
    this.mCanRemoveViews = paramBoolean;
  }
  
  public TransitionPort setDuration(long paramLong)
  {
    this.mDuration = paramLong;
    return this;
  }
  
  public TransitionPort setInterpolator(TimeInterpolator paramTimeInterpolator)
  {
    this.mInterpolator = paramTimeInterpolator;
    return this;
  }
  
  TransitionPort setSceneRoot(ViewGroup paramViewGroup)
  {
    this.mSceneRoot = paramViewGroup;
    return this;
  }
  
  public TransitionPort setStartDelay(long paramLong)
  {
    this.mStartDelay = paramLong;
    return this;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected void start()
  {
    if (this.mNumInstances == 0)
    {
      if ((this.mListeners != null) && (this.mListeners.size() > 0))
      {
        ArrayList localArrayList = (ArrayList)this.mListeners.clone();
        int j = localArrayList.size();
        for (int i = 0; i < j; i++) {
          ((TransitionListener)localArrayList.get(i)).onTransitionStart(this);
        }
      }
      this.mEnded = false;
    }
    this.mNumInstances += 1;
  }
  
  public String toString()
  {
    return toString("");
  }
  
  String toString(String paramString)
  {
    String str = paramString + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ": ";
    paramString = str;
    if (this.mDuration != -1L) {
      paramString = str + "dur(" + this.mDuration + ") ";
    }
    str = paramString;
    if (this.mStartDelay != -1L) {
      str = paramString + "dly(" + this.mStartDelay + ") ";
    }
    paramString = str;
    if (this.mInterpolator != null) {
      paramString = str + "interp(" + this.mInterpolator + ") ";
    }
    if (this.mTargetIds.size() <= 0)
    {
      str = paramString;
      if (this.mTargets.size() <= 0) {}
    }
    else
    {
      str = paramString + "tgts(";
      paramString = str;
      int i;
      if (this.mTargetIds.size() > 0) {
        for (i = 0;; i++)
        {
          paramString = str;
          if (i >= this.mTargetIds.size()) {
            break;
          }
          paramString = str;
          if (i > 0) {
            paramString = str + ", ";
          }
          str = paramString + this.mTargetIds.get(i);
        }
      }
      str = paramString;
      if (this.mTargets.size() > 0) {
        for (i = 0;; i++)
        {
          str = paramString;
          if (i >= this.mTargets.size()) {
            break;
          }
          str = paramString;
          if (i > 0) {
            str = paramString + ", ";
          }
          paramString = str + this.mTargets.get(i);
        }
      }
      str = str + ")";
    }
    return str;
  }
  
  private static class AnimationInfo
  {
    String name;
    TransitionValues values;
    View view;
    WindowIdPort windowId;
    
    AnimationInfo(View paramView, String paramString, WindowIdPort paramWindowIdPort, TransitionValues paramTransitionValues)
    {
      this.view = paramView;
      this.name = paramString;
      this.values = paramTransitionValues;
      this.windowId = paramWindowIdPort;
    }
  }
  
  private static class ArrayListManager
  {
    static <T> ArrayList<T> add(ArrayList<T> paramArrayList, T paramT)
    {
      Object localObject = paramArrayList;
      if (paramArrayList == null) {
        localObject = new ArrayList();
      }
      if (!((ArrayList)localObject).contains(paramT)) {
        ((ArrayList)localObject).add(paramT);
      }
      return (ArrayList<T>)localObject;
    }
    
    static <T> ArrayList<T> remove(ArrayList<T> paramArrayList, T paramT)
    {
      ArrayList<T> localArrayList = paramArrayList;
      if (paramArrayList != null)
      {
        paramArrayList.remove(paramT);
        localArrayList = paramArrayList;
        if (paramArrayList.isEmpty()) {
          localArrayList = null;
        }
      }
      return localArrayList;
    }
  }
  
  public static abstract interface TransitionListener
  {
    public abstract void onTransitionCancel(TransitionPort paramTransitionPort);
    
    public abstract void onTransitionEnd(TransitionPort paramTransitionPort);
    
    public abstract void onTransitionPause(TransitionPort paramTransitionPort);
    
    public abstract void onTransitionResume(TransitionPort paramTransitionPort);
    
    public abstract void onTransitionStart(TransitionPort paramTransitionPort);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public static class TransitionListenerAdapter
    implements TransitionPort.TransitionListener
  {
    public void onTransitionCancel(TransitionPort paramTransitionPort) {}
    
    public void onTransitionEnd(TransitionPort paramTransitionPort) {}
    
    public void onTransitionPause(TransitionPort paramTransitionPort) {}
    
    public void onTransitionResume(TransitionPort paramTransitionPort) {}
    
    public void onTransitionStart(TransitionPort paramTransitionPort) {}
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\transition\TransitionPort.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */