package com.testfairy.p;

import android.util.Log;
import com.testfairy.e;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class q
  extends JSONObject
{
  private static final String b = "text";
  private static final String c = "children";
  private static final String d = "name";
  private static final String e = "x";
  private static final String f = "y";
  private static final String g = "width";
  private static final String h = "height";
  private static final String i = "visibility";
  private static final String j = "enabled";
  private static final String k = "focusable";
  private static final String l = "alpha";
  private static final String m = "id";
  private static final String n = "contentDescription";
  HashMap a = new HashMap() {};
  
  public q()
  {
    try
    {
      JSONArray localJSONArray = new org/json/JSONArray;
      localJSONArray.<init>();
      put("children", localJSONArray);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void a(float paramFloat)
  {
    try
    {
      put("alpha", paramFloat);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void a(int paramInt)
  {
    try
    {
      put("x", paramInt);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void a(String paramString)
  {
    try
    {
      put("name", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", paramString);
      }
    }
  }
  
  public void a(JSONObject paramJSONObject)
  {
    try
    {
      JSONArray localJSONArray = getJSONArray("children");
      localJSONArray.put(paramJSONObject);
      put("children", localJSONArray);
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", paramJSONObject);
      }
    }
  }
  
  public void a(boolean paramBoolean)
  {
    try
    {
      put("focusable", paramBoolean);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void b(int paramInt)
  {
    try
    {
      put("y", paramInt);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void b(String paramString)
  {
    try
    {
      put("text", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", paramString);
      }
    }
  }
  
  public void b(boolean paramBoolean)
  {
    try
    {
      put("enabled", paramBoolean);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void c(int paramInt)
  {
    try
    {
      put("width", paramInt);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void c(String paramString)
  {
    try
    {
      put("contentDescription", paramString);
      return;
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", paramString);
      }
    }
  }
  
  public void d(int paramInt)
  {
    try
    {
      put("height", paramInt);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void e(int paramInt)
  {
    try
    {
      put("visibility", this.a.get(Integer.valueOf(paramInt)));
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
  
  public void f(int paramInt)
  {
    try
    {
      put("id", paramInt);
      return;
    }
    catch (JSONException localJSONException)
    {
      for (;;)
      {
        Log.d(e.a, "ViewNode error", localJSONException);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */