package android.support.v7.recyclerview;

public final class R
{
  public static final class attr
  {
    public static final int layoutManager = 2130772308;
    public static final int reverseLayout = 2130772310;
    public static final int spanCount = 2130772309;
    public static final int stackFromEnd = 2130772311;
  }
  
  public static final class dimen
  {
    public static final int item_touch_helper_max_drag_scroll_per_frame = 2131230919;
    public static final int item_touch_helper_swipe_escape_max_velocity = 2131230920;
    public static final int item_touch_helper_swipe_escape_velocity = 2131230921;
  }
  
  public static final class id
  {
    public static final int item_touch_helper_previous_elevation = 2131623941;
  }
  
  public static final class styleable
  {
    public static final int[] RecyclerView = { 16842948, 16842993, 2130772308, 2130772309, 2130772310, 2130772311 };
    public static final int RecyclerView_android_descendantFocusability = 1;
    public static final int RecyclerView_android_orientation = 0;
    public static final int RecyclerView_layoutManager = 2;
    public static final int RecyclerView_reverseLayout = 4;
    public static final int RecyclerView_spanCount = 3;
    public static final int RecyclerView_stackFromEnd = 5;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\recyclerview\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */