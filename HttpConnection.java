package org.jsoup.helper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import org.jsoup.Connection;
import org.jsoup.Connection.Base;
import org.jsoup.Connection.KeyVal;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Request;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.parser.Parser;
import org.jsoup.parser.TokenQueue;

public class HttpConnection
  implements Connection
{
  public static final String CONTENT_ENCODING = "Content-Encoding";
  private static final String CONTENT_TYPE = "Content-Type";
  public static final String DEFAULT_UA = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36";
  private static final String FORM_URL_ENCODED = "application/x-www-form-urlencoded";
  private static final int HTTP_TEMP_REDIR = 307;
  private static final String MULTIPART_FORM_DATA = "multipart/form-data";
  private static final String USER_AGENT = "User-Agent";
  private Connection.Request req = new Request(null);
  private Connection.Response res = new Response();
  
  public static Connection connect(String paramString)
  {
    HttpConnection localHttpConnection = new HttpConnection();
    localHttpConnection.url(paramString);
    return localHttpConnection;
  }
  
  public static Connection connect(URL paramURL)
  {
    HttpConnection localHttpConnection = new HttpConnection();
    localHttpConnection.url(paramURL);
    return localHttpConnection;
  }
  
  private static String encodeMimeName(String paramString)
  {
    if (paramString == null) {}
    for (paramString = null;; paramString = paramString.replaceAll("\"", "%22")) {
      return paramString;
    }
  }
  
  private static String encodeUrl(String paramString)
  {
    try
    {
      Object localObject = new java/net/URL;
      ((URL)localObject).<init>(paramString);
      localObject = encodeUrl((URL)localObject).toExternalForm();
      paramString = (String)localObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return paramString;
  }
  
  private static URL encodeUrl(URL paramURL)
  {
    try
    {
      URI localURI = new java/net/URI;
      localURI.<init>(paramURL.getProtocol(), paramURL.getUserInfo(), paramURL.getHost(), paramURL.getPort(), paramURL.getPath(), paramURL.getQuery(), paramURL.getRef());
      URL localURL = new java/net/URL;
      localURL.<init>(localURI.toASCIIString());
      paramURL = localURL;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return paramURL;
  }
  
  private static boolean needsMultipart(Connection.Request paramRequest)
  {
    boolean bool2 = false;
    paramRequest = paramRequest.data().iterator();
    do
    {
      bool1 = bool2;
      if (!paramRequest.hasNext()) {
        break;
      }
    } while (!((Connection.KeyVal)paramRequest.next()).hasInputStream());
    boolean bool1 = true;
    return bool1;
  }
  
  public Connection cookie(String paramString1, String paramString2)
  {
    this.req.cookie(paramString1, paramString2);
    return this;
  }
  
  public Connection cookies(Map<String, String> paramMap)
  {
    Validate.notNull(paramMap, "Cookie map must not be null");
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      this.req.cookie((String)localEntry.getKey(), (String)localEntry.getValue());
    }
    return this;
  }
  
  public Connection.KeyVal data(String paramString)
  {
    Validate.notEmpty(paramString, "Data key must not be empty");
    Iterator localIterator = request().data().iterator();
    Connection.KeyVal localKeyVal;
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      localKeyVal = (Connection.KeyVal)localIterator.next();
    } while (!localKeyVal.key().equals(paramString));
    for (paramString = localKeyVal;; paramString = null) {
      return paramString;
    }
  }
  
  public Connection data(String paramString1, String paramString2)
  {
    this.req.data(KeyVal.create(paramString1, paramString2));
    return this;
  }
  
  public Connection data(String paramString1, String paramString2, InputStream paramInputStream)
  {
    this.req.data(KeyVal.create(paramString1, paramString2, paramInputStream));
    return this;
  }
  
  public Connection data(Collection<Connection.KeyVal> paramCollection)
  {
    Validate.notNull(paramCollection, "Data collection must not be null");
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      paramCollection = (Connection.KeyVal)localIterator.next();
      this.req.data(paramCollection);
    }
    return this;
  }
  
  public Connection data(Map<String, String> paramMap)
  {
    Validate.notNull(paramMap, "Data map must not be null");
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)paramMap.next();
      this.req.data(KeyVal.create((String)localEntry.getKey(), (String)localEntry.getValue()));
    }
    return this;
  }
  
  public Connection data(String... paramVarArgs)
  {
    Validate.notNull(paramVarArgs, "Data key value pairs must not be null");
    if (paramVarArgs.length % 2 == 0) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool, "Must supply an even number of key value pairs");
      for (int i = 0; i < paramVarArgs.length; i += 2)
      {
        String str1 = paramVarArgs[i];
        String str2 = paramVarArgs[(i + 1)];
        Validate.notEmpty(str1, "Data key must not be empty");
        Validate.notNull(str2, "Data value must not be null");
        this.req.data(KeyVal.create(str1, str2));
      }
    }
    return this;
  }
  
  public Connection.Response execute()
    throws IOException
  {
    this.res = Response.execute(this.req);
    return this.res;
  }
  
  public Connection followRedirects(boolean paramBoolean)
  {
    this.req.followRedirects(paramBoolean);
    return this;
  }
  
  public Document get()
    throws IOException
  {
    this.req.method(Connection.Method.GET);
    execute();
    return this.res.parse();
  }
  
  public Connection header(String paramString1, String paramString2)
  {
    this.req.header(paramString1, paramString2);
    return this;
  }
  
  public Connection headers(Map<String, String> paramMap)
  {
    Validate.notNull(paramMap, "Header map must not be null");
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      this.req.header((String)paramMap.getKey(), (String)paramMap.getValue());
    }
    return this;
  }
  
  public Connection ignoreContentType(boolean paramBoolean)
  {
    this.req.ignoreContentType(paramBoolean);
    return this;
  }
  
  public Connection ignoreHttpErrors(boolean paramBoolean)
  {
    this.req.ignoreHttpErrors(paramBoolean);
    return this;
  }
  
  public Connection maxBodySize(int paramInt)
  {
    this.req.maxBodySize(paramInt);
    return this;
  }
  
  public Connection method(Connection.Method paramMethod)
  {
    this.req.method(paramMethod);
    return this;
  }
  
  public Connection parser(Parser paramParser)
  {
    this.req.parser(paramParser);
    return this;
  }
  
  public Document post()
    throws IOException
  {
    this.req.method(Connection.Method.POST);
    execute();
    return this.res.parse();
  }
  
  public Connection postDataCharset(String paramString)
  {
    this.req.postDataCharset(paramString);
    return this;
  }
  
  public Connection proxy(String paramString, int paramInt)
  {
    this.req.proxy(paramString, paramInt);
    return this;
  }
  
  public Connection proxy(Proxy paramProxy)
  {
    this.req.proxy(paramProxy);
    return this;
  }
  
  public Connection referrer(String paramString)
  {
    Validate.notNull(paramString, "Referrer must not be null");
    this.req.header("Referer", paramString);
    return this;
  }
  
  public Connection.Request request()
  {
    return this.req;
  }
  
  public Connection request(Connection.Request paramRequest)
  {
    this.req = paramRequest;
    return this;
  }
  
  public Connection requestBody(String paramString)
  {
    this.req.requestBody(paramString);
    return this;
  }
  
  public Connection.Response response()
  {
    return this.res;
  }
  
  public Connection response(Connection.Response paramResponse)
  {
    this.res = paramResponse;
    return this;
  }
  
  public Connection timeout(int paramInt)
  {
    this.req.timeout(paramInt);
    return this;
  }
  
  public Connection url(String paramString)
  {
    Validate.notEmpty(paramString, "Must supply a valid URL");
    try
    {
      Connection.Request localRequest = this.req;
      URL localURL = new java/net/URL;
      localURL.<init>(encodeUrl(paramString));
      localRequest.url(localURL);
      return this;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new IllegalArgumentException("Malformed URL: " + paramString, localMalformedURLException);
    }
  }
  
  public Connection url(URL paramURL)
  {
    this.req.url(paramURL);
    return this;
  }
  
  public Connection userAgent(String paramString)
  {
    Validate.notNull(paramString, "User agent must not be null");
    this.req.header("User-Agent", paramString);
    return this;
  }
  
  public Connection validateTLSCertificates(boolean paramBoolean)
  {
    this.req.validateTLSCertificates(paramBoolean);
    return this;
  }
  
  private static abstract class Base<T extends Connection.Base>
    implements Connection.Base<T>
  {
    Map<String, String> cookies = new LinkedHashMap();
    Map<String, String> headers = new LinkedHashMap();
    Connection.Method method;
    URL url;
    
    private static String fixHeaderEncoding(String paramString)
    {
      try
      {
        byte[] arrayOfByte = paramString.getBytes("ISO-8859-1");
        if (!looksLikeUtf8(arrayOfByte)) {}
        for (;;)
        {
          return paramString;
          String str = new java/lang/String;
          str.<init>(arrayOfByte, "UTF-8");
          paramString = str;
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        for (;;) {}
      }
    }
    
    private String getHeaderCaseInsensitive(String paramString)
    {
      Validate.notNull(paramString, "Header name must not be null");
      Object localObject2 = (String)this.headers.get(paramString);
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = (String)this.headers.get(paramString.toLowerCase());
      }
      localObject2 = localObject1;
      if (localObject1 == null)
      {
        paramString = scanHeaders(paramString);
        localObject2 = localObject1;
        if (paramString != null) {
          localObject2 = (String)paramString.getValue();
        }
      }
      return (String)localObject2;
    }
    
    private static boolean looksLikeUtf8(byte[] paramArrayOfByte)
    {
      boolean bool2 = false;
      int m = 0;
      int i = m;
      int j;
      if (paramArrayOfByte.length >= 3)
      {
        i = m;
        if ((paramArrayOfByte[0] & 0xFF) == 239)
        {
          if ((paramArrayOfByte[1] & 0xFF) != 187) {
            break label103;
          }
          j = 1;
          if ((paramArrayOfByte[2] & 0xFF) != 191) {
            break label108;
          }
        }
      }
      label103:
      label108:
      for (int k = 1;; k = 0)
      {
        i = m;
        if ((j & k) != 0) {
          i = 3;
        }
        m = paramArrayOfByte.length;
        for (;;)
        {
          if (i >= m) {
            break label201;
          }
          j = paramArrayOfByte[i];
          if ((j & 0x80) != 0) {
            break;
          }
          k = i;
          i = k + 1;
        }
        j = 0;
        break;
      }
      boolean bool1;
      if ((j & 0xE0) == 192)
      {
        j = i + 1;
        do
        {
          k = i;
          if (i >= j) {
            break;
          }
          k = i + 1;
          i = k;
        } while ((paramArrayOfByte[k] & 0xC0) == 128);
        bool1 = bool2;
      }
      for (;;)
      {
        return bool1;
        if ((j & 0xF0) == 224)
        {
          j = i + 2;
          break;
        }
        bool1 = bool2;
        if ((j & 0xF8) == 240)
        {
          j = i + 3;
          break;
          label201:
          bool1 = true;
        }
      }
    }
    
    private Map.Entry<String, String> scanHeaders(String paramString)
    {
      String str = paramString.toLowerCase();
      Iterator localIterator = this.headers.entrySet().iterator();
      do
      {
        if (!localIterator.hasNext()) {
          break;
        }
        paramString = (Map.Entry)localIterator.next();
      } while (!((String)paramString.getKey()).toLowerCase().equals(str));
      for (;;)
      {
        return paramString;
        paramString = null;
      }
    }
    
    public String cookie(String paramString)
    {
      Validate.notEmpty(paramString, "Cookie name must not be empty");
      return (String)this.cookies.get(paramString);
    }
    
    public T cookie(String paramString1, String paramString2)
    {
      Validate.notEmpty(paramString1, "Cookie name must not be empty");
      Validate.notNull(paramString2, "Cookie value must not be null");
      this.cookies.put(paramString1, paramString2);
      return this;
    }
    
    public Map<String, String> cookies()
    {
      return this.cookies;
    }
    
    public boolean hasCookie(String paramString)
    {
      Validate.notEmpty(paramString, "Cookie name must not be empty");
      return this.cookies.containsKey(paramString);
    }
    
    public boolean hasHeader(String paramString)
    {
      Validate.notEmpty(paramString, "Header name must not be empty");
      if (getHeaderCaseInsensitive(paramString) != null) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public boolean hasHeaderWithValue(String paramString1, String paramString2)
    {
      if ((hasHeader(paramString1)) && (header(paramString1).equalsIgnoreCase(paramString2))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String header(String paramString)
    {
      Validate.notNull(paramString, "Header name must not be null");
      String str = getHeaderCaseInsensitive(paramString);
      paramString = str;
      if (str != null) {
        paramString = fixHeaderEncoding(str);
      }
      return paramString;
    }
    
    public T header(String paramString1, String paramString2)
    {
      Validate.notEmpty(paramString1, "Header name must not be empty");
      Validate.notNull(paramString2, "Header value must not be null");
      removeHeader(paramString1);
      this.headers.put(paramString1, paramString2);
      return this;
    }
    
    public Map<String, String> headers()
    {
      return this.headers;
    }
    
    public T method(Connection.Method paramMethod)
    {
      Validate.notNull(paramMethod, "Method must not be null");
      this.method = paramMethod;
      return this;
    }
    
    public Connection.Method method()
    {
      return this.method;
    }
    
    public T removeCookie(String paramString)
    {
      Validate.notEmpty(paramString, "Cookie name must not be empty");
      this.cookies.remove(paramString);
      return this;
    }
    
    public T removeHeader(String paramString)
    {
      Validate.notEmpty(paramString, "Header name must not be empty");
      paramString = scanHeaders(paramString);
      if (paramString != null) {
        this.headers.remove(paramString.getKey());
      }
      return this;
    }
    
    public URL url()
    {
      return this.url;
    }
    
    public T url(URL paramURL)
    {
      Validate.notNull(paramURL, "URL must not be null");
      this.url = paramURL;
      return this;
    }
  }
  
  public static class KeyVal
    implements Connection.KeyVal
  {
    private String key;
    private InputStream stream;
    private String value;
    
    public static KeyVal create(String paramString1, String paramString2)
    {
      return new KeyVal().key(paramString1).value(paramString2);
    }
    
    public static KeyVal create(String paramString1, String paramString2, InputStream paramInputStream)
    {
      return new KeyVal().key(paramString1).value(paramString2).inputStream(paramInputStream);
    }
    
    public boolean hasInputStream()
    {
      if (this.stream != null) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public InputStream inputStream()
    {
      return this.stream;
    }
    
    public KeyVal inputStream(InputStream paramInputStream)
    {
      Validate.notNull(this.value, "Data input stream must not be null");
      this.stream = paramInputStream;
      return this;
    }
    
    public String key()
    {
      return this.key;
    }
    
    public KeyVal key(String paramString)
    {
      Validate.notEmpty(paramString, "Data key must not be empty");
      this.key = paramString;
      return this;
    }
    
    public String toString()
    {
      return this.key + "=" + this.value;
    }
    
    public String value()
    {
      return this.value;
    }
    
    public KeyVal value(String paramString)
    {
      Validate.notNull(paramString, "Data value must not be null");
      this.value = paramString;
      return this;
    }
  }
  
  public static class Request
    extends HttpConnection.Base<Connection.Request>
    implements Connection.Request
  {
    private String body = null;
    private Collection<Connection.KeyVal> data = new ArrayList();
    private boolean followRedirects = true;
    private boolean ignoreContentType = false;
    private boolean ignoreHttpErrors = false;
    private int maxBodySizeBytes = 1048576;
    private Parser parser;
    private boolean parserDefined = false;
    private String postDataCharset = "UTF-8";
    private Proxy proxy;
    private int timeoutMilliseconds = 30000;
    private boolean validateTSLCertificates = true;
    
    private Request()
    {
      super();
      this.method = Connection.Method.GET;
      this.headers.put("Accept-Encoding", "gzip");
      this.headers.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36");
      this.parser = Parser.htmlParser();
    }
    
    public Collection<Connection.KeyVal> data()
    {
      return this.data;
    }
    
    public Request data(Connection.KeyVal paramKeyVal)
    {
      Validate.notNull(paramKeyVal, "Key val must not be null");
      this.data.add(paramKeyVal);
      return this;
    }
    
    public Connection.Request followRedirects(boolean paramBoolean)
    {
      this.followRedirects = paramBoolean;
      return this;
    }
    
    public boolean followRedirects()
    {
      return this.followRedirects;
    }
    
    public Connection.Request ignoreContentType(boolean paramBoolean)
    {
      this.ignoreContentType = paramBoolean;
      return this;
    }
    
    public boolean ignoreContentType()
    {
      return this.ignoreContentType;
    }
    
    public Connection.Request ignoreHttpErrors(boolean paramBoolean)
    {
      this.ignoreHttpErrors = paramBoolean;
      return this;
    }
    
    public boolean ignoreHttpErrors()
    {
      return this.ignoreHttpErrors;
    }
    
    public int maxBodySize()
    {
      return this.maxBodySizeBytes;
    }
    
    public Connection.Request maxBodySize(int paramInt)
    {
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        Validate.isTrue(bool, "maxSize must be 0 (unlimited) or larger");
        this.maxBodySizeBytes = paramInt;
        return this;
      }
    }
    
    public Request parser(Parser paramParser)
    {
      this.parser = paramParser;
      this.parserDefined = true;
      return this;
    }
    
    public Parser parser()
    {
      return this.parser;
    }
    
    public String postDataCharset()
    {
      return this.postDataCharset;
    }
    
    public Connection.Request postDataCharset(String paramString)
    {
      Validate.notNull(paramString, "Charset must not be null");
      if (!Charset.isSupported(paramString)) {
        throw new IllegalCharsetNameException(paramString);
      }
      this.postDataCharset = paramString;
      return this;
    }
    
    public Proxy proxy()
    {
      return this.proxy;
    }
    
    public Request proxy(String paramString, int paramInt)
    {
      this.proxy = new Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(paramString, paramInt));
      return this;
    }
    
    public Request proxy(Proxy paramProxy)
    {
      this.proxy = paramProxy;
      return this;
    }
    
    public String requestBody()
    {
      return this.body;
    }
    
    public Connection.Request requestBody(String paramString)
    {
      this.body = paramString;
      return this;
    }
    
    public int timeout()
    {
      return this.timeoutMilliseconds;
    }
    
    public Request timeout(int paramInt)
    {
      if (paramInt >= 0) {}
      for (boolean bool = true;; bool = false)
      {
        Validate.isTrue(bool, "Timeout milliseconds must be 0 (infinite) or greater");
        this.timeoutMilliseconds = paramInt;
        return this;
      }
    }
    
    public void validateTLSCertificates(boolean paramBoolean)
    {
      this.validateTSLCertificates = paramBoolean;
    }
    
    public boolean validateTLSCertificates()
    {
      return this.validateTSLCertificates;
    }
  }
  
  public static class Response
    extends HttpConnection.Base<Connection.Response>
    implements Connection.Response
  {
    private static final String LOCATION = "Location";
    private static final int MAX_REDIRECTS = 20;
    private static SSLSocketFactory sslSocketFactory;
    private static final Pattern xmlContentTypeRxp = Pattern.compile("(application|text)/\\w*\\+?xml.*");
    private ByteBuffer byteData;
    private String charset;
    private String contentType;
    private boolean executed = false;
    private int numRedirects = 0;
    private Connection.Request req;
    private int statusCode;
    private String statusMessage;
    
    Response()
    {
      super();
    }
    
    private Response(Response paramResponse)
      throws IOException
    {
      super();
      if (paramResponse != null)
      {
        paramResponse.numRedirects += 1;
        if (this.numRedirects >= 20) {
          throw new IOException(String.format("Too many redirects occurred trying to load URL %s", new Object[] { paramResponse.url() }));
        }
      }
    }
    
    private static HttpURLConnection createConnection(Connection.Request paramRequest)
      throws IOException
    {
      if (paramRequest.proxy() == null) {}
      for (Object localObject = paramRequest.url().openConnection();; localObject = paramRequest.url().openConnection(paramRequest.proxy()))
      {
        localObject = (HttpURLConnection)localObject;
        ((HttpURLConnection)localObject).setRequestMethod(paramRequest.method().name());
        ((HttpURLConnection)localObject).setInstanceFollowRedirects(false);
        ((HttpURLConnection)localObject).setConnectTimeout(paramRequest.timeout());
        ((HttpURLConnection)localObject).setReadTimeout(paramRequest.timeout());
        if (((localObject instanceof HttpsURLConnection)) && (!paramRequest.validateTLSCertificates()))
        {
          initUnSecureTSL();
          ((HttpsURLConnection)localObject).setSSLSocketFactory(sslSocketFactory);
          ((HttpsURLConnection)localObject).setHostnameVerifier(getInsecureVerifier());
        }
        if (paramRequest.method().hasBody()) {
          ((HttpURLConnection)localObject).setDoOutput(true);
        }
        if (paramRequest.cookies().size() > 0) {
          ((HttpURLConnection)localObject).addRequestProperty("Cookie", getRequestCookieString(paramRequest));
        }
        paramRequest = paramRequest.headers().entrySet().iterator();
        while (paramRequest.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)paramRequest.next();
          ((HttpURLConnection)localObject).addRequestProperty((String)localEntry.getKey(), (String)localEntry.getValue());
        }
      }
      return (HttpURLConnection)localObject;
    }
    
    private static LinkedHashMap<String, List<String>> createHeaderMap(HttpURLConnection paramHttpURLConnection)
    {
      LinkedHashMap localLinkedHashMap = new LinkedHashMap();
      int i = 0;
      for (;;)
      {
        String str2 = paramHttpURLConnection.getHeaderFieldKey(i);
        String str1 = paramHttpURLConnection.getHeaderField(i);
        if ((str2 == null) && (str1 == null)) {
          return localLinkedHashMap;
        }
        int j = i + 1;
        i = j;
        if (str2 != null)
        {
          i = j;
          if (str1 != null) {
            if (localLinkedHashMap.containsKey(str2))
            {
              ((List)localLinkedHashMap.get(str2)).add(str1);
              i = j;
            }
            else
            {
              ArrayList localArrayList = new ArrayList();
              localArrayList.add(str1);
              localLinkedHashMap.put(str2, localArrayList);
              i = j;
            }
          }
        }
      }
    }
    
    static Response execute(Connection.Request paramRequest)
      throws IOException
    {
      return execute(paramRequest, null);
    }
    
    static Response execute(Connection.Request paramRequest, Response paramResponse)
      throws IOException
    {
      Validate.notNull(paramRequest, "Request must not be null");
      Object localObject1 = paramRequest.url().getProtocol();
      if ((!((String)localObject1).equals("http")) && (!((String)localObject1).equals("https"))) {
        throw new MalformedURLException("Only http & https protocols supported");
      }
      boolean bool2 = paramRequest.method().hasBody();
      boolean bool1;
      if (paramRequest.requestBody() != null)
      {
        bool1 = true;
        if (!bool2) {
          Validate.isFalse(bool1, "Cannot set a request body for HTTP method " + paramRequest.method());
        }
        localObject1 = null;
        if ((paramRequest.data().size() <= 0) || ((bool2) && (!bool1))) {
          break label397;
        }
        serialiseRequestUrl(paramRequest);
      }
      HttpURLConnection localHttpURLConnection;
      int i;
      Response localResponse;
      for (;;)
      {
        localHttpURLConnection = createConnection(paramRequest);
        try
        {
          localHttpURLConnection.connect();
          if (localHttpURLConnection.getDoOutput()) {
            writePost(paramRequest, localHttpURLConnection.getOutputStream(), (String)localObject1);
          }
          i = localHttpURLConnection.getResponseCode();
          localResponse = new org/jsoup/helper/HttpConnection$Response;
          localResponse.<init>(paramResponse);
          localResponse.setupFromConnection(localHttpURLConnection, paramResponse);
          localResponse.req = paramRequest;
          if ((!localResponse.hasHeader("Location")) || (!paramRequest.followRedirects())) {
            break label425;
          }
          if (i != 307)
          {
            paramRequest.method(Connection.Method.GET);
            paramRequest.data().clear();
          }
          localObject1 = localResponse.header("Location");
          paramResponse = (Response)localObject1;
          if (localObject1 != null)
          {
            paramResponse = (Response)localObject1;
            if (((String)localObject1).startsWith("http:/"))
            {
              paramResponse = (Response)localObject1;
              if (((String)localObject1).charAt(6) != '/') {
                paramResponse = ((String)localObject1).substring(6);
              }
            }
          }
          paramRequest.url(HttpConnection.encodeUrl(StringUtil.resolve(paramRequest.url(), paramResponse)));
          localObject1 = localResponse.cookies.entrySet().iterator();
          while (((Iterator)localObject1).hasNext())
          {
            paramResponse = (Map.Entry)((Iterator)localObject1).next();
            paramRequest.cookie((String)paramResponse.getKey(), (String)paramResponse.getValue());
          }
          bool1 = false;
        }
        finally
        {
          localHttpURLConnection.disconnect();
        }
        break;
        label397:
        if (bool2) {
          localObject1 = setOutputContentType(paramRequest);
        }
      }
      paramRequest = execute(paramRequest, localResponse);
      localHttpURLConnection.disconnect();
      return paramRequest;
      label425:
      if (((i < 200) || (i >= 400)) && (!paramRequest.ignoreHttpErrors()))
      {
        paramResponse = new org/jsoup/HttpStatusException;
        paramResponse.<init>("HTTP error fetching URL", i, paramRequest.url().toString());
        throw paramResponse;
      }
      paramResponse = localResponse.contentType();
      if ((paramResponse != null) && (!paramRequest.ignoreContentType()) && (!paramResponse.startsWith("text/")) && (!xmlContentTypeRxp.matcher(paramResponse).matches()))
      {
        localObject1 = new org/jsoup/UnsupportedMimeTypeException;
        ((UnsupportedMimeTypeException)localObject1).<init>("Unhandled content type. Must be text/*, application/xml, or application/xhtml+xml", paramResponse, paramRequest.url().toString());
        throw ((Throwable)localObject1);
      }
      if ((paramResponse != null) && (xmlContentTypeRxp.matcher(paramResponse).matches()) && ((paramRequest instanceof HttpConnection.Request)) && (!((HttpConnection.Request)paramRequest).parserDefined)) {
        paramRequest.parser(Parser.xmlParser());
      }
      localResponse.charset = DataUtil.getCharsetFromContentType(localResponse.contentType);
      if (localHttpURLConnection.getContentLength() != 0)
      {
        paramResponse = paramRequest.method();
        localObject1 = Connection.Method.HEAD;
        if (paramResponse != localObject1)
        {
          paramResponse = null;
          localObject1 = paramResponse;
        }
      }
      for (;;)
      {
        try
        {
          if (localHttpURLConnection.getErrorStream() != null)
          {
            localObject1 = paramResponse;
            paramResponse = localHttpURLConnection.getErrorStream();
            Object localObject2 = paramResponse;
            localObject1 = paramResponse;
            if (localResponse.hasHeaderWithValue("Content-Encoding", "gzip"))
            {
              localObject1 = paramResponse;
              localObject2 = new java/util/zip/GZIPInputStream;
              localObject1 = paramResponse;
              ((GZIPInputStream)localObject2).<init>(paramResponse);
            }
            localObject1 = localObject2;
            localResponse.byteData = DataUtil.readToByteBuffer((InputStream)localObject2, paramRequest.maxBodySize());
            if (localObject2 != null) {
              ((InputStream)localObject2).close();
            }
            localHttpURLConnection.disconnect();
            localResponse.executed = true;
            paramRequest = localResponse;
            break;
          }
          localObject1 = paramResponse;
          paramResponse = localHttpURLConnection.getInputStream();
          continue;
          localResponse.byteData = DataUtil.emptyByteBuffer();
        }
        finally
        {
          if (localObject1 != null) {
            ((InputStream)localObject1).close();
          }
        }
      }
    }
    
    private static HostnameVerifier getInsecureVerifier()
    {
      new HostnameVerifier()
      {
        public boolean verify(String paramAnonymousString, SSLSession paramAnonymousSSLSession)
        {
          return true;
        }
      };
    }
    
    private static String getRequestCookieString(Connection.Request paramRequest)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      int i = 1;
      Iterator localIterator = paramRequest.cookies().entrySet().iterator();
      if (localIterator.hasNext())
      {
        paramRequest = (Map.Entry)localIterator.next();
        if (i == 0) {
          localStringBuilder.append("; ");
        }
        for (;;)
        {
          localStringBuilder.append((String)paramRequest.getKey()).append('=').append((String)paramRequest.getValue());
          break;
          i = 0;
        }
      }
      return localStringBuilder.toString();
    }
    
    /* Error */
    private static void initUnSecureTSL()
      throws IOException
    {
      // Byte code:
      //   0: ldc 2
      //   2: monitorenter
      //   3: getstatic 134	org/jsoup/helper/HttpConnection$Response:sslSocketFactory	Ljavax/net/ssl/SSLSocketFactory;
      //   6: ifnonnull +47 -> 53
      //   9: new 14	org/jsoup/helper/HttpConnection$Response$2
      //   12: astore_2
      //   13: aload_2
      //   14: invokespecial 501	org/jsoup/helper/HttpConnection$Response$2:<init>	()V
      //   17: ldc_w 503
      //   20: invokestatic 509	javax/net/ssl/SSLContext:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
      //   23: astore_0
      //   24: new 511	java/security/SecureRandom
      //   27: astore_1
      //   28: aload_1
      //   29: invokespecial 512	java/security/SecureRandom:<init>	()V
      //   32: aload_0
      //   33: aconst_null
      //   34: iconst_1
      //   35: anewarray 514	javax/net/ssl/TrustManager
      //   38: dup
      //   39: iconst_0
      //   40: aload_2
      //   41: aastore
      //   42: aload_1
      //   43: invokevirtual 518	javax/net/ssl/SSLContext:init	([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
      //   46: aload_0
      //   47: invokevirtual 522	javax/net/ssl/SSLContext:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
      //   50: putstatic 134	org/jsoup/helper/HttpConnection$Response:sslSocketFactory	Ljavax/net/ssl/SSLSocketFactory;
      //   53: ldc 2
      //   55: monitorexit
      //   56: return
      //   57: astore_0
      //   58: new 60	java/io/IOException
      //   61: astore_0
      //   62: aload_0
      //   63: ldc_w 524
      //   66: invokespecial 77	java/io/IOException:<init>	(Ljava/lang/String;)V
      //   69: aload_0
      //   70: athrow
      //   71: astore_0
      //   72: ldc 2
      //   74: monitorexit
      //   75: aload_0
      //   76: athrow
      //   77: astore_0
      //   78: new 60	java/io/IOException
      //   81: astore_0
      //   82: aload_0
      //   83: ldc_w 524
      //   86: invokespecial 77	java/io/IOException:<init>	(Ljava/lang/String;)V
      //   89: aload_0
      //   90: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   23	24	0	localSSLContext	javax.net.ssl.SSLContext
      //   57	1	0	localNoSuchAlgorithmException	java.security.NoSuchAlgorithmException
      //   61	9	0	localIOException1	IOException
      //   71	5	0	localObject	Object
      //   77	1	0	localKeyManagementException	java.security.KeyManagementException
      //   81	9	0	localIOException2	IOException
      //   27	16	1	localSecureRandom	java.security.SecureRandom
      //   12	29	2	local2	2
      // Exception table:
      //   from	to	target	type
      //   17	53	57	java/security/NoSuchAlgorithmException
      //   3	17	71	finally
      //   17	53	71	finally
      //   58	71	71	finally
      //   78	91	71	finally
      //   17	53	77	java/security/KeyManagementException
    }
    
    private static void serialiseRequestUrl(Connection.Request paramRequest)
      throws IOException
    {
      Object localObject = paramRequest.url();
      StringBuilder localStringBuilder = new StringBuilder();
      int i = 1;
      localStringBuilder.append(((URL)localObject).getProtocol()).append("://").append(((URL)localObject).getAuthority()).append(((URL)localObject).getPath()).append("?");
      if (((URL)localObject).getQuery() != null)
      {
        localStringBuilder.append(((URL)localObject).getQuery());
        i = 0;
      }
      localObject = paramRequest.data().iterator();
      if (((Iterator)localObject).hasNext())
      {
        Connection.KeyVal localKeyVal = (Connection.KeyVal)((Iterator)localObject).next();
        Validate.isFalse(localKeyVal.hasInputStream(), "InputStream data not supported in URL query string.");
        if (i == 0) {
          localStringBuilder.append('&');
        }
        for (;;)
        {
          localStringBuilder.append(URLEncoder.encode(localKeyVal.key(), "UTF-8")).append('=').append(URLEncoder.encode(localKeyVal.value(), "UTF-8"));
          break;
          i = 0;
        }
      }
      paramRequest.url(new URL(localStringBuilder.toString()));
      paramRequest.data().clear();
    }
    
    private static String setOutputContentType(Connection.Request paramRequest)
    {
      String str = null;
      if (paramRequest.hasHeader("Content-Type")) {
        paramRequest = str;
      }
      for (;;)
      {
        return paramRequest;
        if (HttpConnection.needsMultipart(paramRequest))
        {
          str = DataUtil.mimeBoundary();
          paramRequest.header("Content-Type", "multipart/form-data; boundary=" + str);
          paramRequest = str;
        }
        else
        {
          paramRequest.header("Content-Type", "application/x-www-form-urlencoded; charset=" + paramRequest.postDataCharset());
          paramRequest = str;
        }
      }
    }
    
    private void setupFromConnection(HttpURLConnection paramHttpURLConnection, Connection.Response paramResponse)
      throws IOException
    {
      this.method = Connection.Method.valueOf(paramHttpURLConnection.getRequestMethod());
      this.url = paramHttpURLConnection.getURL();
      this.statusCode = paramHttpURLConnection.getResponseCode();
      this.statusMessage = paramHttpURLConnection.getResponseMessage();
      this.contentType = paramHttpURLConnection.getContentType();
      processResponseHeaders(createHeaderMap(paramHttpURLConnection));
      if (paramResponse != null)
      {
        paramHttpURLConnection = paramResponse.cookies().entrySet().iterator();
        while (paramHttpURLConnection.hasNext())
        {
          paramResponse = (Map.Entry)paramHttpURLConnection.next();
          if (!hasCookie((String)paramResponse.getKey())) {
            cookie((String)paramResponse.getKey(), (String)paramResponse.getValue());
          }
        }
      }
    }
    
    private static void writePost(Connection.Request paramRequest, OutputStream paramOutputStream, String paramString)
      throws IOException
    {
      Object localObject = paramRequest.data();
      BufferedWriter localBufferedWriter = new BufferedWriter(new OutputStreamWriter(paramOutputStream, paramRequest.postDataCharset()));
      if (paramString != null)
      {
        paramRequest = ((Collection)localObject).iterator();
        if (paramRequest.hasNext())
        {
          localObject = (Connection.KeyVal)paramRequest.next();
          localBufferedWriter.write("--");
          localBufferedWriter.write(paramString);
          localBufferedWriter.write("\r\n");
          localBufferedWriter.write("Content-Disposition: form-data; name=\"");
          localBufferedWriter.write(HttpConnection.encodeMimeName(((Connection.KeyVal)localObject).key()));
          localBufferedWriter.write("\"");
          if (((Connection.KeyVal)localObject).hasInputStream())
          {
            localBufferedWriter.write("; filename=\"");
            localBufferedWriter.write(HttpConnection.encodeMimeName(((Connection.KeyVal)localObject).value()));
            localBufferedWriter.write("\"\r\nContent-Type: application/octet-stream\r\n\r\n");
            localBufferedWriter.flush();
            DataUtil.crossStreams(((Connection.KeyVal)localObject).inputStream(), paramOutputStream);
            paramOutputStream.flush();
          }
          for (;;)
          {
            localBufferedWriter.write("\r\n");
            break;
            localBufferedWriter.write("\r\n\r\n");
            localBufferedWriter.write(((Connection.KeyVal)localObject).value());
          }
        }
        localBufferedWriter.write("--");
        localBufferedWriter.write(paramString);
        localBufferedWriter.write("--");
      }
      for (;;)
      {
        localBufferedWriter.close();
        return;
        if (paramRequest.requestBody() == null) {
          break;
        }
        localBufferedWriter.write(paramRequest.requestBody());
      }
      int i = 1;
      paramOutputStream = ((Collection)localObject).iterator();
      label272:
      if (paramOutputStream.hasNext())
      {
        paramString = (Connection.KeyVal)paramOutputStream.next();
        if (i != 0) {
          break label353;
        }
        localBufferedWriter.append('&');
      }
      for (;;)
      {
        localBufferedWriter.write(URLEncoder.encode(paramString.key(), paramRequest.postDataCharset()));
        localBufferedWriter.write(61);
        localBufferedWriter.write(URLEncoder.encode(paramString.value(), paramRequest.postDataCharset()));
        break label272;
        break;
        label353:
        i = 0;
      }
    }
    
    public String body()
    {
      Validate.isTrue(this.executed, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
      if (this.charset == null) {}
      for (String str = Charset.forName("UTF-8").decode(this.byteData).toString();; str = Charset.forName(this.charset).decode(this.byteData).toString())
      {
        this.byteData.rewind();
        return str;
      }
    }
    
    public byte[] bodyAsBytes()
    {
      Validate.isTrue(this.executed, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
      return this.byteData.array();
    }
    
    public String charset()
    {
      return this.charset;
    }
    
    public Response charset(String paramString)
    {
      this.charset = paramString;
      return this;
    }
    
    public String contentType()
    {
      return this.contentType;
    }
    
    public Document parse()
      throws IOException
    {
      Validate.isTrue(this.executed, "Request must be executed (with .execute(), .get(), or .post() before parsing response");
      Document localDocument = DataUtil.parseByteData(this.byteData, this.charset, this.url.toExternalForm(), this.req.parser());
      this.byteData.rewind();
      this.charset = localDocument.outputSettings().charset().name();
      return localDocument;
    }
    
    void processResponseHeaders(Map<String, List<String>> paramMap)
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext())
      {
        Object localObject2 = (Map.Entry)paramMap.next();
        Object localObject1 = (String)((Map.Entry)localObject2).getKey();
        if (localObject1 != null)
        {
          localObject2 = (List)((Map.Entry)localObject2).getValue();
          Object localObject3;
          if (((String)localObject1).equalsIgnoreCase("Set-Cookie"))
          {
            localObject1 = ((List)localObject2).iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (String)((Iterator)localObject1).next();
              if (localObject2 != null)
              {
                localObject3 = new TokenQueue((String)localObject2);
                localObject2 = ((TokenQueue)localObject3).chompTo("=").trim();
                localObject3 = ((TokenQueue)localObject3).consumeTo(";").trim();
                if (((String)localObject2).length() > 0) {
                  cookie((String)localObject2, (String)localObject3);
                }
              }
            }
          }
          else if (((List)localObject2).size() == 1)
          {
            header((String)localObject1, (String)((List)localObject2).get(0));
          }
          else if (((List)localObject2).size() > 1)
          {
            localObject3 = new StringBuilder();
            for (int i = 0; i < ((List)localObject2).size(); i++)
            {
              String str = (String)((List)localObject2).get(i);
              if (i != 0) {
                ((StringBuilder)localObject3).append(", ");
              }
              ((StringBuilder)localObject3).append(str);
            }
            header((String)localObject1, ((StringBuilder)localObject3).toString());
          }
        }
      }
    }
    
    public int statusCode()
    {
      return this.statusCode;
    }
    
    public String statusMessage()
    {
      return this.statusMessage;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\helper\HttpConnection.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */