package io.fabric.sdk.android;

import android.os.SystemClock;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

class FabricKitsFinder
  implements Callable<Map<String, KitInfo>>
{
  private static final String FABRIC_BUILD_TYPE_KEY = "fabric-build-type";
  static final String FABRIC_DIR = "fabric/";
  private static final String FABRIC_IDENTIFIER_KEY = "fabric-identifier";
  private static final String FABRIC_VERSION_KEY = "fabric-version";
  final String apkFileName;
  
  FabricKitsFinder(String paramString)
  {
    this.apkFileName = paramString;
  }
  
  private Map<String, KitInfo> findImplicitKits()
  {
    HashMap localHashMap = new HashMap();
    try
    {
      Class.forName("com.google.android.gms.ads.AdView");
      KitInfo localKitInfo = new io/fabric/sdk/android/KitInfo;
      localKitInfo.<init>("com.google.firebase.firebase-ads", "0.0.0", "binary");
      localHashMap.put(localKitInfo.getIdentifier(), localKitInfo);
      Fabric.getLogger().v("Fabric", "Found kit: com.google.firebase.firebase-ads");
      return localHashMap;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private Map<String, KitInfo> findRegisteredKits()
    throws Exception
  {
    HashMap localHashMap = new HashMap();
    ZipFile localZipFile = loadApkFile();
    Enumeration localEnumeration = localZipFile.entries();
    while (localEnumeration.hasMoreElements())
    {
      Object localObject = (ZipEntry)localEnumeration.nextElement();
      if ((((ZipEntry)localObject).getName().startsWith("fabric/")) && (((ZipEntry)localObject).getName().length() > "fabric/".length()))
      {
        localObject = loadKitInfo((ZipEntry)localObject, localZipFile);
        if (localObject != null)
        {
          localHashMap.put(((KitInfo)localObject).getIdentifier(), localObject);
          Fabric.getLogger().v("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[] { ((KitInfo)localObject).getIdentifier(), ((KitInfo)localObject).getVersion() }));
        }
      }
    }
    if (localZipFile != null) {}
    try
    {
      localZipFile.close();
      return localHashMap;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  private KitInfo loadKitInfo(ZipEntry paramZipEntry, ZipFile paramZipFile)
  {
    Object localObject1 = null;
    ZipFile localZipFile = null;
    try
    {
      paramZipFile = paramZipFile.getInputStream(paramZipEntry);
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      localObject4 = new java/util/Properties;
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      ((Properties)localObject4).<init>();
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      ((Properties)localObject4).load(paramZipFile);
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      localObject2 = ((Properties)localObject4).getProperty("fabric-identifier");
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      Object localObject3 = ((Properties)localObject4).getProperty("fabric-version");
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      localObject4 = ((Properties)localObject4).getProperty("fabric-build-type");
      localZipFile = paramZipFile;
      localObject1 = paramZipFile;
      if (!TextUtils.isEmpty((CharSequence)localObject2))
      {
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        if (!TextUtils.isEmpty((CharSequence)localObject3)) {}
      }
      else
      {
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        localObject2 = new java/lang/IllegalStateException;
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        localObject3 = new java/lang/StringBuilder;
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        ((StringBuilder)localObject3).<init>();
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        ((IllegalStateException)localObject2).<init>("Invalid format of fabric file," + paramZipEntry.getName());
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        throw ((Throwable)localObject2);
      }
    }
    catch (IOException localIOException)
    {
      Object localObject4;
      localObject1 = localZipFile;
      paramZipFile = Fabric.getLogger();
      localObject1 = localZipFile;
      Object localObject2 = new java/lang/StringBuilder;
      localObject1 = localZipFile;
      ((StringBuilder)localObject2).<init>();
      localObject1 = localZipFile;
      paramZipFile.e("Fabric", "Error when parsing fabric properties " + paramZipEntry.getName(), localIOException);
      CommonUtils.closeQuietly(localZipFile);
      for (paramZipEntry = null;; paramZipEntry = (ZipEntry)localObject2)
      {
        return paramZipEntry;
        localZipFile = paramZipFile;
        localObject1 = paramZipFile;
        localObject2 = new KitInfo((String)localObject2, localIOException, (String)localObject4);
        CommonUtils.closeQuietly(paramZipFile);
      }
    }
    finally
    {
      CommonUtils.closeQuietly((Closeable)localObject1);
    }
  }
  
  public Map<String, KitInfo> call()
    throws Exception
  {
    HashMap localHashMap = new HashMap();
    long l = SystemClock.elapsedRealtime();
    localHashMap.putAll(findImplicitKits());
    localHashMap.putAll(findRegisteredKits());
    Fabric.getLogger().v("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - l));
    return localHashMap;
  }
  
  protected ZipFile loadApkFile()
    throws IOException
  {
    return new ZipFile(this.apkFileName);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\FabricKitsFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */