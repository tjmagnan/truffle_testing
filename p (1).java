package com.testfairy.p;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;
import android.util.Patterns;
import com.testfairy.e;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class a
{
  public static final String a = "Could not read accounts";
  public static final String b = "com.testfairy.app";
  
  public static String a(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList();
    try
    {
      for (paramContext : AccountManager.get(paramContext).getAccounts()) {
        if (Patterns.EMAIL_ADDRESS.matcher(paramContext.name).matches()) {
          localArrayList.add(a(paramContext.name));
        }
      }
      if (!localArrayList.isEmpty()) {}
    }
    catch (Throwable paramContext) {}
    for (paramContext = null;; paramContext = m.a(localArrayList)) {
      return paramContext;
    }
  }
  
  private static String a(String paramString)
  {
    try
    {
      byte[] arrayOfByte = MessageDigest.getInstance("MD5").digest(paramString.getBytes());
      paramString = new java/lang/StringBuffer;
      paramString.<init>();
      for (int i = 0; i < arrayOfByte.length; i++) {
        paramString.append(Integer.toHexString(arrayOfByte[i] & 0xFF | 0x100).substring(1, 3));
      }
      paramString = paramString.toString();
    }
    catch (Throwable paramString)
    {
      for (;;)
      {
        Log.e(e.a, "md5 Exception " + paramString.getMessage());
        paramString = "";
      }
    }
    return paramString;
  }
  
  public static List a(AccountManager paramAccountManager)
  {
    return a(paramAccountManager, null);
  }
  
  public static List a(AccountManager paramAccountManager, String paramString)
  {
    localArrayList = new ArrayList();
    try
    {
      for (paramString : paramAccountManager.getAccountsByType(paramString)) {
        if ((a(paramString)) && (!localArrayList.contains(paramString.name))) {
          localArrayList.add(paramString.name);
        }
      }
      return localArrayList;
    }
    catch (Throwable paramAccountManager)
    {
      Log.e(e.a, "Could not access accounts", paramAccountManager);
    }
  }
  
  public static List a(Context paramContext, String paramString)
  {
    try
    {
      paramContext = a((AccountManager)paramContext.getSystemService("account"), paramString);
      return paramContext;
    }
    catch (Exception paramContext)
    {
      for (;;)
      {
        Log.e(e.a, "Could not read accounts", paramContext);
        paramContext = new ArrayList(0);
      }
    }
  }
  
  private static boolean a(Account paramAccount)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramAccount != null)
    {
      if (paramAccount.name != null) {
        break label19;
      }
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      label19:
      int i = paramAccount.name.indexOf("@");
      bool1 = bool2;
      if (i >= 0)
      {
        bool1 = bool2;
        if (paramAccount.name.indexOf(".", i + 1) >= 0) {
          bool1 = true;
        }
      }
    }
  }
  
  public static List b(Context paramContext)
  {
    return a(paramContext, null);
  }
  
  public static boolean b(AccountManager paramAccountManager)
  {
    boolean bool = true;
    if (a(paramAccountManager).size() >= 1) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */