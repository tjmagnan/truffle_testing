package org.joda.time.format;

class InternalParserDateTimeParser
  implements DateTimeParser, InternalParser
{
  private final InternalParser underlying;
  
  private InternalParserDateTimeParser(InternalParser paramInternalParser)
  {
    this.underlying = paramInternalParser;
  }
  
  static DateTimeParser of(InternalParser paramInternalParser)
  {
    if ((paramInternalParser instanceof DateTimeParserInternalParser)) {
      paramInternalParser = ((DateTimeParserInternalParser)paramInternalParser).getUnderlying();
    }
    for (;;)
    {
      return paramInternalParser;
      if ((paramInternalParser instanceof DateTimeParser)) {
        paramInternalParser = (DateTimeParser)paramInternalParser;
      } else if (paramInternalParser == null) {
        paramInternalParser = null;
      } else {
        paramInternalParser = new InternalParserDateTimeParser(paramInternalParser);
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (paramObject == this) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject instanceof InternalParserDateTimeParser))
      {
        paramObject = (InternalParserDateTimeParser)paramObject;
        bool = this.underlying.equals(((InternalParserDateTimeParser)paramObject).underlying);
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public int estimateParsedLength()
  {
    return this.underlying.estimateParsedLength();
  }
  
  public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
  {
    return this.underlying.parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
  }
  
  public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, String paramString, int paramInt)
  {
    return this.underlying.parseInto(paramDateTimeParserBucket, paramString, paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\InternalParserDateTimeParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */