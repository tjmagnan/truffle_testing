package com.crashlytics.android.core;

class MiddleOutStrategy
  implements StackTraceTrimmingStrategy
{
  private final int trimmedSize;
  
  public MiddleOutStrategy(int paramInt)
  {
    this.trimmedSize = paramInt;
  }
  
  public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] paramArrayOfStackTraceElement)
  {
    if (paramArrayOfStackTraceElement.length <= this.trimmedSize) {}
    for (;;)
    {
      return paramArrayOfStackTraceElement;
      int i = this.trimmedSize / 2;
      int j = this.trimmedSize - i;
      StackTraceElement[] arrayOfStackTraceElement = new StackTraceElement[this.trimmedSize];
      System.arraycopy(paramArrayOfStackTraceElement, 0, arrayOfStackTraceElement, 0, j);
      System.arraycopy(paramArrayOfStackTraceElement, paramArrayOfStackTraceElement.length - i, arrayOfStackTraceElement, j, i);
      paramArrayOfStackTraceElement = arrayOfStackTraceElement;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\MiddleOutStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */