package net.sourceforge.zbar;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SymbolIterator
  implements Iterator<Symbol>
{
  private Symbol current;
  
  SymbolIterator(Symbol paramSymbol)
  {
    this.current = paramSymbol;
  }
  
  public boolean hasNext()
  {
    if (this.current != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Symbol next()
  {
    if (this.current == null) {
      throw new NoSuchElementException("access past end of SymbolIterator");
    }
    Symbol localSymbol = this.current;
    long l = this.current.next();
    if (l != 0L) {}
    for (this.current = new Symbol(l);; this.current = null) {
      return localSymbol;
    }
  }
  
  public void remove()
  {
    throw new UnsupportedOperationException("SymbolIterator is immutable");
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\net\sourceforge\zbar\SymbolIterator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */