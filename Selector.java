package org.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;

public class Selector
{
  private final Evaluator evaluator;
  private final Element root;
  
  private Selector(String paramString, Element paramElement)
  {
    Validate.notNull(paramString);
    paramString = paramString.trim();
    Validate.notEmpty(paramString);
    Validate.notNull(paramElement);
    this.evaluator = QueryParser.parse(paramString);
    this.root = paramElement;
  }
  
  private Selector(Evaluator paramEvaluator, Element paramElement)
  {
    Validate.notNull(paramEvaluator);
    Validate.notNull(paramElement);
    this.evaluator = paramEvaluator;
    this.root = paramElement;
  }
  
  static Elements filterOut(Collection<Element> paramCollection1, Collection<Element> paramCollection2)
  {
    Elements localElements = new Elements();
    Iterator localIterator1 = paramCollection1.iterator();
    while (localIterator1.hasNext())
    {
      paramCollection1 = (Element)localIterator1.next();
      int j = 0;
      Iterator localIterator2 = paramCollection2.iterator();
      do
      {
        i = j;
        if (!localIterator2.hasNext()) {
          break;
        }
      } while (!paramCollection1.equals((Element)localIterator2.next()));
      int i = 1;
      if (i == 0) {
        localElements.add(paramCollection1);
      }
    }
    return localElements;
  }
  
  private Elements select()
  {
    return Collector.collect(this.evaluator, this.root);
  }
  
  public static Elements select(String paramString, Iterable<Element> paramIterable)
  {
    Validate.notEmpty(paramString);
    Validate.notNull(paramIterable);
    Evaluator localEvaluator = QueryParser.parse(paramString);
    paramString = new ArrayList();
    IdentityHashMap localIdentityHashMap = new IdentityHashMap();
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      Iterator localIterator = select(localEvaluator, (Element)paramIterable.next()).iterator();
      while (localIterator.hasNext())
      {
        Element localElement = (Element)localIterator.next();
        if (!localIdentityHashMap.containsKey(localElement))
        {
          paramString.add(localElement);
          localIdentityHashMap.put(localElement, Boolean.TRUE);
        }
      }
    }
    return new Elements(paramString);
  }
  
  public static Elements select(String paramString, Element paramElement)
  {
    return new Selector(paramString, paramElement).select();
  }
  
  public static Elements select(Evaluator paramEvaluator, Element paramElement)
  {
    return new Selector(paramEvaluator, paramElement).select();
  }
  
  public static class SelectorParseException
    extends IllegalStateException
  {
    public SelectorParseException(String paramString, Object... paramVarArgs)
    {
      super();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\select\Selector.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */