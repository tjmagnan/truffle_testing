package com.testfairy.e;

import android.content.Context;
import android.util.Log;
import com.testfairy.e;

public class c
{
  private static final long a = 1000L;
  private static final long b = 900000L;
  private static final int c = 72;
  private static final int d = 48;
  private static final int e = 32;
  private static final float f = 0.5F;
  private static final float g = 5.0F;
  private Context h;
  
  public c()
  {
    this(null);
  }
  
  public c(Context paramContext)
  {
    this.h = paramContext;
  }
  
  private long a(String paramString)
  {
    long l;
    if (paramString.endsWith("m"))
    {
      paramString = paramString.substring(0, paramString.length() - 1);
      Log.v(e.a, "Maximum session length is " + paramString + " minutes");
      l = Long.valueOf(paramString).longValue() * 60L * 1000L;
    }
    for (;;)
    {
      return l;
      if (paramString.endsWith("s"))
      {
        paramString = paramString.substring(0, paramString.length() - 1);
        Log.v(e.a, "Maximum session length is " + paramString + " sec");
        l = Long.valueOf(paramString).longValue() * 1000L;
      }
      else if (paramString.equals("unlimited"))
      {
        l = -1L;
      }
      else
      {
        Log.v(e.a, "Unparsable session-length value '" + paramString + "'");
        l = 900000L;
      }
    }
  }
  
  private int b(String paramString)
  {
    int i = 72;
    if (paramString.equals("high")) {}
    for (;;)
    {
      return i;
      if (paramString.equals("medium")) {
        i = 48;
      } else if (paramString.equals("low")) {
        i = 32;
      }
    }
  }
  
  private float c(String paramString)
  {
    float f1 = 1.0F;
    if (this.h == null) {}
    for (;;)
    {
      return f1;
      if (!paramString.equals("high")) {
        f1 = 0.5F;
      }
    }
  }
  
  private long d(String paramString)
  {
    try
    {
      float f1 = Math.min(5.0F, Math.max(0.5F, Float.valueOf(paramString).floatValue()));
      l = (f1 * 1000.0F);
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        long l = 1000L;
      }
    }
    return l;
  }
  
  public a a(String[] paramArrayOfString)
  {
    b localb = new b();
    int j = paramArrayOfString.length;
    int i = 0;
    if (i < j)
    {
      String str = paramArrayOfString[i];
      if (str.equals("logcat")) {
        localb.a(true);
      }
      for (;;)
      {
        i++;
        break;
        if (str.equals("cpu"))
        {
          localb.f(true);
        }
        else if (str.equals("gps"))
        {
          localb.h(true);
        }
        else if (str.equals("wifi"))
        {
          localb.i(true);
        }
        else if (str.equals("battery"))
        {
          localb.l(true);
        }
        else if (str.equals("network"))
        {
          localb.m(true);
        }
        else if (str.equals("memory"))
        {
          localb.b(true);
        }
        else if (str.equals("mic"))
        {
          localb.j(true);
        }
        else if (str.equals("record-audio"))
        {
          localb.k(true);
        }
        else if (str.equals("phone-signal"))
        {
          localb.g(true);
        }
        else if (str.equals("hide-user-input"))
        {
          localb.p(true);
        }
        else if (str.equals("video"))
        {
          localb.c(true);
        }
        else if (str.equals("video=wifi"))
        {
          localb.c(true);
        }
        else if (str.equals("video-only-wifi"))
        {
          localb.d(true);
        }
        else if (str.equals("data-only-wifi"))
        {
          localb.d(true);
          localb.e(true);
        }
        else if (str.equals("shake"))
        {
          localb.n(true);
        }
        else if (str.equals("video-front-camera"))
        {
          localb.o(true);
        }
        else if (str.equals("record-on-background"))
        {
          localb.q(true);
        }
        else if (str.equals("anonymous"))
        {
          localb.r(true);
        }
        else if (str.equals("verify-email"))
        {
          localb.s(true);
        }
        else if (str.equals("anr"))
        {
          localb.t(true);
        }
        else if (str.equals("user-interactions"))
        {
          localb.u(true);
        }
        else if (str.startsWith("video-quality="))
        {
          str = str.substring("video-quality".length() + 1);
          localb.a(b(str));
          localb.a(c(str));
        }
        else if (str.startsWith("screenshot-interval="))
        {
          localb.b(d(str.substring("screenshot-interval".length() + 1)));
        }
        else if (str.startsWith("session-length="))
        {
          localb.a(a(str.substring("session-length".length() + 1)));
        }
      }
    }
    return localb;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\e\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */