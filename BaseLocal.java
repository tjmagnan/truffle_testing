package org.joda.time.base;

public abstract class BaseLocal
  extends AbstractPartial
{
  private static final long serialVersionUID = 276453175381783L;
  
  protected abstract long getLocalMillis();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\BaseLocal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */