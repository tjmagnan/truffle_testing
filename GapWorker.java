package android.support.v7.widget;

import android.support.annotation.Nullable;
import android.support.v4.os.TraceCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

final class GapWorker
  implements Runnable
{
  static final ThreadLocal<GapWorker> sGapWorker = new ThreadLocal();
  static Comparator<Task> sTaskComparator = new Comparator()
  {
    public int compare(GapWorker.Task paramAnonymousTask1, GapWorker.Task paramAnonymousTask2)
    {
      int m = -1;
      int k = 1;
      int i;
      int j;
      if (paramAnonymousTask1.view == null)
      {
        i = 1;
        if (paramAnonymousTask2.view != null) {
          break label48;
        }
        j = 1;
        label25:
        if (i == j) {
          break label59;
        }
        if (paramAnonymousTask1.view != null) {
          break label54;
        }
        i = k;
      }
      for (;;)
      {
        return i;
        i = 0;
        break;
        label48:
        j = 0;
        break label25;
        label54:
        i = -1;
        continue;
        label59:
        if (paramAnonymousTask1.immediate != paramAnonymousTask2.immediate)
        {
          if (paramAnonymousTask1.immediate) {}
          for (i = m;; i = 1) {
            break;
          }
        }
        i = paramAnonymousTask2.viewVelocity - paramAnonymousTask1.viewVelocity;
        if (i == 0)
        {
          i = paramAnonymousTask1.distanceToItem - paramAnonymousTask2.distanceToItem;
          if (i == 0) {
            i = 0;
          }
        }
      }
    }
  };
  long mFrameIntervalNs;
  long mPostTimeNs;
  ArrayList<RecyclerView> mRecyclerViews = new ArrayList();
  private ArrayList<Task> mTasks = new ArrayList();
  
  private void buildTaskList()
  {
    int n = this.mRecyclerViews.size();
    int k = 0;
    int i = 0;
    Object localObject;
    while (i < n)
    {
      localObject = (RecyclerView)this.mRecyclerViews.get(i);
      j = k;
      if (((RecyclerView)localObject).getWindowVisibility() == 0)
      {
        ((RecyclerView)localObject).mPrefetchRegistry.collectPrefetchPositionsFromView((RecyclerView)localObject, false);
        j = k + ((RecyclerView)localObject).mPrefetchRegistry.mCount;
      }
      i++;
      k = j;
    }
    this.mTasks.ensureCapacity(k);
    i = 0;
    int j = 0;
    if (j < n)
    {
      RecyclerView localRecyclerView = (RecyclerView)this.mRecyclerViews.get(j);
      int m;
      if (localRecyclerView.getWindowVisibility() != 0) {
        m = i;
      }
      LayoutPrefetchRegistryImpl localLayoutPrefetchRegistryImpl;
      int i1;
      do
      {
        j++;
        i = m;
        break;
        localLayoutPrefetchRegistryImpl = localRecyclerView.mPrefetchRegistry;
        i1 = Math.abs(localLayoutPrefetchRegistryImpl.mPrefetchDx) + Math.abs(localLayoutPrefetchRegistryImpl.mPrefetchDy);
        k = 0;
        m = i;
      } while (k >= localLayoutPrefetchRegistryImpl.mCount * 2);
      if (i >= this.mTasks.size())
      {
        localObject = new Task();
        this.mTasks.add(localObject);
        label195:
        m = localLayoutPrefetchRegistryImpl.mPrefetchArray[(k + 1)];
        if (m > i1) {
          break label281;
        }
      }
      label281:
      for (boolean bool = true;; bool = false)
      {
        ((Task)localObject).immediate = bool;
        ((Task)localObject).viewVelocity = i1;
        ((Task)localObject).distanceToItem = m;
        ((Task)localObject).view = localRecyclerView;
        ((Task)localObject).position = localLayoutPrefetchRegistryImpl.mPrefetchArray[k];
        i++;
        k += 2;
        break;
        localObject = (Task)this.mTasks.get(i);
        break label195;
      }
    }
    Collections.sort(this.mTasks, sTaskComparator);
  }
  
  private void flushTaskWithDeadline(Task paramTask, long paramLong)
  {
    if (paramTask.immediate) {}
    for (long l = Long.MAX_VALUE;; l = paramLong)
    {
      paramTask = prefetchPositionWithDeadline(paramTask.view, paramTask.position, l);
      if ((paramTask != null) && (paramTask.mNestedRecyclerView != null)) {
        prefetchInnerRecyclerViewWithDeadline((RecyclerView)paramTask.mNestedRecyclerView.get(), paramLong);
      }
      return;
    }
  }
  
  private void flushTasksWithDeadline(long paramLong)
  {
    for (int i = 0;; i++)
    {
      Task localTask;
      if (i < this.mTasks.size())
      {
        localTask = (Task)this.mTasks.get(i);
        if (localTask.view != null) {}
      }
      else
      {
        return;
      }
      flushTaskWithDeadline(localTask, paramLong);
      localTask.clear();
    }
  }
  
  static boolean isPrefetchPositionAttached(RecyclerView paramRecyclerView, int paramInt)
  {
    int j = paramRecyclerView.mChildHelper.getUnfilteredChildCount();
    int i = 0;
    if (i < j)
    {
      RecyclerView.ViewHolder localViewHolder = RecyclerView.getChildViewHolderInt(paramRecyclerView.mChildHelper.getUnfilteredChildAt(i));
      if ((localViewHolder.mPosition != paramInt) || (localViewHolder.isInvalid())) {}
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i++;
      break;
    }
  }
  
  private void prefetchInnerRecyclerViewWithDeadline(@Nullable RecyclerView paramRecyclerView, long paramLong)
  {
    if (paramRecyclerView == null) {}
    for (;;)
    {
      return;
      if ((paramRecyclerView.mDataSetHasChangedAfterLayout) && (paramRecyclerView.mChildHelper.getUnfilteredChildCount() != 0)) {
        paramRecyclerView.removeAndRecycleViews();
      }
      LayoutPrefetchRegistryImpl localLayoutPrefetchRegistryImpl = paramRecyclerView.mPrefetchRegistry;
      localLayoutPrefetchRegistryImpl.collectPrefetchPositionsFromView(paramRecyclerView, true);
      if (localLayoutPrefetchRegistryImpl.mCount == 0) {
        continue;
      }
      try
      {
        TraceCompat.beginSection("RV Nested Prefetch");
        paramRecyclerView.mState.prepareForNestedPrefetch(paramRecyclerView.mAdapter);
        for (int i = 0; i < localLayoutPrefetchRegistryImpl.mCount * 2; i += 2) {
          prefetchPositionWithDeadline(paramRecyclerView, localLayoutPrefetchRegistryImpl.mPrefetchArray[i], paramLong);
        }
        TraceCompat.endSection();
      }
      finally
      {
        TraceCompat.endSection();
      }
    }
  }
  
  private RecyclerView.ViewHolder prefetchPositionWithDeadline(RecyclerView paramRecyclerView, int paramInt, long paramLong)
  {
    if (isPrefetchPositionAttached(paramRecyclerView, paramInt)) {
      paramRecyclerView = null;
    }
    for (;;)
    {
      return paramRecyclerView;
      RecyclerView.Recycler localRecycler = paramRecyclerView.mRecycler;
      RecyclerView.ViewHolder localViewHolder = localRecycler.tryGetViewHolderForPositionByDeadline(paramInt, false, paramLong);
      paramRecyclerView = localViewHolder;
      if (localViewHolder != null) {
        if (localViewHolder.isBound())
        {
          localRecycler.recycleView(localViewHolder.itemView);
          paramRecyclerView = localViewHolder;
        }
        else
        {
          localRecycler.addViewHolderToRecycledViewPool(localViewHolder, false);
          paramRecyclerView = localViewHolder;
        }
      }
    }
  }
  
  public void add(RecyclerView paramRecyclerView)
  {
    this.mRecyclerViews.add(paramRecyclerView);
  }
  
  void postFromTraversal(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    if ((paramRecyclerView.isAttachedToWindow()) && (this.mPostTimeNs == 0L))
    {
      this.mPostTimeNs = paramRecyclerView.getNanoTime();
      paramRecyclerView.post(this);
    }
    paramRecyclerView.mPrefetchRegistry.setPrefetchVector(paramInt1, paramInt2);
  }
  
  void prefetch(long paramLong)
  {
    buildTaskList();
    flushTasksWithDeadline(paramLong);
  }
  
  public void remove(RecyclerView paramRecyclerView)
  {
    this.mRecyclerViews.remove(paramRecyclerView);
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: ldc_w 270
    //   3: invokestatic 194	android/support/v4/os/TraceCompat:beginSection	(Ljava/lang/String;)V
    //   6: aload_0
    //   7: getfield 47	android/support/v7/widget/GapWorker:mRecyclerViews	Ljava/util/ArrayList;
    //   10: invokevirtual 273	java/util/ArrayList:isEmpty	()Z
    //   13: istore_3
    //   14: iload_3
    //   15: ifeq +12 -> 27
    //   18: aload_0
    //   19: lconst_0
    //   20: putfield 247	android/support/v7/widget/GapWorker:mPostTimeNs	J
    //   23: invokestatic 211	android/support/v4/os/TraceCompat:endSection	()V
    //   26: return
    //   27: aload_0
    //   28: getfield 47	android/support/v7/widget/GapWorker:mRecyclerViews	Ljava/util/ArrayList;
    //   31: invokevirtual 54	java/util/ArrayList:size	()I
    //   34: istore_2
    //   35: lconst_0
    //   36: lstore 6
    //   38: iconst_0
    //   39: istore_1
    //   40: iload_1
    //   41: iload_2
    //   42: if_icmpge +50 -> 92
    //   45: aload_0
    //   46: getfield 47	android/support/v7/widget/GapWorker:mRecyclerViews	Ljava/util/ArrayList;
    //   49: iload_1
    //   50: invokevirtual 58	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   53: checkcast 60	android/support/v7/widget/RecyclerView
    //   56: astore 8
    //   58: lload 6
    //   60: lstore 4
    //   62: aload 8
    //   64: invokevirtual 63	android/support/v7/widget/RecyclerView:getWindowVisibility	()I
    //   67: ifne +15 -> 82
    //   70: aload 8
    //   72: invokevirtual 276	android/support/v7/widget/RecyclerView:getDrawingTime	()J
    //   75: lload 6
    //   77: invokestatic 280	java/lang/Math:max	(JJ)J
    //   80: lstore 4
    //   82: iinc 1 1
    //   85: lload 4
    //   87: lstore 6
    //   89: goto -49 -> 40
    //   92: lload 6
    //   94: lconst_0
    //   95: lcmp
    //   96: ifne +14 -> 110
    //   99: aload_0
    //   100: lconst_0
    //   101: putfield 247	android/support/v7/widget/GapWorker:mPostTimeNs	J
    //   104: invokestatic 211	android/support/v4/os/TraceCompat:endSection	()V
    //   107: goto -81 -> 26
    //   110: aload_0
    //   111: getstatic 286	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   114: lload 6
    //   116: invokevirtual 290	java/util/concurrent/TimeUnit:toNanos	(J)J
    //   119: aload_0
    //   120: getfield 292	android/support/v7/widget/GapWorker:mFrameIntervalNs	J
    //   123: ladd
    //   124: invokevirtual 294	android/support/v7/widget/GapWorker:prefetch	(J)V
    //   127: aload_0
    //   128: lconst_0
    //   129: putfield 247	android/support/v7/widget/GapWorker:mPostTimeNs	J
    //   132: invokestatic 211	android/support/v4/os/TraceCompat:endSection	()V
    //   135: goto -109 -> 26
    //   138: astore 8
    //   140: aload_0
    //   141: lconst_0
    //   142: putfield 247	android/support/v7/widget/GapWorker:mPostTimeNs	J
    //   145: invokestatic 211	android/support/v4/os/TraceCompat:endSection	()V
    //   148: aload 8
    //   150: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	151	0	this	GapWorker
    //   39	44	1	i	int
    //   34	9	2	j	int
    //   13	2	3	bool	boolean
    //   60	26	4	l1	long
    //   36	79	6	l2	long
    //   56	15	8	localRecyclerView	RecyclerView
    //   138	11	8	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   0	14	138	finally
    //   27	35	138	finally
    //   45	58	138	finally
    //   62	82	138	finally
    //   110	127	138	finally
  }
  
  static class LayoutPrefetchRegistryImpl
    implements RecyclerView.LayoutManager.LayoutPrefetchRegistry
  {
    int mCount;
    int[] mPrefetchArray;
    int mPrefetchDx;
    int mPrefetchDy;
    
    public void addPosition(int paramInt1, int paramInt2)
    {
      if (paramInt1 < 0) {
        throw new IllegalArgumentException("Layout positions must be non-negative");
      }
      if (paramInt2 < 0) {
        throw new IllegalArgumentException("Pixel distance must be non-negative");
      }
      int i = this.mCount * 2;
      if (this.mPrefetchArray == null)
      {
        this.mPrefetchArray = new int[4];
        Arrays.fill(this.mPrefetchArray, -1);
      }
      for (;;)
      {
        this.mPrefetchArray[i] = paramInt1;
        this.mPrefetchArray[(i + 1)] = paramInt2;
        this.mCount += 1;
        return;
        if (i >= this.mPrefetchArray.length)
        {
          int[] arrayOfInt = this.mPrefetchArray;
          this.mPrefetchArray = new int[i * 2];
          System.arraycopy(arrayOfInt, 0, this.mPrefetchArray, 0, arrayOfInt.length);
        }
      }
    }
    
    void clearPrefetchPositions()
    {
      if (this.mPrefetchArray != null) {
        Arrays.fill(this.mPrefetchArray, -1);
      }
      this.mCount = 0;
    }
    
    void collectPrefetchPositionsFromView(RecyclerView paramRecyclerView, boolean paramBoolean)
    {
      this.mCount = 0;
      if (this.mPrefetchArray != null) {
        Arrays.fill(this.mPrefetchArray, -1);
      }
      RecyclerView.LayoutManager localLayoutManager = paramRecyclerView.mLayout;
      if ((paramRecyclerView.mAdapter != null) && (localLayoutManager != null) && (localLayoutManager.isItemPrefetchEnabled()))
      {
        if (!paramBoolean) {
          break label101;
        }
        if (!paramRecyclerView.mAdapterHelper.hasPendingUpdates()) {
          localLayoutManager.collectInitialPrefetchPositions(paramRecyclerView.mAdapter.getItemCount(), this);
        }
      }
      for (;;)
      {
        if (this.mCount > localLayoutManager.mPrefetchMaxCountObserved)
        {
          localLayoutManager.mPrefetchMaxCountObserved = this.mCount;
          localLayoutManager.mPrefetchMaxObservedInInitialPrefetch = paramBoolean;
          paramRecyclerView.mRecycler.updateViewCacheSize();
        }
        return;
        label101:
        if (!paramRecyclerView.hasPendingAdapterUpdates()) {
          localLayoutManager.collectAdjacentPrefetchPositions(this.mPrefetchDx, this.mPrefetchDy, paramRecyclerView.mState, this);
        }
      }
    }
    
    boolean lastPrefetchIncludedPosition(int paramInt)
    {
      int i;
      if (this.mPrefetchArray != null)
      {
        int j = this.mCount;
        i = 0;
        if (i < j * 2) {
          if (this.mPrefetchArray[i] != paramInt) {}
        }
      }
      for (boolean bool = true;; bool = false)
      {
        return bool;
        i += 2;
        break;
      }
    }
    
    void setPrefetchVector(int paramInt1, int paramInt2)
    {
      this.mPrefetchDx = paramInt1;
      this.mPrefetchDy = paramInt2;
    }
  }
  
  static class Task
  {
    public int distanceToItem;
    public boolean immediate;
    public int position;
    public RecyclerView view;
    public int viewVelocity;
    
    public void clear()
    {
      this.immediate = false;
      this.viewVelocity = 0;
      this.distanceToItem = 0;
      this.view = null;
      this.position = 0;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\GapWorker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */