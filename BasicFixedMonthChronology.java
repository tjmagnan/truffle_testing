package org.joda.time.chrono;

import org.joda.time.Chronology;

abstract class BasicFixedMonthChronology
  extends BasicChronology
{
  static final long MILLIS_PER_MONTH = 2592000000L;
  static final long MILLIS_PER_YEAR = 31557600000L;
  static final int MONTH_LENGTH = 30;
  private static final long serialVersionUID = 261387371998L;
  
  BasicFixedMonthChronology(Chronology paramChronology, Object paramObject, int paramInt)
  {
    super(paramChronology, paramObject, paramInt);
  }
  
  long getAverageMillisPerMonth()
  {
    return 2592000000L;
  }
  
  long getAverageMillisPerYear()
  {
    return 31557600000L;
  }
  
  long getAverageMillisPerYearDividedByTwo()
  {
    return 15778800000L;
  }
  
  int getDayOfMonth(long paramLong)
  {
    return (getDayOfYear(paramLong) - 1) % 30 + 1;
  }
  
  int getDaysInMonthMax()
  {
    return 30;
  }
  
  int getDaysInMonthMax(int paramInt)
  {
    if (paramInt != 13) {}
    for (paramInt = 30;; paramInt = 6) {
      return paramInt;
    }
  }
  
  int getDaysInYearMonth(int paramInt1, int paramInt2)
  {
    if (paramInt2 != 13) {
      paramInt1 = 30;
    }
    for (;;)
    {
      return paramInt1;
      if (isLeapYear(paramInt1)) {
        paramInt1 = 6;
      } else {
        paramInt1 = 5;
      }
    }
  }
  
  int getMaxMonth()
  {
    return 13;
  }
  
  int getMonthOfYear(long paramLong)
  {
    return (getDayOfYear(paramLong) - 1) / 30 + 1;
  }
  
  int getMonthOfYear(long paramLong, int paramInt)
  {
    return (int)((paramLong - getYearMillis(paramInt)) / 2592000000L) + 1;
  }
  
  long getTotalMillisByYearMonth(int paramInt1, int paramInt2)
  {
    return (paramInt2 - 1) * 2592000000L;
  }
  
  long getYearDifference(long paramLong1, long paramLong2)
  {
    int j = getYear(paramLong1);
    int i = getYear(paramLong2);
    long l2 = getYearMillis(j);
    long l1 = getYearMillis(i);
    j -= i;
    i = j;
    if (paramLong1 - l2 < paramLong2 - l1) {
      i = j - 1;
    }
    return i;
  }
  
  boolean isLeapYear(int paramInt)
  {
    if ((paramInt & 0x3) == 3) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  long setYear(long paramLong, int paramInt)
  {
    int j = getDayOfYear(paramLong, getYear(paramLong));
    int k = getMillisOfDay(paramLong);
    int i = j;
    if (j > 365)
    {
      i = j;
      if (!isLeapYear(paramInt)) {
        i = j - 1;
      }
    }
    paramLong = getYearMonthDayMillis(paramInt, 1, i);
    return k + paramLong;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BasicFixedMonthChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */