package io.fabric.sdk.android.services.events;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.QueueFile;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class QueueFileEventStorage
  implements EventsStorage
{
  private final Context context;
  private QueueFile queueFile;
  private File targetDirectory;
  private final String targetDirectoryName;
  private final File workingDirectory;
  private final File workingFile;
  
  public QueueFileEventStorage(Context paramContext, File paramFile, String paramString1, String paramString2)
    throws IOException
  {
    this.context = paramContext;
    this.workingDirectory = paramFile;
    this.targetDirectoryName = paramString2;
    this.workingFile = new File(this.workingDirectory, paramString1);
    this.queueFile = new QueueFile(this.workingFile);
    createTargetDirectory();
  }
  
  private void createTargetDirectory()
  {
    this.targetDirectory = new File(this.workingDirectory, this.targetDirectoryName);
    if (!this.targetDirectory.exists()) {
      this.targetDirectory.mkdirs();
    }
  }
  
  private void move(File paramFile1, File paramFile2)
    throws IOException
  {
    Object localObject3 = null;
    Object localObject1 = null;
    localObject2 = null;
    try
    {
      localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile1);
      localObject1 = localObject3;
      CommonUtils.closeOrLog((Closeable)localObject2, "Failed to close file input stream");
    }
    finally
    {
      try
      {
        paramFile2 = getMoveOutputStream(paramFile2);
        localObject1 = paramFile2;
        CommonUtils.copyStream(localFileInputStream, paramFile2, new byte['Ѐ']);
        CommonUtils.closeOrLog(localFileInputStream, "Failed to close file input stream");
        CommonUtils.closeOrLog(paramFile2, "Failed to close output stream");
        paramFile1.delete();
        return;
      }
      finally
      {
        for (;;)
        {
          FileInputStream localFileInputStream;
          localObject2 = localFileInputStream;
        }
      }
      paramFile2 = finally;
    }
    CommonUtils.closeOrLog((Closeable)localObject1, "Failed to close output stream");
    paramFile1.delete();
    throw paramFile2;
  }
  
  public void add(byte[] paramArrayOfByte)
    throws IOException
  {
    this.queueFile.add(paramArrayOfByte);
  }
  
  public boolean canWorkingFileStore(int paramInt1, int paramInt2)
  {
    return this.queueFile.hasSpaceFor(paramInt1, paramInt2);
  }
  
  public void deleteFilesInRollOverDirectory(List<File> paramList)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      File localFile = (File)paramList.next();
      CommonUtils.logControlled(this.context, String.format("deleting sent analytics file %s", new Object[] { localFile.getName() }));
      localFile.delete();
    }
  }
  
  public void deleteWorkingFile()
  {
    try
    {
      this.queueFile.close();
      this.workingFile.delete();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public List<File> getAllFilesInRollOverDirectory()
  {
    return Arrays.asList(this.targetDirectory.listFiles());
  }
  
  public List<File> getBatchOfFilesToSend(int paramInt)
  {
    ArrayList localArrayList = new ArrayList();
    File[] arrayOfFile = this.targetDirectory.listFiles();
    int j = arrayOfFile.length;
    for (int i = 0;; i++) {
      if (i < j)
      {
        localArrayList.add(arrayOfFile[i]);
        if (localArrayList.size() < paramInt) {}
      }
      else
      {
        return localArrayList;
      }
    }
  }
  
  public OutputStream getMoveOutputStream(File paramFile)
    throws IOException
  {
    return new FileOutputStream(paramFile);
  }
  
  public File getRollOverDirectory()
  {
    return this.targetDirectory;
  }
  
  public File getWorkingDirectory()
  {
    return this.workingDirectory;
  }
  
  public int getWorkingFileUsedSizeInBytes()
  {
    return this.queueFile.usedBytes();
  }
  
  public boolean isWorkingFileEmpty()
  {
    return this.queueFile.isEmpty();
  }
  
  public void rollOver(String paramString)
    throws IOException
  {
    this.queueFile.close();
    move(this.workingFile, new File(this.targetDirectory, paramString));
    this.queueFile = new QueueFile(this.workingFile);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\events\QueueFileEventStorage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */