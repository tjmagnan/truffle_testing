package tech.dcube.companion.model;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class PBObject
{
  static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
  
  public static <T> T fromJson(Class<T> paramClass, String paramString)
  {
    return (T)gson.fromJson(paramString, paramClass);
  }
  
  public static <T> List<T> fromJsonArray(Class<T[]> paramClass, String paramString)
  {
    try
    {
      paramString = (Object[])gson.fromJson(paramString, paramClass);
      paramClass = new java/util/ArrayList;
      paramClass.<init>(Arrays.asList(paramString));
      return paramClass;
    }
    catch (Exception paramClass)
    {
      for (;;)
      {
        Log.e("PBDevice", "Exception: " + paramClass.getMessage());
        paramClass = new ArrayList();
      }
    }
  }
  
  public Map<String, Object> toJsonMap()
  {
    Object localObject = toJsonString();
    Map localMap = (Map)gson.fromJson((String)localObject, Map.class);
    HashMap localHashMap = new HashMap();
    localObject = localMap.keySet().iterator();
    while (((Iterator)localObject).hasNext())
    {
      String str = ((Iterator)localObject).next().toString();
      localHashMap.put(str, localMap.get(str));
    }
    return localHashMap;
  }
  
  public String toJsonString()
  {
    return gson.toJson(this);
  }
  
  public String toString()
  {
    return toJsonString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBObject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */