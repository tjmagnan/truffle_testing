package io.fabric.sdk.android.services.network;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.TreeMap;

public final class UrlUtils
{
  public static final String UTF8 = "UTF8";
  
  public static TreeMap<String, String> getQueryParams(String paramString, boolean paramBoolean)
  {
    TreeMap localTreeMap = new TreeMap();
    if (paramString == null) {
      return localTreeMap;
    }
    String[] arrayOfString = paramString.split("&");
    int j = arrayOfString.length;
    int i = 0;
    label30:
    if (i < j)
    {
      paramString = arrayOfString[i].split("=");
      if (paramString.length != 2) {
        break label94;
      }
      if (!paramBoolean) {
        break label79;
      }
      localTreeMap.put(urlDecode(paramString[0]), urlDecode(paramString[1]));
    }
    for (;;)
    {
      i++;
      break label30;
      break;
      label79:
      localTreeMap.put(paramString[0], paramString[1]);
      continue;
      label94:
      if (!TextUtils.isEmpty(paramString[0])) {
        if (paramBoolean) {
          localTreeMap.put(urlDecode(paramString[0]), "");
        } else {
          localTreeMap.put(paramString[0], "");
        }
      }
    }
  }
  
  public static TreeMap<String, String> getQueryParams(URI paramURI, boolean paramBoolean)
  {
    return getQueryParams(paramURI.getRawQuery(), paramBoolean);
  }
  
  public static String percentEncode(String paramString)
  {
    if (paramString == null) {}
    StringBuilder localStringBuilder;
    for (paramString = "";; paramString = localStringBuilder.toString())
    {
      return paramString;
      localStringBuilder = new StringBuilder();
      paramString = urlEncode(paramString);
      int j = paramString.length();
      int i = 0;
      if (i < j)
      {
        char c = paramString.charAt(i);
        if (c == '*') {
          localStringBuilder.append("%2A");
        }
        for (;;)
        {
          i++;
          break;
          if (c == '+')
          {
            localStringBuilder.append("%20");
          }
          else if ((c == '%') && (i + 2 < j) && (paramString.charAt(i + 1) == '7') && (paramString.charAt(i + 2) == 'E'))
          {
            localStringBuilder.append('~');
            i += 2;
          }
          else
          {
            localStringBuilder.append(c);
          }
        }
      }
    }
  }
  
  public static String urlDecode(String paramString)
  {
    if (paramString == null) {
      paramString = "";
    }
    for (;;)
    {
      return paramString;
      try
      {
        paramString = URLDecoder.decode(paramString, "UTF8");
      }
      catch (UnsupportedEncodingException paramString)
      {
        throw new RuntimeException(paramString.getMessage(), paramString);
      }
    }
  }
  
  public static String urlEncode(String paramString)
  {
    if (paramString == null) {
      paramString = "";
    }
    for (;;)
    {
      return paramString;
      try
      {
        paramString = URLEncoder.encode(paramString, "UTF8");
      }
      catch (UnsupportedEncodingException paramString)
      {
        throw new RuntimeException(paramString.getMessage(), paramString);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\network\UrlUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */