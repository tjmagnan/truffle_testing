package com.testfairy.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.TextureView;
import android.view.View;

public class k
  extends View
{
  private TextureView a;
  
  public k(Context paramContext)
  {
    super(paramContext);
  }
  
  public void a(TextureView paramTextureView)
  {
    this.a = paramTextureView;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    try
    {
      if (this.a != null)
      {
        Bitmap localBitmap = this.a.getBitmap();
        if (localBitmap != null)
        {
          Matrix localMatrix = new android/graphics/Matrix;
          localMatrix.<init>();
          Paint localPaint = new android/graphics/Paint;
          localPaint.<init>();
          paramCanvas.drawBitmap(localBitmap, localMatrix, localPaint);
        }
      }
      return;
    }
    catch (Exception paramCanvas)
    {
      for (;;) {}
    }
    catch (OutOfMemoryError paramCanvas)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */