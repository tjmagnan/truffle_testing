package io.keen.client.android;

import android.content.Context;
import io.keen.client.java.FileEventStore;
import io.keen.client.java.KeenClient.Builder;
import io.keen.client.java.KeenEventStore;
import io.keen.client.java.KeenJsonHandler;
import io.keen.client.java.KeenNetworkStatusHandler;

public class AndroidKeenClientBuilder
  extends KeenClient.Builder
{
  private final Context context;
  
  public AndroidKeenClientBuilder(Context paramContext)
  {
    this.context = paramContext;
  }
  
  protected KeenEventStore getDefaultEventStore()
    throws Exception
  {
    return new FileEventStore(this.context.getCacheDir());
  }
  
  protected KeenJsonHandler getDefaultJsonHandler()
  {
    return new AndroidJsonHandler();
  }
  
  protected KeenNetworkStatusHandler getDefaultNetworkStatusHandler()
  {
    return new AndroidNetworkStatusHandler(this.context);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\keen\client\android\AndroidKeenClientBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */