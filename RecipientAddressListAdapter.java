package tech.dcube.companion.customListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBContact;

public class RecipientAddressListAdapter
  extends ArrayAdapter<PBAddress>
{
  private List<PBAddress> dataSet;
  private int lastPosition = -1;
  Context mContext;
  
  public RecipientAddressListAdapter(List<PBAddress> paramList, Context paramContext)
  {
    super(paramContext, 2130968656, paramList);
    this.dataSet = paramList;
    this.mContext = paramContext;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    PBAddress localPBAddress = (PBAddress)getItem(paramInt);
    Object localObject2;
    Object localObject1;
    if (paramView == null)
    {
      localObject2 = new ViewHolder(null);
      paramView = LayoutInflater.from(getContext()).inflate(2130968656, paramViewGroup, false);
      ((ViewHolder)localObject2).RecipientName = ((TextView)paramView.findViewById(2131624185));
      ((ViewHolder)localObject2).RecipientDetails = ((TextView)paramView.findViewById(2131624260));
      localObject1 = paramView;
      paramView.setTag(localObject2);
      paramViewGroup = (ViewGroup)localObject2;
      localObject2 = this.mContext;
      if (paramInt <= this.lastPosition) {
        break label230;
      }
    }
    label230:
    for (int i = 2131034132;; i = 2131034129)
    {
      ((View)localObject1).startAnimation(AnimationUtils.loadAnimation((Context)localObject2, i));
      this.lastPosition = paramInt;
      localObject1 = localPBAddress.getContact().getFullName();
      localObject2 = localPBAddress.getStreetLine1() + ", " + localPBAddress.getCity() + " " + localPBAddress.getState() + ", " + localPBAddress.getPostalCode() + ", " + localPBAddress.getIsoCountry();
      paramViewGroup.RecipientName.setText((CharSequence)localObject1);
      paramViewGroup.RecipientDetails.setText((CharSequence)localObject2);
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      localObject1 = paramView;
      break;
    }
  }
  
  public void updateData(List<PBAddress> paramList)
  {
    this.dataSet.clear();
    this.dataSet.addAll(paramList);
    notifyDataSetChanged();
  }
  
  private static class ViewHolder
  {
    TextView RecipientDetails;
    TextView RecipientName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\customListAdapters\RecipientAddressListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */