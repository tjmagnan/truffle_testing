package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBCommandStatus
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 4872615290621752202L;
  @Expose
  @SerializedName("commandId")
  private String commandId;
  @Expose
  @SerializedName("commandName")
  private String commandName;
  @Expose
  @SerializedName("requestDate")
  private String requestDate;
  @Expose
  @SerializedName("requestParams")
  private RequestParams requestParams;
  @Expose
  @SerializedName("responseData")
  private ResponseData responseData;
  @Expose
  @SerializedName("responseDate")
  private String responseDate;
  @Expose
  @SerializedName("successful")
  private boolean successful;
  
  public PBCommandStatus() {}
  
  public PBCommandStatus(String paramString1, String paramString2, String paramString3, RequestParams paramRequestParams, ResponseData paramResponseData, String paramString4, boolean paramBoolean)
  {
    this.commandId = paramString1;
    this.commandName = paramString2;
    this.requestDate = paramString3;
    this.requestParams = paramRequestParams;
    this.responseData = paramResponseData;
    this.responseDate = paramString4;
    this.successful = paramBoolean;
  }
  
  public String getCommandId()
  {
    return this.commandId;
  }
  
  public String getCommandName()
  {
    return this.commandName;
  }
  
  public String getRequestDate()
  {
    return this.requestDate;
  }
  
  public RequestParams getRequestParams()
  {
    return this.requestParams;
  }
  
  public ResponseData getResponseData()
  {
    return this.responseData;
  }
  
  public String getResponseDate()
  {
    return this.responseDate;
  }
  
  public boolean isSuccessful()
  {
    return this.successful;
  }
  
  public void setCommandId(String paramString)
  {
    this.commandId = paramString;
  }
  
  public void setCommandName(String paramString)
  {
    this.commandName = paramString;
  }
  
  public void setRequestDate(String paramString)
  {
    this.requestDate = paramString;
  }
  
  public void setRequestParams(RequestParams paramRequestParams)
  {
    this.requestParams = paramRequestParams;
  }
  
  public void setResponseData(ResponseData paramResponseData)
  {
    this.responseData = paramResponseData;
  }
  
  public void setResponseDate(String paramString)
  {
    this.responseDate = paramString;
  }
  
  public void setSuccessful(boolean paramBoolean)
  {
    this.successful = paramBoolean;
  }
  
  public class RequestParams
    extends PBObject
    implements Serializable
  {
    private static final long serialVersionUID = 4171977936925013400L;
    @Expose
    @SerializedName("refillAmount")
    private int refillAmount;
    
    public RequestParams() {}
    
    public RequestParams(int paramInt)
    {
      this.refillAmount = paramInt;
    }
    
    public int getRefillAmount()
    {
      return this.refillAmount;
    }
    
    public void setRefillAmount(int paramInt)
    {
      this.refillAmount = paramInt;
    }
  }
  
  public class ResponseData
    extends PBObject
    implements Serializable
  {
    private static final long serialVersionUID = 2680571726265118504L;
    @Expose
    @SerializedName("deviceErrorCode")
    private String deviceErrorCode;
    @Expose
    @SerializedName("deviceErrorMessage")
    private String deviceErrorMessage;
    @Expose
    @SerializedName("error")
    private String error;
    @Expose
    @SerializedName("message")
    private String message;
    
    public ResponseData() {}
    
    public ResponseData(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      this.deviceErrorMessage = paramString1;
      this.error = paramString2;
      this.message = paramString3;
      this.deviceErrorCode = paramString4;
    }
    
    public String getDeviceErrorCode()
    {
      return this.deviceErrorCode;
    }
    
    public String getDeviceErrorMessage()
    {
      return this.deviceErrorMessage;
    }
    
    public String getError()
    {
      return this.error;
    }
    
    public String getMessage()
    {
      return this.message;
    }
    
    public void setDeviceErrorCode(String paramString)
    {
      this.deviceErrorCode = paramString;
    }
    
    public void setDeviceErrorMessage(String paramString)
    {
      this.deviceErrorMessage = paramString;
    }
    
    public void setError(String paramString)
    {
      this.error = paramString;
    }
    
    public void setMessage(String paramString)
    {
      this.message = paramString;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBCommandStatus.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */