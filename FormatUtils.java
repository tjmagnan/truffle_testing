package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;

public class FormatUtils
{
  private static final double LOG_10 = Math.log(10.0D);
  
  public static void appendPaddedInteger(Appendable paramAppendable, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = paramInt1;
    if (paramInt1 < 0)
    {
      paramAppendable.append('-');
      i = paramInt2;
      if (paramInt1 != Integer.MIN_VALUE) {
        i = -paramInt1;
      }
    }
    else
    {
      if (i >= 10) {
        break label98;
      }
      while (paramInt2 > 1)
      {
        paramAppendable.append('0');
        paramInt2--;
      }
    }
    while (i > 10)
    {
      paramAppendable.append('0');
      i--;
    }
    paramAppendable.append("2147483648");
    for (;;)
    {
      return;
      paramAppendable.append((char)(i + 48));
      continue;
      label98:
      if (i < 100)
      {
        while (paramInt2 > 2)
        {
          paramAppendable.append('0');
          paramInt2--;
        }
        paramInt1 = (i + 1) * 13421772 >> 27;
        paramAppendable.append((char)(paramInt1 + 48));
        paramAppendable.append((char)(i - (paramInt1 << 3) - (paramInt1 << 1) + 48));
      }
      else
      {
        if (i < 1000) {
          paramInt1 = 3;
        }
        while (paramInt2 > paramInt1)
        {
          paramAppendable.append('0');
          paramInt2--;
          continue;
          if (i < 10000) {
            paramInt1 = 4;
          } else {
            paramInt1 = (int)(Math.log(i) / LOG_10) + 1;
          }
        }
        paramAppendable.append(Integer.toString(i));
      }
    }
  }
  
  public static void appendPaddedInteger(Appendable paramAppendable, long paramLong, int paramInt)
    throws IOException
  {
    int i = (int)paramLong;
    if (i == paramLong) {
      appendPaddedInteger(paramAppendable, i, paramInt);
    }
    for (;;)
    {
      return;
      if (paramInt <= 19)
      {
        paramAppendable.append(Long.toString(paramLong));
      }
      else
      {
        long l = paramLong;
        if (paramLong < 0L)
        {
          paramAppendable.append('-');
          i = paramInt;
          if (paramLong != Long.MIN_VALUE) {
            l = -paramLong;
          }
        }
        else
        {
          i = (int)(Math.log(l) / LOG_10);
          while (paramInt > i + 1)
          {
            paramAppendable.append('0');
            paramInt--;
          }
        }
        while (i > 19)
        {
          paramAppendable.append('0');
          i--;
        }
        paramAppendable.append("9223372036854775808");
        continue;
        paramAppendable.append(Long.toString(l));
      }
    }
  }
  
  public static void appendPaddedInteger(StringBuffer paramStringBuffer, int paramInt1, int paramInt2)
  {
    try
    {
      appendPaddedInteger(paramStringBuffer, paramInt1, paramInt2);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
  
  public static void appendPaddedInteger(StringBuffer paramStringBuffer, long paramLong, int paramInt)
  {
    try
    {
      appendPaddedInteger(paramStringBuffer, paramLong, paramInt);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
  
  public static void appendUnpaddedInteger(Appendable paramAppendable, int paramInt)
    throws IOException
  {
    int i = paramInt;
    if (paramInt < 0)
    {
      paramAppendable.append('-');
      if (paramInt != Integer.MIN_VALUE) {
        i = -paramInt;
      }
    }
    else
    {
      if (i >= 10) {
        break label55;
      }
      paramAppendable.append((char)(i + 48));
    }
    for (;;)
    {
      return;
      paramAppendable.append("2147483648");
      continue;
      label55:
      if (i < 100)
      {
        paramInt = (i + 1) * 13421772 >> 27;
        paramAppendable.append((char)(paramInt + 48));
        paramAppendable.append((char)(i - (paramInt << 3) - (paramInt << 1) + 48));
      }
      else
      {
        paramAppendable.append(Integer.toString(i));
      }
    }
  }
  
  public static void appendUnpaddedInteger(Appendable paramAppendable, long paramLong)
    throws IOException
  {
    int i = (int)paramLong;
    if (i == paramLong) {
      appendUnpaddedInteger(paramAppendable, i);
    }
    for (;;)
    {
      return;
      paramAppendable.append(Long.toString(paramLong));
    }
  }
  
  public static void appendUnpaddedInteger(StringBuffer paramStringBuffer, int paramInt)
  {
    try
    {
      appendUnpaddedInteger(paramStringBuffer, paramInt);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
  
  public static void appendUnpaddedInteger(StringBuffer paramStringBuffer, long paramLong)
  {
    try
    {
      appendUnpaddedInteger(paramStringBuffer, paramLong);
      return;
    }
    catch (IOException paramStringBuffer)
    {
      for (;;) {}
    }
  }
  
  public static int calculateDigitCount(long paramLong)
  {
    int i;
    if (paramLong < 0L) {
      if (paramLong != Long.MIN_VALUE) {
        i = calculateDigitCount(-paramLong) + 1;
      }
    }
    for (;;)
    {
      return i;
      i = 20;
      continue;
      if (paramLong < 10L) {
        i = 1;
      } else if (paramLong < 100L) {
        i = 2;
      } else if (paramLong < 1000L) {
        i = 3;
      } else if (paramLong < 10000L) {
        i = 4;
      } else {
        i = (int)(Math.log(paramLong) / LOG_10) + 1;
      }
    }
  }
  
  static String createErrorMessage(String paramString, int paramInt)
  {
    int i = paramInt + 32;
    String str;
    if (paramString.length() <= i + 3)
    {
      str = paramString;
      if (paramInt > 0) {
        break label63;
      }
      paramString = "Invalid format: \"" + str + '"';
    }
    for (;;)
    {
      return paramString;
      str = paramString.substring(0, i).concat("...");
      break;
      label63:
      if (paramInt >= paramString.length()) {
        paramString = "Invalid format: \"" + str + "\" is too short";
      } else {
        paramString = "Invalid format: \"" + str + "\" is malformed at \"" + str.substring(paramInt) + '"';
      }
    }
  }
  
  static int parseTwoDigits(CharSequence paramCharSequence, int paramInt)
  {
    int i = paramCharSequence.charAt(paramInt) - '0';
    return (i << 1) + (i << 3) + paramCharSequence.charAt(paramInt + 1) - 48;
  }
  
  public static void writePaddedInteger(Writer paramWriter, int paramInt1, int paramInt2)
    throws IOException
  {
    int i = paramInt1;
    if (paramInt1 < 0)
    {
      paramWriter.write(45);
      i = paramInt2;
      if (paramInt1 != Integer.MIN_VALUE) {
        i = -paramInt1;
      }
    }
    else
    {
      if (i >= 10) {
        break label82;
      }
      while (paramInt2 > 1)
      {
        paramWriter.write(48);
        paramInt2--;
      }
    }
    while (i > 10)
    {
      paramWriter.write(48);
      i--;
    }
    paramWriter.write("2147483648");
    for (;;)
    {
      return;
      paramWriter.write(i + 48);
      continue;
      label82:
      if (i < 100)
      {
        while (paramInt2 > 2)
        {
          paramWriter.write(48);
          paramInt2--;
        }
        paramInt1 = (i + 1) * 13421772 >> 27;
        paramWriter.write(paramInt1 + 48);
        paramWriter.write(i - (paramInt1 << 3) - (paramInt1 << 1) + 48);
      }
      else
      {
        if (i < 1000) {
          paramInt1 = 3;
        }
        while (paramInt2 > paramInt1)
        {
          paramWriter.write(48);
          paramInt2--;
          continue;
          if (i < 10000) {
            paramInt1 = 4;
          } else {
            paramInt1 = (int)(Math.log(i) / LOG_10) + 1;
          }
        }
        paramWriter.write(Integer.toString(i));
      }
    }
  }
  
  public static void writePaddedInteger(Writer paramWriter, long paramLong, int paramInt)
    throws IOException
  {
    int i = (int)paramLong;
    if (i == paramLong) {
      writePaddedInteger(paramWriter, i, paramInt);
    }
    for (;;)
    {
      return;
      if (paramInt <= 19)
      {
        paramWriter.write(Long.toString(paramLong));
      }
      else
      {
        long l = paramLong;
        if (paramLong < 0L)
        {
          paramWriter.write(45);
          i = paramInt;
          if (paramLong != Long.MIN_VALUE) {
            l = -paramLong;
          }
        }
        else
        {
          i = (int)(Math.log(l) / LOG_10);
          while (paramInt > i + 1)
          {
            paramWriter.write(48);
            paramInt--;
          }
        }
        while (i > 19)
        {
          paramWriter.write(48);
          i--;
        }
        paramWriter.write("9223372036854775808");
        continue;
        paramWriter.write(Long.toString(l));
      }
    }
  }
  
  public static void writeUnpaddedInteger(Writer paramWriter, int paramInt)
    throws IOException
  {
    int i = paramInt;
    if (paramInt < 0)
    {
      paramWriter.write(45);
      if (paramInt != Integer.MIN_VALUE) {
        i = -paramInt;
      }
    }
    else
    {
      if (i >= 10) {
        break label45;
      }
      paramWriter.write(i + 48);
    }
    for (;;)
    {
      return;
      paramWriter.write("2147483648");
      continue;
      label45:
      if (i < 100)
      {
        paramInt = (i + 1) * 13421772 >> 27;
        paramWriter.write(paramInt + 48);
        paramWriter.write(i - (paramInt << 3) - (paramInt << 1) + 48);
      }
      else
      {
        paramWriter.write(Integer.toString(i));
      }
    }
  }
  
  public static void writeUnpaddedInteger(Writer paramWriter, long paramLong)
    throws IOException
  {
    int i = (int)paramLong;
    if (i == paramLong) {
      writeUnpaddedInteger(paramWriter, i);
    }
    for (;;)
    {
      return;
      paramWriter.write(Long.toString(paramLong));
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\FormatUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */