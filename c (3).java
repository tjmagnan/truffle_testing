package com.testfairy.c;

import android.content.Context;

public class c
  extends d
{
  private b a;
  private final a b;
  
  public c(Context paramContext, a parama)
  {
    super(paramContext);
    this.b = parama;
  }
  
  public void a(Context paramContext)
  {
    if (this.a == null)
    {
      this.a = new b(paramContext, this.b);
      this.a.a(paramContext);
      this.a.a(this);
    }
    super.a(paramContext);
  }
  
  public void b(Context paramContext)
  {
    if (this.a != null)
    {
      this.a.b();
      this.a = null;
    }
    super.b(paramContext);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\c\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */