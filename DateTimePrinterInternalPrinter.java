package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class DateTimePrinterInternalPrinter
  implements InternalPrinter
{
  private final DateTimePrinter underlying;
  
  private DateTimePrinterInternalPrinter(DateTimePrinter paramDateTimePrinter)
  {
    this.underlying = paramDateTimePrinter;
  }
  
  static InternalPrinter of(DateTimePrinter paramDateTimePrinter)
  {
    if ((paramDateTimePrinter instanceof InternalPrinterDateTimePrinter)) {
      paramDateTimePrinter = (InternalPrinter)paramDateTimePrinter;
    }
    for (;;)
    {
      return paramDateTimePrinter;
      if (paramDateTimePrinter == null) {
        paramDateTimePrinter = null;
      } else {
        paramDateTimePrinter = new DateTimePrinterInternalPrinter(paramDateTimePrinter);
      }
    }
  }
  
  public int estimatePrintedLength()
  {
    return this.underlying.estimatePrintedLength();
  }
  
  DateTimePrinter getUnderlying()
  {
    return this.underlying;
  }
  
  public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
    throws IOException
  {
    if ((paramAppendable instanceof StringBuffer))
    {
      paramAppendable = (StringBuffer)paramAppendable;
      this.underlying.printTo(paramAppendable, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
    }
    for (;;)
    {
      return;
      if ((paramAppendable instanceof Writer))
      {
        paramAppendable = (Writer)paramAppendable;
        this.underlying.printTo(paramAppendable, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
      }
      else
      {
        StringBuffer localStringBuffer = new StringBuffer(estimatePrintedLength());
        this.underlying.printTo(localStringBuffer, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
        paramAppendable.append(localStringBuffer);
      }
    }
  }
  
  public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
    throws IOException
  {
    if ((paramAppendable instanceof StringBuffer))
    {
      paramAppendable = (StringBuffer)paramAppendable;
      this.underlying.printTo(paramAppendable, paramReadablePartial, paramLocale);
    }
    for (;;)
    {
      return;
      if ((paramAppendable instanceof Writer))
      {
        paramAppendable = (Writer)paramAppendable;
        this.underlying.printTo(paramAppendable, paramReadablePartial, paramLocale);
      }
      else
      {
        StringBuffer localStringBuffer = new StringBuffer(estimatePrintedLength());
        this.underlying.printTo(localStringBuffer, paramReadablePartial, paramLocale);
        paramAppendable.append(localStringBuffer);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\DateTimePrinterInternalPrinter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */