package android.support.v7.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.support.v7.view.SupportActionModeWrapper.CallbackWrapper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Window;
import android.view.Window.Callback;

@TargetApi(14)
@RequiresApi(14)
class AppCompatDelegateImplV14
  extends AppCompatDelegateImplV11
{
  private static final String KEY_LOCAL_NIGHT_MODE = "appcompat:local_night_mode";
  private boolean mApplyDayNightCalled;
  private AutoNightModeManager mAutoNightModeManager;
  private boolean mHandleNativeActionModes = true;
  private int mLocalNightMode = -100;
  
  AppCompatDelegateImplV14(Context paramContext, Window paramWindow, AppCompatCallback paramAppCompatCallback)
  {
    super(paramContext, paramWindow, paramAppCompatCallback);
  }
  
  private void ensureAutoNightModeManager()
  {
    if (this.mAutoNightModeManager == null) {
      this.mAutoNightModeManager = new AutoNightModeManager(TwilightManager.getInstance(this.mContext));
    }
  }
  
  private int getNightMode()
  {
    if (this.mLocalNightMode != -100) {}
    for (int i = this.mLocalNightMode;; i = getDefaultNightMode()) {
      return i;
    }
  }
  
  private boolean shouldRecreateOnNightModeChange()
  {
    boolean bool = true;
    PackageManager localPackageManager;
    if ((this.mApplyDayNightCalled) && ((this.mContext instanceof Activity))) {
      localPackageManager = this.mContext.getPackageManager();
    }
    for (;;)
    {
      try
      {
        ComponentName localComponentName = new android/content/ComponentName;
        localComponentName.<init>(this.mContext, this.mContext.getClass());
        int i = localPackageManager.getActivityInfo(localComponentName, 0).configChanges;
        if ((i & 0x200) != 0) {
          continue;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", localNameNotFoundException);
        continue;
      }
      return bool;
      bool = false;
      continue;
      bool = false;
    }
  }
  
  private boolean updateForNightMode(int paramInt)
  {
    Resources localResources = this.mContext.getResources();
    Configuration localConfiguration = localResources.getConfiguration();
    int i = localConfiguration.uiMode;
    if (paramInt == 2)
    {
      paramInt = 32;
      if ((i & 0x30) == paramInt) {
        break label116;
      }
      if (!shouldRecreateOnNightModeChange()) {
        break label65;
      }
      ((Activity)this.mContext).recreate();
    }
    label55:
    label65:
    label116:
    for (boolean bool = true;; bool = false)
    {
      return bool;
      paramInt = 16;
      break;
      localConfiguration = new Configuration(localConfiguration);
      DisplayMetrics localDisplayMetrics = localResources.getDisplayMetrics();
      localConfiguration.uiMode = (localConfiguration.uiMode & 0xFFFFFFCF | paramInt);
      localResources.updateConfiguration(localConfiguration, localDisplayMetrics);
      ResourcesFlusher.flush(localResources);
      break label55;
    }
  }
  
  public boolean applyDayNight()
  {
    boolean bool = false;
    int i = getNightMode();
    int j = mapNightMode(i);
    if (j != -1) {
      bool = updateForNightMode(j);
    }
    if (i == 0)
    {
      ensureAutoNightModeManager();
      this.mAutoNightModeManager.setup();
    }
    this.mApplyDayNightCalled = true;
    return bool;
  }
  
  @VisibleForTesting
  final AutoNightModeManager getAutoNightModeManager()
  {
    ensureAutoNightModeManager();
    return this.mAutoNightModeManager;
  }
  
  public boolean isHandleNativeActionModesEnabled()
  {
    return this.mHandleNativeActionModes;
  }
  
  int mapNightMode(int paramInt)
  {
    switch (paramInt)
    {
    }
    for (;;)
    {
      return paramInt;
      ensureAutoNightModeManager();
      paramInt = this.mAutoNightModeManager.getApplyableNightMode();
      continue;
      paramInt = -1;
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ((paramBundle != null) && (this.mLocalNightMode == -100)) {
      this.mLocalNightMode = paramBundle.getInt("appcompat:local_night_mode", -100);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.mAutoNightModeManager != null) {
      this.mAutoNightModeManager.cleanup();
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.mLocalNightMode != -100) {
      paramBundle.putInt("appcompat:local_night_mode", this.mLocalNightMode);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    applyDayNight();
  }
  
  public void onStop()
  {
    super.onStop();
    if (this.mAutoNightModeManager != null) {
      this.mAutoNightModeManager.cleanup();
    }
  }
  
  public void setHandleNativeActionModesEnabled(boolean paramBoolean)
  {
    this.mHandleNativeActionModes = paramBoolean;
  }
  
  public void setLocalNightMode(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      Log.i("AppCompatDelegate", "setLocalNightMode() called with an unknown mode");
    }
    for (;;)
    {
      return;
      if (this.mLocalNightMode != paramInt)
      {
        this.mLocalNightMode = paramInt;
        if (this.mApplyDayNightCalled) {
          applyDayNight();
        }
      }
    }
  }
  
  Window.Callback wrapWindowCallback(Window.Callback paramCallback)
  {
    return new AppCompatWindowCallbackV14(paramCallback);
  }
  
  class AppCompatWindowCallbackV14
    extends AppCompatDelegateImplBase.AppCompatWindowCallbackBase
  {
    AppCompatWindowCallbackV14(Window.Callback paramCallback)
    {
      super(paramCallback);
    }
    
    public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback)
    {
      if (AppCompatDelegateImplV14.this.isHandleNativeActionModesEnabled()) {}
      for (paramCallback = startAsSupportActionMode(paramCallback);; paramCallback = super.onWindowStartingActionMode(paramCallback)) {
        return paramCallback;
      }
    }
    
    final ActionMode startAsSupportActionMode(ActionMode.Callback paramCallback)
    {
      SupportActionModeWrapper.CallbackWrapper localCallbackWrapper = new SupportActionModeWrapper.CallbackWrapper(AppCompatDelegateImplV14.this.mContext, paramCallback);
      paramCallback = AppCompatDelegateImplV14.this.startSupportActionMode(localCallbackWrapper);
      if (paramCallback != null) {}
      for (paramCallback = localCallbackWrapper.getActionModeWrapper(paramCallback);; paramCallback = null) {
        return paramCallback;
      }
    }
  }
  
  @VisibleForTesting
  final class AutoNightModeManager
  {
    private BroadcastReceiver mAutoTimeChangeReceiver;
    private IntentFilter mAutoTimeChangeReceiverFilter;
    private boolean mIsNight;
    private TwilightManager mTwilightManager;
    
    AutoNightModeManager(TwilightManager paramTwilightManager)
    {
      this.mTwilightManager = paramTwilightManager;
      this.mIsNight = paramTwilightManager.isNight();
    }
    
    final void cleanup()
    {
      if (this.mAutoTimeChangeReceiver != null)
      {
        AppCompatDelegateImplV14.this.mContext.unregisterReceiver(this.mAutoTimeChangeReceiver);
        this.mAutoTimeChangeReceiver = null;
      }
    }
    
    final void dispatchTimeChanged()
    {
      boolean bool = this.mTwilightManager.isNight();
      if (bool != this.mIsNight)
      {
        this.mIsNight = bool;
        AppCompatDelegateImplV14.this.applyDayNight();
      }
    }
    
    final int getApplyableNightMode()
    {
      this.mIsNight = this.mTwilightManager.isNight();
      if (this.mIsNight) {}
      for (int i = 2;; i = 1) {
        return i;
      }
    }
    
    final void setup()
    {
      cleanup();
      if (this.mAutoTimeChangeReceiver == null) {
        this.mAutoTimeChangeReceiver = new BroadcastReceiver()
        {
          public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
          {
            AppCompatDelegateImplV14.AutoNightModeManager.this.dispatchTimeChanged();
          }
        };
      }
      if (this.mAutoTimeChangeReceiverFilter == null)
      {
        this.mAutoTimeChangeReceiverFilter = new IntentFilter();
        this.mAutoTimeChangeReceiverFilter.addAction("android.intent.action.TIME_SET");
        this.mAutoTimeChangeReceiverFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        this.mAutoTimeChangeReceiverFilter.addAction("android.intent.action.TIME_TICK");
      }
      AppCompatDelegateImplV14.this.mContext.registerReceiver(this.mAutoTimeChangeReceiver, this.mAutoTimeChangeReceiverFilter);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\app\AppCompatDelegateImplV14.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */