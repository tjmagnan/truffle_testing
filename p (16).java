package com.testfairy.p;

import android.os.Build.VERSION;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import com.testfairy.e.a;
import com.testfairy.g;
import com.testfairy.m.b;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class p
  implements Window.Callback
{
  private static final int a = 0;
  private static final int b = 1;
  private static final int c = 0;
  private static final int d = 1;
  private static final int e = 2;
  private static final int f = 3;
  private static final int g = 4;
  private static final int h = 5;
  private static final int i = 6;
  private static final int j = 7;
  private static final WeakHashMap k = new WeakHashMap();
  private static boolean l = false;
  private b m;
  private a n;
  private Window.Callback o;
  private String p;
  
  private p(b paramb, a parama, Window.Callback paramCallback)
  {
    this.m = paramb;
    this.n = parama;
    this.o = paramCallback;
  }
  
  public static void a()
  {
    l = true;
  }
  
  private void a(MotionEvent paramMotionEvent)
  {
    if ((paramMotionEvent.getAction() == 1) && (this.p != null))
    {
      String str = r.e(b(paramMotionEvent));
      if (this.p.equals(str)) {
        a(str);
      }
    }
    for (;;)
    {
      return;
      if (paramMotionEvent.getAction() == 0) {
        this.p = r.e(b(paramMotionEvent));
      }
    }
  }
  
  public static void a(View paramView, b paramb, a parama)
  {
    paramView = s.a(paramView);
    if ((paramView != null) && (!k.containsKey(paramView)))
    {
      paramb = new p(paramb, parama, paramView.getCallback());
      k.put(paramView, Boolean.valueOf(true));
      paramView.setCallback(paramb);
    }
  }
  
  private void a(String paramString)
  {
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("kind", Integer.valueOf(1));
    localHashMap.put("label", paramString);
    paramString = new g(26, localHashMap);
    this.m.a(paramString);
  }
  
  private void a(Map paramMap)
  {
    if (l) {}
    for (;;)
    {
      return;
      paramMap = new g(3, paramMap);
      this.m.a(paramMap);
    }
  }
  
  private View b(MotionEvent paramMotionEvent)
  {
    int i3 = (int)paramMotionEvent.getRawX();
    int i2 = (int)paramMotionEvent.getRawY();
    View[] arrayOfView = s.a();
    int i1 = 0;
    if (i1 < arrayOfView.length)
    {
      paramMotionEvent = r.a(arrayOfView[(arrayOfView.length - 1 - i1)], i3, i2);
      if (paramMotionEvent == null) {}
    }
    for (;;)
    {
      return paramMotionEvent;
      i1++;
      break;
      paramMotionEvent = null;
    }
  }
  
  public static void b()
  {
    l = false;
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    return this.o.dispatchGenericMotionEvent(paramMotionEvent);
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>(4);
      localHashMap.put("t", Integer.valueOf(1));
      localHashMap.put("act", Integer.valueOf(paramKeyEvent.getAction()));
      localHashMap.put("kc", Integer.valueOf(paramKeyEvent.getKeyCode()));
      a(localHashMap);
      return this.o.dispatchKeyEvent(paramKeyEvent);
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
  {
    return this.o.dispatchKeyShortcutEvent(paramKeyEvent);
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return this.o.dispatchPopulateAccessibilityEvent(paramAccessibilityEvent);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    try
    {
      float f2 = paramMotionEvent.getRawX();
      float f1 = paramMotionEvent.getRawY();
      int i1 = (int)(f2 * this.n.v());
      int i2 = (int)(f1 * this.n.v());
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>(4);
      localHashMap.put("t", Integer.valueOf(0));
      localHashMap.put("act", Integer.valueOf(paramMotionEvent.getAction()));
      localHashMap.put("x", Integer.valueOf(i1));
      localHashMap.put("y", Integer.valueOf(i2));
      a(localHashMap);
      if (this.n.s()) {
        a(paramMotionEvent);
      }
      return this.o.dispatchTouchEvent(paramMotionEvent);
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent)
  {
    return this.o.dispatchTrackballEvent(paramMotionEvent);
  }
  
  public void onActionModeFinished(ActionMode paramActionMode)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      this.o.onActionModeFinished(paramActionMode);
    }
  }
  
  public void onActionModeStarted(ActionMode paramActionMode)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      this.o.onActionModeStarted(paramActionMode);
    }
  }
  
  public void onAttachedToWindow()
  {
    this.o.onAttachedToWindow();
  }
  
  public void onContentChanged()
  {
    this.o.onContentChanged();
  }
  
  public boolean onCreatePanelMenu(int paramInt, Menu paramMenu)
  {
    return this.o.onCreatePanelMenu(paramInt, paramMenu);
  }
  
  public View onCreatePanelView(int paramInt)
  {
    return this.o.onCreatePanelView(paramInt);
  }
  
  public void onDetachedFromWindow()
  {
    this.o.onDetachedFromWindow();
  }
  
  public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
  {
    return this.o.onMenuItemSelected(paramInt, paramMenuItem);
  }
  
  public boolean onMenuOpened(int paramInt, Menu paramMenu)
  {
    return this.o.onMenuOpened(paramInt, paramMenu);
  }
  
  public void onPanelClosed(int paramInt, Menu paramMenu)
  {
    this.o.onPanelClosed(paramInt, paramMenu);
  }
  
  public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
  {
    return this.o.onPreparePanel(paramInt, paramView, paramMenu);
  }
  
  public boolean onSearchRequested()
  {
    return this.o.onSearchRequested();
  }
  
  public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams)
  {
    this.o.onWindowAttributesChanged(paramLayoutParams);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    this.o.onWindowFocusChanged(paramBoolean);
  }
  
  public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback)
  {
    if (Build.VERSION.SDK_INT >= 11) {}
    for (paramCallback = this.o.onWindowStartingActionMode(paramCallback);; paramCallback = null) {
      return paramCallback;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */