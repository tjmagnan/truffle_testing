package com.testfairy.n;

import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.testfairy.g;
import java.util.HashMap;
import java.util.Map;

public class k
  extends b
{
  private static final int b = 353;
  private static final String c = "isGsm";
  private static final String d = "cdmaDbm";
  private static final String e = "evdoDbm";
  private static final String f = "evdoSnr";
  private static final String g = "cdmaEcio";
  private static final String h = "evdoEcio";
  private static final String i = "gsmBitErrorRate";
  private static final String j = "gsmSignalStrength";
  private String a = null;
  private boolean k = true;
  private PhoneStateListener l = new PhoneStateListener()
  {
    public void onCallStateChanged(int paramAnonymousInt, String paramAnonymousString)
    {
      super.onCallStateChanged(paramAnonymousInt, paramAnonymousString);
      k.a(k.this, paramAnonymousInt, paramAnonymousString);
    }
    
    public void onDataConnectionStateChanged(int paramAnonymousInt)
    {
      super.onDataConnectionStateChanged(paramAnonymousInt);
    }
    
    public void onDataConnectionStateChanged(int paramAnonymousInt1, int paramAnonymousInt2)
    {
      super.onDataConnectionStateChanged(paramAnonymousInt1, paramAnonymousInt2);
    }
    
    public void onServiceStateChanged(ServiceState paramAnonymousServiceState)
    {
      super.onServiceStateChanged(paramAnonymousServiceState);
    }
    
    public void onSignalStrengthsChanged(SignalStrength paramAnonymousSignalStrength)
    {
      super.onSignalStrengthsChanged(paramAnonymousSignalStrength);
      k.a(k.this, paramAnonymousSignalStrength);
    }
  };
  
  public k(com.testfairy.m.b paramb, TelephonyManager paramTelephonyManager)
  {
    super(paramb);
    paramTelephonyManager.listen(this.l, 353);
  }
  
  private void a(int paramInt, String paramString)
  {
    if (this.k) {
      this.k = false;
    }
    for (;;)
    {
      return;
      try
      {
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>(1);
        localHashMap.put("state", Integer.valueOf(paramInt));
        paramString = new com/testfairy/g;
        paramString.<init>(12, localHashMap);
        b().a(paramString);
        b().a();
      }
      catch (Exception paramString) {}
    }
  }
  
  private void a(SignalStrength paramSignalStrength)
  {
    if ((this.a == null) || (!this.a.equals(paramSignalStrength.toString()))) {
      this.a = paramSignalStrength.toString();
    }
    for (;;)
    {
      try
      {
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>(16);
        localHashMap.put("gsmSignalStrength", Integer.valueOf(paramSignalStrength.getGsmSignalStrength()));
        localHashMap.put("gsmBitErrorRate", Integer.valueOf(paramSignalStrength.getGsmBitErrorRate()));
        localHashMap.put("cdmaDbm", Integer.valueOf(paramSignalStrength.getCdmaDbm()));
        localHashMap.put("cdmaEcio", Integer.valueOf(paramSignalStrength.getCdmaEcio()));
        localHashMap.put("evdoDbm", Integer.valueOf(paramSignalStrength.getEvdoDbm()));
        localHashMap.put("evdoEcio", Integer.valueOf(paramSignalStrength.getEvdoEcio()));
        localHashMap.put("evdoSnr", Integer.valueOf(paramSignalStrength.getEvdoSnr()));
        if (paramSignalStrength.isGsm())
        {
          m = 1;
          localHashMap.put("isGsm", Integer.valueOf(m));
          paramSignalStrength = new com/testfairy/g;
          paramSignalStrength.<init>(8, localHashMap);
          b().a(paramSignalStrength);
          return;
        }
      }
      catch (Throwable paramSignalStrength)
      {
        int m;
        continue;
      }
      m = 0;
    }
  }
  
  public void a() {}
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */