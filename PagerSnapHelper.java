package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;

public class PagerSnapHelper
  extends SnapHelper
{
  private static final int MAX_SCROLL_ON_FLING_DURATION = 100;
  @Nullable
  private OrientationHelper mHorizontalHelper;
  @Nullable
  private OrientationHelper mVerticalHelper;
  
  private int distanceToCenter(@NonNull RecyclerView.LayoutManager paramLayoutManager, @NonNull View paramView, OrientationHelper paramOrientationHelper)
  {
    int k = paramOrientationHelper.getDecoratedStart(paramView);
    int j = paramOrientationHelper.getDecoratedMeasurement(paramView) / 2;
    if (paramLayoutManager.getClipToPadding()) {}
    for (int i = paramOrientationHelper.getStartAfterPadding() + paramOrientationHelper.getTotalSpace() / 2;; i = paramOrientationHelper.getEnd() / 2) {
      return k + j - i;
    }
  }
  
  @Nullable
  private View findCenterView(RecyclerView.LayoutManager paramLayoutManager, OrientationHelper paramOrientationHelper)
  {
    int i1 = paramLayoutManager.getChildCount();
    Object localObject2;
    if (i1 == 0)
    {
      localObject2 = null;
      return (View)localObject2;
    }
    Object localObject1 = null;
    if (paramLayoutManager.getClipToPadding()) {}
    for (int i = paramOrientationHelper.getStartAfterPadding() + paramOrientationHelper.getTotalSpace() / 2;; i = paramOrientationHelper.getEnd() / 2)
    {
      int k = Integer.MAX_VALUE;
      int j = 0;
      for (;;)
      {
        localObject2 = localObject1;
        if (j >= i1) {
          break;
        }
        localObject2 = paramLayoutManager.getChildAt(j);
        int n = Math.abs(paramOrientationHelper.getDecoratedStart((View)localObject2) + paramOrientationHelper.getDecoratedMeasurement((View)localObject2) / 2 - i);
        int m = k;
        if (n < k)
        {
          m = n;
          localObject1 = localObject2;
        }
        j++;
        k = m;
      }
    }
  }
  
  @Nullable
  private View findStartView(RecyclerView.LayoutManager paramLayoutManager, OrientationHelper paramOrientationHelper)
  {
    int n = paramLayoutManager.getChildCount();
    Object localObject2;
    if (n == 0)
    {
      localObject2 = null;
      return (View)localObject2;
    }
    Object localObject1 = null;
    int i = Integer.MAX_VALUE;
    int j = 0;
    for (;;)
    {
      localObject2 = localObject1;
      if (j >= n) {
        break;
      }
      localObject2 = paramLayoutManager.getChildAt(j);
      int m = paramOrientationHelper.getDecoratedStart((View)localObject2);
      int k = i;
      if (m < i)
      {
        k = m;
        localObject1 = localObject2;
      }
      j++;
      i = k;
    }
  }
  
  @NonNull
  private OrientationHelper getHorizontalHelper(@NonNull RecyclerView.LayoutManager paramLayoutManager)
  {
    if ((this.mHorizontalHelper == null) || (this.mHorizontalHelper.mLayoutManager != paramLayoutManager)) {
      this.mHorizontalHelper = OrientationHelper.createHorizontalHelper(paramLayoutManager);
    }
    return this.mHorizontalHelper;
  }
  
  @NonNull
  private OrientationHelper getVerticalHelper(@NonNull RecyclerView.LayoutManager paramLayoutManager)
  {
    if ((this.mVerticalHelper == null) || (this.mVerticalHelper.mLayoutManager != paramLayoutManager)) {
      this.mVerticalHelper = OrientationHelper.createVerticalHelper(paramLayoutManager);
    }
    return this.mVerticalHelper;
  }
  
  @Nullable
  public int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager paramLayoutManager, @NonNull View paramView)
  {
    int[] arrayOfInt = new int[2];
    if (paramLayoutManager.canScrollHorizontally())
    {
      arrayOfInt[0] = distanceToCenter(paramLayoutManager, paramView, getHorizontalHelper(paramLayoutManager));
      if (!paramLayoutManager.canScrollVertically()) {
        break label55;
      }
      arrayOfInt[1] = distanceToCenter(paramLayoutManager, paramView, getVerticalHelper(paramLayoutManager));
    }
    for (;;)
    {
      return arrayOfInt;
      arrayOfInt[0] = 0;
      break;
      label55:
      arrayOfInt[1] = 0;
    }
  }
  
  protected LinearSmoothScroller createSnapScroller(RecyclerView.LayoutManager paramLayoutManager)
  {
    if (!(paramLayoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {}
    for (paramLayoutManager = null;; paramLayoutManager = new LinearSmoothScroller(this.mRecyclerView.getContext())
        {
          protected float calculateSpeedPerPixel(DisplayMetrics paramAnonymousDisplayMetrics)
          {
            return 100.0F / paramAnonymousDisplayMetrics.densityDpi;
          }
          
          protected int calculateTimeForScrolling(int paramAnonymousInt)
          {
            return Math.min(100, super.calculateTimeForScrolling(paramAnonymousInt));
          }
          
          protected void onTargetFound(View paramAnonymousView, RecyclerView.State paramAnonymousState, RecyclerView.SmoothScroller.Action paramAnonymousAction)
          {
            paramAnonymousView = PagerSnapHelper.this.calculateDistanceToFinalSnap(PagerSnapHelper.this.mRecyclerView.getLayoutManager(), paramAnonymousView);
            int i = paramAnonymousView[0];
            int j = paramAnonymousView[1];
            int k = calculateTimeForDeceleration(Math.max(Math.abs(i), Math.abs(j)));
            if (k > 0) {
              paramAnonymousAction.update(i, j, k, this.mDecelerateInterpolator);
            }
          }
        }) {
      return paramLayoutManager;
    }
  }
  
  @Nullable
  public View findSnapView(RecyclerView.LayoutManager paramLayoutManager)
  {
    if (paramLayoutManager.canScrollVertically()) {
      paramLayoutManager = findCenterView(paramLayoutManager, getVerticalHelper(paramLayoutManager));
    }
    for (;;)
    {
      return paramLayoutManager;
      if (paramLayoutManager.canScrollHorizontally()) {
        paramLayoutManager = findCenterView(paramLayoutManager, getHorizontalHelper(paramLayoutManager));
      } else {
        paramLayoutManager = null;
      }
    }
  }
  
  public int findTargetSnapPosition(RecyclerView.LayoutManager paramLayoutManager, int paramInt1, int paramInt2)
  {
    int k = paramLayoutManager.getItemCount();
    if (k == 0) {
      paramInt1 = -1;
    }
    for (;;)
    {
      return paramInt1;
      View localView = null;
      if (paramLayoutManager.canScrollVertically()) {
        localView = findStartView(paramLayoutManager, getVerticalHelper(paramLayoutManager));
      }
      for (;;)
      {
        if (localView != null) {
          break label69;
        }
        paramInt1 = -1;
        break;
        if (paramLayoutManager.canScrollHorizontally()) {
          localView = findStartView(paramLayoutManager, getHorizontalHelper(paramLayoutManager));
        }
      }
      label69:
      int i = paramLayoutManager.getPosition(localView);
      if (i == -1)
      {
        paramInt1 = -1;
      }
      else
      {
        if (paramLayoutManager.canScrollHorizontally()) {
          if (paramInt1 > 0)
          {
            paramInt2 = 1;
            label101:
            int j = 0;
            paramInt1 = j;
            if ((paramLayoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider))
            {
              paramLayoutManager = ((RecyclerView.SmoothScroller.ScrollVectorProvider)paramLayoutManager).computeScrollVectorForPosition(k - 1);
              paramInt1 = j;
              if (paramLayoutManager != null) {
                if ((paramLayoutManager.x >= 0.0F) && (paramLayoutManager.y >= 0.0F)) {
                  break label193;
                }
              }
            }
          }
        }
        label193:
        for (paramInt1 = 1;; paramInt1 = 0)
        {
          if (paramInt1 == 0) {
            break label198;
          }
          paramInt1 = i;
          if (paramInt2 == 0) {
            break;
          }
          paramInt1 = i - 1;
          break;
          paramInt2 = 0;
          break label101;
          if (paramInt2 > 0) {}
          for (paramInt2 = 1;; paramInt2 = 0) {
            break;
          }
        }
        label198:
        paramInt1 = i;
        if (paramInt2 != 0) {
          paramInt1 = i + 1;
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\PagerSnapHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */