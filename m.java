package com.testfairy;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import java.util.ArrayList;
import java.util.List;

public class m
  implements SensorEventListener
{
  public static final int a = 11;
  public static final int b = 13;
  public static final int c = 15;
  private static final int d = 13;
  private final d e = new d();
  private int f = 13;
  private a g;
  
  private boolean a(SensorEvent paramSensorEvent)
  {
    boolean bool = true;
    float f1 = paramSensorEvent.values[0];
    float f2 = paramSensorEvent.values[1];
    float f3 = paramSensorEvent.values[2];
    if (f1 * f1 + f2 * f2 + f3 * f3 > this.f * this.f) {}
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  public void a(a parama)
  {
    this.g = parama;
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    boolean bool = a(paramSensorEvent);
    long l = paramSensorEvent.timestamp;
    this.e.a(l, bool);
    if (this.e.c())
    {
      this.e.a();
      if (this.g != null) {
        this.g.a();
      }
    }
  }
  
  public static abstract interface a
  {
    public abstract void a();
  }
  
  static class b
  {
    long a;
    boolean b;
    b c;
  }
  
  static class c
  {
    private m.b a;
    
    m.b a()
    {
      m.b localb = this.a;
      if (localb == null) {
        localb = new m.b();
      }
      for (;;)
      {
        return localb;
        this.a = localb.c;
      }
    }
    
    void a(m.b paramb)
    {
      paramb.c = this.a;
      this.a = paramb;
    }
  }
  
  static class d
  {
    private static final long a = 500000000L;
    private static final long b = 250000000L;
    private static final int c = 4;
    private final m.c d = new m.c();
    private m.b e;
    private m.b f;
    private int g;
    private int h;
    
    void a()
    {
      while (this.e != null)
      {
        m.b localb = this.e;
        this.e = localb.c;
        this.d.a(localb);
      }
      this.f = null;
      this.g = 0;
      this.h = 0;
    }
    
    void a(long paramLong)
    {
      while ((this.g >= 4) && (this.e != null) && (paramLong - this.e.a > 0L))
      {
        m.b localb = this.e;
        if (localb.b) {
          this.h -= 1;
        }
        this.g -= 1;
        this.e = localb.c;
        if (this.e == null) {
          this.f = null;
        }
        this.d.a(localb);
      }
    }
    
    void a(long paramLong, boolean paramBoolean)
    {
      a(paramLong - 500000000L);
      m.b localb = this.d.a();
      localb.a = paramLong;
      localb.b = paramBoolean;
      localb.c = null;
      if (this.f != null) {
        this.f.c = localb;
      }
      this.f = localb;
      if (this.e == null) {
        this.e = localb;
      }
      this.g += 1;
      if (paramBoolean) {
        this.h += 1;
      }
    }
    
    List b()
    {
      ArrayList localArrayList = new ArrayList();
      for (m.b localb = this.e; localb != null; localb = localb.c) {
        localArrayList.add(localb);
      }
      return localArrayList;
    }
    
    boolean c()
    {
      if ((this.f != null) && (this.e != null) && (this.f.a - this.e.a >= 250000000L) && (this.h >= (this.g >> 1) + (this.g >> 2))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */