package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.id;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class AlertDialogLayout
  extends LinearLayoutCompat
{
  public AlertDialogLayout(@Nullable Context paramContext)
  {
    super(paramContext);
  }
  
  public AlertDialogLayout(@Nullable Context paramContext, @Nullable AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private void forceUniformWidth(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    for (int i = 0; i < paramInt1; i++)
    {
      View localView = getChildAt(i);
      if (localView.getVisibility() != 8)
      {
        LinearLayoutCompat.LayoutParams localLayoutParams = (LinearLayoutCompat.LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.width == -1)
        {
          int k = localLayoutParams.height;
          localLayoutParams.height = localView.getMeasuredHeight();
          measureChildWithMargins(localView, j, 0, paramInt2, 0);
          localLayoutParams.height = k;
        }
      }
    }
  }
  
  private static int resolveMinimumHeight(View paramView)
  {
    int i = ViewCompat.getMinimumHeight(paramView);
    if (i > 0) {}
    for (;;)
    {
      return i;
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        if (paramView.getChildCount() == 1)
        {
          i = resolveMinimumHeight(paramView.getChildAt(0));
          continue;
        }
      }
      i = 0;
    }
  }
  
  private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
  
  private boolean tryOnMeasure(int paramInt1, int paramInt2)
  {
    Object localObject1 = null;
    Object localObject3 = null;
    Object localObject2 = null;
    int i5 = getChildCount();
    int i = 0;
    View localView;
    int j;
    label83:
    boolean bool;
    if (i < i5)
    {
      localView = getChildAt(i);
      if (localView.getVisibility() == 8) {}
      for (;;)
      {
        i++;
        break;
        j = localView.getId();
        if (j == R.id.topPanel)
        {
          localObject1 = localView;
        }
        else
        {
          if (j != R.id.buttonPanel) {
            break label83;
          }
          localObject3 = localView;
        }
      }
      if ((j == R.id.contentPanel) || (j == R.id.customPanel)) {
        if (localObject2 != null) {
          bool = false;
        }
      }
    }
    for (;;)
    {
      return bool;
      localObject2 = localView;
      break;
      bool = false;
      continue;
      int i7 = View.MeasureSpec.getMode(paramInt2);
      int i3 = View.MeasureSpec.getSize(paramInt2);
      int i6 = View.MeasureSpec.getMode(paramInt1);
      int k = 0;
      i = getPaddingTop() + getPaddingBottom();
      j = i;
      if (localObject1 != null)
      {
        ((View)localObject1).measure(paramInt1, 0);
        j = i + ((View)localObject1).getMeasuredHeight();
        k = ViewCompat.combineMeasuredStates(0, ViewCompat.getMeasuredState((View)localObject1));
      }
      i = 0;
      int i2 = 0;
      int n = k;
      int m = j;
      if (localObject3 != null)
      {
        ((View)localObject3).measure(paramInt1, 0);
        i = resolveMinimumHeight((View)localObject3);
        i2 = ((View)localObject3).getMeasuredHeight() - i;
        m = j + i;
        n = ViewCompat.combineMeasuredStates(k, ViewCompat.getMeasuredState((View)localObject3));
      }
      int i1 = 0;
      k = n;
      j = m;
      if (localObject2 != null) {
        if (i7 != 0) {
          break label527;
        }
      }
      label527:
      for (j = 0;; j = View.MeasureSpec.makeMeasureSpec(Math.max(0, i3 - m), i7))
      {
        ((View)localObject2).measure(paramInt1, j);
        i1 = ((View)localObject2).getMeasuredHeight();
        j = m + i1;
        k = ViewCompat.combineMeasuredStates(n, ViewCompat.getMeasuredState((View)localObject2));
        i3 -= j;
        m = k;
        int i4 = i3;
        n = j;
        if (localObject3 != null)
        {
          i2 = Math.min(i3, i2);
          n = i;
          m = i3;
          if (i2 > 0)
          {
            m = i3 - i2;
            n = i + i2;
          }
          ((View)localObject3).measure(paramInt1, View.MeasureSpec.makeMeasureSpec(n, 1073741824));
          n = j - i + ((View)localObject3).getMeasuredHeight();
          i = ViewCompat.combineMeasuredStates(k, ViewCompat.getMeasuredState((View)localObject3));
          i4 = m;
          m = i;
        }
        j = m;
        i = n;
        if (localObject2 != null)
        {
          j = m;
          i = n;
          if (i4 > 0)
          {
            ((View)localObject2).measure(paramInt1, View.MeasureSpec.makeMeasureSpec(i1 + i4, i7));
            i = n - i1 + ((View)localObject2).getMeasuredHeight();
            j = ViewCompat.combineMeasuredStates(m, ViewCompat.getMeasuredState((View)localObject2));
          }
        }
        k = 0;
        m = 0;
        while (m < i5)
        {
          localView = getChildAt(m);
          n = k;
          if (localView.getVisibility() != 8) {
            n = Math.max(k, localView.getMeasuredWidth());
          }
          m++;
          k = n;
        }
      }
      setMeasuredDimension(ViewCompat.resolveSizeAndState(k + (getPaddingLeft() + getPaddingRight()), paramInt1, j), ViewCompat.resolveSizeAndState(i, paramInt2, 0));
      if (i6 != 1073741824) {
        forceUniformWidth(i5, paramInt2);
      }
      bool = true;
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int j = getPaddingLeft();
    int m = paramInt3 - paramInt1;
    int k = getPaddingRight();
    int n = getPaddingRight();
    paramInt1 = getMeasuredHeight();
    int i2 = getChildCount();
    int i1 = getGravity();
    Object localObject;
    label91:
    label94:
    View localView;
    int i3;
    int i4;
    int i;
    switch (i1 & 0x70)
    {
    default: 
      paramInt1 = getPaddingTop();
      localObject = getDividerDrawable();
      if (localObject == null)
      {
        paramInt3 = 0;
        paramInt4 = 0;
        if (paramInt4 >= i2) {
          return;
        }
        localView = getChildAt(paramInt4);
        paramInt2 = paramInt1;
        if (localView != null)
        {
          paramInt2 = paramInt1;
          if (localView.getVisibility() != 8)
          {
            i3 = localView.getMeasuredWidth();
            i4 = localView.getMeasuredHeight();
            localObject = (LinearLayoutCompat.LayoutParams)localView.getLayoutParams();
            i = ((LinearLayoutCompat.LayoutParams)localObject).gravity;
            paramInt2 = i;
            if (i < 0) {
              paramInt2 = i1 & 0x800007;
            }
            switch (GravityCompat.getAbsoluteGravity(paramInt2, ViewCompat.getLayoutDirection(this)) & 0x7)
            {
            default: 
              paramInt2 = j + ((LinearLayoutCompat.LayoutParams)localObject).leftMargin;
            }
          }
        }
      }
      break;
    }
    for (;;)
    {
      i = paramInt1;
      if (hasDividerBeforeChildAt(paramInt4)) {
        i = paramInt1 + paramInt3;
      }
      paramInt1 = i + ((LinearLayoutCompat.LayoutParams)localObject).topMargin;
      setChildFrame(localView, paramInt2, paramInt1, i3, i4);
      paramInt2 = paramInt1 + (((LinearLayoutCompat.LayoutParams)localObject).bottomMargin + i4);
      paramInt4++;
      paramInt1 = paramInt2;
      break label94;
      paramInt1 = getPaddingTop() + paramInt4 - paramInt2 - paramInt1;
      break;
      paramInt1 = getPaddingTop() + (paramInt4 - paramInt2 - paramInt1) / 2;
      break;
      paramInt3 = ((Drawable)localObject).getIntrinsicHeight();
      break label91;
      paramInt2 = (m - j - n - i3) / 2 + j + ((LinearLayoutCompat.LayoutParams)localObject).leftMargin - ((LinearLayoutCompat.LayoutParams)localObject).rightMargin;
      continue;
      paramInt2 = m - k - i3 - ((LinearLayoutCompat.LayoutParams)localObject).rightMargin;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (!tryOnMeasure(paramInt1, paramInt2)) {
      super.onMeasure(paramInt1, paramInt2);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\AlertDialogLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */