package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePartial;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

class StringConverter
  extends AbstractConverter
  implements InstantConverter, PartialConverter, DurationConverter, PeriodConverter, IntervalConverter
{
  static final StringConverter INSTANCE = new StringConverter();
  
  public long getDurationMillis(Object paramObject)
  {
    int n = 1;
    String str = (String)paramObject;
    int i = str.length();
    int k;
    int j;
    if ((i >= 4) && ((str.charAt(0) == 'P') || (str.charAt(0) == 'p')) && ((str.charAt(1) == 'T') || (str.charAt(1) == 't')) && ((str.charAt(i - 1) == 'S') || (str.charAt(i - 1) == 's')))
    {
      paramObject = str.substring(2, i - 1);
      i = 0;
      k = 0;
      j = -1;
      if (i >= ((String)paramObject).length()) {
        break label267;
      }
      if ((((String)paramObject).charAt(i) < '0') || (((String)paramObject).charAt(i) > '9')) {
        break label174;
      }
    }
    for (;;)
    {
      i++;
      break;
      throw new IllegalArgumentException("Invalid format: \"" + str + '"');
      label174:
      if ((i != 0) || (((String)paramObject).charAt(0) != '-')) {
        break label194;
      }
      k = 1;
    }
    label194:
    if (k != 0) {}
    for (int m = 1;; m = 0)
    {
      if ((i <= m) || (((String)paramObject).charAt(i) != '.') || (j != -1)) {
        break label234;
      }
      j = i;
      break;
    }
    label234:
    throw new IllegalArgumentException("Invalid format: \"" + str + '"');
    label267:
    long l2;
    if (k != 0)
    {
      i = n;
      if (j <= 0) {
        break label374;
      }
      l1 = Long.parseLong(((String)paramObject).substring(i, j));
      str = ((String)paramObject).substring(j + 1);
      paramObject = str;
      if (str.length() != 3) {
        paramObject = (str + "000").substring(0, 3);
      }
      l2 = Integer.parseInt((String)paramObject);
      label344:
      if (k == 0) {
        break label411;
      }
    }
    label374:
    label411:
    for (long l1 = FieldUtils.safeAdd(FieldUtils.safeMultiply(-l1, 1000), -l2);; l1 = FieldUtils.safeAdd(FieldUtils.safeMultiply(l1, 1000), l2))
    {
      return l1;
      i = 0;
      break;
      if (k != 0)
      {
        l1 = Long.parseLong(((String)paramObject).substring(i, ((String)paramObject).length()));
        l2 = 0L;
        break label344;
      }
      l1 = Long.parseLong((String)paramObject);
      l2 = 0L;
      break label344;
    }
  }
  
  public long getInstantMillis(Object paramObject, Chronology paramChronology)
  {
    paramObject = (String)paramObject;
    return ISODateTimeFormat.dateTimeParser().withChronology(paramChronology).parseMillis((String)paramObject);
  }
  
  public int[] getPartialValues(ReadablePartial paramReadablePartial, Object paramObject, Chronology paramChronology, DateTimeFormatter paramDateTimeFormatter)
  {
    Chronology localChronology = paramChronology;
    if (paramDateTimeFormatter.getZone() != null) {
      localChronology = paramChronology.withZone(paramDateTimeFormatter.getZone());
    }
    return localChronology.get(paramReadablePartial, paramDateTimeFormatter.withChronology(localChronology).parseMillis((String)paramObject));
  }
  
  public Class<?> getSupportedType()
  {
    return String.class;
  }
  
  public void setInto(ReadWritableInterval paramReadWritableInterval, Object paramObject, Chronology paramChronology)
  {
    Period localPeriod = null;
    String str1 = (String)paramObject;
    int i = str1.indexOf('/');
    if (i < 0) {
      throw new IllegalArgumentException("Format requires a '/' separator: " + str1);
    }
    paramObject = str1.substring(0, i);
    if (((String)paramObject).length() <= 0) {
      throw new IllegalArgumentException("Format invalid: " + str1);
    }
    String str2 = str1.substring(i + 1);
    if (str2.length() <= 0) {
      throw new IllegalArgumentException("Format invalid: " + str1);
    }
    Object localObject = ISODateTimeFormat.dateTimeParser().withChronology(paramChronology);
    PeriodFormatter localPeriodFormatter = ISOPeriodFormat.standard();
    long l1 = 0L;
    i = ((String)paramObject).charAt(0);
    if ((i == 80) || (i == 112)) {
      localPeriod = localPeriodFormatter.withParseType(getPeriodType(paramObject)).parsePeriod((String)paramObject);
    }
    for (paramObject = null;; paramObject = ((DateTime)paramObject).getChronology())
    {
      i = str2.charAt(0);
      if ((i != 80) && (i != 112)) {
        break label330;
      }
      if (localPeriod == null) {
        break;
      }
      throw new IllegalArgumentException("Interval composed of two durations: " + str1);
      paramObject = ((DateTimeFormatter)localObject).parseDateTime((String)paramObject);
      l1 = ((DateTime)paramObject).getMillis();
    }
    localPeriod = localPeriodFormatter.withParseType(getPeriodType(str2)).parsePeriod(str2);
    long l2;
    if (paramChronology != null) {
      l2 = paramChronology.add(localPeriod, l1, 1);
    }
    label330:
    label350:
    label382:
    label387:
    for (;;)
    {
      paramReadWritableInterval.setInterval(l1, l2);
      paramReadWritableInterval.setChronology(paramChronology);
      return;
      paramChronology = (Chronology)paramObject;
      break;
      localObject = ((DateTimeFormatter)localObject).parseDateTime(str2);
      l2 = ((DateTime)localObject).getMillis();
      if (paramObject != null) {
        if (paramChronology == null) {
          break label382;
        }
      }
      for (;;)
      {
        if (localPeriod == null) {
          break label387;
        }
        l1 = paramChronology.add(localPeriod, l2, -1);
        break;
        paramObject = ((DateTime)localObject).getChronology();
        break label350;
        paramChronology = (Chronology)paramObject;
      }
    }
  }
  
  public void setInto(ReadWritablePeriod paramReadWritablePeriod, Object paramObject, Chronology paramChronology)
  {
    paramChronology = (String)paramObject;
    paramObject = ISOPeriodFormat.standard();
    paramReadWritablePeriod.clear();
    int i = ((PeriodFormatter)paramObject).parseInto(paramReadWritablePeriod, paramChronology, 0);
    if (i < paramChronology.length())
    {
      if (i < 0) {
        ((PeriodFormatter)paramObject).withParseType(paramReadWritablePeriod.getPeriodType()).parseMutablePeriod(paramChronology);
      }
      throw new IllegalArgumentException("Invalid format: \"" + paramChronology + '"');
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\StringConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */