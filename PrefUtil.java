package com.afollestad.materialdialogs.prefs;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceManager.OnActivityDestroyListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import com.afollestad.materialdialogs.commons.R.styleable;
import java.lang.reflect.Method;

class PrefUtil
{
  static void registerOnActivityDestroyListener(@NonNull Preference paramPreference, @NonNull PreferenceManager.OnActivityDestroyListener paramOnActivityDestroyListener)
  {
    try
    {
      PreferenceManager localPreferenceManager = paramPreference.getPreferenceManager();
      paramPreference = localPreferenceManager.getClass().getDeclaredMethod("registerOnActivityDestroyListener", new Class[] { PreferenceManager.OnActivityDestroyListener.class });
      paramPreference.setAccessible(true);
      paramPreference.invoke(localPreferenceManager, new Object[] { paramOnActivityDestroyListener });
      return;
    }
    catch (Exception paramPreference)
    {
      for (;;) {}
    }
  }
  
  static void setLayoutResource(@NonNull Context paramContext, @NonNull Preference paramPreference, @Nullable AttributeSet paramAttributeSet)
  {
    int k = 0;
    int j = k;
    if (paramAttributeSet != null) {}
    for (int i = 0;; i++)
    {
      j = k;
      boolean bool;
      if (i < paramAttributeSet.getAttributeCount())
      {
        if ((((XmlResourceParser)paramAttributeSet).getAttributeNamespace(0).equals("http://schemas.android.com/apk/res/android")) && (paramAttributeSet.getAttributeName(i).equals("layout"))) {
          j = 1;
        }
      }
      else
      {
        bool = false;
        if (paramAttributeSet != null) {
          paramContext = paramContext.getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.Preference, 0, 0);
        }
      }
      try
      {
        bool = paramContext.getBoolean(R.styleable.Preference_useStockLayout, false);
        paramContext.recycle();
        if ((j == 0) && (!bool)) {}
        return;
      }
      finally
      {
        paramContext.recycle();
      }
    }
  }
  
  static void unregisterOnActivityDestroyListener(@NonNull Preference paramPreference, @NonNull PreferenceManager.OnActivityDestroyListener paramOnActivityDestroyListener)
  {
    try
    {
      PreferenceManager localPreferenceManager = paramPreference.getPreferenceManager();
      paramPreference = localPreferenceManager.getClass().getDeclaredMethod("unregisterOnActivityDestroyListener", new Class[] { PreferenceManager.OnActivityDestroyListener.class });
      paramPreference.setAccessible(true);
      paramPreference.invoke(localPreferenceManager, new Object[] { paramOnActivityDestroyListener });
      return;
    }
    catch (Exception paramPreference)
    {
      for (;;) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\prefs\PrefUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */