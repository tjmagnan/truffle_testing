package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;

public class Elements
  extends ArrayList<Element>
{
  public Elements() {}
  
  public Elements(int paramInt)
  {
    super(paramInt);
  }
  
  public Elements(Collection<Element> paramCollection)
  {
    super(paramCollection);
  }
  
  public Elements(List<Element> paramList)
  {
    super(paramList);
  }
  
  public Elements(Element... paramVarArgs)
  {
    super(Arrays.asList(paramVarArgs));
  }
  
  private Elements siblings(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    Elements localElements = new Elements();
    Evaluator localEvaluator;
    if (paramString != null)
    {
      localEvaluator = QueryParser.parse(paramString);
      Iterator localIterator = iterator();
      label25:
      if (localIterator.hasNext()) {
        paramString = (Element)localIterator.next();
      }
    }
    else
    {
      label55:
      label90:
      label92:
      label109:
      for (;;)
      {
        if (paramBoolean1)
        {
          paramString = paramString.nextElementSibling();
          if (paramString == null) {
            break label90;
          }
          if (localEvaluator != null) {
            break label92;
          }
          localElements.add(paramString);
        }
        for (;;)
        {
          if (paramBoolean2) {
            break label109;
          }
          break label25;
          localEvaluator = null;
          break;
          paramString = paramString.previousElementSibling();
          break label55;
          break label25;
          if (paramString.is(localEvaluator)) {
            localElements.add(paramString);
          }
        }
      }
    }
    return localElements;
  }
  
  public Elements addClass(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).addClass(paramString);
    }
    return this;
  }
  
  public Elements after(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).after(paramString);
    }
    return this;
  }
  
  public Elements append(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).append(paramString);
    }
    return this;
  }
  
  public String attr(String paramString)
  {
    Iterator localIterator = iterator();
    Element localElement;
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      localElement = (Element)localIterator.next();
    } while (!localElement.hasAttr(paramString));
    for (paramString = localElement.attr(paramString);; paramString = "") {
      return paramString;
    }
  }
  
  public Elements attr(String paramString1, String paramString2)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).attr(paramString1, paramString2);
    }
    return this;
  }
  
  public Elements before(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).before(paramString);
    }
    return this;
  }
  
  public Elements clone()
  {
    Elements localElements = new Elements(size());
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      localElements.add(((Element)localIterator.next()).clone());
    }
    return localElements;
  }
  
  public Elements empty()
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).empty();
    }
    return this;
  }
  
  public Elements eq(int paramInt)
  {
    if (size() > paramInt) {}
    for (Elements localElements = new Elements(new Element[] { (Element)get(paramInt) });; localElements = new Elements()) {
      return localElements;
    }
  }
  
  public Element first()
  {
    if (isEmpty()) {}
    for (Element localElement = null;; localElement = (Element)get(0)) {
      return localElement;
    }
  }
  
  public List<FormElement> forms()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      Element localElement = (Element)localIterator.next();
      if ((localElement instanceof FormElement)) {
        localArrayList.add((FormElement)localElement);
      }
    }
    return localArrayList;
  }
  
  public boolean hasAttr(String paramString)
  {
    Iterator localIterator = iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
    } while (!((Element)localIterator.next()).hasAttr(paramString));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean hasClass(String paramString)
  {
    Iterator localIterator = iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
    } while (!((Element)localIterator.next()).hasClass(paramString));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean hasText()
  {
    Iterator localIterator = iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
    } while (!((Element)localIterator.next()).hasText());
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public String html()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      Element localElement = (Element)localIterator.next();
      if (localStringBuilder.length() != 0) {
        localStringBuilder.append("\n");
      }
      localStringBuilder.append(localElement.html());
    }
    return localStringBuilder.toString();
  }
  
  public Elements html(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).html(paramString);
    }
    return this;
  }
  
  public boolean is(String paramString)
  {
    Evaluator localEvaluator = QueryParser.parse(paramString);
    paramString = iterator();
    do
    {
      if (!paramString.hasNext()) {
        break;
      }
    } while (!((Element)paramString.next()).is(localEvaluator));
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Element last()
  {
    if (isEmpty()) {}
    for (Element localElement = null;; localElement = (Element)get(size() - 1)) {
      return localElement;
    }
  }
  
  public Elements next()
  {
    return siblings(null, true, false);
  }
  
  public Elements next(String paramString)
  {
    return siblings(paramString, true, false);
  }
  
  public Elements nextAll()
  {
    return siblings(null, true, true);
  }
  
  public Elements nextAll(String paramString)
  {
    return siblings(paramString, true, true);
  }
  
  public Elements not(String paramString)
  {
    return Selector.filterOut(this, Selector.select(paramString, this));
  }
  
  public String outerHtml()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      Element localElement = (Element)localIterator.next();
      if (localStringBuilder.length() != 0) {
        localStringBuilder.append("\n");
      }
      localStringBuilder.append(localElement.outerHtml());
    }
    return localStringBuilder.toString();
  }
  
  public Elements parents()
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      localLinkedHashSet.addAll(((Element)localIterator.next()).parents());
    }
    return new Elements(localLinkedHashSet);
  }
  
  public Elements prepend(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).prepend(paramString);
    }
    return this;
  }
  
  public Elements prev()
  {
    return siblings(null, false, false);
  }
  
  public Elements prev(String paramString)
  {
    return siblings(paramString, false, false);
  }
  
  public Elements prevAll()
  {
    return siblings(null, false, true);
  }
  
  public Elements prevAll(String paramString)
  {
    return siblings(paramString, false, true);
  }
  
  public Elements remove()
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).remove();
    }
    return this;
  }
  
  public Elements removeAttr(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).removeAttr(paramString);
    }
    return this;
  }
  
  public Elements removeClass(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).removeClass(paramString);
    }
    return this;
  }
  
  public Elements select(String paramString)
  {
    return Selector.select(paramString, this);
  }
  
  public Elements tagName(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).tagName(paramString);
    }
    return this;
  }
  
  public String text()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = iterator();
    while (localIterator.hasNext())
    {
      Element localElement = (Element)localIterator.next();
      if (localStringBuilder.length() != 0) {
        localStringBuilder.append(" ");
      }
      localStringBuilder.append(localElement.text());
    }
    return localStringBuilder.toString();
  }
  
  public String toString()
  {
    return outerHtml();
  }
  
  public Elements toggleClass(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).toggleClass(paramString);
    }
    return this;
  }
  
  public Elements traverse(NodeVisitor paramNodeVisitor)
  {
    Validate.notNull(paramNodeVisitor);
    NodeTraversor localNodeTraversor = new NodeTraversor(paramNodeVisitor);
    paramNodeVisitor = iterator();
    while (paramNodeVisitor.hasNext()) {
      localNodeTraversor.traverse((Element)paramNodeVisitor.next());
    }
    return this;
  }
  
  public Elements unwrap()
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).unwrap();
    }
    return this;
  }
  
  public String val()
  {
    if (size() > 0) {}
    for (String str = first().val();; str = "") {
      return str;
    }
  }
  
  public Elements val(String paramString)
  {
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).val(paramString);
    }
    return this;
  }
  
  public Elements wrap(String paramString)
  {
    Validate.notEmpty(paramString);
    Iterator localIterator = iterator();
    while (localIterator.hasNext()) {
      ((Element)localIterator.next()).wrap(paramString);
    }
    return this;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\select\Elements.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */