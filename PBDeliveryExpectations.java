package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

class PBDeliveryExpectations
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -8899483591228981115L;
  @Expose
  @SerializedName("additionalDetails")
  private String additionalDetails;
  @Expose
  @SerializedName("estimatedDeliveryDateTime")
  private String estimatedDeliveryDateTime;
  @Expose
  @SerializedName("guaranteed")
  private Boolean guaranteed;
  @Expose
  @SerializedName("maxEstimatedNumberOfDays")
  private Integer maxEstimatedNumberOfDays;
  @Expose
  @SerializedName("minEstimatedNumberOfDays")
  private Integer minEstimatedNumberOfDays;
  
  public PBDeliveryExpectations() {}
  
  public PBDeliveryExpectations(String paramString1, String paramString2, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    this.additionalDetails = paramString1;
    this.estimatedDeliveryDateTime = paramString2;
    this.guaranteed = paramBoolean;
    this.maxEstimatedNumberOfDays = paramInteger1;
    this.minEstimatedNumberOfDays = paramInteger2;
  }
  
  public String getAdditionalDetails()
  {
    return this.additionalDetails;
  }
  
  public String getEstimatedDeliveryDateTime()
  {
    return this.estimatedDeliveryDateTime;
  }
  
  public Boolean getGuaranteed()
  {
    return this.guaranteed;
  }
  
  public Integer getMaxEstimatedNumberOfDays()
  {
    return this.maxEstimatedNumberOfDays;
  }
  
  public Integer getMinEstimatedNumberOfDays()
  {
    return this.minEstimatedNumberOfDays;
  }
  
  public void setAdditionalDetails(String paramString)
  {
    this.additionalDetails = paramString;
  }
  
  public void setEstimatedDeliveryDateTime(String paramString)
  {
    this.estimatedDeliveryDateTime = paramString;
  }
  
  public void setGuaranteed(Boolean paramBoolean)
  {
    this.guaranteed = paramBoolean;
  }
  
  public void setMaxEstimatedNumberOfDays(Integer paramInteger)
  {
    this.maxEstimatedNumberOfDays = paramInteger;
  }
  
  public void setMinEstimatedNumberOfDays(Integer paramInteger)
  {
    this.minEstimatedNumberOfDays = paramInteger;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBDeliveryExpectations.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */