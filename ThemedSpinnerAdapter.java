package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.widget.SpinnerAdapter;

public abstract interface ThemedSpinnerAdapter
  extends SpinnerAdapter
{
  @Nullable
  public abstract Resources.Theme getDropDownViewTheme();
  
  public abstract void setDropDownViewTheme(@Nullable Resources.Theme paramTheme);
  
  public static final class Helper
  {
    private final Context mContext;
    private LayoutInflater mDropDownInflater;
    private final LayoutInflater mInflater;
    
    public Helper(@NonNull Context paramContext)
    {
      this.mContext = paramContext;
      this.mInflater = LayoutInflater.from(paramContext);
    }
    
    @NonNull
    public LayoutInflater getDropDownViewInflater()
    {
      if (this.mDropDownInflater != null) {}
      for (LayoutInflater localLayoutInflater = this.mDropDownInflater;; localLayoutInflater = this.mInflater) {
        return localLayoutInflater;
      }
    }
    
    @Nullable
    public Resources.Theme getDropDownViewTheme()
    {
      if (this.mDropDownInflater == null) {}
      for (Resources.Theme localTheme = null;; localTheme = this.mDropDownInflater.getContext().getTheme()) {
        return localTheme;
      }
    }
    
    public void setDropDownViewTheme(@Nullable Resources.Theme paramTheme)
    {
      if (paramTheme == null) {
        this.mDropDownInflater = null;
      }
      for (;;)
      {
        return;
        if (paramTheme == this.mContext.getTheme()) {
          this.mDropDownInflater = this.mInflater;
        } else {
          this.mDropDownInflater = LayoutInflater.from(new ContextThemeWrapper(this.mContext, paramTheme));
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\ThemedSpinnerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */