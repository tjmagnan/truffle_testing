package com.testfairy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.util.Log;
import com.testfairy.activities.AutoUpdateActivity;
import com.testfairy.activities.AutoUpdateActivity.a;
import com.testfairy.p.c;
import com.testfairy.p.i;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private final a a;
  private String b;
  private String c;
  private String d;
  private boolean e = true;
  private AlertDialog f;
  
  public a(a parama)
  {
    this.a = parama;
  }
  
  private void b(Activity paramActivity)
  {
    if ((paramActivity == null) || (paramActivity.isFinishing()))
    {
      Log.d(e.a, "Fail to update, there is no Activity");
      this.a.a(0);
    }
    for (;;)
    {
      return;
      AutoUpdateActivity.a(new AutoUpdateActivity.a()
      {
        public void a()
        {
          a.a(a.this).a(3);
        }
        
        public void b()
        {
          a.a(a.this).a(4);
        }
      });
      Intent localIntent = new Intent(paramActivity, AutoUpdateActivity.class);
      localIntent.setFlags(67108864);
      localIntent.setFlags(32768);
      localIntent.setFlags(268435456);
      localIntent.putExtra("appName", this.b);
      localIntent.putExtra("newVersion", this.c);
      localIntent.putExtra("upgradeUrl", this.d);
      localIntent.putExtra("isUpgradeMandatory", false);
      paramActivity.startActivity(localIntent);
    }
  }
  
  public void a()
  {
    if (this.f == null) {}
    for (;;)
    {
      return;
      this.f.dismiss();
      this.f = null;
    }
  }
  
  public void a(final Activity paramActivity)
  {
    if (this.e) {}
    for (;;)
    {
      return;
      if (paramActivity == null)
      {
        Log.d(e.a, "Fail to update, there is no Activity to show the 'New version is ready' Dialog");
        this.a.a(0);
      }
      else
      {
        String str = "Would you like to download and install the new version?";
        if (!i.a(paramActivity)) {
          str = "Would you like to download and install the new version? (Storage permission will be required)";
        }
        this.f = c.a(paramActivity).setTitle("New version is available").setMessage(str).setCancelable(false).setIcon(17301599).setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            a.a(a.this, paramActivity);
            a.a(a.this, null);
            a.a(a.this, null);
          }
        }).setNegativeButton("No", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            a.a(a.this, null);
            a.a(a.this, null);
            a.a(a.this).a(2);
          }
        }).create();
        this.f.show();
      }
    }
  }
  
  public void a(JSONObject paramJSONObject)
  {
    try
    {
      this.b = paramJSONObject.getString("appName");
      this.c = paramJSONObject.getString("newVersion");
      this.d = paramJSONObject.getString("upgradeUrl");
      this.e = false;
      return;
    }
    catch (JSONException paramJSONObject)
    {
      for (;;)
      {
        this.e = true;
        Log.d(e.a, "Fail to update, can parse data", paramJSONObject);
        this.a.a(1);
      }
    }
  }
  
  public boolean b()
  {
    if (this.b != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(int paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */