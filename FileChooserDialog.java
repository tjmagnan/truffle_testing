package com.afollestad.materialdialogs.folderselector;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v13.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.MimeTypeMap;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.ListCallback;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.afollestad.materialdialogs.commons.R.string;
import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FileChooserDialog
  extends DialogFragment
  implements MaterialDialog.ListCallback
{
  private static final String DEFAULT_TAG = "[MD_FILE_SELECTOR]";
  private FileCallback callback;
  private boolean canGoUp = true;
  private File[] parentContents;
  private File parentFolder;
  
  private void checkIfCanGoUp()
  {
    for (boolean bool = true;; bool = false) {
      try
      {
        if (this.parentFolder.getPath().split("/").length > 1)
        {
          this.canGoUp = bool;
          return;
        }
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        for (;;)
        {
          this.canGoUp = false;
        }
      }
    }
  }
  
  @NonNull
  private Builder getBuilder()
  {
    return (Builder)getArguments().getSerializable("builder");
  }
  
  boolean fileIsMimeType(File paramFile, String paramString, MimeTypeMap paramMimeTypeMap)
  {
    boolean bool;
    if ((paramString == null) || (paramString.equals("*/*"))) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      paramFile = paramFile.toURI().toString();
      int i = paramFile.lastIndexOf('.');
      if (i == -1)
      {
        bool = false;
      }
      else
      {
        paramFile = paramFile.substring(i + 1);
        if (paramFile.endsWith("json"))
        {
          bool = paramString.startsWith("application/json");
        }
        else
        {
          paramFile = paramMimeTypeMap.getMimeTypeFromExtension(paramFile);
          if (paramFile == null)
          {
            bool = false;
          }
          else if (paramFile.equals(paramString))
          {
            bool = true;
          }
          else
          {
            i = paramString.lastIndexOf('/');
            if (i == -1)
            {
              bool = false;
            }
            else
            {
              paramMimeTypeMap = paramString.substring(0, i);
              if (!paramString.substring(i + 1).equals("*"))
              {
                bool = false;
              }
              else
              {
                i = paramFile.lastIndexOf('/');
                if (i == -1) {
                  bool = false;
                } else if (paramFile.substring(0, i).equals(paramMimeTypeMap)) {
                  bool = true;
                } else {
                  bool = false;
                }
              }
            }
          }
        }
      }
    }
  }
  
  CharSequence[] getContentsArray()
  {
    int i = 1;
    Object localObject;
    if (this.parentContents == null) {
      if (this.canGoUp)
      {
        localObject = new String[1];
        localObject[0] = getBuilder().goUpLabel;
      }
    }
    String[] arrayOfString;
    label82:
    do
    {
      for (;;)
      {
        return (CharSequence[])localObject;
        localObject = new String[0];
      }
      j = this.parentContents.length;
      if (!this.canGoUp) {
        break;
      }
      arrayOfString = new String[i + j];
      if (this.canGoUp) {
        arrayOfString[0] = getBuilder().goUpLabel;
      }
      i = 0;
      localObject = arrayOfString;
    } while (i >= this.parentContents.length);
    if (this.canGoUp) {}
    for (int j = i + 1;; j = i)
    {
      arrayOfString[j] = this.parentContents[i].getName();
      i++;
      break label82;
      i = 0;
      break;
    }
  }
  
  @NonNull
  public String getInitialPath()
  {
    return getBuilder().initialPath;
  }
  
  File[] listFiles(@Nullable String paramString, @Nullable String[] paramArrayOfString)
  {
    File[] arrayOfFile = this.parentFolder.listFiles();
    ArrayList localArrayList = new ArrayList();
    if (arrayOfFile != null)
    {
      MimeTypeMap localMimeTypeMap = MimeTypeMap.getSingleton();
      int n = arrayOfFile.length;
      int i = 0;
      if (i < n)
      {
        File localFile = arrayOfFile[i];
        if (localFile.isDirectory()) {
          localArrayList.add(localFile);
        }
        for (;;)
        {
          i++;
          break;
          if (paramArrayOfString != null)
          {
            int m = 0;
            int i1 = paramArrayOfString.length;
            for (int j = 0;; j++)
            {
              int k = m;
              if (j < i1)
              {
                String str = paramArrayOfString[j];
                if (localFile.getName().toLowerCase().endsWith(str.toLowerCase())) {
                  k = 1;
                }
              }
              else
              {
                if (k == 0) {
                  break;
                }
                localArrayList.add(localFile);
                break;
              }
            }
          }
          if ((paramString != null) && (fileIsMimeType(localFile, paramString, localMimeTypeMap))) {
            localArrayList.add(localFile);
          }
        }
      }
      Collections.sort(localArrayList, new FileSorter(null));
    }
    for (paramString = (File[])localArrayList.toArray(new File[localArrayList.size()]);; paramString = null) {
      return paramString;
    }
  }
  
  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    this.callback = ((FileCallback)paramActivity);
  }
  
  @NonNull
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    if ((Build.VERSION.SDK_INT >= 23) && (ActivityCompat.checkSelfPermission(getActivity(), "android.permission.READ_EXTERNAL_STORAGE") != 0)) {}
    for (paramBundle = new MaterialDialog.Builder(getActivity()).title(R.string.md_error_label).content(R.string.md_storage_perm_error).positiveText(17039370).build();; paramBundle = new MaterialDialog.Builder(getActivity()).title(this.parentFolder.getAbsolutePath()).typeface(getBuilder().mediumFont, getBuilder().regularFont).items(getContentsArray()).itemsCallback(this).onNegative(new MaterialDialog.SingleButtonCallback()
        {
          public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
          {
            paramAnonymousMaterialDialog.dismiss();
          }
        }).autoDismiss(false).negativeText(getBuilder().cancelButton).build())
    {
      return paramBundle;
      if ((getArguments() == null) || (!getArguments().containsKey("builder"))) {
        throw new IllegalStateException("You must create a FileChooserDialog using the Builder.");
      }
      if (!getArguments().containsKey("current_path")) {
        getArguments().putString("current_path", getBuilder().initialPath);
      }
      this.parentFolder = new File(getArguments().getString("current_path"));
      checkIfCanGoUp();
      this.parentContents = listFiles(getBuilder().mimeType, getBuilder().extensions);
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    if (this.callback != null) {
      this.callback.onFileChooserDismissed(this);
    }
  }
  
  public void onSelection(MaterialDialog paramMaterialDialog, View paramView, int paramInt, CharSequence paramCharSequence)
  {
    boolean bool = true;
    if ((this.canGoUp) && (paramInt == 0))
    {
      this.parentFolder = this.parentFolder.getParentFile();
      if (this.parentFolder.getAbsolutePath().equals("/storage/emulated")) {
        this.parentFolder = this.parentFolder.getParentFile();
      }
      if (this.parentFolder.getParent() != null)
      {
        this.canGoUp = bool;
        label68:
        if (!this.parentFolder.isFile()) {
          break label162;
        }
        this.callback.onFileSelection(this, this.parentFolder);
        dismiss();
      }
    }
    for (;;)
    {
      return;
      bool = false;
      break;
      paramMaterialDialog = this.parentContents;
      int i = paramInt;
      if (this.canGoUp) {
        i = paramInt - 1;
      }
      this.parentFolder = paramMaterialDialog[i];
      this.canGoUp = true;
      if (!this.parentFolder.getAbsolutePath().equals("/storage/emulated")) {
        break label68;
      }
      this.parentFolder = Environment.getExternalStorageDirectory();
      break label68;
      label162:
      this.parentContents = listFiles(getBuilder().mimeType, getBuilder().extensions);
      paramMaterialDialog = (MaterialDialog)getDialog();
      paramMaterialDialog.setTitle(this.parentFolder.getAbsolutePath());
      getArguments().putString("current_path", this.parentFolder.getAbsolutePath());
      paramMaterialDialog.setItems(getContentsArray());
    }
  }
  
  public void show(FragmentActivity paramFragmentActivity)
  {
    String str = getBuilder().tag;
    Fragment localFragment = paramFragmentActivity.getSupportFragmentManager().findFragmentByTag(str);
    if (localFragment != null)
    {
      ((DialogFragment)localFragment).dismiss();
      paramFragmentActivity.getSupportFragmentManager().beginTransaction().remove(localFragment).commit();
    }
    show(paramFragmentActivity.getSupportFragmentManager(), str);
  }
  
  public static class Builder
    implements Serializable
  {
    @StringRes
    int cancelButton;
    @NonNull
    final transient AppCompatActivity context;
    String[] extensions;
    String goUpLabel;
    String initialPath;
    @Nullable
    String mediumFont;
    String mimeType;
    @Nullable
    String regularFont;
    String tag;
    
    public <ActivityType extends AppCompatActivity,  extends FileChooserDialog.FileCallback> Builder(@NonNull ActivityType paramActivityType)
    {
      this.context = paramActivityType;
      this.cancelButton = 17039360;
      this.initialPath = Environment.getExternalStorageDirectory().getAbsolutePath();
      this.mimeType = null;
      this.goUpLabel = "...";
    }
    
    @NonNull
    public FileChooserDialog build()
    {
      FileChooserDialog localFileChooserDialog = new FileChooserDialog();
      Bundle localBundle = new Bundle();
      localBundle.putSerializable("builder", this);
      localFileChooserDialog.setArguments(localBundle);
      return localFileChooserDialog;
    }
    
    @NonNull
    public Builder cancelButton(@StringRes int paramInt)
    {
      this.cancelButton = paramInt;
      return this;
    }
    
    @NonNull
    public Builder extensionsFilter(@Nullable String... paramVarArgs)
    {
      this.extensions = paramVarArgs;
      return this;
    }
    
    @NonNull
    public Builder goUpLabel(String paramString)
    {
      this.goUpLabel = paramString;
      return this;
    }
    
    @NonNull
    public Builder initialPath(@Nullable String paramString)
    {
      String str = paramString;
      if (paramString == null) {
        str = File.separator;
      }
      this.initialPath = str;
      return this;
    }
    
    @NonNull
    public Builder mimeType(@Nullable String paramString)
    {
      this.mimeType = paramString;
      return this;
    }
    
    @NonNull
    public FileChooserDialog show()
    {
      FileChooserDialog localFileChooserDialog = build();
      localFileChooserDialog.show(this.context);
      return localFileChooserDialog;
    }
    
    @NonNull
    public Builder tag(@Nullable String paramString)
    {
      String str = paramString;
      if (paramString == null) {
        str = "[MD_FILE_SELECTOR]";
      }
      this.tag = str;
      return this;
    }
    
    @NonNull
    public Builder typeface(@Nullable String paramString1, @Nullable String paramString2)
    {
      this.mediumFont = paramString1;
      this.regularFont = paramString2;
      return this;
    }
  }
  
  public static abstract interface FileCallback
  {
    public abstract void onFileChooserDismissed(@NonNull FileChooserDialog paramFileChooserDialog);
    
    public abstract void onFileSelection(@NonNull FileChooserDialog paramFileChooserDialog, @NonNull File paramFile);
  }
  
  private static class FileSorter
    implements Comparator<File>
  {
    public int compare(File paramFile1, File paramFile2)
    {
      int i;
      if ((paramFile1.isDirectory()) && (!paramFile2.isDirectory())) {
        i = -1;
      }
      for (;;)
      {
        return i;
        if ((!paramFile1.isDirectory()) && (paramFile2.isDirectory())) {
          i = 1;
        } else {
          i = paramFile1.getName().compareTo(paramFile2.getName());
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\folderselector\FileChooserDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */