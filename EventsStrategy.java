package io.fabric.sdk.android.services.events;

public abstract interface EventsStrategy<T>
  extends FileRollOverManager, EventsManager<T>
{
  public abstract FilesSender getFilesSender();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\events\EventsStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */