package com.testfairy.p;

import java.io.File;
import java.io.FilenameFilter;

public class j
  implements FilenameFilter
{
  private String a;
  
  public j(String paramString)
  {
    this.a = paramString;
  }
  
  public boolean accept(File paramFile, String paramString)
  {
    return paramString.startsWith(this.a);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */