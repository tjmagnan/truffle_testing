package com.testfairy.d;

import android.graphics.Rect;
import android.view.View;

public class a
{
  private final String a;
  private final Rect b;
  private final View c;
  
  public a(String paramString, View paramView, Rect paramRect)
  {
    this.b = paramRect;
    this.a = paramString;
    this.c = paramView;
  }
  
  public String a()
  {
    return this.a;
  }
  
  public Rect b()
  {
    return this.b;
  }
  
  public View c()
  {
    return this.c;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */