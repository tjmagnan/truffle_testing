package io.fabric.sdk.android.services.network;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import javax.security.auth.x500.X500Principal;

final class CertificateChainCleaner
{
  public static X509Certificate[] getCleanChain(X509Certificate[] paramArrayOfX509Certificate, SystemKeyStore paramSystemKeyStore)
    throws CertificateException
  {
    LinkedList localLinkedList = new LinkedList();
    int j = 0;
    if (paramSystemKeyStore.isTrustRoot(paramArrayOfX509Certificate[0])) {
      j = 1;
    }
    localLinkedList.add(paramArrayOfX509Certificate[0]);
    int i;
    for (int k = 1;; k++)
    {
      i = j;
      if (k >= paramArrayOfX509Certificate.length) {
        break;
      }
      if (paramSystemKeyStore.isTrustRoot(paramArrayOfX509Certificate[k])) {
        j = 1;
      }
      i = j;
      if (!isValidLink(paramArrayOfX509Certificate[k], paramArrayOfX509Certificate[(k - 1)])) {
        break;
      }
      localLinkedList.add(paramArrayOfX509Certificate[k]);
    }
    paramArrayOfX509Certificate = paramSystemKeyStore.getTrustRootFor(paramArrayOfX509Certificate[(k - 1)]);
    if (paramArrayOfX509Certificate != null)
    {
      localLinkedList.add(paramArrayOfX509Certificate);
      i = 1;
    }
    if (i != 0) {
      return (X509Certificate[])localLinkedList.toArray(new X509Certificate[localLinkedList.size()]);
    }
    throw new CertificateException("Didn't find a trust anchor in chain cleanup!");
  }
  
  private static boolean isValidLink(X509Certificate paramX509Certificate1, X509Certificate paramX509Certificate2)
  {
    boolean bool = false;
    if (!paramX509Certificate1.getSubjectX500Principal().equals(paramX509Certificate2.getIssuerX500Principal())) {}
    for (;;)
    {
      return bool;
      try
      {
        paramX509Certificate2.verify(paramX509Certificate1.getPublicKey());
        bool = true;
      }
      catch (GeneralSecurityException paramX509Certificate1) {}
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\network\CertificateChainCleaner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */