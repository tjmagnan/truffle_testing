package tech.dcube.companion.customListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.Locale;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import tech.dcube.companion.model.PBTrackingDetails;

public class OrderStatusLogListAdapter
  extends ArrayAdapter<PBTrackingDetails>
{
  private List<PBTrackingDetails> dataModel;
  private int lastPosition = -1;
  Context mContext;
  
  public OrderStatusLogListAdapter(List<PBTrackingDetails> paramList, Context paramContext)
  {
    super(paramContext, 2130968658, paramList);
    this.dataModel = paramList;
    this.mContext = paramContext;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject3 = (PBTrackingDetails)getItem(paramInt);
    Object localObject2;
    Object localObject1;
    if (paramView == null)
    {
      localObject2 = new ViewHolder(null);
      paramView = LayoutInflater.from(getContext()).inflate(2130968658, paramViewGroup, false);
      ((ViewHolder)localObject2).LogDate = ((TextView)paramView.findViewById(2131624263));
      ((ViewHolder)localObject2).LogStatus = ((TextView)paramView.findViewById(2131624264));
      ((ViewHolder)localObject2).LogLocation = ((TextView)paramView.findViewById(2131624265));
      localObject1 = paramView;
      paramView.setTag(localObject2);
      paramViewGroup = (ViewGroup)localObject2;
      localObject2 = this.mContext;
      if (paramInt <= this.lastPosition) {
        break label270;
      }
    }
    label270:
    for (int i = 2131034132;; i = 2131034129)
    {
      ((View)localObject1).startAnimation(AnimationUtils.loadAnimation((Context)localObject2, i));
      this.lastPosition = paramInt;
      localObject1 = ((PBTrackingDetails)localObject3).getEventDate();
      localObject1 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.000Z").withLocale(Locale.ENGLISH).parseLocalDate((String)localObject1);
      localObject2 = ((LocalDate)localObject1).getMonthOfYear() + "/" + ((LocalDate)localObject1).getDayOfMonth();
      localObject1 = ((PBTrackingDetails)localObject3).getPackageStatus();
      localObject3 = ((PBTrackingDetails)localObject3).getCity() + ", " + ((PBTrackingDetails)localObject3).getState();
      paramViewGroup.LogDate.setText((CharSequence)localObject2);
      paramViewGroup.LogStatus.setText((CharSequence)localObject1);
      paramViewGroup.LogLocation.setText((CharSequence)localObject3);
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      localObject1 = paramView;
      break;
    }
  }
  
  private class ViewHolder
  {
    TextView LogDate;
    TextView LogLocation;
    TextView LogStatus;
    
    private ViewHolder() {}
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\customListAdapters\OrderStatusLogListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */