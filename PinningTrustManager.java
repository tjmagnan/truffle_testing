package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

class PinningTrustManager
  implements X509TrustManager
{
  private static final X509Certificate[] NO_ISSUERS = new X509Certificate[0];
  private static final long PIN_FRESHNESS_DURATION_MILLIS = 15552000000L;
  private final Set<X509Certificate> cache = Collections.synchronizedSet(new HashSet());
  private final long pinCreationTimeMillis;
  private final List<byte[]> pins = new LinkedList();
  private final SystemKeyStore systemKeyStore;
  private final TrustManager[] systemTrustManagers = initializeSystemTrustManagers(paramSystemKeyStore);
  
  public PinningTrustManager(SystemKeyStore paramSystemKeyStore, PinningInfoProvider paramPinningInfoProvider)
  {
    this.systemKeyStore = paramSystemKeyStore;
    this.pinCreationTimeMillis = paramPinningInfoProvider.getPinCreationTimeInMillis();
    for (paramPinningInfoProvider : paramPinningInfoProvider.getPins()) {
      this.pins.add(hexStringToByteArray(paramPinningInfoProvider));
    }
  }
  
  private void checkPinTrust(X509Certificate[] paramArrayOfX509Certificate)
    throws CertificateException
  {
    if ((this.pinCreationTimeMillis != -1L) && (System.currentTimeMillis() - this.pinCreationTimeMillis > 15552000000L))
    {
      Fabric.getLogger().w("Fabric", "Certificate pins are stale, (" + (System.currentTimeMillis() - this.pinCreationTimeMillis) + " millis vs " + 15552000000L + " millis) falling back to system trust.");
      return;
    }
    paramArrayOfX509Certificate = CertificateChainCleaner.getCleanChain(paramArrayOfX509Certificate, this.systemKeyStore);
    int j = paramArrayOfX509Certificate.length;
    for (int i = 0;; i++)
    {
      if (i >= j) {
        break label114;
      }
      if (isValidPin(paramArrayOfX509Certificate[i])) {
        break;
      }
    }
    label114:
    throw new CertificateException("No valid pins found in chain!");
  }
  
  private void checkSystemTrust(X509Certificate[] paramArrayOfX509Certificate, String paramString)
    throws CertificateException
  {
    TrustManager[] arrayOfTrustManager = this.systemTrustManagers;
    int j = arrayOfTrustManager.length;
    for (int i = 0; i < j; i++) {
      ((X509TrustManager)arrayOfTrustManager[i]).checkServerTrusted(paramArrayOfX509Certificate, paramString);
    }
  }
  
  private byte[] hexStringToByteArray(String paramString)
  {
    int j = paramString.length();
    byte[] arrayOfByte = new byte[j / 2];
    for (int i = 0; i < j; i += 2) {
      arrayOfByte[(i / 2)] = ((byte)((Character.digit(paramString.charAt(i), 16) << 4) + Character.digit(paramString.charAt(i + 1), 16)));
    }
    return arrayOfByte;
  }
  
  private TrustManager[] initializeSystemTrustManagers(SystemKeyStore paramSystemKeyStore)
  {
    try
    {
      TrustManagerFactory localTrustManagerFactory = TrustManagerFactory.getInstance("X509");
      localTrustManagerFactory.init(paramSystemKeyStore.trustStore);
      paramSystemKeyStore = localTrustManagerFactory.getTrustManagers();
      return paramSystemKeyStore;
    }
    catch (NoSuchAlgorithmException paramSystemKeyStore)
    {
      throw new AssertionError(paramSystemKeyStore);
    }
    catch (KeyStoreException paramSystemKeyStore)
    {
      throw new AssertionError(paramSystemKeyStore);
    }
  }
  
  /* Error */
  private boolean isValidPin(X509Certificate paramX509Certificate)
    throws CertificateException
  {
    // Byte code:
    //   0: ldc -61
    //   2: invokestatic 200	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   5: aload_1
    //   6: invokevirtual 204	java/security/cert/X509Certificate:getPublicKey	()Ljava/security/PublicKey;
    //   9: invokeinterface 210 1 0
    //   14: invokevirtual 214	java/security/MessageDigest:digest	([B)[B
    //   17: astore_1
    //   18: aload_0
    //   19: getfield 39	io/fabric/sdk/android/services/network/PinningTrustManager:pins	Ljava/util/List;
    //   22: invokeinterface 218 1 0
    //   27: astore_3
    //   28: aload_3
    //   29: invokeinterface 224 1 0
    //   34: ifeq +25 -> 59
    //   37: aload_3
    //   38: invokeinterface 228 1 0
    //   43: checkcast 230	[B
    //   46: aload_1
    //   47: invokestatic 236	java/util/Arrays:equals	([B[B)Z
    //   50: istore_2
    //   51: iload_2
    //   52: ifeq -24 -> 28
    //   55: iconst_1
    //   56: istore_2
    //   57: iload_2
    //   58: ireturn
    //   59: iconst_0
    //   60: istore_2
    //   61: goto -4 -> 57
    //   64: astore_1
    //   65: new 84	java/security/cert/CertificateException
    //   68: dup
    //   69: aload_1
    //   70: invokespecial 239	java/security/cert/CertificateException:<init>	(Ljava/lang/Throwable;)V
    //   73: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	this	PinningTrustManager
    //   0	74	1	paramX509Certificate	X509Certificate
    //   50	11	2	bool	boolean
    //   27	11	3	localIterator	java.util.Iterator
    // Exception table:
    //   from	to	target	type
    //   0	28	64	java/security/NoSuchAlgorithmException
    //   28	51	64	java/security/NoSuchAlgorithmException
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
    throws CertificateException
  {
    throw new CertificateException("Client certificates not supported!");
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
    throws CertificateException
  {
    if (this.cache.contains(paramArrayOfX509Certificate[0])) {}
    for (;;)
    {
      return;
      checkSystemTrust(paramArrayOfX509Certificate, paramString);
      checkPinTrust(paramArrayOfX509Certificate);
      this.cache.add(paramArrayOfX509Certificate[0]);
    }
  }
  
  public X509Certificate[] getAcceptedIssuers()
  {
    return NO_ISSUERS;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\network\PinningTrustManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */