package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DefaultItemAnimator
  extends SimpleItemAnimator
{
  private static final boolean DEBUG = false;
  ArrayList<RecyclerView.ViewHolder> mAddAnimations = new ArrayList();
  ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList = new ArrayList();
  ArrayList<RecyclerView.ViewHolder> mChangeAnimations = new ArrayList();
  ArrayList<ArrayList<ChangeInfo>> mChangesList = new ArrayList();
  ArrayList<RecyclerView.ViewHolder> mMoveAnimations = new ArrayList();
  ArrayList<ArrayList<MoveInfo>> mMovesList = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> mPendingAdditions = new ArrayList();
  private ArrayList<ChangeInfo> mPendingChanges = new ArrayList();
  private ArrayList<MoveInfo> mPendingMoves = new ArrayList();
  private ArrayList<RecyclerView.ViewHolder> mPendingRemovals = new ArrayList();
  ArrayList<RecyclerView.ViewHolder> mRemoveAnimations = new ArrayList();
  
  private void animateRemoveImpl(final RecyclerView.ViewHolder paramViewHolder)
  {
    final ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = ViewCompat.animate(paramViewHolder.itemView);
    this.mRemoveAnimations.add(paramViewHolder);
    localViewPropertyAnimatorCompat.setDuration(getRemoveDuration()).alpha(0.0F).setListener(new VpaListenerAdapter()
    {
      public void onAnimationEnd(View paramAnonymousView)
      {
        localViewPropertyAnimatorCompat.setListener(null);
        ViewCompat.setAlpha(paramAnonymousView, 1.0F);
        DefaultItemAnimator.this.dispatchRemoveFinished(paramViewHolder);
        DefaultItemAnimator.this.mRemoveAnimations.remove(paramViewHolder);
        DefaultItemAnimator.this.dispatchFinishedWhenDone();
      }
      
      public void onAnimationStart(View paramAnonymousView)
      {
        DefaultItemAnimator.this.dispatchRemoveStarting(paramViewHolder);
      }
    }).start();
  }
  
  private void endChangeAnimation(List<ChangeInfo> paramList, RecyclerView.ViewHolder paramViewHolder)
  {
    for (int i = paramList.size() - 1; i >= 0; i--)
    {
      ChangeInfo localChangeInfo = (ChangeInfo)paramList.get(i);
      if ((endChangeAnimationIfNecessary(localChangeInfo, paramViewHolder)) && (localChangeInfo.oldHolder == null) && (localChangeInfo.newHolder == null)) {
        paramList.remove(localChangeInfo);
      }
    }
  }
  
  private void endChangeAnimationIfNecessary(ChangeInfo paramChangeInfo)
  {
    if (paramChangeInfo.oldHolder != null) {
      endChangeAnimationIfNecessary(paramChangeInfo, paramChangeInfo.oldHolder);
    }
    if (paramChangeInfo.newHolder != null) {
      endChangeAnimationIfNecessary(paramChangeInfo, paramChangeInfo.newHolder);
    }
  }
  
  private boolean endChangeAnimationIfNecessary(ChangeInfo paramChangeInfo, RecyclerView.ViewHolder paramViewHolder)
  {
    boolean bool = false;
    if (paramChangeInfo.newHolder == paramViewHolder)
    {
      paramChangeInfo.newHolder = null;
      ViewCompat.setAlpha(paramViewHolder.itemView, 1.0F);
      ViewCompat.setTranslationX(paramViewHolder.itemView, 0.0F);
      ViewCompat.setTranslationY(paramViewHolder.itemView, 0.0F);
      dispatchChangeFinished(paramViewHolder, bool);
    }
    for (bool = true;; bool = false)
    {
      return bool;
      if (paramChangeInfo.oldHolder == paramViewHolder)
      {
        paramChangeInfo.oldHolder = null;
        bool = true;
        break;
      }
    }
  }
  
  private void resetAnimation(RecyclerView.ViewHolder paramViewHolder)
  {
    AnimatorCompatHelper.clearInterpolator(paramViewHolder.itemView);
    endAnimation(paramViewHolder);
  }
  
  public boolean animateAdd(RecyclerView.ViewHolder paramViewHolder)
  {
    resetAnimation(paramViewHolder);
    ViewCompat.setAlpha(paramViewHolder.itemView, 0.0F);
    this.mPendingAdditions.add(paramViewHolder);
    return true;
  }
  
  void animateAddImpl(final RecyclerView.ViewHolder paramViewHolder)
  {
    final ViewPropertyAnimatorCompat localViewPropertyAnimatorCompat = ViewCompat.animate(paramViewHolder.itemView);
    this.mAddAnimations.add(paramViewHolder);
    localViewPropertyAnimatorCompat.alpha(1.0F).setDuration(getAddDuration()).setListener(new VpaListenerAdapter()
    {
      public void onAnimationCancel(View paramAnonymousView)
      {
        ViewCompat.setAlpha(paramAnonymousView, 1.0F);
      }
      
      public void onAnimationEnd(View paramAnonymousView)
      {
        localViewPropertyAnimatorCompat.setListener(null);
        DefaultItemAnimator.this.dispatchAddFinished(paramViewHolder);
        DefaultItemAnimator.this.mAddAnimations.remove(paramViewHolder);
        DefaultItemAnimator.this.dispatchFinishedWhenDone();
      }
      
      public void onAnimationStart(View paramAnonymousView)
      {
        DefaultItemAnimator.this.dispatchAddStarting(paramViewHolder);
      }
    }).start();
  }
  
  public boolean animateChange(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (paramViewHolder1 == paramViewHolder2) {}
    for (boolean bool = animateMove(paramViewHolder1, paramInt1, paramInt2, paramInt3, paramInt4);; bool = true)
    {
      return bool;
      float f3 = ViewCompat.getTranslationX(paramViewHolder1.itemView);
      float f2 = ViewCompat.getTranslationY(paramViewHolder1.itemView);
      float f1 = ViewCompat.getAlpha(paramViewHolder1.itemView);
      resetAnimation(paramViewHolder1);
      int j = (int)(paramInt3 - paramInt1 - f3);
      int i = (int)(paramInt4 - paramInt2 - f2);
      ViewCompat.setTranslationX(paramViewHolder1.itemView, f3);
      ViewCompat.setTranslationY(paramViewHolder1.itemView, f2);
      ViewCompat.setAlpha(paramViewHolder1.itemView, f1);
      if (paramViewHolder2 != null)
      {
        resetAnimation(paramViewHolder2);
        ViewCompat.setTranslationX(paramViewHolder2.itemView, -j);
        ViewCompat.setTranslationY(paramViewHolder2.itemView, -i);
        ViewCompat.setAlpha(paramViewHolder2.itemView, 0.0F);
      }
      this.mPendingChanges.add(new ChangeInfo(paramViewHolder1, paramViewHolder2, paramInt1, paramInt2, paramInt3, paramInt4));
    }
  }
  
  void animateChangeImpl(final ChangeInfo paramChangeInfo)
  {
    final Object localObject1 = paramChangeInfo.oldHolder;
    if (localObject1 == null)
    {
      localObject1 = null;
      localObject2 = paramChangeInfo.newHolder;
      if (localObject2 == null) {
        break label171;
      }
    }
    label171:
    for (final Object localObject2 = ((RecyclerView.ViewHolder)localObject2).itemView;; localObject2 = null)
    {
      if (localObject1 != null)
      {
        localObject1 = ViewCompat.animate((View)localObject1).setDuration(getChangeDuration());
        this.mChangeAnimations.add(paramChangeInfo.oldHolder);
        ((ViewPropertyAnimatorCompat)localObject1).translationX(paramChangeInfo.toX - paramChangeInfo.fromX);
        ((ViewPropertyAnimatorCompat)localObject1).translationY(paramChangeInfo.toY - paramChangeInfo.fromY);
        ((ViewPropertyAnimatorCompat)localObject1).alpha(0.0F).setListener(new VpaListenerAdapter()
        {
          public void onAnimationEnd(View paramAnonymousView)
          {
            localObject1.setListener(null);
            ViewCompat.setAlpha(paramAnonymousView, 1.0F);
            ViewCompat.setTranslationX(paramAnonymousView, 0.0F);
            ViewCompat.setTranslationY(paramAnonymousView, 0.0F);
            DefaultItemAnimator.this.dispatchChangeFinished(paramChangeInfo.oldHolder, true);
            DefaultItemAnimator.this.mChangeAnimations.remove(paramChangeInfo.oldHolder);
            DefaultItemAnimator.this.dispatchFinishedWhenDone();
          }
          
          public void onAnimationStart(View paramAnonymousView)
          {
            DefaultItemAnimator.this.dispatchChangeStarting(paramChangeInfo.oldHolder, true);
          }
        }).start();
      }
      if (localObject2 != null)
      {
        localObject1 = ViewCompat.animate((View)localObject2);
        this.mChangeAnimations.add(paramChangeInfo.newHolder);
        ((ViewPropertyAnimatorCompat)localObject1).translationX(0.0F).translationY(0.0F).setDuration(getChangeDuration()).alpha(1.0F).setListener(new VpaListenerAdapter()
        {
          public void onAnimationEnd(View paramAnonymousView)
          {
            localObject1.setListener(null);
            ViewCompat.setAlpha(localObject2, 1.0F);
            ViewCompat.setTranslationX(localObject2, 0.0F);
            ViewCompat.setTranslationY(localObject2, 0.0F);
            DefaultItemAnimator.this.dispatchChangeFinished(paramChangeInfo.newHolder, false);
            DefaultItemAnimator.this.mChangeAnimations.remove(paramChangeInfo.newHolder);
            DefaultItemAnimator.this.dispatchFinishedWhenDone();
          }
          
          public void onAnimationStart(View paramAnonymousView)
          {
            DefaultItemAnimator.this.dispatchChangeStarting(paramChangeInfo.newHolder, false);
          }
        }).start();
      }
      return;
      localObject1 = ((RecyclerView.ViewHolder)localObject1).itemView;
      break;
    }
  }
  
  public boolean animateMove(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    View localView = paramViewHolder.itemView;
    paramInt1 = (int)(paramInt1 + ViewCompat.getTranslationX(paramViewHolder.itemView));
    paramInt2 = (int)(paramInt2 + ViewCompat.getTranslationY(paramViewHolder.itemView));
    resetAnimation(paramViewHolder);
    int j = paramInt3 - paramInt1;
    int i = paramInt4 - paramInt2;
    if ((j == 0) && (i == 0)) {
      dispatchMoveFinished(paramViewHolder);
    }
    for (boolean bool = false;; bool = true)
    {
      return bool;
      if (j != 0) {
        ViewCompat.setTranslationX(localView, -j);
      }
      if (i != 0) {
        ViewCompat.setTranslationY(localView, -i);
      }
      this.mPendingMoves.add(new MoveInfo(paramViewHolder, paramInt1, paramInt2, paramInt3, paramInt4));
    }
  }
  
  void animateMoveImpl(final RecyclerView.ViewHolder paramViewHolder, final int paramInt1, final int paramInt2, int paramInt3, int paramInt4)
  {
    final Object localObject = paramViewHolder.itemView;
    paramInt1 = paramInt3 - paramInt1;
    paramInt2 = paramInt4 - paramInt2;
    if (paramInt1 != 0) {
      ViewCompat.animate((View)localObject).translationX(0.0F);
    }
    if (paramInt2 != 0) {
      ViewCompat.animate((View)localObject).translationY(0.0F);
    }
    localObject = ViewCompat.animate((View)localObject);
    this.mMoveAnimations.add(paramViewHolder);
    ((ViewPropertyAnimatorCompat)localObject).setDuration(getMoveDuration()).setListener(new VpaListenerAdapter()
    {
      public void onAnimationCancel(View paramAnonymousView)
      {
        if (paramInt1 != 0) {
          ViewCompat.setTranslationX(paramAnonymousView, 0.0F);
        }
        if (paramInt2 != 0) {
          ViewCompat.setTranslationY(paramAnonymousView, 0.0F);
        }
      }
      
      public void onAnimationEnd(View paramAnonymousView)
      {
        localObject.setListener(null);
        DefaultItemAnimator.this.dispatchMoveFinished(paramViewHolder);
        DefaultItemAnimator.this.mMoveAnimations.remove(paramViewHolder);
        DefaultItemAnimator.this.dispatchFinishedWhenDone();
      }
      
      public void onAnimationStart(View paramAnonymousView)
      {
        DefaultItemAnimator.this.dispatchMoveStarting(paramViewHolder);
      }
    }).start();
  }
  
  public boolean animateRemove(RecyclerView.ViewHolder paramViewHolder)
  {
    resetAnimation(paramViewHolder);
    this.mPendingRemovals.add(paramViewHolder);
    return true;
  }
  
  public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder paramViewHolder, @NonNull List<Object> paramList)
  {
    if ((!paramList.isEmpty()) || (super.canReuseUpdatedViewHolder(paramViewHolder, paramList))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  void cancelAll(List<RecyclerView.ViewHolder> paramList)
  {
    for (int i = paramList.size() - 1; i >= 0; i--) {
      ViewCompat.animate(((RecyclerView.ViewHolder)paramList.get(i)).itemView).cancel();
    }
  }
  
  void dispatchFinishedWhenDone()
  {
    if (!isRunning()) {
      dispatchAnimationsFinished();
    }
  }
  
  public void endAnimation(RecyclerView.ViewHolder paramViewHolder)
  {
    View localView = paramViewHolder.itemView;
    ViewCompat.animate(localView).cancel();
    for (int i = this.mPendingMoves.size() - 1; i >= 0; i--) {
      if (((MoveInfo)this.mPendingMoves.get(i)).holder == paramViewHolder)
      {
        ViewCompat.setTranslationY(localView, 0.0F);
        ViewCompat.setTranslationX(localView, 0.0F);
        dispatchMoveFinished(paramViewHolder);
        this.mPendingMoves.remove(i);
      }
    }
    endChangeAnimation(this.mPendingChanges, paramViewHolder);
    if (this.mPendingRemovals.remove(paramViewHolder))
    {
      ViewCompat.setAlpha(localView, 1.0F);
      dispatchRemoveFinished(paramViewHolder);
    }
    if (this.mPendingAdditions.remove(paramViewHolder))
    {
      ViewCompat.setAlpha(localView, 1.0F);
      dispatchAddFinished(paramViewHolder);
    }
    ArrayList localArrayList;
    for (i = this.mChangesList.size() - 1; i >= 0; i--)
    {
      localArrayList = (ArrayList)this.mChangesList.get(i);
      endChangeAnimation(localArrayList, paramViewHolder);
      if (localArrayList.isEmpty()) {
        this.mChangesList.remove(i);
      }
    }
    i = this.mMovesList.size() - 1;
    if (i >= 0)
    {
      localArrayList = (ArrayList)this.mMovesList.get(i);
      for (int j = localArrayList.size() - 1;; j--)
      {
        if (j >= 0)
        {
          if (((MoveInfo)localArrayList.get(j)).holder != paramViewHolder) {
            continue;
          }
          ViewCompat.setTranslationY(localView, 0.0F);
          ViewCompat.setTranslationX(localView, 0.0F);
          dispatchMoveFinished(paramViewHolder);
          localArrayList.remove(j);
          if (localArrayList.isEmpty()) {
            this.mMovesList.remove(i);
          }
        }
        i--;
        break;
      }
    }
    for (i = this.mAdditionsList.size() - 1; i >= 0; i--)
    {
      localArrayList = (ArrayList)this.mAdditionsList.get(i);
      if (localArrayList.remove(paramViewHolder))
      {
        ViewCompat.setAlpha(localView, 1.0F);
        dispatchAddFinished(paramViewHolder);
        if (localArrayList.isEmpty()) {
          this.mAdditionsList.remove(i);
        }
      }
    }
    if ((!this.mRemoveAnimations.remove(paramViewHolder)) || ((!this.mAddAnimations.remove(paramViewHolder)) || ((!this.mChangeAnimations.remove(paramViewHolder)) || (this.mMoveAnimations.remove(paramViewHolder))))) {}
    dispatchFinishedWhenDone();
  }
  
  public void endAnimations()
  {
    Object localObject1;
    Object localObject2;
    for (int i = this.mPendingMoves.size() - 1; i >= 0; i--)
    {
      localObject1 = (MoveInfo)this.mPendingMoves.get(i);
      localObject2 = ((MoveInfo)localObject1).holder.itemView;
      ViewCompat.setTranslationY((View)localObject2, 0.0F);
      ViewCompat.setTranslationX((View)localObject2, 0.0F);
      dispatchMoveFinished(((MoveInfo)localObject1).holder);
      this.mPendingMoves.remove(i);
    }
    for (i = this.mPendingRemovals.size() - 1; i >= 0; i--)
    {
      dispatchRemoveFinished((RecyclerView.ViewHolder)this.mPendingRemovals.get(i));
      this.mPendingRemovals.remove(i);
    }
    for (i = this.mPendingAdditions.size() - 1; i >= 0; i--)
    {
      localObject1 = (RecyclerView.ViewHolder)this.mPendingAdditions.get(i);
      ViewCompat.setAlpha(((RecyclerView.ViewHolder)localObject1).itemView, 1.0F);
      dispatchAddFinished((RecyclerView.ViewHolder)localObject1);
      this.mPendingAdditions.remove(i);
    }
    for (i = this.mPendingChanges.size() - 1; i >= 0; i--) {
      endChangeAnimationIfNecessary((ChangeInfo)this.mPendingChanges.get(i));
    }
    this.mPendingChanges.clear();
    if (!isRunning()) {}
    for (;;)
    {
      return;
      int j;
      for (i = this.mMovesList.size() - 1; i >= 0; i--)
      {
        ArrayList localArrayList = (ArrayList)this.mMovesList.get(i);
        for (j = localArrayList.size() - 1; j >= 0; j--)
        {
          localObject2 = (MoveInfo)localArrayList.get(j);
          localObject1 = ((MoveInfo)localObject2).holder.itemView;
          ViewCompat.setTranslationY((View)localObject1, 0.0F);
          ViewCompat.setTranslationX((View)localObject1, 0.0F);
          dispatchMoveFinished(((MoveInfo)localObject2).holder);
          localArrayList.remove(j);
          if (localArrayList.isEmpty()) {
            this.mMovesList.remove(localArrayList);
          }
        }
      }
      for (i = this.mAdditionsList.size() - 1; i >= 0; i--)
      {
        localObject2 = (ArrayList)this.mAdditionsList.get(i);
        for (j = ((ArrayList)localObject2).size() - 1; j >= 0; j--)
        {
          localObject1 = (RecyclerView.ViewHolder)((ArrayList)localObject2).get(j);
          ViewCompat.setAlpha(((RecyclerView.ViewHolder)localObject1).itemView, 1.0F);
          dispatchAddFinished((RecyclerView.ViewHolder)localObject1);
          ((ArrayList)localObject2).remove(j);
          if (((ArrayList)localObject2).isEmpty()) {
            this.mAdditionsList.remove(localObject2);
          }
        }
      }
      for (i = this.mChangesList.size() - 1; i >= 0; i--)
      {
        localObject1 = (ArrayList)this.mChangesList.get(i);
        for (j = ((ArrayList)localObject1).size() - 1; j >= 0; j--)
        {
          endChangeAnimationIfNecessary((ChangeInfo)((ArrayList)localObject1).get(j));
          if (((ArrayList)localObject1).isEmpty()) {
            this.mChangesList.remove(localObject1);
          }
        }
      }
      cancelAll(this.mRemoveAnimations);
      cancelAll(this.mMoveAnimations);
      cancelAll(this.mAddAnimations);
      cancelAll(this.mChangeAnimations);
      dispatchAnimationsFinished();
    }
  }
  
  public boolean isRunning()
  {
    if ((!this.mPendingAdditions.isEmpty()) || (!this.mPendingChanges.isEmpty()) || (!this.mPendingMoves.isEmpty()) || (!this.mPendingRemovals.isEmpty()) || (!this.mMoveAnimations.isEmpty()) || (!this.mRemoveAnimations.isEmpty()) || (!this.mAddAnimations.isEmpty()) || (!this.mChangeAnimations.isEmpty()) || (!this.mMovesList.isEmpty()) || (!this.mAdditionsList.isEmpty()) || (!this.mChangesList.isEmpty())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void runPendingAnimations()
  {
    int i;
    int j;
    label24:
    int k;
    label36:
    int m;
    if (!this.mPendingRemovals.isEmpty())
    {
      i = 1;
      if (this.mPendingMoves.isEmpty()) {
        break label72;
      }
      j = 1;
      if (this.mPendingChanges.isEmpty()) {
        break label77;
      }
      k = 1;
      if (this.mPendingAdditions.isEmpty()) {
        break label82;
      }
      m = 1;
      label49:
      if ((i != 0) || (j != 0) || (m != 0) || (k != 0)) {
        break label88;
      }
    }
    for (;;)
    {
      return;
      i = 0;
      break;
      label72:
      j = 0;
      break label24;
      label77:
      k = 0;
      break label36;
      label82:
      m = 0;
      break label49;
      label88:
      final Object localObject1 = this.mPendingRemovals.iterator();
      while (((Iterator)localObject1).hasNext()) {
        animateRemoveImpl((RecyclerView.ViewHolder)((Iterator)localObject1).next());
      }
      this.mPendingRemovals.clear();
      Object localObject2;
      label211:
      label291:
      long l1;
      label366:
      long l2;
      if (j != 0)
      {
        localObject1 = new ArrayList();
        ((ArrayList)localObject1).addAll(this.mPendingMoves);
        this.mMovesList.add(localObject1);
        this.mPendingMoves.clear();
        localObject2 = new Runnable()
        {
          public void run()
          {
            Iterator localIterator = localObject1.iterator();
            while (localIterator.hasNext())
            {
              DefaultItemAnimator.MoveInfo localMoveInfo = (DefaultItemAnimator.MoveInfo)localIterator.next();
              DefaultItemAnimator.this.animateMoveImpl(localMoveInfo.holder, localMoveInfo.fromX, localMoveInfo.fromY, localMoveInfo.toX, localMoveInfo.toY);
            }
            localObject1.clear();
            DefaultItemAnimator.this.mMovesList.remove(localObject1);
          }
        };
        if (i != 0) {
          ViewCompat.postOnAnimationDelayed(((MoveInfo)((ArrayList)localObject1).get(0)).holder.itemView, (Runnable)localObject2, getRemoveDuration());
        }
      }
      else
      {
        if (k != 0)
        {
          localObject1 = new ArrayList();
          ((ArrayList)localObject1).addAll(this.mPendingChanges);
          this.mChangesList.add(localObject1);
          this.mPendingChanges.clear();
          localObject2 = new Runnable()
          {
            public void run()
            {
              Iterator localIterator = localObject1.iterator();
              while (localIterator.hasNext())
              {
                DefaultItemAnimator.ChangeInfo localChangeInfo = (DefaultItemAnimator.ChangeInfo)localIterator.next();
                DefaultItemAnimator.this.animateChangeImpl(localChangeInfo);
              }
              localObject1.clear();
              DefaultItemAnimator.this.mChangesList.remove(localObject1);
            }
          };
          if (i == 0) {
            break label430;
          }
          ViewCompat.postOnAnimationDelayed(((ChangeInfo)((ArrayList)localObject1).get(0)).oldHolder.itemView, (Runnable)localObject2, getRemoveDuration());
        }
        if (m == 0) {
          break label438;
        }
        localObject1 = new ArrayList();
        ((ArrayList)localObject1).addAll(this.mPendingAdditions);
        this.mAdditionsList.add(localObject1);
        this.mPendingAdditions.clear();
        localObject2 = new Runnable()
        {
          public void run()
          {
            Iterator localIterator = localObject1.iterator();
            while (localIterator.hasNext())
            {
              RecyclerView.ViewHolder localViewHolder = (RecyclerView.ViewHolder)localIterator.next();
              DefaultItemAnimator.this.animateAddImpl(localViewHolder);
            }
            localObject1.clear();
            DefaultItemAnimator.this.mAdditionsList.remove(localObject1);
          }
        };
        if ((i == 0) && (j == 0) && (k == 0)) {
          break label458;
        }
        if (i == 0) {
          break label440;
        }
        l1 = getRemoveDuration();
        if (j == 0) {
          break label446;
        }
        l2 = getMoveDuration();
        label376:
        if (k == 0) {
          break label452;
        }
      }
      label430:
      label438:
      label440:
      label446:
      label452:
      for (long l3 = getChangeDuration();; l3 = 0L)
      {
        l2 = Math.max(l2, l3);
        ViewCompat.postOnAnimationDelayed(((RecyclerView.ViewHolder)((ArrayList)localObject1).get(0)).itemView, (Runnable)localObject2, l1 + l2);
        break;
        ((Runnable)localObject2).run();
        break label211;
        ((Runnable)localObject2).run();
        break label291;
        break;
        l1 = 0L;
        break label366;
        l2 = 0L;
        break label376;
      }
      label458:
      ((Runnable)localObject2).run();
    }
  }
  
  private static class ChangeInfo
  {
    public int fromX;
    public int fromY;
    public RecyclerView.ViewHolder newHolder;
    public RecyclerView.ViewHolder oldHolder;
    public int toX;
    public int toY;
    
    private ChangeInfo(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2)
    {
      this.oldHolder = paramViewHolder1;
      this.newHolder = paramViewHolder2;
    }
    
    ChangeInfo(RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this(paramViewHolder1, paramViewHolder2);
      this.fromX = paramInt1;
      this.fromY = paramInt2;
      this.toX = paramInt3;
      this.toY = paramInt4;
    }
    
    public String toString()
    {
      return "ChangeInfo{oldHolder=" + this.oldHolder + ", newHolder=" + this.newHolder + ", fromX=" + this.fromX + ", fromY=" + this.fromY + ", toX=" + this.toX + ", toY=" + this.toY + '}';
    }
  }
  
  private static class MoveInfo
  {
    public int fromX;
    public int fromY;
    public RecyclerView.ViewHolder holder;
    public int toX;
    public int toY;
    
    MoveInfo(RecyclerView.ViewHolder paramViewHolder, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      this.holder = paramViewHolder;
      this.fromX = paramInt1;
      this.fromY = paramInt2;
      this.toX = paramInt3;
      this.toY = paramInt4;
    }
  }
  
  private static class VpaListenerAdapter
    implements ViewPropertyAnimatorListener
  {
    public void onAnimationCancel(View paramView) {}
    
    public void onAnimationEnd(View paramView) {}
    
    public void onAnimationStart(View paramView) {}
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\DefaultItemAnimator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */