package com.testfairy.f;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.j;
import com.testfairy.k.d;
import com.testfairy.n;
import com.testfairy.p.l;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

public class a
  extends Thread
{
  private final Thread a;
  private final Throwable b;
  private final JSONObject c;
  private final d d;
  private final j e;
  
  public a(Thread paramThread, Throwable paramThrowable, j paramj, JSONObject paramJSONObject)
  {
    this(paramThread, paramThrowable, paramj, paramJSONObject, null);
  }
  
  public a(Thread paramThread, Throwable paramThrowable, j paramj, JSONObject paramJSONObject, d paramd)
  {
    this.a = paramThread;
    this.b = paramThrowable;
    this.e = paramj;
    this.c = paramJSONObject;
    this.d = paramd;
  }
  
  public void run()
  {
    for (;;)
    {
      try
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject1 = "https://app.testfairy.com/services/?method=" + n.o;
        if (this.d == null)
        {
          if (this.e != null)
          {
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            localObject1 = this.e.f() + "?method=" + n.o;
          }
          Object localObject2 = e.a;
          Object localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          Log.d((String)localObject2, "Sending crash to " + (String)localObject1);
          localObject2 = new org/apache/http/impl/client/DefaultHttpClient;
          ((DefaultHttpClient)localObject2).<init>();
          localObject3 = new org/apache/http/client/methods/HttpPost;
          ((HttpPost)localObject3).<init>((String)localObject1);
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>(16);
          if (this.e != null)
          {
            localObject4 = new org/apache/http/message/BasicNameValuePair;
            ((BasicNameValuePair)localObject4).<init>("projectId", String.valueOf(this.e.b()));
            ((List)localObject1).add(localObject4);
            localObject4 = new org/apache/http/message/BasicNameValuePair;
            ((BasicNameValuePair)localObject4).<init>("buildId", String.valueOf(this.e.c()));
            ((List)localObject1).add(localObject4);
            localObject4 = new org/apache/http/message/BasicNameValuePair;
            ((BasicNameValuePair)localObject4).<init>("testerId", String.valueOf(this.e.d()));
            ((List)localObject1).add(localObject4);
          }
          if ((this.c != null) && (this.c.length() > 0))
          {
            localObject4 = new org/apache/http/message/BasicNameValuePair;
            ((BasicNameValuePair)localObject4).<init>("deviceData", this.c.toString());
            ((List)localObject1).add(localObject4);
          }
          long l = System.currentTimeMillis() / 1000L;
          Object localObject4 = new org/apache/http/message/BasicNameValuePair;
          ((BasicNameValuePair)localObject4).<init>("timestamp", String.valueOf(l));
          ((List)localObject1).add(localObject4);
          localObject4 = new org/apache/http/message/BasicNameValuePair;
          ((BasicNameValuePair)localObject4).<init>(n.ab, "20170625-78d2bc1");
          ((List)localObject1).add(localObject4);
          localObject4 = new org/apache/http/message/BasicNameValuePair;
          ((BasicNameValuePair)localObject4).<init>("message", this.b.getMessage());
          ((List)localObject1).add(localObject4);
          localObject4 = new org/apache/http/message/BasicNameValuePair;
          ((BasicNameValuePair)localObject4).<init>("stackTrace", l.a(this.b));
          ((List)localObject1).add(localObject4);
          localObject4 = new org/apache/http/client/entity/UrlEncodedFormEntity;
          ((UrlEncodedFormEntity)localObject4).<init>((List)localObject1);
          ((HttpPost)localObject3).setEntity((HttpEntity)localObject4);
          ((HttpClient)localObject2).execute((HttpUriRequest)localObject3);
          return;
        }
      }
      catch (Exception localException)
      {
        Object localObject1;
        Log.v(e.a, "Exception within a crash, how unfortunate", localException);
        continue;
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = this.d.f + "?method=" + n.o;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */