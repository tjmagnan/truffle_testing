package android.support.transition;

import android.animation.TypeEvaluator;
import android.annotation.TargetApi;
import android.graphics.Rect;
import android.support.annotation.RequiresApi;

@TargetApi(14)
@RequiresApi(14)
class RectEvaluator
  implements TypeEvaluator<Rect>
{
  private Rect mRect;
  
  public RectEvaluator() {}
  
  public RectEvaluator(Rect paramRect)
  {
    this.mRect = paramRect;
  }
  
  public Rect evaluate(float paramFloat, Rect paramRect1, Rect paramRect2)
  {
    int k = paramRect1.left + (int)((paramRect2.left - paramRect1.left) * paramFloat);
    int i = paramRect1.top + (int)((paramRect2.top - paramRect1.top) * paramFloat);
    int m = paramRect1.right + (int)((paramRect2.right - paramRect1.right) * paramFloat);
    int j = paramRect1.bottom + (int)((paramRect2.bottom - paramRect1.bottom) * paramFloat);
    if (this.mRect == null) {}
    for (paramRect1 = new Rect(k, i, m, j);; paramRect1 = this.mRect)
    {
      return paramRect1;
      this.mRect.set(k, i, m, j);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\transition\RectEvaluator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */