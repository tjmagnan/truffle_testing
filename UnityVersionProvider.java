package com.crashlytics.android.core;

abstract interface UnityVersionProvider
{
  public abstract String getUnityVersion();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\UnityVersionProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */