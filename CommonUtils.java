package io.fabric.sdk.android.services.common;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import javax.crypto.Cipher;

public class CommonUtils
{
  static final int BYTES_IN_A_GIGABYTE = 1073741824;
  static final int BYTES_IN_A_KILOBYTE = 1024;
  static final int BYTES_IN_A_MEGABYTE = 1048576;
  private static final String CLS_SHARED_PREFERENCES_NAME = "com.crashlytics.prefs";
  static final boolean CLS_TRACE_DEFAULT = false;
  static final String CLS_TRACE_PREFERENCE_NAME = "com.crashlytics.Trace";
  static final String CRASHLYTICS_BUILD_ID = "com.crashlytics.android.build_id";
  public static final int DEVICE_STATE_BETAOS = 8;
  public static final int DEVICE_STATE_COMPROMISEDLIBRARIES = 32;
  public static final int DEVICE_STATE_DEBUGGERATTACHED = 4;
  public static final int DEVICE_STATE_ISSIMULATOR = 1;
  public static final int DEVICE_STATE_JAILBROKEN = 2;
  public static final int DEVICE_STATE_VENDORINTERNAL = 16;
  static final String FABRIC_BUILD_ID = "io.fabric.android.build_id";
  public static final Comparator<File> FILE_MODIFIED_COMPARATOR = new Comparator()
  {
    public int compare(File paramAnonymousFile1, File paramAnonymousFile2)
    {
      return (int)(paramAnonymousFile1.lastModified() - paramAnonymousFile2.lastModified());
    }
  };
  public static final String GOOGLE_SDK = "google_sdk";
  private static final char[] HEX_VALUES;
  private static final String LOG_PRIORITY_NAME_ASSERT = "A";
  private static final String LOG_PRIORITY_NAME_DEBUG = "D";
  private static final String LOG_PRIORITY_NAME_ERROR = "E";
  private static final String LOG_PRIORITY_NAME_INFO = "I";
  private static final String LOG_PRIORITY_NAME_UNKNOWN = "?";
  private static final String LOG_PRIORITY_NAME_VERBOSE = "V";
  private static final String LOG_PRIORITY_NAME_WARN = "W";
  public static final String MD5_INSTANCE = "MD5";
  public static final String SDK = "sdk";
  public static final String SHA1_INSTANCE = "SHA-1";
  private static final long UNCALCULATED_TOTAL_RAM = -1L;
  private static Boolean clsTrace = null;
  private static long totalRamInBytes;
  
  static
  {
    HEX_VALUES = new char[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
    totalRamInBytes = -1L;
  }
  
  public static long calculateFreeRamInBytes(Context paramContext)
  {
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    ((ActivityManager)paramContext.getSystemService("activity")).getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.availMem;
  }
  
  public static long calculateUsedDiskSpaceInBytes(String paramString)
  {
    paramString = new StatFs(paramString);
    long l = paramString.getBlockSize();
    return l * paramString.getBlockCount() - l * paramString.getAvailableBlocks();
  }
  
  public static boolean canTryConnection(Context paramContext)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (checkPermission(paramContext, "android.permission.ACCESS_NETWORK_STATE"))
    {
      paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if ((paramContext == null) || (!paramContext.isConnectedOrConnecting())) {
        break label41;
      }
    }
    label41:
    for (bool1 = bool2;; bool1 = false) {
      return bool1;
    }
  }
  
  public static boolean checkPermission(Context paramContext, String paramString)
  {
    if (paramContext.checkCallingOrSelfPermission(paramString) == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static void closeOrLog(Closeable paramCloseable, String paramString)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException paramCloseable)
    {
      for (;;)
      {
        Fabric.getLogger().e("Fabric", paramString, paramCloseable);
      }
    }
  }
  
  public static void closeQuietly(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (RuntimeException paramCloseable)
    {
      throw paramCloseable;
    }
    catch (Exception paramCloseable)
    {
      for (;;) {}
    }
  }
  
  static long convertMemInfoToBytes(String paramString1, String paramString2, int paramInt)
  {
    return Long.parseLong(paramString1.split(paramString2)[0].trim()) * paramInt;
  }
  
  public static void copyStream(InputStream paramInputStream, OutputStream paramOutputStream, byte[] paramArrayOfByte)
    throws IOException
  {
    for (;;)
    {
      int i = paramInputStream.read(paramArrayOfByte);
      if (i == -1) {
        break;
      }
      paramOutputStream.write(paramArrayOfByte, 0, i);
    }
  }
  
  @Deprecated
  public static Cipher createCipher(int paramInt, String paramString)
    throws InvalidKeyException
  {
    throw new InvalidKeyException("This method is deprecated");
  }
  
  public static String createInstanceIdFrom(String... paramVarArgs)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramVarArgs != null)
    {
      if (paramVarArgs.length != 0) {
        break label20;
      }
      localObject1 = localObject2;
    }
    for (;;)
    {
      return (String)localObject1;
      label20:
      localObject1 = new ArrayList();
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++)
      {
        String str = paramVarArgs[i];
        if (str != null) {
          ((List)localObject1).add(str.replace("-", "").toLowerCase(Locale.US));
        }
      }
      Collections.sort((List)localObject1);
      paramVarArgs = new StringBuilder();
      localObject1 = ((List)localObject1).iterator();
      while (((Iterator)localObject1).hasNext()) {
        paramVarArgs.append((String)((Iterator)localObject1).next());
      }
      paramVarArgs = paramVarArgs.toString();
      localObject1 = localObject2;
      if (paramVarArgs.length() > 0) {
        localObject1 = sha1(paramVarArgs);
      }
    }
  }
  
  public static byte[] dehexify(String paramString)
  {
    int j = paramString.length();
    byte[] arrayOfByte = new byte[j / 2];
    for (int i = 0; i < j; i += 2) {
      arrayOfByte[(i / 2)] = ((byte)((Character.digit(paramString.charAt(i), 16) << 4) + Character.digit(paramString.charAt(i + 1), 16)));
    }
    return arrayOfByte;
  }
  
  /* Error */
  public static String extractFieldFromSystemFile(File paramFile, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: aload 4
    //   8: astore_2
    //   9: aload_0
    //   10: invokevirtual 338	java/io/File:exists	()Z
    //   13: ifeq +99 -> 112
    //   16: aconst_null
    //   17: astore 7
    //   19: aconst_null
    //   20: astore 6
    //   22: aload 7
    //   24: astore_2
    //   25: new 340	java/io/BufferedReader
    //   28: astore_3
    //   29: aload 7
    //   31: astore_2
    //   32: new 342	java/io/FileReader
    //   35: astore 8
    //   37: aload 7
    //   39: astore_2
    //   40: aload 8
    //   42: aload_0
    //   43: invokespecial 345	java/io/FileReader:<init>	(Ljava/io/File;)V
    //   46: aload 7
    //   48: astore_2
    //   49: aload_3
    //   50: aload 8
    //   52: sipush 1024
    //   55: invokespecial 348	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   58: aload_3
    //   59: invokevirtual 351	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   62: astore 6
    //   64: aload 5
    //   66: astore_2
    //   67: aload 6
    //   69: ifnull +36 -> 105
    //   72: ldc_w 353
    //   75: invokestatic 359	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   78: aload 6
    //   80: iconst_2
    //   81: invokevirtual 362	java/util/regex/Pattern:split	(Ljava/lang/CharSequence;I)[Ljava/lang/String;
    //   84: astore_2
    //   85: aload_2
    //   86: arraylength
    //   87: iconst_1
    //   88: if_icmple -30 -> 58
    //   91: aload_2
    //   92: iconst_0
    //   93: aaload
    //   94: aload_1
    //   95: invokevirtual 365	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   98: ifeq -40 -> 58
    //   101: aload_2
    //   102: iconst_1
    //   103: aaload
    //   104: astore_2
    //   105: aload_3
    //   106: ldc_w 367
    //   109: invokestatic 369	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   112: aload_2
    //   113: areturn
    //   114: astore_3
    //   115: aload 6
    //   117: astore_1
    //   118: aload_1
    //   119: astore_2
    //   120: invokestatic 199	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   123: astore 5
    //   125: aload_1
    //   126: astore_2
    //   127: new 291	java/lang/StringBuilder
    //   130: astore 6
    //   132: aload_1
    //   133: astore_2
    //   134: aload 6
    //   136: invokespecial 292	java/lang/StringBuilder:<init>	()V
    //   139: aload_1
    //   140: astore_2
    //   141: aload 5
    //   143: ldc -55
    //   145: aload 6
    //   147: ldc_w 371
    //   150: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: aload_0
    //   154: invokevirtual 374	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   157: invokevirtual 312	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   160: aload_3
    //   161: invokeinterface 207 4 0
    //   166: aload_1
    //   167: ldc_w 367
    //   170: invokestatic 369	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   173: aload 4
    //   175: astore_2
    //   176: goto -64 -> 112
    //   179: astore_0
    //   180: aload_2
    //   181: ldc_w 367
    //   184: invokestatic 369	io/fabric/sdk/android/services/common/CommonUtils:closeOrLog	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   187: aload_0
    //   188: athrow
    //   189: astore_0
    //   190: aload_3
    //   191: astore_2
    //   192: goto -12 -> 180
    //   195: astore_2
    //   196: aload_3
    //   197: astore_1
    //   198: aload_2
    //   199: astore_3
    //   200: goto -82 -> 118
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	203	0	paramFile	File
    //   0	203	1	paramString	String
    //   8	184	2	localObject1	Object
    //   195	4	2	localException1	Exception
    //   28	78	3	localBufferedReader	java.io.BufferedReader
    //   114	83	3	localException2	Exception
    //   199	1	3	localException3	Exception
    //   1	173	4	localObject2	Object
    //   4	138	5	localLogger	Logger
    //   20	126	6	localObject3	Object
    //   17	30	7	localObject4	Object
    //   35	16	8	localFileReader	java.io.FileReader
    // Exception table:
    //   from	to	target	type
    //   25	29	114	java/lang/Exception
    //   32	37	114	java/lang/Exception
    //   40	46	114	java/lang/Exception
    //   49	58	114	java/lang/Exception
    //   25	29	179	finally
    //   32	37	179	finally
    //   40	46	179	finally
    //   49	58	179	finally
    //   120	125	179	finally
    //   127	132	179	finally
    //   134	139	179	finally
    //   141	166	179	finally
    //   58	64	189	finally
    //   72	101	189	finally
    //   58	64	195	java/lang/Exception
    //   72	101	195	java/lang/Exception
  }
  
  @TargetApi(16)
  public static void finishAffinity(Activity paramActivity, int paramInt)
  {
    if (paramActivity == null) {}
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 16)
      {
        paramActivity.finishAffinity();
      }
      else
      {
        paramActivity.setResult(paramInt);
        paramActivity.finish();
      }
    }
  }
  
  @TargetApi(16)
  public static void finishAffinity(Context paramContext, int paramInt)
  {
    if ((paramContext instanceof Activity)) {
      finishAffinity((Activity)paramContext, paramInt);
    }
  }
  
  public static void flushOrLog(Flushable paramFlushable, String paramString)
  {
    if (paramFlushable != null) {}
    try
    {
      paramFlushable.flush();
      return;
    }
    catch (IOException paramFlushable)
    {
      for (;;)
      {
        Fabric.getLogger().e("Fabric", paramString, paramFlushable);
      }
    }
  }
  
  public static String getAppIconHashOrNull(Context paramContext)
  {
    localObject1 = null;
    localObject2 = null;
    try
    {
      InputStream localInputStream = paramContext.getResources().openRawResource(getAppIconResourceId(paramContext));
      localObject2 = localInputStream;
      localObject1 = localInputStream;
      paramContext = sha1(localInputStream);
      localObject2 = localInputStream;
      localObject1 = localInputStream;
      boolean bool = isNullOrEmpty(paramContext);
      if (bool) {
        paramContext = null;
      }
      closeOrLog(localInputStream, "Failed to close icon input stream.");
    }
    catch (Exception paramContext)
    {
      for (;;)
      {
        localObject1 = localObject2;
        Fabric.getLogger().e("Fabric", "Could not calculate hash for app icon.", paramContext);
        closeOrLog((Closeable)localObject2, "Failed to close icon input stream.");
        paramContext = null;
      }
    }
    finally
    {
      closeOrLog((Closeable)localObject1, "Failed to close icon input stream.");
    }
    return paramContext;
  }
  
  public static int getAppIconResourceId(Context paramContext)
  {
    return paramContext.getApplicationContext().getApplicationInfo().icon;
  }
  
  public static ActivityManager.RunningAppProcessInfo getAppProcessInfo(String paramString, Context paramContext)
  {
    Object localObject2 = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    Object localObject1 = null;
    paramContext = (Context)localObject1;
    if (localObject2 != null)
    {
      localObject2 = ((List)localObject2).iterator();
      do
      {
        paramContext = (Context)localObject1;
        if (!((Iterator)localObject2).hasNext()) {
          break;
        }
        paramContext = (ActivityManager.RunningAppProcessInfo)((Iterator)localObject2).next();
      } while (!paramContext.processName.equals(paramString));
    }
    return paramContext;
  }
  
  public static Float getBatteryLevel(Context paramContext)
  {
    Object localObject = null;
    paramContext = paramContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    if (paramContext == null) {}
    int j;
    int i;
    for (paramContext = (Context)localObject;; paramContext = Float.valueOf(j / i))
    {
      return paramContext;
      j = paramContext.getIntExtra("level", -1);
      i = paramContext.getIntExtra("scale", -1);
    }
  }
  
  public static int getBatteryVelocity(Context paramContext, boolean paramBoolean)
  {
    paramContext = getBatteryLevel(paramContext);
    int i;
    if ((!paramBoolean) || (paramContext == null)) {
      i = 1;
    }
    for (;;)
    {
      return i;
      if (paramContext.floatValue() >= 99.0D) {
        i = 3;
      } else if (paramContext.floatValue() < 99.0D) {
        i = 2;
      } else {
        i = 0;
      }
    }
  }
  
  public static boolean getBooleanResourceValue(Context paramContext, String paramString, boolean paramBoolean)
  {
    boolean bool = paramBoolean;
    int i;
    if (paramContext != null)
    {
      Resources localResources = paramContext.getResources();
      bool = paramBoolean;
      if (localResources != null)
      {
        i = getResourcesIdentifier(paramContext, paramString, "bool");
        if (i <= 0) {
          break label45;
        }
        bool = localResources.getBoolean(i);
      }
    }
    for (;;)
    {
      return bool;
      label45:
      i = getResourcesIdentifier(paramContext, paramString, "string");
      bool = paramBoolean;
      if (i > 0) {
        bool = Boolean.parseBoolean(paramContext.getString(i));
      }
    }
  }
  
  public static int getCpuArchitectureInt()
  {
    return Architecture.getValue().ordinal();
  }
  
  public static int getDeviceState(Context paramContext)
  {
    int j = 0;
    if (isEmulator(paramContext)) {
      j = 0x0 | 0x1;
    }
    int i = j;
    if (isRooted(paramContext)) {
      i = j | 0x2;
    }
    j = i;
    if (isDebuggerAttached()) {
      j = i | 0x4;
    }
    return j;
  }
  
  public static boolean getProximitySensorEnabled(Context paramContext)
  {
    boolean bool = false;
    if (isEmulator(paramContext)) {}
    for (;;)
    {
      return bool;
      if (((SensorManager)paramContext.getSystemService("sensor")).getDefaultSensor(8) != null) {
        bool = true;
      }
    }
  }
  
  public static String getResourcePackageName(Context paramContext)
  {
    int i = paramContext.getApplicationContext().getApplicationInfo().icon;
    if (i > 0) {}
    for (paramContext = paramContext.getResources().getResourcePackageName(i);; paramContext = paramContext.getPackageName()) {
      return paramContext;
    }
  }
  
  public static int getResourcesIdentifier(Context paramContext, String paramString1, String paramString2)
  {
    return paramContext.getResources().getIdentifier(paramString1, paramString2, getResourcePackageName(paramContext));
  }
  
  public static SharedPreferences getSharedPrefs(Context paramContext)
  {
    return paramContext.getSharedPreferences("com.crashlytics.prefs", 0);
  }
  
  public static String getStringsFileValue(Context paramContext, String paramString)
  {
    int i = getResourcesIdentifier(paramContext, paramString, "string");
    if (i > 0) {}
    for (paramContext = paramContext.getString(i);; paramContext = "") {
      return paramContext;
    }
  }
  
  /* Error */
  public static long getTotalRamInBytes()
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 112	io/fabric/sdk/android/services/common/CommonUtils:totalRamInBytes	J
    //   6: ldc2_w 84
    //   9: lcmp
    //   10: ifne +75 -> 85
    //   13: lconst_0
    //   14: lstore_2
    //   15: new 335	java/io/File
    //   18: astore 4
    //   20: aload 4
    //   22: ldc_w 569
    //   25: invokespecial 570	java/io/File:<init>	(Ljava/lang/String;)V
    //   28: aload 4
    //   30: ldc_w 572
    //   33: invokestatic 574	io/fabric/sdk/android/services/common/CommonUtils:extractFieldFromSystemFile	(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    //   36: astore 4
    //   38: lload_2
    //   39: lstore_0
    //   40: aload 4
    //   42: invokestatic 580	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   45: ifne +36 -> 81
    //   48: aload 4
    //   50: getstatic 273	java/util/Locale:US	Ljava/util/Locale;
    //   53: invokevirtual 583	java/lang/String:toUpperCase	(Ljava/util/Locale;)Ljava/lang/String;
    //   56: astore 4
    //   58: aload 4
    //   60: ldc_w 585
    //   63: invokevirtual 588	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   66: ifeq +28 -> 94
    //   69: aload 4
    //   71: ldc_w 585
    //   74: sipush 1024
    //   77: invokestatic 590	io/fabric/sdk/android/services/common/CommonUtils:convertMemInfoToBytes	(Ljava/lang/String;Ljava/lang/String;I)J
    //   80: lstore_0
    //   81: lload_0
    //   82: putstatic 112	io/fabric/sdk/android/services/common/CommonUtils:totalRamInBytes	J
    //   85: getstatic 112	io/fabric/sdk/android/services/common/CommonUtils:totalRamInBytes	J
    //   88: lstore_0
    //   89: ldc 2
    //   91: monitorexit
    //   92: lload_0
    //   93: lreturn
    //   94: aload 4
    //   96: ldc_w 592
    //   99: invokevirtual 588	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   102: ifeq +17 -> 119
    //   105: aload 4
    //   107: ldc_w 592
    //   110: ldc 16
    //   112: invokestatic 590	io/fabric/sdk/android/services/common/CommonUtils:convertMemInfoToBytes	(Ljava/lang/String;Ljava/lang/String;I)J
    //   115: lstore_0
    //   116: goto -35 -> 81
    //   119: aload 4
    //   121: ldc_w 594
    //   124: invokevirtual 588	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   127: ifeq +17 -> 144
    //   130: aload 4
    //   132: ldc_w 594
    //   135: ldc 12
    //   137: invokestatic 590	io/fabric/sdk/android/services/common/CommonUtils:convertMemInfoToBytes	(Ljava/lang/String;Ljava/lang/String;I)J
    //   140: lstore_0
    //   141: goto -60 -> 81
    //   144: invokestatic 199	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   147: astore 6
    //   149: new 291	java/lang/StringBuilder
    //   152: astore 5
    //   154: aload 5
    //   156: invokespecial 292	java/lang/StringBuilder:<init>	()V
    //   159: aload 6
    //   161: ldc -55
    //   163: aload 5
    //   165: ldc_w 596
    //   168: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: aload 4
    //   173: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   176: invokevirtual 312	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   179: invokeinterface 600 3 0
    //   184: lload_2
    //   185: lstore_0
    //   186: goto -105 -> 81
    //   189: astore 6
    //   191: invokestatic 199	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   194: astore 7
    //   196: new 291	java/lang/StringBuilder
    //   199: astore 5
    //   201: aload 5
    //   203: invokespecial 292	java/lang/StringBuilder:<init>	()V
    //   206: aload 7
    //   208: ldc -55
    //   210: aload 5
    //   212: ldc_w 596
    //   215: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: aload 4
    //   220: invokevirtual 309	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   223: invokevirtual 312	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   226: aload 6
    //   228: invokeinterface 207 4 0
    //   233: lload_2
    //   234: lstore_0
    //   235: goto -154 -> 81
    //   238: astore 4
    //   240: ldc 2
    //   242: monitorexit
    //   243: aload 4
    //   245: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   39	196	0	l1	long
    //   14	220	2	l2	long
    //   18	201	4	localObject1	Object
    //   238	6	4	localObject2	Object
    //   152	59	5	localStringBuilder	StringBuilder
    //   147	13	6	localLogger1	Logger
    //   189	38	6	localNumberFormatException	NumberFormatException
    //   194	13	7	localLogger2	Logger
    // Exception table:
    //   from	to	target	type
    //   58	81	189	java/lang/NumberFormatException
    //   94	116	189	java/lang/NumberFormatException
    //   119	141	189	java/lang/NumberFormatException
    //   144	184	189	java/lang/NumberFormatException
    //   3	13	238	finally
    //   15	38	238	finally
    //   40	58	238	finally
    //   58	81	238	finally
    //   81	85	238	finally
    //   85	89	238	finally
    //   94	116	238	finally
    //   119	141	238	finally
    //   144	184	238	finally
    //   191	233	238	finally
  }
  
  private static String hash(InputStream paramInputStream, String paramString)
  {
    try
    {
      paramString = MessageDigest.getInstance("SHA-1");
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = paramInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        paramString.update(arrayOfByte, 0, i);
      }
      return paramInputStream;
    }
    catch (Exception paramInputStream)
    {
      Fabric.getLogger().e("Fabric", "Could not calculate hash for app icon.", paramInputStream);
      paramInputStream = "";
    }
    for (;;)
    {
      paramInputStream = hexify(paramString.digest());
    }
  }
  
  private static String hash(String paramString1, String paramString2)
  {
    return hash(paramString1.getBytes(), paramString2);
  }
  
  private static String hash(byte[] paramArrayOfByte, String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance(paramString);
      localMessageDigest.update(paramArrayOfByte);
      paramArrayOfByte = hexify(localMessageDigest.digest());
    }
    catch (NoSuchAlgorithmException paramArrayOfByte)
    {
      for (;;)
      {
        Fabric.getLogger().e("Fabric", "Could not create hashing algorithm: " + paramString + ", returning empty string.", paramArrayOfByte);
        paramArrayOfByte = "";
      }
    }
    return paramArrayOfByte;
  }
  
  public static String hexify(byte[] paramArrayOfByte)
  {
    char[] arrayOfChar = new char[paramArrayOfByte.length * 2];
    for (int i = 0; i < paramArrayOfByte.length; i++)
    {
      int j = paramArrayOfByte[i] & 0xFF;
      arrayOfChar[(i * 2)] = HEX_VALUES[(j >>> 4)];
      arrayOfChar[(i * 2 + 1)] = HEX_VALUES[(j & 0xF)];
    }
    return new String(arrayOfChar);
  }
  
  public static void hideKeyboard(Context paramContext, View paramView)
  {
    paramContext = (InputMethodManager)paramContext.getSystemService("input_method");
    if (paramContext != null) {
      paramContext.hideSoftInputFromWindow(paramView.getWindowToken(), 0);
    }
  }
  
  public static boolean isAppDebuggable(Context paramContext)
  {
    if ((paramContext.getApplicationInfo().flags & 0x2) != 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isClsTrace(Context paramContext)
  {
    if (clsTrace == null) {
      clsTrace = Boolean.valueOf(getBooleanResourceValue(paramContext, "com.crashlytics.Trace", false));
    }
    return clsTrace.booleanValue();
  }
  
  public static boolean isDebuggerAttached()
  {
    if ((Debug.isDebuggerConnected()) || (Debug.waitingForDebugger())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isEmulator(Context paramContext)
  {
    paramContext = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
    if (("sdk".equals(Build.PRODUCT)) || ("google_sdk".equals(Build.PRODUCT)) || (paramContext == null)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @Deprecated
  public static boolean isLoggingEnabled(Context paramContext)
  {
    return false;
  }
  
  public static boolean isNullOrEmpty(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isRooted(Context paramContext)
  {
    boolean bool2 = true;
    boolean bool3 = isEmulator(paramContext);
    paramContext = Build.TAGS;
    boolean bool1;
    if ((!bool3) && (paramContext != null) && (paramContext.contains("test-keys"))) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if (!new File("/system/app/Superuser.apk").exists())
      {
        paramContext = new File("/system/xbin/su");
        if (!bool3)
        {
          bool1 = bool2;
          if (paramContext.exists()) {}
        }
        else
        {
          bool1 = false;
        }
      }
    }
  }
  
  public static void logControlled(Context paramContext, int paramInt, String paramString1, String paramString2)
  {
    if (isClsTrace(paramContext)) {
      Fabric.getLogger().log(paramInt, "Fabric", paramString2);
    }
  }
  
  public static void logControlled(Context paramContext, String paramString)
  {
    if (isClsTrace(paramContext)) {
      Fabric.getLogger().d("Fabric", paramString);
    }
  }
  
  public static void logControlledError(Context paramContext, String paramString, Throwable paramThrowable)
  {
    if (isClsTrace(paramContext)) {
      Fabric.getLogger().e("Fabric", paramString);
    }
  }
  
  public static void logOrThrowIllegalArgumentException(String paramString1, String paramString2)
  {
    if (Fabric.isDebuggable()) {
      throw new IllegalArgumentException(paramString2);
    }
    Fabric.getLogger().w(paramString1, paramString2);
  }
  
  public static void logOrThrowIllegalStateException(String paramString1, String paramString2)
  {
    if (Fabric.isDebuggable()) {
      throw new IllegalStateException(paramString2);
    }
    Fabric.getLogger().w(paramString1, paramString2);
  }
  
  public static String logPriorityToString(int paramInt)
  {
    String str;
    switch (paramInt)
    {
    default: 
      str = "?";
    }
    for (;;)
    {
      return str;
      str = "A";
      continue;
      str = "D";
      continue;
      str = "E";
      continue;
      str = "I";
      continue;
      str = "V";
      continue;
      str = "W";
    }
  }
  
  public static String md5(String paramString)
  {
    return hash(paramString, "MD5");
  }
  
  public static String md5(byte[] paramArrayOfByte)
  {
    return hash(paramArrayOfByte, "MD5");
  }
  
  public static void openKeyboard(Context paramContext, View paramView)
  {
    paramContext = (InputMethodManager)paramContext.getSystemService("input_method");
    if (paramContext != null) {
      paramContext.showSoftInputFromInputMethod(paramView.getWindowToken(), 0);
    }
  }
  
  public static String padWithZerosToMaxIntWidth(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("value must be zero or greater");
    }
    return String.format(Locale.US, "%1$10s", new Object[] { Integer.valueOf(paramInt) }).replace(' ', '0');
  }
  
  public static String resolveBuildId(Context paramContext)
  {
    String str = null;
    int j = getResourcesIdentifier(paramContext, "io.fabric.android.build_id", "string");
    int i = j;
    if (j == 0) {
      i = getResourcesIdentifier(paramContext, "com.crashlytics.android.build_id", "string");
    }
    if (i != 0)
    {
      str = paramContext.getResources().getString(i);
      Fabric.getLogger().d("Fabric", "Build ID is: " + str);
    }
    return str;
  }
  
  public static String sha1(InputStream paramInputStream)
  {
    return hash(paramInputStream, "SHA-1");
  }
  
  public static String sha1(String paramString)
  {
    return hash(paramString, "SHA-1");
  }
  
  public static String sha1(byte[] paramArrayOfByte)
  {
    return hash(paramArrayOfByte, "SHA-1");
  }
  
  public static String streamToString(InputStream paramInputStream)
    throws IOException
  {
    paramInputStream = new Scanner(paramInputStream).useDelimiter("\\A");
    if (paramInputStream.hasNext()) {}
    for (paramInputStream = paramInputStream.next();; paramInputStream = "") {
      return paramInputStream;
    }
  }
  
  public static boolean stringsEqualIncludingNull(String paramString1, String paramString2)
  {
    boolean bool;
    if (paramString1 == paramString2) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if (paramString1 != null) {
        bool = paramString1.equals(paramString2);
      } else {
        bool = false;
      }
    }
  }
  
  static enum Architecture
  {
    private static final Map<String, Architecture> matcher;
    
    static
    {
      ARM_UNKNOWN = new Architecture("ARM_UNKNOWN", 2);
      PPC = new Architecture("PPC", 3);
      PPC64 = new Architecture("PPC64", 4);
      ARMV6 = new Architecture("ARMV6", 5);
      ARMV7 = new Architecture("ARMV7", 6);
      UNKNOWN = new Architecture("UNKNOWN", 7);
      ARMV7S = new Architecture("ARMV7S", 8);
      ARM64 = new Architecture("ARM64", 9);
      $VALUES = new Architecture[] { X86_32, X86_64, ARM_UNKNOWN, PPC, PPC64, ARMV6, ARMV7, UNKNOWN, ARMV7S, ARM64 };
      matcher = new HashMap(4);
      matcher.put("armeabi-v7a", ARMV7);
      matcher.put("armeabi", ARMV6);
      matcher.put("arm64-v8a", ARM64);
      matcher.put("x86", X86_32);
    }
    
    private Architecture() {}
    
    static Architecture getValue()
    {
      Object localObject = Build.CPU_ABI;
      if (TextUtils.isEmpty((CharSequence)localObject))
      {
        Fabric.getLogger().d("Fabric", "Architecture#getValue()::Build.CPU_ABI returned null or empty");
        localObject = UNKNOWN;
      }
      for (;;)
      {
        return (Architecture)localObject;
        localObject = ((String)localObject).toLowerCase(Locale.US);
        Architecture localArchitecture = (Architecture)matcher.get(localObject);
        localObject = localArchitecture;
        if (localArchitecture == null) {
          localObject = UNKNOWN;
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\common\CommonUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */