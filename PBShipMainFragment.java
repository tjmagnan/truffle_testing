package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tech.dcube.companion.managers.util.UtilManager;

public class PBShipMainFragment
  extends Fragment
{
  private OnShipmentFlowListener mListener;
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  @OnClick({2131624177})
  public void initiateBarcodeScan()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentFlowSelected("action_barcode_shipment_flow");
    }
  }
  
  @OnClick({2131624178})
  public void initiateManualItemPicking()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentFlowSelected("action_manual_shipment_flow");
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof ScanViewFinderFragment.OnBarcodeScannedListener))
    {
      this.mListener = ((OnShipmentFlowListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnShipmentFlowListener");
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968643, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public static abstract interface OnShipmentFlowListener
  {
    public abstract void onShipmentFlowSelected(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBShipMainFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */