package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PBSpecialServiceCharges
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 1377203317809080151L;
  @Expose
  @SerializedName("disableReason")
  private String disableReason;
  @Expose
  @SerializedName("displayName")
  private String displayName;
  @Expose
  @SerializedName("enabled")
  private Boolean enabled;
  @Expose
  @SerializedName("fee")
  private Object fee;
  @Expose
  @SerializedName("formInfo")
  private Object formInfo;
  @Expose
  @SerializedName("freeIncludedValue")
  private Integer freeIncludedValue;
  @Expose
  @SerializedName("inputParameters")
  private List<InputParameter> inputParameters = new ArrayList();
  @Expose
  @SerializedName("inputValue")
  private Object inputValue;
  @Expose
  @SerializedName("inputValueRequired")
  private Boolean inputValueRequired;
  @Expose
  @SerializedName("maxInputValue")
  private Double maxInputValue;
  @Expose
  @SerializedName("minInputValue")
  private Double minInputValue;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("selected")
  private Boolean selected;
  @Expose
  @SerializedName("serviceType")
  private String serviceType;
  @Expose
  @SerializedName("trackable")
  private Boolean trackable;
  
  public PBSpecialServiceCharges() {}
  
  public PBSpecialServiceCharges(String paramString1, String paramString2, Boolean paramBoolean1, Object paramObject1, Object paramObject2, Integer paramInteger, List<InputParameter> paramList, Object paramObject3, Boolean paramBoolean2, Double paramDouble1, Double paramDouble2, String paramString3, Boolean paramBoolean3, String paramString4, Boolean paramBoolean4)
  {
    this.disableReason = paramString1;
    this.displayName = paramString2;
    this.enabled = paramBoolean1;
    this.fee = paramObject1;
    this.formInfo = paramObject2;
    this.freeIncludedValue = paramInteger;
    this.inputParameters = paramList;
    this.inputValue = paramObject3;
    this.inputValueRequired = paramBoolean2;
    this.maxInputValue = paramDouble1;
    this.minInputValue = paramDouble2;
    this.name = paramString3;
    this.selected = paramBoolean3;
    this.serviceType = paramString4;
    this.trackable = paramBoolean4;
  }
  
  public String getDisableReason()
  {
    return this.disableReason;
  }
  
  public String getDisplayName()
  {
    return this.displayName;
  }
  
  public Boolean getEnabled()
  {
    return this.enabled;
  }
  
  public Object getFee()
  {
    return this.fee;
  }
  
  public Object getFormInfo()
  {
    return this.formInfo;
  }
  
  public Integer getFreeIncludedValue()
  {
    return this.freeIncludedValue;
  }
  
  public List<InputParameter> getInputParameters()
  {
    return this.inputParameters;
  }
  
  public Object getInputValue()
  {
    return this.inputValue;
  }
  
  public Boolean getInputValueRequired()
  {
    return this.inputValueRequired;
  }
  
  public Double getMaxInputValue()
  {
    return this.maxInputValue;
  }
  
  public Double getMinInputValue()
  {
    return this.minInputValue;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Boolean getSelected()
  {
    return this.selected;
  }
  
  public String getServiceType()
  {
    return this.serviceType;
  }
  
  public Boolean getTrackable()
  {
    return this.trackable;
  }
  
  public void setDisableReason(String paramString)
  {
    this.disableReason = paramString;
  }
  
  public void setDisplayName(String paramString)
  {
    this.displayName = paramString;
  }
  
  public void setEnabled(Boolean paramBoolean)
  {
    this.enabled = paramBoolean;
  }
  
  public void setFee(Object paramObject)
  {
    this.fee = paramObject;
  }
  
  public void setFormInfo(Object paramObject)
  {
    this.formInfo = paramObject;
  }
  
  public void setFreeIncludedValue(Integer paramInteger)
  {
    this.freeIncludedValue = paramInteger;
  }
  
  public void setInputParameters(List<InputParameter> paramList)
  {
    this.inputParameters = paramList;
  }
  
  public void setInputValue(Object paramObject)
  {
    this.inputValue = paramObject;
  }
  
  public void setInputValueRequired(Boolean paramBoolean)
  {
    this.inputValueRequired = paramBoolean;
  }
  
  public void setMaxInputValue(Double paramDouble)
  {
    this.maxInputValue = paramDouble;
  }
  
  public void setMinInputValue(Double paramDouble)
  {
    this.minInputValue = paramDouble;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setSelected(Boolean paramBoolean)
  {
    this.selected = paramBoolean;
  }
  
  public void setServiceType(String paramString)
  {
    this.serviceType = paramString;
  }
  
  public void setTrackable(Boolean paramBoolean)
  {
    this.trackable = paramBoolean;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBSpecialServiceCharges.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */