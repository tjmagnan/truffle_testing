package com.testfairy.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.testfairy.FeedbackContent;
import com.testfairy.FeedbackOptions;
import com.testfairy.FeedbackOptions.Callback;
import com.testfairy.activities.a.a.h;
import com.testfairy.activities.a.a.i;
import com.testfairy.e;
import com.testfairy.h.f;
import com.testfairy.k.d;
import com.testfairy.n;
import com.testfairy.p.m;
import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ProvideFeedbackActivity
  extends Activity
  implements h
{
  public static final String a = "Can't send an empty message, please type something.";
  public static final String b = "Please enter your email address.";
  public static final String c = "Invalid email address, try again.";
  public static final String d = "Thank you for your feedback.";
  public static final String e = "Oops! Something went wrong, Please check your internet connection.";
  public static boolean f = false;
  private static final String g = "Sending Feedback..";
  private FeedbackOptions.Callback h;
  private ProgressDialog i;
  private FeedbackContent j;
  private FeedbackOptions k;
  private String l;
  private float m;
  private i n;
  private String o;
  private Bitmap p;
  private boolean q = true;
  private d r;
  private String s = "";
  private DialogInterface.OnCancelListener t = new DialogInterface.OnCancelListener()
  {
    public void onCancel(DialogInterface paramAnonymousDialogInterface)
    {
      paramAnonymousDialogInterface.dismiss();
      ProvideFeedbackActivity.a(ProvideFeedbackActivity.this);
    }
  };
  
  private int a(Intent paramIntent)
  {
    int i1 = -1;
    if (paramIntent == null) {
      Log.d(e.a, "PFA no intent");
    }
    for (;;)
    {
      return i1;
      paramIntent = paramIntent.getExtras();
      if (paramIntent == null) {
        Log.d(e.a, "PFA no extras");
      } else {
        i1 = paramIntent.getInt(n.M, -1);
      }
    }
  }
  
  private void a(String paramString)
  {
    if ((paramString == null) || (paramString.isEmpty())) {}
    for (;;)
    {
      return;
      try
      {
        SharedPreferences.Editor localEditor = getPreferences(0).edit();
        localEditor.putString("pbz.grfgsnvel.srrqonpxErcbegreRznvy", paramString);
        localEditor.commit();
      }
      catch (Throwable paramString) {}
    }
  }
  
  private void c()
  {
    Log.d(e.a, n.R);
    this.h.onFeedbackCancelled();
    finish();
  }
  
  private void d()
  {
    int i2 = 0;
    Object localObject2 = this.n.b().getText().toString().trim();
    Object localObject1 = this.n.a().getText().toString().trim();
    AlertDialog.Builder localBuilder;
    int i1;
    if ((((String)localObject2).isEmpty()) || ((this.k.b()) && ((((String)localObject1).isEmpty()) || (!m.a((CharSequence)localObject1)))))
    {
      localBuilder = com.testfairy.p.c.a(this);
      if (((String)localObject2).isEmpty())
      {
        localBuilder.setMessage("Can't send an empty message, please type something.");
        i1 = 0;
        i2 = 1;
      }
    }
    for (;;)
    {
      if (i2 != 0) {
        localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
          public void onCancel(DialogInterface paramAnonymousDialogInterface)
          {
            ProvideFeedbackActivity.e(ProvideFeedbackActivity.this).i();
          }
        });
      }
      for (;;)
      {
        localBuilder.setCancelable(true);
        localObject1 = localBuilder.create();
        if (!isFinishing()) {
          ((AlertDialog)localObject1).show();
        }
        this.n.l();
        return;
        if (((String)localObject1).isEmpty())
        {
          localBuilder.setMessage("Please enter your email address.");
          i1 = 1;
          break;
        }
        if (m.a((CharSequence)localObject1)) {
          break label490;
        }
        localBuilder.setMessage("Invalid email address, try again.");
        i1 = 1;
        break;
        if (i1 != 0) {
          localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
          {
            public void onCancel(DialogInterface paramAnonymousDialogInterface)
            {
              ProvideFeedbackActivity.e(ProvideFeedbackActivity.this).j();
            }
          });
        }
      }
      a((String)localObject1);
      e();
      this.i = com.testfairy.p.c.b(this);
      this.i.setIndeterminate(true);
      this.i.setProgressStyle(0);
      this.i.setTitle("Sending Feedback..");
      this.i.setProgressStyle(1);
      this.i.setProgressNumberFormat(null);
      this.i.setOnCancelListener(this.t);
      this.i.setCancelable(true);
      this.i.show();
      this.j = new FeedbackContent((String)localObject2, (String)localObject1, this.m);
      localObject2 = new f();
      ((f)localObject2).a("sessionToken", this.l);
      ((f)localObject2).a("timestamp", String.valueOf(this.j.getTimestamp()));
      ((f)localObject2).a("text", this.j.getText());
      ((f)localObject2).a("reporterEmail", this.j.getEmail());
      ((f)localObject2).a("screenName", this.s);
      this.p = this.n.h();
      if ((!this.n.k()) && (this.p != null)) {
        ((f)localObject2).a("screenshot", new ByteArrayInputStream(com.testfairy.p.b.b(this.p)));
      }
      if (this.n.k()) {}
      for (localObject1 = "true";; localObject1 = "false")
      {
        ((f)localObject2).a("screenshotDisabledByUser", (String)localObject1);
        this.r.g((f)localObject2, new a(null));
        break;
      }
      label490:
      i1 = 0;
    }
  }
  
  private void e()
  {
    try
    {
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.n.b().getWindowToken(), 0);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private String f()
  {
    try
    {
      String str1 = getPreferences(0).getString("pbz.grfgsnvel.srrqonpxErcbegreRznvy", "");
      return str1;
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        String str2 = "";
      }
    }
  }
  
  public void a()
  {
    this.n.e();
    setRequestedOrientation(-1);
  }
  
  public void a(View paramView)
  {
    c();
  }
  
  public void b()
  {
    this.n.f();
    setRequestedOrientation(-1);
  }
  
  public void b(View paramView)
  {
    d();
  }
  
  public void c(View paramView)
  {
    if (this.p == null) {
      return;
    }
    this.n.c();
    if (this.q) {
      setRequestedOrientation(7);
    }
    for (;;)
    {
      e();
      break;
      setRequestedOrientation(6);
    }
  }
  
  public void onBackPressed()
  {
    if (this.n.g())
    {
      this.n.e();
      setRequestedOrientation(-1);
    }
    for (;;)
    {
      return;
      c();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    this.i = null;
    int i1 = a(getIntent());
    paramBundle = com.testfairy.b.a().a(Integer.valueOf(i1));
    com.testfairy.b.a().b(Integer.valueOf(i1));
    Log.d(e.a, "PFA bundleId = " + i1);
    if (paramBundle == null)
    {
      Log.d(e.a, "PFA bundle is empty");
      finish();
      return;
    }
    this.k = ((FeedbackOptions)paramBundle.get(n.H));
    this.h = this.k.c();
    this.s = ((String)paramBundle.get(n.K));
    this.p = ((Bitmap)paramBundle.get(n.L));
    this.r = ((d)paramBundle.get(n.N));
    if (paramBundle.containsKey(n.I))
    {
      this.l = ((String)paramBundle.get(n.I));
      this.m = ((Float)paramBundle.get(n.G)).floatValue();
    }
    if (this.p != null) {
      if (this.p.getWidth() > this.p.getHeight()) {
        break label283;
      }
    }
    label283:
    for (boolean bool = true;; bool = false)
    {
      this.q = bool;
      paramBundle = f();
      this.n = new i(this, this.k, paramBundle, this.p, this);
      setContentView(this.n);
      break;
    }
  }
  
  protected void onPause()
  {
    super.onPause();
    f = false;
  }
  
  protected void onResume()
  {
    super.onResume();
    f = true;
  }
  
  private class a
    extends com.testfairy.h.c
  {
    private a() {}
    
    private void a(boolean paramBoolean)
    {
      ProvideFeedbackActivity.this.getWindow().setFeatureInt(5, -2);
      final Object localObject;
      if (ProvideFeedbackActivity.d(ProvideFeedbackActivity.this) != null)
      {
        com.testfairy.p.c.a(ProvideFeedbackActivity.d(ProvideFeedbackActivity.this));
        ProvideFeedbackActivity.a(ProvideFeedbackActivity.this, null);
        AlertDialog.Builder localBuilder = com.testfairy.p.c.a(ProvideFeedbackActivity.this);
        if (!paramBoolean) {
          break label119;
        }
        localObject = "Thank you for your feedback.";
        localBuilder.setMessage((CharSequence)localObject);
        localObject = localBuilder.create();
        if (!ProvideFeedbackActivity.this.isFinishing()) {
          ((AlertDialog)localObject).show();
        }
        if (!paramBoolean) {
          break label125;
        }
        Log.d(e.a, n.P);
        new Timer().schedule(new TimerTask()
        {
          public void run()
          {
            if (!ProvideFeedbackActivity.this.isFinishing())
            {
              com.testfairy.p.c.a(localObject);
              ProvideFeedbackActivity.this.finish();
            }
          }
        }, 2500L);
      }
      for (;;)
      {
        return;
        label119:
        localObject = "Oops! Something went wrong, Please check your internet connection.";
        break;
        label125:
        ProvideFeedbackActivity.e(ProvideFeedbackActivity.this).l();
        Log.d(e.a, n.Q);
      }
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      a(true);
      ProvideFeedbackActivity.c(ProvideFeedbackActivity.this).onFeedbackSent(ProvideFeedbackActivity.b(ProvideFeedbackActivity.this));
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      a(false);
      ProvideFeedbackActivity.c(ProvideFeedbackActivity.this).onFeedbackFailed(1, ProvideFeedbackActivity.b(ProvideFeedbackActivity.this));
    }
    
    public void b()
    {
      super.b();
      ProvideFeedbackActivity.this.getWindow().setFeatureInt(5, -1);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\ProvideFeedbackActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */