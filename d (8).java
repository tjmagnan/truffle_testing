package com.testfairy.d;

import android.graphics.Bitmap;
import android.view.SurfaceHolder;
import com.testfairy.p.q;
import java.util.List;

public abstract interface h
{
  public abstract SurfaceHolder a(SurfaceHolder paramSurfaceHolder);
  
  public abstract void a(Bitmap paramBitmap, q paramq);
  
  public abstract void a(Bitmap paramBitmap, q paramq, List paramList);
  
  public abstract void a(d paramd, long paramLong);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */