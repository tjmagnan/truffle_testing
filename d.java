package com.testfairy;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;

public class d
  implements SurfaceHolder
{
  private static int b = 0;
  private SurfaceHolder a;
  private Canvas c = null;
  private Canvas d = null;
  private Bitmap e = null;
  
  public d(SurfaceHolder paramSurfaceHolder)
  {
    this.a = paramSurfaceHolder;
  }
  
  public void a(Canvas paramCanvas)
  {
    if (this.e != null) {
      paramCanvas.drawBitmap(this.e, new Matrix(), new Paint());
    }
  }
  
  public void addCallback(SurfaceHolder.Callback paramCallback)
  {
    this.a.addCallback(paramCallback);
  }
  
  public Surface getSurface()
  {
    return this.a.getSurface();
  }
  
  public Rect getSurfaceFrame()
  {
    return this.a.getSurfaceFrame();
  }
  
  public boolean isCreating()
  {
    return this.a.isCreating();
  }
  
  public Canvas lockCanvas()
  {
    this.c = this.a.lockCanvas();
    Canvas localCanvas1;
    if (this.c == null) {
      localCanvas1 = null;
    }
    for (;;)
    {
      return localCanvas1;
      try
      {
        this.e = Bitmap.createBitmap(this.c.getWidth(), this.c.getHeight(), Bitmap.Config.ARGB_8888);
        localCanvas1 = new android/graphics/Canvas;
        localCanvas1.<init>(this.e);
        this.d = localCanvas1;
        localCanvas1 = this.d;
      }
      catch (Exception localException)
      {
        Canvas localCanvas2 = this.c;
      }
      catch (OutOfMemoryError localOutOfMemoryError)
      {
        Canvas localCanvas3 = this.c;
      }
    }
  }
  
  public Canvas lockCanvas(Rect paramRect)
  {
    this.c = this.a.lockCanvas(paramRect);
    try
    {
      this.e = Bitmap.createBitmap(this.c.getWidth(), this.c.getHeight(), Bitmap.Config.ARGB_8888);
      paramRect = new android/graphics/Canvas;
      paramRect.<init>(this.e);
      this.d = paramRect;
      paramRect = this.d;
      return paramRect;
    }
    catch (Exception paramRect)
    {
      for (;;)
      {
        paramRect = this.c;
      }
    }
    catch (OutOfMemoryError paramRect)
    {
      for (;;)
      {
        paramRect = this.c;
      }
    }
  }
  
  public void removeCallback(SurfaceHolder.Callback paramCallback)
  {
    this.a.removeCallback(paramCallback);
  }
  
  public void setFixedSize(int paramInt1, int paramInt2)
  {
    this.a.setFixedSize(paramInt1, paramInt2);
  }
  
  public void setFormat(int paramInt)
  {
    this.a.setFormat(paramInt);
  }
  
  public void setKeepScreenOn(boolean paramBoolean)
  {
    this.a.setKeepScreenOn(paramBoolean);
  }
  
  public void setSizeFromLayout()
  {
    this.a.setSizeFromLayout();
  }
  
  public void setType(int paramInt)
  {
    this.a.setType(paramInt);
  }
  
  public void unlockCanvasAndPost(Canvas paramCanvas)
  {
    if (paramCanvas == this.d)
    {
      this.c.drawBitmap(this.e, new Matrix(), new Paint());
      this.a.unlockCanvasAndPost(this.c);
    }
    for (;;)
    {
      return;
      this.a.unlockCanvasAndPost(paramCanvas);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */