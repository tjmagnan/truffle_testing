package com.testfairy.c;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import com.testfairy.e;

public class d
  extends FrameLayout
{
  private static int e = 0;
  private static int f = 0;
  private WindowManager a;
  private WindowManager.LayoutParams b;
  private int c;
  private int d;
  
  public d(Context paramContext)
  {
    super(paramContext);
    this.a = ((WindowManager)paramContext.getSystemService("window"));
    this.c = 144;
    this.d = 176;
  }
  
  private WindowManager.LayoutParams a()
  {
    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
    localLayoutParams.type = 2003;
    localLayoutParams.flags = 40;
    localLayoutParams.width = -2;
    localLayoutParams.height = -2;
    localLayoutParams.gravity = 51;
    localLayoutParams.format = -3;
    localLayoutParams.x = e;
    localLayoutParams.y = f;
    return localLayoutParams;
  }
  
  public void a(Context paramContext)
  {
    try
    {
      this.b = a();
      this.a.addView(this, this.b);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        Log.e(e.a, "Minor exception, ignored", paramContext);
      }
    }
  }
  
  public void b(Context paramContext)
  {
    try
    {
      this.b = null;
      this.a.removeView(this);
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;)
      {
        Log.e(e.a, "Minor exception, ignoring", paramContext);
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.b != null)
    {
      int j = (int)paramMotionEvent.getRawX() - this.c / 2;
      int i = (int)paramMotionEvent.getRawY() - this.d / 2;
      this.b.x = Math.max(j, 0);
      this.b.y = Math.max(i, 0);
      this.a.updateViewLayout(this, this.b);
      e = j;
      f = i;
    }
    return super.onTouchEvent(paramMotionEvent);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\c\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */