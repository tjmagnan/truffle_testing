package tech.dcube.companion.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBDimensionRules;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentPrintLabel;
import tech.dcube.companion.model.PBShipmentRate;
import tech.dcube.companion.model.PBShipmentService;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBWeightRules;

public class PBSigninActivityOLD
  extends AppCompatActivity
{
  private static final boolean DEBUG_MODE = false;
  AppCompatButton buttonSignIn;
  EditText inputPassword;
  EditText inputUserName;
  LinearLayout layoutTestCredentials;
  String mPassword = "";
  String mUserName = "";
  TextView pbForgotPassword;
  TextView pbSignUp;
  private MaterialDialog progressDialog;
  boolean switchState;
  SwitchCompat switchUser;
  
  void callGetAvailablePackages(final PBAddress paramPBAddress)
  {
    ServerManager.getInstance().getAvailablePackages(getBaseContext(), paramPBAddress, new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Packages Error: " + paramAnonymousVolleyError.getMessage());
      }
      
      public void onSuccess(Map<PBPackageType, List<PBShipmentPackage>> paramAnonymousMap, String paramAnonymousString)
      {
        if (((List)paramAnonymousMap.get(PBPackageType.FLAT_RATE_BOX)).size() > 0)
        {
          int i = UtilManager.getRandom(((List)paramAnonymousMap.get(PBPackageType.FLAT_RATE_BOX)).size());
          paramAnonymousMap = (PBShipmentPackage)((List)paramAnonymousMap.get(PBPackageType.FLAT_RATE_BOX)).get(i);
          if (paramAnonymousMap.getDimensionRequired().booleanValue())
          {
            paramAnonymousMap.getDimensionRules().setLength(10.0D);
            paramAnonymousMap.getDimensionRules().setWidth(10.0D);
            paramAnonymousMap.getDimensionRules().setHeight(10.0D);
            paramAnonymousMap.getDimensionRules().setGirth(10.0D);
          }
          if (paramAnonymousMap.getWeightRequired().booleanValue()) {
            paramAnonymousMap.getWeightRules().setWeight(10.0D);
          }
          PBSigninActivityOLD.this.callGetAvailableServices(paramPBAddress, paramAnonymousMap);
        }
      }
    });
  }
  
  void callGetAvailableServices(final PBAddress paramPBAddress, final PBShipmentPackage paramPBShipmentPackage)
  {
    ServerManager.getInstance().getServices(this, DataManager.getInstance().getUser().getAddress(), paramPBAddress, paramPBShipmentPackage, new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Error in Services: " + paramAnonymousVolleyError.getMessage());
        Log.wtf(paramAnonymousString, "Code: " + paramAnonymousVolleyError.networkResponse.statusCode);
      }
      
      public void onSuccess(List<PBShipmentService> paramAnonymousList, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Services: " + paramAnonymousList);
        PBSigninActivityOLD.this.callGetShipmentRate(paramPBAddress, paramPBShipmentPackage, (PBShipmentService)paramAnonymousList.get(0));
      }
    });
  }
  
  void callGetShipmentRate(final PBAddress paramPBAddress, final PBShipmentPackage paramPBShipmentPackage, final PBShipmentService paramPBShipmentService)
  {
    ServerManager.getInstance().getShipmentRate(this, DataManager.getInstance().getUser().getAddress(), paramPBAddress, paramPBShipmentPackage, paramPBShipmentService, new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Error in rate: " + paramAnonymousVolleyError.getMessage());
        Log.wtf(paramAnonymousString, "Code: " + paramAnonymousVolleyError.networkResponse.statusCode);
      }
      
      public void onSuccess(PBShipmentRate paramAnonymousPBShipmentRate, String paramAnonymousString)
      {
        PBSigninActivityOLD.this.callPrintShipmentLabel(paramPBAddress, paramPBShipmentPackage, paramPBShipmentService, paramAnonymousPBShipmentRate);
      }
    });
  }
  
  void callGetUserAddress()
  {
    ServerManager.getInstance().getUserAddress(getBaseContext(), new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Address Error: " + paramAnonymousVolleyError.getMessage());
      }
      
      public void onSuccess(PBAddress paramAnonymousPBAddress, String paramAnonymousString)
      {
        PBSigninActivityOLD.this.callGetUserAddressBookApi();
      }
    });
  }
  
  void callGetUserAddressBookApi()
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getBaseContext();
      ServerManager.RequestCompletionHandler local7 = new tech/dcube/companion/activities/PBSigninActivityOLD$7;
      local7.<init>(this);
      localServerManager.getAddressBook(localContext, local7);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void callPrintShipmentLabel(PBAddress paramPBAddress, PBShipmentPackage paramPBShipmentPackage, PBShipmentService paramPBShipmentService, PBShipmentRate paramPBShipmentRate)
  {
    ServerManager.getInstance().printShipmentLabel(this, DataManager.getInstance().getUser().getAddress(), paramPBAddress, paramPBShipmentPackage, paramPBShipmentService, paramPBShipmentRate, new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Error in Print: " + paramAnonymousVolleyError.getMessage());
        Log.wtf(paramAnonymousString, "Code: " + paramAnonymousVolleyError.networkResponse.statusCode);
      }
      
      public void onSuccess(PBShipmentPrintLabel paramAnonymousPBShipmentPrintLabel, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Label: " + paramAnonymousPBShipmentPrintLabel);
      }
    });
  }
  
  void callSignInApi(String paramString1, String paramString2)
  {
    this.progressDialog = new MaterialDialog.Builder(this).customView(2130968704, false).build();
    this.progressDialog.show();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local4 = new tech/dcube/companion/activities/PBSigninActivityOLD$4;
      local4.<init>(this);
      localServerManager.login(this, paramString1, paramString2, local4);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  void callVerifyAddressApi(PBAddress paramPBAddress)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getBaseContext();
      ServerManager.RequestCompletionHandler local8 = new tech/dcube/companion/activities/PBSigninActivityOLD$8;
      local8.<init>(this);
      localServerManager.verifyAddress(localContext, paramPBAddress, local8);
      return;
    }
    catch (Exception paramPBAddress)
    {
      for (;;)
      {
        paramPBAddress.printStackTrace();
      }
    }
  }
  
  void launchMainActivity()
  {
    Intent localIntent = new Intent(getApplicationContext(), PBMainFragmentActivity.class);
    finish();
    startActivity(localIntent);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968605);
    UtilManager.hideSoftKeyboard(this);
    this.inputUserName = ((EditText)findViewById(2131624064));
    this.inputPassword = ((EditText)findViewById(2131624066));
    this.pbForgotPassword = ((TextView)findViewById(2131624069));
    this.pbSignUp = ((TextView)findViewById(2131624070));
    this.buttonSignIn = ((AppCompatButton)findViewById(2131624068));
    this.layoutTestCredentials = ((LinearLayout)findViewById(2131624071));
    this.inputUserName.clearFocus();
    this.inputPassword.clearFocus();
    UtilManager.hideSoftKeyboard(this);
    this.switchUser = ((SwitchCompat)findViewById(2131624072));
    this.switchUser.setChecked(false);
    this.layoutTestCredentials.setVisibility(8);
    this.buttonSignIn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PBSigninActivityOLD.this.mUserName = PBSigninActivityOLD.this.inputUserName.getText().toString();
        PBSigninActivityOLD.this.mPassword = PBSigninActivityOLD.this.inputPassword.getText().toString();
        PBSigninActivityOLD.this.callSignInApi(PBSigninActivityOLD.this.mUserName, PBSigninActivityOLD.this.mPassword);
      }
    });
    this.pbForgotPassword.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = new Intent("android.intent.action.VIEW");
        paramAnonymousView.setData(Uri.parse("https://www.pitneybowes.us/signin/logon.go?request_locale=en_US#/account/forgot-password"));
        PBSigninActivityOLD.this.startActivity(paramAnonymousView);
      }
    });
    this.pbSignUp.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = new Intent("android.intent.action.VIEW");
        paramAnonymousView.setData(Uri.parse("https://www.pitneybowes.us/signin/logon.go?request_locale=en_US#/register/step1"));
        PBSigninActivityOLD.this.startActivity(paramAnonymousView);
      }
    });
  }
  
  public void signInToPB()
  {
    if (this.switchUser.isChecked()) {
      this.mUserName = "AgniMulti50TCC1@mailinator.com";
    }
    for (this.mPassword = "Horizon#123";; this.mPassword = "Group!234")
    {
      callSignInApi(this.mUserName, this.mPassword);
      return;
      this.mUserName = "mobileuser1@mailinator.com";
    }
  }
  
  public void testAPIs()
  {
    Log.wtf("T", "Logging IN-------------------");
    ServerManager.getInstance().login(this, "AgniMulti50TCC1@mailinator.com", "Horizon#123", new ServerManager.RequestCompletionHandler()
    {
      public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
      {
        Log.wtf(paramAnonymousString, "Login Error: " + paramAnonymousVolleyError.getMessage());
      }
      
      public void onSuccess(PBUser paramAnonymousPBUser, String paramAnonymousString)
      {
        PBSigninActivityOLD.this.callGetUserAddress();
      }
    });
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\activities\PBSigninActivityOLD.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */