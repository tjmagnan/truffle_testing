package org.joda.time.tz;

import java.util.Set;
import org.joda.time.DateTimeZone;

public abstract interface Provider
{
  public abstract Set<String> getAvailableIDs();
  
  public abstract DateTimeZone getZone(String paramString);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\Provider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */