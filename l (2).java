package com.testfairy.l;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

public class b
  implements GL10
{
  private GL10 a;
  
  public b(GL10 paramGL10)
  {
    this.a = paramGL10;
  }
  
  public void glActiveTexture(int paramInt)
  {
    this.a.glActiveTexture(paramInt);
  }
  
  public void glAlphaFunc(int paramInt, float paramFloat)
  {
    this.a.glAlphaFunc(paramInt, paramFloat);
  }
  
  public void glAlphaFuncx(int paramInt1, int paramInt2)
  {
    this.a.glAlphaFuncx(paramInt1, paramInt2);
  }
  
  public void glBindTexture(int paramInt1, int paramInt2)
  {
    this.a.glBindTexture(paramInt1, paramInt2);
  }
  
  public void glBlendFunc(int paramInt1, int paramInt2)
  {
    this.a.glBlendFunc(paramInt1, paramInt2);
  }
  
  public void glClear(int paramInt)
  {
    this.a.glClear(paramInt);
  }
  
  public void glClearColor(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.a.glClearColor(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void glClearColorx(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a.glClearColorx(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glClearDepthf(float paramFloat)
  {
    this.a.glClearDepthf(paramFloat);
  }
  
  public void glClearDepthx(int paramInt)
  {
    this.a.glClearDepthx(paramInt);
  }
  
  public void glClearStencil(int paramInt)
  {
    this.a.glClearStencil(paramInt);
  }
  
  public void glClientActiveTexture(int paramInt)
  {
    this.a.glClientActiveTexture(paramInt);
  }
  
  public void glColor4f(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.a.glColor4f(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void glColor4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a.glColor4x(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glColorMask(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    this.a.glColorMask(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
  }
  
  public void glColorPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
  {
    this.a.glColorPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
  }
  
  public void glCompressedTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Buffer paramBuffer)
  {
    this.a.glCompressedTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramBuffer);
  }
  
  public void glCompressedTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
  {
    this.a.glCompressedTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
  }
  
  public void glCopyTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    this.a.glCopyTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
  }
  
  public void glCopyTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    this.a.glCopyTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8);
  }
  
  public void glCullFace(int paramInt)
  {
    this.a.glCullFace(paramInt);
  }
  
  public void glDeleteTextures(int paramInt, IntBuffer paramIntBuffer)
  {
    this.a.glDeleteTextures(paramInt, paramIntBuffer);
  }
  
  public void glDeleteTextures(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    this.a.glDeleteTextures(paramInt1, paramArrayOfInt, paramInt2);
  }
  
  public void glDepthFunc(int paramInt)
  {
    this.a.glDepthFunc(paramInt);
  }
  
  public void glDepthMask(boolean paramBoolean)
  {
    this.a.glDepthMask(paramBoolean);
  }
  
  public void glDepthRangef(float paramFloat1, float paramFloat2)
  {
    this.a.glDepthRangef(paramFloat1, paramFloat2);
  }
  
  public void glDepthRangex(int paramInt1, int paramInt2)
  {
    this.a.glDepthRangex(paramInt1, paramInt2);
  }
  
  public void glDisable(int paramInt)
  {
    this.a.glDisable(paramInt);
  }
  
  public void glDisableClientState(int paramInt)
  {
    this.a.glDisableClientState(paramInt);
  }
  
  public void glDrawArrays(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glDrawArrays(paramInt1, paramInt2, paramInt3);
  }
  
  public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
  {
    this.a.glDrawElements(paramInt1, paramInt2, paramInt3, paramBuffer);
  }
  
  public void glEnable(int paramInt)
  {
    this.a.glEnable(paramInt);
  }
  
  public void glEnableClientState(int paramInt)
  {
    this.a.glEnableClientState(paramInt);
  }
  
  public void glFinish()
  {
    this.a.glFinish();
  }
  
  public void glFlush()
  {
    this.a.glFlush();
  }
  
  public void glFogf(int paramInt, float paramFloat)
  {
    this.a.glFogf(paramInt, paramFloat);
  }
  
  public void glFogfv(int paramInt, FloatBuffer paramFloatBuffer)
  {
    this.a.glFogfv(paramInt, paramFloatBuffer);
  }
  
  public void glFogfv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
  {
    this.a.glFogfv(paramInt1, paramArrayOfFloat, paramInt2);
  }
  
  public void glFogx(int paramInt1, int paramInt2)
  {
    this.a.glFogx(paramInt1, paramInt2);
  }
  
  public void glFogxv(int paramInt, IntBuffer paramIntBuffer)
  {
    this.a.glFogxv(paramInt, paramIntBuffer);
  }
  
  public void glFogxv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    this.a.glFogxv(paramInt1, paramArrayOfInt, paramInt2);
  }
  
  public void glFrontFace(int paramInt)
  {
    this.a.glFrontFace(paramInt);
  }
  
  public void glFrustumf(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    this.a.glFrustumf(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
  }
  
  public void glFrustumx(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    this.a.glFrustumx(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
  }
  
  public void glGenTextures(int paramInt, IntBuffer paramIntBuffer)
  {
    this.a.glGenTextures(paramInt, paramIntBuffer);
  }
  
  public void glGenTextures(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    this.a.glGenTextures(paramInt1, paramArrayOfInt, paramInt2);
  }
  
  public int glGetError()
  {
    return this.a.glGetError();
  }
  
  public void glGetIntegerv(int paramInt, IntBuffer paramIntBuffer)
  {
    this.a.glGetIntegerv(paramInt, paramIntBuffer);
  }
  
  public void glGetIntegerv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    this.a.glGetIntegerv(paramInt1, paramArrayOfInt, paramInt2);
  }
  
  public String glGetString(int paramInt)
  {
    return this.a.glGetString(paramInt);
  }
  
  public void glHint(int paramInt1, int paramInt2)
  {
    this.a.glHint(paramInt1, paramInt2);
  }
  
  public void glLightModelf(int paramInt, float paramFloat)
  {
    this.a.glLightModelf(paramInt, paramFloat);
  }
  
  public void glLightModelfv(int paramInt, FloatBuffer paramFloatBuffer)
  {
    this.a.glLightModelfv(paramInt, paramFloatBuffer);
  }
  
  public void glLightModelfv(int paramInt1, float[] paramArrayOfFloat, int paramInt2)
  {
    this.a.glLightModelfv(paramInt1, paramArrayOfFloat, paramInt2);
  }
  
  public void glLightModelx(int paramInt1, int paramInt2)
  {
    this.a.glLightModelx(paramInt1, paramInt2);
  }
  
  public void glLightModelxv(int paramInt, IntBuffer paramIntBuffer)
  {
    this.a.glLightModelxv(paramInt, paramIntBuffer);
  }
  
  public void glLightModelxv(int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    this.a.glLightModelxv(paramInt1, paramArrayOfInt, paramInt2);
  }
  
  public void glLightf(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a.glLightf(paramInt1, paramInt2, paramFloat);
  }
  
  public void glLightfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
  {
    this.a.glLightfv(paramInt1, paramInt2, paramFloatBuffer);
  }
  
  public void glLightfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
  {
    this.a.glLightfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
  }
  
  public void glLightx(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glLightx(paramInt1, paramInt2, paramInt3);
  }
  
  public void glLightxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
  {
    this.a.glLightxv(paramInt1, paramInt2, paramIntBuffer);
  }
  
  public void glLightxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
  {
    this.a.glLightxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
  }
  
  public void glLineWidth(float paramFloat)
  {
    this.a.glLineWidth(paramFloat);
  }
  
  public void glLineWidthx(int paramInt)
  {
    this.a.glLineWidthx(paramInt);
  }
  
  public void glLoadIdentity()
  {
    this.a.glLoadIdentity();
  }
  
  public void glLoadMatrixf(FloatBuffer paramFloatBuffer)
  {
    this.a.glLoadMatrixf(paramFloatBuffer);
  }
  
  public void glLoadMatrixf(float[] paramArrayOfFloat, int paramInt)
  {
    this.a.glLoadMatrixf(paramArrayOfFloat, paramInt);
  }
  
  public void glLoadMatrixx(IntBuffer paramIntBuffer)
  {
    this.a.glLoadMatrixx(paramIntBuffer);
  }
  
  public void glLoadMatrixx(int[] paramArrayOfInt, int paramInt)
  {
    this.a.glLoadMatrixx(paramArrayOfInt, paramInt);
  }
  
  public void glLogicOp(int paramInt)
  {
    this.a.glLogicOp(paramInt);
  }
  
  public void glMaterialf(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a.glMaterialf(paramInt1, paramInt2, paramFloat);
  }
  
  public void glMaterialfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
  {
    this.a.glMaterialfv(paramInt1, paramInt2, paramFloatBuffer);
  }
  
  public void glMaterialfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
  {
    this.a.glMaterialfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
  }
  
  public void glMaterialx(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glMaterialx(paramInt1, paramInt2, paramInt3);
  }
  
  public void glMaterialxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
  {
    this.a.glMaterialxv(paramInt1, paramInt2, paramIntBuffer);
  }
  
  public void glMaterialxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
  {
    this.a.glMaterialxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
  }
  
  public void glMatrixMode(int paramInt)
  {
    this.a.glMatrixMode(paramInt);
  }
  
  public void glMultMatrixf(FloatBuffer paramFloatBuffer)
  {
    this.a.glMultMatrixf(paramFloatBuffer);
  }
  
  public void glMultMatrixf(float[] paramArrayOfFloat, int paramInt)
  {
    this.a.glMultMatrixf(paramArrayOfFloat, paramInt);
  }
  
  public void glMultMatrixx(IntBuffer paramIntBuffer)
  {
    this.a.glMultMatrixx(paramIntBuffer);
  }
  
  public void glMultMatrixx(int[] paramArrayOfInt, int paramInt)
  {
    this.a.glMultMatrixx(paramArrayOfInt, paramInt);
  }
  
  public void glMultiTexCoord4f(int paramInt, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.a.glMultiTexCoord4f(paramInt, paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void glMultiTexCoord4x(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    this.a.glMultiTexCoord4x(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  public void glNormal3f(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.a.glNormal3f(paramFloat1, paramFloat2, paramFloat3);
  }
  
  public void glNormal3x(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glNormal3x(paramInt1, paramInt2, paramInt3);
  }
  
  public void glNormalPointer(int paramInt1, int paramInt2, Buffer paramBuffer)
  {
    this.a.glNormalPointer(paramInt1, paramInt2, paramBuffer);
  }
  
  public void glOrthof(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6)
  {
    this.a.glOrthof(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6);
  }
  
  public void glOrthox(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    this.a.glOrthox(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
  }
  
  public void glPixelStorei(int paramInt1, int paramInt2)
  {
    this.a.glPixelStorei(paramInt1, paramInt2);
  }
  
  public void glPointSize(float paramFloat)
  {
    this.a.glPointSize(paramFloat);
  }
  
  public void glPointSizex(int paramInt)
  {
    this.a.glPointSizex(paramInt);
  }
  
  public void glPolygonOffset(float paramFloat1, float paramFloat2)
  {
    this.a.glPolygonOffset(paramFloat1, paramFloat2);
  }
  
  public void glPolygonOffsetx(int paramInt1, int paramInt2)
  {
    this.a.glPolygonOffsetx(paramInt1, paramInt2);
  }
  
  public void glPopMatrix()
  {
    this.a.glPopMatrix();
  }
  
  public void glPushMatrix()
  {
    this.a.glPushMatrix();
  }
  
  public void glReadPixels(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Buffer paramBuffer)
  {
    this.a.glReadPixels(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBuffer);
  }
  
  public void glRotatef(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.a.glRotatef(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
  }
  
  public void glRotatex(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a.glRotatex(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glSampleCoverage(float paramFloat, boolean paramBoolean)
  {
    this.a.glSampleCoverage(paramFloat, paramBoolean);
  }
  
  public void glSampleCoveragex(int paramInt, boolean paramBoolean)
  {
    this.a.glSampleCoveragex(paramInt, paramBoolean);
  }
  
  public void glScalef(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.a.glScalef(paramFloat1, paramFloat2, paramFloat3);
  }
  
  public void glScalex(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glScalex(paramInt1, paramInt2, paramInt3);
  }
  
  public void glScissor(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a.glScissor(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void glShadeModel(int paramInt)
  {
    this.a.glShadeModel(paramInt);
  }
  
  public void glStencilFunc(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glStencilFunc(paramInt1, paramInt2, paramInt3);
  }
  
  public void glStencilMask(int paramInt)
  {
    this.a.glStencilMask(paramInt);
  }
  
  public void glStencilOp(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glStencilOp(paramInt1, paramInt2, paramInt3);
  }
  
  public void glTexCoordPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
  {
    this.a.glTexCoordPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
  }
  
  public void glTexEnvf(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a.glTexEnvf(paramInt1, paramInt2, paramFloat);
  }
  
  public void glTexEnvfv(int paramInt1, int paramInt2, FloatBuffer paramFloatBuffer)
  {
    this.a.glTexEnvfv(paramInt1, paramInt2, paramFloatBuffer);
  }
  
  public void glTexEnvfv(int paramInt1, int paramInt2, float[] paramArrayOfFloat, int paramInt3)
  {
    this.a.glTexEnvfv(paramInt1, paramInt2, paramArrayOfFloat, paramInt3);
  }
  
  public void glTexEnvx(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glTexEnvx(paramInt1, paramInt2, paramInt3);
  }
  
  public void glTexEnvxv(int paramInt1, int paramInt2, IntBuffer paramIntBuffer)
  {
    this.a.glTexEnvxv(paramInt1, paramInt2, paramIntBuffer);
  }
  
  public void glTexEnvxv(int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
  {
    this.a.glTexEnvxv(paramInt1, paramInt2, paramArrayOfInt, paramInt3);
  }
  
  public void glTexImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
  {
    this.a.glTexImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
  }
  
  public void glTexParameterf(int paramInt1, int paramInt2, float paramFloat)
  {
    this.a.glTexParameterf(paramInt1, paramInt2, paramFloat);
  }
  
  public void glTexParameterx(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glTexParameterx(paramInt1, paramInt2, paramInt3);
  }
  
  public void glTexSubImage2D(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, Buffer paramBuffer)
  {
    this.a.glTexSubImage2D(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramInt8, paramBuffer);
  }
  
  public void glTranslatef(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.a.glTranslatef(paramFloat1, paramFloat2, paramFloat3);
  }
  
  public void glTranslatex(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.glTranslatex(paramInt1, paramInt2, paramInt3);
  }
  
  public void glVertexPointer(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
  {
    this.a.glVertexPointer(paramInt1, paramInt2, paramInt3, paramBuffer);
  }
  
  public void glViewport(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a.glViewport(paramInt1, paramInt2, paramInt3, paramInt4);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\l\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */