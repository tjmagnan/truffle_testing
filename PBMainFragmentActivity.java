package tech.dcube.companion.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.android.volley.VolleyError;
import com.testfairy.TestFairy;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import tech.dcube.companion.HelpFragment;
import tech.dcube.companion.HelpFragment.OnHelpFragmentInteractionListener;
import tech.dcube.companion.MeterSettings;
import tech.dcube.companion.MeterSettings.OnMeterSettingsListener;
import tech.dcube.companion.fragments.CatalogAddItemFragment;
import tech.dcube.companion.fragments.CatalogAddItemFragment.OnAddCatalogItemListener;
import tech.dcube.companion.fragments.CatalogEditItemFragment;
import tech.dcube.companion.fragments.CatalogEditItemFragment.OnEditCatalogListener;
import tech.dcube.companion.fragments.CatalogItemDetailFragment;
import tech.dcube.companion.fragments.CatalogItemDetailFragment.OnCatalogItemListener;
import tech.dcube.companion.fragments.PBBarcodeScanFragment;
import tech.dcube.companion.fragments.PBCatalogHomeFragment;
import tech.dcube.companion.fragments.PBHomeFragment;
import tech.dcube.companion.fragments.PBHomeFragment.HomeInteractionListener;
import tech.dcube.companion.fragments.PBMailParcelClassFragment;
import tech.dcube.companion.fragments.PBMailParcelClassFragment.OnMailParcelClassListener;
import tech.dcube.companion.fragments.PBPackageEnvelopeFragment;
import tech.dcube.companion.fragments.PBPackageEnvelopeFragment.OnPackageEnvelopeSelectListener;
import tech.dcube.companion.fragments.PBPackageLargePackageFragment;
import tech.dcube.companion.fragments.PBPackageLargePackageFragment.OnPackageLargePackageSelectListener;
import tech.dcube.companion.fragments.PBPackageNormalPackageFragment;
import tech.dcube.companion.fragments.PBPackageNormalPackageFragment.OnPackageNormalPackageSelectListener;
import tech.dcube.companion.fragments.PBPackagePMFRBoxFragment;
import tech.dcube.companion.fragments.PBPackagePMFRBoxFragment.OnPackagePMFRBoxListener;
import tech.dcube.companion.fragments.PBPackagePMFREnvelopeFragment;
import tech.dcube.companion.fragments.PBPackagePMFREnvelopeFragment.OnPackagePMFREnvelopeListener;
import tech.dcube.companion.fragments.PBPackageTypeFragment;
import tech.dcube.companion.fragments.PBPackageTypeFragment.OnPackageTypeInteractionListener;
import tech.dcube.companion.fragments.PBReceiptShowFragment;
import tech.dcube.companion.fragments.PBReceiptShowFragment.OnShipmentReceiptListener;
import tech.dcube.companion.fragments.PBRecipientAddressFragment;
import tech.dcube.companion.fragments.PBRecipientAddressFragment.OnRecipientAddressSelectedListener;
import tech.dcube.companion.fragments.PBShipMainFragment;
import tech.dcube.companion.fragments.PBShipMainFragment.OnShipmentFlowListener;
import tech.dcube.companion.fragments.PBShipmentSummaryFragment;
import tech.dcube.companion.fragments.PBShipmentSummaryFragment.OnShipmentSummaryListener;
import tech.dcube.companion.fragments.PBTrackFragment;
import tech.dcube.companion.fragments.PBTrackFragment.OnTrackingListener;
import tech.dcube.companion.fragments.PBTrackingDetailFragment.OnShipmentTrackDetailListener;
import tech.dcube.companion.fragments.ScanViewFinderFragment.OnBarcodeScannedListener;
import tech.dcube.companion.fragments.SettingsFragment.OnSettingsInteractionListener;
import tech.dcube.companion.fragments.ShipItemDetailFragment;
import tech.dcube.companion.fragments.ShipItemDetailFragment.OnShipItemDetailListener;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.PollingHandler;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.BarcodeResponseItem;
import tech.dcube.companion.model.MeterNickname;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBCommandStatus;
import tech.dcube.companion.model.PBCommandStatus.ResponseData;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBParcelPackage;
import tech.dcube.companion.model.PBSendProItem;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentPrintLabel;
import tech.dcube.companion.model.PBShipmentRate;
import tech.dcube.companion.model.PBShipmentService;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

public class PBMainFragmentActivity
  extends AppCompatActivity
  implements TabHost.OnTabChangeListener, PBHomeFragment.HomeInteractionListener, PBTrackingDetailFragment.OnShipmentTrackDetailListener, CatalogItemDetailFragment.OnCatalogItemListener, ShipItemDetailFragment.OnShipItemDetailListener, CatalogEditItemFragment.OnEditCatalogListener, CatalogAddItemFragment.OnAddCatalogItemListener, MeterSettings.OnMeterSettingsListener, SettingsFragment.OnSettingsInteractionListener, HelpFragment.OnHelpFragmentInteractionListener, PBPackageTypeFragment.OnPackageTypeInteractionListener, PBPackagePMFRBoxFragment.OnPackagePMFRBoxListener, PBPackagePMFREnvelopeFragment.OnPackagePMFREnvelopeListener, PBMailParcelClassFragment.OnMailParcelClassListener, ScanViewFinderFragment.OnBarcodeScannedListener, PBRecipientAddressFragment.OnRecipientAddressSelectedListener, PBPackageEnvelopeFragment.OnPackageEnvelopeSelectListener, PBPackageNormalPackageFragment.OnPackageNormalPackageSelectListener, PBPackageLargePackageFragment.OnPackageLargePackageSelectListener, PBShipmentSummaryFragment.OnShipmentSummaryListener, PBReceiptShowFragment.OnShipmentReceiptListener, PBShipMainFragment.OnShipmentFlowListener, PBTrackFragment.OnTrackingListener, ServerManager.PollingHandler
{
  private static final boolean DEBUG_MODE = false;
  private static final boolean DEBUG_MODE_Analytics_ON = true;
  private static final boolean DEBUG_MODE_SEND_PRO = false;
  private static final String FRAGMENT_CATALOG_ADD = CatalogAddItemFragment.class.getName();
  private static final String FRAGMENT_CATALOG_EDIT = CatalogEditItemFragment.class.getName();
  private static final String FRAGMENT_CATALOG_HOME;
  private static final String FRAGMENT_CATALOG_ITEM;
  private static final String FRAGMENT_METER_SETTINGS = MeterSettings.class.getName();
  private static final String FRAGMENT_SHIPMENT_BARCODE;
  private static final String FRAGMENT_SHIPMENT_MAIL_CLASS;
  private static final String FRAGMENT_SHIPMENT_MAIN = PBShipMainFragment.class.getName();
  private static final String FRAGMENT_SHIPMENT_MANUAL;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_ENVELOPE;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_LARGE;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_NORMAL;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_PMFR_BOX;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_PMFR_ENVELOPE;
  private static final String FRAGMENT_SHIPMENT_PACKAGE_TYPE;
  private static final String FRAGMENT_SHIPMENT_PRINT_LABEL;
  private static final String FRAGMENT_SHIPMENT_RECEIPT;
  private static final String FRAGMENT_SHIPMENT_RECIPIENT;
  private static final String FRAGMENT_SHIPMENT_SUMMARY;
  private static final String SETTINGS_LOW_FUND_WARNING_AMOUNT = "_low_fund_warning_amount";
  private static final String SHARED_PREFERENCES_KEY = "COMPANION_APP_SPREF_KEY_";
  private static final String TAG = "PBMainFragActivity: ";
  public static Context mContext;
  String ScannedBarCode;
  boolean isAddCatalogScanBarcode = false;
  boolean isBarcodeShipmentFlow = false;
  private FrameLayout mFadeFL;
  MaterialDialog mLoadingDialog;
  MaterialDialog mMessageDialog;
  View mPrevTab;
  int mPrevTabNo;
  private ProgressBar mProgressBar;
  private FragmentTabHost mTabHost;
  MaterialDialog mYesNoDialog;
  SharedPreferences sharedPreferences;
  private Toolbar toolbar;
  private FrameLayout viewPager;
  
  static
  {
    FRAGMENT_SHIPMENT_BARCODE = PBBarcodeScanFragment.class.getName();
    FRAGMENT_SHIPMENT_MANUAL = PBRecipientAddressFragment.class.getName();
    FRAGMENT_SHIPMENT_RECIPIENT = PBRecipientAddressFragment.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_TYPE = PBPackageType.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_PMFR_BOX = PBPackagePMFRBoxFragment.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_PMFR_ENVELOPE = PBPackagePMFREnvelopeFragment.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_ENVELOPE = PBPackageEnvelopeFragment.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_NORMAL = PBPackageNormalPackageFragment.class.getName();
    FRAGMENT_SHIPMENT_PACKAGE_LARGE = PBPackageLargePackageFragment.class.getName();
    FRAGMENT_SHIPMENT_MAIL_CLASS = PBMailParcelClassFragment.class.getName();
    FRAGMENT_SHIPMENT_SUMMARY = PBShipmentSummaryFragment.class.getName();
    FRAGMENT_SHIPMENT_RECEIPT = PBReceiptShowFragment.class.getName();
    FRAGMENT_SHIPMENT_PRINT_LABEL = PBShipmentPrintLabel.class.getName();
    FRAGMENT_CATALOG_HOME = PBCatalogHomeFragment.class.getName();
    FRAGMENT_CATALOG_ITEM = CatalogItemDetailFragment.class.getName();
  }
  
  private void addTabsView(String paramString1, String paramString2, int paramInt1, int paramInt2)
  {
    LinearLayout localLinearLayout = (LinearLayout)LayoutInflater.from(this).inflate(2130968609, null);
    TextView localTextView = (TextView)localLinearLayout.findViewById(2131624081);
    localTextView.setTextColor(Color.parseColor(paramString2));
    paramString2 = (ImageView)localLinearLayout.findViewById(2131624080);
    localTextView.setText(paramString1);
    paramString2.setImageResource(paramInt1);
  }
  
  private void clearBackStack()
  {
    FragmentManager localFragmentManager = getSupportFragmentManager();
    while (localFragmentManager.getBackStackEntryCount() != 0) {
      localFragmentManager.popBackStackImmediate();
    }
  }
  
  public static void clearCache(Context paramContext, int paramInt)
  {
    Log.i("PBMainFragActivity: ", String.format("Starting cache prune, deleting files older than %d days", new Object[] { Integer.valueOf(paramInt) }));
    Log.i("PBMainFragActivity: ", String.format("Cache pruning completed, %d files deleted", new Object[] { Integer.valueOf(clearCacheFolder(paramContext.getCacheDir(), paramInt)) }));
  }
  
  static int clearCacheFolder(File paramFile, int paramInt)
  {
    int k = 0;
    int m = 0;
    int i = 0;
    j = m;
    if (paramFile != null)
    {
      j = m;
      if (paramFile.isDirectory())
      {
        j = k;
        try
        {
          paramFile = paramFile.listFiles();
          j = k;
          int n = paramFile.length;
          for (m = 0;; m++)
          {
            j = i;
            if (m >= n) {
              break;
            }
            File localFile = paramFile[m];
            k = i;
            j = i;
            if (localFile.isDirectory())
            {
              j = i;
              k = i + clearCacheFolder(localFile, paramInt);
            }
            j = k;
            long l = localFile.lastModified();
            j = k;
            Date localDate = new java/util/Date;
            j = k;
            localDate.<init>();
            i = k;
            j = k;
            if (l < localDate.getTime() - paramInt * 86400000L)
            {
              j = k;
              boolean bool = localFile.delete();
              i = k;
              if (bool) {
                i = k + 1;
              }
            }
          }
          return j;
        }
        catch (Exception paramFile)
        {
          Log.e("PBMainFragActivity: ", String.format("Failed to clean the cache, error %s", new Object[] { paramFile.getMessage() }));
        }
      }
    }
  }
  
  private void setChangeTabIcons(int paramInt)
  {
    setTabsView("Home", "#2214da", 2130837649, 0);
    setTabsView("Track", "#2214da", 2130837648, 1);
    setTabsView("Ship", "#2214da", 2130837655, 2);
    setTabsView("Catalog", "#2214da", 2130837647, 3);
    if (paramInt == 0) {
      setTabsView("Home", "#d3d3d3", 2130837652, 0);
    }
    for (;;)
    {
      return;
      if (paramInt == 1) {
        setTabsView("Track", "#d3d3d3", 2130837651, 1);
      } else if (paramInt == 2) {
        setTabsView("Ship", "#d3d3d3", 2130837653, 2);
      } else if (paramInt == 3) {
        setTabsView("Catalog", "#d3d3d3", 2130837650, 3);
      }
    }
  }
  
  private void setChangeTabIconsWhite()
  {
    setTabsView("Home", "#2214da", 2130837628, 0);
    setTabsView("Alert", "#2214da", 2130837714, 1);
    setTabsView("Reports", "#2214da", 2130837701, 2);
    setTabsView("Address", "#2214da", 2130837606, 3);
  }
  
  private TabHost.TabSpec setIndicator(Context paramContext, TabHost.TabSpec paramTabSpec, int paramInt1, String paramString, int paramInt2)
  {
    paramContext = LayoutInflater.from(paramContext).inflate(2130968710, null);
    TextView localTextView = (TextView)paramContext.findViewById(2131624333);
    ImageView localImageView = (ImageView)paramContext.findViewById(2131624334);
    localTextView.setText(paramString);
    localTextView.setTextColor(getResources().getColor(2131492937));
    localImageView.setImageResource(paramInt2);
    return paramTabSpec.setIndicator(paramContext);
  }
  
  private void setTabsView(String paramString1, String paramString2, int paramInt1, int paramInt2) {}
  
  private void setupTabIcons()
  {
    addTabsView("Home", "#d3d3d3", 2130837649, 0);
    addTabsView("Track", "#2214da", 2130837648, 1);
    addTabsView("Ship", "#2214da", 2130837655, 2);
    addTabsView("Catalog", "#2214da", 2130837647, 3);
  }
  
  private void setupViewPager(ViewPager paramViewPager)
  {
    ViewPagerAdapter localViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
    localViewPagerAdapter.addFrag(new PBHomeFragment(), "Home");
    localViewPagerAdapter.addFrag(new PBTrackFragment(), "Track");
    localViewPagerAdapter.addFrag(new PBShipMainFragment(), "Ship");
    localViewPagerAdapter.addFrag(new PBCatalogHomeFragment(), "Catalog");
    paramViewPager.setAdapter(localViewPagerAdapter);
  }
  
  public void buildLoadingDialog()
  {
    this.mLoadingDialog = new MaterialDialog.Builder(this).customView(2130968704, true).build();
    this.mLoadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    ProgressBar localProgressBar = (ProgressBar)this.mLoadingDialog.getCustomView().findViewById(2131624063);
    if (Build.VERSION.SDK_INT >= 21) {
      localProgressBar.setIndeterminateTintList(ColorStateList.valueOf(getResources().getColor(2131492948)));
    }
    for (;;)
    {
      this.mLoadingDialog.setCancelable(false);
      this.mLoadingDialog.setCanceledOnTouchOutside(false);
      return;
      localProgressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(2131492948), PorterDuff.Mode.SRC_IN);
    }
  }
  
  public void buildMessageDialog()
  {
    this.mMessageDialog = new MaterialDialog.Builder(this).customView(2130968685, true).build();
    this.mMessageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
  }
  
  public void buildYesNoDialog()
  {
    this.mYesNoDialog = new MaterialDialog.Builder(this).customView(2130968712, true).build();
    this.mYesNoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    this.mYesNoDialog.setCancelable(false);
    this.mYesNoDialog.setCanceledOnTouchOutside(false);
  }
  
  void callAddToCatalogApi()
  {
    PBSendProItem localPBSendProItem = DataManager.getInstance().getCurrentShipment().getItem();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local7 = new tech/dcube/companion/activities/PBMainFragmentActivity$7;
      local7.<init>(this);
      localServerManager.saveItem(this, localPBSendProItem, local7);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void callGetAllPackagesV0()
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local8 = new tech/dcube/companion/activities/PBMainFragmentActivity$8;
      local8.<init>(this);
      localServerManager.getAllPackages(this, local8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.d("PackagesV0", " Failed");
        localException.printStackTrace();
        DataManager.getInstance().setPackagesV0Success(false);
      }
    }
  }
  
  void callGetItemFromBarcodeApi(String paramString)
  {
    showLoadingDialog();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local5 = new tech/dcube/companion/activities/PBMainFragmentActivity$5;
      local5.<init>(this);
      localServerManager.getShipmentItemFromBarcode(this, paramString, local5);
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
        hideLoadingDialog();
      }
    }
  }
  
  void callGetItemFromBarcodeApi_Catalog(String paramString)
  {
    showLoadingDialog();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local6 = new tech/dcube/companion/activities/PBMainFragmentActivity$6;
      local6.<init>(this);
      localServerManager.getCatalogItemFromBarcode(this, paramString, local6);
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
        hideLoadingDialog();
      }
    }
  }
  
  void callLowFundAPIs(String paramString1, String paramString2, String paramString3)
  {
    Log.d("Main Fragment", "Low Fund Warning Api");
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local13 = new tech/dcube/companion/activities/PBMainFragmentActivity$13;
      local13.<init>(this);
      localServerManager.updateLowFundsWarning(this, paramString1, paramString2, paramString3, local13);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  void callPostNickNameApi(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local11 = new tech/dcube/companion/activities/PBMainFragmentActivity$11;
      local11.<init>(this);
      localServerManager.postMeterNickname(this, paramString3, paramString2, paramString1, local11);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  void callPrintShipmentReceiptApi()
  {
    showLoadingDialog();
    PBAddress localPBAddress1 = DataManager.getInstance().getCurrentShipment().getSenderAddress();
    PBAddress localPBAddress2 = DataManager.getInstance().getCurrentShipment().getReceiverAddress();
    PBShipmentPackage localPBShipmentPackage = DataManager.getInstance().getCurrentShipment().getShipmentPackage();
    PBShipmentService localPBShipmentService = DataManager.getInstance().getCurrentShipment().getShipmentService();
    PBShipmentRate localPBShipmentRate = DataManager.getInstance().getCurrentShipment().getShipmentRate();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local9 = new tech/dcube/companion/activities/PBMainFragmentActivity$9;
      local9.<init>(this);
      localServerManager.printShipmentLabel(this, localPBAddress1, localPBAddress2, localPBShipmentPackage, localPBShipmentService, localPBShipmentRate, local9);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.d("Print Receipt Api: ", "Failed");
        localException.printStackTrace();
        hideLoadingDialog();
      }
    }
  }
  
  void callPutNickNameApi(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local12 = new tech/dcube/companion/activities/PBMainFragmentActivity$12;
      local12.<init>(this);
      localServerManager.putMeterNickname(this, paramString3, paramString2, paramString1, local12);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  void callUpdateMeterNameApi(String paramString1, String paramString2, String paramString3)
  {
    Log.d("Meter Settings Fragment", "Update Meter Api");
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      ServerManager.RequestCompletionHandler local10 = new tech/dcube/companion/activities/PBMainFragmentActivity$10;
      local10.<init>(this, paramString1, paramString2, paramString3);
      localServerManager.getMeterNickname(this, paramString2, local10);
      return;
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  public void dismissMessageDialog()
  {
    this.mMessageDialog.dismiss();
  }
  
  public void dismissYesNoDialog()
  {
    this.mYesNoDialog.dismiss();
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getApplicationContext(), paramString);
  }
  
  int getTabImageResId(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default: 
      paramInt = -1;
    }
    for (;;)
    {
      return paramInt;
      if (paramBoolean)
      {
        paramInt = 2130837627;
      }
      else
      {
        paramInt = 2130837628;
        continue;
        if (paramBoolean)
        {
          paramInt = 2130837713;
        }
        else
        {
          paramInt = 2130837714;
          continue;
          if (paramBoolean)
          {
            paramInt = 2130837700;
          }
          else
          {
            paramInt = 2130837701;
            continue;
            if (paramBoolean) {
              paramInt = 2130837605;
            } else {
              paramInt = 2130837606;
            }
          }
        }
      }
    }
  }
  
  void hideLoadingDialog()
  {
    try
    {
      this.mLoadingDialog.dismiss();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void hideLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(8);
      this.mFadeFL.setVisibility(8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void highlightCurrTab()
  {
    View localView = this.mTabHost.getCurrentTabView();
    TextView localTextView = (TextView)localView.findViewById(2131624333);
    ImageView localImageView = (ImageView)localView.findViewById(2131624334);
    localTextView.setTextColor(getResources().getColor(2131492939));
    localImageView.setImageResource(getTabImageResId(this.mTabHost.getCurrentTab(), true));
    if (this.mPrevTab != null)
    {
      localTextView = (TextView)this.mPrevTab.findViewById(2131624333);
      localImageView = (ImageView)this.mPrevTab.findViewById(2131624334);
      localTextView.setTextColor(getResources().getColor(2131492937));
      localImageView.setImageResource(getTabImageResId(this.mPrevTabNo, false));
    }
    this.mPrevTab = localView;
    this.mPrevTabNo = this.mTabHost.getCurrentTab();
  }
  
  void launchBarcodeCatalogFragment()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBBarcodeScanFragment(), PBBarcodeScanFragment.class.getName()).addToBackStack(FRAGMENT_SHIPMENT_BARCODE).commit();
  }
  
  void launchBarcodeShipmentFragment()
  {
    if (DataManager.getInstance().getUser().getAuth().isSendProLoggedIn())
    {
      Log.e("Send Pro User", " True");
      this.isBarcodeShipmentFlow = true;
      getSupportFragmentManager().beginTransaction().add(2131624061, new PBBarcodeScanFragment(), PBBarcodeScanFragment.class.getName()).addToBackStack(FRAGMENT_SHIPMENT_BARCODE).commit();
    }
    for (;;)
    {
      return;
      Log.e("Send Pro User", " False");
      displayToast("Log into your Account at  www.pb.com and select SendPro to get an account");
    }
  }
  
  void launchCatalogEditItemFragment() {}
  
  void launchMailServiceFragment()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBMailParcelClassFragment()).addToBackStack(PBMailParcelClassFragment.class.getName()).commit();
  }
  
  void launchManualShipmentFragment()
  {
    if (DataManager.getInstance().getUser().getAuth().isSendProLoggedIn())
    {
      Log.e("Send Pro User", " True");
      this.isBarcodeShipmentFlow = false;
      PBSendProItem localPBSendProItem = new PBSendProItem();
      PBShipment localPBShipment = new PBShipment();
      localPBShipment.setSenderAddress(DataManager.getInstance().getUser().getAddress());
      localPBShipment.setItem(localPBSendProItem);
      localPBShipment.setManualShipment(true);
      DataManager.getInstance().setCurrentShipment(localPBShipment);
      launchSelectRecipientAddressFragment();
    }
    for (;;)
    {
      return;
      Log.e("Send Pro User", " False");
      displayToast("Log into your Account at  www.pb.com and select SendPro to get an account");
    }
  }
  
  void launchPackageTypeFragment()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackageTypeFragment(), PBPackageTypeFragment.class.getName()).addToBackStack(PBPackageTypeFragment.class.getName()).commit();
  }
  
  void launchSelectRecipientAddressFragment()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBRecipientAddressFragment(), PBRecipientAddressFragment.class.getName()).addToBackStack(FRAGMENT_SHIPMENT_RECIPIENT).commit();
  }
  
  void launchShipmentItemDetail(boolean paramBoolean)
  {
    if (paramBoolean) {
      launchShipmentItemDetail_UPCItemFound();
    }
    for (;;)
    {
      return;
      setYesNoDialogMessage("Your Item is not yet in our DB.\nWould you like to add it to your catalog for future shipments?\nIf No use manual ship option.");
      setYesNoDialogNegativeAction(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          PBMainFragmentActivity.this.dismissYesNoDialog();
          PBMainFragmentActivity.this.clearBackStack();
        }
      });
      setYesNoDialogPositiveAction(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          PBMainFragmentActivity.this.launchShipmentItemDetail_UPCItemNotFound();
          PBMainFragmentActivity.this.dismissYesNoDialog();
        }
      });
      showYesNoDialog();
    }
  }
  
  void launchShipmentItemDetail_UPCItemFound()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new ShipItemDetailFragment(), ShipItemDetailFragment.class.getName()).addToBackStack(ShipItemDetailFragment.class.getName()).commit();
  }
  
  void launchShipmentItemDetail_UPCItemNotFound()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, ShipItemDetailFragment.newInstance("", "", 0.0D, 0.0D, 0.0D, 0.0D, this.ScannedBarCode, "", false, ShipItemDetailFragment.class.getName()), ShipItemDetailFragment.class.getName()).addToBackStack(ShipItemDetailFragment.class.getName()).commit();
  }
  
  void launchShipmentReceiptFragment(PBShipmentPrintLabel paramPBShipmentPrintLabel)
  {
    PBShipment localPBShipment = DataManager.getInstance().getCurrentShipment();
    localPBShipment.setPrintLabel(paramPBShipmentPrintLabel);
    DataManager.getInstance().setCurrentShipment(localPBShipment);
    clearBackStack();
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBReceiptShowFragment()).addToBackStack(PBReceiptShowFragment.class.getName()).commit();
  }
  
  void launchShipmentSummaryFragment()
  {
    getSupportFragmentManager().beginTransaction().add(2131624061, new PBShipmentSummaryFragment()).addToBackStack(PBShipmentSummaryFragment.class.getName()).commit();
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onAddCatalogInteraction(String paramString)
  {
    this.isAddCatalogScanBarcode = false;
    if (paramString.equals("action_save_button"))
    {
      clearBackStack();
      updateCatalogListView();
    }
    for (;;)
    {
      return;
      if (paramString.equals("action_cancel_button"))
      {
        clearBackStack();
      }
      else if (paramString.equals("action_scan_barcode_button"))
      {
        Log.e("Add Catalog", "Scan Barcode Initiated");
        this.isAddCatalogScanBarcode = true;
        launchBarcodeCatalogFragment();
      }
    }
  }
  
  public void onBackPressed()
  {
    if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
      finish();
    }
    for (;;)
    {
      return;
      getSupportFragmentManager().popBackStackImmediate();
    }
  }
  
  public void onBarcodeScanResult(String paramString)
  {
    this.ScannedBarCode = paramString;
    Log.e("BarCode", " Scan: " + this.ScannedBarCode);
    if (!this.isAddCatalogScanBarcode) {
      callGetItemFromBarcodeApi(paramString);
    }
    for (;;)
    {
      return;
      Log.wtf("Add Catalog", " Item Barcode Scan" + this.ScannedBarCode);
      onBackPressed();
      callGetItemFromBarcodeApi_Catalog(paramString);
    }
  }
  
  public void onCatalogItemDetailInteraction(String paramString, int paramInt)
  {
    if (paramString.equals("action_ship_button")) {
      if (DataManager.getInstance().getUser().getAuth().isSendProLoggedIn())
      {
        Log.e("Send Pro User", " True");
        PBSendProItem localPBSendProItem = (PBSendProItem)DataManager.getInstance().getItems().get(paramInt);
        this.isBarcodeShipmentFlow = false;
        paramString = new PBShipment();
        paramString.setSenderAddress(DataManager.getInstance().getUser().getAddress());
        paramString.setItem(localPBSendProItem);
        paramString.setManualShipment(false);
        DataManager.getInstance().setCurrentShipment(paramString);
        launchSelectRecipientAddressFragment();
      }
    }
    for (;;)
    {
      return;
      Log.e("Send Pro User", " False");
      displayToast("Log into Your Account at  www.pb.com and select SendPro to get an account");
      continue;
      if (paramString.equals("action_back_button"))
      {
        clearBackStack();
        updateCatalogListView();
      }
      else if (!paramString.equals("action_edit_button")) {}
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968604);
    this.viewPager = ((FrameLayout)findViewById(2131624061));
    this.mProgressBar = ((ProgressBar)findViewById(2131624063));
    this.mFadeFL = ((FrameLayout)findViewById(2131624062));
    TestFairy.begin(this, "5102b0e1a99e5b59265f9b6e7055e81ad7341799");
    paramBundle = AccountManager.get(this).getAccounts();
    int j = paramBundle.length;
    int i = 0;
    if (i < j)
    {
      Object localObject = paramBundle[i];
      if (Patterns.EMAIL_ADDRESS.matcher(((Account)localObject).name).matches()) {
        TestFairy.setUserId(((Account)localObject).name);
      }
    }
    else
    {
      buildLoadingDialog();
      buildMessageDialog();
      buildYesNoDialog();
      if (Build.VERSION.SDK_INT < 21) {
        break label381;
      }
      this.mProgressBar.setIndeterminateTintList(ColorStateList.valueOf(getResources().getColor(2131492943)));
    }
    for (;;)
    {
      getWindow().getDecorView().setSystemUiVisibility(1280);
      if (Build.VERSION.SDK_INT >= 21) {
        getWindow().setStatusBarColor(Color.parseColor("#55000000"));
      }
      mContext = this;
      this.mTabHost = ((FragmentTabHost)findViewById(16908306));
      setupTabIcons();
      this.mTabHost.setup(this, getSupportFragmentManager(), 2131624061);
      this.mTabHost.addTab(setIndicator(this, this.mTabHost.newTabSpec("HOME"), 2131492896, "Home", 2130837628), PBHomeFragment.class, null);
      this.mTabHost.addTab(setIndicator(this, this.mTabHost.newTabSpec("TRACK"), 2131492896, "Track", 2130837714), PBTrackFragment.class, null);
      this.mTabHost.addTab(setIndicator(this, this.mTabHost.newTabSpec("SHIP"), 2131492896, "Ship", 2130837701), PBShipMainFragment.class, null);
      this.mTabHost.addTab(setIndicator(this, this.mTabHost.newTabSpec("CATALOG"), 2131492896, "Catalog", 2130837606), PBCatalogHomeFragment.class, null);
      this.mTabHost.setOnTabChangedListener(this);
      showLoadingDialog();
      highlightCurrTab();
      return;
      i++;
      break;
      label381:
      this.mProgressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(2131492943), PorterDuff.Mode.SRC_IN);
    }
  }
  
  public void onEditCatalogInteraction(String paramString, int paramInt)
  {
    if (paramString.equals("action_save_button"))
    {
      onBackPressed();
      updateCatalogItemDetailLayout();
    }
    for (;;)
    {
      return;
      if (paramString.equals("action_cancel_button")) {
        onBackPressed();
      }
    }
  }
  
  public void onGetAllPackagesV0()
  {
    callGetAllPackagesV0();
  }
  
  public void onHelpClicked()
  {
    FragmentTransaction localFragmentTransaction = getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.add(2131624061, HelpFragment.newInstance());
    localFragmentTransaction.addToBackStack(null);
    localFragmentTransaction.commit();
  }
  
  public void onHelpInteraction(String paramString)
  {
    if (paramString.equals("back")) {
      clearBackStack();
    }
  }
  
  public void onHomeShowMessageDialog(String paramString1, String paramString2)
  {
    if (paramString1.equals("message_sendpro_add_fund"))
    {
      setMessageDialogMessage(paramString2);
      setMessageDialogButtonText("OK");
      setMessageDialogOnActionClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          PBMainFragmentActivity.this.dismissMessageDialog();
        }
      });
      showMessageDialog();
    }
    for (;;)
    {
      return;
      if (paramString1.equals("message_smartlink_add_fund"))
      {
        setMessageDialogMessage(paramString2);
        setMessageDialogButtonText("OK");
        setMessageDialogOnActionClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            PBMainFragmentActivity.this.dismissMessageDialog();
          }
        });
        showMessageDialog();
      }
    }
  }
  
  public void onLoadingDataFromServer(boolean paramBoolean)
  {
    if (paramBoolean) {
      showLoadingDialog();
    }
    for (;;)
    {
      return;
      hideLoadingDialog();
    }
  }
  
  public void onLogOutButtonClicked()
  {
    Log.e("User: ", "Logout");
    ServerManager.getInstance().logout(getApplicationContext());
    clearCache(this, 0);
    deleteDatabase("webview.db");
    deleteDatabase("webviewCache.db");
    Intent localIntent = new Intent(this, PBSigninActivity.class);
    finish();
    startActivity(localIntent);
  }
  
  public void onMailParcelClassSelected(String paramString, PBShipmentService paramPBShipmentService)
  {
    paramString = DataManager.getInstance().getCurrentShipment();
    paramString.setShipmentService(paramPBShipmentService);
    DataManager.getInstance().setCurrentShipment(paramString);
    launchShipmentSummaryFragment();
  }
  
  public void onMeterRefillPollingComplete(String paramString1, String paramString2, PBCommandStatus paramPBCommandStatus)
  {
    Log.wtf("Polling Done", paramPBCommandStatus.toString());
    String str2 = (String)DataManager.getInstance().getMeterSerialToNickName().get(paramString2);
    String str1 = str2;
    if (str2.equals("")) {
      str1 = paramString1 + " " + paramString2;
    }
    setMessageDialogMessage(str1 + "\n" + paramPBCommandStatus.getResponseData().getMessage());
    showMessageDialog();
    updateHomeScreenFullLayout();
  }
  
  public void onMeterSettingsInteraction(String paramString)
  {
    if (paramString.equals("back"))
    {
      clearBackStack();
      this.mTabHost.setCurrentTab(1);
      this.mTabHost.setCurrentTab(0);
    }
  }
  
  public void onMeterSettingsLowFundsChanged(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    callLowFundAPIs(paramString1, paramString2, paramString3);
  }
  
  public void onMeterSettingsNameChanged(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    callUpdateMeterNameApi(paramString1, paramString2, paramString4);
  }
  
  public void onPMFRBoxSelected(PBShipmentPackage paramPBShipmentPackage)
  {
    PBShipment localPBShipment = DataManager.getInstance().getCurrentShipment();
    localPBShipment.setShipmentPackage(paramPBShipmentPackage);
    DataManager.getInstance().setCurrentShipment(localPBShipment);
    launchMailServiceFragment();
  }
  
  public void onPMFREnvelopeSelected(PBShipmentPackage paramPBShipmentPackage)
  {
    PBShipment localPBShipment = DataManager.getInstance().getCurrentShipment();
    localPBShipment.setShipmentPackage(paramPBShipmentPackage);
    DataManager.getInstance().setCurrentShipment(localPBShipment);
    launchMailServiceFragment();
  }
  
  public void onPackageEnvelopeSelected(String paramString, PBShipmentPackage paramPBShipmentPackage)
  {
    paramString = DataManager.getInstance().getCurrentShipment();
    paramString.setShipmentPackage(paramPBShipmentPackage);
    DataManager.getInstance().setCurrentShipment(paramString);
    launchMailServiceFragment();
  }
  
  public void onPackageLargeSelected(String paramString, PBShipmentPackage paramPBShipmentPackage)
  {
    paramString = DataManager.getInstance().getCurrentShipment();
    paramString.setShipmentPackage(paramPBShipmentPackage);
    DataManager.getInstance().setCurrentShipment(paramString);
    launchMailServiceFragment();
  }
  
  public void onPackageNormalSelected(String paramString, PBShipmentPackage paramPBShipmentPackage)
  {
    paramString = DataManager.getInstance().getCurrentShipment();
    paramString.setShipmentPackage(paramPBShipmentPackage);
    DataManager.getInstance().setCurrentShipment(paramString);
    launchMailServiceFragment();
  }
  
  public void onPackageTypeSelectInteraction(PBPackageType paramPBPackageType) {}
  
  protected void onPause()
  {
    super.onPause();
  }
  
  public void onRecipientAddressButtonInteraction(String paramString)
  {
    if (paramString.equals("action_back_button"))
    {
      clearBackStack();
      if (this.isBarcodeShipmentFlow) {
        launchBarcodeShipmentFragment();
      }
    }
  }
  
  public void onRecipientAddressSelected(PBAddress paramPBAddress)
  {
    PBShipment localPBShipment = DataManager.getInstance().getCurrentShipment();
    localPBShipment.setReceiverAddress(paramPBAddress);
    DataManager.getInstance().setCurrentShipment(localPBShipment);
    launchPackageTypeFragment();
  }
  
  protected void onResume()
  {
    super.onResume();
  }
  
  public void onSettingsClicked()
  {
    FragmentTransaction localFragmentTransaction = getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.add(2131624061, MeterSettings.newInstance());
    localFragmentTransaction.addToBackStack(null);
    localFragmentTransaction.commit();
  }
  
  public void onSettingsShowLoadingDialog(boolean paramBoolean)
  {
    if (paramBoolean) {
      showLoadingDialog();
    }
    for (;;)
    {
      return;
      hideLoadingDialog();
    }
  }
  
  public void onShipItemDetailInteraction(String paramString, boolean paramBoolean)
  {
    if (paramString.equals("action_ship"))
    {
      if (paramBoolean) {
        callAddToCatalogApi();
      }
      launchSelectRecipientAddressFragment();
    }
    for (;;)
    {
      return;
      if (paramString.equals("action_rescan_barcode"))
      {
        clearBackStack();
        launchBarcodeShipmentFragment();
      }
    }
  }
  
  public void onShipmentFlowSelected(String paramString)
  {
    if (paramString.equals("action_barcode_shipment_flow"))
    {
      this.isAddCatalogScanBarcode = false;
      launchBarcodeShipmentFragment();
    }
    for (;;)
    {
      return;
      if (paramString.equals("action_manual_shipment_flow"))
      {
        this.isAddCatalogScanBarcode = false;
        launchManualShipmentFragment();
      }
    }
  }
  
  public void onShipmentRateLoaded(PBShipmentRate paramPBShipmentRate)
  {
    PBShipment localPBShipment = DataManager.getInstance().getCurrentShipment();
    localPBShipment.setShipmentRate(paramPBShipmentRate);
    DataManager.getInstance().setCurrentShipment(localPBShipment);
  }
  
  public void onShipmentReceipt(String paramString)
  {
    if (paramString.equals("action_done_button")) {
      clearBackStack();
    }
  }
  
  public void onShipmentSummary(String paramString)
  {
    if (paramString.equals("action_back_button")) {}
    for (;;)
    {
      return;
      if (paramString.equals("action_cancel_button")) {
        clearBackStack();
      } else if (paramString.equals("action_pay_with_send_pro")) {
        callPrintShipmentReceiptApi();
      }
    }
  }
  
  public void onTabChanged(String paramString)
  {
    highlightCurrTab();
    clearBackStack();
  }
  
  public void onTrackListLoading(String paramString) {}
  
  public void onTrackingDetailInteraction(String paramString)
  {
    if (paramString.equals("action_back_button"))
    {
      clearBackStack();
      updateTrackingList();
    }
  }
  
  void saveLowFundWarningPreferences(String paramString1, String paramString2, String paramString3)
  {
    this.sharedPreferences = getSharedPreferences("COMPANION_APP_SPREF_KEY_", 0);
    paramString1 = paramString1 + "_" + paramString2 + "_low_fund_warning_amount";
    paramString2 = this.sharedPreferences.edit();
    paramString2.putString(paramString1, paramString3);
    paramString2.commit();
  }
  
  public void setMessageDialogButtonText(String paramString)
  {
    ((Button)this.mMessageDialog.getView().findViewById(2131624300)).setText(paramString);
  }
  
  public void setMessageDialogMessage(String paramString)
  {
    ((TextView)this.mMessageDialog.getView().findViewById(2131624299)).setText(paramString);
  }
  
  public void setMessageDialogOnActionClickListener(View.OnClickListener paramOnClickListener)
  {
    this.mMessageDialog.getCustomView().findViewById(2131624300).setOnClickListener(paramOnClickListener);
  }
  
  public void setYesNoDialogMessage(String paramString)
  {
    ((TextView)this.mYesNoDialog.getView().findViewById(2131624299)).setText(paramString);
  }
  
  public void setYesNoDialogNegativeAction(View.OnClickListener paramOnClickListener)
  {
    this.mYesNoDialog.getCustomView().findViewById(2131624339).setOnClickListener(paramOnClickListener);
  }
  
  public void setYesNoDialogPositiveAction(View.OnClickListener paramOnClickListener)
  {
    this.mYesNoDialog.getCustomView().findViewById(2131624338).setOnClickListener(paramOnClickListener);
  }
  
  void showLoadingDialog()
  {
    try
    {
      this.mLoadingDialog.show();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void showLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(0);
      this.mFadeFL.setVisibility(0);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void showMessageDialog()
  {
    this.mMessageDialog.show();
  }
  
  public void showYesNoDialog()
  {
    this.mYesNoDialog.show();
  }
  
  void startBarcodeShipment(PBSendProItem paramPBSendProItem, boolean paramBoolean)
  {
    PBShipment localPBShipment;
    if (paramBoolean)
    {
      localPBShipment = new PBShipment();
      localPBShipment.setSenderAddress(DataManager.getInstance().getUser().getAddress());
      localPBShipment.setItem(paramPBSendProItem);
      localPBShipment.setManualShipment(false);
      DataManager.getInstance().setCurrentShipment(localPBShipment);
      launchShipmentItemDetail(true);
    }
    for (;;)
    {
      return;
      localPBShipment = new PBShipment();
      paramPBSendProItem = new PBSendProItem();
      localPBShipment.setSenderAddress(DataManager.getInstance().getUser().getAddress());
      localPBShipment.setItem(paramPBSendProItem);
      localPBShipment.setManualShipment(false);
      DataManager.getInstance().setCurrentShipment(localPBShipment);
      launchShipmentItemDetail(false);
    }
  }
  
  void updateCatalogAddItemLayout(PBSendProItem paramPBSendProItem, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (;;)
    {
      try
      {
        CatalogAddItemFragment localCatalogAddItemFragment = (CatalogAddItemFragment)getSupportFragmentManager().findFragmentById(2131624061);
        if (localCatalogAddItemFragment != null)
        {
          Log.wtf("Add Item Fragment", " Found Hurray");
          localCatalogAddItemFragment.updateBarcodeValue(paramPBSendProItem, this.ScannedBarCode);
          return;
        }
      }
      catch (Exception paramPBSendProItem)
      {
        paramPBSendProItem.printStackTrace();
        continue;
      }
      Log.wtf("Add Item Fragment", " Not Found");
      continue;
      try
      {
        paramPBSendProItem = (CatalogAddItemFragment)getSupportFragmentManager().findFragmentById(2131624061);
        if (paramPBSendProItem == null) {
          break label110;
        }
        Log.wtf("Add Item Fragment", " Found Hurray");
        paramPBSendProItem.updateBarcodeValue(this.ScannedBarCode);
      }
      catch (Exception paramPBSendProItem)
      {
        paramPBSendProItem.printStackTrace();
      }
      continue;
      label110:
      Log.wtf("Add Item Fragment", " Not Found");
    }
  }
  
  public void updateCatalogItemDetailLayout()
  {
    try
    {
      CatalogItemDetailFragment localCatalogItemDetailFragment = (CatalogItemDetailFragment)getSupportFragmentManager().findFragmentById(2131624061);
      if (localCatalogItemDetailFragment != null) {
        localCatalogItemDetailFragment.updateItemDetailLayout();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void updateCatalogListView()
  {
    try
    {
      PBCatalogHomeFragment localPBCatalogHomeFragment = (PBCatalogHomeFragment)getSupportFragmentManager().findFragmentById(2131624061);
      if (localPBCatalogHomeFragment != null) {
        localPBCatalogHomeFragment.updateCatalogList();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void updateHomeScreenFullLayout()
  {
    try
    {
      PBHomeFragment localPBHomeFragment = (PBHomeFragment)getSupportFragmentManager().findFragmentById(2131624061);
      if (localPBHomeFragment != null) {
        localPBHomeFragment.updateHomeFullLayout();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void updateHomeScreenLayout()
  {
    try
    {
      PBHomeFragment localPBHomeFragment = (PBHomeFragment)getSupportFragmentManager().findFragmentById(2131624061);
      if (localPBHomeFragment != null) {
        localPBHomeFragment.updateHomeLayout();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void updateTrackingList()
  {
    for (;;)
    {
      try
      {
        PBTrackFragment localPBTrackFragment = (PBTrackFragment)getSupportFragmentManager().findFragmentById(2131624061);
        if (localPBTrackFragment != null)
        {
          Log.wtf("PBTrackFragment", " Found Hurray");
          localPBTrackFragment.updateTrackingLayout();
          return;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        continue;
      }
      Log.wtf("PBTrackFragment", " Not Found");
    }
  }
  
  public void yesNoDialogSetNegativeButtonText(String paramString)
  {
    ((Button)this.mMessageDialog.getView().findViewById(2131624339)).setText(paramString);
  }
  
  public void yesNoDialogSetPostiveButtonText(String paramString)
  {
    ((Button)this.mMessageDialog.getView().findViewById(2131624338)).setText(paramString);
  }
  
  class ViewPagerAdapter
    extends FragmentPagerAdapter
  {
    private final List<Fragment> mFragmentList = new ArrayList();
    private final List<String> mFragmentTitleList = new ArrayList();
    
    public ViewPagerAdapter(FragmentManager paramFragmentManager)
    {
      super();
    }
    
    public void addFrag(Fragment paramFragment, String paramString)
    {
      this.mFragmentList.add(paramFragment);
      this.mFragmentTitleList.add(paramString);
    }
    
    public int getCount()
    {
      return this.mFragmentList.size();
    }
    
    public Fragment getItem(int paramInt)
    {
      return (Fragment)this.mFragmentList.get(paramInt);
    }
    
    public CharSequence getPageTitle(int paramInt)
    {
      return (CharSequence)this.mFragmentTitleList.get(paramInt);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\activities\PBMainFragmentActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */