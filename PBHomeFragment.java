package tech.dcube.companion.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.android.volley.VolleyError;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.activities.PBMainFragmentActivity;
import tech.dcube.companion.adapters.HomeMetersAdapter;
import tech.dcube.companion.adapters.HomeSendProAdapter;
import tech.dcube.companion.listeners.MeterInteractionListener;
import tech.dcube.companion.listeners.SendProInteractionListener;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.MeterNickname;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBDevice;
import tech.dcube.companion.model.PBDeviceNotification;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;
import tech.dcube.companion.model.SendProItem;

public class PBHomeFragment
  extends PBTopBarFragment
{
  private static final String SETTINGS_FUNDS_LOW = "_funds_low";
  private static final String SETTINGS_LOW_INK_LEVEL = "_low_ink_level";
  private static final String SETTINGS_METER_ERROR = "_low_ink_level";
  private static final String SETTINGS_SOFTWARE_RATE_UPDATES = "_software_rate_updates";
  private static final String SHARED_PREFERENCES_KEY = "COMPANION_APP_SPREF_KEY_";
  TextView ErrorMessage;
  TextView ErrorTitle;
  String SendproMessageFailed = "Your payment was not successful.";
  String SendproMessageSuccess = "Your payment was successful.";
  String SmartLinkMessageFailed = "Your payment was not  submitted.";
  String SmartLinkMessageSuccess = "Your payment has been submitted, please check your notification for confirmation.";
  TextView WarningMessage;
  TextView WarningTitle;
  private MaterialDialog addFundsSendProDialog;
  private MaterialDialog addFundsSmartLinkDialog;
  private TextView addFunds_ErrorMsg;
  public boolean isSendProUser = false;
  public boolean isSmartLinkUser = false;
  LinearLayout layoutSendPro;
  LinearLayout layoutSmartLink;
  int loadingLock = 0;
  boolean low_ink_level = true;
  ImageButton mHelpButton;
  LinearLayout mHelpButtonLayout;
  HomeInteractionListener mListener;
  ImageButton mMeterSettingsButton;
  RecyclerView mMetersList;
  RecyclerView mSendProList;
  private MaterialDialog meterErrorDialog;
  String meterFunds_AddAmount = "0.0";
  List<PBDevice> meterItems;
  private MaterialDialog meterWarningDialog;
  boolean meter_error = true;
  boolean meter_funds_low = true;
  HomeMetersAdapter metersAdapter;
  private ProgressBar progressBar_SL;
  private ProgressBar progressBar_SP;
  private EditText refilFundSLEditText;
  private EditText refilFundSPEditText;
  private PBDevice selectedDevice;
  HomeSendProAdapter sendProAdapter;
  String sendProFunds_AddAmount = "0.0";
  List<SendProItem> sendProItems;
  SharedPreferences sharedPreferences;
  boolean software_rate_update = true;
  
  private void addFundsToSelectedDevice()
  {
    try
    {
      double d = Double.parseDouble(this.meterFunds_AddAmount);
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      String str1 = this.selectedDevice.getModelNumber();
      String str2 = this.selectedDevice.getSerialNumber();
      ServerManager.RequestCompletionHandler local12 = new tech/dcube/companion/fragments/PBHomeFragment$12;
      local12.<init>(this);
      localServerManager.addSmartLinkFunds(localContext, str1, str2, Double.valueOf(d), local12, (PBMainFragmentActivity)getActivity());
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("Home: ", "Add Meter Funds: Failed");
        localException.printStackTrace();
        this.addFundsSmartLinkDialog.dismiss();
        showSmartLinkAddFundsMessage(false);
      }
    }
  }
  
  private void addFundsToSendProAccount()
  {
    try
    {
      double d = Double.parseDouble(this.sendProFunds_AddAmount);
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local13 = new tech/dcube/companion/fragments/PBHomeFragment$13;
      local13.<init>(this);
      localServerManager.addSendProFunds(localContext, Double.valueOf(d), local13);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("Home: ", "Add Send Pro Funds: Failed");
        localException.printStackTrace();
        this.addFundsSendProDialog.dismiss();
        showSendproAddFundsMessage(false);
      }
    }
  }
  
  void callGetAllDevicesApi()
  {
    getLoadingLock();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local7 = new tech/dcube/companion/fragments/PBHomeFragment$7;
      local7.<init>(this);
      localServerManager.getAllDevices(localContext, local7);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        releaseLoadingLock();
      }
    }
  }
  
  void callGetMeterNotificationApi()
  {
    if (!this.isSmartLinkUser) {}
    for (;;)
    {
      return;
      Object localObject2 = DataManager.getInstance().getDevices().iterator();
      Object localObject3;
      Object localObject1;
      while (((Iterator)localObject2).hasNext())
      {
        localObject3 = (PBDevice)((Iterator)localObject2).next();
        localObject1 = ((PBDevice)localObject3).getModelNumber();
        localObject3 = ((PBDevice)localObject3).getSerialNumber();
        Boolean localBoolean = new Boolean(getLowInkLevelPreference((String)localObject1, (String)localObject3));
        DataManager.getInstance().getMeterModelSerialShowNotification().put((String)localObject1 + (String)localObject3, localBoolean);
      }
      try
      {
        localObject3 = ServerManager.getInstance();
        localObject2 = getContext();
        localObject1 = new tech/dcube/companion/fragments/PBHomeFragment$10;
        ((10)localObject1).<init>(this);
        ((ServerManager)localObject3).getNotifications((Context)localObject2, (ServerManager.RequestCompletionHandler)localObject1);
      }
      catch (Exception localException)
      {
        Log.e("Home: ", "Get Devices Notification: Failed");
        localException.printStackTrace();
      }
    }
  }
  
  void callGetMeterSubscriptionApi()
  {
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local11 = new tech/dcube/companion/fragments/PBHomeFragment$11;
      local11.<init>(this);
      localServerManager.getMeterSubscriptions(localContext, local11);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        Log.wtf("Home: ", "Get Subscriptions: Failed");
      }
    }
  }
  
  void callGetSendproFunds()
  {
    getLoadingLock();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local8 = new tech/dcube/companion/fragments/PBHomeFragment$8;
      local8.<init>(this);
      localServerManager.getSendProFunds(localContext, local8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("Home: ", "Sendpro Funds: Failed");
        localException.printStackTrace();
        releaseLoadingLock();
      }
    }
  }
  
  void callGetUserAddressApi()
  {
    getLoadingLock();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local9 = new tech/dcube/companion/fragments/PBHomeFragment$9;
      local9.<init>(this);
      localServerManager.getUserAddress(localContext, local9);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        releaseLoadingLock();
      }
    }
  }
  
  void callMeterAPIs()
  {
    this.isSmartLinkUser = DataManager.getInstance().getUser().getAuth().isSmartLinkLoggedIn();
    Log.e("Home: ", "PBUser Smart Link " + this.isSmartLinkUser);
    if (this.isSmartLinkUser) {
      callGetAllDevicesApi();
    }
  }
  
  public void callSendproAPIs()
  {
    this.isSendProUser = DataManager.getInstance().getUser().getAuth().isSendProLoggedIn();
    Log.e("Home: ", "PBUser Send Pro " + this.isSendProUser);
    if (this.isSendProUser)
    {
      callGetUserAddressApi();
      callGetSendproFunds();
    }
  }
  
  void callServerAPIs()
  {
    callMeterAPIs();
    callSendproAPIs();
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  String formatErrorMessage(String paramString1, String paramString2, String paramString3)
  {
    localObject1 = paramString1 + paramString2;
    paramString1 = "";
    paramString2 = paramString1;
    try
    {
      Iterator localIterator = ((List)DataManager.getInstance().getMeterNotificationList().get(localObject1)).iterator();
      for (;;)
      {
        label49:
        paramString2 = paramString1;
        localObject1 = paramString1;
        if (!localIterator.hasNext()) {
          break;
        }
        paramString2 = paramString1;
        localObject1 = (PBDeviceNotification)localIterator.next();
        paramString2 = paramString1;
        if (((PBDeviceNotification)localObject1).getNotificationType().equals(paramString3))
        {
          paramString2 = paramString1;
          Object localObject2 = ((PBDeviceNotification)localObject1).getNotificationType();
          int i = -1;
          paramString2 = paramString1;
          switch (((String)localObject2).hashCode())
          {
          }
          for (;;)
          {
            switch (i)
            {
            default: 
              paramString2 = paramString1;
              localObject2 = new java/lang/StringBuilder;
              paramString2 = paramString1;
              ((StringBuilder)localObject2).<init>();
              paramString2 = paramString1;
              paramString1 = ((PBDeviceNotification)localObject1).getNotificationType() + " " + ((PBDeviceNotification)localObject1).getNotificationLevel();
              break label49;
              paramString2 = paramString1;
              if (((String)localObject2).equals("SUPPLY"))
              {
                i = 0;
                continue;
                paramString2 = paramString1;
                if (((String)localObject2).equals("FUND"))
                {
                  i = 1;
                  continue;
                  paramString2 = paramString1;
                  if (((String)localObject2).equals("DEVICE")) {
                    i = 2;
                  }
                }
              }
              break;
            }
          }
          paramString2 = paramString1;
          localObject2 = new java/lang/StringBuilder;
          paramString2 = paramString1;
          ((StringBuilder)localObject2).<init>();
          paramString2 = paramString1;
          paramString1 = ((PBDeviceNotification)localObject1).getSupplyLevel() + " " + ((PBDeviceNotification)localObject1).getSupplyType() + " " + ((PBDeviceNotification)localObject1).getNotificationLevel();
          continue;
          paramString2 = paramString1;
          localObject2 = new java/lang/StringBuilder;
          paramString2 = paramString1;
          ((StringBuilder)localObject2).<init>();
          paramString2 = paramString1;
          paramString1 = ((PBDeviceNotification)localObject1).getFundLevel() + " " + ((PBDeviceNotification)localObject1).getNotificationType() + " " + ((PBDeviceNotification)localObject1).getNotificationLevel();
          continue;
          paramString2 = paramString1;
          localObject2 = new java/lang/StringBuilder;
          paramString2 = paramString1;
          ((StringBuilder)localObject2).<init>();
          paramString2 = paramString1;
          paramString1 = ((PBDeviceNotification)localObject1).getNotificationType() + " " + ((PBDeviceNotification)localObject1).getNotificationLevel() + " CODE " + ((PBDeviceNotification)localObject1).getDeviceErrorCode();
        }
      }
      return (String)localObject1;
    }
    catch (Exception paramString1)
    {
      paramString1.printStackTrace();
      localObject1 = paramString2;
    }
  }
  
  void getLoadingLock()
  {
    this.loadingLock += 1;
    onShowLoadingDialog(true);
  }
  
  boolean getLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.low_ink_level = this.sharedPreferences.getBoolean(paramString1, true);
    return this.low_ink_level;
  }
  
  boolean getMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.meter_error = this.sharedPreferences.getBoolean(paramString1, true);
    return this.meter_error;
  }
  
  boolean getMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    this.meter_funds_low = this.sharedPreferences.getBoolean(paramString1, true);
    return this.meter_funds_low;
  }
  
  String getMeterNickName(String paramString)
  {
    String str2 = (String)DataManager.getInstance().getMeterSerialToNickName().get(paramString);
    String str1 = str2;
    if (str2.equals("")) {
      str1 = paramString;
    }
    return str1;
  }
  
  void getPreferences(String paramString1, String paramString2)
  {
    String str3 = paramString1 + "_" + paramString2 + "_low_ink_level";
    String str2 = paramString1 + "_" + paramString2 + "_funds_low";
    String str1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.low_ink_level = this.sharedPreferences.getBoolean(str3, true);
    this.meter_funds_low = this.sharedPreferences.getBoolean(str2, true);
    this.meter_error = this.sharedPreferences.getBoolean(str1, true);
    this.software_rate_update = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  boolean getSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.software_rate_update = this.sharedPreferences.getBoolean(paramString1, true);
    return this.software_rate_update;
  }
  
  public void onClickTopBarLeftButton()
  {
    onLogOutClicked();
  }
  
  public void onClickTopBarRightButton() {}
  
  @SuppressLint({"DefaultLocale"})
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968632, paramViewGroup, false);
    this.sharedPreferences = getActivity().getSharedPreferences("COMPANION_APP_SPREF_KEY_", 0);
    this.mListener = ((HomeInteractionListener)getActivity());
    ButterKnife.bind(this, paramLayoutInflater);
    this.mMetersList = ((RecyclerView)paramLayoutInflater.findViewById(2131624125));
    this.mHelpButton = ((ImageButton)paramLayoutInflater.findViewById(2131624118));
    this.mHelpButtonLayout = ((LinearLayout)paramLayoutInflater.findViewById(2131624117));
    this.layoutSmartLink = ((LinearLayout)paramLayoutInflater.findViewById(2131624119));
    this.layoutSendPro = ((LinearLayout)paramLayoutInflater.findViewById(2131624126));
    showSmartLinkLayout(false);
    showSendProLayout(false);
    this.meterItems = new ArrayList();
    this.metersAdapter = new HomeMetersAdapter(this.meterItems, new MeterInteractionListener()
    {
      public void onMeterAddFunButtonTapped(PBDevice paramAnonymousPBDevice)
      {
        PBHomeFragment.access$002(PBHomeFragment.this, paramAnonymousPBDevice);
        PBHomeFragment.this.showAddFundsDialog();
      }
      
      public void onMeterErrorClicked(String paramAnonymousString1, String paramAnonymousString2)
      {
        PBHomeFragment.this.showMeterErrorDialog(paramAnonymousString1, paramAnonymousString2);
      }
      
      public void onMeterWarningClicked(String paramAnonymousString1, String paramAnonymousString2)
      {
        PBHomeFragment.this.showMeterWarningDialog(paramAnonymousString1, paramAnonymousString2);
      }
    });
    this.mMetersList.setLayoutManager(new LinearLayoutManager(getActivity()));
    this.mMetersList.setAdapter(this.metersAdapter);
    setTopBarTitle("Home");
    setTopBarLeftButtonText("Logout");
    hideTopBarRightButton();
    this.mSendProList = ((RecyclerView)paramLayoutInflater.findViewById(2131624130));
    this.sendProItems = new ArrayList();
    double d = DataManager.getInstance().getUser().getSendProBalance();
    this.sendProItems.add(new SendProItem("SendPro", d));
    this.sendProAdapter = new HomeSendProAdapter(this.sendProItems, new SendProInteractionListener()
    {
      public void onSendProAddFunButtonTapped(String paramAnonymousString)
      {
        PBHomeFragment.this.showAddSendProFundsDialog();
      }
    });
    this.mSendProList.setLayoutManager(new LinearLayoutManager(getActivity()));
    this.mSendProList.setAdapter(this.sendProAdapter);
    this.mSendProList.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PBHomeFragment.this.showAddSendProFundsDialog();
      }
    });
    this.mMeterSettingsButton = ((ImageButton)paramLayoutInflater.findViewById(2131624122));
    this.mMeterSettingsButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PBHomeFragment.this.mListener.onSettingsClicked();
      }
    });
    this.mHelpButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PBHomeFragment.this.mListener.onHelpClicked();
      }
    });
    this.mHelpButtonLayout.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        PBHomeFragment.this.mListener.onHelpClicked();
      }
    });
    callServerAPIs();
    return paramLayoutInflater;
  }
  
  void onLogOutClicked()
  {
    if (this.mListener != null) {
      this.mListener.onLogOutButtonClicked();
    }
  }
  
  public void onShowLoadingDialog(boolean paramBoolean)
  {
    if (this.mListener != null) {
      this.mListener.onLoadingDataFromServer(paramBoolean);
    }
  }
  
  void releaseLoadingLock()
  {
    this.loadingLock -= 1;
    if (this.loadingLock == 0) {
      onShowLoadingDialog(false);
    }
  }
  
  void saveLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.sharedPreferences.edit().putBoolean(paramString1, this.low_ink_level);
  }
  
  void saveMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.sharedPreferences.edit().putBoolean(paramString1, this.meter_error);
  }
  
  void saveMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    this.sharedPreferences.edit().putBoolean(paramString1, this.meter_funds_low);
  }
  
  void savePreferences(String paramString1, String paramString2)
  {
    String str2 = paramString1 + "_" + paramString2 + "_low_ink_level";
    String str1 = paramString1 + "_" + paramString2 + "_funds_low";
    String str3 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString2 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    paramString1 = this.sharedPreferences.edit();
    paramString1.putBoolean(str2, this.low_ink_level);
    paramString1.putBoolean(str1, this.meter_funds_low);
    paramString1.putBoolean(str3, this.meter_error);
    paramString1.putBoolean(paramString2, this.software_rate_update);
    paramString1.commit();
  }
  
  void saveSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.sharedPreferences.edit().putBoolean(paramString1, this.software_rate_update);
  }
  
  public void showAddFundsDialog()
  {
    this.addFundsSmartLinkDialog = new MaterialDialog.Builder(getContext()).customView(2130968607, true).build();
    this.addFundsSmartLinkDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    View localView = this.addFundsSmartLinkDialog.getCustomView();
    this.addFunds_ErrorMsg = ((TextView)localView.findViewById(2131624076));
    this.addFunds_ErrorMsg.setVisibility(8);
    this.progressBar_SL = ((ProgressBar)localView.findViewById(2131624078));
    this.progressBar_SL.setVisibility(8);
    this.refilFundSLEditText = ((EditText)localView.findViewById(2131624075));
    localView.findViewById(2131624077).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        int i = 0;
        try
        {
          int j = Integer.parseInt(PBHomeFragment.this.refilFundSLEditText.getText().toString());
          i = j;
        }
        catch (NumberFormatException paramAnonymousView)
        {
          for (;;)
          {
            paramAnonymousView.printStackTrace();
            continue;
            PBHomeFragment.this.addFunds_ErrorMsg.setText("Minimum Amount is $" + 5 + ".00");
            PBHomeFragment.this.addFunds_ErrorMsg.setVisibility(0);
          }
        }
        if (i >= 5)
        {
          PBHomeFragment.this.meterFunds_AddAmount = PBHomeFragment.this.refilFundSLEditText.getText().toString();
          PBHomeFragment.this.addFundsToSelectedDevice();
          PBHomeFragment.this.progressBar_SL.setVisibility(0);
          return;
        }
      }
    });
    this.addFundsSmartLinkDialog.show();
  }
  
  public void showAddSendProFundsDialog()
  {
    this.addFundsSendProDialog = new MaterialDialog.Builder(getContext()).customView(2130968607, true).build();
    this.addFundsSendProDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    View localView = this.addFundsSendProDialog.getCustomView();
    this.addFunds_ErrorMsg = ((TextView)localView.findViewById(2131624076));
    this.addFunds_ErrorMsg.setVisibility(8);
    this.progressBar_SP = ((ProgressBar)localView.findViewById(2131624078));
    this.progressBar_SP.setVisibility(8);
    this.refilFundSPEditText = ((EditText)localView.findViewById(2131624075));
    localView.findViewById(2131624077).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        int i = 0;
        try
        {
          int j = Integer.parseInt(PBHomeFragment.this.refilFundSPEditText.getText().toString());
          i = j;
        }
        catch (NumberFormatException paramAnonymousView)
        {
          for (;;)
          {
            paramAnonymousView.printStackTrace();
            continue;
            PBHomeFragment.this.addFunds_ErrorMsg.setText("Minimum Amount is $" + 10 + ".00");
            PBHomeFragment.this.addFunds_ErrorMsg.setVisibility(0);
          }
        }
        if (i >= 10)
        {
          PBHomeFragment.this.sendProFunds_AddAmount = PBHomeFragment.this.refilFundSPEditText.getText().toString();
          PBHomeFragment.this.addFundsToSendProAccount();
          PBHomeFragment.this.progressBar_SP.setVisibility(0);
          return;
        }
      }
    });
    this.addFundsSendProDialog.show();
  }
  
  public void showMeterErrorDialog(String paramString1, String paramString2)
  {
    this.meterErrorDialog = new MaterialDialog.Builder(getContext()).customView(2130968686, true).build();
    this.meterErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    Object localObject = this.meterErrorDialog.getCustomView();
    this.ErrorTitle = ((TextView)((View)localObject).findViewById(2131624301));
    this.ErrorMessage = ((TextView)((View)localObject).findViewById(2131624076));
    localObject = formatErrorMessage(paramString1, paramString2, "DEVICE");
    paramString2 = getMeterNickName(paramString2);
    paramString1 = (String)localObject;
    if (!((String)localObject).equals("")) {
      paramString1 = (String)localObject + " for meter " + paramString2;
    }
    this.ErrorMessage.setText(paramString1);
    this.meterErrorDialog.show();
  }
  
  public void showMeterWarningDialog(String paramString1, String paramString2)
  {
    this.meterWarningDialog = new MaterialDialog.Builder(getContext()).customView(2130968687, true).build();
    this.meterWarningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    Object localObject = this.meterWarningDialog.getCustomView();
    this.WarningTitle = ((TextView)((View)localObject).findViewById(2131624301));
    this.WarningMessage = ((TextView)((View)localObject).findViewById(2131624076));
    String str1 = formatErrorMessage(paramString1, paramString2, "SUPPLY");
    localObject = formatErrorMessage(paramString1, paramString2, "FUND");
    String str2 = getMeterNickName(paramString2);
    paramString1 = str1;
    if (!str1.equals("")) {
      paramString1 = str1 + " for meter " + str2;
    }
    paramString2 = (String)localObject;
    if (!((String)localObject).equals("")) {
      paramString2 = (String)localObject + " for meter " + str2;
    }
    paramString1 = paramString1 + "\n" + paramString2;
    this.WarningMessage.setText(paramString1);
    this.meterWarningDialog.show();
  }
  
  void showSendProLayout(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.layoutSendPro.setVisibility(0);
    }
    for (;;)
    {
      return;
      this.layoutSendPro.setVisibility(8);
    }
  }
  
  void showSendproAddFundsMessage(boolean paramBoolean)
  {
    if (paramBoolean) {
      if (this.mListener != null) {
        this.mListener.onHomeShowMessageDialog("message_sendpro_add_fund", this.SendproMessageSuccess);
      }
    }
    for (;;)
    {
      return;
      if (this.mListener != null) {
        this.mListener.onHomeShowMessageDialog("message_sendpro_add_fund", this.SendproMessageFailed);
      }
    }
  }
  
  void showSmartLinkAddFundsMessage(boolean paramBoolean)
  {
    if (paramBoolean) {
      if (this.mListener != null) {
        this.mListener.onHomeShowMessageDialog("message_smartlink_add_fund", this.SmartLinkMessageSuccess);
      }
    }
    for (;;)
    {
      return;
      if (this.mListener != null) {
        this.mListener.onHomeShowMessageDialog("message_smartlink_add_fund", this.SendproMessageFailed);
      }
    }
  }
  
  void showSmartLinkLayout(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.layoutSmartLink.setVisibility(0);
    }
    for (;;)
    {
      return;
      this.layoutSmartLink.setVisibility(8);
    }
  }
  
  public void updateHomeFullLayout()
  {
    callServerAPIs();
  }
  
  public void updateHomeLayout()
  {
    double d = DataManager.getInstance().getUser().getSendProBalance();
    ((SendProItem)this.sendProItems.get(0)).setSendproAmount(d);
    this.sendProAdapter.refreshView();
  }
  
  void updateNotificationDictionaries(List<PBDeviceNotification> paramList)
  {
    Map localMap3 = DataManager.getInstance().getMeterModelSerialNotificationError();
    Map localMap1 = DataManager.getInstance().getMeterModelSerialNotificationWarning();
    Map localMap2 = DataManager.getInstance().getMeterNotificationList();
    localMap2.clear();
    Object localObject1 = paramList.iterator();
    Object localObject2;
    String str;
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (PBDeviceNotification)((Iterator)localObject1).next();
      str = ((PBDeviceNotification)localObject2).getModelNumber();
      localObject2 = ((PBDeviceNotification)localObject2).getSerialNumber();
      str = str + (String)localObject2;
      localMap3.put(str, Boolean.FALSE);
      localMap1.put(str, Boolean.FALSE);
      localMap2.put(str, new ArrayList());
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      localObject1 = (PBDeviceNotification)paramList.next();
      localObject2 = ((PBDeviceNotification)localObject1).getModelNumber();
      str = ((PBDeviceNotification)localObject1).getSerialNumber();
      str = (String)localObject2 + str;
      ((List)localMap2.get(str)).add(localObject1);
      localObject1 = ((PBDeviceNotification)localObject1).getNotificationLevel();
      int i = -1;
      switch (((String)localObject1).hashCode())
      {
      }
      for (;;)
      {
        switch (i)
        {
        default: 
          break;
        case 0: 
          localMap3.put(str, Boolean.TRUE);
          break;
          if (((String)localObject1).equals("ERROR"))
          {
            i = 0;
            continue;
            if (((String)localObject1).equals("WARNING")) {
              i = 1;
            }
          }
          break;
        }
      }
      localMap1.put(str, Boolean.TRUE);
    }
    DataManager.getInstance().setMeterModelSerialNotificationError(localMap3);
    DataManager.getInstance().setMeterModelSerialNotificationWarning(localMap1);
    DataManager.getInstance().setMeterNotificationList(localMap2);
  }
  
  public static abstract interface HomeInteractionListener
  {
    public abstract void onHelpClicked();
    
    public abstract void onHomeShowMessageDialog(String paramString1, String paramString2);
    
    public abstract void onLoadingDataFromServer(boolean paramBoolean);
    
    public abstract void onLogOutButtonClicked();
    
    public abstract void onSettingsClicked();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBHomeFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */