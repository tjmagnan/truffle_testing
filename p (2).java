package com.testfairy.p;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.opengl.GLException;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import com.testfairy.e;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

public class b
{
  public static final int a = 80;
  
  public static Bitmap a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, GL10 paramGL10)
  {
    int[] arrayOfInt = new int[paramInt3 * paramInt4];
    IntBuffer localIntBuffer = IntBuffer.wrap(arrayOfInt);
    localIntBuffer.position(0);
    try
    {
      paramGL10.glReadPixels(paramInt1, paramInt2, paramInt3, paramInt4, 6408, 5121, localIntBuffer);
      paramGL10 = a(arrayOfInt, paramInt3, paramInt4);
      paramGL10 = Bitmap.createBitmap(paramGL10, paramInt3, paramInt4, Bitmap.Config.ARGB_8888);
    }
    catch (GLException paramGL10)
    {
      for (;;)
      {
        paramGL10 = null;
      }
    }
    return paramGL10;
  }
  
  public static Bitmap a(Bitmap paramBitmap, float paramFloat)
  {
    Matrix localMatrix = new Matrix();
    localMatrix.postRotate(paramFloat);
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, true);
  }
  
  public static Bitmap a(Drawable paramDrawable)
  {
    Bitmap localBitmap = Bitmap.createBitmap(paramDrawable.getIntrinsicWidth(), paramDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    paramDrawable.setBounds(0, 0, localCanvas.getWidth(), localCanvas.getHeight());
    paramDrawable.draw(localCanvas);
    return localBitmap;
  }
  
  public static Drawable a(Resources paramResources, String paramString)
  {
    return new BitmapDrawable(paramResources, new ByteArrayInputStream(Base64.decode(paramString, 0)));
  }
  
  public static void a(Bitmap paramBitmap, String paramString)
  {
    a(paramBitmap, paramString, 80);
  }
  
  public static void a(Bitmap paramBitmap, String paramString, int paramInt)
  {
    try
    {
      FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
      localFileOutputStream.<init>(paramString);
      paramBitmap.compress(Bitmap.CompressFormat.JPEG, paramInt, localFileOutputStream);
      localFileOutputStream.close();
      return;
    }
    catch (Exception paramBitmap)
    {
      for (;;) {}
    }
  }
  
  public static boolean a(Bitmap paramBitmap)
  {
    boolean bool2 = false;
    boolean bool1;
    if ((paramBitmap.getPixel(paramBitmap.getWidth() / 2, paramBitmap.getHeight() / 2) & 0xFFFFFF) != 0L) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      int[] arrayOfInt = new int[paramBitmap.getWidth()];
      label97:
      for (int i = 0;; i++)
      {
        if (i >= paramBitmap.getHeight()) {
          break label103;
        }
        paramBitmap.getPixels(arrayOfInt, 0, arrayOfInt.length, 0, i, paramBitmap.getWidth(), 1);
        for (int j = 0;; j++)
        {
          if (j >= paramBitmap.getWidth()) {
            break label97;
          }
          bool1 = bool2;
          if ((arrayOfInt[j] & 0xFFFFFF) != 0) {
            break;
          }
        }
      }
      label103:
      Log.d(e.a, "All pixels are black");
      bool1 = true;
    }
  }
  
  public static byte[] a(Bitmap paramBitmap, int paramInt)
  {
    Object localObject = null;
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      paramBitmap.compress(Bitmap.CompressFormat.JPEG, paramInt, localByteArrayOutputStream);
      paramBitmap = localByteArrayOutputStream.toByteArray();
      return paramBitmap;
    }
    catch (OutOfMemoryError paramBitmap)
    {
      for (;;)
      {
        paramBitmap = (Bitmap)localObject;
      }
    }
    catch (Exception paramBitmap)
    {
      for (;;)
      {
        paramBitmap = (Bitmap)localObject;
      }
    }
  }
  
  public static int[] a(int[] paramArrayOfInt)
  {
    if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN)
    {
      int j = paramArrayOfInt.length;
      for (int i = 0; i < j; i++)
      {
        int k = paramArrayOfInt[i];
        paramArrayOfInt[i] = (k >> 16 & 0xFF | 0xFF00FF00 & k | (k & 0xFF) << 16);
      }
    }
    return paramArrayOfInt;
  }
  
  public static int[] a(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = a(paramArrayOfInt);
    paramArrayOfInt = new int[paramInt1];
    for (int i = 0; i < paramInt1; i++) {
      paramArrayOfInt[i] = -65281;
    }
    for (i = 0; i < paramInt2 >> 1; i++)
    {
      System.arraycopy(arrayOfInt, paramInt1 * i, paramArrayOfInt, 0, paramInt1);
      System.arraycopy(arrayOfInt, (paramInt2 - 1 - i) * paramInt1, arrayOfInt, paramInt1 * i, paramInt1);
      System.arraycopy(paramArrayOfInt, 0, arrayOfInt, (paramInt2 - 1 - i) * paramInt1, paramInt1);
    }
    return arrayOfInt;
  }
  
  public static byte[] b(Bitmap paramBitmap)
  {
    Object localObject = null;
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      paramBitmap.compress(Bitmap.CompressFormat.PNG, 0, localByteArrayOutputStream);
      paramBitmap = localByteArrayOutputStream.toByteArray();
      return paramBitmap;
    }
    catch (OutOfMemoryError paramBitmap)
    {
      for (;;)
      {
        paramBitmap = (Bitmap)localObject;
      }
    }
    catch (Exception paramBitmap)
    {
      for (;;)
      {
        paramBitmap = (Bitmap)localObject;
      }
    }
  }
  
  public static void c(Bitmap paramBitmap)
  {
    if ((paramBitmap == null) && (a(paramBitmap))) {
      Log.d(e.a, "The bitmap is null or black");
    }
    for (;;)
    {
      return;
      File localFile = new File(Environment.getExternalStorageDirectory(), "/images/image-" + System.currentTimeMillis() + ".png");
      a(paramBitmap, localFile.getAbsolutePath());
      Log.d(e.a, localFile.getAbsolutePath() + " size: " + localFile.length());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */