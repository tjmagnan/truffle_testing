package org.joda.time.convert;

public abstract interface Converter
{
  public abstract Class<?> getSupportedType();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\Converter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */