package android.support.constraint.solver;

import java.io.PrintStream;
import java.util.Arrays;

public class ArrayLinkedVariables
{
  private static final boolean DEBUG = false;
  private static final int NONE = -1;
  private int ROW_SIZE = 8;
  private SolverVariable candidate = null;
  int currentSize = 0;
  private int[] mArrayIndices = new int[this.ROW_SIZE];
  private int[] mArrayNextIndices = new int[this.ROW_SIZE];
  private float[] mArrayValues = new float[this.ROW_SIZE];
  private final Cache mCache;
  private boolean mDidFillOnce = false;
  private int mHead = -1;
  private int mLast = -1;
  private final ArrayRow mRow;
  
  ArrayLinkedVariables(ArrayRow paramArrayRow, Cache paramCache)
  {
    this.mRow = paramArrayRow;
    this.mCache = paramCache;
  }
  
  public final void add(SolverVariable paramSolverVariable, float paramFloat)
  {
    if (paramFloat == 0.0F) {}
    for (;;)
    {
      return;
      if (this.mHead != -1) {
        break;
      }
      this.mHead = 0;
      this.mArrayValues[this.mHead] = paramFloat;
      this.mArrayIndices[this.mHead] = paramSolverVariable.id;
      this.mArrayNextIndices[this.mHead] = -1;
      this.currentSize += 1;
      if (!this.mDidFillOnce) {
        this.mLast += 1;
      }
    }
    int i = this.mHead;
    int m = -1;
    int k;
    for (int j = 0;; j++)
    {
      if ((i == -1) || (j >= this.currentSize)) {
        break label254;
      }
      k = this.mArrayIndices[i];
      if (k == paramSolverVariable.id)
      {
        paramSolverVariable = this.mArrayValues;
        paramSolverVariable[i] += paramFloat;
        if (this.mArrayValues[i] != 0.0F) {
          break;
        }
        if (i == this.mHead) {
          this.mHead = this.mArrayNextIndices[i];
        }
        for (;;)
        {
          this.mCache.mIndexedVariables[k].removeClientEquation(this.mRow);
          if (this.mDidFillOnce) {
            this.mLast = i;
          }
          this.currentSize -= 1;
          break;
          this.mArrayNextIndices[m] = this.mArrayNextIndices[i];
        }
      }
      if (this.mArrayIndices[i] < paramSolverVariable.id) {
        m = i;
      }
      i = this.mArrayNextIndices[i];
    }
    label254:
    i = this.mLast + 1;
    if (this.mDidFillOnce)
    {
      if (this.mArrayIndices[this.mLast] == -1) {
        i = this.mLast;
      }
    }
    else
    {
      label286:
      j = i;
      if (i >= this.mArrayIndices.length)
      {
        j = i;
        if (this.currentSize < this.mArrayIndices.length)
        {
          k = 0;
          label316:
          j = i;
          if (k < this.mArrayIndices.length)
          {
            if (this.mArrayIndices[k] != -1) {
              break label541;
            }
            j = k;
          }
        }
      }
      i = j;
      if (j >= this.mArrayIndices.length)
      {
        i = this.mArrayIndices.length;
        this.ROW_SIZE *= 2;
        this.mDidFillOnce = false;
        this.mLast = (i - 1);
        this.mArrayValues = Arrays.copyOf(this.mArrayValues, this.ROW_SIZE);
        this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
        this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
      }
      this.mArrayIndices[i] = paramSolverVariable.id;
      this.mArrayValues[i] = paramFloat;
      if (m == -1) {
        break label547;
      }
      this.mArrayNextIndices[i] = this.mArrayNextIndices[m];
      this.mArrayNextIndices[m] = i;
    }
    for (;;)
    {
      this.currentSize += 1;
      if (!this.mDidFillOnce) {
        this.mLast += 1;
      }
      if (this.mLast < this.mArrayIndices.length) {
        break;
      }
      this.mDidFillOnce = true;
      this.mLast = (this.mArrayIndices.length - 1);
      break;
      i = this.mArrayIndices.length;
      break label286;
      label541:
      k++;
      break label316;
      label547:
      this.mArrayNextIndices[i] = this.mHead;
      this.mHead = i;
    }
  }
  
  public final void clear()
  {
    this.mHead = -1;
    this.mLast = -1;
    this.mDidFillOnce = false;
    this.currentSize = 0;
  }
  
  final boolean containsKey(SolverVariable paramSolverVariable)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this.mHead == -1)
    {
      bool1 = bool2;
      return bool1;
    }
    int j = this.mHead;
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (j == -1) {
        break;
      }
      bool1 = bool2;
      if (i >= this.currentSize) {
        break;
      }
      if (this.mArrayIndices[j] == paramSolverVariable.id)
      {
        bool1 = true;
        break;
      }
      j = this.mArrayNextIndices[j];
    }
  }
  
  public void display()
  {
    int j = this.currentSize;
    System.out.print("{ ");
    int i = 0;
    if (i < j)
    {
      SolverVariable localSolverVariable = getVariable(i);
      if (localSolverVariable == null) {}
      for (;;)
      {
        i++;
        break;
        System.out.print(localSolverVariable + " = " + getVariableValue(i) + " ");
      }
    }
    System.out.println(" }");
  }
  
  void divideByAmount(float paramFloat)
  {
    int i = this.mHead;
    for (int j = 0; (i != -1) && (j < this.currentSize); j++)
    {
      float[] arrayOfFloat = this.mArrayValues;
      arrayOfFloat[i] /= paramFloat;
      i = this.mArrayNextIndices[i];
    }
  }
  
  public final float get(SolverVariable paramSolverVariable)
  {
    int i = this.mHead;
    int j = 0;
    if ((i != -1) && (j < this.currentSize)) {
      if (this.mArrayIndices[i] != paramSolverVariable.id) {}
    }
    for (float f = this.mArrayValues[i];; f = 0.0F)
    {
      return f;
      i = this.mArrayNextIndices[i];
      j++;
      break;
    }
  }
  
  SolverVariable getPivotCandidate()
  {
    if (this.candidate == null)
    {
      int i = this.mHead;
      int j = 0;
      for (Object localObject1 = null;; localObject1 = localObject2)
      {
        localObject2 = localObject1;
        if (i == -1) {
          break;
        }
        localObject2 = localObject1;
        if (j >= this.currentSize) {
          break;
        }
        localObject2 = localObject1;
        if (this.mArrayValues[i] < 0.0F)
        {
          SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[i]];
          if (localObject1 != null)
          {
            localObject2 = localObject1;
            if (((SolverVariable)localObject1).strength >= localSolverVariable.strength) {}
          }
          else
          {
            localObject2 = localSolverVariable;
          }
        }
        i = this.mArrayNextIndices[i];
        j++;
      }
    }
    Object localObject2 = this.candidate;
    return (SolverVariable)localObject2;
  }
  
  final SolverVariable getVariable(int paramInt)
  {
    int i = this.mHead;
    int j = 0;
    if ((i != -1) && (j < this.currentSize)) {
      if (j != paramInt) {}
    }
    for (SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[i]];; localSolverVariable = null)
    {
      return localSolverVariable;
      i = this.mArrayNextIndices[i];
      j++;
      break;
    }
  }
  
  final float getVariableValue(int paramInt)
  {
    int j = this.mHead;
    int i = 0;
    if ((j != -1) && (i < this.currentSize)) {
      if (i != paramInt) {}
    }
    for (float f = this.mArrayValues[j];; f = 0.0F)
    {
      return f;
      j = this.mArrayNextIndices[j];
      i++;
      break;
    }
  }
  
  boolean hasAtLeastOnePositiveVariable()
  {
    int i = this.mHead;
    int j = 0;
    if ((i != -1) && (j < this.currentSize)) {
      if (this.mArrayValues[i] <= 0.0F) {}
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i = this.mArrayNextIndices[i];
      j++;
      break;
    }
  }
  
  void invert()
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      float[] arrayOfFloat = this.mArrayValues;
      arrayOfFloat[j] *= -1.0F;
      j = this.mArrayNextIndices[j];
    }
  }
  
  SolverVariable pickPivotCandidate()
  {
    Object localObject2 = null;
    Object localObject1 = null;
    int i = this.mHead;
    int j = 0;
    float f2;
    float f1;
    label60:
    Object localObject5;
    Object localObject4;
    Object localObject3;
    if ((i != -1) && (j < this.currentSize))
    {
      f2 = this.mArrayValues[i];
      if (f2 < 0.0F)
      {
        f1 = f2;
        if (f2 > -0.001F)
        {
          this.mArrayValues[i] = 0.0F;
          f1 = 0.0F;
        }
        localObject5 = localObject2;
        localObject4 = localObject1;
        if (f1 == 0.0F) {
          break label152;
        }
        localObject3 = this.mCache.mIndexedVariables[this.mArrayIndices[i]];
        if (((SolverVariable)localObject3).mType != SolverVariable.Type.UNRESTRICTED) {
          break label173;
        }
        if (f1 >= 0.0F) {
          break label131;
        }
      }
    }
    for (;;)
    {
      return (SolverVariable)localObject3;
      f1 = f2;
      if (f2 >= 0.001F) {
        break label60;
      }
      this.mArrayValues[i] = 0.0F;
      f1 = 0.0F;
      break label60;
      label131:
      localObject5 = localObject2;
      localObject4 = localObject1;
      if (localObject1 == null)
      {
        localObject4 = localObject3;
        localObject5 = localObject2;
      }
      for (;;)
      {
        label152:
        i = this.mArrayNextIndices[i];
        j++;
        localObject2 = localObject5;
        localObject1 = localObject4;
        break;
        label173:
        localObject5 = localObject2;
        localObject4 = localObject1;
        if (f1 < 0.0F) {
          if (localObject2 != null)
          {
            localObject5 = localObject2;
            localObject4 = localObject1;
            if (((SolverVariable)localObject3).strength >= ((SolverVariable)localObject2).strength) {}
          }
          else
          {
            localObject5 = localObject3;
            localObject4 = localObject1;
          }
        }
      }
      if (localObject1 != null) {
        localObject3 = localObject1;
      } else {
        localObject3 = localObject2;
      }
    }
  }
  
  public final void put(SolverVariable paramSolverVariable, float paramFloat)
  {
    if (paramFloat == 0.0F) {
      remove(paramSolverVariable);
    }
    for (;;)
    {
      return;
      if (this.mHead != -1) {
        break;
      }
      this.mHead = 0;
      this.mArrayValues[this.mHead] = paramFloat;
      this.mArrayIndices[this.mHead] = paramSolverVariable.id;
      this.mArrayNextIndices[this.mHead] = -1;
      this.currentSize += 1;
      if (!this.mDidFillOnce) {
        this.mLast += 1;
      }
    }
    int i = this.mHead;
    int m = -1;
    for (int j = 0;; j++)
    {
      if ((i == -1) || (j >= this.currentSize)) {
        break label166;
      }
      if (this.mArrayIndices[i] == paramSolverVariable.id)
      {
        this.mArrayValues[i] = paramFloat;
        break;
      }
      if (this.mArrayIndices[i] < paramSolverVariable.id) {
        m = i;
      }
      i = this.mArrayNextIndices[i];
    }
    label166:
    i = this.mLast + 1;
    label198:
    int k;
    if (this.mDidFillOnce)
    {
      if (this.mArrayIndices[this.mLast] == -1) {
        i = this.mLast;
      }
    }
    else
    {
      j = i;
      if (i >= this.mArrayIndices.length)
      {
        j = i;
        if (this.currentSize < this.mArrayIndices.length)
        {
          k = 0;
          label228:
          j = i;
          if (k < this.mArrayIndices.length)
          {
            if (this.mArrayIndices[k] != -1) {
              break label442;
            }
            j = k;
          }
        }
      }
      i = j;
      if (j >= this.mArrayIndices.length)
      {
        i = this.mArrayIndices.length;
        this.ROW_SIZE *= 2;
        this.mDidFillOnce = false;
        this.mLast = (i - 1);
        this.mArrayValues = Arrays.copyOf(this.mArrayValues, this.ROW_SIZE);
        this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
        this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
      }
      this.mArrayIndices[i] = paramSolverVariable.id;
      this.mArrayValues[i] = paramFloat;
      if (m == -1) {
        break label448;
      }
      this.mArrayNextIndices[i] = this.mArrayNextIndices[m];
      this.mArrayNextIndices[m] = i;
    }
    for (;;)
    {
      this.currentSize += 1;
      if (!this.mDidFillOnce) {
        this.mLast += 1;
      }
      if (this.currentSize < this.mArrayIndices.length) {
        break;
      }
      this.mDidFillOnce = true;
      break;
      i = this.mArrayIndices.length;
      break label198;
      label442:
      k++;
      break label228;
      label448:
      this.mArrayNextIndices[i] = this.mHead;
      this.mHead = i;
    }
  }
  
  public final float remove(SolverVariable paramSolverVariable)
  {
    float f2 = 0.0F;
    if (this.candidate == paramSolverVariable) {
      this.candidate = null;
    }
    float f1;
    if (this.mHead == -1)
    {
      f1 = f2;
      return f1;
    }
    int i = this.mHead;
    int k = -1;
    for (int j = 0;; j++)
    {
      f1 = f2;
      if (i == -1) {
        break;
      }
      f1 = f2;
      if (j >= this.currentSize) {
        break;
      }
      int m = this.mArrayIndices[i];
      if (m == paramSolverVariable.id)
      {
        if (i == this.mHead) {
          this.mHead = this.mArrayNextIndices[i];
        }
        for (;;)
        {
          this.mCache.mIndexedVariables[m].removeClientEquation(this.mRow);
          this.currentSize -= 1;
          this.mArrayIndices[i] = -1;
          if (this.mDidFillOnce) {
            this.mLast = i;
          }
          f1 = this.mArrayValues[i];
          break;
          this.mArrayNextIndices[k] = this.mArrayNextIndices[i];
        }
      }
      k = i;
      i = this.mArrayNextIndices[i];
    }
  }
  
  int sizeInBytes()
  {
    return 0 + this.mArrayIndices.length * 4 * 3 + 36;
  }
  
  public String toString()
  {
    String str = "";
    int i = this.mHead;
    for (int j = 0; (i != -1) && (j < this.currentSize); j++)
    {
      str = str + " -> ";
      str = str + this.mArrayValues[i] + " : ";
      str = str + this.mCache.mIndexedVariables[this.mArrayIndices[i]];
      i = this.mArrayNextIndices[i];
    }
    return str;
  }
  
  void updateClientEquations(ArrayRow paramArrayRow)
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      this.mCache.mIndexedVariables[this.mArrayIndices[j]].addClientEquation(paramArrayRow);
      j = this.mArrayNextIndices[j];
    }
  }
  
  void updateFromRow(ArrayRow paramArrayRow1, ArrayRow paramArrayRow2)
  {
    int j = this.mHead;
    int i = 0;
    while ((j != -1) && (i < this.currentSize)) {
      if (this.mArrayIndices[j] == paramArrayRow2.variable.id)
      {
        float f = this.mArrayValues[j];
        remove(paramArrayRow2.variable);
        ArrayLinkedVariables localArrayLinkedVariables = paramArrayRow2.variables;
        j = localArrayLinkedVariables.mHead;
        for (i = 0; (j != -1) && (i < localArrayLinkedVariables.currentSize); i++)
        {
          add(this.mCache.mIndexedVariables[localArrayLinkedVariables.mArrayIndices[j]], localArrayLinkedVariables.mArrayValues[j] * f);
          j = localArrayLinkedVariables.mArrayNextIndices[j];
        }
        paramArrayRow1.constantValue += paramArrayRow2.constantValue * f;
        paramArrayRow2.variable.removeClientEquation(paramArrayRow1);
        j = this.mHead;
        i = 0;
      }
      else
      {
        j = this.mArrayNextIndices[j];
        i++;
      }
    }
  }
  
  void updateFromSystem(ArrayRow paramArrayRow, ArrayRow[] paramArrayOfArrayRow)
  {
    int j = this.mHead;
    int i = 0;
    while ((j != -1) && (i < this.currentSize))
    {
      Object localObject = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
      if (((SolverVariable)localObject).definitionId != -1)
      {
        float f = this.mArrayValues[j];
        remove((SolverVariable)localObject);
        localObject = paramArrayOfArrayRow[localObject.definitionId];
        if (!((ArrayRow)localObject).isSimpleDefinition)
        {
          ArrayLinkedVariables localArrayLinkedVariables = ((ArrayRow)localObject).variables;
          i = localArrayLinkedVariables.mHead;
          for (j = 0; (i != -1) && (j < localArrayLinkedVariables.currentSize); j++)
          {
            add(this.mCache.mIndexedVariables[localArrayLinkedVariables.mArrayIndices[i]], localArrayLinkedVariables.mArrayValues[i] * f);
            i = localArrayLinkedVariables.mArrayNextIndices[i];
          }
        }
        paramArrayRow.constantValue += ((ArrayRow)localObject).constantValue * f;
        ((ArrayRow)localObject).variable.removeClientEquation(paramArrayRow);
        j = this.mHead;
        i = 0;
      }
      else
      {
        j = this.mArrayNextIndices[j];
        i++;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\constraint\solver\ArrayLinkedVariables.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */