package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationFieldType;
import org.joda.time.field.DelegatedDateTimeField;
import org.joda.time.field.DividedDateTimeField;
import org.joda.time.field.OffsetDateTimeField;
import org.joda.time.field.RemainderDateTimeField;
import org.joda.time.field.SkipUndoDateTimeField;
import org.joda.time.field.UnsupportedDurationField;

public final class BuddhistChronology
  extends AssembledChronology
{
  public static final int BE = 1;
  private static final int BUDDHIST_OFFSET = 543;
  private static final DateTimeField ERA_FIELD = new BasicSingleEraDateTimeField("BE");
  private static final BuddhistChronology INSTANCE_UTC = getInstance(DateTimeZone.UTC);
  private static final ConcurrentHashMap<DateTimeZone, BuddhistChronology> cCache = new ConcurrentHashMap();
  private static final long serialVersionUID = -3474595157769370126L;
  
  private BuddhistChronology(Chronology paramChronology, Object paramObject)
  {
    super(paramChronology, paramObject);
  }
  
  public static BuddhistChronology getInstance()
  {
    return getInstance(DateTimeZone.getDefault());
  }
  
  public static BuddhistChronology getInstance(DateTimeZone paramDateTimeZone)
  {
    Object localObject = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localObject = DateTimeZone.getDefault();
    }
    BuddhistChronology localBuddhistChronology = (BuddhistChronology)cCache.get(localObject);
    paramDateTimeZone = localBuddhistChronology;
    if (localBuddhistChronology == null)
    {
      paramDateTimeZone = new BuddhistChronology(GJChronology.getInstance((DateTimeZone)localObject, null), null);
      paramDateTimeZone = new BuddhistChronology(LimitChronology.getInstance(paramDateTimeZone, new DateTime(1, 1, 1, 0, 0, 0, 0, paramDateTimeZone), null), "");
      localObject = (BuddhistChronology)cCache.putIfAbsent(localObject, paramDateTimeZone);
      if (localObject == null) {
        break label91;
      }
      paramDateTimeZone = (DateTimeZone)localObject;
    }
    label91:
    for (;;)
    {
      return paramDateTimeZone;
    }
  }
  
  public static BuddhistChronology getInstanceUTC()
  {
    return INSTANCE_UTC;
  }
  
  private Object readResolve()
  {
    Object localObject = getBase();
    if (localObject == null) {}
    for (localObject = getInstanceUTC();; localObject = getInstance(((Chronology)localObject).getZone())) {
      return localObject;
    }
  }
  
  protected void assemble(AssembledChronology.Fields paramFields)
  {
    if (getParam() == null)
    {
      paramFields.eras = UnsupportedDurationField.getInstance(DurationFieldType.eras());
      paramFields.year = new OffsetDateTimeField(new SkipUndoDateTimeField(this, paramFields.year), 543);
      DateTimeField localDateTimeField = paramFields.yearOfEra;
      paramFields.yearOfEra = new DelegatedDateTimeField(paramFields.year, paramFields.eras, DateTimeFieldType.yearOfEra());
      paramFields.weekyear = new OffsetDateTimeField(new SkipUndoDateTimeField(this, paramFields.weekyear), 543);
      paramFields.centuryOfEra = new DividedDateTimeField(new OffsetDateTimeField(paramFields.yearOfEra, 99), paramFields.eras, DateTimeFieldType.centuryOfEra(), 100);
      paramFields.centuries = paramFields.centuryOfEra.getDurationField();
      paramFields.yearOfCentury = new OffsetDateTimeField(new RemainderDateTimeField((DividedDateTimeField)paramFields.centuryOfEra), DateTimeFieldType.yearOfCentury(), 1);
      paramFields.weekyearOfCentury = new OffsetDateTimeField(new RemainderDateTimeField(paramFields.weekyear, paramFields.centuries, DateTimeFieldType.weekyearOfCentury(), 100), DateTimeFieldType.weekyearOfCentury(), 1);
      paramFields.era = ERA_FIELD;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject instanceof BuddhistChronology))
      {
        paramObject = (BuddhistChronology)paramObject;
        bool = getZone().equals(((BuddhistChronology)paramObject).getZone());
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public int hashCode()
  {
    return "Buddhist".hashCode() * 11 + getZone().hashCode();
  }
  
  public String toString()
  {
    String str = "BuddhistChronology";
    DateTimeZone localDateTimeZone = getZone();
    if (localDateTimeZone != null) {
      str = "BuddhistChronology" + '[' + localDateTimeZone.getID() + ']';
    }
    return str;
  }
  
  public Chronology withUTC()
  {
    return INSTANCE_UTC;
  }
  
  public Chronology withZone(DateTimeZone paramDateTimeZone)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    if (localDateTimeZone == getZone()) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = getInstance(localDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BuddhistChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */