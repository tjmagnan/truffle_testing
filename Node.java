package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.SerializationException;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public abstract class Node
  implements Cloneable
{
  private static final List<Node> EMPTY_NODES = ;
  Attributes attributes;
  String baseUri;
  List<Node> childNodes;
  Node parentNode;
  int siblingIndex;
  
  protected Node()
  {
    this.childNodes = EMPTY_NODES;
    this.attributes = null;
  }
  
  protected Node(String paramString)
  {
    this(paramString, new Attributes());
  }
  
  protected Node(String paramString, Attributes paramAttributes)
  {
    Validate.notNull(paramString);
    Validate.notNull(paramAttributes);
    this.childNodes = EMPTY_NODES;
    this.baseUri = paramString.trim();
    this.attributes = paramAttributes;
  }
  
  private void addSiblingHtml(int paramInt, String paramString)
  {
    Validate.notNull(paramString);
    Validate.notNull(this.parentNode);
    if ((parent() instanceof Element)) {}
    for (Element localElement = (Element)parent();; localElement = null)
    {
      paramString = Parser.parseFragment(paramString, localElement, baseUri());
      this.parentNode.addChildren(paramInt, (Node[])paramString.toArray(new Node[paramString.size()]));
      return;
    }
  }
  
  private Element getDeepChild(Element paramElement)
  {
    Elements localElements = paramElement.children();
    if (localElements.size() > 0) {
      paramElement = getDeepChild((Element)localElements.get(0));
    }
    return paramElement;
  }
  
  private void reindexChildren(int paramInt)
  {
    while (paramInt < this.childNodes.size())
    {
      ((Node)this.childNodes.get(paramInt)).setSiblingIndex(paramInt);
      paramInt++;
    }
  }
  
  public String absUrl(String paramString)
  {
    Validate.notEmpty(paramString);
    if (!hasAttr(paramString)) {}
    for (paramString = "";; paramString = StringUtil.resolve(this.baseUri, attr(paramString))) {
      return paramString;
    }
  }
  
  protected void addChildren(int paramInt, Node... paramVarArgs)
  {
    Validate.noNullElements(paramVarArgs);
    ensureChildNodes();
    for (int i = paramVarArgs.length - 1; i >= 0; i--)
    {
      Node localNode = paramVarArgs[i];
      reparentChild(localNode);
      this.childNodes.add(paramInt, localNode);
      reindexChildren(paramInt);
    }
  }
  
  protected void addChildren(Node... paramVarArgs)
  {
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      Node localNode = paramVarArgs[i];
      reparentChild(localNode);
      ensureChildNodes();
      this.childNodes.add(localNode);
      localNode.setSiblingIndex(this.childNodes.size() - 1);
    }
  }
  
  public Node after(String paramString)
  {
    addSiblingHtml(this.siblingIndex + 1, paramString);
    return this;
  }
  
  public Node after(Node paramNode)
  {
    Validate.notNull(paramNode);
    Validate.notNull(this.parentNode);
    this.parentNode.addChildren(this.siblingIndex + 1, new Node[] { paramNode });
    return this;
  }
  
  public String attr(String paramString)
  {
    Validate.notNull(paramString);
    String str = this.attributes.getIgnoreCase(paramString);
    if (str.length() > 0) {
      paramString = str;
    }
    for (;;)
    {
      return paramString;
      if (paramString.toLowerCase().startsWith("abs:")) {
        paramString = absUrl(paramString.substring("abs:".length()));
      } else {
        paramString = "";
      }
    }
  }
  
  public Node attr(String paramString1, String paramString2)
  {
    this.attributes.put(paramString1, paramString2);
    return this;
  }
  
  public Attributes attributes()
  {
    return this.attributes;
  }
  
  public String baseUri()
  {
    return this.baseUri;
  }
  
  public Node before(String paramString)
  {
    addSiblingHtml(this.siblingIndex, paramString);
    return this;
  }
  
  public Node before(Node paramNode)
  {
    Validate.notNull(paramNode);
    Validate.notNull(this.parentNode);
    this.parentNode.addChildren(this.siblingIndex, new Node[] { paramNode });
    return this;
  }
  
  public Node childNode(int paramInt)
  {
    return (Node)this.childNodes.get(paramInt);
  }
  
  public final int childNodeSize()
  {
    return this.childNodes.size();
  }
  
  public List<Node> childNodes()
  {
    return Collections.unmodifiableList(this.childNodes);
  }
  
  protected Node[] childNodesAsArray()
  {
    return (Node[])this.childNodes.toArray(new Node[childNodeSize()]);
  }
  
  public List<Node> childNodesCopy()
  {
    ArrayList localArrayList = new ArrayList(this.childNodes.size());
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext()) {
      localArrayList.add(((Node)localIterator.next()).clone());
    }
    return localArrayList;
  }
  
  public Node clone()
  {
    Node localNode2 = doClone(null);
    LinkedList localLinkedList = new LinkedList();
    localLinkedList.add(localNode2);
    while (!localLinkedList.isEmpty())
    {
      Node localNode3 = (Node)localLinkedList.remove();
      for (int i = 0; i < localNode3.childNodes.size(); i++)
      {
        Node localNode1 = ((Node)localNode3.childNodes.get(i)).doClone(localNode3);
        localNode3.childNodes.set(i, localNode1);
        localLinkedList.add(localNode1);
      }
    }
    return localNode2;
  }
  
  protected Node doClone(Node paramNode)
  {
    Node localNode1;
    for (;;)
    {
      try
      {
        localNode1 = (Node)super.clone();
        localNode1.parentNode = paramNode;
        if (paramNode == null)
        {
          i = 0;
          localNode1.siblingIndex = i;
          if (this.attributes == null) {
            break label135;
          }
          paramNode = this.attributes.clone();
          localNode1.attributes = paramNode;
          localNode1.baseUri = this.baseUri;
          localNode1.childNodes = new ArrayList(this.childNodes.size());
          paramNode = this.childNodes.iterator();
          if (!paramNode.hasNext()) {
            break;
          }
          Node localNode2 = (Node)paramNode.next();
          localNode1.childNodes.add(localNode2);
          continue;
        }
        int i = this.siblingIndex;
      }
      catch (CloneNotSupportedException paramNode)
      {
        throw new RuntimeException(paramNode);
      }
      continue;
      label135:
      paramNode = null;
    }
    return localNode1;
  }
  
  protected void ensureChildNodes()
  {
    if (this.childNodes == EMPTY_NODES) {
      this.childNodes = new ArrayList(4);
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  Document.OutputSettings getOutputSettings()
  {
    Object localObject = ownerDocument();
    if (localObject != null) {}
    for (localObject = ((Document)localObject).outputSettings();; localObject = new Document("").outputSettings()) {
      return (Document.OutputSettings)localObject;
    }
  }
  
  public boolean hasAttr(String paramString)
  {
    Validate.notNull(paramString);
    if (paramString.startsWith("abs:"))
    {
      String str = paramString.substring("abs:".length());
      if ((!this.attributes.hasKeyIgnoreCase(str)) || (absUrl(str).equals(""))) {}
    }
    for (boolean bool = true;; bool = this.attributes.hasKeyIgnoreCase(paramString)) {
      return bool;
    }
  }
  
  public boolean hasSameValue(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject == null) || (getClass() != paramObject.getClass())) {
        bool = false;
      } else {
        bool = outerHtml().equals(((Node)paramObject).outerHtml());
      }
    }
  }
  
  public <T extends Appendable> T html(T paramT)
  {
    outerHtml(paramT);
    return paramT;
  }
  
  protected void indent(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    paramAppendable.append("\n").append(StringUtil.padding(paramOutputSettings.indentAmount() * paramInt));
  }
  
  public Node nextSibling()
  {
    Node localNode = null;
    if (this.parentNode == null) {}
    for (;;)
    {
      return localNode;
      List localList = this.parentNode.childNodes;
      int i = this.siblingIndex + 1;
      if (localList.size() > i) {
        localNode = (Node)localList.get(i);
      }
    }
  }
  
  public abstract String nodeName();
  
  public String outerHtml()
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    outerHtml(localStringBuilder);
    return localStringBuilder.toString();
  }
  
  protected void outerHtml(Appendable paramAppendable)
  {
    new NodeTraversor(new OuterHtmlVisitor(paramAppendable, getOutputSettings())).traverse(this);
  }
  
  abstract void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException;
  
  abstract void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException;
  
  public Document ownerDocument()
  {
    Object localObject = root();
    if ((localObject instanceof Document)) {}
    for (localObject = (Document)localObject;; localObject = null) {
      return (Document)localObject;
    }
  }
  
  public Node parent()
  {
    return this.parentNode;
  }
  
  public final Node parentNode()
  {
    return this.parentNode;
  }
  
  public Node previousSibling()
  {
    Node localNode = null;
    if (this.parentNode == null) {}
    for (;;)
    {
      return localNode;
      if (this.siblingIndex > 0) {
        localNode = (Node)this.parentNode.childNodes.get(this.siblingIndex - 1);
      }
    }
  }
  
  public void remove()
  {
    Validate.notNull(this.parentNode);
    this.parentNode.removeChild(this);
  }
  
  public Node removeAttr(String paramString)
  {
    Validate.notNull(paramString);
    this.attributes.removeIgnoreCase(paramString);
    return this;
  }
  
  protected void removeChild(Node paramNode)
  {
    if (paramNode.parentNode == this) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool);
      int i = paramNode.siblingIndex;
      this.childNodes.remove(i);
      reindexChildren(i);
      paramNode.parentNode = null;
      return;
    }
  }
  
  protected void reparentChild(Node paramNode)
  {
    if (paramNode.parentNode != null) {
      paramNode.parentNode.removeChild(paramNode);
    }
    paramNode.setParentNode(this);
  }
  
  protected void replaceChild(Node paramNode1, Node paramNode2)
  {
    if (paramNode1.parentNode == this) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool);
      Validate.notNull(paramNode2);
      if (paramNode2.parentNode != null) {
        paramNode2.parentNode.removeChild(paramNode2);
      }
      int i = paramNode1.siblingIndex;
      this.childNodes.set(i, paramNode2);
      paramNode2.parentNode = this;
      paramNode2.setSiblingIndex(i);
      paramNode1.parentNode = null;
      return;
    }
  }
  
  public void replaceWith(Node paramNode)
  {
    Validate.notNull(paramNode);
    Validate.notNull(this.parentNode);
    this.parentNode.replaceChild(this, paramNode);
  }
  
  public Node root()
  {
    for (Node localNode = this; localNode.parentNode != null; localNode = localNode.parentNode) {}
    return localNode;
  }
  
  public void setBaseUri(final String paramString)
  {
    Validate.notNull(paramString);
    traverse(new NodeVisitor()
    {
      public void head(Node paramAnonymousNode, int paramAnonymousInt)
      {
        paramAnonymousNode.baseUri = paramString;
      }
      
      public void tail(Node paramAnonymousNode, int paramAnonymousInt) {}
    });
  }
  
  protected void setParentNode(Node paramNode)
  {
    if (this.parentNode != null) {
      this.parentNode.removeChild(this);
    }
    this.parentNode = paramNode;
  }
  
  protected void setSiblingIndex(int paramInt)
  {
    this.siblingIndex = paramInt;
  }
  
  public int siblingIndex()
  {
    return this.siblingIndex;
  }
  
  public List<Node> siblingNodes()
  {
    if (this.parentNode == null)
    {
      localObject = Collections.emptyList();
      return (List<Node>)localObject;
    }
    Object localObject = this.parentNode.childNodes;
    ArrayList localArrayList = new ArrayList(((List)localObject).size() - 1);
    Iterator localIterator = ((List)localObject).iterator();
    for (;;)
    {
      localObject = localArrayList;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject = (Node)localIterator.next();
      if (localObject != this) {
        localArrayList.add(localObject);
      }
    }
  }
  
  public String toString()
  {
    return outerHtml();
  }
  
  public Node traverse(NodeVisitor paramNodeVisitor)
  {
    Validate.notNull(paramNodeVisitor);
    new NodeTraversor(paramNodeVisitor).traverse(this);
    return this;
  }
  
  public Node unwrap()
  {
    Validate.notNull(this.parentNode);
    if (this.childNodes.size() > 0) {}
    for (Node localNode = (Node)this.childNodes.get(0);; localNode = null)
    {
      this.parentNode.addChildren(this.siblingIndex, childNodesAsArray());
      remove();
      return localNode;
    }
  }
  
  public Node wrap(String paramString)
  {
    Validate.notEmpty(paramString);
    Object localObject;
    if ((parent() instanceof Element))
    {
      localObject = (Element)parent();
      localObject = Parser.parseFragment(paramString, (Element)localObject, baseUri());
      paramString = (Node)((List)localObject).get(0);
      if ((paramString != null) && ((paramString instanceof Element))) {
        break label63;
      }
      paramString = null;
    }
    label63:
    Element localElement;
    do
    {
      return paramString;
      localObject = null;
      break;
      localElement = (Element)paramString;
      paramString = getDeepChild(localElement);
      this.parentNode.replaceChild(this, localElement);
      paramString.addChildren(new Node[] { this });
      paramString = this;
    } while (((List)localObject).size() <= 0);
    for (int i = 0;; i++)
    {
      paramString = this;
      if (i >= ((List)localObject).size()) {
        break;
      }
      paramString = (Node)((List)localObject).get(i);
      paramString.parentNode.removeChild(paramString);
      localElement.appendChild(paramString);
    }
  }
  
  private static class OuterHtmlVisitor
    implements NodeVisitor
  {
    private Appendable accum;
    private Document.OutputSettings out;
    
    OuterHtmlVisitor(Appendable paramAppendable, Document.OutputSettings paramOutputSettings)
    {
      this.accum = paramAppendable;
      this.out = paramOutputSettings;
    }
    
    public void head(Node paramNode, int paramInt)
    {
      try
      {
        paramNode.outerHtmlHead(this.accum, paramInt, this.out);
        return;
      }
      catch (IOException paramNode)
      {
        throw new SerializationException(paramNode);
      }
    }
    
    public void tail(Node paramNode, int paramInt)
    {
      if (!paramNode.nodeName().equals("#text")) {}
      try
      {
        paramNode.outerHtmlTail(this.accum, paramInt, this.out);
        return;
      }
      catch (IOException paramNode)
      {
        throw new SerializationException(paramNode);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\Node.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */