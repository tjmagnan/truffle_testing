package com.testfairy.h;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class f
{
  private static String c = "UTF-8";
  protected ConcurrentHashMap a;
  protected ConcurrentHashMap b;
  
  public f()
  {
    d();
  }
  
  public f(String paramString1, String paramString2)
  {
    d();
    a(paramString1, paramString2);
  }
  
  public f(Map paramMap)
  {
    d();
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      paramMap = (Map.Entry)localIterator.next();
      a((String)paramMap.getKey(), (String)paramMap.getValue());
    }
  }
  
  private void d()
  {
    this.a = new ConcurrentHashMap();
    this.b = new ConcurrentHashMap();
  }
  
  public HttpEntity a()
  {
    Object localObject1;
    if (!this.b.isEmpty())
    {
      h localh = new h();
      localObject1 = this.a.entrySet().iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject3 = (Map.Entry)((Iterator)localObject1).next();
        localh.a((String)((Map.Entry)localObject3).getKey(), (String)((Map.Entry)localObject3).getValue());
      }
      int j = this.b.entrySet().size();
      Object localObject3 = this.b.entrySet().iterator();
      int i = 0;
      localObject1 = localh;
      if (!((Iterator)localObject3).hasNext()) {
        break label263;
      }
      localObject1 = (Map.Entry)((Iterator)localObject3).next();
      a locala = (a)((Map.Entry)localObject1).getValue();
      boolean bool;
      if (locala.a != null)
      {
        if (i != j - 1) {
          break label212;
        }
        bool = true;
        label167:
        if (locala.c == null) {
          break label217;
        }
        localh.a((String)((Map.Entry)localObject1).getKey(), locala.a(), locala.a, locala.c, bool);
      }
      for (;;)
      {
        i++;
        break;
        label212:
        bool = false;
        break label167;
        label217:
        localh.a((String)((Map.Entry)localObject1).getKey(), locala.a(), locala.a, bool);
      }
    }
    try
    {
      localObject1 = new org/apache/http/client/entity/UrlEncodedFormEntity;
      ((UrlEncodedFormEntity)localObject1).<init>(b(), c);
      label263:
      return (HttpEntity)localObject1;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;)
      {
        localUnsupportedEncodingException.printStackTrace();
        Object localObject2 = null;
      }
    }
  }
  
  public void a(String paramString)
  {
    this.a.remove(paramString);
    this.b.remove(paramString);
  }
  
  public void a(String paramString, File paramFile)
  {
    a(paramString, new FileInputStream(paramFile), paramFile.getName());
  }
  
  public void a(String paramString, InputStream paramInputStream)
  {
    a(paramString, paramInputStream, null);
  }
  
  public void a(String paramString1, InputStream paramInputStream, String paramString2)
  {
    a(paramString1, paramInputStream, paramString2, null);
  }
  
  public void a(String paramString1, InputStream paramInputStream, String paramString2, String paramString3)
  {
    if ((paramString1 != null) && (paramInputStream != null)) {
      this.b.put(paramString1, new a(paramInputStream, paramString2, paramString3));
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString2 != null)) {
      this.a.put(paramString1, paramString2);
    }
  }
  
  protected List b()
  {
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator = this.a.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localLinkedList.add(new BasicNameValuePair((String)localEntry.getKey(), (String)localEntry.getValue()));
    }
    return localLinkedList;
  }
  
  protected String c()
  {
    return URLEncodedUtils.format(b(), c);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Object localObject2 = this.a.entrySet().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (Map.Entry)((Iterator)localObject2).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject1).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append((String)((Map.Entry)localObject1).getValue());
    }
    Object localObject1 = this.b.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      if (localStringBuilder.length() > 0) {
        localStringBuilder.append("&");
      }
      localStringBuilder.append((String)((Map.Entry)localObject2).getKey());
      localStringBuilder.append("=");
      localStringBuilder.append("FILE");
    }
    return localStringBuilder.toString();
  }
  
  private static class a
  {
    public InputStream a;
    public String b;
    public String c;
    
    public a(InputStream paramInputStream, String paramString1, String paramString2)
    {
      this.a = paramInputStream;
      this.b = paramString1;
      this.c = paramString2;
    }
    
    public String a()
    {
      if (this.b != null) {}
      for (String str = this.b;; str = "nofilename") {
        return str;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */