package org.joda.time;

import java.io.Serializable;
import java.math.RoundingMode;
import org.joda.convert.FromString;
import org.joda.time.base.BaseDuration;
import org.joda.time.field.FieldUtils;

public final class Duration
  extends BaseDuration
  implements ReadableDuration, Serializable
{
  public static final Duration ZERO = new Duration(0L);
  private static final long serialVersionUID = 2471658376918L;
  
  public Duration(long paramLong)
  {
    super(paramLong);
  }
  
  public Duration(long paramLong1, long paramLong2)
  {
    super(paramLong1, paramLong2);
  }
  
  public Duration(Object paramObject)
  {
    super(paramObject);
  }
  
  public Duration(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    super(paramReadableInstant1, paramReadableInstant2);
  }
  
  public static Duration millis(long paramLong)
  {
    if (paramLong == 0L) {}
    for (Duration localDuration = ZERO;; localDuration = new Duration(paramLong)) {
      return localDuration;
    }
  }
  
  @FromString
  public static Duration parse(String paramString)
  {
    return new Duration(paramString);
  }
  
  public static Duration standardDays(long paramLong)
  {
    if (paramLong == 0L) {}
    for (Duration localDuration = ZERO;; localDuration = new Duration(FieldUtils.safeMultiply(paramLong, 86400000))) {
      return localDuration;
    }
  }
  
  public static Duration standardHours(long paramLong)
  {
    if (paramLong == 0L) {}
    for (Duration localDuration = ZERO;; localDuration = new Duration(FieldUtils.safeMultiply(paramLong, 3600000))) {
      return localDuration;
    }
  }
  
  public static Duration standardMinutes(long paramLong)
  {
    if (paramLong == 0L) {}
    for (Duration localDuration = ZERO;; localDuration = new Duration(FieldUtils.safeMultiply(paramLong, 60000))) {
      return localDuration;
    }
  }
  
  public static Duration standardSeconds(long paramLong)
  {
    if (paramLong == 0L) {}
    for (Duration localDuration = ZERO;; localDuration = new Duration(FieldUtils.safeMultiply(paramLong, 1000))) {
      return localDuration;
    }
  }
  
  public Duration dividedBy(long paramLong)
  {
    if (paramLong == 1L) {}
    for (Duration localDuration = this;; localDuration = new Duration(FieldUtils.safeDivide(getMillis(), paramLong))) {
      return localDuration;
    }
  }
  
  public Duration dividedBy(long paramLong, RoundingMode paramRoundingMode)
  {
    if (paramLong == 1L) {}
    for (paramRoundingMode = this;; paramRoundingMode = new Duration(FieldUtils.safeDivide(getMillis(), paramLong, paramRoundingMode))) {
      return paramRoundingMode;
    }
  }
  
  public long getStandardDays()
  {
    return getMillis() / 86400000L;
  }
  
  public long getStandardHours()
  {
    return getMillis() / 3600000L;
  }
  
  public long getStandardMinutes()
  {
    return getMillis() / 60000L;
  }
  
  public long getStandardSeconds()
  {
    return getMillis() / 1000L;
  }
  
  public Duration minus(long paramLong)
  {
    return withDurationAdded(paramLong, -1);
  }
  
  public Duration minus(ReadableDuration paramReadableDuration)
  {
    if (paramReadableDuration == null) {}
    for (paramReadableDuration = this;; paramReadableDuration = withDurationAdded(paramReadableDuration.getMillis(), -1)) {
      return paramReadableDuration;
    }
  }
  
  public Duration multipliedBy(long paramLong)
  {
    if (paramLong == 1L) {}
    for (Duration localDuration = this;; localDuration = new Duration(FieldUtils.safeMultiply(getMillis(), paramLong))) {
      return localDuration;
    }
  }
  
  public Duration negated()
  {
    if (getMillis() == Long.MIN_VALUE) {
      throw new ArithmeticException("Negation of this duration would overflow");
    }
    return new Duration(-getMillis());
  }
  
  public Duration plus(long paramLong)
  {
    return withDurationAdded(paramLong, 1);
  }
  
  public Duration plus(ReadableDuration paramReadableDuration)
  {
    if (paramReadableDuration == null) {}
    for (paramReadableDuration = this;; paramReadableDuration = withDurationAdded(paramReadableDuration.getMillis(), 1)) {
      return paramReadableDuration;
    }
  }
  
  public Duration toDuration()
  {
    return this;
  }
  
  public Days toStandardDays()
  {
    return Days.days(FieldUtils.safeToInt(getStandardDays()));
  }
  
  public Hours toStandardHours()
  {
    return Hours.hours(FieldUtils.safeToInt(getStandardHours()));
  }
  
  public Minutes toStandardMinutes()
  {
    return Minutes.minutes(FieldUtils.safeToInt(getStandardMinutes()));
  }
  
  public Seconds toStandardSeconds()
  {
    return Seconds.seconds(FieldUtils.safeToInt(getStandardSeconds()));
  }
  
  public Duration withDurationAdded(long paramLong, int paramInt)
  {
    Duration localDuration = this;
    if (paramLong != 0L) {
      if (paramInt != 0) {
        break label19;
      }
    }
    for (localDuration = this;; localDuration = new Duration(FieldUtils.safeAdd(getMillis(), paramLong)))
    {
      return localDuration;
      label19:
      paramLong = FieldUtils.safeMultiply(paramLong, paramInt);
    }
  }
  
  public Duration withDurationAdded(ReadableDuration paramReadableDuration, int paramInt)
  {
    Duration localDuration = this;
    if (paramReadableDuration != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localDuration = this;; localDuration = withDurationAdded(paramReadableDuration.getMillis(), paramInt)) {
      return localDuration;
    }
  }
  
  public Duration withMillis(long paramLong)
  {
    if (paramLong == getMillis()) {}
    for (Duration localDuration = this;; localDuration = new Duration(paramLong)) {
      return localDuration;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Duration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */