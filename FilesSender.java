package io.fabric.sdk.android.services.events;

import java.io.File;
import java.util.List;

public abstract interface FilesSender
{
  public abstract boolean send(List<File> paramList);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\events\FilesSender.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */