package tech.dcube.companion.adapters;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.listeners.MeterInteractionListener;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBDevice;

public class HomeMetersAdapter
  extends RecyclerView.Adapter<HomeMetersViewHolder>
{
  private static final String SETTINGS_FUNDS_LOW = "_funds_low";
  private static final String SETTINGS_LOW_INK_LEVEL = "_low_ink_level";
  private static final String SETTINGS_METER_ERROR = "_low_ink_level";
  private static final String SETTINGS_SOFTWARE_RATE_UPDATES = "_software_rate_updates";
  private static final String SHARED_PREFERENCES_KEY = "COMPANION_APP_SPREF_KEY_";
  private MeterInteractionListener listener;
  boolean low_ink_level = true;
  HomeMetersViewHolder mHolder;
  List<PBDevice> mMeterItemList;
  boolean meter_error = true;
  boolean meter_funds_low = true;
  SharedPreferences sharedPreferences;
  boolean software_rate_update = true;
  
  public HomeMetersAdapter(List<PBDevice> paramList, MeterInteractionListener paramMeterInteractionListener)
  {
    this.mMeterItemList = paramList;
    this.listener = paramMeterInteractionListener;
  }
  
  public int getItemCount()
  {
    return this.mMeterItemList.size();
  }
  
  boolean getLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.low_ink_level = this.sharedPreferences.getBoolean(paramString1, true);
    return this.low_ink_level;
  }
  
  boolean getMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.meter_error = this.sharedPreferences.getBoolean(paramString1, true);
    return this.meter_error;
  }
  
  boolean getMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    this.meter_funds_low = this.sharedPreferences.getBoolean(paramString1, true);
    return this.meter_funds_low;
  }
  
  void getPreferences(String paramString1, String paramString2)
  {
    String str2 = paramString1 + "_" + paramString2 + "_low_ink_level";
    String str3 = paramString1 + "_" + paramString2 + "_funds_low";
    String str1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.low_ink_level = this.sharedPreferences.getBoolean(str2, true);
    this.meter_funds_low = this.sharedPreferences.getBoolean(str3, true);
    this.meter_error = this.sharedPreferences.getBoolean(str1, true);
    this.software_rate_update = this.sharedPreferences.getBoolean(paramString1, true);
  }
  
  boolean getSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.software_rate_update = this.sharedPreferences.getBoolean(paramString1, true);
    return this.software_rate_update;
  }
  
  public void onBindViewHolder(final HomeMetersViewHolder paramHomeMetersViewHolder, int paramInt)
  {
    String str4 = ((PBDevice)this.mMeterItemList.get(paramInt)).getModelNumber();
    String str2 = ((PBDevice)this.mMeterItemList.get(paramInt)).getSerialNumber();
    paramHomeMetersViewHolder.pos = paramInt;
    paramHomeMetersViewHolder.meterModel = str4;
    paramHomeMetersViewHolder.meterSerial = str2;
    this.mHolder = paramHomeMetersViewHolder;
    String str3 = (String)DataManager.getInstance().getMeterSerialToNickName().get(str2);
    String str1 = str3;
    if (str3 == "") {
      str1 = str2;
    }
    double d = ((PBDevice)this.mMeterItemList.get(paramInt)).getMeterBalance().floatValue();
    str3 = new DecimalFormat("##.##").format(d);
    paramHomeMetersViewHolder.mMeterNameTV.setText(str1);
    paramHomeMetersViewHolder.mMeterAmountTV.setText(str3);
    updateMeterNotifications(str4, str2);
    paramHomeMetersViewHolder.mAddFundsIB.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (HomeMetersAdapter.this.listener != null) {
          HomeMetersAdapter.this.listener.onMeterAddFunButtonTapped((PBDevice)HomeMetersAdapter.this.mMeterItemList.get(paramHomeMetersViewHolder.getAdapterPosition()));
        }
      }
    });
    paramHomeMetersViewHolder.mStatusIV.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (HomeMetersAdapter.this.listener != null) {
          HomeMetersAdapter.this.listener.onMeterErrorClicked(paramHomeMetersViewHolder.meterModel, paramHomeMetersViewHolder.meterSerial);
        }
      }
    });
    paramHomeMetersViewHolder.mAlertIV.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (HomeMetersAdapter.this.listener != null) {
          HomeMetersAdapter.this.listener.onMeterWarningClicked(paramHomeMetersViewHolder.meterModel, paramHomeMetersViewHolder.meterSerial);
        }
      }
    });
  }
  
  public HomeMetersViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new HomeMetersViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968688, paramViewGroup, false));
  }
  
  public void refreshView()
  {
    notifyDataSetChanged();
  }
  
  void saveLowInkLevelPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.sharedPreferences.edit().putBoolean(paramString1, this.low_ink_level);
  }
  
  void saveMeterErrorPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    this.sharedPreferences.edit().putBoolean(paramString1, this.meter_error);
  }
  
  void saveMeterFundsLowPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_funds_low";
    this.sharedPreferences.edit().putBoolean(paramString1, this.meter_funds_low);
  }
  
  void savePreferences(String paramString1, String paramString2)
  {
    String str1 = paramString1 + "_" + paramString2 + "_low_ink_level";
    String str2 = paramString1 + "_" + paramString2 + "_funds_low";
    String str3 = paramString1 + "_" + paramString2 + "_low_ink_level";
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    paramString2 = this.sharedPreferences.edit();
    paramString2.putBoolean(str1, this.low_ink_level);
    paramString2.putBoolean(str2, this.meter_funds_low);
    paramString2.putBoolean(str3, this.meter_error);
    paramString2.putBoolean(paramString1, this.software_rate_update);
    paramString2.commit();
  }
  
  void saveSoftwareRateUpdatesPreference(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + "_" + paramString2 + "_software_rate_updates";
    this.sharedPreferences.edit().putBoolean(paramString1, this.software_rate_update);
  }
  
  public void updateData(List<PBDevice> paramList)
  {
    this.mMeterItemList.clear();
    this.mMeterItemList.addAll(paramList);
    notifyDataSetChanged();
  }
  
  public void updateMeterNotifications(String paramString1, String paramString2)
  {
    try
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      paramString1 = paramString1 + paramString2;
      try
      {
        this.mHolder.mStatusIV.setVisibility(4);
        this.mHolder.mAlertIV.setVisibility(4);
        this.mHolder.mStatusWorkingIV.setVisibility(4);
        Map localMap2 = DataManager.getInstance().getMeterModelSerialNotificationError();
        Map localMap1 = DataManager.getInstance().getMeterModelSerialNotificationWarning();
        paramString2 = new java/lang/Boolean;
        paramString2.<init>(Boolean.FALSE.booleanValue());
        localObject = new java/lang/Boolean;
        ((Boolean)localObject).<init>(Boolean.FALSE.booleanValue());
        if (localMap2.containsKey(paramString1)) {
          paramString2 = (Boolean)localMap2.get(paramString1);
        }
        if (localMap1.containsKey(paramString1)) {
          localObject = (Boolean)localMap1.get(paramString1);
        }
        if (!paramString2.booleanValue()) {
          break label218;
        }
        this.mHolder.mStatusIV.setVisibility(0);
        if (!((Boolean)localObject).booleanValue()) {
          break label240;
        }
        this.mHolder.mAlertIV.setVisibility(0);
      }
      catch (Exception paramString1)
      {
        for (;;)
        {
          paramString1.printStackTrace();
          continue;
          this.mHolder.mAlertIV.setVisibility(4);
          continue;
          this.mHolder.mStatusWorkingIV.setVisibility(4);
        }
      }
      if ((!paramString2.booleanValue()) && (!((Boolean)localObject).booleanValue()))
      {
        this.mHolder.mStatusWorkingIV.setVisibility(0);
        return;
      }
    }
    catch (Exception paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
        paramString1 = "empty";
        continue;
        label218:
        this.mHolder.mStatusIV.setVisibility(4);
      }
    }
  }
  
  public class HomeMetersViewHolder
    extends RecyclerView.ViewHolder
  {
    public ImageButton mAddFundsIB;
    public ImageView mAlertIV;
    public TextView mMeterAmountTV;
    public TextView mMeterNameTV;
    public ImageView mStatusIV;
    public ImageView mStatusWorkingIV;
    public View mView;
    public String meterModel;
    public String meterSerial;
    public int pos;
    
    public HomeMetersViewHolder(View paramView)
    {
      super();
      this.mView = paramView;
      this.mStatusWorkingIV = ((ImageView)this.mView.findViewById(2131624303));
      this.mStatusIV = ((ImageView)this.mView.findViewById(2131624304));
      this.mAlertIV = ((ImageView)this.mView.findViewById(2131624305));
      this.mMeterNameTV = ((TextView)this.mView.findViewById(2131624306));
      this.mMeterAmountTV = ((TextView)this.mView.findViewById(2131624307));
      this.mAddFundsIB = ((ImageButton)this.mView.findViewById(2131624309));
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\adapters\HomeMetersAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */