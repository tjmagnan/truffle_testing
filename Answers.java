package com.crashlytics.android.answers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.Crash.FatalException;
import io.fabric.sdk.android.services.common.Crash.LoggedException;
import io.fabric.sdk.android.services.settings.FeaturesSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.io.File;

public class Answers
  extends Kit<Boolean>
{
  static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  public static final String TAG = "Answers";
  SessionAnalyticsManager analyticsManager;
  
  public static Answers getInstance()
  {
    return (Answers)Fabric.getKit(Answers.class);
  }
  
  protected Boolean doInBackground()
  {
    for (;;)
    {
      try
      {
        localObject = Settings.getInstance().awaitSettingsData();
        if (localObject != null) {
          continue;
        }
        Fabric.getLogger().e("Answers", "Failed to retrieve settings");
        localObject = Boolean.valueOf(false);
      }
      catch (Exception localException)
      {
        Object localObject;
        Fabric.getLogger().e("Answers", "Error dealing with settings", localException);
        Boolean localBoolean = Boolean.valueOf(false);
        continue;
      }
      return (Boolean)localObject;
      if (((SettingsData)localObject).featuresData.collectAnalytics)
      {
        Fabric.getLogger().d("Answers", "Analytics collection enabled");
        this.analyticsManager.setAnalyticsSettingsData(((SettingsData)localObject).analyticsSettingsData, getOverridenSpiEndpoint());
        localObject = Boolean.valueOf(true);
      }
      else
      {
        Fabric.getLogger().d("Answers", "Analytics collection disabled");
        this.analyticsManager.disable();
        localObject = Boolean.valueOf(false);
      }
    }
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android:answers";
  }
  
  String getOverridenSpiEndpoint()
  {
    return CommonUtils.getStringsFileValue(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  public String getVersion()
  {
    return "1.3.13.dev";
  }
  
  public void logAddToCart(AddToCartEvent paramAddToCartEvent)
  {
    if (paramAddToCartEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramAddToCartEvent);
    }
  }
  
  public void logContentView(ContentViewEvent paramContentViewEvent)
  {
    if (paramContentViewEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramContentViewEvent);
    }
  }
  
  public void logCustom(CustomEvent paramCustomEvent)
  {
    if (paramCustomEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onCustom(paramCustomEvent);
    }
  }
  
  public void logInvite(InviteEvent paramInviteEvent)
  {
    if (paramInviteEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramInviteEvent);
    }
  }
  
  public void logLevelEnd(LevelEndEvent paramLevelEndEvent)
  {
    if (paramLevelEndEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramLevelEndEvent);
    }
  }
  
  public void logLevelStart(LevelStartEvent paramLevelStartEvent)
  {
    if (paramLevelStartEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramLevelStartEvent);
    }
  }
  
  public void logLogin(LoginEvent paramLoginEvent)
  {
    if (paramLoginEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramLoginEvent);
    }
  }
  
  public void logPurchase(PurchaseEvent paramPurchaseEvent)
  {
    if (paramPurchaseEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramPurchaseEvent);
    }
  }
  
  public void logRating(RatingEvent paramRatingEvent)
  {
    if (paramRatingEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramRatingEvent);
    }
  }
  
  public void logSearch(SearchEvent paramSearchEvent)
  {
    if (paramSearchEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramSearchEvent);
    }
  }
  
  public void logShare(ShareEvent paramShareEvent)
  {
    if (paramShareEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramShareEvent);
    }
  }
  
  public void logSignUp(SignUpEvent paramSignUpEvent)
  {
    if (paramSignUpEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramSignUpEvent);
    }
  }
  
  public void logStartCheckout(StartCheckoutEvent paramStartCheckoutEvent)
  {
    if (paramStartCheckoutEvent == null) {
      throw new NullPointerException("event must not be null");
    }
    if (this.analyticsManager != null) {
      this.analyticsManager.onPredefined(paramStartCheckoutEvent);
    }
  }
  
  public void onException(Crash.FatalException paramFatalException)
  {
    if (this.analyticsManager != null) {
      this.analyticsManager.onCrash(paramFatalException.getSessionId(), paramFatalException.getExceptionName());
    }
  }
  
  public void onException(Crash.LoggedException paramLoggedException)
  {
    if (this.analyticsManager != null) {
      this.analyticsManager.onError(paramLoggedException.getSessionId());
    }
  }
  
  @SuppressLint({"NewApi"})
  protected boolean onPreExecute()
  {
    for (;;)
    {
      try
      {
        Context localContext = getContext();
        localObject1 = localContext.getPackageManager();
        localObject2 = localContext.getPackageName();
        localPackageInfo = ((PackageManager)localObject1).getPackageInfo((String)localObject2, 0);
        String str2 = Integer.toString(localPackageInfo.versionCode);
        if (localPackageInfo.versionName != null) {
          continue;
        }
        str1 = "0.0";
        if (Build.VERSION.SDK_INT < 9) {
          continue;
        }
        l = localPackageInfo.firstInstallTime;
        this.analyticsManager = SessionAnalyticsManager.build(this, localContext, getIdManager(), str2, str1, l);
        this.analyticsManager.enable();
        bool = true;
      }
      catch (Exception localException)
      {
        Object localObject1;
        Object localObject2;
        PackageInfo localPackageInfo;
        String str1;
        long l;
        Fabric.getLogger().e("Answers", "Error retrieving app properties", localException);
        boolean bool = false;
        continue;
      }
      return bool;
      str1 = localPackageInfo.versionName;
      continue;
      localObject1 = ((PackageManager)localObject1).getApplicationInfo((String)localObject2, 0);
      localObject2 = new java/io/File;
      ((File)localObject2).<init>(((ApplicationInfo)localObject1).sourceDir);
      l = ((File)localObject2).lastModified();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\answers\Answers.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */