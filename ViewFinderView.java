package me.dm7.barcodescanner.core;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class ViewFinderView
  extends View
  implements IViewFinder
{
  private static final long ANIMATION_DELAY = 80L;
  private static final float DEFAULT_SQUARE_DIMENSION_RATIO = 0.625F;
  private static final float LANDSCAPE_HEIGHT_RATIO = 0.625F;
  private static final float LANDSCAPE_WIDTH_HEIGHT_RATIO = 1.4F;
  private static final int MIN_DIMENSION_DIFF = 50;
  private static final int POINT_SIZE = 10;
  private static final float PORTRAIT_WIDTH_HEIGHT_RATIO = 0.75F;
  private static final float PORTRAIT_WIDTH_RATIO = 0.75F;
  private static final int[] SCANNER_ALPHA = { 0, 64, 128, 192, 255, 192, 128, 64 };
  private static final String TAG = "ViewFinderView";
  protected int mBorderLineLength;
  protected Paint mBorderPaint;
  private float mBordersAlpha;
  private final int mDefaultBorderColor = getResources().getColor(R.color.viewfinder_border);
  private final int mDefaultBorderLineLength = getResources().getInteger(R.integer.viewfinder_border_length);
  private final int mDefaultBorderStrokeWidth = getResources().getInteger(R.integer.viewfinder_border_width);
  private final int mDefaultLaserColor = getResources().getColor(R.color.viewfinder_laser);
  private final int mDefaultMaskColor = getResources().getColor(R.color.viewfinder_mask);
  protected Paint mFinderMaskPaint;
  private Rect mFramingRect;
  private boolean mIsLaserEnabled;
  protected Paint mLaserPaint;
  protected boolean mSquareViewFinder;
  private int mViewFinderOffset = 0;
  private int scannerAlpha;
  
  public ViewFinderView(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public ViewFinderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init()
  {
    this.mLaserPaint = new Paint();
    this.mLaserPaint.setColor(this.mDefaultLaserColor);
    this.mLaserPaint.setStyle(Paint.Style.FILL);
    this.mFinderMaskPaint = new Paint();
    this.mFinderMaskPaint.setColor(this.mDefaultMaskColor);
    this.mBorderPaint = new Paint();
    this.mBorderPaint.setColor(this.mDefaultBorderColor);
    this.mBorderPaint.setStyle(Paint.Style.STROKE);
    this.mBorderPaint.setStrokeWidth(this.mDefaultBorderStrokeWidth);
    this.mBorderPaint.setAntiAlias(true);
    this.mBorderLineLength = this.mDefaultBorderLineLength;
  }
  
  public void drawLaser(Canvas paramCanvas)
  {
    Rect localRect = getFramingRect();
    this.mLaserPaint.setAlpha(SCANNER_ALPHA[this.scannerAlpha]);
    this.scannerAlpha = ((this.scannerAlpha + 1) % SCANNER_ALPHA.length);
    int i = localRect.height() / 2 + localRect.top;
    paramCanvas.drawRect(localRect.left + 2, i - 1, localRect.right - 1, i + 2, this.mLaserPaint);
    postInvalidateDelayed(80L, localRect.left - 10, localRect.top - 10, localRect.right + 10, localRect.bottom + 10);
  }
  
  public void drawViewFinderBorder(Canvas paramCanvas)
  {
    Rect localRect = getFramingRect();
    Path localPath = new Path();
    localPath.moveTo(localRect.left, localRect.top + this.mBorderLineLength);
    localPath.lineTo(localRect.left, localRect.top);
    localPath.lineTo(localRect.left + this.mBorderLineLength, localRect.top);
    paramCanvas.drawPath(localPath, this.mBorderPaint);
    localPath.moveTo(localRect.right, localRect.top + this.mBorderLineLength);
    localPath.lineTo(localRect.right, localRect.top);
    localPath.lineTo(localRect.right - this.mBorderLineLength, localRect.top);
    paramCanvas.drawPath(localPath, this.mBorderPaint);
    localPath.moveTo(localRect.right, localRect.bottom - this.mBorderLineLength);
    localPath.lineTo(localRect.right, localRect.bottom);
    localPath.lineTo(localRect.right - this.mBorderLineLength, localRect.bottom);
    paramCanvas.drawPath(localPath, this.mBorderPaint);
    localPath.moveTo(localRect.left, localRect.bottom - this.mBorderLineLength);
    localPath.lineTo(localRect.left, localRect.bottom);
    localPath.lineTo(localRect.left + this.mBorderLineLength, localRect.bottom);
    paramCanvas.drawPath(localPath, this.mBorderPaint);
  }
  
  public void drawViewFinderMask(Canvas paramCanvas)
  {
    int i = paramCanvas.getWidth();
    int j = paramCanvas.getHeight();
    Rect localRect = getFramingRect();
    paramCanvas.drawRect(0.0F, 0.0F, i, localRect.top, this.mFinderMaskPaint);
    paramCanvas.drawRect(0.0F, localRect.top, localRect.left, localRect.bottom + 1, this.mFinderMaskPaint);
    paramCanvas.drawRect(localRect.right + 1, localRect.top, i, localRect.bottom + 1, this.mFinderMaskPaint);
    paramCanvas.drawRect(0.0F, localRect.bottom + 1, i, j, this.mFinderMaskPaint);
  }
  
  public Rect getFramingRect()
  {
    return this.mFramingRect;
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    if (getFramingRect() == null) {}
    for (;;)
    {
      return;
      drawViewFinderMask(paramCanvas);
      drawViewFinderBorder(paramCanvas);
      if (this.mIsLaserEnabled) {
        drawLaser(paramCanvas);
      }
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    updateFramingRect();
  }
  
  public void setBorderAlpha(float paramFloat)
  {
    int i = (int)(255.0F * paramFloat);
    this.mBordersAlpha = paramFloat;
    this.mBorderPaint.setAlpha(i);
  }
  
  public void setBorderColor(int paramInt)
  {
    this.mBorderPaint.setColor(paramInt);
  }
  
  public void setBorderCornerRadius(int paramInt)
  {
    this.mBorderPaint.setPathEffect(new CornerPathEffect(paramInt));
  }
  
  public void setBorderCornerRounded(boolean paramBoolean)
  {
    if (paramBoolean) {
      this.mBorderPaint.setStrokeJoin(Paint.Join.ROUND);
    }
    for (;;)
    {
      return;
      this.mBorderPaint.setStrokeJoin(Paint.Join.BEVEL);
    }
  }
  
  public void setBorderLineLength(int paramInt)
  {
    this.mBorderLineLength = paramInt;
  }
  
  public void setBorderStrokeWidth(int paramInt)
  {
    this.mBorderPaint.setStrokeWidth(paramInt);
  }
  
  public void setLaserColor(int paramInt)
  {
    this.mLaserPaint.setColor(paramInt);
  }
  
  public void setLaserEnabled(boolean paramBoolean)
  {
    this.mIsLaserEnabled = paramBoolean;
  }
  
  public void setMaskColor(int paramInt)
  {
    this.mFinderMaskPaint.setColor(paramInt);
  }
  
  public void setSquareViewFinder(boolean paramBoolean)
  {
    this.mSquareViewFinder = paramBoolean;
  }
  
  public void setViewFinderOffset(int paramInt)
  {
    this.mViewFinderOffset = paramInt;
  }
  
  public void setupViewFinder()
  {
    updateFramingRect();
    invalidate();
  }
  
  /* Error */
  public void updateFramingRect()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 264	android/graphics/Point
    //   5: astore 5
    //   7: aload 5
    //   9: aload_0
    //   10: invokevirtual 265	me/dm7/barcodescanner/core/ViewFinderView:getWidth	()I
    //   13: aload_0
    //   14: invokevirtual 266	me/dm7/barcodescanner/core/ViewFinderView:getHeight	()I
    //   17: invokespecial 269	android/graphics/Point:<init>	(II)V
    //   20: aload_0
    //   21: invokevirtual 273	me/dm7/barcodescanner/core/ViewFinderView:getContext	()Landroid/content/Context;
    //   24: invokestatic 279	me/dm7/barcodescanner/core/DisplayUtils:getScreenOrientation	(Landroid/content/Context;)I
    //   27: istore_1
    //   28: aload_0
    //   29: getfield 257	me/dm7/barcodescanner/core/ViewFinderView:mSquareViewFinder	Z
    //   32: ifeq +141 -> 173
    //   35: iload_1
    //   36: iconst_1
    //   37: if_icmpeq +121 -> 158
    //   40: aload_0
    //   41: invokevirtual 266	me/dm7/barcodescanner/core/ViewFinderView:getHeight	()I
    //   44: i2f
    //   45: ldc 13
    //   47: fmul
    //   48: f2i
    //   49: istore_1
    //   50: iload_1
    //   51: istore_2
    //   52: iload_2
    //   53: istore_3
    //   54: iload_2
    //   55: aload_0
    //   56: invokevirtual 265	me/dm7/barcodescanner/core/ViewFinderView:getWidth	()I
    //   59: if_icmple +11 -> 70
    //   62: aload_0
    //   63: invokevirtual 265	me/dm7/barcodescanner/core/ViewFinderView:getWidth	()I
    //   66: bipush 50
    //   68: isub
    //   69: istore_3
    //   70: iload_1
    //   71: istore_2
    //   72: iload_1
    //   73: aload_0
    //   74: invokevirtual 266	me/dm7/barcodescanner/core/ViewFinderView:getHeight	()I
    //   77: if_icmple +11 -> 88
    //   80: aload_0
    //   81: invokevirtual 266	me/dm7/barcodescanner/core/ViewFinderView:getHeight	()I
    //   84: bipush 50
    //   86: isub
    //   87: istore_2
    //   88: aload 5
    //   90: getfield 282	android/graphics/Point:x	I
    //   93: iload_3
    //   94: isub
    //   95: iconst_2
    //   96: idiv
    //   97: istore 4
    //   99: aload 5
    //   101: getfield 285	android/graphics/Point:y	I
    //   104: iload_2
    //   105: isub
    //   106: iconst_2
    //   107: idiv
    //   108: istore_1
    //   109: new 157	android/graphics/Rect
    //   112: astore 5
    //   114: aload 5
    //   116: aload_0
    //   117: getfield 101	me/dm7/barcodescanner/core/ViewFinderView:mViewFinderOffset	I
    //   120: iload 4
    //   122: iadd
    //   123: aload_0
    //   124: getfield 101	me/dm7/barcodescanner/core/ViewFinderView:mViewFinderOffset	I
    //   127: iload_1
    //   128: iadd
    //   129: iload 4
    //   131: iload_3
    //   132: iadd
    //   133: aload_0
    //   134: getfield 101	me/dm7/barcodescanner/core/ViewFinderView:mViewFinderOffset	I
    //   137: isub
    //   138: iload_1
    //   139: iload_2
    //   140: iadd
    //   141: aload_0
    //   142: getfield 101	me/dm7/barcodescanner/core/ViewFinderView:mViewFinderOffset	I
    //   145: isub
    //   146: invokespecial 287	android/graphics/Rect:<init>	(IIII)V
    //   149: aload_0
    //   150: aload 5
    //   152: putfield 207	me/dm7/barcodescanner/core/ViewFinderView:mFramingRect	Landroid/graphics/Rect;
    //   155: aload_0
    //   156: monitorexit
    //   157: return
    //   158: aload_0
    //   159: invokevirtual 265	me/dm7/barcodescanner/core/ViewFinderView:getWidth	()I
    //   162: i2f
    //   163: ldc 13
    //   165: fmul
    //   166: f2i
    //   167: istore_2
    //   168: iload_2
    //   169: istore_1
    //   170: goto -118 -> 52
    //   173: iload_1
    //   174: iconst_1
    //   175: if_icmpeq +23 -> 198
    //   178: aload_0
    //   179: invokevirtual 266	me/dm7/barcodescanner/core/ViewFinderView:getHeight	()I
    //   182: i2f
    //   183: ldc 13
    //   185: fmul
    //   186: f2i
    //   187: istore_1
    //   188: ldc 16
    //   190: iload_1
    //   191: i2f
    //   192: fmul
    //   193: f2i
    //   194: istore_2
    //   195: goto -143 -> 52
    //   198: aload_0
    //   199: invokevirtual 265	me/dm7/barcodescanner/core/ViewFinderView:getWidth	()I
    //   202: istore_1
    //   203: iload_1
    //   204: i2f
    //   205: ldc 23
    //   207: fmul
    //   208: f2i
    //   209: istore_2
    //   210: iload_2
    //   211: i2f
    //   212: ldc 23
    //   214: fmul
    //   215: f2i
    //   216: istore_1
    //   217: goto -165 -> 52
    //   220: astore 5
    //   222: aload_0
    //   223: monitorexit
    //   224: aload 5
    //   226: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	227	0	this	ViewFinderView
    //   27	190	1	i	int
    //   51	160	2	j	int
    //   53	80	3	k	int
    //   97	36	4	m	int
    //   5	146	5	localObject1	Object
    //   220	5	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   2	35	220	finally
    //   40	50	220	finally
    //   54	70	220	finally
    //   72	88	220	finally
    //   88	155	220	finally
    //   158	168	220	finally
    //   178	188	220	finally
    //   198	203	220	finally
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\ViewFinderView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */