package tech.dcube.companion;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "tech.dcube.companion";
  public static final String BUILD_TYPE = "debug";
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "0.7.9.1";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */