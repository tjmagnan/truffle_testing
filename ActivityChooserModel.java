package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ActivityChooserModel
  extends DataSetObservable
{
  static final String ATTRIBUTE_ACTIVITY = "activity";
  static final String ATTRIBUTE_TIME = "time";
  static final String ATTRIBUTE_WEIGHT = "weight";
  static final boolean DEBUG = false;
  private static final int DEFAULT_ACTIVITY_INFLATION = 5;
  private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0F;
  public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
  public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
  private static final String HISTORY_FILE_EXTENSION = ".xml";
  private static final int INVALID_INDEX = -1;
  static final String LOG_TAG = ActivityChooserModel.class.getSimpleName();
  static final String TAG_HISTORICAL_RECORD = "historical-record";
  static final String TAG_HISTORICAL_RECORDS = "historical-records";
  private static final Map<String, ActivityChooserModel> sDataModelRegistry = new HashMap();
  private static final Object sRegistryLock = new Object();
  private final List<ActivityResolveInfo> mActivities = new ArrayList();
  private OnChooseActivityListener mActivityChoserModelPolicy;
  private ActivitySorter mActivitySorter = new DefaultSorter();
  boolean mCanReadHistoricalData = true;
  final Context mContext;
  private final List<HistoricalRecord> mHistoricalRecords = new ArrayList();
  private boolean mHistoricalRecordsChanged = true;
  final String mHistoryFileName;
  private int mHistoryMaxSize = 50;
  private final Object mInstanceLock = new Object();
  private Intent mIntent;
  private boolean mReadShareHistoryCalled = false;
  private boolean mReloadActivities = false;
  
  private ActivityChooserModel(Context paramContext, String paramString)
  {
    this.mContext = paramContext.getApplicationContext();
    if ((!TextUtils.isEmpty(paramString)) && (!paramString.endsWith(".xml"))) {}
    for (this.mHistoryFileName = (paramString + ".xml");; this.mHistoryFileName = paramString) {
      return;
    }
  }
  
  private boolean addHistoricalRecord(HistoricalRecord paramHistoricalRecord)
  {
    boolean bool = this.mHistoricalRecords.add(paramHistoricalRecord);
    if (bool)
    {
      this.mHistoricalRecordsChanged = true;
      pruneExcessiveHistoricalRecordsIfNeeded();
      persistHistoricalDataIfNeeded();
      sortActivitiesIfNeeded();
      notifyChanged();
    }
    return bool;
  }
  
  private void ensureConsistentState()
  {
    boolean bool1 = loadActivitiesIfNeeded();
    boolean bool2 = readHistoricalDataIfNeeded();
    pruneExcessiveHistoricalRecordsIfNeeded();
    if ((bool1 | bool2))
    {
      sortActivitiesIfNeeded();
      notifyChanged();
    }
  }
  
  public static ActivityChooserModel get(Context paramContext, String paramString)
  {
    synchronized (sRegistryLock)
    {
      ActivityChooserModel localActivityChooserModel2 = (ActivityChooserModel)sDataModelRegistry.get(paramString);
      ActivityChooserModel localActivityChooserModel1 = localActivityChooserModel2;
      if (localActivityChooserModel2 == null)
      {
        localActivityChooserModel1 = new android/support/v7/widget/ActivityChooserModel;
        localActivityChooserModel1.<init>(paramContext, paramString);
        sDataModelRegistry.put(paramString, localActivityChooserModel1);
      }
      return localActivityChooserModel1;
    }
  }
  
  private boolean loadActivitiesIfNeeded()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (this.mReloadActivities)
    {
      bool1 = bool2;
      if (this.mIntent != null)
      {
        this.mReloadActivities = false;
        this.mActivities.clear();
        List localList = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0);
        int j = localList.size();
        for (int i = 0; i < j; i++)
        {
          ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i);
          this.mActivities.add(new ActivityResolveInfo(localResolveInfo));
        }
        bool1 = true;
      }
    }
    return bool1;
  }
  
  private void persistHistoricalDataIfNeeded()
  {
    if (!this.mReadShareHistoryCalled) {
      throw new IllegalStateException("No preceding call to #readHistoricalData");
    }
    if (!this.mHistoricalRecordsChanged) {}
    for (;;)
    {
      return;
      this.mHistoricalRecordsChanged = false;
      if (!TextUtils.isEmpty(this.mHistoryFileName)) {
        AsyncTaskCompat.executeParallel(new PersistHistoryAsyncTask(), new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
      }
    }
  }
  
  private void pruneExcessiveHistoricalRecordsIfNeeded()
  {
    int j = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
    if (j <= 0) {}
    for (;;)
    {
      return;
      this.mHistoricalRecordsChanged = true;
      for (int i = 0; i < j; i++) {
        HistoricalRecord localHistoricalRecord = (HistoricalRecord)this.mHistoricalRecords.remove(0);
      }
    }
  }
  
  private boolean readHistoricalDataIfNeeded()
  {
    boolean bool = true;
    if ((this.mCanReadHistoricalData) && (this.mHistoricalRecordsChanged) && (!TextUtils.isEmpty(this.mHistoryFileName)))
    {
      this.mCanReadHistoricalData = false;
      this.mReadShareHistoryCalled = true;
      readHistoricalDataImpl();
    }
    for (;;)
    {
      return bool;
      bool = false;
    }
  }
  
  /* Error */
  private void readHistoricalDataImpl()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 143	android/support/v7/widget/ActivityChooserModel:mContext	Landroid/content/Context;
    //   4: aload_0
    //   5: getfield 167	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   8: invokevirtual 268	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   11: astore 5
    //   13: invokestatic 274	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
    //   16: astore 6
    //   18: aload 6
    //   20: aload 5
    //   22: ldc_w 276
    //   25: invokeinterface 282 3 0
    //   30: iconst_0
    //   31: istore_2
    //   32: iload_2
    //   33: iconst_1
    //   34: if_icmpeq +22 -> 56
    //   37: iload_2
    //   38: iconst_2
    //   39: if_icmpeq +17 -> 56
    //   42: aload 6
    //   44: invokeinterface 285 1 0
    //   49: istore_2
    //   50: goto -18 -> 32
    //   53: astore 5
    //   55: return
    //   56: ldc 61
    //   58: aload 6
    //   60: invokeinterface 288 1 0
    //   65: invokevirtual 291	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   68: ifne +80 -> 148
    //   71: new 262	org/xmlpull/v1/XmlPullParserException
    //   74: astore 6
    //   76: aload 6
    //   78: ldc_w 293
    //   81: invokespecial 294	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   84: aload 6
    //   86: athrow
    //   87: astore 6
    //   89: getstatic 96	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
    //   92: astore 7
    //   94: new 157	java/lang/StringBuilder
    //   97: astore 8
    //   99: aload 8
    //   101: invokespecial 158	java/lang/StringBuilder:<init>	()V
    //   104: aload 7
    //   106: aload 8
    //   108: ldc_w 296
    //   111: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   114: aload_0
    //   115: getfield 167	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   118: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   124: aload 6
    //   126: invokestatic 302	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   129: pop
    //   130: aload 5
    //   132: ifnull -77 -> 55
    //   135: aload 5
    //   137: invokevirtual 307	java/io/FileInputStream:close	()V
    //   140: goto -85 -> 55
    //   143: astore 5
    //   145: goto -90 -> 55
    //   148: aload_0
    //   149: getfield 120	android/support/v7/widget/ActivityChooserModel:mHistoricalRecords	Ljava/util/List;
    //   152: astore 9
    //   154: aload 9
    //   156: invokeinterface 213 1 0
    //   161: aload 6
    //   163: invokeinterface 285 1 0
    //   168: istore_2
    //   169: iload_2
    //   170: iconst_1
    //   171: if_icmpne +21 -> 192
    //   174: aload 5
    //   176: ifnull -121 -> 55
    //   179: aload 5
    //   181: invokevirtual 307	java/io/FileInputStream:close	()V
    //   184: goto -129 -> 55
    //   187: astore 5
    //   189: goto -134 -> 55
    //   192: iload_2
    //   193: iconst_3
    //   194: if_icmpeq -33 -> 161
    //   197: iload_2
    //   198: iconst_4
    //   199: if_icmpeq -38 -> 161
    //   202: ldc 58
    //   204: aload 6
    //   206: invokeinterface 288 1 0
    //   211: invokevirtual 291	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   214: ifne +80 -> 294
    //   217: new 262	org/xmlpull/v1/XmlPullParserException
    //   220: astore 6
    //   222: aload 6
    //   224: ldc_w 309
    //   227: invokespecial 294	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   230: aload 6
    //   232: athrow
    //   233: astore 7
    //   235: getstatic 96	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
    //   238: astore 8
    //   240: new 157	java/lang/StringBuilder
    //   243: astore 6
    //   245: aload 6
    //   247: invokespecial 158	java/lang/StringBuilder:<init>	()V
    //   250: aload 8
    //   252: aload 6
    //   254: ldc_w 296
    //   257: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   260: aload_0
    //   261: getfield 167	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   264: invokevirtual 162	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   267: invokevirtual 165	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   270: aload 7
    //   272: invokestatic 302	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   275: pop
    //   276: aload 5
    //   278: ifnull -223 -> 55
    //   281: aload 5
    //   283: invokevirtual 307	java/io/FileInputStream:close	()V
    //   286: goto -231 -> 55
    //   289: astore 5
    //   291: goto -236 -> 55
    //   294: aload 6
    //   296: aconst_null
    //   297: ldc 29
    //   299: invokeinterface 313 3 0
    //   304: astore 7
    //   306: aload 6
    //   308: aconst_null
    //   309: ldc 32
    //   311: invokeinterface 313 3 0
    //   316: invokestatic 319	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   319: lstore_3
    //   320: aload 6
    //   322: aconst_null
    //   323: ldc 35
    //   325: invokeinterface 313 3 0
    //   330: invokestatic 325	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   333: fstore_1
    //   334: new 18	android/support/v7/widget/ActivityChooserModel$HistoricalRecord
    //   337: astore 8
    //   339: aload 8
    //   341: aload 7
    //   343: lload_3
    //   344: fload_1
    //   345: invokespecial 328	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:<init>	(Ljava/lang/String;JF)V
    //   348: aload 9
    //   350: aload 8
    //   352: invokeinterface 175 2 0
    //   357: pop
    //   358: goto -197 -> 161
    //   361: astore 6
    //   363: aload 5
    //   365: ifnull +8 -> 373
    //   368: aload 5
    //   370: invokevirtual 307	java/io/FileInputStream:close	()V
    //   373: aload 6
    //   375: athrow
    //   376: astore 5
    //   378: goto -5 -> 373
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	381	0	this	ActivityChooserModel
    //   333	12	1	f	float
    //   31	169	2	i	int
    //   319	25	3	l	long
    //   11	10	5	localFileInputStream	java.io.FileInputStream
    //   53	83	5	localFileNotFoundException	java.io.FileNotFoundException
    //   143	37	5	localIOException1	java.io.IOException
    //   187	95	5	localIOException2	java.io.IOException
    //   289	80	5	localIOException3	java.io.IOException
    //   376	1	5	localIOException4	java.io.IOException
    //   16	69	6	localObject1	Object
    //   87	118	6	localXmlPullParserException	org.xmlpull.v1.XmlPullParserException
    //   220	101	6	localObject2	Object
    //   361	13	6	localObject3	Object
    //   92	13	7	str1	String
    //   233	38	7	localIOException5	java.io.IOException
    //   304	38	7	str2	String
    //   97	254	8	localObject4	Object
    //   152	197	9	localList	List
    // Exception table:
    //   from	to	target	type
    //   0	13	53	java/io/FileNotFoundException
    //   13	30	87	org/xmlpull/v1/XmlPullParserException
    //   42	50	87	org/xmlpull/v1/XmlPullParserException
    //   56	87	87	org/xmlpull/v1/XmlPullParserException
    //   148	161	87	org/xmlpull/v1/XmlPullParserException
    //   161	169	87	org/xmlpull/v1/XmlPullParserException
    //   202	233	87	org/xmlpull/v1/XmlPullParserException
    //   294	358	87	org/xmlpull/v1/XmlPullParserException
    //   135	140	143	java/io/IOException
    //   179	184	187	java/io/IOException
    //   13	30	233	java/io/IOException
    //   42	50	233	java/io/IOException
    //   56	87	233	java/io/IOException
    //   148	161	233	java/io/IOException
    //   161	169	233	java/io/IOException
    //   202	233	233	java/io/IOException
    //   294	358	233	java/io/IOException
    //   281	286	289	java/io/IOException
    //   13	30	361	finally
    //   42	50	361	finally
    //   56	87	361	finally
    //   89	130	361	finally
    //   148	161	361	finally
    //   161	169	361	finally
    //   202	233	361	finally
    //   235	276	361	finally
    //   294	358	361	finally
    //   368	373	376	java/io/IOException
  }
  
  private boolean sortActivitiesIfNeeded()
  {
    if ((this.mActivitySorter != null) && (this.mIntent != null) && (!this.mActivities.isEmpty()) && (!this.mHistoricalRecords.isEmpty())) {
      this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList(this.mHistoricalRecords));
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Intent chooseActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == null) {}
      ComponentName localComponentName;
      for (Object localObject1 = null;; localObject1 = null)
      {
        return (Intent)localObject1;
        ensureConsistentState();
        localObject1 = (ActivityResolveInfo)this.mActivities.get(paramInt);
        localComponentName = new android/content/ComponentName;
        localComponentName.<init>(((ActivityResolveInfo)localObject1).resolveInfo.activityInfo.packageName, ((ActivityResolveInfo)localObject1).resolveInfo.activityInfo.name);
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>(this.mIntent);
        ((Intent)localObject1).setComponent(localComponentName);
        if (this.mActivityChoserModelPolicy == null) {
          break;
        }
        localObject4 = new android/content/Intent;
        ((Intent)localObject4).<init>((Intent)localObject1);
        if (!this.mActivityChoserModelPolicy.onChooseActivity(this, (Intent)localObject4)) {
          break;
        }
      }
      Object localObject4 = new android/support/v7/widget/ActivityChooserModel$HistoricalRecord;
      ((HistoricalRecord)localObject4).<init>(localComponentName, System.currentTimeMillis(), 1.0F);
      addHistoricalRecord((HistoricalRecord)localObject4);
    }
  }
  
  public ResolveInfo getActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(paramInt)).resolveInfo;
      return localResolveInfo;
    }
  }
  
  public int getActivityCount()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mActivities.size();
      return i;
    }
  }
  
  public int getActivityIndex(ResolveInfo paramResolveInfo)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      List localList = this.mActivities;
      int j = localList.size();
      for (int i = 0; i < j; i++) {
        if (((ActivityResolveInfo)localList.get(i)).resolveInfo == paramResolveInfo) {
          return i;
        }
      }
      i = -1;
    }
  }
  
  public ResolveInfo getDefaultActivity()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      if (!this.mActivities.isEmpty())
      {
        localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(0)).resolveInfo;
        return localResolveInfo;
      }
      ResolveInfo localResolveInfo = null;
    }
  }
  
  public int getHistoryMaxSize()
  {
    synchronized (this.mInstanceLock)
    {
      int i = this.mHistoryMaxSize;
      return i;
    }
  }
  
  public int getHistorySize()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mHistoricalRecords.size();
      return i;
    }
  }
  
  public Intent getIntent()
  {
    synchronized (this.mInstanceLock)
    {
      Intent localIntent = this.mIntent;
      return localIntent;
    }
  }
  
  public void setActivitySorter(ActivitySorter paramActivitySorter)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mActivitySorter == paramActivitySorter) {
        return;
      }
      this.mActivitySorter = paramActivitySorter;
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
    }
  }
  
  public void setDefaultActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      Object localObject4 = (ActivityResolveInfo)this.mActivities.get(paramInt);
      Object localObject2 = (ActivityResolveInfo)this.mActivities.get(0);
      if (localObject2 != null)
      {
        f = ((ActivityResolveInfo)localObject2).weight - ((ActivityResolveInfo)localObject4).weight + 5.0F;
        localObject2 = new android/content/ComponentName;
        ((ComponentName)localObject2).<init>(((ActivityResolveInfo)localObject4).resolveInfo.activityInfo.packageName, ((ActivityResolveInfo)localObject4).resolveInfo.activityInfo.name);
        localObject4 = new android/support/v7/widget/ActivityChooserModel$HistoricalRecord;
        ((HistoricalRecord)localObject4).<init>((ComponentName)localObject2, System.currentTimeMillis(), f);
        addHistoricalRecord((HistoricalRecord)localObject4);
        return;
      }
      float f = 1.0F;
    }
  }
  
  public void setHistoryMaxSize(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mHistoryMaxSize == paramInt) {
        return;
      }
      this.mHistoryMaxSize = paramInt;
      pruneExcessiveHistoricalRecordsIfNeeded();
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
    }
  }
  
  public void setIntent(Intent paramIntent)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == paramIntent) {
        return;
      }
      this.mIntent = paramIntent;
      this.mReloadActivities = true;
      ensureConsistentState();
    }
  }
  
  public void setOnChooseActivityListener(OnChooseActivityListener paramOnChooseActivityListener)
  {
    synchronized (this.mInstanceLock)
    {
      this.mActivityChoserModelPolicy = paramOnChooseActivityListener;
      return;
    }
  }
  
  public static abstract interface ActivityChooserModelClient
  {
    public abstract void setActivityChooserModel(ActivityChooserModel paramActivityChooserModel);
  }
  
  public final class ActivityResolveInfo
    implements Comparable<ActivityResolveInfo>
  {
    public final ResolveInfo resolveInfo;
    public float weight;
    
    public ActivityResolveInfo(ResolveInfo paramResolveInfo)
    {
      this.resolveInfo = paramResolveInfo;
    }
    
    public int compareTo(ActivityResolveInfo paramActivityResolveInfo)
    {
      return Float.floatToIntBits(paramActivityResolveInfo.weight) - Float.floatToIntBits(this.weight);
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool = true;
      if (this == paramObject) {}
      for (;;)
      {
        return bool;
        if (paramObject == null)
        {
          bool = false;
        }
        else if (getClass() != paramObject.getClass())
        {
          bool = false;
        }
        else
        {
          paramObject = (ActivityResolveInfo)paramObject;
          if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(((ActivityResolveInfo)paramObject).weight)) {
            bool = false;
          }
        }
      }
    }
    
    public int hashCode()
    {
      return Float.floatToIntBits(this.weight) + 31;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("resolveInfo:").append(this.resolveInfo.toString());
      localStringBuilder.append("; weight:").append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface ActivitySorter
  {
    public abstract void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1);
  }
  
  private final class DefaultSorter
    implements ActivityChooserModel.ActivitySorter
  {
    private static final float WEIGHT_DECAY_COEFFICIENT = 0.95F;
    private final Map<ComponentName, ActivityChooserModel.ActivityResolveInfo> mPackageNameToActivityMap = new HashMap();
    
    DefaultSorter() {}
    
    public void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1)
    {
      paramIntent = this.mPackageNameToActivityMap;
      paramIntent.clear();
      int j = paramList.size();
      Object localObject;
      for (int i = 0; i < j; i++)
      {
        localObject = (ActivityChooserModel.ActivityResolveInfo)paramList.get(i);
        ((ActivityChooserModel.ActivityResolveInfo)localObject).weight = 0.0F;
        paramIntent.put(new ComponentName(((ActivityChooserModel.ActivityResolveInfo)localObject).resolveInfo.activityInfo.packageName, ((ActivityChooserModel.ActivityResolveInfo)localObject).resolveInfo.activityInfo.name), localObject);
      }
      i = paramList1.size();
      float f2 = 1.0F;
      i--;
      while (i >= 0)
      {
        localObject = (ActivityChooserModel.HistoricalRecord)paramList1.get(i);
        ActivityChooserModel.ActivityResolveInfo localActivityResolveInfo = (ActivityChooserModel.ActivityResolveInfo)paramIntent.get(((ActivityChooserModel.HistoricalRecord)localObject).activity);
        float f1 = f2;
        if (localActivityResolveInfo != null)
        {
          localActivityResolveInfo.weight += ((ActivityChooserModel.HistoricalRecord)localObject).weight * f2;
          f1 = f2 * 0.95F;
        }
        i--;
        f2 = f1;
      }
      Collections.sort(paramList);
    }
  }
  
  public static final class HistoricalRecord
  {
    public final ComponentName activity;
    public final long time;
    public final float weight;
    
    public HistoricalRecord(ComponentName paramComponentName, long paramLong, float paramFloat)
    {
      this.activity = paramComponentName;
      this.time = paramLong;
      this.weight = paramFloat;
    }
    
    public HistoricalRecord(String paramString, long paramLong, float paramFloat)
    {
      this(ComponentName.unflattenFromString(paramString), paramLong, paramFloat);
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool = true;
      if (this == paramObject) {}
      for (;;)
      {
        return bool;
        if (paramObject == null)
        {
          bool = false;
        }
        else if (getClass() != paramObject.getClass())
        {
          bool = false;
        }
        else
        {
          paramObject = (HistoricalRecord)paramObject;
          if (this.activity == null)
          {
            if (((HistoricalRecord)paramObject).activity != null) {
              bool = false;
            }
          }
          else if (!this.activity.equals(((HistoricalRecord)paramObject).activity))
          {
            bool = false;
            continue;
          }
          if (this.time != ((HistoricalRecord)paramObject).time) {
            bool = false;
          } else if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(((HistoricalRecord)paramObject).weight)) {
            bool = false;
          }
        }
      }
    }
    
    public int hashCode()
    {
      if (this.activity == null) {}
      for (int i = 0;; i = this.activity.hashCode()) {
        return ((i + 31) * 31 + (int)(this.time ^ this.time >>> 32)) * 31 + Float.floatToIntBits(this.weight);
      }
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("; activity:").append(this.activity);
      localStringBuilder.append("; time:").append(this.time);
      localStringBuilder.append("; weight:").append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface OnChooseActivityListener
  {
    public abstract boolean onChooseActivity(ActivityChooserModel paramActivityChooserModel, Intent paramIntent);
  }
  
  private final class PersistHistoryAsyncTask
    extends AsyncTask<Object, Void, Void>
  {
    PersistHistoryAsyncTask() {}
    
    /* Error */
    public Void doInBackground(Object... paramVarArgs)
    {
      // Byte code:
      //   0: aload_1
      //   1: iconst_0
      //   2: aaload
      //   3: checkcast 33	java/util/List
      //   6: astore 4
      //   8: aload_1
      //   9: iconst_1
      //   10: aaload
      //   11: checkcast 35	java/lang/String
      //   14: astore 5
      //   16: aload_0
      //   17: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   20: getfield 39	android/support/v7/widget/ActivityChooserModel:mContext	Landroid/content/Context;
      //   23: aload 5
      //   25: iconst_0
      //   26: invokevirtual 45	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
      //   29: astore_1
      //   30: invokestatic 51	android/util/Xml:newSerializer	()Lorg/xmlpull/v1/XmlSerializer;
      //   33: astore 6
      //   35: aload 6
      //   37: aload_1
      //   38: aconst_null
      //   39: invokeinterface 57 3 0
      //   44: aload 6
      //   46: ldc 59
      //   48: iconst_1
      //   49: invokestatic 65	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   52: invokeinterface 69 3 0
      //   57: aload 6
      //   59: aconst_null
      //   60: ldc 71
      //   62: invokeinterface 75 3 0
      //   67: pop
      //   68: aload 4
      //   70: invokeinterface 79 1 0
      //   75: istore_3
      //   76: iconst_0
      //   77: istore_2
      //   78: iload_2
      //   79: iload_3
      //   80: if_icmpge +132 -> 212
      //   83: aload 4
      //   85: iconst_0
      //   86: invokeinterface 83 2 0
      //   91: checkcast 85	android/support/v7/widget/ActivityChooserModel$HistoricalRecord
      //   94: astore 5
      //   96: aload 6
      //   98: aconst_null
      //   99: ldc 87
      //   101: invokeinterface 75 3 0
      //   106: pop
      //   107: aload 6
      //   109: aconst_null
      //   110: ldc 89
      //   112: aload 5
      //   114: getfield 92	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:activity	Landroid/content/ComponentName;
      //   117: invokevirtual 98	android/content/ComponentName:flattenToString	()Ljava/lang/String;
      //   120: invokeinterface 102 4 0
      //   125: pop
      //   126: aload 6
      //   128: aconst_null
      //   129: ldc 104
      //   131: aload 5
      //   133: getfield 107	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:time	J
      //   136: invokestatic 110	java/lang/String:valueOf	(J)Ljava/lang/String;
      //   139: invokeinterface 102 4 0
      //   144: pop
      //   145: aload 6
      //   147: aconst_null
      //   148: ldc 112
      //   150: aload 5
      //   152: getfield 115	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:weight	F
      //   155: invokestatic 118	java/lang/String:valueOf	(F)Ljava/lang/String;
      //   158: invokeinterface 102 4 0
      //   163: pop
      //   164: aload 6
      //   166: aconst_null
      //   167: ldc 87
      //   169: invokeinterface 121 3 0
      //   174: pop
      //   175: iinc 2 1
      //   178: goto -100 -> 78
      //   181: astore_1
      //   182: getstatic 125	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   185: new 127	java/lang/StringBuilder
      //   188: dup
      //   189: invokespecial 128	java/lang/StringBuilder:<init>	()V
      //   192: ldc -126
      //   194: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   197: aload 5
      //   199: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   202: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   205: aload_1
      //   206: invokestatic 143	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   209: pop
      //   210: aconst_null
      //   211: areturn
      //   212: aload 6
      //   214: aconst_null
      //   215: ldc 71
      //   217: invokeinterface 121 3 0
      //   222: pop
      //   223: aload 6
      //   225: invokeinterface 146 1 0
      //   230: aload_0
      //   231: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   234: iconst_1
      //   235: putfield 150	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   238: aload_1
      //   239: ifnull +7 -> 246
      //   242: aload_1
      //   243: invokevirtual 155	java/io/FileOutputStream:close	()V
      //   246: goto -36 -> 210
      //   249: astore 6
      //   251: getstatic 125	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   254: astore 4
      //   256: new 127	java/lang/StringBuilder
      //   259: astore 5
      //   261: aload 5
      //   263: invokespecial 128	java/lang/StringBuilder:<init>	()V
      //   266: aload 4
      //   268: aload 5
      //   270: ldc -126
      //   272: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   275: aload_0
      //   276: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   279: getfield 158	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   282: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   285: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   288: aload 6
      //   290: invokestatic 143	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   293: pop
      //   294: aload_0
      //   295: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   298: iconst_1
      //   299: putfield 150	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   302: aload_1
      //   303: ifnull -57 -> 246
      //   306: aload_1
      //   307: invokevirtual 155	java/io/FileOutputStream:close	()V
      //   310: goto -64 -> 246
      //   313: astore_1
      //   314: goto -68 -> 246
      //   317: astore 4
      //   319: getstatic 125	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   322: astore 5
      //   324: new 127	java/lang/StringBuilder
      //   327: astore 6
      //   329: aload 6
      //   331: invokespecial 128	java/lang/StringBuilder:<init>	()V
      //   334: aload 5
      //   336: aload 6
      //   338: ldc -126
      //   340: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   343: aload_0
      //   344: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   347: getfield 158	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   350: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   353: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   356: aload 4
      //   358: invokestatic 143	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   361: pop
      //   362: aload_0
      //   363: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   366: iconst_1
      //   367: putfield 150	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   370: aload_1
      //   371: ifnull -125 -> 246
      //   374: aload_1
      //   375: invokevirtual 155	java/io/FileOutputStream:close	()V
      //   378: goto -132 -> 246
      //   381: astore_1
      //   382: goto -136 -> 246
      //   385: astore 6
      //   387: getstatic 125	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   390: astore 5
      //   392: new 127	java/lang/StringBuilder
      //   395: astore 4
      //   397: aload 4
      //   399: invokespecial 128	java/lang/StringBuilder:<init>	()V
      //   402: aload 5
      //   404: aload 4
      //   406: ldc -126
      //   408: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   411: aload_0
      //   412: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   415: getfield 158	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   418: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   421: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   424: aload 6
      //   426: invokestatic 143	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   429: pop
      //   430: aload_0
      //   431: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   434: iconst_1
      //   435: putfield 150	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   438: aload_1
      //   439: ifnull -193 -> 246
      //   442: aload_1
      //   443: invokevirtual 155	java/io/FileOutputStream:close	()V
      //   446: goto -200 -> 246
      //   449: astore_1
      //   450: goto -204 -> 246
      //   453: astore 4
      //   455: aload_0
      //   456: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   459: iconst_1
      //   460: putfield 150	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   463: aload_1
      //   464: ifnull +7 -> 471
      //   467: aload_1
      //   468: invokevirtual 155	java/io/FileOutputStream:close	()V
      //   471: aload 4
      //   473: athrow
      //   474: astore_1
      //   475: goto -229 -> 246
      //   478: astore_1
      //   479: goto -8 -> 471
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	482	0	this	PersistHistoryAsyncTask
      //   0	482	1	paramVarArgs	Object[]
      //   77	99	2	i	int
      //   75	6	3	j	int
      //   6	261	4	localObject1	Object
      //   317	40	4	localIllegalStateException	IllegalStateException
      //   395	10	4	localStringBuilder1	StringBuilder
      //   453	19	4	localObject2	Object
      //   14	389	5	localObject3	Object
      //   33	191	6	localXmlSerializer	org.xmlpull.v1.XmlSerializer
      //   249	40	6	localIllegalArgumentException	IllegalArgumentException
      //   327	10	6	localStringBuilder2	StringBuilder
      //   385	40	6	localIOException	java.io.IOException
      // Exception table:
      //   from	to	target	type
      //   16	30	181	java/io/FileNotFoundException
      //   35	76	249	java/lang/IllegalArgumentException
      //   83	175	249	java/lang/IllegalArgumentException
      //   212	230	249	java/lang/IllegalArgumentException
      //   306	310	313	java/io/IOException
      //   35	76	317	java/lang/IllegalStateException
      //   83	175	317	java/lang/IllegalStateException
      //   212	230	317	java/lang/IllegalStateException
      //   374	378	381	java/io/IOException
      //   35	76	385	java/io/IOException
      //   83	175	385	java/io/IOException
      //   212	230	385	java/io/IOException
      //   442	446	449	java/io/IOException
      //   35	76	453	finally
      //   83	175	453	finally
      //   212	230	453	finally
      //   251	294	453	finally
      //   319	362	453	finally
      //   387	430	453	finally
      //   242	246	474	java/io/IOException
      //   467	471	478	java/io/IOException
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\ActivityChooserModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */