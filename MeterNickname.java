package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class MeterNickname
  extends PBObject
  implements Serializable
{
  @Expose
  @SerializedName("created_at")
  private String createdAt;
  @Expose
  @SerializedName("model")
  private String model;
  @Expose
  @SerializedName("nickname")
  private String nickname;
  @Expose
  @SerializedName("serial_number")
  private String serialNumber;
  @Expose
  @SerializedName("updated_at")
  private String updatedAt;
  @Expose
  @SerializedName("user_id")
  private String userId;
  
  public MeterNickname() {}
  
  public MeterNickname(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    this.userId = paramString1;
    this.model = paramString2;
    this.nickname = paramString3;
    this.createdAt = paramString4;
    this.updatedAt = paramString5;
    this.serialNumber = paramString6;
  }
  
  public String getCreatedAt()
  {
    return this.createdAt;
  }
  
  public String getModel()
  {
    return this.model;
  }
  
  public String getNickname()
  {
    return this.nickname;
  }
  
  public String getSerialNumber()
  {
    return this.serialNumber;
  }
  
  public String getUpdatedAt()
  {
    return this.updatedAt;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public void setCreatedAt(String paramString)
  {
    this.createdAt = paramString;
  }
  
  public void setModel(String paramString)
  {
    this.model = paramString;
  }
  
  public void setNickname(String paramString)
  {
    this.nickname = paramString;
  }
  
  public void setSerialNumber(String paramString)
  {
    this.serialNumber = paramString;
  }
  
  public void setUpdatedAt(String paramString)
  {
    this.updatedAt = paramString;
  }
  
  public void setUserId(String paramString)
  {
    this.userId = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\MeterNickname.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */