package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import me.dm7.barcodescanner.zbar.ZBarScannerView.ResultHandler;

public class ScanViewFinderFragment
  extends Fragment
  implements ZBarScannerView.ResultHandler
{
  private static final boolean DEBUG_MODE = false;
  private OnBarcodeScannedListener mListener;
  private ZBarScannerView mScannerView;
  
  public void handleResult(Result paramResult)
  {
    onBarcodeScanned(paramResult.getContents());
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        ScanViewFinderFragment.this.mScannerView.resumeCameraPreview(ScanViewFinderFragment.this);
      }
    }, 2000L);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnBarcodeScannedListener))
    {
      this.mListener = ((OnBarcodeScannedListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnBarcodeScannedListener");
  }
  
  void onBarcodeScanned(String paramString)
  {
    if (this.mListener != null) {
      this.mListener.onBarcodeScanResult(paramString);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mScannerView = new ZBarScannerView(getActivity());
    this.mScannerView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView) {}
    });
    return this.mScannerView;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPause()
  {
    super.onPause();
    this.mScannerView.stopCamera();
  }
  
  public void onResume()
  {
    super.onResume();
    this.mScannerView.setResultHandler(this);
    this.mScannerView.startCamera();
  }
  
  public static abstract interface OnBarcodeScannedListener
  {
    public abstract void onBarcodeScanResult(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\ScanViewFinderFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */