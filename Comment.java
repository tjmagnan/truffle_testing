package org.jsoup.nodes;

import java.io.IOException;

public class Comment
  extends Node
{
  private static final String COMMENT_KEY = "comment";
  
  public Comment(String paramString1, String paramString2)
  {
    super(paramString2);
    this.attributes.put("comment", paramString1);
  }
  
  public String getData()
  {
    return this.attributes.get("comment");
  }
  
  public String nodeName()
  {
    return "#comment";
  }
  
  void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    if (paramOutputSettings.prettyPrint()) {
      indent(paramAppendable, paramInt, paramOutputSettings);
    }
    paramAppendable.append("<!--").append(getData()).append("-->");
  }
  
  void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings) {}
  
  public String toString()
  {
    return outerHtml();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\Comment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */