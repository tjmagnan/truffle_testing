package io.fabric.sdk.android.services.network;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import javax.security.auth.x500.X500Principal;

class SystemKeyStore
{
  private final HashMap<Principal, X509Certificate> trustRoots;
  final KeyStore trustStore;
  
  public SystemKeyStore(InputStream paramInputStream, String paramString)
  {
    paramInputStream = getTrustStore(paramInputStream, paramString);
    this.trustRoots = initializeTrustedRoots(paramInputStream);
    this.trustStore = paramInputStream;
  }
  
  /* Error */
  private KeyStore getTrustStore(InputStream paramInputStream, String paramString)
  {
    // Byte code:
    //   0: ldc 37
    //   2: invokestatic 43	java/security/KeyStore:getInstance	(Ljava/lang/String;)Ljava/security/KeyStore;
    //   5: astore 4
    //   7: new 45	java/io/BufferedInputStream
    //   10: astore_3
    //   11: aload_3
    //   12: aload_1
    //   13: invokespecial 48	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   16: aload 4
    //   18: aload_3
    //   19: aload_2
    //   20: invokevirtual 54	java/lang/String:toCharArray	()[C
    //   23: invokevirtual 58	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   26: aload_3
    //   27: invokevirtual 61	java/io/BufferedInputStream:close	()V
    //   30: aload 4
    //   32: areturn
    //   33: astore_1
    //   34: aload_3
    //   35: invokevirtual 61	java/io/BufferedInputStream:close	()V
    //   38: aload_1
    //   39: athrow
    //   40: astore_1
    //   41: new 63	java/lang/AssertionError
    //   44: dup
    //   45: aload_1
    //   46: invokespecial 66	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   49: athrow
    //   50: astore_1
    //   51: new 63	java/lang/AssertionError
    //   54: dup
    //   55: aload_1
    //   56: invokespecial 66	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   59: athrow
    //   60: astore_1
    //   61: new 63	java/lang/AssertionError
    //   64: dup
    //   65: aload_1
    //   66: invokespecial 66	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   69: athrow
    //   70: astore_1
    //   71: new 63	java/lang/AssertionError
    //   74: dup
    //   75: aload_1
    //   76: invokespecial 66	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   79: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	80	0	this	SystemKeyStore
    //   0	80	1	paramInputStream	InputStream
    //   0	80	2	paramString	String
    //   10	25	3	localBufferedInputStream	java.io.BufferedInputStream
    //   5	26	4	localKeyStore	KeyStore
    // Exception table:
    //   from	to	target	type
    //   16	26	33	finally
    //   0	16	40	java/security/KeyStoreException
    //   26	30	40	java/security/KeyStoreException
    //   34	40	40	java/security/KeyStoreException
    //   0	16	50	java/security/NoSuchAlgorithmException
    //   26	30	50	java/security/NoSuchAlgorithmException
    //   34	40	50	java/security/NoSuchAlgorithmException
    //   0	16	60	java/security/cert/CertificateException
    //   26	30	60	java/security/cert/CertificateException
    //   34	40	60	java/security/cert/CertificateException
    //   0	16	70	java/io/IOException
    //   26	30	70	java/io/IOException
    //   34	40	70	java/io/IOException
  }
  
  private HashMap<Principal, X509Certificate> initializeTrustedRoots(KeyStore paramKeyStore)
  {
    try
    {
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      Enumeration localEnumeration = paramKeyStore.aliases();
      while (localEnumeration.hasMoreElements())
      {
        X509Certificate localX509Certificate = (X509Certificate)paramKeyStore.getCertificate((String)localEnumeration.nextElement());
        if (localX509Certificate != null) {
          localHashMap.put(localX509Certificate.getSubjectX500Principal(), localX509Certificate);
        }
      }
      return localHashMap;
    }
    catch (KeyStoreException paramKeyStore)
    {
      throw new AssertionError(paramKeyStore);
    }
  }
  
  public X509Certificate getTrustRootFor(X509Certificate paramX509Certificate)
  {
    X509Certificate localX509Certificate = (X509Certificate)this.trustRoots.get(paramX509Certificate.getIssuerX500Principal());
    if (localX509Certificate == null) {
      paramX509Certificate = null;
    }
    for (;;)
    {
      return paramX509Certificate;
      if (localX509Certificate.getSubjectX500Principal().equals(paramX509Certificate.getSubjectX500Principal())) {
        paramX509Certificate = null;
      } else {
        try
        {
          paramX509Certificate.verify(localX509Certificate.getPublicKey());
          paramX509Certificate = localX509Certificate;
        }
        catch (GeneralSecurityException paramX509Certificate)
        {
          paramX509Certificate = null;
        }
      }
    }
  }
  
  public boolean isTrustRoot(X509Certificate paramX509Certificate)
  {
    X509Certificate localX509Certificate = (X509Certificate)this.trustRoots.get(paramX509Certificate.getSubjectX500Principal());
    if ((localX509Certificate != null) && (localX509Certificate.getPublicKey().equals(paramX509Certificate.getPublicKey()))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\network\SystemKeyStore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */