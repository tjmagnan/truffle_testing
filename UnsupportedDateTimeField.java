package org.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;

public final class UnsupportedDateTimeField
  extends DateTimeField
  implements Serializable
{
  private static HashMap<DateTimeFieldType, UnsupportedDateTimeField> cCache;
  private static final long serialVersionUID = -1934618396111902255L;
  private final DurationField iDurationField;
  private final DateTimeFieldType iType;
  
  private UnsupportedDateTimeField(DateTimeFieldType paramDateTimeFieldType, DurationField paramDurationField)
  {
    if ((paramDateTimeFieldType == null) || (paramDurationField == null)) {
      throw new IllegalArgumentException();
    }
    this.iType = paramDateTimeFieldType;
    this.iDurationField = paramDurationField;
  }
  
  /* Error */
  public static UnsupportedDateTimeField getInstance(DateTimeFieldType paramDateTimeFieldType, DurationField paramDurationField)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 34	org/joda/time/field/UnsupportedDateTimeField:cCache	Ljava/util/HashMap;
    //   6: ifnonnull +49 -> 55
    //   9: new 36	java/util/HashMap
    //   12: astore_2
    //   13: aload_2
    //   14: bipush 7
    //   16: invokespecial 39	java/util/HashMap:<init>	(I)V
    //   19: aload_2
    //   20: putstatic 34	org/joda/time/field/UnsupportedDateTimeField:cCache	Ljava/util/HashMap;
    //   23: aconst_null
    //   24: astore_2
    //   25: aload_2
    //   26: astore_3
    //   27: aload_2
    //   28: ifnonnull +22 -> 50
    //   31: new 2	org/joda/time/field/UnsupportedDateTimeField
    //   34: astore_3
    //   35: aload_3
    //   36: aload_0
    //   37: aload_1
    //   38: invokespecial 41	org/joda/time/field/UnsupportedDateTimeField:<init>	(Lorg/joda/time/DateTimeFieldType;Lorg/joda/time/DurationField;)V
    //   41: getstatic 34	org/joda/time/field/UnsupportedDateTimeField:cCache	Ljava/util/HashMap;
    //   44: aload_0
    //   45: aload_3
    //   46: invokevirtual 45	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   49: pop
    //   50: ldc 2
    //   52: monitorexit
    //   53: aload_3
    //   54: areturn
    //   55: getstatic 34	org/joda/time/field/UnsupportedDateTimeField:cCache	Ljava/util/HashMap;
    //   58: aload_0
    //   59: invokevirtual 49	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   62: checkcast 2	org/joda/time/field/UnsupportedDateTimeField
    //   65: astore_3
    //   66: aload_3
    //   67: astore_2
    //   68: aload_3
    //   69: ifnull -44 -> 25
    //   72: aload_3
    //   73: invokevirtual 53	org/joda/time/field/UnsupportedDateTimeField:getDurationField	()Lorg/joda/time/DurationField;
    //   76: astore 4
    //   78: aload_3
    //   79: astore_2
    //   80: aload 4
    //   82: aload_1
    //   83: if_acmpeq -58 -> 25
    //   86: aconst_null
    //   87: astore_2
    //   88: goto -63 -> 25
    //   91: astore_0
    //   92: ldc 2
    //   94: monitorexit
    //   95: aload_0
    //   96: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	97	0	paramDateTimeFieldType	DateTimeFieldType
    //   0	97	1	paramDurationField	DurationField
    //   12	76	2	localObject1	Object
    //   26	53	3	localObject2	Object
    //   76	5	4	localDurationField	DurationField
    // Exception table:
    //   from	to	target	type
    //   3	23	91	finally
    //   31	50	91	finally
    //   55	66	91	finally
    //   72	78	91	finally
  }
  
  private Object readResolve()
  {
    return getInstance(this.iType, this.iDurationField);
  }
  
  private UnsupportedOperationException unsupported()
  {
    return new UnsupportedOperationException(this.iType + " field is unsupported");
  }
  
  public long add(long paramLong, int paramInt)
  {
    return getDurationField().add(paramLong, paramInt);
  }
  
  public long add(long paramLong1, long paramLong2)
  {
    return getDurationField().add(paramLong1, paramLong2);
  }
  
  public int[] add(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    throw unsupported();
  }
  
  public long addWrapField(long paramLong, int paramInt)
  {
    throw unsupported();
  }
  
  public int[] addWrapField(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    throw unsupported();
  }
  
  public int[] addWrapPartial(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    throw unsupported();
  }
  
  public int get(long paramLong)
  {
    throw unsupported();
  }
  
  public String getAsShortText(int paramInt, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsShortText(long paramLong)
  {
    throw unsupported();
  }
  
  public String getAsShortText(long paramLong, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsShortText(ReadablePartial paramReadablePartial, int paramInt, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsShortText(ReadablePartial paramReadablePartial, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsText(int paramInt, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsText(long paramLong)
  {
    throw unsupported();
  }
  
  public String getAsText(long paramLong, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsText(ReadablePartial paramReadablePartial, int paramInt, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String getAsText(ReadablePartial paramReadablePartial, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public int getDifference(long paramLong1, long paramLong2)
  {
    return getDurationField().getDifference(paramLong1, paramLong2);
  }
  
  public long getDifferenceAsLong(long paramLong1, long paramLong2)
  {
    return getDurationField().getDifferenceAsLong(paramLong1, paramLong2);
  }
  
  public DurationField getDurationField()
  {
    return this.iDurationField;
  }
  
  public int getLeapAmount(long paramLong)
  {
    throw unsupported();
  }
  
  public DurationField getLeapDurationField()
  {
    return null;
  }
  
  public int getMaximumShortTextLength(Locale paramLocale)
  {
    throw unsupported();
  }
  
  public int getMaximumTextLength(Locale paramLocale)
  {
    throw unsupported();
  }
  
  public int getMaximumValue()
  {
    throw unsupported();
  }
  
  public int getMaximumValue(long paramLong)
  {
    throw unsupported();
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial)
  {
    throw unsupported();
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt)
  {
    throw unsupported();
  }
  
  public int getMinimumValue()
  {
    throw unsupported();
  }
  
  public int getMinimumValue(long paramLong)
  {
    throw unsupported();
  }
  
  public int getMinimumValue(ReadablePartial paramReadablePartial)
  {
    throw unsupported();
  }
  
  public int getMinimumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt)
  {
    throw unsupported();
  }
  
  public String getName()
  {
    return this.iType.getName();
  }
  
  public DurationField getRangeDurationField()
  {
    return null;
  }
  
  public DateTimeFieldType getType()
  {
    return this.iType;
  }
  
  public boolean isLeap(long paramLong)
  {
    throw unsupported();
  }
  
  public boolean isLenient()
  {
    return false;
  }
  
  public boolean isSupported()
  {
    return false;
  }
  
  public long remainder(long paramLong)
  {
    throw unsupported();
  }
  
  public long roundCeiling(long paramLong)
  {
    throw unsupported();
  }
  
  public long roundFloor(long paramLong)
  {
    throw unsupported();
  }
  
  public long roundHalfCeiling(long paramLong)
  {
    throw unsupported();
  }
  
  public long roundHalfEven(long paramLong)
  {
    throw unsupported();
  }
  
  public long roundHalfFloor(long paramLong)
  {
    throw unsupported();
  }
  
  public long set(long paramLong, int paramInt)
  {
    throw unsupported();
  }
  
  public long set(long paramLong, String paramString)
  {
    throw unsupported();
  }
  
  public long set(long paramLong, String paramString, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public int[] set(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    throw unsupported();
  }
  
  public int[] set(ReadablePartial paramReadablePartial, int paramInt, int[] paramArrayOfInt, String paramString, Locale paramLocale)
  {
    throw unsupported();
  }
  
  public String toString()
  {
    return "UnsupportedDateTimeField";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\field\UnsupportedDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */