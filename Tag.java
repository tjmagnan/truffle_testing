package org.jsoup.parser;

import java.util.HashMap;
import java.util.Map;
import org.jsoup.helper.Validate;

public class Tag
{
  private static final String[] blockTags;
  private static final String[] emptyTags;
  private static final String[] formListedTags;
  private static final String[] formSubmitTags;
  private static final String[] formatAsInlineTags;
  private static final String[] inlineTags;
  private static final String[] preserveWhitespaceTags;
  private static final Map<String, Tag> tags;
  private boolean canContainBlock = true;
  private boolean canContainInline = true;
  private boolean empty = false;
  private boolean formList = false;
  private boolean formSubmit = false;
  private boolean formatAsBlock = true;
  private boolean isBlock = true;
  private boolean preserveWhitespace = false;
  private boolean selfClosing = false;
  private String tagName;
  
  static
  {
    int j = 0;
    tags = new HashMap();
    blockTags = new String[] { "html", "head", "body", "frameset", "script", "noscript", "style", "meta", "link", "title", "frame", "noframes", "section", "nav", "aside", "hgroup", "header", "footer", "p", "h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "pre", "div", "blockquote", "hr", "address", "figure", "figcaption", "form", "fieldset", "ins", "del", "s", "dl", "dt", "dd", "li", "table", "caption", "thead", "tfoot", "tbody", "colgroup", "col", "tr", "th", "td", "video", "audio", "canvas", "details", "menu", "plaintext", "template", "article", "main", "svg", "math" };
    inlineTags = new String[] { "object", "base", "font", "tt", "i", "b", "u", "big", "small", "em", "strong", "dfn", "code", "samp", "kbd", "var", "cite", "abbr", "time", "acronym", "mark", "ruby", "rt", "rp", "a", "img", "br", "wbr", "map", "q", "sub", "sup", "bdo", "iframe", "embed", "span", "input", "select", "textarea", "label", "button", "optgroup", "option", "legend", "datalist", "keygen", "output", "progress", "meter", "area", "param", "source", "track", "summary", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track", "data", "bdi" };
    emptyTags = new String[] { "meta", "link", "base", "frame", "img", "br", "wbr", "embed", "hr", "input", "keygen", "col", "command", "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track" };
    formatAsInlineTags = new String[] { "title", "a", "p", "h1", "h2", "h3", "h4", "h5", "h6", "pre", "address", "li", "th", "td", "script", "style", "ins", "del", "s" };
    preserveWhitespaceTags = new String[] { "pre", "plaintext", "title", "textarea" };
    formListedTags = new String[] { "button", "fieldset", "input", "keygen", "object", "output", "select", "textarea" };
    formSubmitTags = new String[] { "input", "keygen", "object", "select", "textarea" };
    String[] arrayOfString = blockTags;
    int k = arrayOfString.length;
    for (int i = 0; i < k; i++) {
      register(new Tag(arrayOfString[i]));
    }
    arrayOfString = inlineTags;
    k = arrayOfString.length;
    Object localObject;
    for (i = 0; i < k; i++)
    {
      localObject = new Tag(arrayOfString[i]);
      ((Tag)localObject).isBlock = false;
      ((Tag)localObject).canContainBlock = false;
      ((Tag)localObject).formatAsBlock = false;
      register((Tag)localObject);
    }
    for (localObject : emptyTags)
    {
      localObject = (Tag)tags.get(localObject);
      Validate.notNull(localObject);
      ((Tag)localObject).canContainBlock = false;
      ((Tag)localObject).canContainInline = false;
      ((Tag)localObject).empty = true;
    }
    for (localObject : formatAsInlineTags)
    {
      localObject = (Tag)tags.get(localObject);
      Validate.notNull(localObject);
      ((Tag)localObject).formatAsBlock = false;
    }
    for (localObject : preserveWhitespaceTags)
    {
      localObject = (Tag)tags.get(localObject);
      Validate.notNull(localObject);
      ((Tag)localObject).preserveWhitespace = true;
    }
    for (localObject : formListedTags)
    {
      localObject = (Tag)tags.get(localObject);
      Validate.notNull(localObject);
      ((Tag)localObject).formList = true;
    }
    arrayOfString = formSubmitTags;
    k = arrayOfString.length;
    for (i = j; i < k; i++)
    {
      localObject = arrayOfString[i];
      localObject = (Tag)tags.get(localObject);
      Validate.notNull(localObject);
      ((Tag)localObject).formSubmit = true;
    }
  }
  
  private Tag(String paramString)
  {
    this.tagName = paramString;
  }
  
  public static boolean isKnownTag(String paramString)
  {
    return tags.containsKey(paramString);
  }
  
  private static void register(Tag paramTag)
  {
    tags.put(paramTag.tagName, paramTag);
  }
  
  public static Tag valueOf(String paramString)
  {
    return valueOf(paramString, ParseSettings.preserveCase);
  }
  
  public static Tag valueOf(String paramString, ParseSettings paramParseSettings)
  {
    Validate.notNull(paramString);
    Tag localTag = (Tag)tags.get(paramString);
    Object localObject = localTag;
    if (localTag == null)
    {
      paramParseSettings = paramParseSettings.normalizeTag(paramString);
      Validate.notEmpty(paramParseSettings);
      paramString = (Tag)tags.get(paramParseSettings);
      localObject = paramString;
      if (paramString == null)
      {
        localObject = new Tag(paramParseSettings);
        ((Tag)localObject).isBlock = false;
        ((Tag)localObject).canContainBlock = true;
      }
    }
    return (Tag)localObject;
  }
  
  public boolean canContainBlock()
  {
    return this.canContainBlock;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool3 = true;
    boolean bool2 = false;
    if (this == paramObject) {
      bool1 = true;
    }
    do
    {
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  do
                  {
                    do
                    {
                      do
                      {
                        return bool1;
                        bool1 = bool2;
                      } while (!(paramObject instanceof Tag));
                      paramObject = (Tag)paramObject;
                      bool1 = bool2;
                    } while (!this.tagName.equals(((Tag)paramObject).tagName));
                    bool1 = bool2;
                  } while (this.canContainBlock != ((Tag)paramObject).canContainBlock);
                  bool1 = bool2;
                } while (this.canContainInline != ((Tag)paramObject).canContainInline);
                bool1 = bool2;
              } while (this.empty != ((Tag)paramObject).empty);
              bool1 = bool2;
            } while (this.formatAsBlock != ((Tag)paramObject).formatAsBlock);
            bool1 = bool2;
          } while (this.isBlock != ((Tag)paramObject).isBlock);
          bool1 = bool2;
        } while (this.preserveWhitespace != ((Tag)paramObject).preserveWhitespace);
        bool1 = bool2;
      } while (this.selfClosing != ((Tag)paramObject).selfClosing);
      bool1 = bool2;
    } while (this.formList != ((Tag)paramObject).formList);
    if (this.formSubmit == ((Tag)paramObject).formSubmit) {}
    for (boolean bool1 = bool3;; bool1 = false) {
      break;
    }
  }
  
  public boolean formatAsBlock()
  {
    return this.formatAsBlock;
  }
  
  public String getName()
  {
    return this.tagName;
  }
  
  public int hashCode()
  {
    int i4 = 1;
    int i5 = this.tagName.hashCode();
    int i;
    int j;
    label30:
    int k;
    label39:
    int m;
    label49:
    int n;
    label59:
    int i1;
    label69:
    int i2;
    label79:
    int i3;
    if (this.isBlock)
    {
      i = 1;
      if (!this.formatAsBlock) {
        break label155;
      }
      j = 1;
      if (!this.canContainBlock) {
        break label160;
      }
      k = 1;
      if (!this.canContainInline) {
        break label165;
      }
      m = 1;
      if (!this.empty) {
        break label171;
      }
      n = 1;
      if (!this.selfClosing) {
        break label177;
      }
      i1 = 1;
      if (!this.preserveWhitespace) {
        break label183;
      }
      i2 = 1;
      if (!this.formList) {
        break label189;
      }
      i3 = 1;
      label89:
      if (!this.formSubmit) {
        break label195;
      }
    }
    for (;;)
    {
      return ((((((((i5 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4;
      i = 0;
      break;
      label155:
      j = 0;
      break label30;
      label160:
      k = 0;
      break label39;
      label165:
      m = 0;
      break label49;
      label171:
      n = 0;
      break label59;
      label177:
      i1 = 0;
      break label69;
      label183:
      i2 = 0;
      break label79;
      label189:
      i3 = 0;
      break label89;
      label195:
      i4 = 0;
    }
  }
  
  public boolean isBlock()
  {
    return this.isBlock;
  }
  
  public boolean isData()
  {
    if ((!this.canContainInline) && (!isEmpty())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isEmpty()
  {
    return this.empty;
  }
  
  public boolean isFormListed()
  {
    return this.formList;
  }
  
  public boolean isFormSubmittable()
  {
    return this.formSubmit;
  }
  
  public boolean isInline()
  {
    if (!this.isBlock) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isKnownTag()
  {
    return tags.containsKey(this.tagName);
  }
  
  public boolean isSelfClosing()
  {
    if ((this.empty) || (this.selfClosing)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean preserveWhitespace()
  {
    return this.preserveWhitespace;
  }
  
  Tag setSelfClosing()
  {
    this.selfClosing = true;
    return this;
  }
  
  public String toString()
  {
    return this.tagName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\Tag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */