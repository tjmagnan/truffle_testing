package me.zhanghai.android.materialprogressbar.internal;

import android.graphics.PorterDuff.Mode;

public class DrawableCompat
{
  public static PorterDuff.Mode parseTintMode(int paramInt, PorterDuff.Mode paramMode)
  {
    PorterDuff.Mode localMode = paramMode;
    switch (paramInt)
    {
    default: 
      localMode = paramMode;
    }
    for (;;)
    {
      return localMode;
      localMode = PorterDuff.Mode.SRC_OVER;
      continue;
      localMode = PorterDuff.Mode.SRC_IN;
      continue;
      localMode = PorterDuff.Mode.SRC_ATOP;
      continue;
      localMode = PorterDuff.Mode.MULTIPLY;
      continue;
      localMode = PorterDuff.Mode.SCREEN;
      continue;
      localMode = PorterDuff.Mode.ADD;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\internal\DrawableCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */