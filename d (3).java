package com.testfairy.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.opengl.GLSurfaceView;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.p.b;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

public class c
  extends GLSurfaceView
{
  private GLSurfaceView a;
  private final Object b = new Object();
  
  public c(Context paramContext)
  {
    super(paramContext);
  }
  
  public c(Context paramContext, GLSurfaceView paramGLSurfaceView)
  {
    super(paramContext);
    this.a = paramGLSurfaceView;
  }
  
  private void a(GLSurfaceView paramGLSurfaceView, a parama)
  {
    try
    {
      Runnable local2 = new com/testfairy/d/c$2;
      local2.<init>(this, paramGLSurfaceView, parama);
      paramGLSurfaceView.queueEvent(local2);
      return;
    }
    catch (Exception paramGLSurfaceView)
    {
      for (;;)
      {
        parama.a("drawGLSurfaceViewOnTheCanvas Exception, " + paramGLSurfaceView.getMessage());
      }
    }
    catch (OutOfMemoryError paramGLSurfaceView)
    {
      for (;;)
      {
        parama.a("drawGLSurfaceViewOnTheCanvas OutOfMemoryError, " + paramGLSurfaceView.getMessage());
      }
    }
  }
  
  public void a(GLSurfaceView paramGLSurfaceView)
  {
    this.a = paramGLSurfaceView;
  }
  
  protected void onDraw(Canvas arg1)
  {
    super.onDraw(???);
    if ((this.a.getWidth() <= 0) || (this.a.getHeight() <= 0)) {
      return;
    }
    a(this.a, new a()
    {
      public void a(Bitmap paramAnonymousBitmap)
      {
        paramCanvas.drawBitmap(paramAnonymousBitmap, new Matrix(), new Paint());
        synchronized (c.a(c.this))
        {
          c.a(c.this).notify();
          return;
        }
      }
      
      public void a(String paramAnonymousString)
      {
        Log.d(e.a, paramAnonymousString);
        synchronized (c.a(c.this))
        {
          c.a(c.this).notify();
          return;
        }
      }
    });
    try
    {
      synchronized (this.b)
      {
        this.b.wait();
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(Bitmap paramBitmap);
    
    public abstract void a(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */