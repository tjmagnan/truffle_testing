package com.crashlytics.android.answers;

abstract interface EventFilter
{
  public abstract boolean skipEvent(SessionEvent paramSessionEvent);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\answers\EventFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */