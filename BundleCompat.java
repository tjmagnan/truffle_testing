package android.support.v4.app;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;

public final class BundleCompat
{
  public static IBinder getBinder(Bundle paramBundle, String paramString)
  {
    if (Build.VERSION.SDK_INT >= 18) {}
    for (paramBundle = BundleCompatJellybeanMR2.getBinder(paramBundle, paramString);; paramBundle = BundleCompatGingerbread.getBinder(paramBundle, paramString)) {
      return paramBundle;
    }
  }
  
  public static void putBinder(Bundle paramBundle, String paramString, IBinder paramIBinder)
  {
    if (Build.VERSION.SDK_INT >= 18) {
      BundleCompatJellybeanMR2.putBinder(paramBundle, paramString, paramIBinder);
    }
    for (;;)
    {
      return;
      BundleCompatGingerbread.putBinder(paramBundle, paramString, paramIBinder);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\app\BundleCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */