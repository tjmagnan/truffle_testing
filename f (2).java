package com.testfairy.f;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.h.c;
import com.testfairy.i;
import com.testfairy.j;
import com.testfairy.p.l;
import java.io.File;
import org.json.JSONObject;

public class f
  extends Thread
{
  private final Thread a;
  private final Throwable b;
  private final String c;
  private final j d;
  private final i e;
  private final com.testfairy.k.d f;
  private c g = new c()
  {
    public void a(String paramAnonymousString) {}
  };
  
  public f(Thread paramThread, Throwable paramThrowable, String paramString, j paramj, i parami)
  {
    this(paramThread, paramThrowable, paramString, paramj, parami, null);
  }
  
  public f(Thread paramThread, Throwable paramThrowable, String paramString, j paramj, i parami, com.testfairy.k.d paramd)
  {
    this.a = paramThread;
    this.b = paramThrowable;
    this.c = paramString;
    this.d = paramj;
    this.e = parami;
    this.f = paramd;
  }
  
  private void a(JSONObject paramJSONObject)
  {
    if (this.c != null)
    {
      File localFile = new File(this.c + "/" + "testfairy-stacktrace" + "." + this.e.a() + "." + System.currentTimeMillis());
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("sessionToken", this.e.a());
      localJSONObject.put("data", paramJSONObject);
      com.testfairy.p.d.b(localFile, localJSONObject.toString().getBytes());
      Log.i(e.a, "Saved stack trace to " + localFile.getAbsolutePath());
    }
  }
  
  public void run()
  {
    try
    {
      Object localObject2 = l.a(this.b);
      Object localObject3 = l.a((String)localObject2);
      Object localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      ((JSONObject)localObject1).put("timestamp", System.currentTimeMillis() / 1000L);
      ((JSONObject)localObject1).put("message", localObject3);
      ((JSONObject)localObject1).put("stackTrace", localObject2);
      a((JSONObject)localObject1);
      localObject2 = e.a;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      Log.i((String)localObject2, "Sending crash " + ((JSONObject)localObject1).toString());
      localObject2 = new com/testfairy/h/f;
      ((com.testfairy.h.f)localObject2).<init>();
      ((com.testfairy.h.f)localObject2).a("sessionToken", this.e.a());
      ((com.testfairy.h.f)localObject2).a("data", ((JSONObject)localObject1).toString());
      if (this.f == null)
      {
        localObject1 = new com/testfairy/k/d;
        ((com.testfairy.k.d)localObject1).<init>(this.d.f());
        ((com.testfairy.k.d)localObject1).c((com.testfairy.h.f)localObject2, this.g);
      }
      for (;;)
      {
        return;
        this.f.c((com.testfairy.h.f)localObject2, this.g);
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        Log.e(e.a, "Ouch! JSON Exception", localThrowable);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */