package tech.dcube.companion.managers.server.backend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

public class VolleySingleton
{
  @SuppressLint({"StaticFieldLeak"})
  private static Context mCtx;
  @SuppressLint({"StaticFieldLeak"})
  private static VolleySingleton mInstance;
  private ImageLoader mImageLoader;
  private RequestQueue mRequestQueue;
  
  private VolleySingleton(Context paramContext)
  {
    mCtx = paramContext;
    this.mRequestQueue = getRequestQueue();
    this.mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache()
    {
      private final LruCache<String, Bitmap> cache = new LruCache(20);
      
      public Bitmap getBitmap(String paramAnonymousString)
      {
        return (Bitmap)this.cache.get(paramAnonymousString);
      }
      
      public void putBitmap(String paramAnonymousString, Bitmap paramAnonymousBitmap)
      {
        this.cache.put(paramAnonymousString, paramAnonymousBitmap);
      }
    });
  }
  
  public static VolleySingleton getInstance(Context paramContext)
  {
    try
    {
      if (mInstance == null)
      {
        VolleySingleton localVolleySingleton = new tech/dcube/companion/managers/server/backend/VolleySingleton;
        localVolleySingleton.<init>(paramContext);
        mInstance = localVolleySingleton;
      }
      paramContext = mInstance;
      return paramContext;
    }
    finally {}
  }
  
  public <T> void addToRequestQueue(Request<T> paramRequest)
  {
    getRequestQueue().add(paramRequest);
  }
  
  public ImageLoader getImageLoader()
  {
    return this.mImageLoader;
  }
  
  public RequestQueue getRequestQueue()
  {
    if (this.mRequestQueue == null) {
      this.mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
    }
    return this.mRequestQueue;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\backend\VolleySingleton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */