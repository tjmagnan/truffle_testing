package org.jsoup.parser;

import java.util.Arrays;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;

final class Tokeniser
{
  private static final char[] notCharRefCharsSorted = { 9, 10, 13, 12, 32, 60, 38 };
  static final char replacementChar = '�';
  Token.Character charPending = new Token.Character();
  private StringBuilder charsBuilder = new StringBuilder(1024);
  private String charsString = null;
  private final int[] codepointHolder = new int[1];
  Token.Comment commentPending = new Token.Comment();
  StringBuilder dataBuffer = new StringBuilder(1024);
  Token.Doctype doctypePending = new Token.Doctype();
  private Token emitPending;
  Token.EndTag endPending = new Token.EndTag();
  private final ParseErrorList errors;
  private boolean isEmitPending = false;
  private String lastStartTag;
  private final int[] multipointHolder = new int[2];
  private final CharacterReader reader;
  private boolean selfClosingFlagAcknowledged = true;
  Token.StartTag startPending = new Token.StartTag();
  private TokeniserState state = TokeniserState.Data;
  Token.Tag tagPending;
  
  static
  {
    Arrays.sort(notCharRefCharsSorted);
  }
  
  Tokeniser(CharacterReader paramCharacterReader, ParseErrorList paramParseErrorList)
  {
    this.reader = paramCharacterReader;
    this.errors = paramParseErrorList;
  }
  
  private void characterReferenceError(String paramString)
  {
    if (this.errors.canAddError()) {
      this.errors.add(new ParseError(this.reader.pos(), "Invalid character reference: %s", new Object[] { paramString }));
    }
  }
  
  private void error(String paramString)
  {
    if (this.errors.canAddError()) {
      this.errors.add(new ParseError(this.reader.pos(), paramString));
    }
  }
  
  void acknowledgeSelfClosingFlag()
  {
    this.selfClosingFlagAcknowledged = true;
  }
  
  void advanceTransition(TokeniserState paramTokeniserState)
  {
    this.reader.advance();
    this.state = paramTokeniserState;
  }
  
  String appropriateEndTagName()
  {
    if (this.lastStartTag == null) {}
    for (String str = null;; str = this.lastStartTag) {
      return str;
    }
  }
  
  int[] consumeCharacterReference(Character paramCharacter, boolean paramBoolean)
  {
    if (this.reader.isEmpty()) {
      paramCharacter = null;
    }
    for (;;)
    {
      return paramCharacter;
      if ((paramCharacter != null) && (paramCharacter.charValue() == this.reader.current()))
      {
        paramCharacter = null;
        continue;
      }
      if (this.reader.matchesAnySorted(notCharRefCharsSorted))
      {
        paramCharacter = null;
        continue;
      }
      int[] arrayOfInt = this.codepointHolder;
      this.reader.mark();
      label138:
      int j;
      int i;
      if (this.reader.matchConsume("#"))
      {
        paramBoolean = this.reader.matchConsumeIgnoreCase("X");
        if (paramBoolean) {}
        for (paramCharacter = this.reader.consumeHexSequence();; paramCharacter = this.reader.consumeDigitSequence())
        {
          if (paramCharacter.length() != 0) {
            break label138;
          }
          characterReferenceError("numeric reference with no numerals");
          this.reader.rewindToMark();
          paramCharacter = null;
          break;
        }
        if (!this.reader.matchConsume(";")) {
          characterReferenceError("missing semicolon");
        }
        j = -1;
        if (paramBoolean) {
          i = 16;
        }
      }
      try
      {
        for (;;)
        {
          i = Integer.valueOf(paramCharacter, i).intValue();
          if ((i != -1) && ((i < 55296) || (i > 57343)) && (i <= 1114111)) {
            break label222;
          }
          characterReferenceError("character outside of valid range");
          arrayOfInt[0] = 65533;
          paramCharacter = arrayOfInt;
          break;
          i = 10;
        }
        label222:
        arrayOfInt[0] = i;
        paramCharacter = arrayOfInt;
        continue;
        paramCharacter = this.reader.consumeLetterThenDigitSequence();
        boolean bool = this.reader.matches(';');
        if ((Entities.isBaseNamedEntity(paramCharacter)) || ((Entities.isNamedEntity(paramCharacter)) && (bool))) {}
        for (i = 1;; i = 0)
        {
          if (i != 0) {
            break label316;
          }
          this.reader.rewindToMark();
          if (bool) {
            characterReferenceError(String.format("invalid named referenece '%s'", new Object[] { paramCharacter }));
          }
          paramCharacter = null;
          break;
        }
        label316:
        if (paramBoolean) {
          if ((!this.reader.matchesLetter()) && (!this.reader.matchesDigit()))
          {
            if (!this.reader.matchesAny(new char[] { 61, 45, 95 })) {}
          }
          else
          {
            this.reader.rewindToMark();
            paramCharacter = null;
            continue;
          }
        }
        if (!this.reader.matchConsume(";")) {
          characterReferenceError("missing semicolon");
        }
        i = Entities.codepointsForName(paramCharacter, this.multipointHolder);
        if (i == 1)
        {
          arrayOfInt[0] = this.multipointHolder[0];
          paramCharacter = arrayOfInt;
          continue;
        }
        if (i == 2)
        {
          paramCharacter = this.multipointHolder;
          continue;
        }
        Validate.fail("Unexpected characters returned for " + paramCharacter);
        paramCharacter = this.multipointHolder;
      }
      catch (NumberFormatException paramCharacter)
      {
        for (;;)
        {
          i = j;
        }
      }
    }
  }
  
  void createCommentPending()
  {
    this.commentPending.reset();
  }
  
  void createDoctypePending()
  {
    this.doctypePending.reset();
  }
  
  Token.Tag createTagPending(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (Token.Tag localTag = this.startPending.reset();; localTag = this.endPending.reset())
    {
      this.tagPending = localTag;
      return this.tagPending;
    }
  }
  
  void createTempBuffer()
  {
    Token.reset(this.dataBuffer);
  }
  
  boolean currentNodeInHtmlNS()
  {
    return true;
  }
  
  void emit(char paramChar)
  {
    emit(String.valueOf(paramChar));
  }
  
  void emit(String paramString)
  {
    if (this.charsString == null) {
      this.charsString = paramString;
    }
    for (;;)
    {
      return;
      if (this.charsBuilder.length() == 0) {
        this.charsBuilder.append(this.charsString);
      }
      this.charsBuilder.append(paramString);
    }
  }
  
  void emit(Token paramToken)
  {
    Validate.isFalse(this.isEmitPending, "There is an unread token pending!");
    this.emitPending = paramToken;
    this.isEmitPending = true;
    if (paramToken.type == Token.TokenType.StartTag)
    {
      paramToken = (Token.StartTag)paramToken;
      this.lastStartTag = paramToken.tagName;
      if (paramToken.selfClosing) {
        this.selfClosingFlagAcknowledged = false;
      }
    }
    for (;;)
    {
      return;
      if ((paramToken.type == Token.TokenType.EndTag) && (((Token.EndTag)paramToken).attributes != null)) {
        error("Attributes incorrectly present on end tag");
      }
    }
  }
  
  void emit(char[] paramArrayOfChar)
  {
    emit(String.valueOf(paramArrayOfChar));
  }
  
  void emit(int[] paramArrayOfInt)
  {
    emit(new String(paramArrayOfInt, 0, paramArrayOfInt.length));
  }
  
  void emitCommentPending()
  {
    emit(this.commentPending);
  }
  
  void emitDoctypePending()
  {
    emit(this.doctypePending);
  }
  
  void emitTagPending()
  {
    this.tagPending.finaliseTag();
    emit(this.tagPending);
  }
  
  void eofError(TokeniserState paramTokeniserState)
  {
    if (this.errors.canAddError()) {
      this.errors.add(new ParseError(this.reader.pos(), "Unexpectedly reached end of file (EOF) in input state [%s]", new Object[] { paramTokeniserState }));
    }
  }
  
  void error(TokeniserState paramTokeniserState)
  {
    if (this.errors.canAddError()) {
      this.errors.add(new ParseError(this.reader.pos(), "Unexpected character '%s' in input state [%s]", new Object[] { Character.valueOf(this.reader.current()), paramTokeniserState }));
    }
  }
  
  TokeniserState getState()
  {
    return this.state;
  }
  
  boolean isAppropriateEndTagToken()
  {
    if ((this.lastStartTag != null) && (this.tagPending.name().equalsIgnoreCase(this.lastStartTag))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  Token read()
  {
    if (!this.selfClosingFlagAcknowledged)
    {
      error("Self closing flag not acknowledged");
      this.selfClosingFlagAcknowledged = true;
    }
    while (!this.isEmitPending) {
      this.state.read(this, this.reader);
    }
    Object localObject;
    if (this.charsBuilder.length() > 0)
    {
      localObject = this.charsBuilder.toString();
      this.charsBuilder.delete(0, this.charsBuilder.length());
      this.charsString = null;
      localObject = this.charPending.data((String)localObject);
    }
    for (;;)
    {
      return (Token)localObject;
      if (this.charsString != null)
      {
        localObject = this.charPending.data(this.charsString);
        this.charsString = null;
      }
      else
      {
        this.isEmitPending = false;
        localObject = this.emitPending;
      }
    }
  }
  
  void transition(TokeniserState paramTokeniserState)
  {
    this.state = paramTokeniserState;
  }
  
  String unescapeEntities(boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    while (!this.reader.isEmpty())
    {
      localStringBuilder.append(this.reader.consumeTo('&'));
      if (this.reader.matches('&'))
      {
        this.reader.consume();
        int[] arrayOfInt = consumeCharacterReference(null, paramBoolean);
        if ((arrayOfInt == null) || (arrayOfInt.length == 0))
        {
          localStringBuilder.append('&');
        }
        else
        {
          localStringBuilder.appendCodePoint(arrayOfInt[0]);
          if (arrayOfInt.length == 2) {
            localStringBuilder.appendCodePoint(arrayOfInt[1]);
          }
        }
      }
    }
    return localStringBuilder.toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\Tokeniser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */