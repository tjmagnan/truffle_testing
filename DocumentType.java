package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.helper.StringUtil;

public class DocumentType
  extends Node
{
  private static final String NAME = "name";
  private static final String PUBLIC_ID = "publicId";
  public static final String PUBLIC_KEY = "PUBLIC";
  private static final String PUB_SYS_KEY = "pubSysKey";
  private static final String SYSTEM_ID = "systemId";
  public static final String SYSTEM_KEY = "SYSTEM";
  
  public DocumentType(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    super(paramString4);
    attr("name", paramString1);
    attr("publicId", paramString2);
    if (has("publicId")) {
      attr("pubSysKey", "PUBLIC");
    }
    attr("systemId", paramString3);
  }
  
  public DocumentType(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    super(paramString5);
    attr("name", paramString1);
    if (paramString2 != null) {
      attr("pubSysKey", paramString2);
    }
    attr("publicId", paramString3);
    attr("systemId", paramString4);
  }
  
  private boolean has(String paramString)
  {
    if (!StringUtil.isBlank(attr(paramString))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public String nodeName()
  {
    return "#doctype";
  }
  
  void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    if ((paramOutputSettings.syntax() == Document.OutputSettings.Syntax.html) && (!has("publicId")) && (!has("systemId"))) {
      paramAppendable.append("<!doctype");
    }
    for (;;)
    {
      if (has("name")) {
        paramAppendable.append(" ").append(attr("name"));
      }
      if (has("pubSysKey")) {
        paramAppendable.append(" ").append(attr("pubSysKey"));
      }
      if (has("publicId")) {
        paramAppendable.append(" \"").append(attr("publicId")).append('"');
      }
      if (has("systemId")) {
        paramAppendable.append(" \"").append(attr("systemId")).append('"');
      }
      paramAppendable.append('>');
      return;
      paramAppendable.append("<!DOCTYPE");
    }
  }
  
  void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings) {}
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\DocumentType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */