package org.jsoup.parser;

import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

public class Parser
{
  private static final int DEFAULT_MAX_ERRORS = 0;
  private ParseErrorList errors;
  private int maxErrors = 0;
  private ParseSettings settings;
  private TreeBuilder treeBuilder;
  
  public Parser(TreeBuilder paramTreeBuilder)
  {
    this.treeBuilder = paramTreeBuilder;
    this.settings = paramTreeBuilder.defaultSettings();
  }
  
  public static Parser htmlParser()
  {
    return new Parser(new HtmlTreeBuilder());
  }
  
  public static Document parse(String paramString1, String paramString2)
  {
    HtmlTreeBuilder localHtmlTreeBuilder = new HtmlTreeBuilder();
    return localHtmlTreeBuilder.parse(paramString1, paramString2, ParseErrorList.noTracking(), localHtmlTreeBuilder.defaultSettings());
  }
  
  public static Document parseBodyFragment(String paramString1, String paramString2)
  {
    Document localDocument = Document.createShell(paramString2);
    Element localElement = localDocument.body();
    paramString1 = parseFragment(paramString1, localElement, paramString2);
    paramString1 = (Node[])paramString1.toArray(new Node[paramString1.size()]);
    for (int i = paramString1.length - 1; i > 0; i--) {
      paramString1[i].remove();
    }
    int j = paramString1.length;
    for (i = 0; i < j; i++) {
      localElement.appendChild(paramString1[i]);
    }
    return localDocument;
  }
  
  public static Document parseBodyFragmentRelaxed(String paramString1, String paramString2)
  {
    return parse(paramString1, paramString2);
  }
  
  public static List<Node> parseFragment(String paramString1, Element paramElement, String paramString2)
  {
    HtmlTreeBuilder localHtmlTreeBuilder = new HtmlTreeBuilder();
    return localHtmlTreeBuilder.parseFragment(paramString1, paramElement, paramString2, ParseErrorList.noTracking(), localHtmlTreeBuilder.defaultSettings());
  }
  
  public static List<Node> parseFragment(String paramString1, Element paramElement, String paramString2, ParseErrorList paramParseErrorList)
  {
    HtmlTreeBuilder localHtmlTreeBuilder = new HtmlTreeBuilder();
    return localHtmlTreeBuilder.parseFragment(paramString1, paramElement, paramString2, paramParseErrorList, localHtmlTreeBuilder.defaultSettings());
  }
  
  public static List<Node> parseXmlFragment(String paramString1, String paramString2)
  {
    XmlTreeBuilder localXmlTreeBuilder = new XmlTreeBuilder();
    return localXmlTreeBuilder.parseFragment(paramString1, paramString2, ParseErrorList.noTracking(), localXmlTreeBuilder.defaultSettings());
  }
  
  public static String unescapeEntities(String paramString, boolean paramBoolean)
  {
    return new Tokeniser(new CharacterReader(paramString), ParseErrorList.noTracking()).unescapeEntities(paramBoolean);
  }
  
  public static Parser xmlParser()
  {
    return new Parser(new XmlTreeBuilder());
  }
  
  public List<ParseError> getErrors()
  {
    return this.errors;
  }
  
  public TreeBuilder getTreeBuilder()
  {
    return this.treeBuilder;
  }
  
  public boolean isTrackErrors()
  {
    if (this.maxErrors > 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Document parseInput(String paramString1, String paramString2)
  {
    if (isTrackErrors()) {}
    for (ParseErrorList localParseErrorList = ParseErrorList.tracking(this.maxErrors);; localParseErrorList = ParseErrorList.noTracking())
    {
      this.errors = localParseErrorList;
      return this.treeBuilder.parse(paramString1, paramString2, this.errors, this.settings);
    }
  }
  
  public Parser setTrackErrors(int paramInt)
  {
    this.maxErrors = paramInt;
    return this;
  }
  
  public Parser setTreeBuilder(TreeBuilder paramTreeBuilder)
  {
    this.treeBuilder = paramTreeBuilder;
    return this;
  }
  
  public ParseSettings settings()
  {
    return this.settings;
  }
  
  public Parser settings(ParseSettings paramParseSettings)
  {
    this.settings = paramParseSettings;
    return this;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\Parser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */