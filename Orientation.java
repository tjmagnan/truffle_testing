package net.sourceforge.zbar;

public class Orientation
{
  public static final int DOWN = 2;
  public static final int LEFT = 3;
  public static final int RIGHT = 1;
  public static final int UNKNOWN = -1;
  public static final int UP = 0;
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\net\sourceforge\zbar\Orientation.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */