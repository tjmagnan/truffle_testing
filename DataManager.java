package tech.dcube.companion.managers.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.model.CatalogItemDataModel;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBDevice;
import tech.dcube.companion.model.PBDeviceNotification;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBParcelPackage;
import tech.dcube.companion.model.PBSendProItem;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentService;
import tech.dcube.companion.model.PBShipmentTracking;
import tech.dcube.companion.model.PBShipmentTrackingDetail;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.TrackListDataModel;

public class DataManager
{
  private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
  @SuppressLint({"StaticFieldLeak"})
  private static final DataManager ourInstance = new DataManager();
  private List<PBAddress> addresses;
  private List<CatalogItemDataModel> catalogItemDataModels;
  private Context context;
  private PBShipment currentShipment;
  private List<PBDevice> devices;
  private List<PBSendProItem> items;
  private Map<String, Boolean> meterModelSerialNotificationError;
  private Map<String, Boolean> meterModelSerialNotificationWarning;
  private Map<String, Boolean> meterModelSerialShowNotification;
  private Map<String, List<PBDeviceNotification>> meterNotificationList;
  private Map<String, String> meterSerialToNickName;
  private Map<String, Boolean> meterSubscriptions;
  private List<PBDeviceNotification> notifications;
  private boolean packagesV0Success;
  private Map<String, PBParcelPackage> parcelPackages;
  private PBShipmentTrackingDetail queryTrackingDetail;
  private List<PBShipmentService> services;
  private Map<PBPackageType, List<PBShipmentPackage>> shipmentPackages;
  private List<TrackListDataModel> trackListDataModels = new ArrayList();
  private List<PBShipmentTracking> trackings;
  private PBUser user;
  
  private DataManager()
  {
    this.trackListDataModels.add(new TrackListDataModel("Order #890550", "Walter Bowes", "05/09 Waiting for pickup"));
    this.trackListDataModels.add(new TrackListDataModel("Order #890657", "Jess Potato", "05/05 Arrived at USPS Facility"));
    this.catalogItemDataModels = new ArrayList();
    this.catalogItemDataModels.add(new CatalogItemDataModel("Apple iPhone SE 64GB Rose Gold", "The most powerful 4-inch phone everwith A9 chip. The 12-MP camera captures incredible photos and 4K videos.", 4.900000095367432D, 2.299999952316284D, 0.30000001192092896D, 10.699999809265137D, "888462803946"));
    this.meterSerialToNickName = new HashMap();
    this.meterModelSerialShowNotification = new HashMap();
    this.meterModelSerialNotificationError = new HashMap();
    this.meterModelSerialNotificationWarning = new HashMap();
    this.meterNotificationList = new HashMap();
  }
  
  public static DataManager getInstance()
  {
    return ourInstance;
  }
  
  public List<PBAddress> getAddresses()
  {
    if (this.addresses == null) {
      this.addresses = new ArrayList();
    }
    return this.addresses;
  }
  
  public Context getContext()
  {
    return this.context;
  }
  
  public PBShipment getCurrentShipment()
  {
    return this.currentShipment;
  }
  
  public List<PBDevice> getDevices()
  {
    if (this.devices == null) {
      this.devices = new ArrayList();
    }
    return this.devices;
  }
  
  public List<PBSendProItem> getItems()
  {
    if (this.items == null) {
      this.items = new ArrayList();
    }
    return this.items;
  }
  
  public Map<String, Boolean> getMeterModelSerialNotificationError()
  {
    return this.meterModelSerialNotificationError;
  }
  
  public Map<String, Boolean> getMeterModelSerialNotificationWarning()
  {
    return this.meterModelSerialNotificationWarning;
  }
  
  public Map<String, Boolean> getMeterModelSerialShowNotification()
  {
    return this.meterModelSerialShowNotification;
  }
  
  public Map<String, List<PBDeviceNotification>> getMeterNotificationList()
  {
    return this.meterNotificationList;
  }
  
  public Map<String, String> getMeterSerialToNickName()
  {
    return this.meterSerialToNickName;
  }
  
  public Map<String, Boolean> getMeterSubscriptions()
  {
    if ((this.meterSubscriptions == null) || (this.meterSubscriptions.isEmpty()))
    {
      this.meterSubscriptions = new HashMap();
      this.meterSubscriptions.put("SUPPLY", Boolean.valueOf(false));
      this.meterSubscriptions.put("FUNDS", Boolean.valueOf(false));
      this.meterSubscriptions.put("DEVICE", Boolean.valueOf(false));
    }
    return this.meterSubscriptions;
  }
  
  public List<PBDeviceNotification> getNotifications()
  {
    if (this.notifications == null) {
      this.notifications = new ArrayList();
    }
    return this.notifications;
  }
  
  public Map<String, PBParcelPackage> getParcelPackages()
  {
    if (this.parcelPackages == null) {
      this.parcelPackages = new HashMap();
    }
    return this.parcelPackages;
  }
  
  public PBShipmentTrackingDetail getQueryTrackingDetail()
  {
    return this.queryTrackingDetail;
  }
  
  public List<PBShipmentService> getServices()
  {
    if (this.services == null) {
      this.services = new ArrayList();
    }
    return this.services;
  }
  
  public Map<PBPackageType, List<PBShipmentPackage>> getShipmentPackages()
  {
    if (this.shipmentPackages == null) {
      this.shipmentPackages = new HashMap();
    }
    return this.shipmentPackages;
  }
  
  public List<PBShipmentTracking> getTrackings()
  {
    if (this.trackings == null) {
      this.trackings = new ArrayList();
    }
    return this.trackings;
  }
  
  public PBUser getUser()
  {
    if (this.user == null) {}
    try
    {
      String str = this.context.getSharedPreferences("user_credentials", 0).getString("user", "");
      this.user = ((PBUser)gson.fromJson(str, PBUser.class));
      return this.user;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.wtf("Get User Exception", localException);
      }
    }
  }
  
  public boolean isPackagesV0Success()
  {
    return this.packagesV0Success;
  }
  
  public void logout()
  {
    this.user = new PBUser();
    updateUser();
    this.devices = new ArrayList();
    this.notifications = new ArrayList();
    this.items = new ArrayList();
    this.addresses = new ArrayList();
    this.trackings = new ArrayList();
    this.services = new ArrayList();
    this.shipmentPackages = new HashMap();
    this.parcelPackages = new HashMap();
    this.packagesV0Success = false;
    this.currentShipment = new PBShipment();
    this.meterSerialToNickName = new HashMap();
    this.meterModelSerialShowNotification = new HashMap();
    this.meterModelSerialNotificationError = new HashMap();
    this.meterModelSerialNotificationWarning = new HashMap();
  }
  
  public void setContext(Context paramContext)
  {
    this.context = paramContext;
  }
  
  public void setCurrentShipment(PBShipment paramPBShipment)
  {
    this.currentShipment = paramPBShipment;
  }
  
  public void setMeterModelSerialNotificationError(Map<String, Boolean> paramMap)
  {
    this.meterModelSerialNotificationError = paramMap;
  }
  
  public void setMeterModelSerialNotificationWarning(Map<String, Boolean> paramMap)
  {
    this.meterModelSerialNotificationWarning = paramMap;
  }
  
  public void setMeterNotificationList(Map<String, List<PBDeviceNotification>> paramMap)
  {
    this.meterNotificationList = paramMap;
  }
  
  public void setMeterSubscriptions(Map<String, Boolean> paramMap)
  {
    this.meterSubscriptions = paramMap;
  }
  
  public void setPackagesV0Success(boolean paramBoolean)
  {
    this.packagesV0Success = paramBoolean;
  }
  
  public void setQueryTrackingDetail(PBShipmentTrackingDetail paramPBShipmentTrackingDetail)
  {
    this.queryTrackingDetail = paramPBShipmentTrackingDetail;
  }
  
  public void setUser(PBUser paramPBUser)
  {
    this.user = paramPBUser;
    updateUser();
  }
  
  public void updateUser()
  {
    SharedPreferences.Editor localEditor = this.context.getSharedPreferences("user_credentials", 0).edit();
    if (this.user == null) {
      localEditor.remove("user");
    }
    for (;;)
    {
      localEditor.apply();
      return;
      localEditor.putString("user", this.user.toJsonString());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\data\DataManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */