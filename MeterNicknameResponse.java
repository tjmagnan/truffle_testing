package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MeterNicknameResponse
  extends PBObject
  implements Serializable
{
  @Expose
  @SerializedName("message")
  String message;
  @Expose
  @SerializedName("results")
  List<MeterNickname> results = new ArrayList();
  
  public MeterNicknameResponse() {}
  
  public MeterNicknameResponse(String paramString, List<MeterNickname> paramList)
  {
    this.message = paramString;
    this.results = paramList;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public List<MeterNickname> getResults()
  {
    return this.results;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public void setResults(List<MeterNickname> paramList)
  {
    this.results = paramList;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\MeterNicknameResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */