package android.support.v4.app;

import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class FragmentTransition
{
  private static final int[] INVERSE_OPS = { 0, 3, 0, 1, 5, 4, 7, 6 };
  
  private static void addSharedElementsWithMatchingNames(ArrayList<View> paramArrayList, ArrayMap<String, View> paramArrayMap, Collection<String> paramCollection)
  {
    for (int i = paramArrayMap.size() - 1; i >= 0; i--)
    {
      View localView = (View)paramArrayMap.valueAt(i);
      if (paramCollection.contains(ViewCompat.getTransitionName(localView))) {
        paramArrayList.add(localView);
      }
    }
  }
  
  private static void addToFirstInLastOut(BackStackRecord paramBackStackRecord, BackStackRecord.Op paramOp, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean1, boolean paramBoolean2)
  {
    Fragment localFragment = paramOp.fragment;
    int i3 = localFragment.mContainerId;
    if (i3 == 0) {
      return;
    }
    int m;
    label33:
    boolean bool2;
    int i1;
    int i2;
    int n;
    boolean bool1;
    int j;
    int k;
    if (paramBoolean1)
    {
      m = INVERSE_OPS[paramOp.cmd];
      bool2 = false;
      i1 = 0;
      i2 = 0;
      n = 0;
      i = i2;
      bool1 = bool2;
      j = n;
      k = i1;
    }
    switch (m)
    {
    default: 
      k = i1;
      j = n;
      bool1 = bool2;
      i = i2;
    case 2: 
    case 5: 
    case 1: 
    case 7: 
      for (;;)
      {
        Object localObject = (FragmentContainerTransition)paramSparseArray.get(i3);
        paramOp = (BackStackRecord.Op)localObject;
        if (bool1)
        {
          paramOp = ensureContainer((FragmentContainerTransition)localObject, paramSparseArray, i3);
          paramOp.lastIn = localFragment;
          paramOp.lastInIsPop = paramBoolean1;
          paramOp.lastInTransaction = paramBackStackRecord;
        }
        if ((!paramBoolean2) && (j != 0))
        {
          if ((paramOp != null) && (paramOp.firstOut == localFragment)) {
            paramOp.firstOut = null;
          }
          localObject = paramBackStackRecord.mManager;
          if ((localFragment.mState < 1) && (((FragmentManagerImpl)localObject).mCurState >= 1) && (!paramBackStackRecord.mAllowOptimization))
          {
            ((FragmentManagerImpl)localObject).makeActive(localFragment);
            ((FragmentManagerImpl)localObject).moveToState(localFragment, 1, 0, 0, false);
          }
        }
        localObject = paramOp;
        if (i != 0) {
          if (paramOp != null)
          {
            localObject = paramOp;
            if (paramOp.firstOut != null) {}
          }
          else
          {
            localObject = ensureContainer(paramOp, paramSparseArray, i3);
            ((FragmentContainerTransition)localObject).firstOut = localFragment;
            ((FragmentContainerTransition)localObject).firstOutIsPop = paramBoolean1;
            ((FragmentContainerTransition)localObject).firstOutTransaction = paramBackStackRecord;
          }
        }
        if ((paramBoolean2) || (k == 0) || (localObject == null) || (((FragmentContainerTransition)localObject).lastIn != localFragment)) {
          break;
        }
        ((FragmentContainerTransition)localObject).lastIn = null;
        break;
        m = paramOp.cmd;
        break label33;
        if (paramBoolean2) {
          if ((localFragment.mHiddenChanged) && (!localFragment.mHidden) && (localFragment.mAdded)) {
            bool1 = true;
          }
        }
        for (;;)
        {
          j = 1;
          i = i2;
          k = i1;
          break;
          bool1 = false;
          continue;
          bool1 = localFragment.mHidden;
        }
        if (!paramBoolean2) {
          break label422;
        }
        bool1 = localFragment.mIsNewlyAdded;
        j = 1;
        i = i2;
        k = i1;
      }
      if ((!localFragment.mAdded) && (!localFragment.mHidden)) {}
      for (bool1 = true;; bool1 = false) {
        break;
      }
    case 4: 
      label422:
      if (paramBoolean2)
      {
        if ((localFragment.mHiddenChanged) && (localFragment.mAdded) && (localFragment.mHidden)) {}
        for (i = 1;; i = 0)
        {
          k = 1;
          bool1 = bool2;
          j = n;
          break;
        }
      }
      if ((localFragment.mAdded) && (!localFragment.mHidden)) {}
      for (i = 1;; i = 0) {
        break;
      }
    }
    if (paramBoolean2)
    {
      if ((!localFragment.mAdded) && (localFragment.mView != null) && (localFragment.mView.getVisibility() == 0) && (localFragment.mPostponedAlpha >= 0.0F)) {}
      for (i = 1;; i = 0)
      {
        k = 1;
        bool1 = bool2;
        j = n;
        break;
      }
    }
    if ((localFragment.mAdded) && (!localFragment.mHidden)) {}
    for (int i = 1;; i = 0) {
      break;
    }
  }
  
  public static void calculateFragments(BackStackRecord paramBackStackRecord, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean)
  {
    int j = paramBackStackRecord.mOps.size();
    for (int i = 0; i < j; i++) {
      addToFirstInLastOut(paramBackStackRecord, (BackStackRecord.Op)paramBackStackRecord.mOps.get(i), paramSparseArray, false, paramBoolean);
    }
  }
  
  private static ArrayMap<String, String> calculateNameOverrides(int paramInt1, ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt2, int paramInt3)
  {
    ArrayMap localArrayMap = new ArrayMap();
    paramInt3--;
    if (paramInt3 >= paramInt2)
    {
      Object localObject = (BackStackRecord)paramArrayList.get(paramInt3);
      if (!((BackStackRecord)localObject).interactsWith(paramInt1)) {}
      boolean bool;
      do
      {
        paramInt3--;
        break;
        bool = ((Boolean)paramArrayList1.get(paramInt3)).booleanValue();
      } while (((BackStackRecord)localObject).mSharedElementSourceNames == null);
      int j = ((BackStackRecord)localObject).mSharedElementSourceNames.size();
      ArrayList localArrayList2;
      ArrayList localArrayList1;
      label95:
      int i;
      label98:
      String str1;
      if (bool)
      {
        localArrayList2 = ((BackStackRecord)localObject).mSharedElementSourceNames;
        localArrayList1 = ((BackStackRecord)localObject).mSharedElementTargetNames;
        i = 0;
        if (i < j)
        {
          str1 = (String)localArrayList1.get(i);
          localObject = (String)localArrayList2.get(i);
          String str2 = (String)localArrayMap.remove(localObject);
          if (str2 == null) {
            break label179;
          }
          localArrayMap.put(str1, str2);
        }
      }
      for (;;)
      {
        i++;
        break label98;
        break;
        localArrayList1 = ((BackStackRecord)localObject).mSharedElementSourceNames;
        localArrayList2 = ((BackStackRecord)localObject).mSharedElementTargetNames;
        break label95;
        label179:
        localArrayMap.put(str1, localObject);
      }
    }
    return localArrayMap;
  }
  
  public static void calculatePopFragments(BackStackRecord paramBackStackRecord, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean)
  {
    if (!paramBackStackRecord.mManager.mContainer.onHasView()) {}
    for (;;)
    {
      return;
      for (int i = paramBackStackRecord.mOps.size() - 1; i >= 0; i--) {
        addToFirstInLastOut(paramBackStackRecord, (BackStackRecord.Op)paramBackStackRecord.mOps.get(i), paramSparseArray, true, paramBoolean);
      }
    }
  }
  
  private static void callSharedElementStartEnd(Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean1, ArrayMap<String, View> paramArrayMap, boolean paramBoolean2)
  {
    ArrayList localArrayList;
    if (paramBoolean1)
    {
      paramFragment1 = paramFragment2.getEnterTransitionCallback();
      if (paramFragment1 == null) {
        break label106;
      }
      localArrayList = new ArrayList();
      paramFragment2 = new ArrayList();
      if (paramArrayMap != null) {
        break label84;
      }
    }
    label84:
    for (int i = 0;; i = paramArrayMap.size())
    {
      for (int j = 0; j < i; j++)
      {
        paramFragment2.add(paramArrayMap.keyAt(j));
        localArrayList.add(paramArrayMap.valueAt(j));
      }
      paramFragment1 = paramFragment1.getEnterTransitionCallback();
      break;
    }
    if (paramBoolean2) {
      paramFragment1.onSharedElementStart(paramFragment2, localArrayList, null);
    }
    for (;;)
    {
      label106:
      return;
      paramFragment1.onSharedElementEnd(paramFragment2, localArrayList, null);
    }
  }
  
  private static ArrayMap<String, View> captureInSharedElements(ArrayMap<String, String> paramArrayMap, Object paramObject, FragmentContainerTransition paramFragmentContainerTransition)
  {
    Object localObject = paramFragmentContainerTransition.lastIn;
    View localView = ((Fragment)localObject).getView();
    if ((paramArrayMap.isEmpty()) || (paramObject == null) || (localView == null))
    {
      paramArrayMap.clear();
      paramFragmentContainerTransition = null;
    }
    for (;;)
    {
      return paramFragmentContainerTransition;
      ArrayMap localArrayMap = new ArrayMap();
      FragmentTransitionCompat21.findNamedViews(localArrayMap, localView);
      paramObject = paramFragmentContainerTransition.lastInTransaction;
      int i;
      if (paramFragmentContainerTransition.lastInIsPop)
      {
        paramFragmentContainerTransition = ((Fragment)localObject).getExitTransitionCallback();
        paramObject = ((BackStackRecord)paramObject).mSharedElementSourceNames;
        if (paramObject != null) {
          localArrayMap.retainAll((Collection)paramObject);
        }
        if (paramFragmentContainerTransition == null) {
          break label213;
        }
        paramFragmentContainerTransition.onMapSharedElements((List)paramObject, localArrayMap);
        i = ((ArrayList)paramObject).size() - 1;
        label105:
        paramFragmentContainerTransition = localArrayMap;
        if (i < 0) {
          continue;
        }
        localObject = (String)((ArrayList)paramObject).get(i);
        paramFragmentContainerTransition = (View)localArrayMap.get(localObject);
        if (paramFragmentContainerTransition != null) {
          break label174;
        }
        paramFragmentContainerTransition = findKeyForValue(paramArrayMap, (String)localObject);
        if (paramFragmentContainerTransition != null) {
          paramArrayMap.remove(paramFragmentContainerTransition);
        }
      }
      for (;;)
      {
        i--;
        break label105;
        paramFragmentContainerTransition = ((Fragment)localObject).getEnterTransitionCallback();
        paramObject = ((BackStackRecord)paramObject).mSharedElementTargetNames;
        break;
        label174:
        if (!((String)localObject).equals(ViewCompat.getTransitionName(paramFragmentContainerTransition)))
        {
          localObject = findKeyForValue(paramArrayMap, (String)localObject);
          if (localObject != null) {
            paramArrayMap.put(localObject, ViewCompat.getTransitionName(paramFragmentContainerTransition));
          }
        }
      }
      label213:
      retainValues(paramArrayMap, localArrayMap);
      paramFragmentContainerTransition = localArrayMap;
    }
  }
  
  private static ArrayMap<String, View> captureOutSharedElements(ArrayMap<String, String> paramArrayMap, Object paramObject, FragmentContainerTransition paramFragmentContainerTransition)
  {
    if ((paramArrayMap.isEmpty()) || (paramObject == null))
    {
      paramArrayMap.clear();
      paramFragmentContainerTransition = null;
    }
    for (;;)
    {
      return paramFragmentContainerTransition;
      Object localObject = paramFragmentContainerTransition.firstOut;
      ArrayMap localArrayMap = new ArrayMap();
      FragmentTransitionCompat21.findNamedViews(localArrayMap, ((Fragment)localObject).getView());
      paramObject = paramFragmentContainerTransition.firstOutTransaction;
      int i;
      if (paramFragmentContainerTransition.firstOutIsPop)
      {
        paramFragmentContainerTransition = ((Fragment)localObject).getEnterTransitionCallback();
        paramObject = ((BackStackRecord)paramObject).mSharedElementTargetNames;
        localArrayMap.retainAll((Collection)paramObject);
        if (paramFragmentContainerTransition == null) {
          break label188;
        }
        paramFragmentContainerTransition.onMapSharedElements((List)paramObject, localArrayMap);
        i = ((ArrayList)paramObject).size() - 1;
        label92:
        paramFragmentContainerTransition = localArrayMap;
        if (i < 0) {
          continue;
        }
        localObject = (String)((ArrayList)paramObject).get(i);
        paramFragmentContainerTransition = (View)localArrayMap.get(localObject);
        if (paramFragmentContainerTransition != null) {
          break label151;
        }
        paramArrayMap.remove(localObject);
      }
      for (;;)
      {
        i--;
        break label92;
        paramFragmentContainerTransition = ((Fragment)localObject).getExitTransitionCallback();
        paramObject = ((BackStackRecord)paramObject).mSharedElementSourceNames;
        break;
        label151:
        if (!((String)localObject).equals(ViewCompat.getTransitionName(paramFragmentContainerTransition)))
        {
          localObject = (String)paramArrayMap.remove(localObject);
          paramArrayMap.put(ViewCompat.getTransitionName(paramFragmentContainerTransition), localObject);
        }
      }
      label188:
      paramArrayMap.retainAll(localArrayMap.keySet());
      paramFragmentContainerTransition = localArrayMap;
    }
  }
  
  private static ArrayList<View> configureEnteringExitingViews(Object paramObject, Fragment paramFragment, ArrayList<View> paramArrayList, View paramView)
  {
    Object localObject = null;
    if (paramObject != null)
    {
      ArrayList localArrayList = new ArrayList();
      paramFragment = paramFragment.getView();
      if (paramFragment != null) {
        FragmentTransitionCompat21.captureTransitioningViews(localArrayList, paramFragment);
      }
      if (paramArrayList != null) {
        localArrayList.removeAll(paramArrayList);
      }
      localObject = localArrayList;
      if (!localArrayList.isEmpty())
      {
        localArrayList.add(paramView);
        FragmentTransitionCompat21.addTargets(paramObject, localArrayList);
        localObject = localArrayList;
      }
    }
    return (ArrayList<View>)localObject;
  }
  
  private static Object configureSharedElementsOptimized(ViewGroup paramViewGroup, final View paramView, ArrayMap<String, String> paramArrayMap, final FragmentContainerTransition paramFragmentContainerTransition, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2, Object paramObject1, Object paramObject2)
  {
    Fragment localFragment1 = paramFragmentContainerTransition.lastIn;
    final Fragment localFragment2 = paramFragmentContainerTransition.firstOut;
    if (localFragment1 != null) {
      localFragment1.getView().setVisibility(0);
    }
    if ((localFragment1 == null) || (localFragment2 == null))
    {
      paramArrayMap = null;
      return paramArrayMap;
    }
    final boolean bool = paramFragmentContainerTransition.lastInIsPop;
    Object localObject;
    label56:
    ArrayMap localArrayMap2;
    final ArrayMap localArrayMap1;
    if (paramArrayMap.isEmpty())
    {
      localObject = null;
      localArrayMap2 = captureOutSharedElements(paramArrayMap, localObject, paramFragmentContainerTransition);
      localArrayMap1 = captureInSharedElements(paramArrayMap, localObject, paramFragmentContainerTransition);
      if (!paramArrayMap.isEmpty()) {
        break label143;
      }
      localObject = null;
      if (localArrayMap2 != null) {
        localArrayMap2.clear();
      }
      paramArrayMap = (ArrayMap<String, String>)localObject;
      if (localArrayMap1 != null) {
        localArrayMap1.clear();
      }
    }
    for (paramArrayMap = (ArrayMap<String, String>)localObject;; paramArrayMap = (ArrayMap<String, String>)localObject)
    {
      if ((paramObject1 != null) || (paramObject2 != null) || (paramArrayMap != null)) {
        break label171;
      }
      paramArrayMap = null;
      break;
      localObject = getSharedElementTransition(localFragment1, localFragment2, bool);
      break label56;
      label143:
      addSharedElementsWithMatchingNames(paramArrayList1, localArrayMap2, paramArrayMap.keySet());
      addSharedElementsWithMatchingNames(paramArrayList2, localArrayMap1, paramArrayMap.values());
    }
    label171:
    callSharedElementStartEnd(localFragment1, localFragment2, bool, localArrayMap2, true);
    if (paramArrayMap != null)
    {
      paramArrayList2.add(paramView);
      FragmentTransitionCompat21.setSharedElementTargets(paramArrayMap, paramView, paramArrayList1);
      setOutEpicenter(paramArrayMap, paramObject2, localArrayMap2, paramFragmentContainerTransition.firstOutIsPop, paramFragmentContainerTransition.firstOutTransaction);
      paramArrayList1 = new Rect();
      paramArrayList2 = getInEpicenterView(localArrayMap1, paramFragmentContainerTransition, paramObject1, bool);
      paramView = paramArrayList2;
      paramFragmentContainerTransition = paramArrayList1;
      if (paramArrayList2 != null)
      {
        FragmentTransitionCompat21.setEpicenter(paramObject1, paramArrayList1);
        paramFragmentContainerTransition = paramArrayList1;
      }
    }
    for (paramView = paramArrayList2;; paramView = null)
    {
      OneShotPreDrawListener.add(paramViewGroup, new Runnable()
      {
        public void run()
        {
          FragmentTransition.callSharedElementStartEnd(this.val$inFragment, localFragment2, bool, localArrayMap1, false);
          if (paramView != null) {
            FragmentTransitionCompat21.getBoundsOnScreen(paramView, paramFragmentContainerTransition);
          }
        }
      });
      break;
      paramFragmentContainerTransition = null;
    }
  }
  
  private static Object configureSharedElementsUnoptimized(ViewGroup paramViewGroup, final View paramView, ArrayMap<String, String> paramArrayMap, final FragmentContainerTransition paramFragmentContainerTransition, final ArrayList<View> paramArrayList1, final ArrayList<View> paramArrayList2, final Object paramObject1, final Object paramObject2)
  {
    final Fragment localFragment2 = paramFragmentContainerTransition.lastIn;
    final Fragment localFragment1 = paramFragmentContainerTransition.firstOut;
    final Object localObject;
    if ((localFragment2 == null) || (localFragment1 == null))
    {
      localObject = null;
      return localObject;
    }
    final boolean bool = paramFragmentContainerTransition.lastInIsPop;
    label44:
    ArrayMap localArrayMap;
    if (paramArrayMap.isEmpty())
    {
      localObject = null;
      localArrayMap = captureOutSharedElements(paramArrayMap, localObject, paramFragmentContainerTransition);
      if (!paramArrayMap.isEmpty()) {
        break label98;
      }
      localObject = null;
    }
    for (;;)
    {
      if ((paramObject1 != null) || (paramObject2 != null) || (localObject != null)) {
        break label112;
      }
      localObject = null;
      break;
      localObject = getSharedElementTransition(localFragment2, localFragment1, bool);
      break label44;
      label98:
      paramArrayList1.addAll(localArrayMap.values());
    }
    label112:
    callSharedElementStartEnd(localFragment2, localFragment1, bool, localArrayMap, true);
    Rect localRect;
    if (localObject != null)
    {
      localRect = new Rect();
      FragmentTransitionCompat21.setSharedElementTargets(localObject, paramView, paramArrayList1);
      setOutEpicenter(localObject, paramObject2, localArrayMap, paramFragmentContainerTransition.firstOutIsPop, paramFragmentContainerTransition.firstOutTransaction);
      paramObject2 = localRect;
      if (paramObject1 != null) {
        FragmentTransitionCompat21.setEpicenter(paramObject1, localRect);
      }
    }
    for (paramObject2 = localRect;; paramObject2 = null)
    {
      OneShotPreDrawListener.add(paramViewGroup, new Runnable()
      {
        public void run()
        {
          Object localObject = FragmentTransition.captureInSharedElements(this.val$nameOverrides, localObject, paramFragmentContainerTransition);
          if (localObject != null)
          {
            paramArrayList2.addAll(((ArrayMap)localObject).values());
            paramArrayList2.add(paramView);
          }
          FragmentTransition.callSharedElementStartEnd(localFragment2, localFragment1, bool, (ArrayMap)localObject, false);
          if (localObject != null)
          {
            FragmentTransitionCompat21.swapSharedElementTargets(localObject, paramArrayList1, paramArrayList2);
            localObject = FragmentTransition.getInEpicenterView((ArrayMap)localObject, paramFragmentContainerTransition, paramObject1, bool);
            if (localObject != null) {
              FragmentTransitionCompat21.getBoundsOnScreen((View)localObject, paramObject2);
            }
          }
        }
      });
      break;
    }
  }
  
  private static void configureTransitionsOptimized(FragmentManagerImpl paramFragmentManagerImpl, int paramInt, FragmentContainerTransition paramFragmentContainerTransition, View paramView, ArrayMap<String, String> paramArrayMap)
  {
    ViewGroup localViewGroup = null;
    if (paramFragmentManagerImpl.mContainer.onHasView()) {
      localViewGroup = (ViewGroup)paramFragmentManagerImpl.mContainer.onFindViewById(paramInt);
    }
    if (localViewGroup == null) {}
    for (;;)
    {
      return;
      Object localObject3 = paramFragmentContainerTransition.lastIn;
      Object localObject2 = paramFragmentContainerTransition.firstOut;
      boolean bool1 = paramFragmentContainerTransition.lastInIsPop;
      boolean bool2 = paramFragmentContainerTransition.firstOutIsPop;
      ArrayList localArrayList1 = new ArrayList();
      ArrayList localArrayList2 = new ArrayList();
      paramFragmentManagerImpl = getEnterTransition((Fragment)localObject3, bool1);
      Object localObject1 = getExitTransition((Fragment)localObject2, bool2);
      paramFragmentContainerTransition = configureSharedElementsOptimized(localViewGroup, paramView, paramArrayMap, paramFragmentContainerTransition, localArrayList2, localArrayList1, paramFragmentManagerImpl, localObject1);
      if ((paramFragmentManagerImpl != null) || (paramFragmentContainerTransition != null) || (localObject1 != null))
      {
        ArrayList localArrayList3 = configureEnteringExitingViews(localObject1, (Fragment)localObject2, localArrayList2, paramView);
        paramView = configureEnteringExitingViews(paramFragmentManagerImpl, (Fragment)localObject3, localArrayList1, paramView);
        setViewVisibility(paramView, 4);
        localObject3 = mergeTransitions(paramFragmentManagerImpl, localObject1, paramFragmentContainerTransition, (Fragment)localObject3, bool1);
        if (localObject3 != null)
        {
          replaceHide(localObject1, (Fragment)localObject2, localArrayList3);
          localObject2 = FragmentTransitionCompat21.prepareSetNameOverridesOptimized(localArrayList1);
          FragmentTransitionCompat21.scheduleRemoveTargets(localObject3, paramFragmentManagerImpl, paramView, localObject1, localArrayList3, paramFragmentContainerTransition, localArrayList1);
          FragmentTransitionCompat21.beginDelayedTransition(localViewGroup, localObject3);
          FragmentTransitionCompat21.setNameOverridesOptimized(localViewGroup, localArrayList2, localArrayList1, (ArrayList)localObject2, paramArrayMap);
          setViewVisibility(paramView, 0);
          FragmentTransitionCompat21.swapSharedElementTargets(paramFragmentContainerTransition, localArrayList2, localArrayList1);
        }
      }
    }
  }
  
  private static void configureTransitionsUnoptimized(FragmentManagerImpl paramFragmentManagerImpl, int paramInt, FragmentContainerTransition paramFragmentContainerTransition, View paramView, ArrayMap<String, String> paramArrayMap)
  {
    ViewGroup localViewGroup = null;
    if (paramFragmentManagerImpl.mContainer.onHasView()) {
      localViewGroup = (ViewGroup)paramFragmentManagerImpl.mContainer.onFindViewById(paramInt);
    }
    if (localViewGroup == null) {}
    for (;;)
    {
      return;
      Fragment localFragment = paramFragmentContainerTransition.lastIn;
      Object localObject3 = paramFragmentContainerTransition.firstOut;
      boolean bool2 = paramFragmentContainerTransition.lastInIsPop;
      boolean bool1 = paramFragmentContainerTransition.firstOutIsPop;
      Object localObject2 = getEnterTransition(localFragment, bool2);
      paramFragmentManagerImpl = getExitTransition((Fragment)localObject3, bool1);
      ArrayList localArrayList2 = new ArrayList();
      ArrayList localArrayList1 = new ArrayList();
      Object localObject1 = configureSharedElementsUnoptimized(localViewGroup, paramView, paramArrayMap, paramFragmentContainerTransition, localArrayList2, localArrayList1, localObject2, paramFragmentManagerImpl);
      if ((localObject2 != null) || (localObject1 != null) || (paramFragmentManagerImpl != null))
      {
        localArrayList2 = configureEnteringExitingViews(paramFragmentManagerImpl, (Fragment)localObject3, localArrayList2, paramView);
        if ((localArrayList2 == null) || (localArrayList2.isEmpty())) {
          paramFragmentManagerImpl = null;
        }
        FragmentTransitionCompat21.addTarget(localObject2, paramView);
        localObject3 = mergeTransitions(localObject2, paramFragmentManagerImpl, localObject1, localFragment, paramFragmentContainerTransition.lastInIsPop);
        if (localObject3 != null)
        {
          paramFragmentContainerTransition = new ArrayList();
          FragmentTransitionCompat21.scheduleRemoveTargets(localObject3, localObject2, paramFragmentContainerTransition, paramFragmentManagerImpl, localArrayList2, localObject1, localArrayList1);
          scheduleTargetChange(localViewGroup, localFragment, paramView, localArrayList1, localObject2, paramFragmentContainerTransition, paramFragmentManagerImpl, localArrayList2);
          FragmentTransitionCompat21.setNameOverridesUnoptimized(localViewGroup, localArrayList1, paramArrayMap);
          FragmentTransitionCompat21.beginDelayedTransition(localViewGroup, localObject3);
          FragmentTransitionCompat21.scheduleNameReset(localViewGroup, localArrayList1, paramArrayMap);
        }
      }
    }
  }
  
  private static FragmentContainerTransition ensureContainer(FragmentContainerTransition paramFragmentContainerTransition, SparseArray<FragmentContainerTransition> paramSparseArray, int paramInt)
  {
    FragmentContainerTransition localFragmentContainerTransition = paramFragmentContainerTransition;
    if (paramFragmentContainerTransition == null)
    {
      localFragmentContainerTransition = new FragmentContainerTransition();
      paramSparseArray.put(paramInt, localFragmentContainerTransition);
    }
    return localFragmentContainerTransition;
  }
  
  private static String findKeyForValue(ArrayMap<String, String> paramArrayMap, String paramString)
  {
    int j = paramArrayMap.size();
    int i = 0;
    if (i < j) {
      if (!paramString.equals(paramArrayMap.valueAt(i))) {}
    }
    for (paramArrayMap = (String)paramArrayMap.keyAt(i);; paramArrayMap = null)
    {
      return paramArrayMap;
      i++;
      break;
    }
  }
  
  private static Object getEnterTransition(Fragment paramFragment, boolean paramBoolean)
  {
    if (paramFragment == null)
    {
      paramFragment = null;
      return paramFragment;
    }
    if (paramBoolean) {}
    for (paramFragment = paramFragment.getReenterTransition();; paramFragment = paramFragment.getEnterTransition())
    {
      paramFragment = FragmentTransitionCompat21.cloneTransition(paramFragment);
      break;
    }
  }
  
  private static Object getExitTransition(Fragment paramFragment, boolean paramBoolean)
  {
    if (paramFragment == null)
    {
      paramFragment = null;
      return paramFragment;
    }
    if (paramBoolean) {}
    for (paramFragment = paramFragment.getReturnTransition();; paramFragment = paramFragment.getExitTransition())
    {
      paramFragment = FragmentTransitionCompat21.cloneTransition(paramFragment);
      break;
    }
  }
  
  private static View getInEpicenterView(ArrayMap<String, View> paramArrayMap, FragmentContainerTransition paramFragmentContainerTransition, Object paramObject, boolean paramBoolean)
  {
    paramFragmentContainerTransition = paramFragmentContainerTransition.lastInTransaction;
    if ((paramObject != null) && (paramArrayMap != null) && (paramFragmentContainerTransition.mSharedElementSourceNames != null) && (!paramFragmentContainerTransition.mSharedElementSourceNames.isEmpty())) {
      if (paramBoolean) {
        paramFragmentContainerTransition = (String)paramFragmentContainerTransition.mSharedElementSourceNames.get(0);
      }
    }
    for (paramArrayMap = (View)paramArrayMap.get(paramFragmentContainerTransition);; paramArrayMap = null)
    {
      return paramArrayMap;
      paramFragmentContainerTransition = (String)paramFragmentContainerTransition.mSharedElementTargetNames.get(0);
      break;
    }
  }
  
  private static Object getSharedElementTransition(Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean)
  {
    if ((paramFragment1 == null) || (paramFragment2 == null))
    {
      paramFragment1 = null;
      return paramFragment1;
    }
    if (paramBoolean) {}
    for (paramFragment1 = paramFragment2.getSharedElementReturnTransition();; paramFragment1 = paramFragment1.getSharedElementEnterTransition())
    {
      paramFragment1 = FragmentTransitionCompat21.wrapTransitionInSet(FragmentTransitionCompat21.cloneTransition(paramFragment1));
      break;
    }
  }
  
  private static Object mergeTransitions(Object paramObject1, Object paramObject2, Object paramObject3, Fragment paramFragment, boolean paramBoolean)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (paramObject1 != null)
    {
      bool1 = bool2;
      if (paramObject2 != null)
      {
        bool1 = bool2;
        if (paramFragment != null)
        {
          if (!paramBoolean) {
            break label52;
          }
          bool1 = paramFragment.getAllowReturnTransitionOverlap();
        }
      }
    }
    if (bool1) {}
    for (paramObject1 = FragmentTransitionCompat21.mergeTransitionsTogether(paramObject2, paramObject1, paramObject3);; paramObject1 = FragmentTransitionCompat21.mergeTransitionsInSequence(paramObject2, paramObject1, paramObject3))
    {
      return paramObject1;
      label52:
      bool1 = paramFragment.getAllowEnterTransitionOverlap();
      break;
    }
  }
  
  private static void replaceHide(Object paramObject, Fragment paramFragment, ArrayList<View> paramArrayList)
  {
    if ((paramFragment != null) && (paramObject != null) && (paramFragment.mAdded) && (paramFragment.mHidden) && (paramFragment.mHiddenChanged))
    {
      paramFragment.setHideReplaced(true);
      FragmentTransitionCompat21.scheduleHideFragmentView(paramObject, paramFragment.getView(), paramArrayList);
      OneShotPreDrawListener.add(paramFragment.mContainer, new Runnable()
      {
        public void run()
        {
          FragmentTransition.setViewVisibility(this.val$exitingViews, 4);
        }
      });
    }
  }
  
  private static void retainValues(ArrayMap<String, String> paramArrayMap, ArrayMap<String, View> paramArrayMap1)
  {
    for (int i = paramArrayMap.size() - 1; i >= 0; i--) {
      if (!paramArrayMap1.containsKey((String)paramArrayMap.valueAt(i))) {
        paramArrayMap.removeAt(i);
      }
    }
  }
  
  private static void scheduleTargetChange(ViewGroup paramViewGroup, final Fragment paramFragment, final View paramView, final ArrayList<View> paramArrayList1, Object paramObject1, final ArrayList<View> paramArrayList2, final Object paramObject2, final ArrayList<View> paramArrayList3)
  {
    OneShotPreDrawListener.add(paramViewGroup, new Runnable()
    {
      public void run()
      {
        ArrayList localArrayList;
        if (this.val$enterTransition != null)
        {
          FragmentTransitionCompat21.removeTarget(this.val$enterTransition, paramView);
          localArrayList = FragmentTransition.configureEnteringExitingViews(this.val$enterTransition, paramFragment, paramArrayList1, paramView);
          paramArrayList2.addAll(localArrayList);
        }
        if (paramArrayList3 != null)
        {
          if (paramObject2 != null)
          {
            localArrayList = new ArrayList();
            localArrayList.add(paramView);
            FragmentTransitionCompat21.replaceTargets(paramObject2, paramArrayList3, localArrayList);
          }
          paramArrayList3.clear();
          paramArrayList3.add(paramView);
        }
      }
    });
  }
  
  private static void setOutEpicenter(Object paramObject1, Object paramObject2, ArrayMap<String, View> paramArrayMap, boolean paramBoolean, BackStackRecord paramBackStackRecord)
  {
    if ((paramBackStackRecord.mSharedElementSourceNames != null) && (!paramBackStackRecord.mSharedElementSourceNames.isEmpty())) {
      if (!paramBoolean) {
        break label62;
      }
    }
    label62:
    for (paramBackStackRecord = (String)paramBackStackRecord.mSharedElementTargetNames.get(0);; paramBackStackRecord = (String)paramBackStackRecord.mSharedElementSourceNames.get(0))
    {
      paramArrayMap = (View)paramArrayMap.get(paramBackStackRecord);
      FragmentTransitionCompat21.setEpicenter(paramObject1, paramArrayMap);
      if (paramObject2 != null) {
        FragmentTransitionCompat21.setEpicenter(paramObject2, paramArrayMap);
      }
      return;
    }
  }
  
  private static void setViewVisibility(ArrayList<View> paramArrayList, int paramInt)
  {
    if (paramArrayList == null) {}
    for (;;)
    {
      return;
      for (int i = paramArrayList.size() - 1; i >= 0; i--) {
        ((View)paramArrayList.get(i)).setVisibility(paramInt);
      }
    }
  }
  
  static void startTransitions(FragmentManagerImpl paramFragmentManagerImpl, ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if ((paramFragmentManagerImpl.mCurState < 1) || (Build.VERSION.SDK_INT < 21)) {}
    SparseArray localSparseArray;
    Object localObject;
    do
    {
      return;
      localSparseArray = new SparseArray();
      i = paramInt1;
      if (i < paramInt2)
      {
        localObject = (BackStackRecord)paramArrayList.get(i);
        if (((Boolean)paramArrayList1.get(i)).booleanValue()) {
          calculatePopFragments((BackStackRecord)localObject, localSparseArray, paramBoolean);
        }
        for (;;)
        {
          i++;
          break;
          calculateFragments((BackStackRecord)localObject, localSparseArray, paramBoolean);
        }
      }
    } while (localSparseArray.size() == 0);
    View localView = new View(paramFragmentManagerImpl.mHost.getContext());
    int j = localSparseArray.size();
    int i = 0;
    label123:
    int k;
    FragmentContainerTransition localFragmentContainerTransition;
    if (i < j)
    {
      k = localSparseArray.keyAt(i);
      localObject = calculateNameOverrides(k, paramArrayList, paramArrayList1, paramInt1, paramInt2);
      localFragmentContainerTransition = (FragmentContainerTransition)localSparseArray.valueAt(i);
      if (!paramBoolean) {
        break label186;
      }
      configureTransitionsOptimized(paramFragmentManagerImpl, k, localFragmentContainerTransition, localView, (ArrayMap)localObject);
    }
    for (;;)
    {
      i++;
      break label123;
      break;
      label186:
      configureTransitionsUnoptimized(paramFragmentManagerImpl, k, localFragmentContainerTransition, localView, (ArrayMap)localObject);
    }
  }
  
  static class FragmentContainerTransition
  {
    public Fragment firstOut;
    public boolean firstOutIsPop;
    public BackStackRecord firstOutTransaction;
    public Fragment lastIn;
    public boolean lastInIsPop;
    public BackStackRecord lastInTransaction;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\app\FragmentTransition.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */