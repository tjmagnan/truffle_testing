package org.joda.time.tz;

import java.util.Locale;

public abstract interface NameProvider
{
  public abstract String getName(Locale paramLocale, String paramString1, String paramString2);
  
  public abstract String getShortName(Locale paramLocale, String paramString1, String paramString2);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\NameProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */