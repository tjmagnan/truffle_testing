package android.support.constraint.solver;

import java.util.ArrayList;

public class Goal
{
  ArrayList<SolverVariable> variables = new ArrayList();
  
  private void initFromSystemErrors(LinearSystem paramLinearSystem)
  {
    this.variables.clear();
    int i = 1;
    if (i < paramLinearSystem.mNumColumns)
    {
      SolverVariable localSolverVariable = paramLinearSystem.mCache.mIndexedVariables[i];
      for (int j = 0; j < 6; j++) {
        localSolverVariable.strengthVector[j] = 0.0F;
      }
      localSolverVariable.strengthVector[localSolverVariable.strength] = 1.0F;
      if (localSolverVariable.mType != SolverVariable.Type.ERROR) {}
      for (;;)
      {
        i++;
        break;
        this.variables.add(localSolverVariable);
      }
    }
  }
  
  SolverVariable getPivotCandidate()
  {
    int n = this.variables.size();
    Object localObject2 = null;
    int j = 0;
    for (int m = 0; m < n; m++)
    {
      SolverVariable localSolverVariable = (SolverVariable)this.variables.get(m);
      for (int i = 5; i >= 0; i--)
      {
        float f = localSolverVariable.strengthVector[i];
        Object localObject1 = localObject2;
        int k = j;
        if (localObject2 == null)
        {
          localObject1 = localObject2;
          k = j;
          if (f < 0.0F)
          {
            localObject1 = localObject2;
            k = j;
            if (i >= j)
            {
              k = i;
              localObject1 = localSolverVariable;
            }
          }
        }
        localObject2 = localObject1;
        j = k;
        if (f > 0.0F)
        {
          localObject2 = localObject1;
          j = k;
          if (i > k)
          {
            j = i;
            localObject2 = null;
          }
        }
      }
    }
    return (SolverVariable)localObject2;
  }
  
  public String toString()
  {
    String str = "Goal: ";
    int j = this.variables.size();
    for (int i = 0; i < j; i++)
    {
      SolverVariable localSolverVariable = (SolverVariable)this.variables.get(i);
      str = str + localSolverVariable.strengthsToString();
    }
    return str;
  }
  
  void updateFromSystem(LinearSystem paramLinearSystem)
  {
    initFromSystemErrors(paramLinearSystem);
    int m = this.variables.size();
    for (int i = 0; i < m; i++)
    {
      SolverVariable localSolverVariable2 = (SolverVariable)this.variables.get(i);
      if (localSolverVariable2.definitionId != -1)
      {
        ArrayLinkedVariables localArrayLinkedVariables = paramLinearSystem.getRow(localSolverVariable2.definitionId).variables;
        int n = localArrayLinkedVariables.currentSize;
        int j = 0;
        if (j < n)
        {
          SolverVariable localSolverVariable1 = localArrayLinkedVariables.getVariable(j);
          if (localSolverVariable1 == null) {}
          for (;;)
          {
            j++;
            break;
            float f = localArrayLinkedVariables.getVariableValue(j);
            for (int k = 0; k < 6; k++)
            {
              float[] arrayOfFloat = localSolverVariable1.strengthVector;
              arrayOfFloat[k] += localSolverVariable2.strengthVector[k] * f;
            }
            if (!this.variables.contains(localSolverVariable1)) {
              this.variables.add(localSolverVariable1);
            }
          }
        }
        localSolverVariable2.clearStrengths();
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\constraint\solver\Goal.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */