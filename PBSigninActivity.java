package tech.dcube.companion.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

public class PBSigninActivity
  extends AppCompatActivity
{
  private static final String TAG = "PBSigninActivity: ";
  private static final String URL_OKTA = "https://login-dev.saase2e.pitneycloud.com/companionapp?flow=v2&TargetResource=https%3A%2F%2Fpitneybowes.oktapreview.com%2Foauth2%2Faus8zl82cdIGhGxGr0h7%2Fv1%2Fauthorize%3Fclient_id%3DwTvlYFbGmfqgIlMdnKxw%26scope%3Doffline_access%26response_type%3Dcode%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fhome%2F%26nonce%3D11111%26state%3D11111%26prompt%3Dnone%26response_mode%3Dfragment";
  private String code;
  private boolean isRedirected = false;
  private MaterialDialog mLoadingDialog;
  private MaterialDialog mSignInLoadingDialog;
  private WebView mWebView;
  
  private void callSignInApi(String paramString)
  {
    try
    {
      this.mSignInLoadingDialog.show();
    }
    catch (Exception localException)
    {
      try
      {
        for (;;)
        {
          ServerManager localServerManager = ServerManager.getInstance();
          ServerManager.RequestCompletionHandler local2 = new tech/dcube/companion/activities/PBSigninActivity$2;
          local2.<init>(this);
          localServerManager.authenticate(this, paramString, local2);
          return;
          localException = localException;
          localException.printStackTrace();
        }
      }
      catch (Exception paramString)
      {
        for (;;)
        {
          paramString.printStackTrace();
        }
      }
    }
  }
  
  private void launchMainActivity()
  {
    Intent localIntent = new Intent(getApplicationContext(), PBMainFragmentActivity.class);
    finish();
    startActivity(localIntent);
  }
  
  private void setupWebView()
  {
    this.mWebView = ((WebView)findViewById(2131624266));
    this.mWebView.setWebViewClient(new MyWebViewClient(null));
    this.mWebView.getSettings().setJavaScriptEnabled(true);
    this.mWebView.clearCache(true);
    if (DataManager.getInstance().getUser() != null) {
      this.mWebView.setVisibility(4);
    }
    for (;;)
    {
      try
      {
        this.mSignInLoadingDialog.show();
        ServerManager.getInstance().refreshAccessToken(this, new ServerManager.RequestCompletionHandler()
        {
          public void onError(VolleyError paramAnonymousVolleyError, String paramAnonymousString)
          {
            PBSigninActivity.this.mWebView.setVisibility(0);
            try
            {
              PBSigninActivity.this.mSignInLoadingDialog.dismiss();
              PBSigninActivity.this.mWebView.loadUrl("https://login-dev.saase2e.pitneycloud.com/companionapp?flow=v2&TargetResource=https%3A%2F%2Fpitneybowes.oktapreview.com%2Foauth2%2Faus8zl82cdIGhGxGr0h7%2Fv1%2Fauthorize%3Fclient_id%3DwTvlYFbGmfqgIlMdnKxw%26scope%3Doffline_access%26response_type%3Dcode%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fhome%2F%26nonce%3D11111%26state%3D11111%26prompt%3Dnone%26response_mode%3Dfragment");
              return;
            }
            catch (Exception paramAnonymousVolleyError)
            {
              for (;;)
              {
                paramAnonymousVolleyError.printStackTrace();
              }
            }
          }
          
          public void onSuccess(PBUser paramAnonymousPBUser, String paramAnonymousString)
          {
            PBSigninActivity.this.mWebView.setVisibility(0);
            try
            {
              PBSigninActivity.this.mSignInLoadingDialog.dismiss();
              PBSigninActivity.this.launchMainActivity();
              return;
            }
            catch (Exception paramAnonymousPBUser)
            {
              for (;;)
              {
                paramAnonymousPBUser.printStackTrace();
              }
            }
          }
        });
        return;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        continue;
      }
      this.mWebView.loadUrl("https://login-dev.saase2e.pitneycloud.com/companionapp?flow=v2&TargetResource=https%3A%2F%2Fpitneybowes.oktapreview.com%2Foauth2%2Faus8zl82cdIGhGxGr0h7%2Fv1%2Fauthorize%3Fclient_id%3DwTvlYFbGmfqgIlMdnKxw%26scope%3Doffline_access%26response_type%3Dcode%26redirect_uri%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fhome%2F%26nonce%3D11111%26state%3D11111%26prompt%3Dnone%26response_mode%3Dfragment");
    }
  }
  
  public void buildLoadingDialog()
  {
    this.mLoadingDialog = new MaterialDialog.Builder(this).customView(2130968704, true).build();
    this.mLoadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    ProgressBar localProgressBar = (ProgressBar)this.mLoadingDialog.getCustomView().findViewById(2131624063);
    if (Build.VERSION.SDK_INT >= 21) {
      localProgressBar.setIndeterminateTintList(ColorStateList.valueOf(getResources().getColor(2131492948)));
    }
    for (;;)
    {
      this.mLoadingDialog.setCancelable(false);
      this.mLoadingDialog.setCanceledOnTouchOutside(false);
      return;
      localProgressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(2131492948), PorterDuff.Mode.SRC_IN);
    }
  }
  
  public void buildSignInLoadingDialog()
  {
    this.mSignInLoadingDialog = new MaterialDialog.Builder(this).customView(2130968704, true).build();
    this.mSignInLoadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    ProgressBar localProgressBar = (ProgressBar)this.mLoadingDialog.getCustomView().findViewById(2131624063);
    if (Build.VERSION.SDK_INT >= 21) {
      localProgressBar.setIndeterminateTintList(ColorStateList.valueOf(getResources().getColor(2131492948)));
    }
    for (;;)
    {
      this.mSignInLoadingDialog.setCancelable(false);
      this.mSignInLoadingDialog.setCanceledOnTouchOutside(false);
      return;
      localProgressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(2131492948), PorterDuff.Mode.SRC_IN);
    }
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getApplicationContext(), paramString);
  }
  
  protected void onCreate(@Nullable Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968659);
    DataManager.getInstance().setContext(getApplicationContext());
    buildLoadingDialog();
    buildSignInLoadingDialog();
    setupWebView();
  }
  
  private class MyWebViewClient
    extends WebViewClient
  {
    private MyWebViewClient() {}
    
    private boolean shouldOverrideUrlLoading(String paramString)
    {
      if (paramString.indexOf("http://localhost:8080") == 0)
      {
        Object localObject = paramString.split("\\?");
        if (localObject.length > 0)
        {
          localObject = localObject[0].split("#");
          if (localObject.length > 1)
          {
            String[] arrayOfString = localObject[1].split("\\&");
            int j = arrayOfString.length;
            int i = 0;
            if (i < j)
            {
              localObject = arrayOfString[i];
              PBSigninActivity localPBSigninActivity;
              if (((String)localObject).indexOf("code") == 0)
              {
                localPBSigninActivity = PBSigninActivity.this;
                if (((String)localObject).split("=").length <= 0) {
                  break label171;
                }
              }
              label171:
              for (localObject = localObject.split("=")[1];; localObject = null)
              {
                PBSigninActivity.access$502(localPBSigninActivity, (String)localObject);
                if ((PBSigninActivity.this.code != null) && (!PBSigninActivity.this.code.trim().equals("")))
                {
                  Log.wtf("Code is ", PBSigninActivity.this.code);
                  PBSigninActivity.access$602(PBSigninActivity.this, true);
                }
                i++;
                break;
              }
            }
          }
        }
      }
      if (PBSigninActivity.this.isRedirected)
      {
        PBSigninActivity.this.callSignInApi(PBSigninActivity.this.code);
        PBSigninActivity.access$602(PBSigninActivity.this, false);
      }
      for (;;)
      {
        return true;
        PBSigninActivity.this.mWebView.loadUrl(paramString);
      }
    }
    
    public void onPageFinished(WebView paramWebView, String paramString)
    {
      try
      {
        PBSigninActivity.this.mLoadingDialog.dismiss();
        super.onPageFinished(paramWebView, paramString);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          localException.printStackTrace();
        }
      }
    }
    
    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      try
      {
        PBSigninActivity.this.mLoadingDialog.show();
        super.onPageStarted(paramWebView, paramString, paramBitmap);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          localException.printStackTrace();
        }
      }
    }
    
    @TargetApi(21)
    public boolean shouldOverrideUrlLoading(WebView paramWebView, WebResourceRequest paramWebResourceRequest)
    {
      return shouldOverrideUrlLoading(paramWebResourceRequest.getUrl().toString());
    }
    
    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      return shouldOverrideUrlLoading(paramString);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\activities\PBSigninActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */