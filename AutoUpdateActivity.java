package com.testfairy.activities;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.StrictMode.VmPolicy;
import android.os.StrictMode.VmPolicy.Builder;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.testfairy.e;
import com.testfairy.k.a;
import com.testfairy.k.a.a;
import com.testfairy.p.c;
import com.testfairy.p.i;
import java.io.File;
import java.lang.reflect.Method;

public class AutoUpdateActivity
  extends Activity
  implements a.a
{
  private static final int a = 1000;
  private static a l;
  private File b;
  private ProgressDialog c;
  private a d;
  private final String e = "application/vnd.android.package-archive";
  private final int f = 1;
  private boolean g = false;
  private boolean h = false;
  private String i = null;
  private String j = null;
  private String k = null;
  private boolean m = true;
  private DialogInterface.OnKeyListener n = new DialogInterface.OnKeyListener()
  {
    public boolean onKey(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      boolean bool2 = false;
      boolean bool1 = bool2;
      if (paramAnonymousInt == 4)
      {
        bool1 = bool2;
        if (paramAnonymousKeyEvent.getAction() == 0)
        {
          Log.v(e.a, "User pressed on back button");
          c.a(paramAnonymousDialogInterface);
          AutoUpdateActivity.a(AutoUpdateActivity.this, false);
          bool1 = true;
        }
      }
      return bool1;
    }
  };
  
  private File a(Context paramContext)
  {
    Object localObject2;
    if (Build.VERSION.SDK_INT < 8)
    {
      localObject2 = new File(Environment.getExternalStorageDirectory().getPath() + "/Android/data/" + paramContext.getPackageName() + "/cache/");
      localObject1 = localObject2;
      if (!((File)localObject2).exists())
      {
        localObject1 = localObject2;
        if (((File)localObject2).mkdirs()) {}
      }
    }
    for (Object localObject1 = null;; localObject1 = paramContext.getExternalCacheDir())
    {
      localObject2 = localObject1;
      if (localObject1 == null) {
        localObject2 = paramContext.getCacheDir();
      }
      return (File)localObject2;
    }
  }
  
  public static void a(a parama)
  {
    l = parama;
  }
  
  private void a(boolean paramBoolean)
  {
    if (l == null)
    {
      Log.e(e.a, "AutoUpdateCallback must be set before starting AutoUpdateActivity");
      return;
    }
    if (paramBoolean) {
      l.b();
    }
    for (;;)
    {
      finish();
      break;
      l.a();
    }
  }
  
  private void d()
  {
    try
    {
      Activity.class.getDeclaredMethod("requestPermissions", new Class[] { String[].class, Integer.TYPE }).invoke(this, new Object[] { { "android.permission.WRITE_EXTERNAL_STORAGE" }, Integer.valueOf(1000) });
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  private void e()
  {
    if (this.m)
    {
      this.m = false;
      d();
    }
    for (;;)
    {
      return;
      DialogInterface.OnClickListener local2 = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          c.a(paramAnonymousDialogInterface);
          AutoUpdateActivity.a(AutoUpdateActivity.this);
        }
      };
      Object localObject = new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          AutoUpdateActivity.a(AutoUpdateActivity.this, true);
        }
      };
      localObject = c.a(this).setTitle("New version is available").setMessage("In order to download and save the update, we need to access your device storage").setCancelable(false).setIcon(17301599).setPositiveButton("Get the new version", local2).setNegativeButton("don't update", (DialogInterface.OnClickListener)localObject);
      if (!isFinishing()) {
        try
        {
          ((AlertDialog.Builder)localObject).show();
        }
        catch (Exception localException) {}
      }
    }
  }
  
  public void a() {}
  
  public void a(long paramLong1, final long paramLong2)
  {
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        try
        {
          AutoUpdateActivity.c(AutoUpdateActivity.this).setIndeterminate(false);
          AutoUpdateActivity.c(AutoUpdateActivity.this).setMax((int)(paramLong2 >> 10));
          AutoUpdateActivity.c(AutoUpdateActivity.this).setProgress((int)(this.b >> 10));
          return;
        }
        catch (Exception localException)
        {
          for (;;) {}
        }
      }
    });
  }
  
  public void b()
  {
    if ((this.c != null) && (this.c.isShowing()))
    {
      this.c.dismiss();
      this.c = null;
    }
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        AutoUpdateActivity.b(AutoUpdateActivity.this);
      }
    });
  }
  
  public void c()
  {
    Log.d(e.a, "onDownloadCompleted: ");
    runOnUiThread(new Runnable()
    {
      public void run()
      {
        try
        {
          AutoUpdateActivity.c(AutoUpdateActivity.this).setMessage("Running installer");
          AutoUpdateActivity.c(AutoUpdateActivity.this).setIndeterminate(true);
          AutoUpdateActivity.c(AutoUpdateActivity.this).dismiss();
          return;
        }
        catch (Exception localException)
        {
          for (;;) {}
        }
      }
    });
    StrictMode.VmPolicy localVmPolicy = StrictMode.getVmPolicy();
    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setDataAndType(Uri.fromFile(this.b), "application/vnd.android.package-archive");
    localIntent.setFlags(67108864);
    localIntent.setFlags(32768);
    startActivityForResult(localIntent, 1);
    StrictMode.setVmPolicy(localVmPolicy);
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt1 == 1) {
      this.g = true;
    }
    if (this.h) {
      c.a(this).setTitle("Update required").setMessage("Update is mandatory. In order to continue using the app, please install the new version.").setPositiveButton("Install", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface = new Intent("android.intent.action.VIEW");
          paramAnonymousDialogInterface.setDataAndType(Uri.fromFile(AutoUpdateActivity.d(AutoUpdateActivity.this)), "application/vnd.android.package-archive");
          paramAnonymousDialogInterface.setFlags(67108864);
          paramAnonymousDialogInterface.setFlags(32768);
          AutoUpdateActivity.this.startActivityForResult(paramAnonymousDialogInterface, 1);
        }
      }).setNegativeButton("Exit", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface = new Intent("android.intent.action.MAIN");
          paramAnonymousDialogInterface.addCategory("android.intent.category.HOME");
          paramAnonymousDialogInterface.setFlags(67108864);
          AutoUpdateActivity.this.startActivity(paramAnonymousDialogInterface);
          System.exit(0);
        }
      }).setIcon(17301599).setOnKeyListener(new DialogInterface.OnKeyListener()
      {
        public boolean onKey(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
        {
          if (paramAnonymousInt == 4) {}
          for (boolean bool = true;; bool = false) {
            return bool;
          }
        }
      }).setOnCancelListener(null).show();
    }
    for (;;)
    {
      return;
      a(false);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.g = false;
    if ((getIntent() == null) || (getIntent().getExtras() == null)) {
      finish();
    }
    paramBundle = getIntent().getExtras();
    this.i = paramBundle.getString("appName");
    this.j = paramBundle.getString("newVersion");
    this.k = paramBundle.getString("upgradeUrl");
    this.h = paramBundle.getBoolean("isUpgradeMandatory");
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    try
    {
      if ((this.c != null) && (this.c.isShowing()))
      {
        Log.v(e.a, "Dismissing previous dialog, because visible");
        this.c.dismiss();
        this.c = null;
      }
      try
      {
        if (this.d != null) {
          this.d.a();
        }
        return;
      }
      catch (Exception localException1)
      {
        for (;;) {}
      }
    }
    catch (Exception localException2)
    {
      for (;;) {}
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    if (this.c != null) {
      Log.d(e.a, "Second call to onResume");
    }
    for (;;)
    {
      return;
      if (!this.g) {
        if (!i.a(this))
        {
          e();
        }
        else
        {
          setRequestedOrientation(1);
          LinearLayout localLinearLayout = new LinearLayout(this);
          localLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
          localLinearLayout.setBackgroundColor(-3355444);
          setContentView(localLinearLayout);
          this.c = new ProgressDialog(this);
          this.c.setCancelable(false);
          this.c.setMax(0);
          this.c.setTitle("Please Wait");
          this.c.setMessage("Downloading " + this.i + " " + this.j);
          this.c.setProgressStyle(1);
          this.c.setIndeterminate(true);
          this.c.setOnKeyListener(this.n);
          if (Build.VERSION.SDK_INT >= 14) {
            this.c.setProgressNumberFormat("%1d KB/%2d KB");
          }
          this.c.show();
          this.b = new File(a(this), "/testfairy-upgrade-1.apk");
          this.d = new a(this.k, this.b);
          this.d.a(this);
          this.d.b();
        }
      }
    }
  }
  
  public static abstract interface a
  {
    public abstract void a();
    
    public abstract void b();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\AutoUpdateActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */