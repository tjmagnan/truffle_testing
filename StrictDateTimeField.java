package org.joda.time.field;

import org.joda.time.DateTimeField;

public class StrictDateTimeField
  extends DelegatedDateTimeField
{
  private static final long serialVersionUID = 3154803964207950910L;
  
  protected StrictDateTimeField(DateTimeField paramDateTimeField)
  {
    super(paramDateTimeField);
  }
  
  public static DateTimeField getInstance(DateTimeField paramDateTimeField)
  {
    if (paramDateTimeField == null) {
      paramDateTimeField = null;
    }
    for (;;)
    {
      return paramDateTimeField;
      DateTimeField localDateTimeField = paramDateTimeField;
      if ((paramDateTimeField instanceof LenientDateTimeField)) {
        localDateTimeField = ((LenientDateTimeField)paramDateTimeField).getWrappedField();
      }
      paramDateTimeField = localDateTimeField;
      if (localDateTimeField.isLenient()) {
        paramDateTimeField = new StrictDateTimeField(localDateTimeField);
      }
    }
  }
  
  public final boolean isLenient()
  {
    return false;
  }
  
  public long set(long paramLong, int paramInt)
  {
    FieldUtils.verifyValueBounds(this, paramInt, getMinimumValue(paramLong), getMaximumValue(paramLong));
    return super.set(paramLong, paramInt);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\field\StrictDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */