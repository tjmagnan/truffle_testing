package com.testfairy;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.hardware.SensorManager;
import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;
import com.testfairy.a.a;
import com.testfairy.activities.ProvideFeedbackActivity;
import com.testfairy.k.d;
import com.testfairy.p.c;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class h
{
  private static final String a = "There is no internet connection.";
  private static final String b = "Could not connect. Please check your internet connection and restart the app.";
  private static final int k = 3;
  private final SensorManager c;
  private FeedbackOptions d;
  private a e;
  private m f;
  private AlertDialog g;
  private Integer h;
  private float i;
  private com.testfairy.a.b j;
  private int l = 0;
  private final Object m = new Object();
  private m.a n = new m.a()
  {
    public void a()
    {
      h.a(h.this);
    }
  };
  private DialogInterface.OnClickListener o = new DialogInterface.OnClickListener()
  {
    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
    {
      c.a(h.b(h.this));
      h.a(h.this, null);
      b.a().b(h.c(h.this));
      h.a(h.this, null);
    }
  };
  private DialogInterface.OnCancelListener p = new DialogInterface.OnCancelListener()
  {
    public void onCancel(DialogInterface paramAnonymousDialogInterface)
    {
      h.a(h.this, null);
      b.a().b(h.c(h.this));
      h.a(h.this, null);
    }
  };
  
  public h(com.testfairy.a.b paramb, a parama, Context paramContext, SensorManager paramSensorManager)
  {
    this.j = paramb;
    this.c = paramSensorManager;
    if (!com.testfairy.p.h.a(paramContext, ProvideFeedbackActivity.class)) {
      Log.w(e.a, "ProvideFeedbackActivity is not listed in your Manifest, Feedback will not be available. Please add <activity android:name=\"com.testfairy.activities.ProvideFeedbackActivity\"/>");
    }
    for (;;)
    {
      return;
      this.f = new m();
      this.f.a(this.n);
      this.e = parama;
    }
  }
  
  private Uri.Builder a(Uri.Builder paramBuilder, String paramString1, String paramString2)
  {
    if (paramString2 != null) {
      paramBuilder.appendQueryParameter(paramString1, paramString2);
    }
    return paramBuilder;
  }
  
  private Uri a(Context paramContext, String paramString, float paramFloat)
  {
    try
    {
      String str = URLEncoder.encode(this.e.c().e(), "UTF-8");
      paramString = Uri.parse(paramString).buildUpon();
      a(paramString, "sessionUrl", str);
      a(paramString, "timestamp", String.valueOf(paramFloat));
      a(paramString, "user", this.e.i());
      a(paramString, "platform", "android");
      a(paramString, "packageName", com.testfairy.p.h.e(paramContext));
      a(paramString, "versionName", com.testfairy.p.h.c(paramContext));
      a(paramString, "versionCode", String.valueOf(com.testfairy.p.h.b(paramContext)));
      a(paramString, "screenName", this.e.j());
      return paramString.build();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;)
      {
        Object localObject = null;
      }
    }
  }
  
  private void a(int paramInt, float paramFloat)
  {
    Activity localActivity = this.j.a();
    if (localActivity == null)
    {
      Log.w(e.a, "openProvideFeedback failed, can't find Activity");
      return;
    }
    Log.d(e.a, "openPF " + paramInt);
    Intent localIntent;
    if ((this.d != null) && (this.d.d() != null)) {
      localIntent = new Intent("android.intent.action.VIEW", a(localActivity, this.d.d(), paramFloat));
    }
    for (;;)
    {
      localActivity.startActivity(localIntent);
      this.h = null;
      break;
      localIntent = new Intent(localActivity, ProvideFeedbackActivity.class);
      localIntent.putExtra(n.M, paramInt);
    }
  }
  
  private void f()
  {
    for (;;)
    {
      synchronized (this.m)
      {
        if (this.l >= 3) {
          return;
        }
        localObject4 = this.j.a();
        if (localObject4 == null) {
          Log.w(e.a, "onShake failed, can't find Activity");
        }
      }
      if ((this.g != null) || (ProvideFeedbackActivity.f)) {}
      this.l += 1;
      Object localObject6;
      if ((!this.e.f()) || (!com.testfairy.k.b.d()))
      {
        if (this.e.f()) {}
        for (localObject2 = "There is no internet connection.";; localObject2 = "Could not connect. Please check your internet connection and restart the app.")
        {
          localObject6 = e.a;
          localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>();
          Log.d((String)localObject6, n.O + ": " + (String)localObject2);
          localObject4 = c.a((Context)localObject4);
          ((AlertDialog.Builder)localObject4).setIcon(17301659);
          ((AlertDialog.Builder)localObject4).setTitle(n.E);
          ((AlertDialog.Builder)localObject4).setMessage((CharSequence)localObject2);
          ((AlertDialog.Builder)localObject4).setCancelable(false);
          localObject2 = new com/testfairy/h$2;
          ((2)localObject2).<init>(this);
          ((AlertDialog.Builder)localObject4).setPositiveButton("OK", (DialogInterface.OnClickListener)localObject2);
          this.g = ((AlertDialog.Builder)localObject4).create();
          this.g.show();
          break;
        }
      }
      if (this.h == null)
      {
        this.i = ((float)(System.currentTimeMillis() - this.e.g()) / 1000.0F);
        localObject6 = new java/util/HashMap;
        ((HashMap)localObject6).<init>(7);
        ((HashMap)localObject6).put(n.I, this.e.c().a());
        ((HashMap)localObject6).put(n.G, Float.valueOf(this.i));
        ((HashMap)localObject6).put(n.H, this.d);
        localObject5 = n.N;
        localObject2 = new com/testfairy/k/d;
        ((d)localObject2).<init>(this.e.k());
        ((HashMap)localObject6).put(localObject5, localObject2);
        ((HashMap)localObject6).put(n.K, this.e.j());
        ((HashMap)localObject6).put(n.L, this.e.c().g());
        this.h = Integer.valueOf(b.a().a((Map)localObject6));
      }
      Object localObject5 = e.a;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Log.d((String)localObject5, n.O + ", bundleId: " + this.h);
      Object localObject4 = c.a((Context)localObject4);
      ((AlertDialog.Builder)localObject4).setTitle(n.E);
      ((AlertDialog.Builder)localObject4).setMessage(n.F);
      ((AlertDialog.Builder)localObject4).setCancelable(false);
      ((AlertDialog.Builder)localObject4).setIcon(17301659);
      ((AlertDialog.Builder)localObject4).setOnCancelListener(this.p);
      localObject2 = new com/testfairy/h$a;
      ((a)localObject2).<init>(this, this.h.intValue(), this.i);
      ((AlertDialog.Builder)localObject4).setPositiveButton("Yes", (DialogInterface.OnClickListener)localObject2);
      ((AlertDialog.Builder)localObject4).setNegativeButton("No", this.o);
      this.g = ((AlertDialog.Builder)localObject4).create();
      this.g.show();
    }
  }
  
  public void a()
  {
    this.n.a();
  }
  
  public void a(FeedbackOptions paramFeedbackOptions)
  {
    this.d = paramFeedbackOptions;
  }
  
  public void b()
  {
    if (this.g != null)
    {
      this.l -= 1;
      c.a(this.g);
    }
  }
  
  public void c()
  {
    if (this.g != null)
    {
      this.g = null;
      f();
    }
  }
  
  public void d()
  {
    this.c.registerListener(this.f, this.c.getDefaultSensor(1), 3);
  }
  
  public void e()
  {
    this.c.unregisterListener(this.f, this.c.getDefaultSensor(1));
  }
  
  private class a
    implements DialogInterface.OnClickListener
  {
    private final int b;
    private final float c;
    
    public a(int paramInt, float paramFloat)
    {
      this.b = paramInt;
      this.c = paramFloat;
    }
    
    public void onClick(DialogInterface paramDialogInterface, int paramInt)
    {
      c.a(h.b(h.this));
      h.a(h.this, null);
      h.a(h.this, this.b, this.c);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */