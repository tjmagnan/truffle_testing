package tech.dcube.companion.listeners;

import tech.dcube.companion.model.PBDevice;

public abstract interface MeterInteractionListener
{
  public abstract void onMeterAddFunButtonTapped(PBDevice paramPBDevice);
  
  public abstract void onMeterErrorClicked(String paramString1, String paramString2);
  
  public abstract void onMeterWarningClicked(String paramString1, String paramString2);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\listeners\MeterInteractionListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */