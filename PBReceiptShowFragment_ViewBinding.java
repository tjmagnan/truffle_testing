package tech.dcube.companion.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PBReceiptShowFragment_ViewBinding
  extends PBTopBarFragment_ViewBinding
{
  private PBReceiptShowFragment target;
  private View view2131624197;
  private View view2131624198;
  
  @UiThread
  public PBReceiptShowFragment_ViewBinding(final PBReceiptShowFragment paramPBReceiptShowFragment, View paramView)
  {
    super(paramPBReceiptShowFragment, paramView);
    this.target = paramPBReceiptShowFragment;
    paramPBReceiptShowFragment.webView = ((WebView)Utils.findRequiredViewAsType(paramView, 2131624196, "field 'webView'", WebView.class));
    View localView = Utils.findRequiredView(paramView, 2131624198, "method 'share'");
    this.view2131624198 = localView;
    localView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBReceiptShowFragment.share();
      }
    });
    paramView = Utils.findRequiredView(paramView, 2131624197, "method 'print'");
    this.view2131624197 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBReceiptShowFragment.print();
      }
    });
  }
  
  public void unbind()
  {
    PBReceiptShowFragment localPBReceiptShowFragment = this.target;
    if (localPBReceiptShowFragment == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    localPBReceiptShowFragment.webView = null;
    this.view2131624198.setOnClickListener(null);
    this.view2131624198 = null;
    this.view2131624197.setOnClickListener(null);
    this.view2131624197 = null;
    super.unbind();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBReceiptShowFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */