package me.zhanghai.android.materialprogressbar;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class CircularProgressDrawable
  extends BaseProgressLayerDrawable<SingleCircularProgressDrawable, CircularProgressBackgroundDrawable>
{
  public CircularProgressDrawable(int paramInt, Context paramContext)
  {
    super(new Drawable[] { new CircularProgressBackgroundDrawable(), new SingleCircularProgressDrawable(paramInt), new SingleCircularProgressDrawable(paramInt) }, paramContext);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\CircularProgressDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */