package com.testfairy;

public class FeedbackContent
{
  private String a;
  private String b;
  private float c;
  
  public FeedbackContent(String paramString1, String paramString2, float paramFloat)
  {
    this.b = paramString1;
    this.a = paramString2;
    this.c = paramFloat;
  }
  
  public String getEmail()
  {
    return this.a;
  }
  
  public String getText()
  {
    return this.b;
  }
  
  public float getTimestamp()
  {
    return this.c;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\FeedbackContent.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */