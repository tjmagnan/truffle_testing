package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBDeviceNotification
  extends PBObject
  implements Serializable
{
  public static final String LEVEL_ERROR = "ERROR";
  public static final String LEVEL_WARNING = "WARNING";
  private static final long serialVersionUID = 3930347138905609140L;
  @Expose
  @SerializedName("deviceErrorCode")
  private String deviceErrorCode;
  @Expose
  @SerializedName("deviceStatusCode")
  private String deviceStatusCode;
  @Expose
  @SerializedName("deviceWarningCode")
  private String deviceWarningCode;
  @Expose
  @SerializedName("fundLevel")
  private String fundLevel;
  @Expose
  @SerializedName("modelNumber")
  private String modelNumber;
  @Expose
  @SerializedName("notificationId")
  private String notificationId;
  @Expose
  @SerializedName("notificationLevel")
  private String notificationLevel;
  @Expose
  @SerializedName("notificationType")
  private String notificationType;
  @Expose
  @SerializedName("serialNumber")
  private String serialNumber;
  @Expose
  @SerializedName("supplyLevel")
  private String supplyLevel;
  @Expose
  @SerializedName("supplyType")
  private String supplyType;
  @Expose
  @SerializedName("timestamp")
  private String timestamp;
  
  public PBDeviceNotification() {}
  
  public PBDeviceNotification(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    this.modelNumber = paramString1;
    this.serialNumber = paramString2;
    this.notificationType = paramString3;
    this.notificationLevel = paramString4;
    this.notificationId = paramString5;
    this.timestamp = paramString6;
    this.supplyType = paramString7;
    this.supplyLevel = paramString8;
    this.deviceErrorCode = paramString9;
    this.deviceWarningCode = paramString10;
    this.deviceStatusCode = paramString11;
  }
  
  public String getDeviceErrorCode()
  {
    return this.deviceErrorCode;
  }
  
  public String getDeviceStatusCode()
  {
    return this.deviceStatusCode;
  }
  
  public String getDeviceWarningCode()
  {
    return this.deviceWarningCode;
  }
  
  public String getFundLevel()
  {
    return this.fundLevel;
  }
  
  public String getModelNumber()
  {
    return this.modelNumber;
  }
  
  public String getNotificationId()
  {
    return this.notificationId;
  }
  
  public String getNotificationLevel()
  {
    return this.notificationLevel;
  }
  
  public String getNotificationType()
  {
    return this.notificationType;
  }
  
  public String getSerialNumber()
  {
    return this.serialNumber;
  }
  
  public String getSupplyLevel()
  {
    return this.supplyLevel;
  }
  
  public String getSupplyType()
  {
    return this.supplyType;
  }
  
  public String getTimestamp()
  {
    return this.timestamp;
  }
  
  public void setDeviceErrorCode(String paramString)
  {
    this.deviceErrorCode = paramString;
  }
  
  public void setDeviceStatusCode(String paramString)
  {
    this.deviceStatusCode = paramString;
  }
  
  public void setDeviceWarningCode(String paramString)
  {
    this.deviceWarningCode = paramString;
  }
  
  public void setFundLevel(String paramString)
  {
    this.fundLevel = paramString;
  }
  
  public void setModelNumber(String paramString)
  {
    this.modelNumber = paramString;
  }
  
  public void setNotificationId(String paramString)
  {
    this.notificationId = paramString;
  }
  
  public void setNotificationLevel(String paramString)
  {
    this.notificationLevel = paramString;
  }
  
  public void setNotificationType(String paramString)
  {
    this.notificationType = paramString;
  }
  
  public void setSerialNumber(String paramString)
  {
    this.serialNumber = paramString;
  }
  
  public void setSupplyLevel(String paramString)
  {
    this.supplyLevel = paramString;
  }
  
  public void setSupplyType(String paramString)
  {
    this.supplyType = paramString;
  }
  
  public void setTimestamp(String paramString)
  {
    this.timestamp = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBDeviceNotification.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */