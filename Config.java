package net.sourceforge.zbar;

public class Config
{
  public static final int ADD_CHECK = 1;
  public static final int ASCII = 3;
  public static final int EMIT_CHECK = 2;
  public static final int ENABLE = 0;
  public static final int MAX_LEN = 33;
  public static final int MIN_LEN = 32;
  public static final int POSITION = 128;
  public static final int UNCERTAINTY = 64;
  public static final int X_DENSITY = 256;
  public static final int Y_DENSITY = 257;
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\net\sourceforge\zbar\Config.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */