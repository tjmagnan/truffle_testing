package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

public class PBShipmentService
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -5166124850387451590L;
  @Expose
  @SerializedName("additionalRequiredFields")
  private Object additionalRequiredFields;
  @Expose
  @SerializedName("allowHidePostge")
  private Boolean allowHidePostge;
  @Expose
  @SerializedName("carrier")
  private String carrier;
  @Expose
  @SerializedName("correlationId")
  private String correlationId;
  @Expose
  @SerializedName("deliveryExpectation")
  private PBDeliveryExpectations deliveryExpectation;
  @Expose
  @SerializedName("effectiveDate")
  private Date effectiveDate;
  @Expose
  @SerializedName("packageDimWeightOunces")
  private Object packageDimWeightOunces;
  @Expose
  @SerializedName("rateType")
  private String rateType;
  @Expose
  @SerializedName("requiredTrackable")
  private Boolean requiredTrackable;
  @Expose
  @SerializedName("serviceId")
  private String serviceId;
  @Expose
  @SerializedName("serviceName")
  private String serviceName;
  @Expose
  @SerializedName("serviceOption")
  private String serviceOption;
  @Expose
  @SerializedName("totalPackageCharge")
  private Double totalPackageCharge;
  
  public PBShipmentService() {}
  
  public PBShipmentService(Object paramObject1, Boolean paramBoolean1, String paramString1, String paramString2, PBDeliveryExpectations paramPBDeliveryExpectations, Date paramDate, Object paramObject2, String paramString3, Boolean paramBoolean2, String paramString4, String paramString5, String paramString6, Double paramDouble)
  {
    this.additionalRequiredFields = paramObject1;
    this.allowHidePostge = paramBoolean1;
    this.carrier = paramString1;
    this.correlationId = paramString2;
    this.deliveryExpectation = paramPBDeliveryExpectations;
    this.effectiveDate = paramDate;
    this.packageDimWeightOunces = paramObject2;
    this.rateType = paramString3;
    this.requiredTrackable = paramBoolean2;
    this.serviceId = paramString4;
    this.serviceName = paramString5;
    this.serviceOption = paramString6;
    this.totalPackageCharge = paramDouble;
  }
  
  public Object getAdditionalRequiredFields()
  {
    return this.additionalRequiredFields;
  }
  
  public Boolean getAllowHidePostge()
  {
    return this.allowHidePostge;
  }
  
  public String getCarrier()
  {
    return this.carrier;
  }
  
  public String getCorrelationId()
  {
    return this.correlationId;
  }
  
  public PBDeliveryExpectations getDeliveryExpectation()
  {
    return this.deliveryExpectation;
  }
  
  public Date getEffectiveDate()
  {
    return this.effectiveDate;
  }
  
  public Object getPackageDimWeightOunces()
  {
    return this.packageDimWeightOunces;
  }
  
  public String getRateType()
  {
    return this.rateType;
  }
  
  public Boolean getRequiredTrackable()
  {
    return this.requiredTrackable;
  }
  
  public String getServiceId()
  {
    return this.serviceId;
  }
  
  public String getServiceName()
  {
    return this.serviceName;
  }
  
  public String getServiceOption()
  {
    return this.serviceOption;
  }
  
  public Double getTotalPackageCharge()
  {
    return this.totalPackageCharge;
  }
  
  public void setAdditionalRequiredFields(Object paramObject)
  {
    this.additionalRequiredFields = paramObject;
  }
  
  public void setAllowHidePostge(Boolean paramBoolean)
  {
    this.allowHidePostge = paramBoolean;
  }
  
  public void setCarrier(String paramString)
  {
    this.carrier = paramString;
  }
  
  public void setCorrelationId(String paramString)
  {
    this.correlationId = paramString;
  }
  
  public void setDeliveryExpectation(PBDeliveryExpectations paramPBDeliveryExpectations)
  {
    this.deliveryExpectation = paramPBDeliveryExpectations;
  }
  
  public void setEffectiveDate(Date paramDate)
  {
    this.effectiveDate = paramDate;
  }
  
  public void setPackageDimWeightOunces(Object paramObject)
  {
    this.packageDimWeightOunces = paramObject;
  }
  
  public void setRateType(String paramString)
  {
    this.rateType = paramString;
  }
  
  public void setRequiredTrackable(Boolean paramBoolean)
  {
    this.requiredTrackable = paramBoolean;
  }
  
  public void setServiceId(String paramString)
  {
    this.serviceId = paramString;
  }
  
  public void setServiceName(String paramString)
  {
    this.serviceName = paramString;
  }
  
  public void setServiceOption(String paramString)
  {
    this.serviceOption = paramString;
  }
  
  public void setTotalPackageCharge(Double paramDouble)
  {
    this.totalPackageCharge = paramDouble;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */