package com.android.volley.toolbox;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.http.AndroidHttpClient;
import android.os.Build.VERSION;
import com.android.volley.RequestQueue;
import java.io.File;

public class Volley
{
  private static final String DEFAULT_CACHE_DIR = "volley";
  
  public static RequestQueue newRequestQueue(Context paramContext)
  {
    return newRequestQueue(paramContext, null);
  }
  
  public static RequestQueue newRequestQueue(Context paramContext, HttpStack paramHttpStack)
  {
    File localFile = new File(paramContext.getCacheDir(), "volley");
    Object localObject1 = "volley/0";
    try
    {
      Object localObject2 = paramContext.getPackageName();
      PackageInfo localPackageInfo = paramContext.getPackageManager().getPackageInfo((String)localObject2, 0);
      paramContext = String.valueOf(String.valueOf(localObject2));
      int i = localPackageInfo.versionCode;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>(paramContext.length() + 12);
      paramContext = paramContext + "/" + i;
      localObject1 = paramContext;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      label144:
      for (;;) {}
    }
    paramContext = paramHttpStack;
    if (paramHttpStack == null) {
      if (Build.VERSION.SDK_INT < 9) {
        break label144;
      }
    }
    for (paramContext = new HurlStack();; paramContext = new HttpClientStack(AndroidHttpClient.newInstance((String)localObject1)))
    {
      paramContext = new BasicNetwork(paramContext);
      paramContext = new RequestQueue(new DiskBasedCache(localFile), paramContext);
      paramContext.start();
      return paramContext;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\toolbox\Volley.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */