package org.joda.time.field;

import java.io.Serializable;
import java.util.Locale;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DurationField;
import org.joda.time.Interval;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;

public abstract class AbstractReadableInstantFieldProperty
  implements Serializable
{
  private static final long serialVersionUID = 1971226328211649661L;
  
  public int compareTo(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {
      throw new IllegalArgumentException("The instant must not be null");
    }
    int j = get();
    int i = paramReadableInstant.get(getFieldType());
    if (j < i) {
      i = -1;
    }
    for (;;)
    {
      return i;
      if (j > i) {
        i = 1;
      } else {
        i = 0;
      }
    }
  }
  
  public int compareTo(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {
      throw new IllegalArgumentException("The partial must not be null");
    }
    int i = get();
    int j = paramReadablePartial.get(getFieldType());
    if (i < j) {
      i = -1;
    }
    for (;;)
    {
      return i;
      if (i > j) {
        i = 1;
      } else {
        i = 0;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (!(paramObject instanceof AbstractReadableInstantFieldProperty))
      {
        bool = false;
      }
      else
      {
        paramObject = (AbstractReadableInstantFieldProperty)paramObject;
        if ((get() != ((AbstractReadableInstantFieldProperty)paramObject).get()) || (!getFieldType().equals(((AbstractReadableInstantFieldProperty)paramObject).getFieldType())) || (!FieldUtils.equals(getChronology(), ((AbstractReadableInstantFieldProperty)paramObject).getChronology()))) {
          bool = false;
        }
      }
    }
  }
  
  public int get()
  {
    return getField().get(getMillis());
  }
  
  public String getAsShortText()
  {
    return getAsShortText(null);
  }
  
  public String getAsShortText(Locale paramLocale)
  {
    return getField().getAsShortText(getMillis(), paramLocale);
  }
  
  public String getAsString()
  {
    return Integer.toString(get());
  }
  
  public String getAsText()
  {
    return getAsText(null);
  }
  
  public String getAsText(Locale paramLocale)
  {
    return getField().getAsText(getMillis(), paramLocale);
  }
  
  protected Chronology getChronology()
  {
    throw new UnsupportedOperationException("The method getChronology() was added in v1.4 and needs to be implemented by subclasses of AbstractReadableInstantFieldProperty");
  }
  
  public int getDifference(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (int i = getField().getDifference(getMillis(), DateTimeUtils.currentTimeMillis());; i = getField().getDifference(getMillis(), paramReadableInstant.getMillis())) {
      return i;
    }
  }
  
  public long getDifferenceAsLong(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (long l = getField().getDifferenceAsLong(getMillis(), DateTimeUtils.currentTimeMillis());; l = getField().getDifferenceAsLong(getMillis(), paramReadableInstant.getMillis())) {
      return l;
    }
  }
  
  public DurationField getDurationField()
  {
    return getField().getDurationField();
  }
  
  public abstract DateTimeField getField();
  
  public DateTimeFieldType getFieldType()
  {
    return getField().getType();
  }
  
  public int getLeapAmount()
  {
    return getField().getLeapAmount(getMillis());
  }
  
  public DurationField getLeapDurationField()
  {
    return getField().getLeapDurationField();
  }
  
  public int getMaximumShortTextLength(Locale paramLocale)
  {
    return getField().getMaximumShortTextLength(paramLocale);
  }
  
  public int getMaximumTextLength(Locale paramLocale)
  {
    return getField().getMaximumTextLength(paramLocale);
  }
  
  public int getMaximumValue()
  {
    return getField().getMaximumValue(getMillis());
  }
  
  public int getMaximumValueOverall()
  {
    return getField().getMaximumValue();
  }
  
  protected abstract long getMillis();
  
  public int getMinimumValue()
  {
    return getField().getMinimumValue(getMillis());
  }
  
  public int getMinimumValueOverall()
  {
    return getField().getMinimumValue();
  }
  
  public String getName()
  {
    return getField().getName();
  }
  
  public DurationField getRangeDurationField()
  {
    return getField().getRangeDurationField();
  }
  
  public int hashCode()
  {
    return get() * 17 + getFieldType().hashCode() + getChronology().hashCode();
  }
  
  public boolean isLeap()
  {
    return getField().isLeap(getMillis());
  }
  
  public long remainder()
  {
    return getField().remainder(getMillis());
  }
  
  public Interval toInterval()
  {
    DateTimeField localDateTimeField = getField();
    long l = localDateTimeField.roundFloor(getMillis());
    return new Interval(l, localDateTimeField.add(l, 1), getChronology());
  }
  
  public String toString()
  {
    return "Property[" + getName() + "]";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\field\AbstractReadableInstantFieldProperty.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */