package com.testfairy.p;

import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.view.ContextThemeWrapper;

public class c
{
  public static AlertDialog.Builder a(Context paramContext)
  {
    if (Build.VERSION.SDK_INT >= 14) {
      paramContext = new AlertDialog.Builder(new ContextThemeWrapper(paramContext, 16974126));
    }
    for (;;)
    {
      return paramContext;
      if (Build.VERSION.SDK_INT >= 11) {
        paramContext = new AlertDialog.Builder(new ContextThemeWrapper(paramContext, 16973935));
      } else {
        paramContext = new AlertDialog.Builder(new ContextThemeWrapper(paramContext, 16973835));
      }
    }
  }
  
  public static void a(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface != null) {}
    try
    {
      paramDialogInterface.dismiss();
      return;
    }
    catch (Exception paramDialogInterface)
    {
      for (;;) {}
    }
  }
  
  public static ProgressDialog b(Context paramContext)
  {
    if (Build.VERSION.SDK_INT >= 14) {
      paramContext = new ProgressDialog(new ContextThemeWrapper(paramContext, 16974126));
    }
    for (;;)
    {
      return paramContext;
      if (Build.VERSION.SDK_INT >= 11) {
        paramContext = new ProgressDialog(new ContextThemeWrapper(paramContext, 16973935));
      } else {
        paramContext = new ProgressDialog(new ContextThemeWrapper(paramContext, 16973835));
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */