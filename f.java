package com.testfairy;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.Reader;
import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class f
{
  private static final String a = "test-keys";
  private static final String b = "os.version";
  private static final String c = "/proc/meminfo";
  private static final String d = "/sys/devices/system/cpu/";
  private static final String e = "/system/app/Superuser.apk";
  private int f = -1;
  private int g = -1;
  private int h = 0;
  private int i = 0;
  private int j = 0;
  private DisplayMetrics k = null;
  
  private static String a(byte[] paramArrayOfByte)
  {
    Formatter localFormatter = new Formatter();
    int n = paramArrayOfByte.length;
    for (int m = 0; m < n; m++) {
      localFormatter.format("%02x", new Object[] { Byte.valueOf(paramArrayOfByte[m]) });
    }
    return localFormatter.toString();
  }
  
  private void e(Context paramContext)
  {
    if (this.k == null)
    {
      this.k = paramContext.getResources().getDisplayMetrics();
      this.h = this.k.widthPixels;
      this.i = this.k.heightPixels;
      this.j = this.k.densityDpi;
    }
  }
  
  public int a()
  {
    int m = 0;
    try
    {
      if (this.g < 0) {
        break label18;
      }
      n = this.g;
      m = n;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        int n;
        Pattern localPattern;
        BufferedReader localBufferedReader;
        Object localObject;
        Log.e(e.a, "failed to get memory size");
      }
    }
    return m;
    label18:
    this.g = 0;
    localPattern = Pattern.compile("^MemTotal:\\s*(\\d+)\\s*kB");
    localBufferedReader = new java/io/BufferedReader;
    localObject = new java/io/FileReader;
    ((FileReader)localObject).<init>("/proc/meminfo");
    localBufferedReader.<init>((Reader)localObject);
    label53:
    localObject = localBufferedReader.readLine();
    if (localObject == null) {}
    for (;;)
    {
      localBufferedReader.close();
      n = this.g;
      m = n;
      break;
      localObject = localPattern.matcher((CharSequence)localObject);
      if (!((Matcher)localObject).matches()) {
        break label53;
      }
      this.g = Integer.valueOf(((Matcher)localObject).group(1)).intValue();
    }
  }
  
  public String a(Context paramContext)
  {
    try
    {
      String str = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
      if (str == null) {
        break label30;
      }
      int m = str.length();
      if (m < 8) {
        break label30;
      }
      paramContext = str;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        try
        {
          label30:
          paramContext = ((TelephonyManager)paramContext.getSystemService("phone")).getDeviceId();
        }
        catch (Exception paramContext)
        {
          paramContext = "";
        }
      }
    }
    return paramContext;
  }
  
  public int b()
  {
    for (int m = 1;; m = n)
    {
      try
      {
        if (this.f < 0) {
          break label18;
        }
        n = this.f;
        m = n;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          int n;
          label18:
          File localFile;
          FilenameFilter local1;
          this.f = 1;
        }
      }
      return m;
      localFile = new java/io/File;
      localFile.<init>("/sys/devices/system/cpu/");
      local1 = new com/testfairy/f$1;
      local1.<init>(this);
      this.f = localFile.listFiles(local1).length;
      n = this.f;
    }
  }
  
  public int b(Context paramContext)
  {
    e(paramContext);
    return this.h;
  }
  
  public int c(Context paramContext)
  {
    e(paramContext);
    return this.i;
  }
  
  public String c()
  {
    return System.getProperty("os.version");
  }
  
  public int d(Context paramContext)
  {
    e(paramContext);
    return this.j;
  }
  
  public boolean d()
  {
    boolean bool1 = true;
    Object localObject = Build.TAGS;
    if ((localObject != null) && (((String)localObject).contains("test-keys"))) {}
    for (;;)
    {
      return bool1;
      try
      {
        localObject = new java/io/File;
        ((File)localObject).<init>("/system/app/Superuser.apk");
        boolean bool2 = ((File)localObject).exists();
        if (bool2) {
          continue;
        }
      }
      catch (Throwable localThrowable)
      {
        for (;;) {}
      }
      bool1 = false;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */