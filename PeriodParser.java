package org.joda.time.format;

import java.util.Locale;
import org.joda.time.ReadWritablePeriod;

public abstract interface PeriodParser
{
  public abstract int parseInto(ReadWritablePeriod paramReadWritablePeriod, String paramString, int paramInt, Locale paramLocale);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\PeriodParser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */