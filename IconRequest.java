package io.fabric.sdk.android.services.settings;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.CommonUtils;

public class IconRequest
{
  public final String hash;
  public final int height;
  public final int iconResourceId;
  public final int width;
  
  public IconRequest(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    this.hash = paramString;
    this.iconResourceId = paramInt1;
    this.width = paramInt2;
    this.height = paramInt3;
  }
  
  public static IconRequest build(Context paramContext, String paramString)
  {
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramString != null) {}
    try
    {
      int i = CommonUtils.getAppIconResourceId(paramContext);
      Object localObject3 = Fabric.getLogger();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((Logger)localObject3).d("Fabric", "App icon resource ID is " + i);
      localObject3 = new android/graphics/BitmapFactory$Options;
      ((BitmapFactory.Options)localObject3).<init>();
      ((BitmapFactory.Options)localObject3).inJustDecodeBounds = true;
      BitmapFactory.decodeResource(paramContext.getResources(), i, (BitmapFactory.Options)localObject3);
      localObject1 = new io/fabric/sdk/android/services/settings/IconRequest;
      ((IconRequest)localObject1).<init>(paramString, i, ((BitmapFactory.Options)localObject3).outWidth, ((BitmapFactory.Options)localObject3).outHeight);
      return (IconRequest)localObject1;
    }
    catch (Exception paramContext)
    {
      for (;;)
      {
        Fabric.getLogger().e("Fabric", "Failed to load icon", paramContext);
        localObject1 = localObject2;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\IconRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */