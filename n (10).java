package com.testfairy.n;

import android.net.TrafficStats;
import android.os.Process;
import com.testfairy.g;
import java.util.HashMap;
import java.util.Map;

public class j
  extends b
{
  private long a = -1L;
  private long b = -1L;
  private long c = 0L;
  private long d = 0L;
  private int e = Process.myUid();
  
  public j(com.testfairy.m.b paramb)
  {
    super(paramb);
  }
  
  public void a()
  {
    long l2 = 0L;
    long l4 = TrafficStats.getUidRxBytes(this.e);
    long l5 = TrafficStats.getUidTxBytes(this.e);
    long l1;
    long l3;
    if ((l4 != this.a) || (l5 != this.b))
    {
      l1 = Math.max(l4 - this.a, 0L);
      long l6 = Math.max(l5 - this.b, 0L);
      l3 = l1 - this.c;
      l1 = l3;
      if (l3 < 0L)
      {
        this.c = (-l3);
        l1 = 0L;
      }
      l3 = l6 - this.d;
      if (l3 >= 0L) {
        break label188;
      }
      this.d = (-l3);
    }
    for (;;)
    {
      Object localObject = new HashMap(2);
      ((Map)localObject).put("rx", Long.valueOf(l1));
      ((Map)localObject).put("tx", Long.valueOf(l2));
      localObject = new g(13, (Map)localObject);
      b().a((g)localObject);
      this.a = l4;
      this.b = l5;
      return;
      label188:
      l2 = l3;
    }
  }
  
  public void a(long paramLong1, long paramLong2)
  {
    this.c += paramLong1;
    this.d += paramLong2;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */