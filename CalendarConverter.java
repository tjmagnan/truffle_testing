package org.joda.time.convert;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.BuddhistChronology;
import org.joda.time.chrono.GJChronology;
import org.joda.time.chrono.GregorianChronology;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.JulianChronology;

final class CalendarConverter
  extends AbstractConverter
  implements InstantConverter, PartialConverter
{
  static final CalendarConverter INSTANCE = new CalendarConverter();
  
  public Chronology getChronology(Object paramObject, Chronology paramChronology)
  {
    if (paramChronology != null) {}
    for (;;)
    {
      return paramChronology;
      paramChronology = (Calendar)paramObject;
      try
      {
        paramObject = DateTimeZone.forTimeZone(paramChronology.getTimeZone());
        paramChronology = getChronology(paramChronology, (DateTimeZone)paramObject);
      }
      catch (IllegalArgumentException paramObject)
      {
        for (;;)
        {
          paramObject = DateTimeZone.getDefault();
        }
      }
    }
  }
  
  public Chronology getChronology(Object paramObject, DateTimeZone paramDateTimeZone)
  {
    if (paramObject.getClass().getName().endsWith(".BuddhistCalendar")) {
      paramObject = BuddhistChronology.getInstance(paramDateTimeZone);
    }
    for (;;)
    {
      return (Chronology)paramObject;
      if ((paramObject instanceof GregorianCalendar))
      {
        long l = ((GregorianCalendar)paramObject).getGregorianChange().getTime();
        if (l == Long.MIN_VALUE) {
          paramObject = GregorianChronology.getInstance(paramDateTimeZone);
        } else if (l == Long.MAX_VALUE) {
          paramObject = JulianChronology.getInstance(paramDateTimeZone);
        } else {
          paramObject = GJChronology.getInstance(paramDateTimeZone, l, 4);
        }
      }
      else
      {
        paramObject = ISOChronology.getInstance(paramDateTimeZone);
      }
    }
  }
  
  public long getInstantMillis(Object paramObject, Chronology paramChronology)
  {
    return ((Calendar)paramObject).getTime().getTime();
  }
  
  public Class<?> getSupportedType()
  {
    return Calendar.class;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\CalendarConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */