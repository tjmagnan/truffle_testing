package org.joda.time;

import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Minutes
  extends BaseSingleFieldPeriod
{
  public static final Minutes MAX_VALUE = new Minutes(Integer.MAX_VALUE);
  public static final Minutes MIN_VALUE = new Minutes(Integer.MIN_VALUE);
  public static final Minutes ONE;
  private static final PeriodFormatter PARSER = ISOPeriodFormat.standard().withParseType(PeriodType.minutes());
  public static final Minutes THREE;
  public static final Minutes TWO;
  public static final Minutes ZERO = new Minutes(0);
  private static final long serialVersionUID = 87525275727380863L;
  
  static
  {
    ONE = new Minutes(1);
    TWO = new Minutes(2);
    THREE = new Minutes(3);
  }
  
  private Minutes(int paramInt)
  {
    super(paramInt);
  }
  
  public static Minutes minutes(int paramInt)
  {
    Minutes localMinutes;
    switch (paramInt)
    {
    default: 
      localMinutes = new Minutes(paramInt);
    }
    for (;;)
    {
      return localMinutes;
      localMinutes = ZERO;
      continue;
      localMinutes = ONE;
      continue;
      localMinutes = TWO;
      continue;
      localMinutes = THREE;
      continue;
      localMinutes = MAX_VALUE;
      continue;
      localMinutes = MIN_VALUE;
    }
  }
  
  public static Minutes minutesBetween(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    return minutes(BaseSingleFieldPeriod.between(paramReadableInstant1, paramReadableInstant2, DurationFieldType.minutes()));
  }
  
  public static Minutes minutesBetween(ReadablePartial paramReadablePartial1, ReadablePartial paramReadablePartial2)
  {
    if (((paramReadablePartial1 instanceof LocalTime)) && ((paramReadablePartial2 instanceof LocalTime))) {}
    for (paramReadablePartial1 = minutes(DateTimeUtils.getChronology(paramReadablePartial1.getChronology()).minutes().getDifference(((LocalTime)paramReadablePartial2).getLocalMillis(), ((LocalTime)paramReadablePartial1).getLocalMillis()));; paramReadablePartial1 = minutes(BaseSingleFieldPeriod.between(paramReadablePartial1, paramReadablePartial2, ZERO))) {
      return paramReadablePartial1;
    }
  }
  
  public static Minutes minutesIn(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (paramReadableInterval = ZERO;; paramReadableInterval = minutes(BaseSingleFieldPeriod.between(paramReadableInterval.getStart(), paramReadableInterval.getEnd(), DurationFieldType.minutes()))) {
      return paramReadableInterval;
    }
  }
  
  @FromString
  public static Minutes parseMinutes(String paramString)
  {
    if (paramString == null) {}
    for (paramString = ZERO;; paramString = minutes(PARSER.parsePeriod(paramString).getMinutes())) {
      return paramString;
    }
  }
  
  private Object readResolve()
  {
    return minutes(getValue());
  }
  
  public static Minutes standardMinutesIn(ReadablePeriod paramReadablePeriod)
  {
    return minutes(BaseSingleFieldPeriod.standardPeriodIn(paramReadablePeriod, 60000L));
  }
  
  public Minutes dividedBy(int paramInt)
  {
    if (paramInt == 1) {}
    for (Minutes localMinutes = this;; localMinutes = minutes(getValue() / paramInt)) {
      return localMinutes;
    }
  }
  
  public DurationFieldType getFieldType()
  {
    return DurationFieldType.minutes();
  }
  
  public int getMinutes()
  {
    return getValue();
  }
  
  public PeriodType getPeriodType()
  {
    return PeriodType.minutes();
  }
  
  public boolean isGreaterThan(Minutes paramMinutes)
  {
    boolean bool = true;
    if (paramMinutes == null) {
      if (getValue() <= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() <= paramMinutes.getValue()) {
        bool = false;
      }
    }
  }
  
  public boolean isLessThan(Minutes paramMinutes)
  {
    boolean bool = true;
    if (paramMinutes == null) {
      if (getValue() >= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() >= paramMinutes.getValue()) {
        bool = false;
      }
    }
  }
  
  public Minutes minus(int paramInt)
  {
    return plus(FieldUtils.safeNegate(paramInt));
  }
  
  public Minutes minus(Minutes paramMinutes)
  {
    if (paramMinutes == null) {}
    for (paramMinutes = this;; paramMinutes = minus(paramMinutes.getValue())) {
      return paramMinutes;
    }
  }
  
  public Minutes multipliedBy(int paramInt)
  {
    return minutes(FieldUtils.safeMultiply(getValue(), paramInt));
  }
  
  public Minutes negated()
  {
    return minutes(FieldUtils.safeNegate(getValue()));
  }
  
  public Minutes plus(int paramInt)
  {
    if (paramInt == 0) {}
    for (Minutes localMinutes = this;; localMinutes = minutes(FieldUtils.safeAdd(getValue(), paramInt))) {
      return localMinutes;
    }
  }
  
  public Minutes plus(Minutes paramMinutes)
  {
    if (paramMinutes == null) {}
    for (paramMinutes = this;; paramMinutes = plus(paramMinutes.getValue())) {
      return paramMinutes;
    }
  }
  
  public Days toStandardDays()
  {
    return Days.days(getValue() / 1440);
  }
  
  public Duration toStandardDuration()
  {
    return new Duration(getValue() * 60000L);
  }
  
  public Hours toStandardHours()
  {
    return Hours.hours(getValue() / 60);
  }
  
  public Seconds toStandardSeconds()
  {
    return Seconds.seconds(FieldUtils.safeMultiply(getValue(), 60));
  }
  
  public Weeks toStandardWeeks()
  {
    return Weeks.weeks(getValue() / 10080);
  }
  
  @ToString
  public String toString()
  {
    return "PT" + String.valueOf(getValue()) + "M";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Minutes.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */