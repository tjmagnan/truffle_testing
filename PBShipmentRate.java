package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PBShipmentRate
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -1006859404479414873L;
  @Expose
  @SerializedName("baseCharge")
  private Double baseCharge;
  @Expose
  @SerializedName("baseChargeCurrency")
  private Object baseChargeCurrency;
  @Expose
  @SerializedName("batch")
  private PBShipmentRateBatch batch;
  @Expose
  @SerializedName("correlationId")
  private String correlationId;
  @Expose
  @SerializedName("dateOfMailingUTC")
  private Date dateOfMailingUTC;
  @Expose
  @SerializedName("discounts")
  private Object discounts;
  @Expose
  @SerializedName("estimatedDeliveryDate")
  private Date estimatedDeliveryDate;
  @Expose
  @SerializedName("labelPrintingOptions")
  private PBPrintingOptions labelPrintingOptions;
  @Expose
  @SerializedName("rateDiscountType")
  private Object rateDiscountType;
  @Expose
  @SerializedName("rateEffectiveDate")
  private Object rateEffectiveDate;
  @Expose
  @SerializedName("serviceCost")
  private Double serviceCost;
  @Expose
  @SerializedName("chargesSpecialServices")
  private List<PBSpecialServiceCharges> specialServiceCharges = new ArrayList();
  @Expose
  @SerializedName("surcharges")
  private Object surcharges;
  @Expose
  @SerializedName("taxes")
  private Object taxes;
  @Expose
  @SerializedName("totalPackageCharge")
  private Double totalPackageCharge;
  @Expose
  @SerializedName("totalPackageChargeCurrency")
  private Object totalPackageChargeCurrency;
  @Expose
  @SerializedName("totalTaxAmount")
  private Object totalTaxAmount;
  
  public PBShipmentRate() {}
  
  public PBShipmentRate(Double paramDouble1, Object paramObject1, PBShipmentRateBatch paramPBShipmentRateBatch, List<PBSpecialServiceCharges> paramList, String paramString, Date paramDate1, Object paramObject2, Date paramDate2, PBPrintingOptions paramPBPrintingOptions, Object paramObject3, Object paramObject4, Double paramDouble2, Object paramObject5, Object paramObject6, Double paramDouble3, Object paramObject7, Object paramObject8)
  {
    this.baseCharge = paramDouble1;
    this.baseChargeCurrency = paramObject1;
    this.batch = paramPBShipmentRateBatch;
    this.specialServiceCharges = paramList;
    this.correlationId = paramString;
    this.dateOfMailingUTC = paramDate1;
    this.discounts = paramObject2;
    this.estimatedDeliveryDate = paramDate2;
    this.labelPrintingOptions = paramPBPrintingOptions;
    this.rateDiscountType = paramObject3;
    this.rateEffectiveDate = paramObject4;
    this.serviceCost = paramDouble2;
    this.surcharges = paramObject5;
    this.taxes = paramObject6;
    this.totalPackageCharge = paramDouble3;
    this.totalPackageChargeCurrency = paramObject7;
    this.totalTaxAmount = paramObject8;
  }
  
  public Double getBaseCharge()
  {
    return this.baseCharge;
  }
  
  public Object getBaseChargeCurrency()
  {
    return this.baseChargeCurrency;
  }
  
  public PBShipmentRateBatch getBatch()
  {
    return this.batch;
  }
  
  public String getCorrelationId()
  {
    return this.correlationId;
  }
  
  public Date getDateOfMailingUTC()
  {
    return this.dateOfMailingUTC;
  }
  
  public Object getDiscounts()
  {
    return this.discounts;
  }
  
  public Date getEstimatedDeliveryDate()
  {
    return this.estimatedDeliveryDate;
  }
  
  public PBPrintingOptions getLabelPrintingOptions()
  {
    return this.labelPrintingOptions;
  }
  
  public Object getRateDiscountType()
  {
    return this.rateDiscountType;
  }
  
  public Object getRateEffectiveDate()
  {
    return this.rateEffectiveDate;
  }
  
  public Double getServiceCost()
  {
    return this.serviceCost;
  }
  
  public List<PBSpecialServiceCharges> getSpecialServiceCharges()
  {
    return this.specialServiceCharges;
  }
  
  public Object getSurcharges()
  {
    return this.surcharges;
  }
  
  public Object getTaxes()
  {
    return this.taxes;
  }
  
  public Double getTotalPackageCharge()
  {
    return this.totalPackageCharge;
  }
  
  public Object getTotalPackageChargeCurrency()
  {
    return this.totalPackageChargeCurrency;
  }
  
  public Object getTotalTaxAmount()
  {
    return this.totalTaxAmount;
  }
  
  public void setBaseCharge(Double paramDouble)
  {
    this.baseCharge = paramDouble;
  }
  
  public void setBaseChargeCurrency(Object paramObject)
  {
    this.baseChargeCurrency = paramObject;
  }
  
  public void setBatch(PBShipmentRateBatch paramPBShipmentRateBatch)
  {
    this.batch = paramPBShipmentRateBatch;
  }
  
  public void setCorrelationId(String paramString)
  {
    this.correlationId = paramString;
  }
  
  public void setDateOfMailingUTC(Date paramDate)
  {
    this.dateOfMailingUTC = paramDate;
  }
  
  public void setDiscounts(Object paramObject)
  {
    this.discounts = paramObject;
  }
  
  public void setEstimatedDeliveryDate(Date paramDate)
  {
    this.estimatedDeliveryDate = paramDate;
  }
  
  public void setLabelPrintingOptions(PBPrintingOptions paramPBPrintingOptions)
  {
    this.labelPrintingOptions = paramPBPrintingOptions;
  }
  
  public void setRateDiscountType(Object paramObject)
  {
    this.rateDiscountType = paramObject;
  }
  
  public void setRateEffectiveDate(Object paramObject)
  {
    this.rateEffectiveDate = paramObject;
  }
  
  public void setServiceCost(Double paramDouble)
  {
    this.serviceCost = paramDouble;
  }
  
  public void setSpecialServiceCharges(List<PBSpecialServiceCharges> paramList)
  {
    this.specialServiceCharges = paramList;
  }
  
  public void setSurcharges(Object paramObject)
  {
    this.surcharges = paramObject;
  }
  
  public void setTaxes(Object paramObject)
  {
    this.taxes = paramObject;
  }
  
  public void setTotalPackageCharge(Double paramDouble)
  {
    this.totalPackageCharge = paramDouble;
  }
  
  public void setTotalPackageChargeCurrency(Object paramObject)
  {
    this.totalPackageChargeCurrency = paramObject;
  }
  
  public void setTotalTaxAmount(Object paramObject)
  {
    this.totalTaxAmount = paramObject;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentRate.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */