package com.crashlytics.android.beta;

class ImmediateCheckForUpdatesController
  extends AbstractCheckForUpdatesController
{
  public ImmediateCheckForUpdatesController()
  {
    super(true);
  }
  
  public boolean isActivityLifecycleTriggered()
  {
    return false;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\beta\ImmediateCheckForUpdatesController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */