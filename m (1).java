package com.testfairy.m;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.h.c;
import com.testfairy.h.f;
import com.testfairy.k.b;
import com.testfairy.n;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONException;
import org.json.JSONObject;

public class a
{
  private static final String a = "testfairy-events";
  private Handler b;
  private File c;
  private Vector d = new Vector();
  private long e = 0L;
  private long f = 0L;
  private final Object g = new Object();
  private boolean h = false;
  private HashMap i = new HashMap();
  private JSONObject j;
  private com.testfairy.a.a k;
  private Comparator l = new Comparator()
  {
    public int a(File paramAnonymousFile1, File paramAnonymousFile2)
    {
      return (int)(paramAnonymousFile1.lastModified() - paramAnonymousFile2.lastModified());
    }
  };
  private Runnable m = new Runnable()
  {
    public void run()
    {
      File localFile = null;
      synchronized (a.d(a.this))
      {
        if (a.e(a.this).size() > 0) {
          localFile = (File)a.e(a.this).remove(0);
        }
        if (localFile != null)
        {
          Log.v(e.a, "Uploading file " + localFile.getAbsolutePath());
          a.a(a.this, localFile);
        }
        return;
      }
    }
  };
  
  public a(Context paramContext, com.testfairy.a.a parama)
  {
    this.j = parama.e();
    this.k = parama;
    this.c = paramContext.getFilesDir();
    Log.d(e.a, "BackgroundUploader: persistentPath=" + this.c);
    if (this.c != null)
    {
      g();
      paramContext = new HandlerThread("testfairy-background-uploader", 1);
      paramContext.start();
      this.b = new Handler(paramContext.getLooper());
    }
  }
  
  private long a(long paramLong, File paramFile, String paramString)
  {
    paramString = a(paramString);
    if (paramString == null) {}
    for (;;)
    {
      long l1 = paramFile.lastModified();
      if (l1 > 0L) {}
      for (paramLong = l1 - paramLong;; paramLong = 0L) {
        return paramLong;
      }
      paramFile = paramString;
    }
  }
  
  private File a(String paramString)
  {
    int n = this.d.size() - 1;
    if (n >= 0) {
      if (!a((File)this.d.get(n)).equals(paramString)) {}
    }
    for (paramString = (File)this.d.get(n);; paramString = null)
    {
      return paramString;
      n--;
      break;
    }
  }
  
  private String a(File paramFile)
  {
    paramFile = paramFile.getAbsolutePath();
    int n = paramFile.lastIndexOf("/testfairy-events.");
    int i1;
    if (n >= 0)
    {
      i1 = "/testfairy-events.".length() + n;
      n = paramFile.indexOf('.', i1);
      if (n <= 0) {}
    }
    for (paramFile = paramFile.substring(i1, n);; paramFile = null)
    {
      return paramFile;
      Log.e(e.a, "Could not parse token from " + paramFile);
    }
  }
  
  private File[] a(File[] paramArrayOfFile)
  {
    int i1;
    int n;
    try
    {
      Arrays.sort(paramArrayOfFile, this.l);
      return paramArrayOfFile;
    }
    catch (Exception localException)
    {
      Log.e(e.a, "Exception while sorting these files:");
      i1 = paramArrayOfFile.length;
      n = 0;
    }
    while (n < i1)
    {
      File localFile = paramArrayOfFile[n];
      Log.e(e.a, "     " + localFile.getAbsolutePath() + " @ " + localFile.lastModified());
      n++;
    }
  }
  
  private void b(File paramFile)
  {
    String str1 = a(paramFile);
    ??? = str1.substring("anonymous-".length());
    Log.v(e.a, "Found an anonymous disk event file to be uploaded: " + str1 + ", with start time " + (String)???);
    long l1 = a(Long.valueOf((String)???).longValue(), paramFile, str1);
    com.testfairy.j localj = this.k.b();
    String str2 = "" + localj.d() + "-" + localj.b() + "-" + localj.c() + "-" + localj.e();
    f localf = new f();
    localf.a(n.W, str2);
    localf.a(n.X, (String)???);
    localf.a("duration", String.valueOf(l1));
    localf.a(n.Z, this.j.toString());
    localf.a(n.ac, String.valueOf(2));
    localf.a("isOffline", "1");
    if (b.a()) {
      ??? = "on";
    }
    for (;;)
    {
      localf.a("wifi", (String)???);
      localf.a(n.ab, "20170625-78d2bc1");
      synchronized (this.g)
      {
        this.d.add(0, paramFile);
        new com.testfairy.k.d(localj.f()).a(localf, new c(str1));
        return;
        ??? = "off";
      }
    }
  }
  
  private void c(File paramFile)
  {
    String str1 = a(paramFile);
    Object localObject;
    if (this.i.containsKey(str1))
    {
      String str2 = (String)this.i.get(str1);
      localObject = com.testfairy.p.d.a(paramFile);
      Log.v(e.a, "Sending persistent file to endpoint " + str2);
      new com.testfairy.k.d(str2).a("20170625-78d2bc1", str1, (String)localObject, new a(paramFile));
    }
    for (;;)
    {
      return;
      Log.d(e.a, "Resuming session for token " + str1);
      localObject = new f();
      ((f)localObject).a("sessionToken", str1);
      new com.testfairy.k.d(this.k.b().f()).b((f)localObject, new b(str1, paramFile));
    }
  }
  
  private void d(File paramFile)
  {
    try
    {
      if (a(paramFile).startsWith("anonymous-")) {
        b(paramFile);
      }
      for (;;)
      {
        return;
        c(paramFile);
      }
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        Log.e(e.a, "Exception while reading " + paramFile.getAbsolutePath(), localThrowable);
      }
    }
  }
  
  private void g()
  {
    File[] arrayOfFile1 = this.c.listFiles(new com.testfairy.p.j("testfairy-events"));
    if (arrayOfFile1 == null) {}
    for (;;)
    {
      return;
      File[] arrayOfFile2 = a(arrayOfFile1);
      this.d = new Vector();
      int i1 = arrayOfFile2.length;
      int n = 0;
      long l1 = 0L;
      while (n < i1)
      {
        arrayOfFile1 = arrayOfFile2[n];
        Log.d(e.a, "Found file: " + arrayOfFile1 + ": " + arrayOfFile1.length() + " bytes");
        l1 += arrayOfFile1.length();
        this.d.add(arrayOfFile1);
        n++;
      }
      Log.v(e.a, "Total of " + l1 + " bytes are waiting to be sent");
      this.f = 0L;
      this.e = l1;
    }
  }
  
  public int a()
  {
    return this.d.size();
  }
  
  public long b()
  {
    return this.f;
  }
  
  public long c()
  {
    return this.e;
  }
  
  public void d()
  {
    this.b.postDelayed(this.m, 2000L);
  }
  
  public boolean e()
  {
    if (this.d.size() == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean f()
  {
    return this.h;
  }
  
  private class a
    extends c
  {
    private File b;
    
    public a(File paramFile)
    {
      this.b = paramFile;
    }
    
    public void a(String paramString)
    {
      Log.d(e.a, "Success sending events to server, continuing to next file in queue");
      super.a(paramString);
      a.a(a.this, a.a(a.this) + this.b.length());
      this.b.delete();
      a.c(a.this).postDelayed(a.b(a.this), 1L);
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      Log.e(e.a, "Could not send " + this.b.getAbsolutePath() + " to server endpoint");
    }
  }
  
  private class b
    extends c
  {
    private File b;
    private String g;
    
    public b(String paramString, File paramFile)
    {
      this.b = paramFile;
      this.g = paramString;
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      Log.e(e.a, "Resumed session with token " + this.g + " => " + paramString);
      for (;;)
      {
        try
        {
          ??? = new org/json/JSONObject;
          ((JSONObject)???).<init>(paramString);
          paramString = ((JSONObject)???).getString("endpointAddress");
          a.g(a.this).put(this.g, paramString);
        }
        catch (Exception paramString)
        {
          Log.e(e.a, "Failed to process json response", paramString);
          a.a(a.this, true);
          continue;
        }
        synchronized (a.d(a.this))
        {
          a.e(a.this).add(0, this.b);
          a.c(a.this).post(a.b(a.this));
          return;
        }
      }
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      Log.e(e.a, "Failed to start session ");
      a.a(a.this, true);
    }
  }
  
  private class c
    extends c
  {
    private String b;
    
    public c(String paramString)
    {
      this.b = paramString;
    }
    
    private void d(String paramString)
    {
      Log.v(e.a, "Associating anonymous token " + this.b + " with server token " + paramString);
      synchronized (a.d(a.this))
      {
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = "testfairy-events." + this.b + ".";
        for (int i = 0; i < a.e(a.this).size(); i++)
        {
          File localFile = (File)a.e(a.this).elementAt(i);
          if (localFile.getName().startsWith((String)localObject2))
          {
            Object localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = localFile.getParent() + "/" + "testfairy-events" + "." + paramString + "." + localFile.getName().substring(((String)localObject2).length());
            Log.v(e.a, "Found matching event file");
            Object localObject4 = new java/io/File;
            ((File)localObject4).<init>((String)localObject3);
            localFile.renameTo((File)localObject4);
            a.e(a.this).remove(i);
            localObject4 = a.e(a.this);
            localFile = new java/io/File;
            localFile.<init>((String)localObject3);
            ((Vector)localObject4).add(i, localFile);
          }
        }
        return;
      }
    }
    
    private void e()
    {
      int i = 0;
      synchronized (a.d(a.this))
      {
        Iterator localIterator = a.e(a.this).iterator();
        int k = 0;
        while (localIterator.hasNext())
        {
          File localFile = (File)localIterator.next();
          int j = i;
          int m = k;
          if (localFile.getName().contains("anonymous"))
          {
            m = (int)(k + localFile.length());
            j = i + 1;
            localFile.delete();
            localIterator.remove();
          }
          k = m;
          i = j;
        }
        Log.d(e.a, i + " files was deleted (total size : " + k + ")");
        a.b(a.this, a.f(a.this) - k);
        return;
      }
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      Log.v(e.a, "Background uploader start session received");
      for (;;)
      {
        try
        {
          localJSONObject = new org/json/JSONObject;
          localJSONObject.<init>(paramString);
          paramString = localJSONObject.getString("status");
          if ((paramString != null) && (paramString.equals("ok")))
          {
            d(localJSONObject.getString("sessionToken"));
            a.c(a.this).post(a.b(a.this));
            return;
          }
        }
        catch (JSONException paramString)
        {
          JSONObject localJSONObject;
          Log.e(e.a, "Background uploader could not parse json" + paramString);
          a.a(a.this, true);
          continue;
          Log.e(e.a, "Background uploader could not connect to server to start a session");
          a.a(a.this, true);
          continue;
        }
        if ((paramString == null) || (!paramString.equals("fail"))) {
          continue;
        }
        Log.i(e.a, "Background uploader will not start a session");
        if (localJSONObject.getInt("code") == 101) {
          e();
        }
        a.a(a.this, true);
      }
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      Log.e(e.a, "Could not start session");
      a.a(a.this, true);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\m\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */