package com.testfairy.k;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class b
{
  private static ConnectivityManager a = null;
  private static WifiManager b = null;
  private static long c = 0L;
  private static long d = 0L;
  private static boolean e = false;
  private static boolean f = false;
  private static final long g = 1000L;
  
  public static void a(ConnectivityManager paramConnectivityManager, WifiManager paramWifiManager)
  {
    a = paramConnectivityManager;
    b = paramWifiManager;
  }
  
  public static boolean a()
  {
    bool1 = true;
    bool2 = false;
    for (;;)
    {
      try
      {
        long l = System.currentTimeMillis();
        if (l - c >= 1000L)
        {
          c = l;
          NetworkInfo localNetworkInfo = a.getNetworkInfo(1);
          if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected())) {
            continue;
          }
          e = bool1;
        }
        bool1 = e;
      }
      catch (Throwable localThrowable)
      {
        bool1 = bool2;
        continue;
      }
      return bool1;
      bool1 = false;
    }
  }
  
  public static boolean b()
  {
    bool2 = false;
    for (;;)
    {
      try
      {
        long l = System.currentTimeMillis();
        if (l - d >= 1000L)
        {
          d = l;
          NetworkInfo localNetworkInfo = a.getNetworkInfo(0);
          if ((localNetworkInfo == null) || (!localNetworkInfo.isConnected())) {
            continue;
          }
          bool1 = true;
          f = bool1;
        }
        bool1 = f;
      }
      catch (Throwable localThrowable)
      {
        boolean bool1 = bool2;
        continue;
      }
      return bool1;
      bool1 = false;
    }
  }
  
  public static WifiManager c()
  {
    return b;
  }
  
  public static boolean d()
  {
    if ((b()) || (a())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\k\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */