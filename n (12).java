package com.testfairy.n;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;

public class l
  extends TimerTask
{
  private boolean a = false;
  private final List b = Collections.synchronizedList(new ArrayList());
  
  public void a()
  {
    this.b.clear();
  }
  
  public void a(b paramb)
  {
    this.b.add(paramb);
  }
  
  public void b()
  {
    this.a = true;
  }
  
  public void b(b paramb)
  {
    this.b.remove(paramb);
  }
  
  public void c()
  {
    this.a = false;
  }
  
  public void run()
  {
    if (!this.a)
    {
      ListIterator localListIterator = this.b.listIterator();
      while (localListIterator.hasNext()) {
        ((b)localListIterator.next()).a();
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */