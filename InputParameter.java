package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class InputParameter
  implements Serializable
{
  private static final long serialVersionUID = 4288528840944544164L;
  @Expose
  @SerializedName("brandedName")
  private String brandedName;
  @Expose
  @SerializedName("dataType")
  private String dataType;
  @Expose
  @SerializedName("description")
  private Object description;
  @Expose
  @SerializedName("format")
  private Object format;
  @Expose
  @SerializedName("freeValue")
  private Integer freeValue;
  @Expose
  @SerializedName("maxValue")
  private Integer maxValue;
  @Expose
  @SerializedName("minValue")
  private Integer minValue;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("required")
  private Boolean required;
  @Expose
  @SerializedName("validValues")
  private Object validValues;
  @Expose
  @SerializedName("value")
  private Object value;
  
  public InputParameter() {}
  
  public InputParameter(String paramString1, String paramString2, Object paramObject1, Object paramObject2, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, String paramString3, Boolean paramBoolean, Object paramObject3, Object paramObject4)
  {
    this.brandedName = paramString1;
    this.dataType = paramString2;
    this.description = paramObject1;
    this.format = paramObject2;
    this.freeValue = paramInteger1;
    this.maxValue = paramInteger2;
    this.minValue = paramInteger3;
    this.name = paramString3;
    this.required = paramBoolean;
    this.validValues = paramObject3;
    this.value = paramObject4;
  }
  
  public String getBrandedName()
  {
    return this.brandedName;
  }
  
  public String getDataType()
  {
    return this.dataType;
  }
  
  public Object getDescription()
  {
    return this.description;
  }
  
  public Object getFormat()
  {
    return this.format;
  }
  
  public Integer getFreeValue()
  {
    return this.freeValue;
  }
  
  public Integer getMaxValue()
  {
    return this.maxValue;
  }
  
  public Integer getMinValue()
  {
    return this.minValue;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public Boolean getRequired()
  {
    return this.required;
  }
  
  public Object getValidValues()
  {
    return this.validValues;
  }
  
  public Object getValue()
  {
    return this.value;
  }
  
  public void setBrandedName(String paramString)
  {
    this.brandedName = paramString;
  }
  
  public void setDataType(String paramString)
  {
    this.dataType = paramString;
  }
  
  public void setDescription(Object paramObject)
  {
    this.description = paramObject;
  }
  
  public void setFormat(Object paramObject)
  {
    this.format = paramObject;
  }
  
  public void setFreeValue(Integer paramInteger)
  {
    this.freeValue = paramInteger;
  }
  
  public void setMaxValue(Integer paramInteger)
  {
    this.maxValue = paramInteger;
  }
  
  public void setMinValue(Integer paramInteger)
  {
    this.minValue = paramInteger;
  }
  
  public void setName(String paramString)
  {
    this.name = paramString;
  }
  
  public void setRequired(Boolean paramBoolean)
  {
    this.required = paramBoolean;
  }
  
  public void setValidValues(Object paramObject)
  {
    this.validValues = paramObject;
  }
  
  public void setValue(Object paramObject)
  {
    this.value = paramObject;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\InputParameter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */