package org.jsoup.parser;

import java.util.Arrays;
import java.util.Locale;
import org.jsoup.helper.Validate;

public final class CharacterReader
{
  static final char EOF = '￿';
  private static final int maxCacheLen = 12;
  private final char[] input;
  private final int length;
  private int mark = 0;
  private int pos = 0;
  private final String[] stringCache = new String['Ȁ'];
  
  public CharacterReader(String paramString)
  {
    Validate.notNull(paramString);
    this.input = paramString.toCharArray();
    this.length = this.input.length;
  }
  
  private String cacheString(int paramInt1, int paramInt2)
  {
    char[] arrayOfChar = this.input;
    String[] arrayOfString = this.stringCache;
    Object localObject;
    if (paramInt2 > 12) {
      localObject = new String(arrayOfChar, paramInt1, paramInt2);
    }
    for (;;)
    {
      return (String)localObject;
      int k = 0;
      int j = 0;
      for (int i = paramInt1; j < paramInt2; i++)
      {
        k = k * 31 + arrayOfChar[i];
        j++;
      }
      i = k & arrayOfString.length - 1;
      String str = arrayOfString[i];
      if (str == null)
      {
        localObject = new String(arrayOfChar, paramInt1, paramInt2);
        arrayOfString[i] = localObject;
      }
      else
      {
        localObject = str;
        if (!rangeEquals(paramInt1, paramInt2, str))
        {
          localObject = new String(arrayOfChar, paramInt1, paramInt2);
          arrayOfString[i] = localObject;
        }
      }
    }
  }
  
  public void advance()
  {
    this.pos += 1;
  }
  
  char consume()
  {
    if (this.pos >= this.length) {}
    int j;
    for (int i = 65535;; j = this.input[this.pos])
    {
      this.pos += 1;
      return i;
    }
  }
  
  String consumeAsString()
  {
    char[] arrayOfChar = this.input;
    int i = this.pos;
    this.pos = (i + 1);
    return new String(arrayOfChar, i, 1);
  }
  
  String consumeData()
  {
    int j = this.pos;
    int i = this.length;
    Object localObject = this.input;
    if (this.pos < i)
    {
      int k = localObject[this.pos];
      if ((k != 38) && (k != 60) && (k != 0)) {}
    }
    else
    {
      if (this.pos <= j) {
        break label85;
      }
    }
    label85:
    for (localObject = cacheString(j, this.pos - j);; localObject = "")
    {
      return (String)localObject;
      this.pos += 1;
      break;
    }
  }
  
  String consumeDigitSequence()
  {
    int j = this.pos;
    while (this.pos < this.length)
    {
      int i = this.input[this.pos];
      if ((i < 48) || (i > 57)) {
        break;
      }
      this.pos += 1;
    }
    return cacheString(j, this.pos - j);
  }
  
  String consumeHexSequence()
  {
    int i = this.pos;
    while (this.pos < this.length)
    {
      int j = this.input[this.pos];
      if (((j < 48) || (j > 57)) && ((j < 65) || (j > 70)) && ((j < 97) || (j > 102))) {
        break;
      }
      this.pos += 1;
    }
    return cacheString(i, this.pos - i);
  }
  
  String consumeLetterSequence()
  {
    int i = this.pos;
    while (this.pos < this.length)
    {
      char c = this.input[this.pos];
      if (((c < 'A') || (c > 'Z')) && ((c < 'a') || (c > 'z')) && (!Character.isLetter(c))) {
        break;
      }
      this.pos += 1;
    }
    return cacheString(i, this.pos - i);
  }
  
  String consumeLetterThenDigitSequence()
  {
    int j = this.pos;
    while (this.pos < this.length)
    {
      char c = this.input[this.pos];
      if (((c < 'A') || (c > 'Z')) && ((c < 'a') || (c > 'z')) && (!Character.isLetter(c))) {
        break;
      }
      this.pos += 1;
    }
    while (!isEmpty())
    {
      int i = this.input[this.pos];
      if ((i < 48) || (i > 57)) {
        break;
      }
      this.pos += 1;
    }
    return cacheString(j, this.pos - j);
  }
  
  String consumeTagName()
  {
    int k = this.pos;
    int j = this.length;
    Object localObject = this.input;
    if (this.pos < j)
    {
      int i = localObject[this.pos];
      if ((i != 9) && (i != 10) && (i != 13) && (i != 12) && (i != 32) && (i != 47) && (i != 62) && (i != 0)) {}
    }
    else
    {
      if (this.pos <= k) {
        break label115;
      }
    }
    label115:
    for (localObject = cacheString(k, this.pos - k);; localObject = "")
    {
      return (String)localObject;
      this.pos += 1;
      break;
    }
  }
  
  public String consumeTo(char paramChar)
  {
    int i = nextIndexOf(paramChar);
    String str;
    if (i != -1)
    {
      str = cacheString(this.pos, i);
      this.pos += i;
    }
    for (;;)
    {
      return str;
      str = consumeToEnd();
    }
  }
  
  String consumeTo(String paramString)
  {
    int i = nextIndexOf(paramString);
    if (i != -1)
    {
      paramString = cacheString(this.pos, i);
      this.pos += i;
    }
    for (;;)
    {
      return paramString;
      paramString = consumeToEnd();
    }
  }
  
  public String consumeToAny(char... paramVarArgs)
  {
    int m = this.pos;
    int k = this.length;
    char[] arrayOfChar = this.input;
    int i;
    if (this.pos < k)
    {
      int j = paramVarArgs.length;
      i = 0;
      label32:
      if (i >= j) {
        break label85;
      }
      int n = paramVarArgs[i];
      if (arrayOfChar[this.pos] != n) {}
    }
    else
    {
      if (this.pos <= m) {
        break label98;
      }
    }
    label85:
    label98:
    for (paramVarArgs = cacheString(m, this.pos - m);; paramVarArgs = "")
    {
      return paramVarArgs;
      i++;
      break label32;
      this.pos += 1;
      break;
    }
  }
  
  String consumeToAnySorted(char... paramVarArgs)
  {
    int i = this.pos;
    int j = this.length;
    char[] arrayOfChar = this.input;
    if ((this.pos >= j) || (Arrays.binarySearch(paramVarArgs, arrayOfChar[this.pos]) >= 0)) {
      if (this.pos <= i) {
        break label73;
      }
    }
    label73:
    for (paramVarArgs = cacheString(i, this.pos - i);; paramVarArgs = "")
    {
      return paramVarArgs;
      this.pos += 1;
      break;
    }
  }
  
  String consumeToEnd()
  {
    String str = cacheString(this.pos, this.length - this.pos);
    this.pos = this.length;
    return str;
  }
  
  boolean containsIgnoreCase(String paramString)
  {
    String str = paramString.toLowerCase(Locale.ENGLISH);
    paramString = paramString.toUpperCase(Locale.ENGLISH);
    if ((nextIndexOf(str) > -1) || (nextIndexOf(paramString) > -1)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public char current()
  {
    if (this.pos >= this.length) {}
    int j;
    for (int i = 65535;; j = this.input[this.pos]) {
      return i;
    }
  }
  
  public boolean isEmpty()
  {
    if (this.pos >= this.length) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  void mark()
  {
    this.mark = this.pos;
  }
  
  boolean matchConsume(String paramString)
  {
    if (matches(paramString)) {
      this.pos += paramString.length();
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean matchConsumeIgnoreCase(String paramString)
  {
    if (matchesIgnoreCase(paramString)) {
      this.pos += paramString.length();
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean matches(char paramChar)
  {
    if ((!isEmpty()) && (this.input[this.pos] == paramChar)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean matches(String paramString)
  {
    boolean bool2 = false;
    int j = paramString.length();
    boolean bool1;
    if (j > this.length - this.pos) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      for (int i = 0;; i++)
      {
        if (i >= j) {
          break label64;
        }
        bool1 = bool2;
        if (paramString.charAt(i) != this.input[(this.pos + i)]) {
          break;
        }
      }
      label64:
      bool1 = true;
    }
  }
  
  boolean matchesAny(char... paramVarArgs)
  {
    boolean bool2 = false;
    boolean bool1;
    if (isEmpty())
    {
      bool1 = bool2;
      return bool1;
    }
    int j = this.input[this.pos];
    int k = paramVarArgs.length;
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= k) {
        break;
      }
      if (paramVarArgs[i] == j)
      {
        bool1 = true;
        break;
      }
    }
  }
  
  boolean matchesAnySorted(char[] paramArrayOfChar)
  {
    if ((!isEmpty()) && (Arrays.binarySearch(paramArrayOfChar, this.input[this.pos]) >= 0)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean matchesDigit()
  {
    boolean bool2 = false;
    boolean bool1;
    if (isEmpty()) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      int i = this.input[this.pos];
      bool1 = bool2;
      if (i >= 48)
      {
        bool1 = bool2;
        if (i <= 57) {
          bool1 = true;
        }
      }
    }
  }
  
  boolean matchesIgnoreCase(String paramString)
  {
    boolean bool2 = false;
    int j = paramString.length();
    boolean bool1;
    if (j > this.length - this.pos) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      for (int i = 0;; i++)
      {
        if (i >= j) {
          break label70;
        }
        bool1 = bool2;
        if (Character.toUpperCase(paramString.charAt(i)) != Character.toUpperCase(this.input[(this.pos + i)])) {
          break;
        }
      }
      label70:
      bool1 = true;
    }
  }
  
  boolean matchesLetter()
  {
    boolean bool = false;
    if (isEmpty()) {}
    for (;;)
    {
      return bool;
      char c = this.input[this.pos];
      if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || (Character.isLetter(c))) {
        bool = true;
      }
    }
  }
  
  int nextIndexOf(char paramChar)
  {
    int i = this.pos;
    if (i < this.length) {
      if (paramChar != this.input[i]) {}
    }
    for (paramChar = i - this.pos;; paramChar = '￿')
    {
      return paramChar;
      i++;
      break;
    }
  }
  
  int nextIndexOf(CharSequence paramCharSequence)
  {
    int m = paramCharSequence.charAt(0);
    int i = this.pos;
    int j;
    if (i < this.length)
    {
      j = i;
      if (m != this.input[i])
      {
        j = i;
        do
        {
          i = j + 1;
          j = i;
          if (i >= this.length) {
            break;
          }
          j = i;
        } while (m != this.input[i]);
        j = i;
      }
      int k = j + 1;
      int n = paramCharSequence.length() + k - 1;
      if ((j < this.length) && (n <= this.length))
      {
        for (i = 1; (k < n) && (paramCharSequence.charAt(i) == this.input[k]); i++) {
          k++;
        }
        if (k != n) {}
      }
    }
    for (i = j - this.pos;; i = -1)
    {
      return i;
      i = j + 1;
      break;
    }
  }
  
  public int pos()
  {
    return this.pos;
  }
  
  boolean rangeEquals(int paramInt1, int paramInt2, String paramString)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    char[] arrayOfChar;
    int i;
    if (paramInt2 == paramString.length())
    {
      arrayOfChar = this.input;
      i = 0;
    }
    for (;;)
    {
      if (paramInt2 != 0)
      {
        if (arrayOfChar[paramInt1] == paramString.charAt(i)) {}
      }
      else {
        for (bool1 = bool2;; bool1 = true) {
          return bool1;
        }
      }
      i++;
      paramInt1++;
      paramInt2--;
    }
  }
  
  void rewindToMark()
  {
    this.pos = this.mark;
  }
  
  public String toString()
  {
    return new String(this.input, this.pos, this.length - this.pos);
  }
  
  void unconsume()
  {
    this.pos -= 1;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\CharacterReader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */