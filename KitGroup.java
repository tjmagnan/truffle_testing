package io.fabric.sdk.android;

import java.util.Collection;

public abstract interface KitGroup
{
  public abstract Collection<? extends Kit> getKits();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\KitGroup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */