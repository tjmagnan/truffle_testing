package org.jsoup.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public final class DataUtil
{
  static final int boundaryLength = 32;
  private static final int bufferSize = 60000;
  private static final Pattern charsetPattern = Pattern.compile("(?i)\\bcharset=\\s*(?:\"|')?([^\\s,;\"']*)");
  static final String defaultCharset = "UTF-8";
  private static final char[] mimeBoundaryChars = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  
  static void crossStreams(InputStream paramInputStream, OutputStream paramOutputStream)
    throws IOException
  {
    byte[] arrayOfByte = new byte[60000];
    for (;;)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1) {
        break;
      }
      paramOutputStream.write(arrayOfByte, 0, i);
    }
  }
  
  private static String detectCharsetFromBom(ByteBuffer paramByteBuffer, String paramString)
  {
    paramByteBuffer.mark();
    byte[] arrayOfByte = new byte[4];
    if (paramByteBuffer.remaining() >= arrayOfByte.length)
    {
      paramByteBuffer.get(arrayOfByte);
      paramByteBuffer.rewind();
    }
    String str;
    if (((arrayOfByte[0] == 0) && (arrayOfByte[1] == 0) && (arrayOfByte[2] == -2) && (arrayOfByte[3] == -1)) || ((arrayOfByte[0] == -1) && (arrayOfByte[1] == -2) && (arrayOfByte[2] == 0) && (arrayOfByte[3] == 0))) {
      str = "UTF-32";
    }
    for (;;)
    {
      return str;
      if (((arrayOfByte[0] == -2) && (arrayOfByte[1] == -1)) || ((arrayOfByte[0] == -1) && (arrayOfByte[1] == -2)))
      {
        str = "UTF-16";
      }
      else
      {
        str = paramString;
        if (arrayOfByte[0] == -17)
        {
          str = paramString;
          if (arrayOfByte[1] == -69)
          {
            str = paramString;
            if (arrayOfByte[2] == -65)
            {
              str = "UTF-8";
              paramByteBuffer.position(3);
            }
          }
        }
      }
    }
  }
  
  static ByteBuffer emptyByteBuffer()
  {
    return ByteBuffer.allocate(0);
  }
  
  static String getCharsetFromContentType(String paramString)
  {
    Object localObject = null;
    if (paramString == null) {
      paramString = (String)localObject;
    }
    for (;;)
    {
      return paramString;
      Matcher localMatcher = charsetPattern.matcher(paramString);
      paramString = (String)localObject;
      if (localMatcher.find()) {
        paramString = validateCharset(localMatcher.group(1).trim().replace("charset=", ""));
      }
    }
  }
  
  public static Document load(File paramFile, String paramString1, String paramString2)
    throws IOException
  {
    return parseByteData(readFileToByteBuffer(paramFile), paramString1, paramString2, Parser.htmlParser());
  }
  
  public static Document load(InputStream paramInputStream, String paramString1, String paramString2)
    throws IOException
  {
    return parseByteData(readToByteBuffer(paramInputStream), paramString1, paramString2, Parser.htmlParser());
  }
  
  public static Document load(InputStream paramInputStream, String paramString1, String paramString2, Parser paramParser)
    throws IOException
  {
    return parseByteData(readToByteBuffer(paramInputStream), paramString1, paramString2, paramParser);
  }
  
  static String mimeBoundary()
  {
    StringBuilder localStringBuilder = new StringBuilder(32);
    Random localRandom = new Random();
    for (int i = 0; i < 32; i++) {
      localStringBuilder.append(mimeBoundaryChars[localRandom.nextInt(mimeBoundaryChars.length)]);
    }
    return localStringBuilder.toString();
  }
  
  static Document parseByteData(ByteBuffer paramByteBuffer, String paramString1, String paramString2, Parser paramParser)
  {
    Object localObject1 = null;
    String str1 = detectCharsetFromBom(paramByteBuffer, paramString1);
    Object localObject2;
    if (str1 == null)
    {
      String str2 = Charset.forName("UTF-8").decode(paramByteBuffer).toString();
      Document localDocument = paramParser.parseInput(str2, paramString2);
      localObject2 = localDocument.select("meta[http-equiv=content-type], meta[charset]").first();
      paramString1 = null;
      localObject1 = null;
      if (localObject2 != null)
      {
        if (((Element)localObject2).hasAttr("http-equiv")) {
          localObject1 = getCharsetFromContentType(((Element)localObject2).attr("content"));
        }
        paramString1 = (String)localObject1;
        if (localObject1 == null)
        {
          paramString1 = (String)localObject1;
          if (((Element)localObject2).hasAttr("charset")) {
            paramString1 = ((Element)localObject2).attr("charset");
          }
        }
      }
      localObject1 = paramString1;
      if (paramString1 == null)
      {
        localObject1 = paramString1;
        if (localDocument.childNodeSize() > 0)
        {
          localObject1 = paramString1;
          if ((localDocument.childNode(0) instanceof XmlDeclaration))
          {
            localObject2 = (XmlDeclaration)localDocument.childNode(0);
            localObject1 = paramString1;
            if (((XmlDeclaration)localObject2).name().equals("xml")) {
              localObject1 = ((XmlDeclaration)localObject2).attr("encoding");
            }
          }
        }
      }
      String str3 = validateCharset((String)localObject1);
      paramString1 = localDocument;
      localObject1 = str2;
      localObject2 = str1;
      if (str3 != null)
      {
        paramString1 = localDocument;
        localObject1 = str2;
        localObject2 = str1;
        if (!str3.equals("UTF-8"))
        {
          paramString1 = str3.trim().replaceAll("[\"']", "");
          localObject2 = paramString1;
          paramByteBuffer.rewind();
          localObject1 = Charset.forName(paramString1).decode(paramByteBuffer).toString();
          paramString1 = null;
        }
      }
    }
    for (;;)
    {
      paramByteBuffer = paramString1;
      if (paramString1 == null)
      {
        paramByteBuffer = paramParser.parseInput((String)localObject1, paramString2);
        paramByteBuffer.outputSettings().charset((String)localObject2);
      }
      return paramByteBuffer;
      Validate.notEmpty(str1, "Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
      paramByteBuffer = Charset.forName(str1).decode(paramByteBuffer).toString();
      paramString1 = (String)localObject1;
      localObject1 = paramByteBuffer;
      localObject2 = str1;
    }
  }
  
  static ByteBuffer readFileToByteBuffer(File paramFile)
    throws IOException
  {
    Object localObject2 = null;
    Object localObject1;
    try
    {
      localObject1 = new java/io/RandomAccessFile;
      ((RandomAccessFile)localObject1).<init>(paramFile, "r");
      if (localObject1 == null) {
        break label53;
      }
    }
    finally
    {
      try
      {
        paramFile = new byte[(int)((RandomAccessFile)localObject1).length()];
        ((RandomAccessFile)localObject1).readFully(paramFile);
        paramFile = ByteBuffer.wrap(paramFile);
        if (localObject1 != null) {
          ((RandomAccessFile)localObject1).close();
        }
        return paramFile;
      }
      finally {}
      paramFile = finally;
      localObject1 = localObject2;
    }
    ((RandomAccessFile)localObject1).close();
    label53:
    throw paramFile;
  }
  
  static ByteBuffer readToByteBuffer(InputStream paramInputStream)
    throws IOException
  {
    return readToByteBuffer(paramInputStream, 0);
  }
  
  public static ByteBuffer readToByteBuffer(InputStream paramInputStream, int paramInt)
    throws IOException
  {
    int k = 60000;
    boolean bool;
    int i;
    label25:
    label37:
    byte[] arrayOfByte;
    ByteArrayOutputStream localByteArrayOutputStream;
    if (paramInt >= 0)
    {
      bool = true;
      Validate.isTrue(bool, "maxSize must be 0 (unlimited) or larger");
      if (paramInt <= 0) {
        break label98;
      }
      i = 1;
      if ((i == 0) || (paramInt >= 60000)) {
        break label103;
      }
      j = paramInt;
      arrayOfByte = new byte[j];
      j = k;
      if (i != 0) {
        j = paramInt;
      }
      localByteArrayOutputStream = new ByteArrayOutputStream(j);
    }
    for (int j = paramInt;; j = paramInt)
    {
      if (!Thread.interrupted())
      {
        k = paramInputStream.read(arrayOfByte);
        if (k != -1) {
          break label109;
        }
      }
      for (;;)
      {
        return ByteBuffer.wrap(localByteArrayOutputStream.toByteArray());
        bool = false;
        break;
        label98:
        i = 0;
        break label25;
        label103:
        j = 60000;
        break label37;
        label109:
        paramInt = j;
        if (i == 0) {
          break label138;
        }
        if (k <= j) {
          break label133;
        }
        localByteArrayOutputStream.write(arrayOfByte, 0, j);
      }
      label133:
      paramInt = j - k;
      label138:
      localByteArrayOutputStream.write(arrayOfByte, 0, k);
    }
  }
  
  private static String validateCharset(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0)) {}
    for (paramString = null;; paramString = null) {
      for (;;)
      {
        return paramString;
        String str = paramString.trim().replaceAll("[\"']", "");
        paramString = str;
        try
        {
          if (!Charset.isSupported(str))
          {
            paramString = str.toUpperCase(Locale.ENGLISH);
            boolean bool = Charset.isSupported(paramString);
            if (bool) {}
          }
        }
        catch (IllegalCharsetNameException paramString)
        {
          for (;;) {}
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\helper\DataUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */