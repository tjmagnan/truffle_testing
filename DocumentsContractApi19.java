package android.support.v4.provider;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.DocumentsContract;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;

@TargetApi(19)
@RequiresApi(19)
class DocumentsContractApi19
{
  private static final int FLAG_VIRTUAL_DOCUMENT = 512;
  private static final String TAG = "DocumentFile";
  
  public static boolean canRead(Context paramContext, Uri paramUri)
  {
    boolean bool = false;
    if (paramContext.checkCallingOrSelfUriPermission(paramUri, 1) != 0) {}
    for (;;)
    {
      return bool;
      if (!TextUtils.isEmpty(getRawType(paramContext, paramUri))) {
        bool = true;
      }
    }
  }
  
  public static boolean canWrite(Context paramContext, Uri paramUri)
  {
    boolean bool2 = false;
    boolean bool1;
    if (paramContext.checkCallingOrSelfUriPermission(paramUri, 2) != 0) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      String str = getRawType(paramContext, paramUri);
      int i = queryForInt(paramContext, paramUri, "flags", 0);
      bool1 = bool2;
      if (!TextUtils.isEmpty(str)) {
        if ((i & 0x4) != 0)
        {
          bool1 = true;
        }
        else if (("vnd.android.document/directory".equals(str)) && ((i & 0x8) != 0))
        {
          bool1 = true;
        }
        else
        {
          bool1 = bool2;
          if (!TextUtils.isEmpty(str))
          {
            bool1 = bool2;
            if ((i & 0x2) != 0) {
              bool1 = true;
            }
          }
        }
      }
    }
  }
  
  private static void closeQuietly(AutoCloseable paramAutoCloseable)
  {
    if (paramAutoCloseable != null) {}
    try
    {
      paramAutoCloseable.close();
      return;
    }
    catch (RuntimeException paramAutoCloseable)
    {
      throw paramAutoCloseable;
    }
    catch (Exception paramAutoCloseable)
    {
      for (;;) {}
    }
  }
  
  public static boolean delete(Context paramContext, Uri paramUri)
  {
    return DocumentsContract.deleteDocument(paramContext.getContentResolver(), paramUri);
  }
  
  public static boolean exists(Context paramContext, Uri paramUri)
  {
    localObject2 = paramContext.getContentResolver();
    localObject1 = null;
    paramContext = null;
    for (;;)
    {
      try
      {
        paramUri = ((ContentResolver)localObject2).query(paramUri, new String[] { "document_id" }, null, null, null);
        paramContext = paramUri;
        localObject1 = paramUri;
        int i = paramUri.getCount();
        if (i <= 0) {
          continue;
        }
        bool = true;
        closeQuietly(paramUri);
      }
      catch (Exception paramUri)
      {
        localObject1 = paramContext;
        localObject2 = new java/lang/StringBuilder;
        localObject1 = paramContext;
        ((StringBuilder)localObject2).<init>();
        localObject1 = paramContext;
        Log.w("DocumentFile", "Failed query: " + paramUri);
        closeQuietly(paramContext);
        boolean bool = false;
        continue;
      }
      finally
      {
        closeQuietly((AutoCloseable)localObject1);
      }
      return bool;
      bool = false;
    }
  }
  
  public static long getFlags(Context paramContext, Uri paramUri)
  {
    return queryForLong(paramContext, paramUri, "flags", 0L);
  }
  
  public static String getName(Context paramContext, Uri paramUri)
  {
    return queryForString(paramContext, paramUri, "_display_name", null);
  }
  
  private static String getRawType(Context paramContext, Uri paramUri)
  {
    return queryForString(paramContext, paramUri, "mime_type", null);
  }
  
  public static String getType(Context paramContext, Uri paramUri)
  {
    paramUri = getRawType(paramContext, paramUri);
    paramContext = paramUri;
    if ("vnd.android.document/directory".equals(paramUri)) {
      paramContext = null;
    }
    return paramContext;
  }
  
  public static boolean isDirectory(Context paramContext, Uri paramUri)
  {
    return "vnd.android.document/directory".equals(getRawType(paramContext, paramUri));
  }
  
  public static boolean isDocumentUri(Context paramContext, Uri paramUri)
  {
    return DocumentsContract.isDocumentUri(paramContext, paramUri);
  }
  
  public static boolean isFile(Context paramContext, Uri paramUri)
  {
    paramContext = getRawType(paramContext, paramUri);
    if (("vnd.android.document/directory".equals(paramContext)) || (TextUtils.isEmpty(paramContext))) {}
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  public static boolean isVirtual(Context paramContext, Uri paramUri)
  {
    boolean bool = false;
    if (!isDocumentUri(paramContext, paramUri)) {}
    for (;;)
    {
      return bool;
      if ((getFlags(paramContext, paramUri) & 0x200) != 0L) {
        bool = true;
      }
    }
  }
  
  public static long lastModified(Context paramContext, Uri paramUri)
  {
    return queryForLong(paramContext, paramUri, "last_modified", 0L);
  }
  
  public static long length(Context paramContext, Uri paramUri)
  {
    return queryForLong(paramContext, paramUri, "_size", 0L);
  }
  
  private static int queryForInt(Context paramContext, Uri paramUri, String paramString, int paramInt)
  {
    return (int)queryForLong(paramContext, paramUri, paramString, paramInt);
  }
  
  private static long queryForLong(Context paramContext, Uri paramUri, String paramString, long paramLong)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    localObject = null;
    paramContext = null;
    for (;;)
    {
      try
      {
        paramUri = localContentResolver.query(paramUri, new String[] { paramString }, null, null, null);
        paramContext = paramUri;
        localObject = paramUri;
        if (!paramUri.moveToFirst()) {
          continue;
        }
        paramContext = paramUri;
        localObject = paramUri;
        if (paramUri.isNull(0)) {
          continue;
        }
        paramContext = paramUri;
        localObject = paramUri;
        long l = paramUri.getLong(0);
        paramLong = l;
        closeQuietly(paramUri);
      }
      catch (Exception paramString)
      {
        localObject = paramContext;
        paramUri = new java/lang/StringBuilder;
        localObject = paramContext;
        paramUri.<init>();
        localObject = paramContext;
        Log.w("DocumentFile", "Failed query: " + paramString);
        closeQuietly(paramContext);
        continue;
      }
      finally
      {
        closeQuietly((AutoCloseable)localObject);
      }
      return paramLong;
      closeQuietly(paramUri);
    }
  }
  
  private static String queryForString(Context paramContext, Uri paramUri, String paramString1, String paramString2)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    paramContext = null;
    localUri = null;
    for (;;)
    {
      try
      {
        paramUri = localContentResolver.query(paramUri, new String[] { paramString1 }, null, null, null);
        localUri = paramUri;
        paramContext = paramUri;
        if (!paramUri.moveToFirst()) {
          continue;
        }
        localUri = paramUri;
        paramContext = paramUri;
        if (paramUri.isNull(0)) {
          continue;
        }
        localUri = paramUri;
        paramContext = paramUri;
        paramString1 = paramUri.getString(0);
        paramContext = paramString1;
      }
      catch (Exception paramString1)
      {
        paramContext = localUri;
        paramUri = new java/lang/StringBuilder;
        paramContext = localUri;
        paramUri.<init>();
        paramContext = localUri;
        Log.w("DocumentFile", "Failed query: " + paramString1);
        closeQuietly(localUri);
        paramContext = paramString2;
        continue;
      }
      finally
      {
        closeQuietly(paramContext);
      }
      return paramContext;
      closeQuietly(paramUri);
      paramContext = paramString2;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\provider\DocumentsContractApi19.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */