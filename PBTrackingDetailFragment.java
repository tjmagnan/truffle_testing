package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import tech.dcube.companion.customListAdapters.OrderStatusLogListAdapter;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBShipmentTracking;
import tech.dcube.companion.model.PBShipmentTrackingDetail;
import tech.dcube.companion.model.PBTrackingDetails;

public class PBTrackingDetailFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String ARG_PARAM3 = "param3";
  private static final String ARG_PARAM4 = "param4";
  private String Tag;
  TextView deliveryTime;
  boolean isStatusLogHistoryVisible;
  private OnShipmentTrackDetailListener mListener;
  String mOrderID;
  private int mTrackingPosition;
  boolean mTrackingQuery = false;
  TextView orderAddress;
  TextView orderRecipient;
  TextView orderStatus;
  OrderStatusLogListAdapter orderStatusLogListAdapter;
  LinearLayout printOrderSummary;
  TextView remainingTime;
  TextView shippingPrice;
  LinearLayout showAddressOnMap;
  RelativeLayout showStatusLogHistory;
  LinearLayout showStatusReport;
  List<PBTrackingDetails> statusLogArrayList;
  ListView statusLogListView;
  
  public static PBTrackingDetailFragment newInstance(int paramInt, String paramString1, boolean paramBoolean, String paramString2)
  {
    PBTrackingDetailFragment localPBTrackingDetailFragment = new PBTrackingDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putInt("param1", paramInt);
    localBundle.putString("param2", paramString2);
    localBundle.putString("param3", paramString1);
    localBundle.putBoolean("param4", paramBoolean);
    localPBTrackingDetailFragment.setArguments(localBundle);
    return localPBTrackingDetailFragment;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnShipmentTrackDetailListener))
    {
      this.mListener = ((OnShipmentTrackDetailListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement");
  }
  
  public void onBackButtonPressed()
  {
    if (this.mListener != null) {
      this.mListener.onTrackingDetailInteraction("action_back_button");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    onBackButtonPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mTrackingPosition = getArguments().getInt("param1", -1);
      this.Tag = getArguments().getString("param2");
      this.mOrderID = getArguments().getString("param3", "");
      this.mTrackingQuery = getArguments().getBoolean("param4", false);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramViewGroup = paramLayoutInflater.inflate(2130968650, paramViewGroup, false);
    this.showStatusReport = ((LinearLayout)paramViewGroup.findViewById(2131624235));
    this.showAddressOnMap = ((LinearLayout)paramViewGroup.findViewById(2131624238));
    this.printOrderSummary = ((LinearLayout)paramViewGroup.findViewById(2131624246));
    this.orderStatus = ((TextView)paramViewGroup.findViewById(2131624236));
    this.orderRecipient = ((TextView)paramViewGroup.findViewById(2131624240));
    this.orderAddress = ((TextView)paramViewGroup.findViewById(2131624241));
    this.deliveryTime = ((TextView)paramViewGroup.findViewById(2131624243));
    this.remainingTime = ((TextView)paramViewGroup.findViewById(2131624244));
    this.shippingPrice = ((TextView)paramViewGroup.findViewById(2131624245));
    this.showStatusLogHistory = ((RelativeLayout)paramViewGroup.findViewById(2131624247));
    this.statusLogListView = ((ListView)paramViewGroup.findViewById(2131624249));
    this.showStatusLogHistory.setVisibility(0);
    this.isStatusLogHistoryVisible = true;
    if (!this.mTrackingQuery) {
      paramLayoutInflater = ((PBShipmentTracking)DataManager.getInstance().getTrackings().get(this.mTrackingPosition)).getShipmentDetail();
    }
    for (this.statusLogArrayList = ((PBShipmentTracking)DataManager.getInstance().getTrackings().get(this.mTrackingPosition)).getShipmentDetail().getTrackingDetails();; this.statusLogArrayList = DataManager.getInstance().getQueryTrackingDetail().getTrackingDetails())
    {
      paramLayoutInflater = paramLayoutInflater.getPackageIdentifier();
      ButterKnife.bind(this, paramViewGroup);
      hideTopBarRightButton();
      setTopBarTitle(paramLayoutInflater);
      setTopBarLeftButtonText("Back");
      Collections.sort(this.statusLogArrayList, new Comparator()
      {
        public int compare(PBTrackingDetails paramAnonymousPBTrackingDetails1, PBTrackingDetails paramAnonymousPBTrackingDetails2)
        {
          return paramAnonymousPBTrackingDetails2.getEventDate().compareTo(paramAnonymousPBTrackingDetails1.getEventDate());
        }
      });
      this.orderStatusLogListAdapter = new OrderStatusLogListAdapter(this.statusLogArrayList, getContext());
      this.statusLogListView.setAdapter(this.orderStatusLogListAdapter);
      this.showStatusReport.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          PBTrackingDetailFragment.this.isStatusLogHistoryVisible = true;
          PBTrackingDetailFragment.this.showStatusLogHistory.setVisibility(0);
        }
      });
      this.showAddressOnMap.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView) {}
      });
      this.printOrderSummary.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView) {}
      });
      return paramViewGroup;
      paramLayoutInflater = DataManager.getInstance().getQueryTrackingDetail();
    }
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public static abstract interface OnShipmentTrackDetailListener
  {
    public abstract void onTrackingDetailInteraction(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBTrackingDetailFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */