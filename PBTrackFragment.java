package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.android.volley.VolleyError;
import java.util.ArrayList;
import java.util.List;
import tech.dcube.companion.customListAdapters.TrackListAdapter;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBShipmentTracking;
import tech.dcube.companion.model.PBShipmentTrackingDetail;

public class PBTrackFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static TrackListAdapter adapter;
  RelativeLayout layoutTrackList;
  RelativeLayout layoutTrackListEmpty;
  private FrameLayout mFadeFL;
  private OnTrackingListener mListener;
  private String mParam1;
  private String mParam2;
  private ProgressBar mProgressBar;
  TextView numCurrentShipments;
  int positionShipmentTrackingList;
  SearchView searchView;
  List<PBShipmentTracking> shipmentTrackingList;
  ListView trackList;
  boolean waitingState = false;
  
  private void hideTrackListLayout()
  {
    this.layoutTrackListEmpty.setVisibility(8);
    this.layoutTrackList.setVisibility(8);
  }
  
  public static PBTrackFragment newInstance(String paramString1, String paramString2)
  {
    PBTrackFragment localPBTrackFragment = new PBTrackFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBTrackFragment.setArguments(localBundle);
    return localPBTrackFragment;
  }
  
  private void updateTracklistLayout(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.layoutTrackListEmpty.setVisibility(0);
      this.layoutTrackList.setVisibility(8);
    }
    for (;;)
    {
      return;
      this.numCurrentShipments.setText("" + this.shipmentTrackingList.size());
      this.layoutTrackListEmpty.setVisibility(8);
      this.layoutTrackList.setVisibility(0);
    }
  }
  
  void callGetTrackingDetailApi_Query(PBShipmentTracking paramPBShipmentTracking)
  {
    showLoadingOverlay();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local6 = new tech/dcube/companion/fragments/PBTrackFragment$6;
      local6.<init>(this);
      localServerManager.getTrackingDetail(localContext, paramPBShipmentTracking, local6);
      return;
    }
    catch (Exception paramPBShipmentTracking)
    {
      for (;;)
      {
        paramPBShipmentTracking.printStackTrace();
        hideLoadingOverlay();
      }
    }
  }
  
  void callGetTrackingListApi()
  {
    showLoadingOverlay();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local5 = new tech/dcube/companion/fragments/PBTrackFragment$5;
      local5.<init>(this);
      localServerManager.getTrackingsList(localContext, local5);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        hideLoadingOverlay();
      }
    }
  }
  
  void callTrackingIDQueryApi(String paramString)
  {
    showLoadingOverlay();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local7 = new tech/dcube/companion/fragments/PBTrackFragment$7;
      local7.<init>(this);
      localServerManager.trackShipmentById(localContext, paramString, local7);
      return;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        Log.d("Tracking Query: ", "Failed");
        paramString.printStackTrace();
        hideLoadingOverlay();
      }
    }
  }
  
  public void callTrackingItemApi(int paramInt)
  {
    if (this.waitingState) {}
    for (;;)
    {
      return;
      this.waitingState = true;
      this.positionShipmentTrackingList = paramInt;
      this.waitingState = true;
      showLoadingOverlay();
      try
      {
        ServerManager localServerManager = ServerManager.getInstance();
        Context localContext = getContext();
        PBShipmentTracking localPBShipmentTracking = (PBShipmentTracking)this.shipmentTrackingList.get(paramInt);
        ServerManager.RequestCompletionHandler local8 = new tech/dcube/companion/fragments/PBTrackFragment$8;
        local8.<init>(this);
        localServerManager.getTrackingDetail(localContext, localPBShipmentTracking, local8);
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        hideLoadingOverlay();
      }
    }
  }
  
  void clearTrackingQuery()
  {
    this.searchView.setQuery("", false);
    UtilManager.hideSoftKeyboard(getActivity());
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void hideLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(8);
      this.mFadeFL.setVisibility(8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof ScanViewFinderFragment.OnBarcodeScannedListener))
    {
      this.mListener = ((OnTrackingListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnTrackingListener");
  }
  
  public void onClickTopBarLeftButton() {}
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968645, paramViewGroup, false);
    this.searchView = ((SearchView)paramLayoutInflater.findViewById(2131624114));
    this.layoutTrackList = ((RelativeLayout)paramLayoutInflater.findViewById(2131624115));
    this.layoutTrackListEmpty = ((RelativeLayout)paramLayoutInflater.findViewById(2131624192));
    this.numCurrentShipments = ((TextView)paramLayoutInflater.findViewById(2131624194));
    this.mProgressBar = ((ProgressBar)paramLayoutInflater.findViewById(2131624063));
    this.mFadeFL = ((FrameLayout)paramLayoutInflater.findViewById(2131624062));
    showLoadingOverlay();
    this.trackList = ((ListView)paramLayoutInflater.findViewById(2131624195));
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Track");
    hideTopBarLeftButton();
    hideTopBarRightButton();
    setTopBarTitle("Track");
    this.searchView.clearFocus();
    this.searchView.setOnCloseListener(new SearchView.OnCloseListener()
    {
      public boolean onClose()
      {
        UtilManager.hideSoftKeyboard(PBTrackFragment.this.getActivity());
        return false;
      }
    });
    this.searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        Log.e("Search bar focus: ", "" + paramAnonymousBoolean);
        if (!paramAnonymousBoolean) {
          UtilManager.hideSoftKeyboard(PBTrackFragment.this.getActivity());
        }
      }
    });
    this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
    {
      public boolean onQueryTextChange(String paramAnonymousString)
      {
        return false;
      }
      
      public boolean onQueryTextSubmit(String paramAnonymousString)
      {
        PBTrackFragment.this.callTrackingIDQueryApi(paramAnonymousString);
        UtilManager.hideSoftKeyboard(PBTrackFragment.this.getActivity());
        return false;
      }
    });
    this.shipmentTrackingList = new ArrayList();
    adapter = new TrackListAdapter(this.shipmentTrackingList, getContext());
    this.trackList.setAdapter(adapter);
    hideTrackListLayout();
    this.trackList.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        PBTrackFragment.this.callTrackingItemApi(paramAnonymousInt);
      }
    });
    updateTrackingList();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onLoadingOverlay(String paramString) {}
  
  public void showLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(0);
      this.mFadeFL.setVisibility(0);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void updateTrackingLayout()
  {
    callGetTrackingListApi();
    clearTrackingQuery();
  }
  
  void updateTrackingList()
  {
    callGetTrackingListApi();
  }
  
  public static abstract interface OnTrackingListener
  {
    public abstract void onTrackListLoading(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBTrackFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */