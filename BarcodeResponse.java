package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class BarcodeResponse
  extends PBObject
  implements Serializable
{
  @Expose
  @SerializedName("catalogue")
  Catalogue catalogue;
  @Expose
  @SerializedName("message")
  String message;
  
  public BarcodeResponse() {}
  
  public BarcodeResponse(String paramString, Catalogue paramCatalogue)
  {
    this.message = paramString;
    this.catalogue = paramCatalogue;
  }
  
  public Catalogue getCatalogue()
  {
    return this.catalogue;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setCatalogue(Catalogue paramCatalogue)
  {
    this.catalogue = paramCatalogue;
  }
  
  public void setMessage(String paramString)
  {
    this.message = paramString;
  }
  
  public class Catalogue
  {
    private BarcodeResponseItem Item;
    
    public Catalogue() {}
    
    public Catalogue(BarcodeResponseItem paramBarcodeResponseItem)
    {
      this.Item = paramBarcodeResponseItem;
    }
    
    public BarcodeResponseItem getItem()
    {
      return this.Item;
    }
    
    public void setItem(BarcodeResponseItem paramBarcodeResponseItem)
    {
      this.Item = paramBarcodeResponseItem;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\BarcodeResponse.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */