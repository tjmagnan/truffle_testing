package com.afollestad.materialdialogs.color;

import android.graphics.Color;

class ColorPalette
{
  static final int[] ACCENT_COLORS;
  static final int[][] ACCENT_COLORS_SUB;
  static final int[] PRIMARY_COLORS = { Color.parseColor("#F44336"), Color.parseColor("#E91E63"), Color.parseColor("#9C27B0"), Color.parseColor("#673AB7"), Color.parseColor("#3F51B5"), Color.parseColor("#2196F3"), Color.parseColor("#03A9F4"), Color.parseColor("#00BCD4"), Color.parseColor("#009688"), Color.parseColor("#4CAF50"), Color.parseColor("#8BC34A"), Color.parseColor("#CDDC39"), Color.parseColor("#FFEB3B"), Color.parseColor("#FFC107"), Color.parseColor("#FF9800"), Color.parseColor("#FF5722"), Color.parseColor("#795548"), Color.parseColor("#9E9E9E"), Color.parseColor("#607D8B") };
  static final int[][] PRIMARY_COLORS_SUB;
  
  static
  {
    int i55 = Color.parseColor("#FFEBEE");
    int i78 = Color.parseColor("#FFCDD2");
    int i27 = Color.parseColor("#EF9A9A");
    int i14 = Color.parseColor("#E57373");
    int i19 = Color.parseColor("#EF5350");
    int i70 = Color.parseColor("#F44336");
    int i46 = Color.parseColor("#E53935");
    int i67 = Color.parseColor("#D32F2F");
    int i21 = Color.parseColor("#C62828");
    int i25 = Color.parseColor("#B71C1C");
    int i52 = Color.parseColor("#FCE4EC");
    int i3 = Color.parseColor("#F8BBD0");
    int i72 = Color.parseColor("#F48FB1");
    int m = Color.parseColor("#F06292");
    int i9 = Color.parseColor("#EC407A");
    int i24 = Color.parseColor("#E91E63");
    int i75 = Color.parseColor("#D81B60");
    int i38 = Color.parseColor("#C2185B");
    int i18 = Color.parseColor("#AD1457");
    int i85 = Color.parseColor("#880E4F");
    int[] arrayOfInt1 = { Color.parseColor("#F3E5F5"), Color.parseColor("#E1BEE7"), Color.parseColor("#CE93D8"), Color.parseColor("#BA68C8"), Color.parseColor("#AB47BC"), Color.parseColor("#9C27B0"), Color.parseColor("#8E24AA"), Color.parseColor("#7B1FA2"), Color.parseColor("#6A1B9A"), Color.parseColor("#4A148C") };
    int i17 = Color.parseColor("#EDE7F6");
    int i34 = Color.parseColor("#D1C4E9");
    int i11 = Color.parseColor("#B39DDB");
    int i44 = Color.parseColor("#9575CD");
    int i69 = Color.parseColor("#7E57C2");
    int i63 = Color.parseColor("#673AB7");
    int i76 = Color.parseColor("#5E35B1");
    int i10 = Color.parseColor("#512DA8");
    int i74 = Color.parseColor("#4527A0");
    int i61 = Color.parseColor("#311B92");
    int[] arrayOfInt2 = { Color.parseColor("#E8EAF6"), Color.parseColor("#C5CAE9"), Color.parseColor("#9FA8DA"), Color.parseColor("#7986CB"), Color.parseColor("#5C6BC0"), Color.parseColor("#3F51B5"), Color.parseColor("#3949AB"), Color.parseColor("#303F9F"), Color.parseColor("#283593"), Color.parseColor("#1A237E") };
    int[] arrayOfInt3 = { Color.parseColor("#E3F2FD"), Color.parseColor("#BBDEFB"), Color.parseColor("#90CAF9"), Color.parseColor("#64B5F6"), Color.parseColor("#42A5F5"), Color.parseColor("#2196F3"), Color.parseColor("#1E88E5"), Color.parseColor("#1976D2"), Color.parseColor("#1565C0"), Color.parseColor("#0D47A1") };
    int i41 = Color.parseColor("#E1F5FE");
    int i16 = Color.parseColor("#B3E5FC");
    int i66 = Color.parseColor("#81D4FA");
    int i28 = Color.parseColor("#4FC3F7");
    int i53 = Color.parseColor("#29B6F6");
    int i81 = Color.parseColor("#03A9F4");
    int i60 = Color.parseColor("#039BE5");
    int i57 = Color.parseColor("#0288D1");
    int i45 = Color.parseColor("#0277BD");
    int i77 = Color.parseColor("#01579B");
    int[] arrayOfInt4 = { Color.parseColor("#E0F7FA"), Color.parseColor("#B2EBF2"), Color.parseColor("#80DEEA"), Color.parseColor("#4DD0E1"), Color.parseColor("#26C6DA"), Color.parseColor("#00BCD4"), Color.parseColor("#00ACC1"), Color.parseColor("#0097A7"), Color.parseColor("#00838F"), Color.parseColor("#006064") };
    int[] arrayOfInt5 = { Color.parseColor("#E0F2F1"), Color.parseColor("#B2DFDB"), Color.parseColor("#80CBC4"), Color.parseColor("#4DB6AC"), Color.parseColor("#26A69A"), Color.parseColor("#009688"), Color.parseColor("#00897B"), Color.parseColor("#00796B"), Color.parseColor("#00695C"), Color.parseColor("#004D40") };
    int i80 = Color.parseColor("#E8F5E9");
    int i56 = Color.parseColor("#C8E6C9");
    int i = Color.parseColor("#A5D6A7");
    int j = Color.parseColor("#81C784");
    int i65 = Color.parseColor("#66BB6A");
    int i71 = Color.parseColor("#4CAF50");
    int i50 = Color.parseColor("#43A047");
    int i4 = Color.parseColor("#388E3C");
    int i40 = Color.parseColor("#2E7D32");
    int i22 = Color.parseColor("#1B5E20");
    int[] arrayOfInt6 = { Color.parseColor("#F1F8E9"), Color.parseColor("#DCEDC8"), Color.parseColor("#C5E1A5"), Color.parseColor("#AED581"), Color.parseColor("#9CCC65"), Color.parseColor("#8BC34A"), Color.parseColor("#7CB342"), Color.parseColor("#689F38"), Color.parseColor("#558B2F"), Color.parseColor("#33691E") };
    int[] arrayOfInt7 = { Color.parseColor("#F9FBE7"), Color.parseColor("#F0F4C3"), Color.parseColor("#E6EE9C"), Color.parseColor("#DCE775"), Color.parseColor("#D4E157"), Color.parseColor("#CDDC39"), Color.parseColor("#C0CA33"), Color.parseColor("#AFB42B"), Color.parseColor("#9E9D24"), Color.parseColor("#827717") };
    int i5 = Color.parseColor("#FFFDE7");
    int i2 = Color.parseColor("#FFF9C4");
    int i64 = Color.parseColor("#FFF59D");
    int i26 = Color.parseColor("#FFF176");
    int i32 = Color.parseColor("#FFEE58");
    int i79 = Color.parseColor("#FFEB3B");
    int i29 = Color.parseColor("#FDD835");
    int i37 = Color.parseColor("#FBC02D");
    int i8 = Color.parseColor("#F9A825");
    int i68 = Color.parseColor("#F57F17");
    int i15 = Color.parseColor("#FFF8E1");
    int i62 = Color.parseColor("#FFECB3");
    int i59 = Color.parseColor("#FFE082");
    int i58 = Color.parseColor("#FFD54F");
    int i54 = Color.parseColor("#FFCA28");
    int i31 = Color.parseColor("#FFC107");
    int i48 = Color.parseColor("#FFB300");
    int i39 = Color.parseColor("#FFA000");
    int i12 = Color.parseColor("#FF8F00");
    int i51 = Color.parseColor("#FF6F00");
    int i6 = Color.parseColor("#FFF3E0");
    int n = Color.parseColor("#FFE0B2");
    int i33 = Color.parseColor("#FFCC80");
    int i35 = Color.parseColor("#FFB74D");
    int i7 = Color.parseColor("#FFA726");
    int i47 = Color.parseColor("#FF9800");
    int i43 = Color.parseColor("#FB8C00");
    int i84 = Color.parseColor("#F57C00");
    int i49 = Color.parseColor("#EF6C00");
    int i23 = Color.parseColor("#E65100");
    int[] arrayOfInt8 = { Color.parseColor("#FBE9E7"), Color.parseColor("#FFCCBC"), Color.parseColor("#FFAB91"), Color.parseColor("#FF8A65"), Color.parseColor("#FF7043"), Color.parseColor("#FF5722"), Color.parseColor("#F4511E"), Color.parseColor("#E64A19"), Color.parseColor("#D84315"), Color.parseColor("#BF360C") };
    int i73 = Color.parseColor("#EFEBE9");
    int k = Color.parseColor("#D7CCC8");
    int i30 = Color.parseColor("#BCAAA4");
    int i36 = Color.parseColor("#A1887F");
    int i82 = Color.parseColor("#8D6E63");
    int i13 = Color.parseColor("#795548");
    int i83 = Color.parseColor("#6D4C41");
    int i20 = Color.parseColor("#5D4037");
    int i1 = Color.parseColor("#4E342E");
    int i42 = Color.parseColor("#3E2723");
    int[] arrayOfInt9 = { Color.parseColor("#FAFAFA"), Color.parseColor("#F5F5F5"), Color.parseColor("#EEEEEE"), Color.parseColor("#E0E0E0"), Color.parseColor("#BDBDBD"), Color.parseColor("#9E9E9E"), Color.parseColor("#757575"), Color.parseColor("#616161"), Color.parseColor("#424242"), Color.parseColor("#212121") };
    int[] arrayOfInt10 = { Color.parseColor("#ECEFF1"), Color.parseColor("#CFD8DC"), Color.parseColor("#B0BEC5"), Color.parseColor("#90A4AE"), Color.parseColor("#78909C"), Color.parseColor("#607D8B"), Color.parseColor("#546E7A"), Color.parseColor("#455A64"), Color.parseColor("#37474F"), Color.parseColor("#263238") };
    PRIMARY_COLORS_SUB = new int[][] { { i55, i78, i27, i14, i19, i70, i46, i67, i21, i25 }, { i52, i3, i72, m, i9, i24, i75, i38, i18, i85 }, arrayOfInt1, { i17, i34, i11, i44, i69, i63, i76, i10, i74, i61 }, arrayOfInt2, arrayOfInt3, { i41, i16, i66, i28, i53, i81, i60, i57, i45, i77 }, arrayOfInt4, arrayOfInt5, { i80, i56, i, j, i65, i71, i50, i4, i40, i22 }, arrayOfInt6, arrayOfInt7, { i5, i2, i64, i26, i32, i79, i29, i37, i8, i68 }, { i15, i62, i59, i58, i54, i31, i48, i39, i12, i51 }, { i6, n, i33, i35, i7, i47, i43, i84, i49, i23 }, arrayOfInt8, { i73, k, i30, i36, i82, i13, i83, i20, i1, i42 }, arrayOfInt9, arrayOfInt10 };
    ACCENT_COLORS = new int[] { Color.parseColor("#FF1744"), Color.parseColor("#F50057"), Color.parseColor("#D500F9"), Color.parseColor("#651FFF"), Color.parseColor("#3D5AFE"), Color.parseColor("#2979FF"), Color.parseColor("#00B0FF"), Color.parseColor("#00E5FF"), Color.parseColor("#1DE9B6"), Color.parseColor("#00E676"), Color.parseColor("#76FF03"), Color.parseColor("#C6FF00"), Color.parseColor("#FFEA00"), Color.parseColor("#FFC400"), Color.parseColor("#FF9100"), Color.parseColor("#FF3D00") };
    arrayOfInt1 = new int[] { Color.parseColor("#FF8A80"), Color.parseColor("#FF5252"), Color.parseColor("#FF1744"), Color.parseColor("#D50000") };
    arrayOfInt2 = new int[] { Color.parseColor("#FF80AB"), Color.parseColor("#FF4081"), Color.parseColor("#F50057"), Color.parseColor("#C51162") };
    arrayOfInt3 = new int[] { Color.parseColor("#EA80FC"), Color.parseColor("#E040FB"), Color.parseColor("#D500F9"), Color.parseColor("#AA00FF") };
    arrayOfInt4 = new int[] { Color.parseColor("#B388FF"), Color.parseColor("#7C4DFF"), Color.parseColor("#651FFF"), Color.parseColor("#6200EA") };
    arrayOfInt5 = new int[] { Color.parseColor("#8C9EFF"), Color.parseColor("#536DFE"), Color.parseColor("#3D5AFE"), Color.parseColor("#304FFE") };
    m = Color.parseColor("#82B1FF");
    i5 = Color.parseColor("#448AFF");
    i11 = Color.parseColor("#2979FF");
    i4 = Color.parseColor("#2962FF");
    arrayOfInt6 = new int[] { Color.parseColor("#80D8FF"), Color.parseColor("#40C4FF"), Color.parseColor("#00B0FF"), Color.parseColor("#0091EA") };
    arrayOfInt7 = new int[] { Color.parseColor("#84FFFF"), Color.parseColor("#18FFFF"), Color.parseColor("#00E5FF"), Color.parseColor("#00B8D4") };
    arrayOfInt8 = new int[] { Color.parseColor("#A7FFEB"), Color.parseColor("#64FFDA"), Color.parseColor("#1DE9B6"), Color.parseColor("#00BFA5") };
    i7 = Color.parseColor("#B9F6CA");
    i9 = Color.parseColor("#69F0AE");
    i1 = Color.parseColor("#00E676");
    k = Color.parseColor("#00C853");
    i10 = Color.parseColor("#CCFF90");
    i2 = Color.parseColor("#B2FF59");
    j = Color.parseColor("#76FF03");
    i6 = Color.parseColor("#64DD17");
    arrayOfInt9 = new int[] { Color.parseColor("#F4FF81"), Color.parseColor("#EEFF41"), Color.parseColor("#C6FF00"), Color.parseColor("#AEEA00") };
    i = Color.parseColor("#FFFF8D");
    i8 = Color.parseColor("#FFFF00");
    n = Color.parseColor("#FFEA00");
    i3 = Color.parseColor("#FFD600");
    arrayOfInt10 = new int[] { Color.parseColor("#FFE57F"), Color.parseColor("#FFD740"), Color.parseColor("#FFC400"), Color.parseColor("#FFAB00") };
    int[] arrayOfInt11 = { Color.parseColor("#FFD180"), Color.parseColor("#FFAB40"), Color.parseColor("#FF9100"), Color.parseColor("#FF6D00") };
    int[] arrayOfInt12 = { Color.parseColor("#FF9E80"), Color.parseColor("#FF6E40"), Color.parseColor("#FF3D00"), Color.parseColor("#DD2C00") };
    ACCENT_COLORS_SUB = new int[][] { arrayOfInt1, arrayOfInt2, arrayOfInt3, arrayOfInt4, arrayOfInt5, { m, i5, i11, i4 }, arrayOfInt6, arrayOfInt7, arrayOfInt8, { i7, i9, i1, k }, { i10, i2, j, i6 }, arrayOfInt9, { i, i8, n, i3 }, arrayOfInt10, arrayOfInt11, arrayOfInt12 };
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\color\ColorPalette.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */