package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

class TwilightManager
{
  private static final int SUNRISE = 6;
  private static final int SUNSET = 22;
  private static final String TAG = "TwilightManager";
  private static TwilightManager sInstance;
  private final Context mContext;
  private final LocationManager mLocationManager;
  private final TwilightState mTwilightState = new TwilightState();
  
  @VisibleForTesting
  TwilightManager(@NonNull Context paramContext, @NonNull LocationManager paramLocationManager)
  {
    this.mContext = paramContext;
    this.mLocationManager = paramLocationManager;
  }
  
  static TwilightManager getInstance(@NonNull Context paramContext)
  {
    if (sInstance == null)
    {
      paramContext = paramContext.getApplicationContext();
      sInstance = new TwilightManager(paramContext, (LocationManager)paramContext.getSystemService("location"));
    }
    return sInstance;
  }
  
  private Location getLastKnownLocation()
  {
    Location localLocation1 = null;
    Location localLocation3 = null;
    if (PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
      localLocation1 = getLastKnownLocationForProvider("network");
    }
    if (PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_FINE_LOCATION") == 0) {
      localLocation3 = getLastKnownLocationForProvider("gps");
    }
    Location localLocation2;
    if ((localLocation3 != null) && (localLocation1 != null)) {
      if (localLocation3.getTime() > localLocation1.getTime()) {
        localLocation2 = localLocation3;
      }
    }
    for (;;)
    {
      return localLocation2;
      localLocation2 = localLocation1;
      continue;
      localLocation2 = localLocation3;
      if (localLocation3 == null) {
        localLocation2 = localLocation1;
      }
    }
  }
  
  private Location getLastKnownLocationForProvider(String paramString)
  {
    if (this.mLocationManager != null) {}
    for (;;)
    {
      try
      {
        if (this.mLocationManager.isProviderEnabled(paramString))
        {
          paramString = this.mLocationManager.getLastKnownLocation(paramString);
          return paramString;
        }
      }
      catch (Exception paramString)
      {
        Log.d("TwilightManager", "Failed to get last known location", paramString);
      }
      paramString = null;
    }
  }
  
  private boolean isStateValid()
  {
    if ((this.mTwilightState != null) && (this.mTwilightState.nextUpdate > System.currentTimeMillis())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @VisibleForTesting
  static void setInstance(TwilightManager paramTwilightManager)
  {
    sInstance = paramTwilightManager;
  }
  
  private void updateState(@NonNull Location paramLocation)
  {
    TwilightState localTwilightState = this.mTwilightState;
    long l1 = System.currentTimeMillis();
    TwilightCalculator localTwilightCalculator = TwilightCalculator.getInstance();
    localTwilightCalculator.calculateTwilight(l1 - 86400000L, paramLocation.getLatitude(), paramLocation.getLongitude());
    long l2 = localTwilightCalculator.sunset;
    localTwilightCalculator.calculateTwilight(l1, paramLocation.getLatitude(), paramLocation.getLongitude());
    if (localTwilightCalculator.state == 1) {}
    long l4;
    long l3;
    long l5;
    for (boolean bool = true;; bool = false)
    {
      l4 = localTwilightCalculator.sunrise;
      l3 = localTwilightCalculator.sunset;
      localTwilightCalculator.calculateTwilight(86400000L + l1, paramLocation.getLatitude(), paramLocation.getLongitude());
      l5 = localTwilightCalculator.sunrise;
      if ((l4 != -1L) && (l3 != -1L)) {
        break;
      }
      l1 += 43200000L;
      localTwilightState.isNight = bool;
      localTwilightState.yesterdaySunset = l2;
      localTwilightState.todaySunrise = l4;
      localTwilightState.todaySunset = l3;
      localTwilightState.tomorrowSunrise = l5;
      localTwilightState.nextUpdate = l1;
      return;
    }
    if (l1 > l3) {
      l1 = 0L + l5;
    }
    for (;;)
    {
      l1 += 60000L;
      break;
      if (l1 > l4) {
        l1 = 0L + l3;
      } else {
        l1 = 0L + l4;
      }
    }
  }
  
  boolean isNight()
  {
    TwilightState localTwilightState = this.mTwilightState;
    boolean bool;
    if (isStateValid()) {
      bool = localTwilightState.isNight;
    }
    for (;;)
    {
      return bool;
      Location localLocation = getLastKnownLocation();
      if (localLocation != null)
      {
        updateState(localLocation);
        bool = localTwilightState.isNight;
      }
      else
      {
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        if ((i < 6) || (i >= 22)) {
          bool = true;
        } else {
          bool = false;
        }
      }
    }
  }
  
  private static class TwilightState
  {
    boolean isNight;
    long nextUpdate;
    long todaySunrise;
    long todaySunset;
    long tomorrowSunrise;
    long yesterdaySunset;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\app\TwilightManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */