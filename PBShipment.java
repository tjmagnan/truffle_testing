package tech.dcube.companion.model;

public class PBShipment
  extends PBObject
{
  public PBSendProItem item;
  public boolean manualShipment;
  public PBShipmentPrintLabel printLabel;
  public PBAddress receiverAddress;
  public PBAddress senderAddress;
  public PBShipmentPackage shipmentPackage;
  public PBShipmentRate shipmentRate;
  public PBShipmentService shipmentService;
  
  public PBSendProItem getItem()
  {
    return this.item;
  }
  
  public PBShipmentPrintLabel getPrintLabel()
  {
    return this.printLabel;
  }
  
  public PBAddress getReceiverAddress()
  {
    return this.receiverAddress;
  }
  
  public PBAddress getSenderAddress()
  {
    return this.senderAddress;
  }
  
  public PBShipmentPackage getShipmentPackage()
  {
    return this.shipmentPackage;
  }
  
  public PBShipmentRate getShipmentRate()
  {
    return this.shipmentRate;
  }
  
  public PBShipmentService getShipmentService()
  {
    return this.shipmentService;
  }
  
  public boolean isManualShipment()
  {
    return this.manualShipment;
  }
  
  public void setItem(PBSendProItem paramPBSendProItem)
  {
    this.item = paramPBSendProItem;
  }
  
  public void setManualShipment(boolean paramBoolean)
  {
    this.manualShipment = paramBoolean;
  }
  
  public void setPrintLabel(PBShipmentPrintLabel paramPBShipmentPrintLabel)
  {
    this.printLabel = paramPBShipmentPrintLabel;
  }
  
  public void setReceiverAddress(PBAddress paramPBAddress)
  {
    this.receiverAddress = paramPBAddress;
  }
  
  public void setSenderAddress(PBAddress paramPBAddress)
  {
    this.senderAddress = paramPBAddress;
  }
  
  public void setShipmentPackage(PBShipmentPackage paramPBShipmentPackage)
  {
    this.shipmentPackage = paramPBShipmentPackage;
  }
  
  public void setShipmentRate(PBShipmentRate paramPBShipmentRate)
  {
    this.shipmentRate = paramPBShipmentRate;
  }
  
  public void setShipmentService(PBShipmentService paramPBShipmentService)
  {
    this.shipmentService = paramPBShipmentService;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */