package tech.dcube.companion.fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class PBShipMainFragment_ViewBinding
  implements Unbinder
{
  private PBShipMainFragment target;
  private View view2131624177;
  private View view2131624178;
  
  @UiThread
  public PBShipMainFragment_ViewBinding(final PBShipMainFragment paramPBShipMainFragment, View paramView)
  {
    this.target = paramPBShipMainFragment;
    View localView = Utils.findRequiredView(paramView, 2131624177, "method 'initiateBarcodeScan'");
    this.view2131624177 = localView;
    localView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBShipMainFragment.initiateBarcodeScan();
      }
    });
    paramView = Utils.findRequiredView(paramView, 2131624178, "method 'initiateManualItemPicking'");
    this.view2131624178 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramPBShipMainFragment.initiateManualItemPicking();
      }
    });
  }
  
  @CallSuper
  public void unbind()
  {
    if (this.target == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    this.view2131624177.setOnClickListener(null);
    this.view2131624177 = null;
    this.view2131624178.setOnClickListener(null);
    this.view2131624178 = null;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBShipMainFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */