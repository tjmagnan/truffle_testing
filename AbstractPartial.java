package org.joda.time.base;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DurationFieldType;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadablePartial;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;

public abstract class AbstractPartial
  implements ReadablePartial, Comparable<ReadablePartial>
{
  public int compareTo(ReadablePartial paramReadablePartial)
  {
    int k = 0;
    if (this == paramReadablePartial)
    {
      i = k;
      return i;
    }
    if (size() != paramReadablePartial.size()) {
      throw new ClassCastException("ReadablePartial objects must have matching field types");
    }
    int j = size();
    for (int i = 0; i < j; i++) {
      if (getFieldType(i) != paramReadablePartial.getFieldType(i)) {
        throw new ClassCastException("ReadablePartial objects must have matching field types");
      }
    }
    int m = size();
    for (j = 0;; j++)
    {
      i = k;
      if (j >= m) {
        break;
      }
      if (getValue(j) > paramReadablePartial.getValue(j))
      {
        i = 1;
        break;
      }
      if (getValue(j) < paramReadablePartial.getValue(j))
      {
        i = -1;
        break;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this == paramObject) {
      bool1 = true;
    }
    for (;;)
    {
      return bool1;
      bool1 = bool2;
      if ((paramObject instanceof ReadablePartial))
      {
        paramObject = (ReadablePartial)paramObject;
        bool1 = bool2;
        if (size() == ((ReadablePartial)paramObject).size())
        {
          int j = size();
          for (int i = 0;; i++)
          {
            if (i >= j) {
              break label103;
            }
            bool1 = bool2;
            if (getValue(i) != ((ReadablePartial)paramObject).getValue(i)) {
              break;
            }
            bool1 = bool2;
            if (getFieldType(i) != ((ReadablePartial)paramObject).getFieldType(i)) {
              break;
            }
          }
          label103:
          bool1 = FieldUtils.equals(getChronology(), ((ReadablePartial)paramObject).getChronology());
        }
      }
    }
  }
  
  public int get(DateTimeFieldType paramDateTimeFieldType)
  {
    return getValue(indexOfSupported(paramDateTimeFieldType));
  }
  
  public DateTimeField getField(int paramInt)
  {
    return getField(paramInt, getChronology());
  }
  
  protected abstract DateTimeField getField(int paramInt, Chronology paramChronology);
  
  public DateTimeFieldType getFieldType(int paramInt)
  {
    return getField(paramInt, getChronology()).getType();
  }
  
  public DateTimeFieldType[] getFieldTypes()
  {
    DateTimeFieldType[] arrayOfDateTimeFieldType = new DateTimeFieldType[size()];
    for (int i = 0; i < arrayOfDateTimeFieldType.length; i++) {
      arrayOfDateTimeFieldType[i] = getFieldType(i);
    }
    return arrayOfDateTimeFieldType;
  }
  
  public DateTimeField[] getFields()
  {
    DateTimeField[] arrayOfDateTimeField = new DateTimeField[size()];
    for (int i = 0; i < arrayOfDateTimeField.length; i++) {
      arrayOfDateTimeField[i] = getField(i);
    }
    return arrayOfDateTimeField;
  }
  
  public int[] getValues()
  {
    int[] arrayOfInt = new int[size()];
    for (int i = 0; i < arrayOfInt.length; i++) {
      arrayOfInt[i] = getValue(i);
    }
    return arrayOfInt;
  }
  
  public int hashCode()
  {
    int i = 157;
    int j = 0;
    int k = size();
    while (j < k)
    {
      i = (i * 23 + getValue(j)) * 23 + getFieldType(j).hashCode();
      j++;
    }
    return getChronology().hashCode() + i;
  }
  
  public int indexOf(DateTimeFieldType paramDateTimeFieldType)
  {
    int i = 0;
    int j = size();
    if (i < j) {
      if (getFieldType(i) != paramDateTimeFieldType) {}
    }
    for (;;)
    {
      return i;
      i++;
      break;
      i = -1;
    }
  }
  
  protected int indexOf(DurationFieldType paramDurationFieldType)
  {
    int i = 0;
    int j = size();
    if (i < j) {
      if (getFieldType(i).getDurationType() != paramDurationFieldType) {}
    }
    for (;;)
    {
      return i;
      i++;
      break;
      i = -1;
    }
  }
  
  protected int indexOfSupported(DateTimeFieldType paramDateTimeFieldType)
  {
    int i = indexOf(paramDateTimeFieldType);
    if (i == -1) {
      throw new IllegalArgumentException("Field '" + paramDateTimeFieldType + "' is not supported");
    }
    return i;
  }
  
  protected int indexOfSupported(DurationFieldType paramDurationFieldType)
  {
    int i = indexOf(paramDurationFieldType);
    if (i == -1) {
      throw new IllegalArgumentException("Field '" + paramDurationFieldType + "' is not supported");
    }
    return i;
  }
  
  public boolean isAfter(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {
      throw new IllegalArgumentException("Partial cannot be null");
    }
    if (compareTo(paramReadablePartial) > 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isBefore(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {
      throw new IllegalArgumentException("Partial cannot be null");
    }
    if (compareTo(paramReadablePartial) < 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isEqual(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {
      throw new IllegalArgumentException("Partial cannot be null");
    }
    if (compareTo(paramReadablePartial) == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isSupported(DateTimeFieldType paramDateTimeFieldType)
  {
    if (indexOf(paramDateTimeFieldType) != -1) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public DateTime toDateTime(ReadableInstant paramReadableInstant)
  {
    Chronology localChronology = DateTimeUtils.getInstantChronology(paramReadableInstant);
    return new DateTime(localChronology.set(this, DateTimeUtils.getInstantMillis(paramReadableInstant)), localChronology);
  }
  
  public String toString(DateTimeFormatter paramDateTimeFormatter)
  {
    if (paramDateTimeFormatter == null) {}
    for (paramDateTimeFormatter = toString();; paramDateTimeFormatter = paramDateTimeFormatter.print(this)) {
      return paramDateTimeFormatter;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\AbstractPartial.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */