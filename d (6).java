package com.testfairy.d;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.testfairy.p.q;
import java.lang.reflect.Field;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

public class f
{
  public static final int a = 129;
  private static final int e = Color.argb(0, 0, 0, 0);
  private static final String g = "com.unity3d.player.UnityPlayer";
  private final h b;
  private Bitmap c = null;
  private boolean d = false;
  private boolean f = false;
  
  public f(h paramh)
  {
    this.b = paramh;
  }
  
  private a a(View paramView, Set paramSet, h paramh)
  {
    a locala = new a(null);
    Object localObject2 = ((ArrayList)com.testfairy.p.r.a(paramView, SurfaceView.class)).iterator();
    Object localObject3;
    int i;
    while (((Iterator)localObject2).hasNext())
    {
      localObject1 = (SurfaceView)((Iterator)localObject2).next();
      if (((SurfaceView)localObject1).willNotDraw())
      {
        ((SurfaceView)localObject1).setWillNotDraw(false);
        localObject3 = new p((View)localObject1);
        locala.a.add(localObject3);
      }
      localObject3 = (ViewGroup)((SurfaceView)localObject1).getParent();
      Object localObject4;
      if ((localObject1 instanceof GLSurfaceView))
      {
        i = com.testfairy.p.r.c((View)localObject1);
        localObject4 = new c(((SurfaceView)localObject1).getContext(), (GLSurfaceView)localObject1);
        ((c)localObject4).layout(((SurfaceView)localObject1).getLeft(), ((SurfaceView)localObject1).getTop(), ((SurfaceView)localObject1).getRight(), ((SurfaceView)localObject1).getBottom());
        ((c)localObject4).invalidate();
        ((c)localObject4).setWillNotDraw(false);
        ((ViewGroup)localObject3).addView((View)localObject4, i + 1);
        localObject3 = new e((View)localObject4);
        locala.a.add(localObject3);
      }
      if (localObject1.getClass().getName().equals(com.testfairy.n.T))
      {
        localObject3 = (ViewGroup)((SurfaceView)localObject1).getParent();
        i = com.testfairy.p.r.c((View)localObject1);
        localObject4 = new c(((SurfaceView)localObject1).getContext());
        ((c)localObject4).a(null);
        ((c)localObject4).layout(((SurfaceView)localObject1).getLeft(), ((SurfaceView)localObject1).getTop(), ((SurfaceView)localObject1).getRight(), ((SurfaceView)localObject1).getBottom());
        ((ViewGroup)localObject3).addView((View)localObject4, i + 1);
        ((c)localObject4).invalidate();
        localObject3 = new e((View)localObject4);
        locala.a.add(localObject3);
      }
      if (localObject1.getClass().getName().equals(com.testfairy.n.S))
      {
        localObject4 = a((SurfaceView)localObject1, paramh);
        if (localObject4 != null)
        {
          localObject3 = (ViewGroup)((SurfaceView)localObject1).getParent();
          i = com.testfairy.p.r.c((View)localObject1);
          j localj = new j(((SurfaceView)localObject1).getContext());
          localj.a((com.testfairy.d)localObject4);
          localj.layout(((SurfaceView)localObject1).getLeft(), ((SurfaceView)localObject1).getTop(), ((SurfaceView)localObject1).getRight(), ((SurfaceView)localObject1).getBottom());
          ((ViewGroup)localObject3).addView(localj, i + 1);
          localj.invalidate();
          locala.a.add(new e(localj));
        }
      }
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      paramh = com.testfairy.p.r.a(paramView, TextureView.class).iterator();
      while (paramh.hasNext())
      {
        localObject3 = (TextureView)paramh.next();
        localObject2 = (ViewGroup)((TextureView)localObject3).getParent();
        i = com.testfairy.p.r.c((View)localObject3);
        localObject1 = new k(((TextureView)localObject3).getContext());
        ((k)localObject1).a((TextureView)localObject3);
        ((k)localObject1).layout(((TextureView)localObject3).getLeft(), ((TextureView)localObject3).getTop(), ((TextureView)localObject3).getRight(), ((TextureView)localObject3).getBottom());
        ((ViewGroup)localObject2).addView((View)localObject1, i + 1);
        ((k)localObject1).invalidate();
        locala.a.add(new e((View)localObject1));
      }
    }
    Object localObject1 = ((ArrayList)com.testfairy.p.r.a(paramView, WebView.class)).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject3 = (WebView)((Iterator)localObject1).next();
      paramh = (ViewGroup)((WebView)localObject3).getParent();
      i = com.testfairy.p.r.c((View)localObject3);
      localObject2 = new r(((WebView)localObject3).getContext());
      ((r)localObject2).a((WebView)localObject3);
      ((r)localObject2).setScrollX(((WebView)localObject3).getScrollX());
      ((r)localObject2).setScrollY(((WebView)localObject3).getScrollY());
      ((r)localObject2).layout(((WebView)localObject3).getLeft(), ((WebView)localObject3).getTop(), ((WebView)localObject3).getRight(), ((WebView)localObject3).getBottom());
      paramh.addView((View)localObject2, i + 1);
      ((r)localObject2).invalidate();
      locala.a.add(new e((View)localObject2));
    }
    if (!this.f)
    {
      paramh = com.testfairy.p.r.a(paramView, TextView.class).iterator();
      while (paramh.hasNext())
      {
        localObject2 = (TextView)paramh.next();
        if (((((TextView)localObject2).getInputType() & 0x81) == 129) && (((TextView)localObject2).getVisibility() == 0))
        {
          localObject1 = new n((TextView)localObject2);
          ((TextView)localObject2).setTextColor(e);
          locala.a.add(localObject1);
        }
      }
    }
    if (this.f)
    {
      paramh = com.testfairy.p.r.a(paramView, EditText.class).iterator();
      while (paramh.hasNext())
      {
        localObject2 = (EditText)paramh.next();
        if (((EditText)localObject2).getVisibility() == 0)
        {
          localObject1 = new n((TextView)localObject2);
          ((EditText)localObject2).setTextColor(e);
          locala.a.add(localObject1);
        }
      }
    }
    localObject2 = paramSet.iterator();
    while (((Iterator)localObject2).hasNext())
    {
      paramSet = com.testfairy.p.r.a(paramView, ((Integer)((Iterator)localObject2).next()).intValue()).iterator();
      while (paramSet.hasNext())
      {
        paramh = (View)paramSet.next();
        localObject1 = new l(paramh);
        paramh.setAlpha(0.0F);
        locala.a.add(localObject1);
      }
    }
    return locala;
  }
  
  private com.testfairy.d a(SurfaceView paramSurfaceView, h paramh)
  {
    for (;;)
    {
      try
      {
        Field localField = paramSurfaceView.getClass().getDeclaredField(com.testfairy.n.aF);
        localField.setAccessible(true);
        localObject = localField.get(paramSurfaceView);
        if ((localObject instanceof com.testfairy.d)) {
          continue;
        }
        Log.d(com.testfairy.e.a, com.testfairy.n.aG);
        paramh = (com.testfairy.d)paramh.a((SurfaceHolder)localObject);
        localField.set(paramSurfaceView, paramh);
        paramSurfaceView = com.testfairy.e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.d(paramSurfaceView, com.testfairy.n.aH + localObject);
        paramSurfaceView = paramh;
        localField.setAccessible(false);
      }
      catch (Exception paramSurfaceView)
      {
        Object localObject;
        paramSurfaceView = null;
        continue;
      }
      return paramSurfaceView;
      paramSurfaceView = (com.testfairy.d)localObject;
    }
  }
  
  private void a(Canvas paramCanvas, Set paramSet, View paramView)
  {
    if (paramSet.contains(Integer.valueOf(paramView.getId()))) {}
    for (;;)
    {
      return;
      int[] arrayOfInt = new int[2];
      Matrix localMatrix = new Matrix();
      paramView.getLocationOnScreen(arrayOfInt);
      localMatrix.setTranslate(arrayOfInt[0], arrayOfInt[1]);
      paramCanvas.setMatrix(localMatrix);
      paramSet = a(paramView, paramSet, null);
      paramView.draw(paramCanvas);
      a(paramSet);
    }
  }
  
  private void a(a parama)
  {
    parama = parama.a.iterator();
    while (parama.hasNext()) {
      ((m)parama.next()).a();
    }
  }
  
  private List b(View paramView)
  {
    ArrayList localArrayList = new ArrayList();
    paramView = com.testfairy.p.r.a(paramView, com.testfairy.n.aC);
    if (paramView.size() == 0) {}
    for (;;)
    {
      return localArrayList;
      paramView = paramView.iterator();
      while (paramView.hasNext())
      {
        View localView = (View)paramView.next();
        int i = localView.getMeasuredWidth();
        int j = localView.getMeasuredHeight();
        if ((i > 0) && (j > 0))
        {
          Object localObject = new int[2];
          localView.getLocationOnScreen((int[])localObject);
          localObject = new Rect(localObject[0], localObject[1], i + localObject[0], j + localObject[1]);
          localArrayList.add(new a(com.testfairy.n.aC, localView, (Rect)localObject));
        }
      }
    }
  }
  
  public void a(int paramInt1, int paramInt2, Set paramSet, List paramList)
  {
    try
    {
      if (paramList.size() < 1) {
        this.b.a(null, null);
      }
      for (;;)
      {
        return;
        if ((Build.VERSION.SDK_INT >= 11) || (paramSet.size() <= 0)) {
          break;
        }
        Log.v(com.testfairy.e.a, "API<11 and secure-view-id is set, won't be able to protected views. Disabling screenshots");
        this.b.a(null, null);
      }
    }
    catch (Exception paramSet)
    {
      for (;;)
      {
        Log.e(com.testfairy.e.a, "Exception while drawing view tree", paramSet);
        this.b.a(null, null);
        continue;
        if ((paramInt1 <= 0) || (paramInt2 <= 0))
        {
          this.b.a(null, null);
        }
        else
        {
          if ((this.c != null) && ((this.c.getWidth() != paramInt1) || (this.c.getHeight() != paramInt2))) {
            this.c = null;
          }
          Object localObject = this.c;
          if (localObject == null) {}
          ArrayList localArrayList;
          try
          {
            this.c = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
            localObject = new android/graphics/Canvas;
            ((Canvas)localObject).<init>(this.c);
            localArrayList = new java/util/ArrayList;
            localArrayList.<init>();
            Iterator localIterator = paramList.iterator();
            while (localIterator.hasNext())
            {
              View localView = (View)localIterator.next();
              a((Canvas)localObject, paramSet, localView);
              localArrayList.addAll(b(localView));
            }
          }
          catch (OutOfMemoryError paramSet)
          {
            Log.e(com.testfairy.e.a, com.testfairy.n.aI);
            this.c = null;
            this.b.a(null, null);
          }
          this.b.a(this.c, com.testfairy.p.r.a(paramList, paramSet, paramInt1, paramInt2), localArrayList);
        }
      }
    }
  }
  
  public void a(View paramView)
  {
    if (this.d) {
      return;
    }
    Object localObject1 = null;
    Object localObject2 = paramView.getClass().getDeclaredFields();
    int j = localObject2.length;
    int i = 0;
    label26:
    ConcurrentLinkedQueue localConcurrentLinkedQueue;
    if (i < j)
    {
      localConcurrentLinkedQueue = localObject2[i];
      if (!localConcurrentLinkedQueue.getType().getName().equals(com.testfairy.n.aE)) {
        break label145;
      }
      localConcurrentLinkedQueue.setAccessible(true);
    }
    label145:
    for (;;)
    {
      try
      {
        localConcurrentLinkedQueue = (ConcurrentLinkedQueue)localConcurrentLinkedQueue.get(paramView);
        localObject1 = localConcurrentLinkedQueue;
        i++;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        continue;
      }
      break label26;
      if (localObject1 == null) {
        break;
      }
      this.d = true;
      int[] arrayOfInt = new int[paramView.getWidth() * paramView.getHeight()];
      localObject2 = IntBuffer.wrap(arrayOfInt);
      ((IntBuffer)localObject2).position(0);
      ((ConcurrentLinkedQueue)localObject1).add(new c(paramView, arrayOfInt, (IntBuffer)localObject2));
      break;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  private static class a
  {
    public List a = new ArrayList();
  }
  
  public static abstract interface b
  {
    public abstract void a(Bitmap paramBitmap, q paramq);
    
    public abstract void a(Bitmap paramBitmap, q paramq, List paramList);
    
    public abstract void a(Canvas paramCanvas, GLSurfaceView paramGLSurfaceView);
  }
  
  private class c
    implements Runnable
  {
    private final View b;
    private final IntBuffer c;
    private int[] d;
    
    public c(View paramView, int[] paramArrayOfInt, IntBuffer paramIntBuffer)
    {
      this.b = paramView;
      this.c = paramIntBuffer;
      this.d = paramArrayOfInt;
    }
    
    public void run()
    {
      long l2 = System.currentTimeMillis();
      ((GL10)((EGL10)EGLContext.getEGL()).eglGetCurrentContext().getGL()).glReadPixels(0, 0, this.b.getWidth(), this.b.getHeight(), 6408, 5121, this.c);
      d locald = new d(this.d, this.b.getWidth(), this.b.getHeight());
      long l1 = System.currentTimeMillis();
      f.a(f.this).a(locald, l1 - l2);
      f.a(f.this, false);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */