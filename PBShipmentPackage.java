package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PBShipmentPackage
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -1574411492107576981L;
  @Expose
  @SerializedName("dimensionRequired")
  private Boolean dimensionRequired;
  @Expose
  @SerializedName("dimensionRules")
  private PBDimensionRules dimensionRules;
  @Expose
  @SerializedName("displayName")
  private String displayName;
  @Expose
  @SerializedName("packageId")
  private String packageId;
  @Expose
  @SerializedName("packageName")
  private String packageName;
  @Expose
  @SerializedName("rateType")
  private String rateType;
  @Expose
  @SerializedName("trackableRequred")
  private Boolean trackableRequired;
  @Expose
  @SerializedName("weightRequired")
  private Boolean weightRequired;
  @Expose
  @SerializedName("weightRules")
  private PBWeightRules weightRules;
  
  public PBShipmentPackage() {}
  
  public PBShipmentPackage(Boolean paramBoolean1, PBDimensionRules paramPBDimensionRules, String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean2, Boolean paramBoolean3, PBWeightRules paramPBWeightRules)
  {
    this.dimensionRequired = paramBoolean1;
    this.dimensionRules = paramPBDimensionRules;
    this.displayName = paramString1;
    this.packageId = paramString2;
    this.packageName = paramString3;
    this.rateType = paramString4;
    this.trackableRequired = paramBoolean2;
    this.weightRequired = paramBoolean3;
    this.weightRules = paramPBWeightRules;
  }
  
  public Boolean getDimensionRequired()
  {
    return this.dimensionRequired;
  }
  
  public PBDimensionRules getDimensionRules()
  {
    return this.dimensionRules;
  }
  
  public String getDisplayName()
  {
    return this.displayName;
  }
  
  public String getPackageId()
  {
    return this.packageId;
  }
  
  public String getPackageName()
  {
    return this.packageName;
  }
  
  public String getRateType()
  {
    return this.rateType;
  }
  
  public Boolean getTrackableRequired()
  {
    return this.trackableRequired;
  }
  
  public Boolean getWeightRequired()
  {
    return this.weightRequired;
  }
  
  public PBWeightRules getWeightRules()
  {
    return this.weightRules;
  }
  
  public void setDimensionRequired(Boolean paramBoolean)
  {
    this.dimensionRequired = paramBoolean;
  }
  
  public void setDimensionRules(PBDimensionRules paramPBDimensionRules)
  {
    this.dimensionRules = paramPBDimensionRules;
  }
  
  public void setDisplayName(String paramString)
  {
    this.displayName = paramString;
  }
  
  public void setPackageId(String paramString)
  {
    this.packageId = paramString;
  }
  
  public void setPackageName(String paramString)
  {
    this.packageName = paramString;
  }
  
  public void setRateType(String paramString)
  {
    this.rateType = paramString;
  }
  
  public void setTrackableRequired(Boolean paramBoolean)
  {
    this.trackableRequired = paramBoolean;
  }
  
  public void setWeightRequired(Boolean paramBoolean)
  {
    this.weightRequired = paramBoolean;
  }
  
  public void setWeightRules(PBWeightRules paramPBWeightRules)
  {
    this.weightRules = paramPBWeightRules;
  }
  
  public Map<String, Object> toJsonMap()
  {
    HashMap localHashMap = new HashMap();
    Object localObject8 = null;
    Object localObject5 = null;
    Object localObject7 = null;
    Object localObject9 = null;
    Object localObject6 = null;
    Object localObject4 = localObject9;
    Object localObject3 = localObject7;
    Object localObject2 = localObject8;
    Object localObject1 = localObject5;
    if (this.dimensionRules != null)
    {
      localObject4 = localObject9;
      localObject3 = localObject7;
      localObject2 = localObject8;
      localObject1 = localObject5;
      if (this.dimensionRequired.booleanValue())
      {
        localObject2 = Double.valueOf(this.dimensionRules.getLength());
        localObject1 = Double.valueOf(this.dimensionRules.getWidth());
        localObject3 = Double.valueOf(this.dimensionRules.getHeight());
        localObject4 = Double.valueOf(this.dimensionRules.getGirth());
      }
    }
    localObject5 = localObject6;
    if (this.weightRules != null)
    {
      localObject5 = localObject6;
      if (this.weightRequired.booleanValue()) {
        localObject5 = Double.valueOf(this.weightRules.getWeight());
      }
    }
    localHashMap.put("packageId", this.packageId);
    localHashMap.put("lengthMeters", localObject2);
    localHashMap.put("widthMeters", localObject1);
    localHashMap.put("heightMeters", localObject3);
    localHashMap.put("girthMeters", localObject4);
    localHashMap.put("weightKilos", localObject5);
    return localHashMap;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentPackage.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */