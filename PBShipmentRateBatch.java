package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PBShipmentRateBatch
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -3618188003837137276L;
  @Expose
  @SerializedName("variances")
  private List<String> variances = new ArrayList();
  
  public PBShipmentRateBatch() {}
  
  public PBShipmentRateBatch(List<String> paramList)
  {
    this.variances = paramList;
  }
  
  public List<String> getVariances()
  {
    return this.variances;
  }
  
  public void setVariances(List<String> paramList)
  {
    this.variances = paramList;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentRateBatch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */