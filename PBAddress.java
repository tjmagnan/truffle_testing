package tech.dcube.companion.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PBAddress
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -4575534136565700046L;
  private String addressType = "";
  private Boolean archived = Boolean.valueOf(false);
  private String city = "";
  private PBContact contact = new PBContact();
  private Integer id = Integer.valueOf(0);
  private String isoCountry = "";
  private String lastVerified = "";
  private Boolean militaryAddress = Boolean.valueOf(false);
  private String modifyDate = "";
  private String postalCode = "";
  private Boolean residential = Boolean.valueOf(false);
  private String state = "";
  private String streetLine1 = "";
  private String streetLine2 = "";
  private String streetLine3 = "";
  private Boolean verified = Boolean.valueOf(false);
  private Integer version = Integer.valueOf(0);
  
  public PBAddress()
  {
    this.addressType = "";
    this.archived = Boolean.valueOf(false);
    this.city = "";
    this.contact = new PBContact();
    this.id = Integer.valueOf(0);
    this.isoCountry = "";
    this.lastVerified = "";
    this.militaryAddress = Boolean.valueOf(false);
    this.modifyDate = "";
    this.postalCode = "";
    this.residential = Boolean.valueOf(false);
    this.state = "";
    this.streetLine1 = "";
    this.streetLine2 = "";
    this.streetLine3 = "";
    this.verified = Boolean.valueOf(false);
    this.version = Integer.valueOf(0);
  }
  
  public PBAddress(String paramString1, Boolean paramBoolean1, String paramString2, PBContact paramPBContact, Integer paramInteger1, String paramString3, String paramString4, Boolean paramBoolean2, String paramString5, String paramString6, Boolean paramBoolean3, String paramString7, String paramString8, String paramString9, String paramString10, Boolean paramBoolean4, Integer paramInteger2)
  {
    this.addressType = paramString1;
    this.archived = paramBoolean1;
    this.city = paramString2;
    this.contact = paramPBContact;
    this.id = paramInteger1;
    this.isoCountry = paramString3;
    this.lastVerified = paramString4;
    this.militaryAddress = paramBoolean2;
    this.modifyDate = paramString5;
    this.postalCode = paramString6;
    this.residential = paramBoolean3;
    this.state = paramString7;
    this.streetLine1 = paramString8;
    this.streetLine2 = paramString9;
    this.streetLine3 = paramString10;
    this.verified = paramBoolean4;
    this.version = paramInteger2;
  }
  
  public String getAddressType()
  {
    return this.addressType;
  }
  
  public Boolean getArchived()
  {
    return this.archived;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public PBContact getContact()
  {
    return this.contact;
  }
  
  public Integer getId()
  {
    return this.id;
  }
  
  public String getIsoCountry()
  {
    return this.isoCountry;
  }
  
  public String getLastVerified()
  {
    return this.lastVerified;
  }
  
  public Boolean getMilitaryAddress()
  {
    return this.militaryAddress;
  }
  
  public String getModifyDate()
  {
    return this.modifyDate;
  }
  
  public String getPostalCode()
  {
    return this.postalCode;
  }
  
  public Boolean getResidential()
  {
    return this.residential;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public String getStreetLine1()
  {
    return this.streetLine1;
  }
  
  public String getStreetLine2()
  {
    return this.streetLine2;
  }
  
  public String getStreetLine3()
  {
    return this.streetLine3;
  }
  
  public Boolean getVerified()
  {
    return this.verified;
  }
  
  public Integer getVersion()
  {
    return this.version;
  }
  
  public void setAddressType(String paramString)
  {
    this.addressType = paramString;
  }
  
  public void setArchived(Boolean paramBoolean)
  {
    this.archived = paramBoolean;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setContact(PBContact paramPBContact)
  {
    this.contact = paramPBContact;
  }
  
  public void setId(Integer paramInteger)
  {
    this.id = paramInteger;
  }
  
  public void setIsoCountry(String paramString)
  {
    this.isoCountry = paramString;
  }
  
  public void setLastVerified(String paramString)
  {
    this.lastVerified = paramString;
  }
  
  public void setMilitaryAddress(Boolean paramBoolean)
  {
    this.militaryAddress = paramBoolean;
  }
  
  public void setModifyDate(String paramString)
  {
    this.modifyDate = paramString;
  }
  
  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }
  
  public void setResidential(Boolean paramBoolean)
  {
    this.residential = paramBoolean;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
  
  public void setStreetLine1(String paramString)
  {
    this.streetLine1 = paramString;
  }
  
  public void setStreetLine2(String paramString)
  {
    this.streetLine2 = paramString;
  }
  
  public void setStreetLine3(String paramString)
  {
    this.streetLine3 = paramString;
  }
  
  public void setVerified(Boolean paramBoolean)
  {
    this.verified = paramBoolean;
  }
  
  public void setVersion(Integer paramInteger)
  {
    this.version = paramInteger;
  }
  
  public Map<String, Object> toJsonMap()
  {
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("type", this.addressType);
    localHashMap1.put("isoCountry", this.isoCountry);
    localHashMap1.put("fullName", this.contact.getFullName());
    localHashMap1.put("company", this.contact.getCompanyName());
    localHashMap1.put("streetLine1", this.streetLine1);
    localHashMap1.put("streetLine2", this.streetLine2);
    localHashMap1.put("streetLine3", this.streetLine3);
    localHashMap1.put("city", this.city);
    localHashMap1.put("state", this.state);
    localHashMap1.put("postalCode", this.postalCode);
    localHashMap1.put("residential", this.residential);
    localHashMap1.put("verified", this.verified);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("cultureCode", "en-us");
    localHashMap2.put("allowValidLastLine", Boolean.valueOf(true));
    localHashMap2.put("originCountry", this.isoCountry);
    localHashMap2.put("validatePostalCodeOnly", Boolean.valueOf(true));
    localHashMap2.put("address", localHashMap1);
    return localHashMap2;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */