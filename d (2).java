package com.testfairy.d;

public class b
{
  public static final long a = 150L;
  public static final long b = 32000L;
  private final long c;
  private long d;
  
  public b(long paramLong)
  {
    this.c = paramLong;
    this.d = paramLong;
  }
  
  public long a()
  {
    return this.d;
  }
  
  public void a(long paramLong)
  {
    if (paramLong > 150L) {
      this.d = Math.min(32000L, this.d * 2L);
    }
    for (;;)
    {
      return;
      if (paramLong < 150L) {
        this.d = Math.max(this.c, this.d / 2L);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\d\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */