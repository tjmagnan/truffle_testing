package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBTrackingDetails
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -7307333488328347669L;
  @Expose
  @SerializedName("city")
  private String city;
  @Expose
  @SerializedName("eventDate")
  private String eventDate;
  @Expose
  @SerializedName("isoCountry")
  private String isoCountry;
  @Expose
  @SerializedName("packageStatus")
  private String packageStatus;
  @Expose
  @SerializedName("postalCode")
  private String postalCode;
  @Expose
  @SerializedName("scanDescription")
  private String scanDescription;
  @Expose
  @SerializedName("state")
  private String state;
  
  public PBTrackingDetails() {}
  
  public PBTrackingDetails(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    this.city = paramString1;
    this.eventDate = paramString2;
    this.isoCountry = paramString3;
    this.packageStatus = paramString4;
    this.postalCode = paramString5;
    this.scanDescription = paramString6;
    this.state = paramString7;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getEventDate()
  {
    return this.eventDate;
  }
  
  public String getIsoCountry()
  {
    return this.isoCountry;
  }
  
  public String getPackageStatus()
  {
    return this.packageStatus;
  }
  
  public String getPostalCode()
  {
    return this.postalCode;
  }
  
  public String getScanDescription()
  {
    return this.scanDescription;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setEventDate(String paramString)
  {
    this.eventDate = paramString;
  }
  
  public void setIsoCountry(String paramString)
  {
    this.isoCountry = paramString;
  }
  
  public void setPackageStatus(String paramString)
  {
    this.packageStatus = paramString;
  }
  
  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }
  
  public void setScanDescription(String paramString)
  {
    this.scanDescription = paramString;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBTrackingDetails.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */