package me.zhanghai.android.materialprogressbar.internal;

import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.os.Build.VERSION;
import android.util.Property;

public class ObjectAnimatorCompat
{
  public static <T> ObjectAnimator ofArgb(T paramT, Property<T, Integer> paramProperty, int... paramVarArgs)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramT = ObjectAnimatorCompatLollipop.ofArgb(paramT, paramProperty, paramVarArgs);; paramT = ObjectAnimatorCompatBase.ofArgb(paramT, paramProperty, paramVarArgs)) {
      return paramT;
    }
  }
  
  public static ObjectAnimator ofArgb(Object paramObject, String paramString, int... paramVarArgs)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramObject = ObjectAnimatorCompatLollipop.ofArgb(paramObject, paramString, paramVarArgs);; paramObject = ObjectAnimatorCompatBase.ofArgb(paramObject, paramString, paramVarArgs)) {
      return (ObjectAnimator)paramObject;
    }
  }
  
  public static <T> ObjectAnimator ofFloat(T paramT, Property<T, Float> paramProperty1, Property<T, Float> paramProperty2, Path paramPath)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramT = ObjectAnimatorCompatLollipop.ofFloat(paramT, paramProperty1, paramProperty2, paramPath);; paramT = ObjectAnimatorCompatBase.ofFloat(paramT, paramProperty1, paramProperty2, paramPath)) {
      return paramT;
    }
  }
  
  public static ObjectAnimator ofFloat(Object paramObject, String paramString1, String paramString2, Path paramPath)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramObject = ObjectAnimatorCompatLollipop.ofFloat(paramObject, paramString1, paramString2, paramPath);; paramObject = ObjectAnimatorCompatBase.ofFloat(paramObject, paramString1, paramString2, paramPath)) {
      return (ObjectAnimator)paramObject;
    }
  }
  
  public static <T> ObjectAnimator ofInt(T paramT, Property<T, Integer> paramProperty1, Property<T, Integer> paramProperty2, Path paramPath)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramT = ObjectAnimatorCompatLollipop.ofInt(paramT, paramProperty1, paramProperty2, paramPath);; paramT = ObjectAnimatorCompatBase.ofInt(paramT, paramProperty1, paramProperty2, paramPath)) {
      return paramT;
    }
  }
  
  public static ObjectAnimator ofInt(Object paramObject, String paramString1, String paramString2, Path paramPath)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (paramObject = ObjectAnimatorCompatLollipop.ofInt(paramObject, paramString1, paramString2, paramPath);; paramObject = ObjectAnimatorCompatBase.ofInt(paramObject, paramString1, paramString2, paramPath)) {
      return (ObjectAnimator)paramObject;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\internal\ObjectAnimatorCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */