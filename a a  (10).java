package com.testfairy.activities.a.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import com.testfairy.FeedbackOptions;

public class j
  extends LinearLayout
{
  public static final String a = "Write your email";
  public static final String b = "feedback text";
  public static final String c = "your email";
  public static final String d = "screenshot_thumbnail";
  public static final String e = "Attach screenshot checkbox";
  private static final String f = "Write your feedback";
  private static final String g = "Include screenshot";
  private static final String h = "Click to edit screenshot";
  private a i;
  private TextView j;
  private ImageView k;
  private boolean l = false;
  private final EditText m;
  private final EditText n;
  
  public j(Context paramContext, String paramString, FeedbackOptions paramFeedbackOptions, Bitmap paramBitmap, final h paramh)
  {
    super(paramContext);
    setOrientation(1);
    this.i = new a(paramContext, paramh);
    addView(this.i);
    LinearLayout localLinearLayout = new LinearLayout(paramContext);
    localLinearLayout.setOrientation(1);
    this.m = new EditText(paramContext);
    this.m.setContentDescription("your email");
    this.m.setHint("Write your email");
    this.m.setInputType(33);
    if ((paramString != null) && (!paramString.isEmpty())) {
      this.m.setText(paramString);
    }
    if (!paramFeedbackOptions.a()) {
      this.m.setVisibility(8);
    }
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(25, 25, 25, 25);
    localLinearLayout.addView(this.m, localLayoutParams);
    this.n = new EditText(paramContext);
    this.n.setContentDescription("feedback text");
    this.n.setHint("Write your feedback");
    this.n.setInputType(147457);
    if (((paramString != null) && (!paramString.isEmpty())) || (!paramFeedbackOptions.a())) {
      this.n.requestFocus();
    }
    paramString = new LinearLayout.LayoutParams(-1, -2);
    paramString.setMargins(25, 25, 25, 25);
    localLinearLayout.addView(this.n, paramString);
    if (paramBitmap != null)
    {
      paramFeedbackOptions = new CheckBox(paramContext);
      paramFeedbackOptions.setText("Include screenshot");
      paramFeedbackOptions.setContentDescription("Attach screenshot checkbox");
      paramFeedbackOptions.setChecked(true);
      paramFeedbackOptions.setTextSize(11.0F);
      paramFeedbackOptions.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          j.a(j.this, (CheckBox)paramAnonymousView);
        }
      });
      paramString = new LinearLayout.LayoutParams(-2, -2);
      paramString.setMargins(37, 0, 25, 12);
      localLinearLayout.addView(paramFeedbackOptions, paramString);
      this.k = new ImageView(paramContext);
      this.k.setContentDescription("screenshot_thumbnail");
      this.k.setImageBitmap(paramBitmap);
      this.k.setScaleType(ImageView.ScaleType.CENTER_CROP);
      if (Build.VERSION.SDK_INT >= 16)
      {
        paramString = new GradientDrawable();
        paramString.setShape(0);
        paramString.setStroke(2, -16777216);
        this.k.setBackground(paramString);
      }
      this.k.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          paramh.c(paramAnonymousView);
        }
      });
      paramString = new LinearLayout.LayoutParams(paramContext.getResources().getDimensionPixelSize(17104897), paramContext.getResources().getDimensionPixelSize(17104897));
      paramString.setMargins(25, 0, 25, 0);
      localLinearLayout.addView(this.k, paramString);
      this.j = new TextView(paramContext);
      this.j.setText("Click to edit screenshot");
      this.j.setTextColor(Color.rgb(137, 166, 155));
      this.j.setTextSize(11.0F);
      this.j.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          paramh.c(paramAnonymousView);
        }
      });
      paramString = new LinearLayout.LayoutParams(-2, -2);
      paramString.setMargins(50, 25, 25, 25);
      localLinearLayout.addView(this.j, paramString);
    }
    paramContext = new ScrollView(paramContext);
    paramContext.addView(localLinearLayout);
    addView(paramContext);
  }
  
  private void a(CheckBox paramCheckBox)
  {
    if (paramCheckBox.isChecked()) {
      j();
    }
    for (this.l = false;; this.l = true)
    {
      return;
      i();
    }
  }
  
  private void a(final EditText paramEditText)
  {
    new Handler().postDelayed(new Runnable()
    {
      public void run()
      {
        paramEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 0, 0.0F, 0.0F, 0));
        paramEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 1, 0.0F, 0.0F, 0));
      }
    }, 200L);
  }
  
  private void i()
  {
    this.k.setVisibility(8);
    this.j.setVisibility(8);
  }
  
  private void j()
  {
    this.k.setVisibility(0);
    this.j.setVisibility(0);
  }
  
  public EditText a()
  {
    return this.m;
  }
  
  public void a(Bitmap paramBitmap)
  {
    this.k.setImageBitmap(paramBitmap);
  }
  
  public EditText b()
  {
    return this.n;
  }
  
  public void c()
  {
    setVisibility(8);
  }
  
  public void d()
  {
    setVisibility(0);
    this.n.requestFocus();
  }
  
  void e()
  {
    this.n.requestFocus();
    a(this.n);
  }
  
  void f()
  {
    this.m.requestFocus();
    a(this.m);
  }
  
  public boolean g()
  {
    return this.l;
  }
  
  public void h()
  {
    this.i.a();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\activities\a\a\j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */