package me.zhanghai.android.materialprogressbar;

import android.graphics.Canvas;
import android.graphics.Paint;

class SingleCircularProgressDrawable
  extends BaseSingleCircularProgressDrawable
  implements ShowBackgroundDrawable
{
  private static final int LEVEL_MAX = 10000;
  private static final float START_ANGLE_MAX_DYNAMIC = 360.0F;
  private static final float START_ANGLE_MAX_NORMAL = 0.0F;
  private static final float SWEEP_ANGLE_MAX = 360.0F;
  private boolean mShowBackground;
  private final float mStartAngleMax;
  
  SingleCircularProgressDrawable(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("Invalid value for style");
    }
    for (this.mStartAngleMax = 0.0F;; this.mStartAngleMax = 360.0F) {
      return;
    }
  }
  
  public boolean getShowBackground()
  {
    return this.mShowBackground;
  }
  
  protected void onDrawRing(Canvas paramCanvas, Paint paramPaint)
  {
    int i = getLevel();
    if (i == 0) {}
    for (;;)
    {
      return;
      float f2 = i / 10000.0F;
      float f1 = f2 * this.mStartAngleMax;
      f2 *= 360.0F;
      drawRing(paramCanvas, paramPaint, f1, f2);
      if (this.mShowBackground) {
        drawRing(paramCanvas, paramPaint, f1, f2);
      }
    }
  }
  
  protected boolean onLevelChange(int paramInt)
  {
    invalidateSelf();
    return true;
  }
  
  public void setShowBackground(boolean paramBoolean)
  {
    if (this.mShowBackground != paramBoolean)
    {
      this.mShowBackground = paramBoolean;
      invalidateSelf();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\SingleCircularProgressDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */