package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class InvalidSessionReport
  implements Report
{
  private final Map<String, String> customHeaders;
  private final File[] files;
  private final String identifier;
  
  public InvalidSessionReport(String paramString, File[] paramArrayOfFile)
  {
    this.files = paramArrayOfFile;
    this.customHeaders = new HashMap(ReportUploader.HEADER_INVALID_CLS_FILE);
    this.identifier = paramString;
  }
  
  public Map<String, String> getCustomHeaders()
  {
    return Collections.unmodifiableMap(this.customHeaders);
  }
  
  public File getFile()
  {
    return this.files[0];
  }
  
  public String getFileName()
  {
    return this.files[0].getName();
  }
  
  public File[] getFiles()
  {
    return this.files;
  }
  
  public String getIdentifier()
  {
    return this.identifier;
  }
  
  public void remove()
  {
    for (File localFile : this.files)
    {
      Fabric.getLogger().d("CrashlyticsCore", "Removing invalid report file at " + localFile.getPath());
      localFile.delete();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\InvalidSessionReport.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */