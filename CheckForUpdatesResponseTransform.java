package com.crashlytics.android.beta;

import java.io.IOException;
import org.json.JSONObject;

class CheckForUpdatesResponseTransform
{
  static final String BUILD_VERSION = "build_version";
  static final String DISPLAY_VERSION = "display_version";
  static final String IDENTIFIER = "identifier";
  static final String INSTANCE_IDENTIFIER = "instance_identifier";
  static final String URL = "url";
  static final String VERSION_STRING = "version_string";
  
  public CheckForUpdatesResponse fromJson(JSONObject paramJSONObject)
    throws IOException
  {
    String str1 = null;
    if (paramJSONObject == null) {}
    String str3;
    String str2;
    for (paramJSONObject = str1;; paramJSONObject = new CheckForUpdatesResponse(str1, str3, paramJSONObject.optString("display_version", null), str2, paramJSONObject.optString("identifier", null), paramJSONObject.optString("instance_identifier", null)))
    {
      return paramJSONObject;
      str1 = paramJSONObject.optString("url", null);
      str3 = paramJSONObject.optString("version_string", null);
      str2 = paramJSONObject.optString("build_version", null);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\beta\CheckForUpdatesResponseTransform.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */