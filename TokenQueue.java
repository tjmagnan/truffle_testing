package org.jsoup.parser;

import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;

public class TokenQueue
{
  private static final char ESC = '\\';
  private int pos = 0;
  private String queue;
  
  public TokenQueue(String paramString)
  {
    Validate.notNull(paramString);
    this.queue = paramString;
  }
  
  private int remainingLength()
  {
    return this.queue.length() - this.pos;
  }
  
  public static String unescape(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int j = 0;
    paramString = paramString.toCharArray();
    int k = paramString.length;
    int i = 0;
    if (i < k)
    {
      char c = paramString[i];
      if (c == '\\') {
        if ((j != 0) && (j == 92)) {
          localStringBuilder.append(c);
        }
      }
      for (;;)
      {
        j = c;
        i++;
        break;
        localStringBuilder.append(c);
      }
    }
    return localStringBuilder.toString();
  }
  
  public void addFirst(Character paramCharacter)
  {
    addFirst(paramCharacter.toString());
  }
  
  public void addFirst(String paramString)
  {
    this.queue = (paramString + this.queue.substring(this.pos));
    this.pos = 0;
  }
  
  public void advance()
  {
    if (!isEmpty()) {
      this.pos += 1;
    }
  }
  
  public String chompBalanced(char paramChar1, char paramChar2)
  {
    int k = -1;
    int j = -1;
    int i2 = 0;
    int i4 = 0;
    int m = 0;
    int i;
    if (isEmpty())
    {
      i = j;
      label25:
      if (i < 0) {
        break label293;
      }
    }
    label166:
    label171:
    label254:
    label293:
    for (Object localObject = this.queue.substring(k, i);; localObject = "")
    {
      return (String)localObject;
      localObject = Character.valueOf(consume());
      int n;
      int i3;
      int i1;
      if (i4 != 0)
      {
        n = i2;
        i3 = m;
        i1 = k;
        if (i4 == 92) {}
      }
      else
      {
        if (!((Character)localObject).equals(Character.valueOf('\'')))
        {
          i = m;
          if (!((Character)localObject).equals(Character.valueOf('"'))) {}
        }
        else
        {
          i = m;
          if (((Character)localObject).charValue() != paramChar1) {
            if (m != 0) {
              break label166;
            }
          }
        }
        for (i = 1;; i = 0)
        {
          if (i == 0) {
            break label171;
          }
          i1 = k;
          m = i;
          i = j;
          n = i2;
          i2 = n;
          j = i;
          k = i1;
          if (n > 0) {
            break;
          }
          k = i1;
          break label25;
        }
        if (!((Character)localObject).equals(Character.valueOf(paramChar1))) {
          break label254;
        }
        m = i2 + 1;
        n = m;
        i3 = i;
        i1 = k;
        if (k == -1)
        {
          i1 = this.pos;
          i3 = i;
          n = m;
        }
      }
      for (;;)
      {
        i = j;
        if (n > 0)
        {
          i = j;
          if (i4 != 0) {
            i = this.pos;
          }
        }
        i4 = ((Character)localObject).charValue();
        m = i3;
        break;
        n = i2;
        i3 = i;
        i1 = k;
        if (((Character)localObject).equals(Character.valueOf(paramChar2)))
        {
          n = i2 - 1;
          i3 = i;
          i1 = k;
        }
      }
    }
  }
  
  public String chompTo(String paramString)
  {
    String str = consumeTo(paramString);
    matchChomp(paramString);
    return str;
  }
  
  public String chompToIgnoreCase(String paramString)
  {
    String str = consumeToIgnoreCase(paramString);
    matchChomp(paramString);
    return str;
  }
  
  public char consume()
  {
    String str = this.queue;
    int i = this.pos;
    this.pos = (i + 1);
    return str.charAt(i);
  }
  
  public void consume(String paramString)
  {
    if (!matches(paramString)) {
      throw new IllegalStateException("Queue did not match expected sequence");
    }
    int i = paramString.length();
    if (i > remainingLength()) {
      throw new IllegalStateException("Queue not long enough to consume sequence");
    }
    this.pos += i;
  }
  
  public String consumeAttributeKey()
  {
    int i = this.pos;
    while (!isEmpty())
    {
      if (!matchesWord()) {
        if (!matchesAny(new char[] { 45, 95, 58 })) {
          break;
        }
      }
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public String consumeCssIdentifier()
  {
    int i = this.pos;
    while (!isEmpty())
    {
      if (!matchesWord()) {
        if (!matchesAny(new char[] { 45, 95 })) {
          break;
        }
      }
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public String consumeElementSelector()
  {
    int i = this.pos;
    while (!isEmpty())
    {
      if (!matchesWord()) {
        if (!matchesAny(new String[] { "*|", "|", "_", "-" })) {
          break;
        }
      }
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public String consumeTagName()
  {
    int i = this.pos;
    while (!isEmpty())
    {
      if (!matchesWord()) {
        if (!matchesAny(new char[] { 58, 95, 45 })) {
          break;
        }
      }
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public String consumeTo(String paramString)
  {
    int i = this.queue.indexOf(paramString, this.pos);
    if (i != -1)
    {
      paramString = this.queue.substring(this.pos, i);
      this.pos += paramString.length();
    }
    for (;;)
    {
      return paramString;
      paramString = remainder();
    }
  }
  
  public String consumeToAny(String... paramVarArgs)
  {
    int i = this.pos;
    while ((!isEmpty()) && (!matchesAny(paramVarArgs))) {
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public String consumeToIgnoreCase(String paramString)
  {
    int j = this.pos;
    String str = paramString.substring(0, 1);
    boolean bool = str.toLowerCase().equals(str.toUpperCase());
    for (;;)
    {
      if ((isEmpty()) || (matches(paramString))) {
        return this.queue.substring(j, this.pos);
      }
      if (bool)
      {
        int i = this.queue.indexOf(str, this.pos) - this.pos;
        if (i == 0) {
          this.pos += 1;
        } else if (i < 0) {
          this.pos = this.queue.length();
        } else {
          this.pos += i;
        }
      }
      else
      {
        this.pos += 1;
      }
    }
  }
  
  public boolean consumeWhitespace()
  {
    for (boolean bool = false; matchesWhitespace(); bool = true) {
      this.pos += 1;
    }
    return bool;
  }
  
  public String consumeWord()
  {
    int i = this.pos;
    while (matchesWord()) {
      this.pos += 1;
    }
    return this.queue.substring(i, this.pos);
  }
  
  public boolean isEmpty()
  {
    if (remainingLength() == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean matchChomp(String paramString)
  {
    if (matches(paramString)) {
      this.pos += paramString.length();
    }
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean matches(String paramString)
  {
    return this.queue.regionMatches(true, this.pos, paramString, 0, paramString.length());
  }
  
  public boolean matchesAny(char... paramVarArgs)
  {
    boolean bool2 = false;
    boolean bool1;
    if (isEmpty())
    {
      bool1 = bool2;
      return bool1;
    }
    int j = paramVarArgs.length;
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= j) {
        break;
      }
      int k = paramVarArgs[i];
      if (this.queue.charAt(this.pos) == k)
      {
        bool1 = true;
        break;
      }
    }
  }
  
  public boolean matchesAny(String... paramVarArgs)
  {
    boolean bool2 = false;
    int j = paramVarArgs.length;
    for (int i = 0;; i++)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (matches(paramVarArgs[i])) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  public boolean matchesCS(String paramString)
  {
    return this.queue.startsWith(paramString, this.pos);
  }
  
  public boolean matchesStartTag()
  {
    if ((remainingLength() >= 2) && (this.queue.charAt(this.pos) == '<') && (Character.isLetter(this.queue.charAt(this.pos + 1)))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean matchesWhitespace()
  {
    if ((!isEmpty()) && (StringUtil.isWhitespace(this.queue.charAt(this.pos)))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean matchesWord()
  {
    if ((!isEmpty()) && (Character.isLetterOrDigit(this.queue.charAt(this.pos)))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public char peek()
  {
    if (isEmpty()) {}
    for (char c = '\000';; c = this.queue.charAt(this.pos)) {
      return c;
    }
  }
  
  public String remainder()
  {
    String str = this.queue.substring(this.pos, this.queue.length());
    this.pos = this.queue.length();
    return str;
  }
  
  public String toString()
  {
    return this.queue.substring(this.pos);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\TokenQueue.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */