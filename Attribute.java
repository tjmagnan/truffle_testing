package org.jsoup.nodes;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map.Entry;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;

public class Attribute
  implements Map.Entry<String, String>, Cloneable
{
  private static final String[] booleanAttributes = { "allowfullscreen", "async", "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", "hidden", "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch" };
  private String key;
  private String value;
  
  public Attribute(String paramString1, String paramString2)
  {
    Validate.notEmpty(paramString1);
    Validate.notNull(paramString2);
    this.key = paramString1.trim();
    this.value = paramString2;
  }
  
  public static Attribute createFromEncoded(String paramString1, String paramString2)
  {
    return new Attribute(paramString1, Entities.unescape(paramString2, true));
  }
  
  public Attribute clone()
  {
    try
    {
      Attribute localAttribute = (Attribute)super.clone();
      return localAttribute;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      throw new RuntimeException(localCloneNotSupportedException);
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (!(paramObject instanceof Attribute))
      {
        bool = false;
      }
      else
      {
        paramObject = (Attribute)paramObject;
        if (this.key != null)
        {
          if (this.key.equals(((Attribute)paramObject).key)) {}
        }
        else {
          while (((Attribute)paramObject).key != null)
          {
            bool = false;
            break;
          }
        }
        if (this.value != null)
        {
          if (this.value.equals(((Attribute)paramObject).value)) {}
        }
        else {
          while (((Attribute)paramObject).value != null)
          {
            bool = false;
            break;
          }
        }
      }
    }
  }
  
  public String getKey()
  {
    return this.key;
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public int hashCode()
  {
    int j = 0;
    if (this.key != null) {}
    for (int i = this.key.hashCode();; i = 0)
    {
      if (this.value != null) {
        j = this.value.hashCode();
      }
      return i * 31 + j;
    }
  }
  
  public String html()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      Document localDocument = new org/jsoup/nodes/Document;
      localDocument.<init>("");
      html(localStringBuilder, localDocument.outputSettings());
      return localStringBuilder.toString();
    }
    catch (IOException localIOException)
    {
      throw new SerializationException(localIOException);
    }
  }
  
  protected void html(Appendable paramAppendable, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    paramAppendable.append(this.key);
    if (!shouldCollapseAttribute(paramOutputSettings))
    {
      paramAppendable.append("=\"");
      Entities.escape(paramAppendable, this.value, paramOutputSettings, true, false, false);
      paramAppendable.append('"');
    }
  }
  
  protected boolean isBooleanAttribute()
  {
    if (Arrays.binarySearch(booleanAttributes, this.key) >= 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected boolean isDataAttribute()
  {
    if ((this.key.startsWith("data-")) && (this.key.length() > "data-".length())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void setKey(String paramString)
  {
    Validate.notEmpty(paramString);
    this.key = paramString.trim();
  }
  
  public String setValue(String paramString)
  {
    Validate.notNull(paramString);
    String str = this.value;
    this.value = paramString;
    return str;
  }
  
  protected final boolean shouldCollapseAttribute(Document.OutputSettings paramOutputSettings)
  {
    if ((("".equals(this.value)) || (this.value.equalsIgnoreCase(this.key))) && (paramOutputSettings.syntax() == Document.OutputSettings.Syntax.html) && (isBooleanAttribute())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public String toString()
  {
    return html();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\Attribute.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */