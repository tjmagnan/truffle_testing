package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.helper.Validate;

public class XmlDeclaration
  extends Node
{
  private final boolean isProcessingInstruction;
  private final String name;
  
  public XmlDeclaration(String paramString1, String paramString2, boolean paramBoolean)
  {
    super(paramString2);
    Validate.notNull(paramString1);
    this.name = paramString1;
    this.isProcessingInstruction = paramBoolean;
  }
  
  public String getWholeDeclaration()
  {
    return this.attributes.html().trim();
  }
  
  public String name()
  {
    return this.name;
  }
  
  public String nodeName()
  {
    return "#declaration";
  }
  
  void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    Appendable localAppendable = paramAppendable.append("<");
    String str;
    if (this.isProcessingInstruction)
    {
      str = "!";
      localAppendable.append(str).append(this.name);
      this.attributes.html(paramAppendable, paramOutputSettings);
      if (!this.isProcessingInstruction) {
        break label82;
      }
    }
    label82:
    for (paramOutputSettings = "!";; paramOutputSettings = "?")
    {
      paramAppendable.append(paramOutputSettings).append(">");
      return;
      str = "?";
      break;
    }
  }
  
  void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings) {}
  
  public String toString()
  {
    return outerHtml();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\XmlDeclaration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */