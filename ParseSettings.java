package org.jsoup.parser;

import java.util.Iterator;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;

public class ParseSettings
{
  public static final ParseSettings htmlDefault = new ParseSettings(false, false);
  public static final ParseSettings preserveCase = new ParseSettings(true, true);
  private final boolean preserveAttributeCase;
  private final boolean preserveTagCase;
  
  public ParseSettings(boolean paramBoolean1, boolean paramBoolean2)
  {
    this.preserveTagCase = paramBoolean1;
    this.preserveAttributeCase = paramBoolean2;
  }
  
  String normalizeAttribute(String paramString)
  {
    String str = paramString.trim();
    paramString = str;
    if (!this.preserveAttributeCase) {
      paramString = str.toLowerCase();
    }
    return paramString;
  }
  
  Attributes normalizeAttributes(Attributes paramAttributes)
  {
    if (!this.preserveAttributeCase)
    {
      Iterator localIterator = paramAttributes.iterator();
      while (localIterator.hasNext())
      {
        Attribute localAttribute = (Attribute)localIterator.next();
        localAttribute.setKey(localAttribute.getKey().toLowerCase());
      }
    }
    return paramAttributes;
  }
  
  String normalizeTag(String paramString)
  {
    String str = paramString.trim();
    paramString = str;
    if (!this.preserveTagCase) {
      paramString = str.toLowerCase();
    }
    return paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\ParseSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */