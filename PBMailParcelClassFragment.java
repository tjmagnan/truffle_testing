package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.android.volley.VolleyError;
import java.util.List;
import tech.dcube.companion.customListAdapters.MailServiceItemListAdapter;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentService;

public class PBMailParcelClassFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  public static final String FIRST_CLASS_MAIL = "FCM";
  public static final String LIBRARY_MAIL = "LIB";
  public static final String MEDIA_MAIL = "MEDIA";
  public static final String PRACEL_SELECT_GROUND = "PRCLSEL";
  public static final String PRIORITY_MAIL = "PM";
  public static final String PRIORITY_MAIL_EXPRESS = "EM";
  ListView MailServiceListView;
  private OnMailParcelClassListener mListener;
  private String mParam1;
  private String mParam2;
  MailServiceItemListAdapter mailServiceListAdapter;
  List<PBShipmentService> shipmentServiceList;
  
  public static PBMailParcelClassFragment newInstance(String paramString1, String paramString2)
  {
    PBMailParcelClassFragment localPBMailParcelClassFragment = new PBMailParcelClassFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBMailParcelClassFragment.setArguments(localBundle);
    return localPBMailParcelClassFragment;
  }
  
  void callMailServicesAPI()
  {
    PBAddress localPBAddress1 = DataManager.getInstance().getCurrentShipment().getSenderAddress();
    PBAddress localPBAddress2 = DataManager.getInstance().getCurrentShipment().getReceiverAddress();
    PBShipmentPackage localPBShipmentPackage = DataManager.getInstance().getCurrentShipment().getShipmentPackage();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local2 = new tech/dcube/companion/fragments/PBMailParcelClassFragment$2;
      local2.<init>(this);
      localServerManager.getServices(localContext, localPBAddress1, localPBAddress2, localPBShipmentPackage, local2);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnMailParcelClassListener))
    {
      this.mListener = ((OnMailParcelClassListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnMailParcelClassListener");
  }
  
  public void onClickTopBarLeftButton()
  {
    DataManager.getInstance().getServices().clear();
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968633, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Select Mail Service");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    DataManager.getInstance().getServices().clear();
    this.MailServiceListView = ((ListView)paramLayoutInflater.findViewById(2131624131));
    this.shipmentServiceList = DataManager.getInstance().getServices();
    this.mailServiceListAdapter = new MailServiceItemListAdapter(this.shipmentServiceList, getContext());
    this.MailServiceListView.setAdapter(this.mailServiceListAdapter);
    this.MailServiceListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = (PBShipmentService)DataManager.getInstance().getServices().get(paramAnonymousInt);
        PBMailParcelClassFragment.this.onMailClassSelected(paramAnonymousAdapterView);
      }
    });
    callMailServicesAPI();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onMailClassSelected(PBShipmentService paramPBShipmentService)
  {
    if (this.mListener != null) {
      this.mListener.onMailParcelClassSelected("action_mail_class_selected", paramPBShipmentService);
    }
  }
  
  public static abstract interface OnMailParcelClassListener
  {
    public abstract void onMailParcelClassSelected(String paramString, PBShipmentService paramPBShipmentService);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBMailParcelClassFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */