package me.dm7.barcodescanner.core;

import android.graphics.Rect;

public abstract interface IViewFinder
{
  public abstract Rect getFramingRect();
  
  public abstract int getHeight();
  
  public abstract int getWidth();
  
  public abstract void setBorderAlpha(float paramFloat);
  
  public abstract void setBorderColor(int paramInt);
  
  public abstract void setBorderCornerRadius(int paramInt);
  
  public abstract void setBorderCornerRounded(boolean paramBoolean);
  
  public abstract void setBorderLineLength(int paramInt);
  
  public abstract void setBorderStrokeWidth(int paramInt);
  
  public abstract void setLaserColor(int paramInt);
  
  public abstract void setLaserEnabled(boolean paramBoolean);
  
  public abstract void setMaskColor(int paramInt);
  
  public abstract void setSquareViewFinder(boolean paramBoolean);
  
  public abstract void setViewFinderOffset(int paramInt);
  
  public abstract void setupViewFinder();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\IViewFinder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */