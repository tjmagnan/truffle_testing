package org.joda.time.format;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTimeFieldType;

public class ISODateTimeFormat
{
  private static void appendSeparator(DateTimeFormatterBuilder paramDateTimeFormatterBuilder, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramDateTimeFormatterBuilder.appendLiteral('-');
    }
  }
  
  public static DateTimeFormatter basicDate()
  {
    return Constants.bd;
  }
  
  public static DateTimeFormatter basicDateTime()
  {
    return Constants.bdt;
  }
  
  public static DateTimeFormatter basicDateTimeNoMillis()
  {
    return Constants.bdtx;
  }
  
  public static DateTimeFormatter basicOrdinalDate()
  {
    return Constants.bod;
  }
  
  public static DateTimeFormatter basicOrdinalDateTime()
  {
    return Constants.bodt;
  }
  
  public static DateTimeFormatter basicOrdinalDateTimeNoMillis()
  {
    return Constants.bodtx;
  }
  
  public static DateTimeFormatter basicTTime()
  {
    return Constants.btt;
  }
  
  public static DateTimeFormatter basicTTimeNoMillis()
  {
    return Constants.bttx;
  }
  
  public static DateTimeFormatter basicTime()
  {
    return Constants.bt;
  }
  
  public static DateTimeFormatter basicTimeNoMillis()
  {
    return Constants.btx;
  }
  
  public static DateTimeFormatter basicWeekDate()
  {
    return Constants.bwd;
  }
  
  public static DateTimeFormatter basicWeekDateTime()
  {
    return Constants.bwdt;
  }
  
  public static DateTimeFormatter basicWeekDateTimeNoMillis()
  {
    return Constants.bwdtx;
  }
  
  private static void checkNotStrictISO(Collection<DateTimeFieldType> paramCollection, boolean paramBoolean)
  {
    if (paramBoolean) {
      throw new IllegalArgumentException("No valid ISO8601 format for fields: " + paramCollection);
    }
  }
  
  public static DateTimeFormatter date()
  {
    return yearMonthDay();
  }
  
  private static boolean dateByMonth(DateTimeFormatterBuilder paramDateTimeFormatterBuilder, Collection<DateTimeFieldType> paramCollection, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = false;
    if (paramCollection.remove(DateTimeFieldType.year()))
    {
      paramDateTimeFormatterBuilder.append(Constants.ye);
      if (paramCollection.remove(DateTimeFieldType.monthOfYear())) {
        if (paramCollection.remove(DateTimeFieldType.dayOfMonth()))
        {
          appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
          paramDateTimeFormatterBuilder.appendMonthOfYear(2);
          appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
          paramDateTimeFormatterBuilder.appendDayOfMonth(2);
          paramBoolean1 = bool;
        }
      }
    }
    for (;;)
    {
      return paramBoolean1;
      paramDateTimeFormatterBuilder.appendLiteral('-');
      paramDateTimeFormatterBuilder.appendMonthOfYear(2);
      paramBoolean1 = true;
      continue;
      if (paramCollection.remove(DateTimeFieldType.dayOfMonth()))
      {
        checkNotStrictISO(paramCollection, paramBoolean2);
        paramDateTimeFormatterBuilder.appendLiteral('-');
        paramDateTimeFormatterBuilder.appendLiteral('-');
        paramDateTimeFormatterBuilder.appendDayOfMonth(2);
        paramBoolean1 = bool;
      }
      else
      {
        paramBoolean1 = true;
        continue;
        if (paramCollection.remove(DateTimeFieldType.monthOfYear()))
        {
          paramDateTimeFormatterBuilder.appendLiteral('-');
          paramDateTimeFormatterBuilder.appendLiteral('-');
          paramDateTimeFormatterBuilder.appendMonthOfYear(2);
          if (paramCollection.remove(DateTimeFieldType.dayOfMonth()))
          {
            appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
            paramDateTimeFormatterBuilder.appendDayOfMonth(2);
            paramBoolean1 = bool;
          }
          else
          {
            paramBoolean1 = true;
          }
        }
        else
        {
          paramBoolean1 = bool;
          if (paramCollection.remove(DateTimeFieldType.dayOfMonth()))
          {
            paramDateTimeFormatterBuilder.appendLiteral('-');
            paramDateTimeFormatterBuilder.appendLiteral('-');
            paramDateTimeFormatterBuilder.appendLiteral('-');
            paramDateTimeFormatterBuilder.appendDayOfMonth(2);
            paramBoolean1 = bool;
          }
        }
      }
    }
  }
  
  private static boolean dateByOrdinal(DateTimeFormatterBuilder paramDateTimeFormatterBuilder, Collection<DateTimeFieldType> paramCollection, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramBoolean2 = false;
    if (paramCollection.remove(DateTimeFieldType.year()))
    {
      paramDateTimeFormatterBuilder.append(Constants.ye);
      if (paramCollection.remove(DateTimeFieldType.dayOfYear()))
      {
        appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
        paramDateTimeFormatterBuilder.appendDayOfYear(3);
        paramBoolean1 = paramBoolean2;
      }
    }
    for (;;)
    {
      return paramBoolean1;
      paramBoolean1 = true;
      continue;
      paramBoolean1 = paramBoolean2;
      if (paramCollection.remove(DateTimeFieldType.dayOfYear()))
      {
        paramDateTimeFormatterBuilder.appendLiteral('-');
        paramDateTimeFormatterBuilder.appendDayOfYear(3);
        paramBoolean1 = paramBoolean2;
      }
    }
  }
  
  private static boolean dateByWeek(DateTimeFormatterBuilder paramDateTimeFormatterBuilder, Collection<DateTimeFieldType> paramCollection, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = false;
    if (paramCollection.remove(DateTimeFieldType.weekyear()))
    {
      paramDateTimeFormatterBuilder.append(Constants.we);
      if (paramCollection.remove(DateTimeFieldType.weekOfWeekyear()))
      {
        appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
        paramDateTimeFormatterBuilder.appendLiteral('W');
        paramDateTimeFormatterBuilder.appendWeekOfWeekyear(2);
        if (paramCollection.remove(DateTimeFieldType.dayOfWeek()))
        {
          appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
          paramDateTimeFormatterBuilder.appendDayOfWeek(1);
          paramBoolean1 = bool;
        }
      }
    }
    for (;;)
    {
      return paramBoolean1;
      paramBoolean1 = true;
      continue;
      if (paramCollection.remove(DateTimeFieldType.dayOfWeek()))
      {
        checkNotStrictISO(paramCollection, paramBoolean2);
        appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
        paramDateTimeFormatterBuilder.appendLiteral('W');
        paramDateTimeFormatterBuilder.appendLiteral('-');
        paramDateTimeFormatterBuilder.appendDayOfWeek(1);
        paramBoolean1 = bool;
      }
      else
      {
        paramBoolean1 = true;
        continue;
        if (paramCollection.remove(DateTimeFieldType.weekOfWeekyear()))
        {
          paramDateTimeFormatterBuilder.appendLiteral('-');
          paramDateTimeFormatterBuilder.appendLiteral('W');
          paramDateTimeFormatterBuilder.appendWeekOfWeekyear(2);
          if (paramCollection.remove(DateTimeFieldType.dayOfWeek()))
          {
            appendSeparator(paramDateTimeFormatterBuilder, paramBoolean1);
            paramDateTimeFormatterBuilder.appendDayOfWeek(1);
            paramBoolean1 = bool;
          }
          else
          {
            paramBoolean1 = true;
          }
        }
        else
        {
          paramBoolean1 = bool;
          if (paramCollection.remove(DateTimeFieldType.dayOfWeek()))
          {
            paramDateTimeFormatterBuilder.appendLiteral('-');
            paramDateTimeFormatterBuilder.appendLiteral('W');
            paramDateTimeFormatterBuilder.appendLiteral('-');
            paramDateTimeFormatterBuilder.appendDayOfWeek(1);
            paramBoolean1 = bool;
          }
        }
      }
    }
  }
  
  public static DateTimeFormatter dateElementParser()
  {
    return Constants.dpe;
  }
  
  public static DateTimeFormatter dateHour()
  {
    return Constants.dh;
  }
  
  public static DateTimeFormatter dateHourMinute()
  {
    return Constants.dhm;
  }
  
  public static DateTimeFormatter dateHourMinuteSecond()
  {
    return Constants.dhms;
  }
  
  public static DateTimeFormatter dateHourMinuteSecondFraction()
  {
    return Constants.dhmsf;
  }
  
  public static DateTimeFormatter dateHourMinuteSecondMillis()
  {
    return Constants.dhmsl;
  }
  
  public static DateTimeFormatter dateOptionalTimeParser()
  {
    return Constants.dotp;
  }
  
  public static DateTimeFormatter dateParser()
  {
    return Constants.dp;
  }
  
  public static DateTimeFormatter dateTime()
  {
    return Constants.dt;
  }
  
  public static DateTimeFormatter dateTimeNoMillis()
  {
    return Constants.dtx;
  }
  
  public static DateTimeFormatter dateTimeParser()
  {
    return Constants.dtp;
  }
  
  public static DateTimeFormatter forFields(Collection<DateTimeFieldType> paramCollection, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((paramCollection == null) || (paramCollection.size() == 0)) {
      throw new IllegalArgumentException("The fields must not be null or empty");
    }
    HashSet localHashSet = new HashSet(paramCollection);
    int i = localHashSet.size();
    DateTimeFormatterBuilder localDateTimeFormatterBuilder = new DateTimeFormatterBuilder();
    boolean bool1;
    if (localHashSet.contains(DateTimeFieldType.monthOfYear())) {
      bool1 = dateByMonth(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2);
    }
    for (;;)
    {
      if (localHashSet.size() < i) {}
      for (boolean bool2 = true;; bool2 = false)
      {
        time(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2, bool1, bool2);
        if (localDateTimeFormatterBuilder.canBuildFormatter()) {
          break label306;
        }
        throw new IllegalArgumentException("No valid format for fields: " + paramCollection);
        if (localHashSet.contains(DateTimeFieldType.dayOfYear()))
        {
          bool1 = dateByOrdinal(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2);
          break;
        }
        if (localHashSet.contains(DateTimeFieldType.weekOfWeekyear()))
        {
          bool1 = dateByWeek(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2);
          break;
        }
        if (localHashSet.contains(DateTimeFieldType.dayOfMonth()))
        {
          bool1 = dateByMonth(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2);
          break;
        }
        if (localHashSet.contains(DateTimeFieldType.dayOfWeek()))
        {
          bool1 = dateByWeek(localDateTimeFormatterBuilder, localHashSet, paramBoolean1, paramBoolean2);
          break;
        }
        if (localHashSet.remove(DateTimeFieldType.year()))
        {
          localDateTimeFormatterBuilder.append(Constants.ye);
          bool1 = true;
          break;
        }
        if (!localHashSet.remove(DateTimeFieldType.weekyear())) {
          break label325;
        }
        localDateTimeFormatterBuilder.append(Constants.we);
        bool1 = true;
        break;
      }
      try
      {
        label306:
        paramCollection.retainAll(localHashSet);
        return localDateTimeFormatterBuilder.toFormatter();
      }
      catch (UnsupportedOperationException paramCollection)
      {
        for (;;) {}
      }
      label325:
      bool1 = false;
    }
  }
  
  public static DateTimeFormatter hour()
  {
    return Constants.hde;
  }
  
  public static DateTimeFormatter hourMinute()
  {
    return Constants.hm;
  }
  
  public static DateTimeFormatter hourMinuteSecond()
  {
    return Constants.hms;
  }
  
  public static DateTimeFormatter hourMinuteSecondFraction()
  {
    return Constants.hmsf;
  }
  
  public static DateTimeFormatter hourMinuteSecondMillis()
  {
    return Constants.hmsl;
  }
  
  public static DateTimeFormatter localDateOptionalTimeParser()
  {
    return Constants.ldotp;
  }
  
  public static DateTimeFormatter localDateParser()
  {
    return Constants.ldp;
  }
  
  public static DateTimeFormatter localTimeParser()
  {
    return Constants.ltp;
  }
  
  public static DateTimeFormatter ordinalDate()
  {
    return Constants.od;
  }
  
  public static DateTimeFormatter ordinalDateTime()
  {
    return Constants.odt;
  }
  
  public static DateTimeFormatter ordinalDateTimeNoMillis()
  {
    return Constants.odtx;
  }
  
  public static DateTimeFormatter tTime()
  {
    return Constants.tt;
  }
  
  public static DateTimeFormatter tTimeNoMillis()
  {
    return Constants.ttx;
  }
  
  public static DateTimeFormatter time()
  {
    return Constants.t;
  }
  
  private static void time(DateTimeFormatterBuilder paramDateTimeFormatterBuilder, Collection<DateTimeFieldType> paramCollection, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    boolean bool4 = paramCollection.remove(DateTimeFieldType.hourOfDay());
    boolean bool3 = paramCollection.remove(DateTimeFieldType.minuteOfHour());
    boolean bool1 = paramCollection.remove(DateTimeFieldType.secondOfMinute());
    boolean bool2 = paramCollection.remove(DateTimeFieldType.millisOfSecond());
    if ((!bool4) && (!bool3) && (!bool1) && (!bool2)) {}
    label164:
    label175:
    label207:
    label358:
    label383:
    label403:
    label416:
    for (;;)
    {
      return;
      if ((bool4) || (bool3) || (bool1) || (bool2))
      {
        if ((paramBoolean2) && (paramBoolean3)) {
          throw new IllegalArgumentException("No valid ISO8601 format for fields because Date was reduced precision: " + paramCollection);
        }
        if (paramBoolean4) {
          paramDateTimeFormatterBuilder.appendLiteral('T');
        }
      }
      if (((bool4) && (bool3) && (bool1)) || ((bool4) && (!bool1) && (!bool2)))
      {
        if (!bool4) {
          break label358;
        }
        paramDateTimeFormatterBuilder.appendHourOfDay(2);
        if ((paramBoolean1) && (bool4) && (bool3)) {
          paramDateTimeFormatterBuilder.appendLiteral(':');
        }
        if (!bool3) {
          break label383;
        }
        paramDateTimeFormatterBuilder.appendMinuteOfHour(2);
        if ((paramBoolean1) && (bool3) && (bool1)) {
          paramDateTimeFormatterBuilder.appendLiteral(':');
        }
        if (!bool1) {
          break label403;
        }
        paramDateTimeFormatterBuilder.appendSecondOfMinute(2);
      }
      for (;;)
      {
        if (!bool2) {
          break label416;
        }
        paramDateTimeFormatterBuilder.appendLiteral('.');
        paramDateTimeFormatterBuilder.appendMillisOfSecond(3);
        break;
        if ((paramBoolean2) && (paramBoolean4)) {
          throw new IllegalArgumentException("No valid ISO8601 format for fields because Time was truncated: " + paramCollection);
        }
        if (((!bool4) && (((bool3) && (bool1)) || ((bool3) && (!bool2)) || (bool1))) || (!paramBoolean2)) {
          break label164;
        }
        throw new IllegalArgumentException("No valid ISO8601 format for fields: " + paramCollection);
        if ((!bool3) && (!bool1) && (!bool2)) {
          break label175;
        }
        paramDateTimeFormatterBuilder.appendLiteral('-');
        break label175;
        if ((!bool1) && (!bool2)) {
          break label207;
        }
        paramDateTimeFormatterBuilder.appendLiteral('-');
        break label207;
        if (bool2) {
          paramDateTimeFormatterBuilder.appendLiteral('-');
        }
      }
    }
  }
  
  public static DateTimeFormatter timeElementParser()
  {
    return Constants.tpe;
  }
  
  public static DateTimeFormatter timeNoMillis()
  {
    return Constants.tx;
  }
  
  public static DateTimeFormatter timeParser()
  {
    return Constants.tp;
  }
  
  public static DateTimeFormatter weekDate()
  {
    return Constants.wwd;
  }
  
  public static DateTimeFormatter weekDateTime()
  {
    return Constants.wdt;
  }
  
  public static DateTimeFormatter weekDateTimeNoMillis()
  {
    return Constants.wdtx;
  }
  
  public static DateTimeFormatter weekyear()
  {
    return Constants.we;
  }
  
  public static DateTimeFormatter weekyearWeek()
  {
    return Constants.ww;
  }
  
  public static DateTimeFormatter weekyearWeekDay()
  {
    return Constants.wwd;
  }
  
  public static DateTimeFormatter year()
  {
    return Constants.ye;
  }
  
  public static DateTimeFormatter yearMonth()
  {
    return Constants.ym;
  }
  
  public static DateTimeFormatter yearMonthDay()
  {
    return Constants.ymd;
  }
  
  static final class Constants
  {
    private static final DateTimeFormatter bd;
    private static final DateTimeFormatter bdt;
    private static final DateTimeFormatter bdtx;
    private static final DateTimeFormatter bod;
    private static final DateTimeFormatter bodt;
    private static final DateTimeFormatter bodtx;
    private static final DateTimeFormatter bt;
    private static final DateTimeFormatter btt;
    private static final DateTimeFormatter bttx;
    private static final DateTimeFormatter btx;
    private static final DateTimeFormatter bwd;
    private static final DateTimeFormatter bwdt;
    private static final DateTimeFormatter bwdtx;
    private static final DateTimeFormatter dh;
    private static final DateTimeFormatter dhm;
    private static final DateTimeFormatter dhms;
    private static final DateTimeFormatter dhmsf;
    private static final DateTimeFormatter dhmsl;
    private static final DateTimeFormatter dme;
    private static final DateTimeFormatter dotp = dateOptionalTimeParser();
    private static final DateTimeFormatter dp;
    private static final DateTimeFormatter dpe;
    private static final DateTimeFormatter dt;
    private static final DateTimeFormatter dtp;
    private static final DateTimeFormatter dtx;
    private static final DateTimeFormatter dwe;
    private static final DateTimeFormatter dye;
    private static final DateTimeFormatter fse;
    private static final DateTimeFormatter hde;
    private static final DateTimeFormatter hm;
    private static final DateTimeFormatter hms;
    private static final DateTimeFormatter hmsf;
    private static final DateTimeFormatter hmsl;
    private static final DateTimeFormatter ldotp = localDateOptionalTimeParser();
    private static final DateTimeFormatter ldp;
    private static final DateTimeFormatter lte;
    private static final DateTimeFormatter ltp;
    private static final DateTimeFormatter mhe;
    private static final DateTimeFormatter mye;
    private static final DateTimeFormatter od;
    private static final DateTimeFormatter odt;
    private static final DateTimeFormatter odtx;
    private static final DateTimeFormatter sme;
    private static final DateTimeFormatter t;
    private static final DateTimeFormatter tp;
    private static final DateTimeFormatter tpe;
    private static final DateTimeFormatter tt;
    private static final DateTimeFormatter ttx;
    private static final DateTimeFormatter tx;
    private static final DateTimeFormatter wdt;
    private static final DateTimeFormatter wdtx;
    private static final DateTimeFormatter we;
    private static final DateTimeFormatter ww;
    private static final DateTimeFormatter wwd;
    private static final DateTimeFormatter wwe;
    private static final DateTimeFormatter ye = ;
    private static final DateTimeFormatter ym;
    private static final DateTimeFormatter ymd;
    private static final DateTimeFormatter ze;
    
    static
    {
      mye = monthElement();
      dme = dayOfMonthElement();
      we = weekyearElement();
      wwe = weekElement();
      dwe = dayOfWeekElement();
      dye = dayOfYearElement();
      hde = hourElement();
      mhe = minuteElement();
      sme = secondElement();
      fse = fractionElement();
      ze = offsetElement();
      lte = literalTElement();
      ym = yearMonth();
      ymd = yearMonthDay();
      ww = weekyearWeek();
      wwd = weekyearWeekDay();
      hm = hourMinute();
      hms = hourMinuteSecond();
      hmsl = hourMinuteSecondMillis();
      hmsf = hourMinuteSecondFraction();
      dh = dateHour();
      dhm = dateHourMinute();
      dhms = dateHourMinuteSecond();
      dhmsl = dateHourMinuteSecondMillis();
      dhmsf = dateHourMinuteSecondFraction();
      t = time();
      tx = timeNoMillis();
      tt = tTime();
      ttx = tTimeNoMillis();
      dt = dateTime();
      dtx = dateTimeNoMillis();
      wdt = weekDateTime();
      wdtx = weekDateTimeNoMillis();
      od = ordinalDate();
      odt = ordinalDateTime();
      odtx = ordinalDateTimeNoMillis();
      bd = basicDate();
      bt = basicTime();
      btx = basicTimeNoMillis();
      btt = basicTTime();
      bttx = basicTTimeNoMillis();
      bdt = basicDateTime();
      bdtx = basicDateTimeNoMillis();
      bod = basicOrdinalDate();
      bodt = basicOrdinalDateTime();
      bodtx = basicOrdinalDateTimeNoMillis();
      bwd = basicWeekDate();
      bwdt = basicWeekDateTime();
      bwdtx = basicWeekDateTimeNoMillis();
      dpe = dateElementParser();
      tpe = timeElementParser();
      dp = dateParser();
      ldp = localDateParser();
      tp = timeParser();
      ltp = localTimeParser();
      dtp = dateTimeParser();
    }
    
    private static DateTimeFormatter basicDate()
    {
      if (bd == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendYear(4, 4).appendFixedDecimal(DateTimeFieldType.monthOfYear(), 2).appendFixedDecimal(DateTimeFieldType.dayOfMonth(), 2).toFormatter();; localDateTimeFormatter = bd) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicDateTime()
    {
      if (bdt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicDate()).append(basicTTime()).toFormatter();; localDateTimeFormatter = bdt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicDateTimeNoMillis()
    {
      if (bdtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicDate()).append(basicTTimeNoMillis()).toFormatter();; localDateTimeFormatter = bdtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicOrdinalDate()
    {
      if (bod == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendYear(4, 4).appendFixedDecimal(DateTimeFieldType.dayOfYear(), 3).toFormatter();; localDateTimeFormatter = bod) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicOrdinalDateTime()
    {
      if (bodt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicOrdinalDate()).append(basicTTime()).toFormatter();; localDateTimeFormatter = bodt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicOrdinalDateTimeNoMillis()
    {
      if (bodtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicOrdinalDate()).append(basicTTimeNoMillis()).toFormatter();; localDateTimeFormatter = bodtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicTTime()
    {
      if (btt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(literalTElement()).append(basicTime()).toFormatter();; localDateTimeFormatter = btt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicTTimeNoMillis()
    {
      if (bttx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(literalTElement()).append(basicTimeNoMillis()).toFormatter();; localDateTimeFormatter = bttx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicTime()
    {
      if (bt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendFixedDecimal(DateTimeFieldType.hourOfDay(), 2).appendFixedDecimal(DateTimeFieldType.minuteOfHour(), 2).appendFixedDecimal(DateTimeFieldType.secondOfMinute(), 2).appendLiteral('.').appendFractionOfSecond(3, 9).appendTimeZoneOffset("Z", false, 2, 2).toFormatter();; localDateTimeFormatter = bt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicTimeNoMillis()
    {
      if (btx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendFixedDecimal(DateTimeFieldType.hourOfDay(), 2).appendFixedDecimal(DateTimeFieldType.minuteOfHour(), 2).appendFixedDecimal(DateTimeFieldType.secondOfMinute(), 2).appendTimeZoneOffset("Z", false, 2, 2).toFormatter();; localDateTimeFormatter = btx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicWeekDate()
    {
      if (bwd == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendWeekyear(4, 4).appendLiteral('W').appendFixedDecimal(DateTimeFieldType.weekOfWeekyear(), 2).appendFixedDecimal(DateTimeFieldType.dayOfWeek(), 1).toFormatter();; localDateTimeFormatter = bwd) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicWeekDateTime()
    {
      if (bwdt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicWeekDate()).append(basicTTime()).toFormatter();; localDateTimeFormatter = bwdt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter basicWeekDateTimeNoMillis()
    {
      if (bwdtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(basicWeekDate()).append(basicTTimeNoMillis()).toFormatter();; localDateTimeFormatter = bwdtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateElementParser()
    {
      if (dpe == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(null, new DateTimeParser[] { new DateTimeFormatterBuilder().append(yearElement()).appendOptional(new DateTimeFormatterBuilder().append(monthElement()).appendOptional(dayOfMonthElement().getParser()).toParser()).toParser(), new DateTimeFormatterBuilder().append(weekyearElement()).append(weekElement()).appendOptional(dayOfWeekElement().getParser()).toParser(), new DateTimeFormatterBuilder().append(yearElement()).append(dayOfYearElement()).toParser() }).toFormatter();; localDateTimeFormatter = dpe) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateHour()
    {
      if (dh == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(literalTElement()).append(ISODateTimeFormat.hour()).toFormatter();; localDateTimeFormatter = dh) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateHourMinute()
    {
      if (dhm == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(literalTElement()).append(hourMinute()).toFormatter();; localDateTimeFormatter = dhm) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateHourMinuteSecond()
    {
      if (dhms == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(literalTElement()).append(hourMinuteSecond()).toFormatter();; localDateTimeFormatter = dhms) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateHourMinuteSecondFraction()
    {
      if (dhmsf == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(literalTElement()).append(hourMinuteSecondFraction()).toFormatter();; localDateTimeFormatter = dhmsf) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateHourMinuteSecondMillis()
    {
      if (dhmsl == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(literalTElement()).append(hourMinuteSecondMillis()).toFormatter();; localDateTimeFormatter = dhmsl) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateOptionalTimeParser()
    {
      if (dotp == null) {
        localObject = new DateTimeFormatterBuilder().appendLiteral('T').appendOptional(timeElementParser().getParser()).appendOptional(offsetElement().getParser()).toParser();
      }
      for (Object localObject = new DateTimeFormatterBuilder().append(dateElementParser()).appendOptional((DateTimeParser)localObject).toFormatter();; localObject = dotp) {
        return (DateTimeFormatter)localObject;
      }
    }
    
    private static DateTimeFormatter dateParser()
    {
      if (dp == null) {
        localObject = new DateTimeFormatterBuilder().appendLiteral('T').append(offsetElement()).toParser();
      }
      for (Object localObject = new DateTimeFormatterBuilder().append(dateElementParser()).appendOptional((DateTimeParser)localObject).toFormatter();; localObject = dp) {
        return (DateTimeFormatter)localObject;
      }
    }
    
    private static DateTimeFormatter dateTime()
    {
      if (dt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(tTime()).toFormatter();; localDateTimeFormatter = dt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateTimeNoMillis()
    {
      if (dtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.date()).append(tTimeNoMillis()).toFormatter();; localDateTimeFormatter = dtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dateTimeParser()
    {
      if (dtp == null) {
        localObject = new DateTimeFormatterBuilder().appendLiteral('T').append(timeElementParser()).appendOptional(offsetElement().getParser()).toParser();
      }
      for (Object localObject = new DateTimeFormatterBuilder().append(null, new DateTimeParser[] { localObject, dateOptionalTimeParser().getParser() }).toFormatter();; localObject = dtp) {
        return (DateTimeFormatter)localObject;
      }
    }
    
    private static DateTimeFormatter dayOfMonthElement()
    {
      if (dme == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('-').appendDayOfMonth(2).toFormatter();; localDateTimeFormatter = dme) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dayOfWeekElement()
    {
      if (dwe == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('-').appendDayOfWeek(1).toFormatter();; localDateTimeFormatter = dwe) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter dayOfYearElement()
    {
      if (dye == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('-').appendDayOfYear(3).toFormatter();; localDateTimeFormatter = dye) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter fractionElement()
    {
      if (fse == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('.').appendFractionOfSecond(3, 9).toFormatter();; localDateTimeFormatter = fse) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter hourElement()
    {
      if (hde == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendHourOfDay(2).toFormatter();; localDateTimeFormatter = hde) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter hourMinute()
    {
      if (hm == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourElement()).append(minuteElement()).toFormatter();; localDateTimeFormatter = hm) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter hourMinuteSecond()
    {
      if (hms == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourElement()).append(minuteElement()).append(secondElement()).toFormatter();; localDateTimeFormatter = hms) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter hourMinuteSecondFraction()
    {
      if (hmsf == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourElement()).append(minuteElement()).append(secondElement()).append(fractionElement()).toFormatter();; localDateTimeFormatter = hmsf) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter hourMinuteSecondMillis()
    {
      if (hmsl == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourElement()).append(minuteElement()).append(secondElement()).appendLiteral('.').appendFractionOfSecond(3, 3).toFormatter();; localDateTimeFormatter = hmsl) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter literalTElement()
    {
      if (lte == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('T').toFormatter();; localDateTimeFormatter = lte) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter localDateOptionalTimeParser()
    {
      if (ldotp == null) {
        localObject = new DateTimeFormatterBuilder().appendLiteral('T').append(timeElementParser()).toParser();
      }
      for (Object localObject = new DateTimeFormatterBuilder().append(dateElementParser()).appendOptional((DateTimeParser)localObject).toFormatter().withZoneUTC();; localObject = ldotp) {
        return (DateTimeFormatter)localObject;
      }
    }
    
    private static DateTimeFormatter localDateParser()
    {
      if (ldp == null) {}
      for (DateTimeFormatter localDateTimeFormatter = dateElementParser().withZoneUTC();; localDateTimeFormatter = ldp) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter localTimeParser()
    {
      if (ltp == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendOptional(literalTElement().getParser()).append(timeElementParser()).toFormatter().withZoneUTC();; localDateTimeFormatter = ltp) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter minuteElement()
    {
      if (mhe == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral(':').appendMinuteOfHour(2).toFormatter();; localDateTimeFormatter = mhe) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter monthElement()
    {
      if (mye == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral('-').appendMonthOfYear(2).toFormatter();; localDateTimeFormatter = mye) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter offsetElement()
    {
      if (ze == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendTimeZoneOffset("Z", true, 2, 4).toFormatter();; localDateTimeFormatter = ze) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter ordinalDate()
    {
      if (od == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(yearElement()).append(dayOfYearElement()).toFormatter();; localDateTimeFormatter = od) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter ordinalDateTime()
    {
      if (odt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ordinalDate()).append(tTime()).toFormatter();; localDateTimeFormatter = odt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter ordinalDateTimeNoMillis()
    {
      if (odtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ordinalDate()).append(tTimeNoMillis()).toFormatter();; localDateTimeFormatter = odtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter secondElement()
    {
      if (sme == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral(':').appendSecondOfMinute(2).toFormatter();; localDateTimeFormatter = sme) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter tTime()
    {
      if (tt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(literalTElement()).append(time()).toFormatter();; localDateTimeFormatter = tt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter tTimeNoMillis()
    {
      if (ttx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(literalTElement()).append(timeNoMillis()).toFormatter();; localDateTimeFormatter = ttx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter time()
    {
      if (t == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourMinuteSecondFraction()).append(offsetElement()).toFormatter();; localDateTimeFormatter = t) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter timeElementParser()
    {
      if (tpe == null) {
        localObject = new DateTimeFormatterBuilder().append(null, new DateTimeParser[] { new DateTimeFormatterBuilder().appendLiteral('.').toParser(), new DateTimeFormatterBuilder().appendLiteral(',').toParser() }).toParser();
      }
      for (Object localObject = new DateTimeFormatterBuilder().append(hourElement()).append(null, new DateTimeParser[] { new DateTimeFormatterBuilder().append(minuteElement()).append(null, new DateTimeParser[] { new DateTimeFormatterBuilder().append(secondElement()).appendOptional(new DateTimeFormatterBuilder().append((DateTimeParser)localObject).appendFractionOfSecond(1, 9).toParser()).toParser(), new DateTimeFormatterBuilder().append((DateTimeParser)localObject).appendFractionOfMinute(1, 9).toParser(), null }).toParser(), new DateTimeFormatterBuilder().append((DateTimeParser)localObject).appendFractionOfHour(1, 9).toParser(), null }).toFormatter();; localObject = tpe) {
        return (DateTimeFormatter)localObject;
      }
    }
    
    private static DateTimeFormatter timeNoMillis()
    {
      if (tx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(hourMinuteSecond()).append(offsetElement()).toFormatter();; localDateTimeFormatter = tx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter timeParser()
    {
      if (tp == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendOptional(literalTElement().getParser()).append(timeElementParser()).appendOptional(offsetElement().getParser()).toFormatter();; localDateTimeFormatter = tp) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekDateTime()
    {
      if (wdt == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.weekDate()).append(tTime()).toFormatter();; localDateTimeFormatter = wdt) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekDateTimeNoMillis()
    {
      if (wdtx == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(ISODateTimeFormat.weekDate()).append(tTimeNoMillis()).toFormatter();; localDateTimeFormatter = wdtx) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekElement()
    {
      if (wwe == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendLiteral("-W").appendWeekOfWeekyear(2).toFormatter();; localDateTimeFormatter = wwe) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekyearElement()
    {
      if (we == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendWeekyear(4, 9).toFormatter();; localDateTimeFormatter = we) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekyearWeek()
    {
      if (ww == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(weekyearElement()).append(weekElement()).toFormatter();; localDateTimeFormatter = ww) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter weekyearWeekDay()
    {
      if (wwd == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(weekyearElement()).append(weekElement()).append(dayOfWeekElement()).toFormatter();; localDateTimeFormatter = wwd) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter yearElement()
    {
      if (ye == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().appendYear(4, 9).toFormatter();; localDateTimeFormatter = ye) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter yearMonth()
    {
      if (ym == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(yearElement()).append(monthElement()).toFormatter();; localDateTimeFormatter = ym) {
        return localDateTimeFormatter;
      }
    }
    
    private static DateTimeFormatter yearMonthDay()
    {
      if (ymd == null) {}
      for (DateTimeFormatter localDateTimeFormatter = new DateTimeFormatterBuilder().append(yearElement()).append(monthElement()).append(dayOfMonthElement()).toFormatter();; localDateTimeFormatter = ymd) {
        return localDateTimeFormatter;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\ISODateTimeFormat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */