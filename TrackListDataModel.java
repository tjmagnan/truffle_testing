package tech.dcube.companion.model;

import java.util.ArrayList;

public class TrackListDataModel
{
  String OrderAddress = null;
  String OrderDeliveryTime = null;
  String OrderRecipient = null;
  String OrderRemainingTime = null;
  String OrderSerial = null;
  String OrderShippingPrice = null;
  String OrderStatus = null;
  String OrderStatusReport = null;
  public ArrayList<OrderStatusLogDataModel> statusLogHistory;
  
  public TrackListDataModel(String paramString1, String paramString2, String paramString3)
  {
    this.OrderSerial = paramString1;
    this.OrderRecipient = paramString2;
    this.OrderStatus = paramString3;
    this.statusLogHistory = new ArrayList();
    addStatusLogHistory("05/09", "Waiting for pickup", "Danbury, CT");
    addStatusLogHistory("05/09", "Arrived at USPS Facility", "Danbury, CT");
    addStatusLogHistory("05/08", "Scanned at transfer facility", "Richmond, VA");
    addStatusLogHistory("05/08", "Left origin facility", "Nashville, TN");
  }
  
  public TrackListDataModel(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    this.OrderSerial = paramString1;
    this.OrderRecipient = paramString2;
    this.OrderAddress = paramString3;
    this.OrderStatus = paramString4;
    this.OrderStatusReport = paramString5;
    this.OrderDeliveryTime = paramString6;
    this.OrderRemainingTime = paramString7;
    this.OrderShippingPrice = paramString8;
  }
  
  public void addStatusLogHistory(String paramString1, String paramString2, String paramString3)
  {
    this.statusLogHistory.add(new OrderStatusLogDataModel(paramString1, paramString2, paramString3));
  }
  
  public String getOrderAddress()
  {
    return this.OrderAddress;
  }
  
  public String getOrderDeliveryTime()
  {
    return this.OrderDeliveryTime;
  }
  
  public String getOrderRecipient()
  {
    return this.OrderRecipient;
  }
  
  public String getOrderRemainingTime()
  {
    return this.OrderRemainingTime;
  }
  
  public String getOrderSerial()
  {
    return this.OrderSerial;
  }
  
  public String getOrderShippingPrice()
  {
    return this.OrderShippingPrice;
  }
  
  public String getOrderStatus()
  {
    return this.OrderStatus;
  }
  
  public String getOrderStatusReport()
  {
    return this.OrderStatusReport;
  }
  
  public ArrayList<OrderStatusLogDataModel> getStatusLogHistory()
  {
    return this.statusLogHistory;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\TrackListDataModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */