package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBDimensionRules
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 1406020962723348632L;
  @Expose
  @SerializedName("dimensionHidden")
  private Boolean dimensionHidden;
  @Expose
  private double girth = 0.1D;
  @Expose
  private double height = 0.1D;
  @Expose
  @SerializedName("hiddenHeight")
  private double hiddenHeight;
  @Expose
  @SerializedName("hiddenLength")
  private double hiddenLength;
  @Expose
  @SerializedName("hiddenWidth")
  private double hiddenWidth;
  @Expose
  private double length = 0.1D;
  @Expose
  @SerializedName("maxGirth")
  private double maxGirth;
  @Expose
  @SerializedName("maxHeight")
  private double maxHeight;
  @Expose
  @SerializedName("maxLength")
  private double maxLength;
  @Expose
  @SerializedName("maxLengthPlusGirth")
  private double maxLengthPlusGirth;
  @Expose
  @SerializedName("maxLengthWidthHeightSum")
  private double maxLengthWidthHeightSum;
  @Expose
  @SerializedName("maxWidth")
  private double maxWidth;
  @Expose
  @SerializedName("minGirth")
  private double minGirth;
  @Expose
  @SerializedName("minHeight")
  private double minHeight;
  @Expose
  @SerializedName("minLength")
  private double minLength;
  @Expose
  @SerializedName("minLengthPlusGirth")
  private double minLengthPlusGirth;
  @Expose
  @SerializedName("minLengthWidthHeightSum")
  private double minLengthWidthHeightSum;
  @Expose
  @SerializedName("minWidth")
  private double minWidth;
  @Expose
  @SerializedName("packageNotRectangular")
  private Boolean packageNotRectangular;
  @Expose
  private double width = 0.1D;
  
  public PBDimensionRules() {}
  
  public PBDimensionRules(Boolean paramBoolean1, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, Double paramDouble1, Double paramDouble2, Double paramDouble3, Integer paramInteger5, Double paramDouble4, Integer paramInteger6, Double paramDouble5, Double paramDouble6, Integer paramInteger7, Integer paramInteger8, Double paramDouble7, Boolean paramBoolean2)
  {
    this.dimensionHidden = paramBoolean1;
    this.hiddenHeight = paramInteger1.intValue();
    this.hiddenLength = paramInteger2.intValue();
    this.hiddenWidth = paramInteger3.intValue();
    this.maxGirth = paramInteger4.intValue();
    this.maxHeight = paramDouble1.doubleValue();
    this.maxLength = paramDouble2.doubleValue();
    this.maxLengthPlusGirth = paramDouble3.doubleValue();
    this.maxLengthWidthHeightSum = paramInteger5.intValue();
    this.maxWidth = paramDouble4.doubleValue();
    this.minGirth = paramInteger6.intValue();
    this.minHeight = paramDouble5.doubleValue();
    this.minLength = paramDouble6.doubleValue();
    this.minLengthPlusGirth = paramInteger7.intValue();
    this.minLengthWidthHeightSum = paramInteger8.intValue();
    this.minWidth = paramDouble7.doubleValue();
    this.packageNotRectangular = paramBoolean2;
  }
  
  public Boolean getDimensionHidden()
  {
    return this.dimensionHidden;
  }
  
  public double getGirth()
  {
    return this.girth;
  }
  
  public double getHeight()
  {
    return this.height;
  }
  
  public double getHiddenHeight()
  {
    return this.hiddenHeight;
  }
  
  public double getHiddenLength()
  {
    return this.hiddenLength;
  }
  
  public double getHiddenWidth()
  {
    return this.hiddenWidth;
  }
  
  public double getLength()
  {
    return this.length;
  }
  
  public double getMaxGirth()
  {
    return this.maxGirth;
  }
  
  public double getMaxHeight()
  {
    return this.maxHeight;
  }
  
  public double getMaxLength()
  {
    return this.maxLength;
  }
  
  public double getMaxLengthPlusGirth()
  {
    return this.maxLengthPlusGirth;
  }
  
  public double getMaxLengthWidthHeightSum()
  {
    return this.maxLengthWidthHeightSum;
  }
  
  public double getMaxWidth()
  {
    return this.maxWidth;
  }
  
  public double getMinGirth()
  {
    return this.minGirth;
  }
  
  public double getMinHeight()
  {
    return this.minHeight;
  }
  
  public double getMinLength()
  {
    return this.minLength;
  }
  
  public double getMinLengthPlusGirth()
  {
    return this.minLengthPlusGirth;
  }
  
  public double getMinLengthWidthHeightSum()
  {
    return this.minLengthWidthHeightSum;
  }
  
  public double getMinWidth()
  {
    return this.minWidth;
  }
  
  public Boolean getPackageNotRectangular()
  {
    return this.packageNotRectangular;
  }
  
  public double getWidth()
  {
    return this.width;
  }
  
  public void setDimensionHidden(Boolean paramBoolean)
  {
    this.dimensionHidden = paramBoolean;
  }
  
  public void setGirth(double paramDouble)
  {
    this.girth = paramDouble;
  }
  
  public void setHeight(double paramDouble)
  {
    this.height = paramDouble;
  }
  
  public void setHiddenHeight(double paramDouble)
  {
    this.hiddenHeight = paramDouble;
  }
  
  public void setHiddenLength(double paramDouble)
  {
    this.hiddenLength = paramDouble;
  }
  
  public void setHiddenWidth(double paramDouble)
  {
    this.hiddenWidth = paramDouble;
  }
  
  public void setLength(double paramDouble)
  {
    this.length = paramDouble;
  }
  
  public void setMaxGirth(double paramDouble)
  {
    this.maxGirth = paramDouble;
  }
  
  public void setMaxHeight(double paramDouble)
  {
    this.maxHeight = paramDouble;
  }
  
  public void setMaxLength(double paramDouble)
  {
    this.maxLength = paramDouble;
  }
  
  public void setMaxLengthPlusGirth(double paramDouble)
  {
    this.maxLengthPlusGirth = paramDouble;
  }
  
  public void setMaxLengthWidthHeightSum(double paramDouble)
  {
    this.maxLengthWidthHeightSum = paramDouble;
  }
  
  public void setMaxWidth(double paramDouble)
  {
    this.maxWidth = paramDouble;
  }
  
  public void setMinGirth(double paramDouble)
  {
    this.minGirth = paramDouble;
  }
  
  public void setMinHeight(double paramDouble)
  {
    this.minHeight = paramDouble;
  }
  
  public void setMinLength(double paramDouble)
  {
    this.minLength = paramDouble;
  }
  
  public void setMinLengthPlusGirth(double paramDouble)
  {
    this.minLengthPlusGirth = paramDouble;
  }
  
  public void setMinLengthWidthHeightSum(double paramDouble)
  {
    this.minLengthWidthHeightSum = paramDouble;
  }
  
  public void setMinWidth(double paramDouble)
  {
    this.minWidth = paramDouble;
  }
  
  public void setPackageNotRectangular(Boolean paramBoolean)
  {
    this.packageNotRectangular = paramBoolean;
  }
  
  public void setWidth(double paramDouble)
  {
    this.width = paramDouble;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBDimensionRules.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */