package org.joda.time;

import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Months
  extends BaseSingleFieldPeriod
{
  public static final Months EIGHT;
  public static final Months ELEVEN;
  public static final Months FIVE;
  public static final Months FOUR;
  public static final Months MAX_VALUE = new Months(Integer.MAX_VALUE);
  public static final Months MIN_VALUE = new Months(Integer.MIN_VALUE);
  public static final Months NINE;
  public static final Months ONE;
  private static final PeriodFormatter PARSER = ISOPeriodFormat.standard().withParseType(PeriodType.months());
  public static final Months SEVEN;
  public static final Months SIX;
  public static final Months TEN;
  public static final Months THREE;
  public static final Months TWELVE;
  public static final Months TWO;
  public static final Months ZERO = new Months(0);
  private static final long serialVersionUID = 87525275727380867L;
  
  static
  {
    ONE = new Months(1);
    TWO = new Months(2);
    THREE = new Months(3);
    FOUR = new Months(4);
    FIVE = new Months(5);
    SIX = new Months(6);
    SEVEN = new Months(7);
    EIGHT = new Months(8);
    NINE = new Months(9);
    TEN = new Months(10);
    ELEVEN = new Months(11);
    TWELVE = new Months(12);
  }
  
  private Months(int paramInt)
  {
    super(paramInt);
  }
  
  public static Months months(int paramInt)
  {
    Months localMonths;
    switch (paramInt)
    {
    default: 
      localMonths = new Months(paramInt);
    }
    for (;;)
    {
      return localMonths;
      localMonths = ZERO;
      continue;
      localMonths = ONE;
      continue;
      localMonths = TWO;
      continue;
      localMonths = THREE;
      continue;
      localMonths = FOUR;
      continue;
      localMonths = FIVE;
      continue;
      localMonths = SIX;
      continue;
      localMonths = SEVEN;
      continue;
      localMonths = EIGHT;
      continue;
      localMonths = NINE;
      continue;
      localMonths = TEN;
      continue;
      localMonths = ELEVEN;
      continue;
      localMonths = TWELVE;
      continue;
      localMonths = MAX_VALUE;
      continue;
      localMonths = MIN_VALUE;
    }
  }
  
  public static Months monthsBetween(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    return months(BaseSingleFieldPeriod.between(paramReadableInstant1, paramReadableInstant2, DurationFieldType.months()));
  }
  
  public static Months monthsBetween(ReadablePartial paramReadablePartial1, ReadablePartial paramReadablePartial2)
  {
    if (((paramReadablePartial1 instanceof LocalDate)) && ((paramReadablePartial2 instanceof LocalDate))) {}
    for (paramReadablePartial1 = months(DateTimeUtils.getChronology(paramReadablePartial1.getChronology()).months().getDifference(((LocalDate)paramReadablePartial2).getLocalMillis(), ((LocalDate)paramReadablePartial1).getLocalMillis()));; paramReadablePartial1 = months(BaseSingleFieldPeriod.between(paramReadablePartial1, paramReadablePartial2, ZERO))) {
      return paramReadablePartial1;
    }
  }
  
  public static Months monthsIn(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (paramReadableInterval = ZERO;; paramReadableInterval = months(BaseSingleFieldPeriod.between(paramReadableInterval.getStart(), paramReadableInterval.getEnd(), DurationFieldType.months()))) {
      return paramReadableInterval;
    }
  }
  
  @FromString
  public static Months parseMonths(String paramString)
  {
    if (paramString == null) {}
    for (paramString = ZERO;; paramString = months(PARSER.parsePeriod(paramString).getMonths())) {
      return paramString;
    }
  }
  
  private Object readResolve()
  {
    return months(getValue());
  }
  
  public Months dividedBy(int paramInt)
  {
    if (paramInt == 1) {}
    for (Months localMonths = this;; localMonths = months(getValue() / paramInt)) {
      return localMonths;
    }
  }
  
  public DurationFieldType getFieldType()
  {
    return DurationFieldType.months();
  }
  
  public int getMonths()
  {
    return getValue();
  }
  
  public PeriodType getPeriodType()
  {
    return PeriodType.months();
  }
  
  public boolean isGreaterThan(Months paramMonths)
  {
    boolean bool = true;
    if (paramMonths == null) {
      if (getValue() <= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() <= paramMonths.getValue()) {
        bool = false;
      }
    }
  }
  
  public boolean isLessThan(Months paramMonths)
  {
    boolean bool = true;
    if (paramMonths == null) {
      if (getValue() >= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() >= paramMonths.getValue()) {
        bool = false;
      }
    }
  }
  
  public Months minus(int paramInt)
  {
    return plus(FieldUtils.safeNegate(paramInt));
  }
  
  public Months minus(Months paramMonths)
  {
    if (paramMonths == null) {}
    for (paramMonths = this;; paramMonths = minus(paramMonths.getValue())) {
      return paramMonths;
    }
  }
  
  public Months multipliedBy(int paramInt)
  {
    return months(FieldUtils.safeMultiply(getValue(), paramInt));
  }
  
  public Months negated()
  {
    return months(FieldUtils.safeNegate(getValue()));
  }
  
  public Months plus(int paramInt)
  {
    if (paramInt == 0) {}
    for (Months localMonths = this;; localMonths = months(FieldUtils.safeAdd(getValue(), paramInt))) {
      return localMonths;
    }
  }
  
  public Months plus(Months paramMonths)
  {
    if (paramMonths == null) {}
    for (paramMonths = this;; paramMonths = plus(paramMonths.getValue())) {
      return paramMonths;
    }
  }
  
  @ToString
  public String toString()
  {
    return "P" + String.valueOf(getValue()) + "M";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Months.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */