package tech.dcube.companion.customListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.Locale;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import tech.dcube.companion.model.PBShipmentTracking;

public class TrackListAdapter
  extends ArrayAdapter<PBShipmentTracking>
  implements View.OnClickListener
{
  private List<PBShipmentTracking> dataSet;
  private int lastPosition = -1;
  Context mContext;
  
  public TrackListAdapter(List<PBShipmentTracking> paramList, Context paramContext)
  {
    super(paramContext, 2130968657, paramList);
    this.dataSet = paramList;
    this.mContext = paramContext;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    PBShipmentTracking localPBShipmentTracking = (PBShipmentTracking)getItem(paramInt);
    Object localObject1;
    int i;
    if (paramView == null)
    {
      Object localObject2 = new ViewHolder(null);
      paramView = LayoutInflater.from(getContext()).inflate(2130968657, paramViewGroup, false);
      ((ViewHolder)localObject2).OrderSerial = ((TextView)paramView.findViewById(2131624261));
      ((ViewHolder)localObject2).OrderRecipient = ((TextView)paramView.findViewById(2131624262));
      ((ViewHolder)localObject2).OrderStatus = ((TextView)paramView.findViewById(2131624236));
      localObject1 = paramView;
      paramView.setTag(localObject2);
      paramViewGroup = (ViewGroup)localObject2;
      localObject2 = this.mContext;
      if (paramInt <= this.lastPosition) {
        break label237;
      }
      i = 2131034132;
      label111:
      ((View)localObject1).startAnimation(AnimationUtils.loadAnimation((Context)localObject2, i));
      this.lastPosition = paramInt;
      if (localPBShipmentTracking.getTrackingId() != null)
      {
        localObject1 = "Order No." + localPBShipmentTracking.getTrackingId();
        paramViewGroup.OrderSerial.setText((CharSequence)localObject1);
      }
      if (localPBShipmentTracking.getRecipientName() == null) {
        break label244;
      }
      localObject1 = localPBShipmentTracking.getRecipientName().toString();
      paramViewGroup.OrderRecipient.setText((CharSequence)localObject1);
      label197:
      if (localPBShipmentTracking.getDisplayField() == null) {
        break label255;
      }
      localObject1 = localPBShipmentTracking.getDisplayField();
      paramViewGroup.OrderStatus.setText((CharSequence)localObject1);
    }
    for (;;)
    {
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      localObject1 = paramView;
      break;
      label237:
      i = 2131034129;
      break label111;
      label244:
      paramViewGroup.OrderRecipient.setVisibility(4);
      break label197;
      label255:
      if (localPBShipmentTracking.getEstimatedDelivery() != null)
      {
        localObject1 = localPBShipmentTracking.getEstimatedDelivery();
        localObject1 = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.000Z").withLocale(Locale.ENGLISH).parseLocalDate((String)localObject1);
        localObject1 = ((LocalDate)localObject1).getYear() + "/" + ((LocalDate)localObject1).getMonthOfYear() + "/" + ((LocalDate)localObject1).getDayOfMonth();
        localObject1 = "Estimated Date : " + (String)localObject1;
        paramViewGroup.OrderStatus.setText((CharSequence)localObject1);
      }
      else
      {
        paramViewGroup.OrderStatus.setVisibility(4);
      }
    }
  }
  
  public void onClick(View paramView)
  {
    PBShipmentTracking localPBShipmentTracking = (PBShipmentTracking)getItem(((Integer)paramView.getTag()).intValue());
    paramView.getId();
  }
  
  public void updateData(List<PBShipmentTracking> paramList)
  {
    this.dataSet.clear();
    this.dataSet.addAll(paramList);
    notifyDataSetChanged();
  }
  
  private static class ViewHolder
  {
    TextView OrderRecipient;
    TextView OrderSerial;
    TextView OrderStatus;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\customListAdapters\TrackListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */