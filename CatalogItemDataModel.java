package tech.dcube.companion.model;

public class CatalogItemDataModel
{
  double Height = 0.0D;
  double Length = 0.0D;
  String Picture = null;
  String ProductDescription = null;
  String ProductName = null;
  String UPC = null;
  double Weight = 0.0D;
  double Width = 0.0D;
  
  public CatalogItemDataModel()
  {
    this.ProductName = "product name";
    this.ProductDescription = "product description";
    this.Length = 0.0D;
    this.Width = 0.0D;
    this.Height = 0.0D;
    this.Weight = 0.0D;
    this.UPC = "";
  }
  
  public CatalogItemDataModel(String paramString1, String paramString2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, String paramString3)
  {
    this.ProductName = paramString1;
    this.ProductDescription = paramString2;
    this.Length = paramDouble1;
    this.Width = paramDouble2;
    this.Height = paramDouble3;
    this.Weight = paramDouble4;
    this.UPC = paramString3;
  }
  
  public CatalogItemDataModel(String paramString1, String paramString2, double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, String paramString3, String paramString4)
  {
    this.ProductName = paramString1;
    this.ProductDescription = paramString2;
    this.Length = paramDouble1;
    this.Width = paramDouble2;
    this.Height = paramDouble3;
    this.Weight = paramDouble4;
    this.UPC = paramString3;
    this.Picture = paramString4;
  }
  
  public double getHeight()
  {
    return this.Height;
  }
  
  public double getLength()
  {
    return this.Length;
  }
  
  public String getPicture()
  {
    return this.Picture;
  }
  
  public String getProductDescription()
  {
    return this.ProductDescription;
  }
  
  public String getProductName()
  {
    return this.ProductName;
  }
  
  public String getUPC()
  {
    return this.UPC;
  }
  
  public double getWeight()
  {
    return this.Weight;
  }
  
  public double getWidth()
  {
    return this.Width;
  }
  
  public void setHeight(double paramDouble)
  {
    this.Height = paramDouble;
  }
  
  public void setLength(double paramDouble)
  {
    this.Length = paramDouble;
  }
  
  public void setPicture(String paramString)
  {
    this.Picture = paramString;
  }
  
  public void setProductDescription(String paramString)
  {
    this.ProductDescription = paramString;
  }
  
  public void setProductName(String paramString)
  {
    this.ProductName = paramString;
  }
  
  public void setUPC(String paramString)
  {
    this.UPC = paramString;
  }
  
  public void setWeight(double paramDouble)
  {
    this.Weight = paramDouble;
  }
  
  public void setWidth(double paramDouble)
  {
    this.Width = paramDouble;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\CatalogItemDataModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */