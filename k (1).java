package com.testfairy.k;

import android.util.Log;
import com.testfairy.e;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public class a
{
  private String a;
  private File b;
  private a c;
  private boolean d;
  
  public a(String paramString, File paramFile)
  {
    this.a = paramString;
    this.b = paramFile;
  }
  
  public void a()
  {
    this.d = true;
  }
  
  public void a(a parama)
  {
    this.c = parama;
  }
  
  public void b()
  {
    this.d = false;
    new b().start();
  }
  
  public static abstract interface a
  {
    public abstract void a();
    
    public abstract void a(long paramLong1, long paramLong2);
    
    public abstract void b();
    
    public abstract void c();
  }
  
  class b
    extends Thread
  {
    b() {}
    
    public void run()
    {
      long l3 = 0L;
      for (;;)
      {
        try
        {
          Object localObject1 = e.a;
          Object localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          Log.v((String)localObject1, "Downloading2 " + a.a(a.this));
          localObject2 = new org/apache/http/impl/client/DefaultHttpClient;
          ((DefaultHttpClient)localObject2).<init>();
          localObject1 = new org/apache/http/client/methods/HttpGet;
          ((HttpGet)localObject1).<init>(a.a(a.this));
          localObject2 = ((HttpClient)localObject2).execute((HttpUriRequest)localObject1).getEntity();
          localObject1 = ((HttpEntity)localObject2).getContent();
          a.b(a.this).a();
          l4 = ((HttpEntity)localObject2).getContentLength();
          a.b(a.this).a(0L, l4);
          FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
          localFileOutputStream.<init>(a.c(a.this));
          localObject2 = new byte['က'];
          long l1 = 0L;
          if (!a.d(a.this))
          {
            int i = ((InputStream)localObject1).read((byte[])localObject2);
            if (i != -1)
            {
              localFileOutputStream.write((byte[])localObject2, 0, i);
              long l2 = l1 + i;
              a.b(a.this).a(l2, l4);
              l1 = l2;
              if (l2 - l3 <= 1048576L) {
                continue;
              }
              String str = e.a;
              StringBuilder localStringBuilder = new java/lang/StringBuilder;
              localStringBuilder.<init>();
              Log.d(str, "Downloaded " + l2 + " bytes so far");
              l3 = l2;
              l1 = l2;
              continue;
            }
          }
          localFileOutputStream.flush();
          localFileOutputStream.close();
          ((InputStream)localObject1).close();
          if (a.d(a.this))
          {
            Log.v(e.a, "Download aborted");
            a.b(a.this).b();
            a.c(a.this).delete();
            return;
          }
        }
        catch (Exception localException)
        {
          long l4;
          Log.e(e.a, "Failed downloading ");
          a.b(a.this).b();
          continue;
        }
        a.b(a.this).a(l4, l4);
        a.b(a.this).c();
        Log.v(e.a, "Download completed successfully");
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\k\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */