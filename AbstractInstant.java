package org.joda.time.base;

import java.util.Date;
import org.joda.convert.ToString;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.MutableDateTime;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public abstract class AbstractInstant
  implements ReadableInstant
{
  public int compareTo(ReadableInstant paramReadableInstant)
  {
    int i = 0;
    if (this == paramReadableInstant) {}
    for (;;)
    {
      return i;
      long l2 = paramReadableInstant.getMillis();
      long l1 = getMillis();
      if (l1 != l2) {
        if (l1 < l2) {
          i = -1;
        } else {
          i = 1;
        }
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (!(paramObject instanceof ReadableInstant))
      {
        bool = false;
      }
      else
      {
        paramObject = (ReadableInstant)paramObject;
        if ((getMillis() != ((ReadableInstant)paramObject).getMillis()) || (!FieldUtils.equals(getChronology(), ((ReadableInstant)paramObject).getChronology()))) {
          bool = false;
        }
      }
    }
  }
  
  public int get(DateTimeField paramDateTimeField)
  {
    if (paramDateTimeField == null) {
      throw new IllegalArgumentException("The DateTimeField must not be null");
    }
    return paramDateTimeField.get(getMillis());
  }
  
  public int get(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("The DateTimeFieldType must not be null");
    }
    return paramDateTimeFieldType.getField(getChronology()).get(getMillis());
  }
  
  public DateTimeZone getZone()
  {
    return getChronology().getZone();
  }
  
  public int hashCode()
  {
    return (int)(getMillis() ^ getMillis() >>> 32) + getChronology().hashCode();
  }
  
  public boolean isAfter(long paramLong)
  {
    if (getMillis() > paramLong) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isAfter(ReadableInstant paramReadableInstant)
  {
    return isAfter(DateTimeUtils.getInstantMillis(paramReadableInstant));
  }
  
  public boolean isAfterNow()
  {
    return isAfter(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean isBefore(long paramLong)
  {
    if (getMillis() < paramLong) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isBefore(ReadableInstant paramReadableInstant)
  {
    return isBefore(DateTimeUtils.getInstantMillis(paramReadableInstant));
  }
  
  public boolean isBeforeNow()
  {
    return isBefore(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean isEqual(long paramLong)
  {
    if (getMillis() == paramLong) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isEqual(ReadableInstant paramReadableInstant)
  {
    return isEqual(DateTimeUtils.getInstantMillis(paramReadableInstant));
  }
  
  public boolean isEqualNow()
  {
    return isEqual(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean isSupported(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {}
    for (boolean bool = false;; bool = paramDateTimeFieldType.getField(getChronology()).isSupported()) {
      return bool;
    }
  }
  
  public Date toDate()
  {
    return new Date(getMillis());
  }
  
  public DateTime toDateTime()
  {
    return new DateTime(getMillis(), getZone());
  }
  
  public DateTime toDateTime(Chronology paramChronology)
  {
    return new DateTime(getMillis(), paramChronology);
  }
  
  public DateTime toDateTime(DateTimeZone paramDateTimeZone)
  {
    paramDateTimeZone = DateTimeUtils.getChronology(getChronology()).withZone(paramDateTimeZone);
    return new DateTime(getMillis(), paramDateTimeZone);
  }
  
  public DateTime toDateTimeISO()
  {
    return new DateTime(getMillis(), ISOChronology.getInstance(getZone()));
  }
  
  public Instant toInstant()
  {
    return new Instant(getMillis());
  }
  
  public MutableDateTime toMutableDateTime()
  {
    return new MutableDateTime(getMillis(), getZone());
  }
  
  public MutableDateTime toMutableDateTime(Chronology paramChronology)
  {
    return new MutableDateTime(getMillis(), paramChronology);
  }
  
  public MutableDateTime toMutableDateTime(DateTimeZone paramDateTimeZone)
  {
    paramDateTimeZone = DateTimeUtils.getChronology(getChronology()).withZone(paramDateTimeZone);
    return new MutableDateTime(getMillis(), paramDateTimeZone);
  }
  
  public MutableDateTime toMutableDateTimeISO()
  {
    return new MutableDateTime(getMillis(), ISOChronology.getInstance(getZone()));
  }
  
  @ToString
  public String toString()
  {
    return ISODateTimeFormat.dateTime().print(this);
  }
  
  public String toString(DateTimeFormatter paramDateTimeFormatter)
  {
    if (paramDateTimeFormatter == null) {}
    for (paramDateTimeFormatter = toString();; paramDateTimeFormatter = paramDateTimeFormatter.print(this)) {
      return paramDateTimeFormatter;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\AbstractInstant.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */