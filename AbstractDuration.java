package org.joda.time.base;

import org.joda.convert.ToString;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.ReadableDuration;
import org.joda.time.format.FormatUtils;

public abstract class AbstractDuration
  implements ReadableDuration
{
  public int compareTo(ReadableDuration paramReadableDuration)
  {
    long l1 = getMillis();
    long l2 = paramReadableDuration.getMillis();
    int i;
    if (l1 < l2) {
      i = -1;
    }
    for (;;)
    {
      return i;
      if (l1 > l2) {
        i = 1;
      } else {
        i = 0;
      }
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (!(paramObject instanceof ReadableDuration))
      {
        bool = false;
      }
      else
      {
        paramObject = (ReadableDuration)paramObject;
        if (getMillis() != ((ReadableDuration)paramObject).getMillis()) {
          bool = false;
        }
      }
    }
  }
  
  public int hashCode()
  {
    long l = getMillis();
    return (int)(l ^ l >>> 32);
  }
  
  public boolean isEqual(ReadableDuration paramReadableDuration)
  {
    Object localObject = paramReadableDuration;
    if (paramReadableDuration == null) {
      localObject = Duration.ZERO;
    }
    if (compareTo((ReadableDuration)localObject) == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isLongerThan(ReadableDuration paramReadableDuration)
  {
    Object localObject = paramReadableDuration;
    if (paramReadableDuration == null) {
      localObject = Duration.ZERO;
    }
    if (compareTo((ReadableDuration)localObject) > 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isShorterThan(ReadableDuration paramReadableDuration)
  {
    Object localObject = paramReadableDuration;
    if (paramReadableDuration == null) {
      localObject = Duration.ZERO;
    }
    if (compareTo((ReadableDuration)localObject) < 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Duration toDuration()
  {
    return new Duration(getMillis());
  }
  
  public Period toPeriod()
  {
    return new Period(getMillis());
  }
  
  @ToString
  public String toString()
  {
    long l = getMillis();
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("PT");
    int i;
    if (l < 0L)
    {
      i = 1;
      FormatUtils.appendUnpaddedInteger(localStringBuffer, l);
      label39:
      int k = localStringBuffer.length();
      if (i == 0) {
        break label80;
      }
      j = 7;
      label52:
      if (k >= j) {
        break label91;
      }
      if (i == 0) {
        break label86;
      }
    }
    label80:
    label86:
    for (int j = 3;; j = 2)
    {
      localStringBuffer.insert(j, "0");
      break label39;
      i = 0;
      break;
      j = 6;
      break label52;
    }
    label91:
    if (l / 1000L * 1000L == l) {
      localStringBuffer.setLength(localStringBuffer.length() - 3);
    }
    for (;;)
    {
      localStringBuffer.append('S');
      return localStringBuffer.toString();
      localStringBuffer.insert(localStringBuffer.length() - 3, ".");
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\AbstractDuration.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */