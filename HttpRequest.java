package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPInputStream;

public class HttpRequest
{
  private static final String BOUNDARY = "00content0boundary00";
  public static final String CHARSET_UTF8 = "UTF-8";
  private static ConnectionFactory CONNECTION_FACTORY = ConnectionFactory.DEFAULT;
  public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
  public static final String CONTENT_TYPE_JSON = "application/json";
  private static final String CONTENT_TYPE_MULTIPART = "multipart/form-data; boundary=00content0boundary00";
  private static final String CRLF = "\r\n";
  private static final String[] EMPTY_STRINGS = new String[0];
  public static final String ENCODING_GZIP = "gzip";
  public static final String HEADER_ACCEPT = "Accept";
  public static final String HEADER_ACCEPT_CHARSET = "Accept-Charset";
  public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
  public static final String HEADER_AUTHORIZATION = "Authorization";
  public static final String HEADER_CACHE_CONTROL = "Cache-Control";
  public static final String HEADER_CONTENT_ENCODING = "Content-Encoding";
  public static final String HEADER_CONTENT_LENGTH = "Content-Length";
  public static final String HEADER_CONTENT_TYPE = "Content-Type";
  public static final String HEADER_DATE = "Date";
  public static final String HEADER_ETAG = "ETag";
  public static final String HEADER_EXPIRES = "Expires";
  public static final String HEADER_IF_NONE_MATCH = "If-None-Match";
  public static final String HEADER_LAST_MODIFIED = "Last-Modified";
  public static final String HEADER_LOCATION = "Location";
  public static final String HEADER_PROXY_AUTHORIZATION = "Proxy-Authorization";
  public static final String HEADER_REFERER = "Referer";
  public static final String HEADER_SERVER = "Server";
  public static final String HEADER_USER_AGENT = "User-Agent";
  public static final String METHOD_DELETE = "DELETE";
  public static final String METHOD_GET = "GET";
  public static final String METHOD_HEAD = "HEAD";
  public static final String METHOD_OPTIONS = "OPTIONS";
  public static final String METHOD_POST = "POST";
  public static final String METHOD_PUT = "PUT";
  public static final String METHOD_TRACE = "TRACE";
  public static final String PARAM_CHARSET = "charset";
  private int bufferSize = 8192;
  private HttpURLConnection connection = null;
  private boolean form;
  private String httpProxyHost;
  private int httpProxyPort;
  private boolean ignoreCloseExceptions = true;
  private boolean multipart;
  private RequestOutputStream output;
  private final String requestMethod;
  private boolean uncompress = false;
  public final URL url;
  
  public HttpRequest(CharSequence paramCharSequence, String paramString)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      URL localURL = new java/net/URL;
      localURL.<init>(paramCharSequence.toString());
      this.url = localURL;
      this.requestMethod = paramString;
      return;
    }
    catch (MalformedURLException paramCharSequence)
    {
      throw new HttpRequestException(paramCharSequence);
    }
  }
  
  public HttpRequest(URL paramURL, String paramString)
    throws HttpRequest.HttpRequestException
  {
    this.url = paramURL;
    this.requestMethod = paramString;
  }
  
  private static StringBuilder addParamPrefix(String paramString, StringBuilder paramStringBuilder)
  {
    int i = paramString.indexOf('?');
    int j = paramStringBuilder.length() - 1;
    if (i == -1) {
      paramStringBuilder.append('?');
    }
    for (;;)
    {
      return paramStringBuilder;
      if ((i < j) && (paramString.charAt(j) != '&')) {
        paramStringBuilder.append('&');
      }
    }
  }
  
  private static StringBuilder addPathSeparator(String paramString, StringBuilder paramStringBuilder)
  {
    if (paramString.indexOf(':') + 2 == paramString.lastIndexOf('/')) {
      paramStringBuilder.append('/');
    }
    return paramStringBuilder;
  }
  
  public static String append(CharSequence paramCharSequence, Map<?, ?> paramMap)
  {
    Object localObject = paramCharSequence.toString();
    paramCharSequence = (CharSequence)localObject;
    if (paramMap != null) {
      if (!paramMap.isEmpty()) {
        break label26;
      }
    }
    for (paramCharSequence = (CharSequence)localObject;; paramCharSequence = paramCharSequence.toString())
    {
      return paramCharSequence;
      label26:
      paramCharSequence = new StringBuilder((String)localObject);
      addPathSeparator((String)localObject, paramCharSequence);
      addParamPrefix((String)localObject, paramCharSequence);
      paramMap = paramMap.entrySet().iterator();
      localObject = (Map.Entry)paramMap.next();
      paramCharSequence.append(((Map.Entry)localObject).getKey().toString());
      paramCharSequence.append('=');
      localObject = ((Map.Entry)localObject).getValue();
      if (localObject != null) {
        paramCharSequence.append(localObject);
      }
      while (paramMap.hasNext())
      {
        paramCharSequence.append('&');
        localObject = (Map.Entry)paramMap.next();
        paramCharSequence.append(((Map.Entry)localObject).getKey().toString());
        paramCharSequence.append('=');
        localObject = ((Map.Entry)localObject).getValue();
        if (localObject != null) {
          paramCharSequence.append(localObject);
        }
      }
    }
  }
  
  public static String append(CharSequence paramCharSequence, Object... paramVarArgs)
  {
    Object localObject = paramCharSequence.toString();
    paramCharSequence = (CharSequence)localObject;
    if (paramVarArgs != null) {
      if (paramVarArgs.length != 0) {
        break label22;
      }
    }
    for (paramCharSequence = (CharSequence)localObject;; paramCharSequence = paramCharSequence.toString())
    {
      return paramCharSequence;
      label22:
      if (paramVarArgs.length % 2 != 0) {
        throw new IllegalArgumentException("Must specify an even number of parameter names/values");
      }
      paramCharSequence = new StringBuilder((String)localObject);
      addPathSeparator((String)localObject, paramCharSequence);
      addParamPrefix((String)localObject, paramCharSequence);
      paramCharSequence.append(paramVarArgs[0]);
      paramCharSequence.append('=');
      localObject = paramVarArgs[1];
      if (localObject != null) {
        paramCharSequence.append(localObject);
      }
      for (int i = 2; i < paramVarArgs.length; i += 2)
      {
        paramCharSequence.append('&');
        paramCharSequence.append(paramVarArgs[i]);
        paramCharSequence.append('=');
        localObject = paramVarArgs[(i + 1)];
        if (localObject != null) {
          paramCharSequence.append(localObject);
        }
      }
    }
  }
  
  /* Error */
  private HttpURLConnection createConnection()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 301	io/fabric/sdk/android/services/network/HttpRequest:httpProxyHost	Ljava/lang/String;
    //   4: ifnull +30 -> 34
    //   7: getstatic 174	io/fabric/sdk/android/services/network/HttpRequest:CONNECTION_FACTORY	Lio/fabric/sdk/android/services/network/HttpRequest$ConnectionFactory;
    //   10: aload_0
    //   11: getfield 202	io/fabric/sdk/android/services/network/HttpRequest:url	Ljava/net/URL;
    //   14: aload_0
    //   15: invokespecial 305	io/fabric/sdk/android/services/network/HttpRequest:createProxy	()Ljava/net/Proxy;
    //   18: invokeinterface 309 3 0
    //   23: astore_1
    //   24: aload_1
    //   25: aload_0
    //   26: getfield 204	io/fabric/sdk/android/services/network/HttpRequest:requestMethod	Ljava/lang/String;
    //   29: invokevirtual 314	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   32: aload_1
    //   33: areturn
    //   34: getstatic 174	io/fabric/sdk/android/services/network/HttpRequest:CONNECTION_FACTORY	Lio/fabric/sdk/android/services/network/HttpRequest$ConnectionFactory;
    //   37: aload_0
    //   38: getfield 202	io/fabric/sdk/android/services/network/HttpRequest:url	Ljava/net/URL;
    //   41: invokeinterface 317 2 0
    //   46: astore_1
    //   47: goto -23 -> 24
    //   50: astore_1
    //   51: new 36	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   54: dup
    //   55: aload_1
    //   56: invokespecial 207	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   59: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	60	0	this	HttpRequest
    //   23	24	1	localHttpURLConnection	HttpURLConnection
    //   50	6	1	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   0	24	50	java/io/IOException
    //   24	32	50	java/io/IOException
    //   34	47	50	java/io/IOException
  }
  
  private Proxy createProxy()
  {
    return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.httpProxyHost, this.httpProxyPort));
  }
  
  public static HttpRequest delete(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "DELETE");
  }
  
  public static HttpRequest delete(CharSequence paramCharSequence, Map<?, ?> paramMap, boolean paramBoolean)
  {
    paramMap = append(paramCharSequence, paramMap);
    paramCharSequence = paramMap;
    if (paramBoolean) {
      paramCharSequence = encode(paramMap);
    }
    return delete(paramCharSequence);
  }
  
  public static HttpRequest delete(CharSequence paramCharSequence, boolean paramBoolean, Object... paramVarArgs)
  {
    paramVarArgs = append(paramCharSequence, paramVarArgs);
    paramCharSequence = paramVarArgs;
    if (paramBoolean) {
      paramCharSequence = encode(paramVarArgs);
    }
    return delete(paramCharSequence);
  }
  
  public static HttpRequest delete(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "DELETE");
  }
  
  /* Error */
  public static String encode(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    // Byte code:
    //   0: new 191	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: invokeinterface 197 1 0
    //   10: invokespecial 200	java/net/URL:<init>	(Ljava/lang/String;)V
    //   13: astore_3
    //   14: aload_3
    //   15: invokevirtual 360	java/net/URL:getHost	()Ljava/lang/String;
    //   18: astore_2
    //   19: aload_3
    //   20: invokevirtual 363	java/net/URL:getPort	()I
    //   23: istore_1
    //   24: aload_2
    //   25: astore_0
    //   26: iload_1
    //   27: iconst_m1
    //   28: if_icmpeq +30 -> 58
    //   31: new 224	java/lang/StringBuilder
    //   34: dup
    //   35: invokespecial 364	java/lang/StringBuilder:<init>	()V
    //   38: aload_2
    //   39: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: bipush 58
    //   44: invokevirtual 232	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   47: iload_1
    //   48: invokestatic 369	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   51: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: invokevirtual 287	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   57: astore_0
    //   58: new 371	java/net/URI
    //   61: astore_2
    //   62: aload_2
    //   63: aload_3
    //   64: invokevirtual 374	java/net/URL:getProtocol	()Ljava/lang/String;
    //   67: aload_0
    //   68: aload_3
    //   69: invokevirtual 377	java/net/URL:getPath	()Ljava/lang/String;
    //   72: aload_3
    //   73: invokevirtual 380	java/net/URL:getQuery	()Ljava/lang/String;
    //   76: aconst_null
    //   77: invokespecial 383	java/net/URI:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   80: aload_2
    //   81: invokevirtual 386	java/net/URI:toASCIIString	()Ljava/lang/String;
    //   84: astore_2
    //   85: aload_2
    //   86: bipush 63
    //   88: invokevirtual 222	java/lang/String:indexOf	(I)I
    //   91: istore_1
    //   92: aload_2
    //   93: astore_0
    //   94: iload_1
    //   95: ifle +58 -> 153
    //   98: aload_2
    //   99: astore_0
    //   100: iload_1
    //   101: iconst_1
    //   102: iadd
    //   103: aload_2
    //   104: invokevirtual 387	java/lang/String:length	()I
    //   107: if_icmpge +46 -> 153
    //   110: new 224	java/lang/StringBuilder
    //   113: astore_0
    //   114: aload_0
    //   115: invokespecial 364	java/lang/StringBuilder:<init>	()V
    //   118: aload_0
    //   119: aload_2
    //   120: iconst_0
    //   121: iload_1
    //   122: iconst_1
    //   123: iadd
    //   124: invokevirtual 391	java/lang/String:substring	(II)Ljava/lang/String;
    //   127: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: aload_2
    //   131: iload_1
    //   132: iconst_1
    //   133: iadd
    //   134: invokevirtual 393	java/lang/String:substring	(I)Ljava/lang/String;
    //   137: ldc_w 395
    //   140: ldc_w 397
    //   143: invokevirtual 401	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   146: invokevirtual 277	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   149: invokevirtual 287	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   152: astore_0
    //   153: aload_0
    //   154: areturn
    //   155: astore_0
    //   156: new 36	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   159: dup
    //   160: aload_0
    //   161: invokespecial 207	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   164: athrow
    //   165: astore_2
    //   166: new 299	java/io/IOException
    //   169: dup
    //   170: ldc_w 403
    //   173: invokespecial 404	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   176: astore_0
    //   177: aload_0
    //   178: aload_2
    //   179: invokevirtual 408	java/io/IOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   182: pop
    //   183: new 36	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   186: dup
    //   187: aload_0
    //   188: invokespecial 207	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   191: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	192	0	paramCharSequence	CharSequence
    //   23	111	1	i	int
    //   18	113	2	localObject	Object
    //   165	14	2	localURISyntaxException	java.net.URISyntaxException
    //   13	60	3	localURL	URL
    // Exception table:
    //   from	to	target	type
    //   0	14	155	java/io/IOException
    //   58	92	165	java/net/URISyntaxException
    //   100	153	165	java/net/URISyntaxException
  }
  
  public static HttpRequest get(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "GET");
  }
  
  public static HttpRequest get(CharSequence paramCharSequence, Map<?, ?> paramMap, boolean paramBoolean)
  {
    paramMap = append(paramCharSequence, paramMap);
    paramCharSequence = paramMap;
    if (paramBoolean) {
      paramCharSequence = encode(paramMap);
    }
    return get(paramCharSequence);
  }
  
  public static HttpRequest get(CharSequence paramCharSequence, boolean paramBoolean, Object... paramVarArgs)
  {
    paramVarArgs = append(paramCharSequence, paramVarArgs);
    paramCharSequence = paramVarArgs;
    if (paramBoolean) {
      paramCharSequence = encode(paramVarArgs);
    }
    return get(paramCharSequence);
  }
  
  public static HttpRequest get(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "GET");
  }
  
  private static String getValidCharset(String paramString)
  {
    if ((paramString != null) && (paramString.length() > 0)) {}
    for (;;)
    {
      return paramString;
      paramString = "UTF-8";
    }
  }
  
  public static HttpRequest head(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "HEAD");
  }
  
  public static HttpRequest head(CharSequence paramCharSequence, Map<?, ?> paramMap, boolean paramBoolean)
  {
    paramMap = append(paramCharSequence, paramMap);
    paramCharSequence = paramMap;
    if (paramBoolean) {
      paramCharSequence = encode(paramMap);
    }
    return head(paramCharSequence);
  }
  
  public static HttpRequest head(CharSequence paramCharSequence, boolean paramBoolean, Object... paramVarArgs)
  {
    paramVarArgs = append(paramCharSequence, paramVarArgs);
    paramCharSequence = paramVarArgs;
    if (paramBoolean) {
      paramCharSequence = encode(paramVarArgs);
    }
    return head(paramCharSequence);
  }
  
  public static HttpRequest head(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "HEAD");
  }
  
  public static void keepAlive(boolean paramBoolean)
  {
    setProperty("http.keepAlive", Boolean.toString(paramBoolean));
  }
  
  public static void nonProxyHosts(String... paramVarArgs)
  {
    if ((paramVarArgs != null) && (paramVarArgs.length > 0))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      int j = paramVarArgs.length - 1;
      for (int i = 0; i < j; i++) {
        localStringBuilder.append(paramVarArgs[i]).append('|');
      }
      localStringBuilder.append(paramVarArgs[j]);
      setProperty("http.nonProxyHosts", localStringBuilder.toString());
    }
    for (;;)
    {
      return;
      setProperty("http.nonProxyHosts", null);
    }
  }
  
  public static HttpRequest options(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "OPTIONS");
  }
  
  public static HttpRequest options(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "OPTIONS");
  }
  
  public static HttpRequest post(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "POST");
  }
  
  public static HttpRequest post(CharSequence paramCharSequence, Map<?, ?> paramMap, boolean paramBoolean)
  {
    paramMap = append(paramCharSequence, paramMap);
    paramCharSequence = paramMap;
    if (paramBoolean) {
      paramCharSequence = encode(paramMap);
    }
    return post(paramCharSequence);
  }
  
  public static HttpRequest post(CharSequence paramCharSequence, boolean paramBoolean, Object... paramVarArgs)
  {
    paramVarArgs = append(paramCharSequence, paramVarArgs);
    paramCharSequence = paramVarArgs;
    if (paramBoolean) {
      paramCharSequence = encode(paramVarArgs);
    }
    return post(paramCharSequence);
  }
  
  public static HttpRequest post(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "POST");
  }
  
  public static void proxyHost(String paramString)
  {
    setProperty("http.proxyHost", paramString);
    setProperty("https.proxyHost", paramString);
  }
  
  public static void proxyPort(int paramInt)
  {
    String str = Integer.toString(paramInt);
    setProperty("http.proxyPort", str);
    setProperty("https.proxyPort", str);
  }
  
  public static HttpRequest put(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "PUT");
  }
  
  public static HttpRequest put(CharSequence paramCharSequence, Map<?, ?> paramMap, boolean paramBoolean)
  {
    paramMap = append(paramCharSequence, paramMap);
    paramCharSequence = paramMap;
    if (paramBoolean) {
      paramCharSequence = encode(paramMap);
    }
    return put(paramCharSequence);
  }
  
  public static HttpRequest put(CharSequence paramCharSequence, boolean paramBoolean, Object... paramVarArgs)
  {
    paramVarArgs = append(paramCharSequence, paramVarArgs);
    paramCharSequence = paramVarArgs;
    if (paramBoolean) {
      paramCharSequence = encode(paramVarArgs);
    }
    return put(paramCharSequence);
  }
  
  public static HttpRequest put(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "PUT");
  }
  
  public static void setConnectionFactory(ConnectionFactory paramConnectionFactory)
  {
    if (paramConnectionFactory == null) {}
    for (CONNECTION_FACTORY = ConnectionFactory.DEFAULT;; CONNECTION_FACTORY = paramConnectionFactory) {
      return;
    }
  }
  
  private static String setProperty(String paramString1, final String paramString2)
  {
    if (paramString2 != null) {}
    for (paramString1 = new PrivilegedAction()
        {
          public String run()
          {
            return System.setProperty(this.val$name, paramString2);
          }
        };; paramString1 = new PrivilegedAction()
        {
          public String run()
          {
            return System.clearProperty(this.val$name);
          }
        }) {
      return (String)AccessController.doPrivileged(paramString1);
    }
  }
  
  public static HttpRequest trace(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramCharSequence, "TRACE");
  }
  
  public static HttpRequest trace(URL paramURL)
    throws HttpRequest.HttpRequestException
  {
    return new HttpRequest(paramURL, "TRACE");
  }
  
  public HttpRequest accept(String paramString)
  {
    return header("Accept", paramString);
  }
  
  public HttpRequest acceptCharset(String paramString)
  {
    return header("Accept-Charset", paramString);
  }
  
  public HttpRequest acceptEncoding(String paramString)
  {
    return header("Accept-Encoding", paramString);
  }
  
  public HttpRequest acceptGzipEncoding()
  {
    return acceptEncoding("gzip");
  }
  
  public HttpRequest acceptJson()
  {
    return accept("application/json");
  }
  
  public HttpRequest authorization(String paramString)
  {
    return header("Authorization", paramString);
  }
  
  public boolean badRequest()
    throws HttpRequest.HttpRequestException
  {
    if (400 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public HttpRequest basic(String paramString1, String paramString2)
  {
    return authorization("Basic " + Base64.encode(new StringBuilder().append(paramString1).append(':').append(paramString2).toString()));
  }
  
  public HttpRequest body(AtomicReference<String> paramAtomicReference)
    throws HttpRequest.HttpRequestException
  {
    paramAtomicReference.set(body());
    return this;
  }
  
  public HttpRequest body(AtomicReference<String> paramAtomicReference, String paramString)
    throws HttpRequest.HttpRequestException
  {
    paramAtomicReference.set(body(paramString));
    return this;
  }
  
  public String body()
    throws HttpRequest.HttpRequestException
  {
    return body(charset());
  }
  
  public String body(String paramString)
    throws HttpRequest.HttpRequestException
  {
    ByteArrayOutputStream localByteArrayOutputStream = byteStream();
    try
    {
      copy(buffer(), localByteArrayOutputStream);
      paramString = localByteArrayOutputStream.toString(getValidCharset(paramString));
      return paramString;
    }
    catch (IOException paramString)
    {
      throw new HttpRequestException(paramString);
    }
  }
  
  public BufferedInputStream buffer()
    throws HttpRequest.HttpRequestException
  {
    return new BufferedInputStream(stream(), this.bufferSize);
  }
  
  public int bufferSize()
  {
    return this.bufferSize;
  }
  
  public HttpRequest bufferSize(int paramInt)
  {
    if (paramInt < 1) {
      throw new IllegalArgumentException("Size must be greater than zero");
    }
    this.bufferSize = paramInt;
    return this;
  }
  
  public BufferedReader bufferedReader()
    throws HttpRequest.HttpRequestException
  {
    return bufferedReader(charset());
  }
  
  public BufferedReader bufferedReader(String paramString)
    throws HttpRequest.HttpRequestException
  {
    return new BufferedReader(reader(paramString), this.bufferSize);
  }
  
  protected ByteArrayOutputStream byteStream()
  {
    int i = contentLength();
    if (i > 0) {}
    for (ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(i);; localByteArrayOutputStream = new ByteArrayOutputStream()) {
      return localByteArrayOutputStream;
    }
  }
  
  public byte[] bytes()
    throws HttpRequest.HttpRequestException
  {
    ByteArrayOutputStream localByteArrayOutputStream = byteStream();
    try
    {
      copy(buffer(), localByteArrayOutputStream);
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      throw new HttpRequestException(localIOException);
    }
  }
  
  public String cacheControl()
  {
    return header("Cache-Control");
  }
  
  public String charset()
  {
    return parameter("Content-Type", "charset");
  }
  
  public HttpRequest chunk(int paramInt)
  {
    getConnection().setChunkedStreamingMode(paramInt);
    return this;
  }
  
  protected HttpRequest closeOutput()
    throws IOException
  {
    if (this.output == null) {}
    for (;;)
    {
      return this;
      if (this.multipart) {
        this.output.write("\r\n--00content0boundary00--\r\n");
      }
      if (this.ignoreCloseExceptions) {}
      try
      {
        this.output.close();
        for (;;)
        {
          this.output = null;
          break;
          this.output.close();
        }
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  protected HttpRequest closeOutputQuietly()
    throws HttpRequest.HttpRequestException
  {
    try
    {
      HttpRequest localHttpRequest = closeOutput();
      return localHttpRequest;
    }
    catch (IOException localIOException)
    {
      throw new HttpRequestException(localIOException);
    }
  }
  
  public int code()
    throws HttpRequest.HttpRequestException
  {
    try
    {
      closeOutput();
      int i = getConnection().getResponseCode();
      return i;
    }
    catch (IOException localIOException)
    {
      throw new HttpRequestException(localIOException);
    }
  }
  
  public HttpRequest code(AtomicInteger paramAtomicInteger)
    throws HttpRequest.HttpRequestException
  {
    paramAtomicInteger.set(code());
    return this;
  }
  
  public HttpRequest connectTimeout(int paramInt)
  {
    getConnection().setConnectTimeout(paramInt);
    return this;
  }
  
  public String contentEncoding()
  {
    return header("Content-Encoding");
  }
  
  public int contentLength()
  {
    return intHeader("Content-Length");
  }
  
  public HttpRequest contentLength(int paramInt)
  {
    getConnection().setFixedLengthStreamingMode(paramInt);
    return this;
  }
  
  public HttpRequest contentLength(String paramString)
  {
    return contentLength(Integer.parseInt(paramString));
  }
  
  public HttpRequest contentType(String paramString)
  {
    return contentType(paramString, null);
  }
  
  public HttpRequest contentType(String paramString1, String paramString2)
  {
    if ((paramString2 != null) && (paramString2.length() > 0)) {}
    for (paramString1 = header("Content-Type", paramString1 + "; charset=" + paramString2);; paramString1 = header("Content-Type", paramString1)) {
      return paramString1;
    }
  }
  
  public String contentType()
  {
    return header("Content-Type");
  }
  
  protected HttpRequest copy(final InputStream paramInputStream, final OutputStream paramOutputStream)
    throws IOException
  {
    (HttpRequest)new CloseOperation(paramInputStream, this.ignoreCloseExceptions)
    {
      public HttpRequest run()
        throws IOException
      {
        byte[] arrayOfByte = new byte[HttpRequest.this.bufferSize];
        for (;;)
        {
          int i = paramInputStream.read(arrayOfByte);
          if (i == -1) {
            break;
          }
          paramOutputStream.write(arrayOfByte, 0, i);
        }
        return HttpRequest.this;
      }
    }.call();
  }
  
  protected HttpRequest copy(final Reader paramReader, final Writer paramWriter)
    throws IOException
  {
    (HttpRequest)new CloseOperation(paramReader, this.ignoreCloseExceptions)
    {
      public HttpRequest run()
        throws IOException
      {
        char[] arrayOfChar = new char[HttpRequest.this.bufferSize];
        for (;;)
        {
          int i = paramReader.read(arrayOfChar);
          if (i == -1) {
            break;
          }
          paramWriter.write(arrayOfChar, 0, i);
        }
        return HttpRequest.this;
      }
    }.call();
  }
  
  public boolean created()
    throws HttpRequest.HttpRequestException
  {
    if (201 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public long date()
  {
    return dateHeader("Date");
  }
  
  public long dateHeader(String paramString)
    throws HttpRequest.HttpRequestException
  {
    return dateHeader(paramString, -1L);
  }
  
  public long dateHeader(String paramString, long paramLong)
    throws HttpRequest.HttpRequestException
  {
    closeOutputQuietly();
    return getConnection().getHeaderFieldDate(paramString, paramLong);
  }
  
  public HttpRequest disconnect()
  {
    getConnection().disconnect();
    return this;
  }
  
  public String eTag()
  {
    return header("ETag");
  }
  
  public long expires()
  {
    return dateHeader("Expires");
  }
  
  public HttpRequest followRedirects(boolean paramBoolean)
  {
    getConnection().setInstanceFollowRedirects(paramBoolean);
    return this;
  }
  
  public HttpRequest form(Object paramObject1, Object paramObject2)
    throws HttpRequest.HttpRequestException
  {
    return form(paramObject1, paramObject2, "UTF-8");
  }
  
  public HttpRequest form(Object paramObject1, Object paramObject2, String paramString)
    throws HttpRequest.HttpRequestException
  {
    if (!this.form) {}
    for (int i = 1;; i = 0)
    {
      if (i != 0)
      {
        contentType("application/x-www-form-urlencoded", paramString);
        this.form = true;
      }
      paramString = getValidCharset(paramString);
      try
      {
        openOutput();
        if (i == 0) {
          this.output.write(38);
        }
        this.output.write(URLEncoder.encode(paramObject1.toString(), paramString));
        this.output.write(61);
        if (paramObject2 != null) {
          this.output.write(URLEncoder.encode(paramObject2.toString(), paramString));
        }
        return this;
      }
      catch (IOException paramObject1)
      {
        throw new HttpRequestException((IOException)paramObject1);
      }
    }
  }
  
  public HttpRequest form(Map.Entry<?, ?> paramEntry)
    throws HttpRequest.HttpRequestException
  {
    return form(paramEntry, "UTF-8");
  }
  
  public HttpRequest form(Map.Entry<?, ?> paramEntry, String paramString)
    throws HttpRequest.HttpRequestException
  {
    return form(paramEntry.getKey(), paramEntry.getValue(), paramString);
  }
  
  public HttpRequest form(Map<?, ?> paramMap)
    throws HttpRequest.HttpRequestException
  {
    return form(paramMap, "UTF-8");
  }
  
  public HttpRequest form(Map<?, ?> paramMap, String paramString)
    throws HttpRequest.HttpRequestException
  {
    if (!paramMap.isEmpty())
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext()) {
        form((Map.Entry)paramMap.next(), paramString);
      }
    }
    return this;
  }
  
  public HttpURLConnection getConnection()
  {
    if (this.connection == null) {
      this.connection = createConnection();
    }
    return this.connection;
  }
  
  protected String getParam(String paramString1, String paramString2)
  {
    if ((paramString1 == null) || (paramString1.length() == 0)) {
      paramString1 = null;
    }
    for (;;)
    {
      return paramString1;
      int j = paramString1.length();
      int m = paramString1.indexOf(';') + 1;
      if ((m == 0) || (m == j))
      {
        paramString1 = null;
      }
      else
      {
        int n = paramString1.indexOf(';', m);
        int i = n;
        int k = m;
        if (n == -1)
        {
          i = j;
          k = m;
        }
        for (;;)
        {
          if (k >= i) {
            break label242;
          }
          m = paramString1.indexOf('=', k);
          if ((m != -1) && (m < i) && (paramString2.equals(paramString1.substring(k, m).trim())))
          {
            String str = paramString1.substring(m + 1, i).trim();
            k = str.length();
            if (k != 0)
            {
              paramString1 = str;
              if (k <= 2) {
                break;
              }
              paramString1 = str;
              if ('"' != str.charAt(0)) {
                break;
              }
              paramString1 = str;
              if ('"' != str.charAt(k - 1)) {
                break;
              }
              paramString1 = str.substring(1, k - 1);
              break;
            }
          }
          m = i + 1;
          n = paramString1.indexOf(';', m);
          i = n;
          k = m;
          if (n == -1)
          {
            i = j;
            k = m;
          }
        }
        label242:
        paramString1 = null;
      }
    }
  }
  
  protected Map<String, String> getParams(String paramString)
  {
    Object localObject;
    if ((paramString == null) || (paramString.length() == 0)) {
      localObject = Collections.emptyMap();
    }
    int j;
    int k;
    int i;
    LinkedHashMap localLinkedHashMap;
    do
    {
      for (;;)
      {
        return (Map<String, String>)localObject;
        j = paramString.length();
        k = paramString.indexOf(';') + 1;
        if ((k != 0) && (k != j)) {
          break;
        }
        localObject = Collections.emptyMap();
      }
      m = paramString.indexOf(';', k);
      i = m;
      if (m == -1) {
        i = j;
      }
      localLinkedHashMap = new LinkedHashMap();
      localObject = localLinkedHashMap;
    } while (k >= i);
    int m = paramString.indexOf('=', k);
    String str;
    if ((m != -1) && (m < i))
    {
      localObject = paramString.substring(k, m).trim();
      if (((String)localObject).length() > 0)
      {
        str = paramString.substring(m + 1, i).trim();
        k = str.length();
        if (k != 0)
        {
          if ((k <= 2) || ('"' != str.charAt(0)) || ('"' != str.charAt(k - 1))) {
            break label250;
          }
          localLinkedHashMap.put(localObject, str.substring(1, k - 1));
        }
      }
    }
    for (;;)
    {
      m = i + 1;
      int n = paramString.indexOf(';', m);
      i = n;
      k = m;
      if (n != -1) {
        break;
      }
      i = j;
      k = m;
      break;
      label250:
      localLinkedHashMap.put(localObject, str);
    }
  }
  
  public HttpRequest header(String paramString, Number paramNumber)
  {
    if (paramNumber != null) {}
    for (paramNumber = paramNumber.toString();; paramNumber = null) {
      return header(paramString, paramNumber);
    }
  }
  
  public HttpRequest header(String paramString1, String paramString2)
  {
    getConnection().setRequestProperty(paramString1, paramString2);
    return this;
  }
  
  public HttpRequest header(Map.Entry<String, String> paramEntry)
  {
    return header((String)paramEntry.getKey(), (String)paramEntry.getValue());
  }
  
  public String header(String paramString)
    throws HttpRequest.HttpRequestException
  {
    closeOutputQuietly();
    return getConnection().getHeaderField(paramString);
  }
  
  public HttpRequest headers(Map<String, String> paramMap)
  {
    if (!paramMap.isEmpty())
    {
      paramMap = paramMap.entrySet().iterator();
      while (paramMap.hasNext()) {
        header((Map.Entry)paramMap.next());
      }
    }
    return this;
  }
  
  public Map<String, List<String>> headers()
    throws HttpRequest.HttpRequestException
  {
    closeOutputQuietly();
    return getConnection().getHeaderFields();
  }
  
  public String[] headers(String paramString)
  {
    Map localMap = headers();
    if ((localMap == null) || (localMap.isEmpty())) {
      paramString = EMPTY_STRINGS;
    }
    for (;;)
    {
      return paramString;
      paramString = (List)localMap.get(paramString);
      if ((paramString != null) && (!paramString.isEmpty())) {
        paramString = (String[])paramString.toArray(new String[paramString.size()]);
      } else {
        paramString = EMPTY_STRINGS;
      }
    }
  }
  
  public HttpRequest ifModifiedSince(long paramLong)
  {
    getConnection().setIfModifiedSince(paramLong);
    return this;
  }
  
  public HttpRequest ifNoneMatch(String paramString)
  {
    return header("If-None-Match", paramString);
  }
  
  public HttpRequest ignoreCloseExceptions(boolean paramBoolean)
  {
    this.ignoreCloseExceptions = paramBoolean;
    return this;
  }
  
  public boolean ignoreCloseExceptions()
  {
    return this.ignoreCloseExceptions;
  }
  
  public int intHeader(String paramString)
    throws HttpRequest.HttpRequestException
  {
    return intHeader(paramString, -1);
  }
  
  public int intHeader(String paramString, int paramInt)
    throws HttpRequest.HttpRequestException
  {
    closeOutputQuietly();
    return getConnection().getHeaderFieldInt(paramString, paramInt);
  }
  
  public boolean isBodyEmpty()
    throws HttpRequest.HttpRequestException
  {
    if (contentLength() == 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public long lastModified()
  {
    return dateHeader("Last-Modified");
  }
  
  public String location()
  {
    return header("Location");
  }
  
  public String message()
    throws HttpRequest.HttpRequestException
  {
    try
    {
      closeOutput();
      String str = getConnection().getResponseMessage();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new HttpRequestException(localIOException);
    }
  }
  
  public String method()
  {
    return getConnection().getRequestMethod();
  }
  
  public boolean notFound()
    throws HttpRequest.HttpRequestException
  {
    if (404 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean notModified()
    throws HttpRequest.HttpRequestException
  {
    if (304 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean ok()
    throws HttpRequest.HttpRequestException
  {
    if (200 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected HttpRequest openOutput()
    throws IOException
  {
    if (this.output != null) {}
    for (;;)
    {
      return this;
      getConnection().setDoOutput(true);
      String str = getParam(getConnection().getRequestProperty("Content-Type"), "charset");
      this.output = new RequestOutputStream(getConnection().getOutputStream(), str, this.bufferSize);
    }
  }
  
  public String parameter(String paramString1, String paramString2)
  {
    return getParam(header(paramString1), paramString2);
  }
  
  public Map<String, String> parameters(String paramString)
  {
    return getParams(header(paramString));
  }
  
  public HttpRequest part(String paramString, File paramFile)
    throws HttpRequest.HttpRequestException
  {
    return part(paramString, null, paramFile);
  }
  
  public HttpRequest part(String paramString, InputStream paramInputStream)
    throws HttpRequest.HttpRequestException
  {
    return part(paramString, null, null, paramInputStream);
  }
  
  public HttpRequest part(String paramString, Number paramNumber)
    throws HttpRequest.HttpRequestException
  {
    return part(paramString, null, paramNumber);
  }
  
  public HttpRequest part(String paramString1, String paramString2)
  {
    return part(paramString1, null, paramString2);
  }
  
  public HttpRequest part(String paramString1, String paramString2, File paramFile)
    throws HttpRequest.HttpRequestException
  {
    return part(paramString1, paramString2, null, paramFile);
  }
  
  public HttpRequest part(String paramString1, String paramString2, Number paramNumber)
    throws HttpRequest.HttpRequestException
  {
    if (paramNumber != null) {}
    for (paramNumber = paramNumber.toString();; paramNumber = null) {
      return part(paramString1, paramString2, paramNumber);
    }
  }
  
  public HttpRequest part(String paramString1, String paramString2, String paramString3)
    throws HttpRequest.HttpRequestException
  {
    return part(paramString1, paramString2, null, paramString3);
  }
  
  /* Error */
  public HttpRequest part(String paramString1, String paramString2, String paramString3, File paramFile)
    throws HttpRequest.HttpRequestException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 8
    //   3: aconst_null
    //   4: astore 7
    //   6: aload 8
    //   8: astore 5
    //   10: new 524	java/io/BufferedInputStream
    //   13: astore 6
    //   15: aload 8
    //   17: astore 5
    //   19: new 813	java/io/FileInputStream
    //   22: astore 9
    //   24: aload 8
    //   26: astore 5
    //   28: aload 9
    //   30: aload 4
    //   32: invokespecial 816	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   35: aload 8
    //   37: astore 5
    //   39: aload 6
    //   41: aload 9
    //   43: invokespecial 819	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   46: aload_0
    //   47: aload_1
    //   48: aload_2
    //   49: aload_3
    //   50: aload 6
    //   52: invokevirtual 799	io/fabric/sdk/android/services/network/HttpRequest:part	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lio/fabric/sdk/android/services/network/HttpRequest;
    //   55: astore_1
    //   56: aload 6
    //   58: ifnull +8 -> 66
    //   61: aload 6
    //   63: invokevirtual 822	java/io/InputStream:close	()V
    //   66: aload_1
    //   67: areturn
    //   68: astore_2
    //   69: aload 7
    //   71: astore_1
    //   72: aload_1
    //   73: astore 5
    //   75: new 36	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   78: astore_3
    //   79: aload_1
    //   80: astore 5
    //   82: aload_3
    //   83: aload_2
    //   84: invokespecial 207	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   87: aload_1
    //   88: astore 5
    //   90: aload_3
    //   91: athrow
    //   92: astore_1
    //   93: aload 5
    //   95: ifnull +8 -> 103
    //   98: aload 5
    //   100: invokevirtual 822	java/io/InputStream:close	()V
    //   103: aload_1
    //   104: athrow
    //   105: astore_2
    //   106: goto -40 -> 66
    //   109: astore_2
    //   110: goto -7 -> 103
    //   113: astore_1
    //   114: aload 6
    //   116: astore 5
    //   118: goto -25 -> 93
    //   121: astore_2
    //   122: aload 6
    //   124: astore_1
    //   125: goto -53 -> 72
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	128	0	this	HttpRequest
    //   0	128	1	paramString1	String
    //   0	128	2	paramString2	String
    //   0	128	3	paramString3	String
    //   0	128	4	paramFile	File
    //   8	109	5	localObject1	Object
    //   13	110	6	localBufferedInputStream	BufferedInputStream
    //   4	66	7	localObject2	Object
    //   1	35	8	localObject3	Object
    //   22	20	9	localFileInputStream	FileInputStream
    // Exception table:
    //   from	to	target	type
    //   10	15	68	java/io/IOException
    //   19	24	68	java/io/IOException
    //   28	35	68	java/io/IOException
    //   39	46	68	java/io/IOException
    //   10	15	92	finally
    //   19	24	92	finally
    //   28	35	92	finally
    //   39	46	92	finally
    //   75	79	92	finally
    //   82	87	92	finally
    //   90	92	92	finally
    //   61	66	105	java/io/IOException
    //   98	103	109	java/io/IOException
    //   46	56	113	finally
    //   46	56	121	java/io/IOException
  }
  
  public HttpRequest part(String paramString1, String paramString2, String paramString3, InputStream paramInputStream)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      startPart();
      writePartHeader(paramString1, paramString2, paramString3);
      copy(paramInputStream, this.output);
      return this;
    }
    catch (IOException paramString1)
    {
      throw new HttpRequestException(paramString1);
    }
  }
  
  public HttpRequest part(String paramString1, String paramString2, String paramString3, String paramString4)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      startPart();
      writePartHeader(paramString1, paramString2, paramString3);
      this.output.write(paramString4);
      return this;
    }
    catch (IOException paramString1)
    {
      throw new HttpRequestException(paramString1);
    }
  }
  
  public HttpRequest partHeader(String paramString1, String paramString2)
    throws HttpRequest.HttpRequestException
  {
    return send(paramString1).send(": ").send(paramString2).send("\r\n");
  }
  
  public HttpRequest proxyAuthorization(String paramString)
  {
    return header("Proxy-Authorization", paramString);
  }
  
  public HttpRequest proxyBasic(String paramString1, String paramString2)
  {
    return proxyAuthorization("Basic " + Base64.encode(new StringBuilder().append(paramString1).append(':').append(paramString2).toString()));
  }
  
  public HttpRequest readTimeout(int paramInt)
  {
    getConnection().setReadTimeout(paramInt);
    return this;
  }
  
  public InputStreamReader reader()
    throws HttpRequest.HttpRequestException
  {
    return reader(charset());
  }
  
  public InputStreamReader reader(String paramString)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      paramString = new InputStreamReader(stream(), getValidCharset(paramString));
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new HttpRequestException(paramString);
    }
  }
  
  public HttpRequest receive(final File paramFile)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
      localFileOutputStream.<init>(paramFile);
      paramFile = new BufferedOutputStream(localFileOutputStream, this.bufferSize);
      (HttpRequest)new CloseOperation(paramFile, this.ignoreCloseExceptions)
      {
        protected HttpRequest run()
          throws HttpRequest.HttpRequestException, IOException
        {
          return HttpRequest.this.receive(paramFile);
        }
      }.call();
    }
    catch (FileNotFoundException paramFile)
    {
      throw new HttpRequestException(paramFile);
    }
  }
  
  public HttpRequest receive(OutputStream paramOutputStream)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      paramOutputStream = copy(buffer(), paramOutputStream);
      return paramOutputStream;
    }
    catch (IOException paramOutputStream)
    {
      throw new HttpRequestException(paramOutputStream);
    }
  }
  
  public HttpRequest receive(PrintStream paramPrintStream)
    throws HttpRequest.HttpRequestException
  {
    return receive(paramPrintStream);
  }
  
  public HttpRequest receive(final Writer paramWriter)
    throws HttpRequest.HttpRequestException
  {
    final BufferedReader localBufferedReader = bufferedReader();
    (HttpRequest)new CloseOperation(localBufferedReader, this.ignoreCloseExceptions)
    {
      public HttpRequest run()
        throws IOException
      {
        return HttpRequest.this.copy(localBufferedReader, paramWriter);
      }
    }.call();
  }
  
  public HttpRequest receive(final Appendable paramAppendable)
    throws HttpRequest.HttpRequestException
  {
    final BufferedReader localBufferedReader = bufferedReader();
    (HttpRequest)new CloseOperation(localBufferedReader, this.ignoreCloseExceptions)
    {
      public HttpRequest run()
        throws IOException
      {
        CharBuffer localCharBuffer = CharBuffer.allocate(HttpRequest.this.bufferSize);
        for (;;)
        {
          int i = localBufferedReader.read(localCharBuffer);
          if (i == -1) {
            break;
          }
          localCharBuffer.rewind();
          paramAppendable.append(localCharBuffer, 0, i);
          localCharBuffer.rewind();
        }
        return HttpRequest.this;
      }
    }.call();
  }
  
  public HttpRequest referer(String paramString)
  {
    return header("Referer", paramString);
  }
  
  public HttpRequest send(File paramFile)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile);
      paramFile = new BufferedInputStream(localFileInputStream);
      return send(paramFile);
    }
    catch (FileNotFoundException paramFile)
    {
      throw new HttpRequestException(paramFile);
    }
  }
  
  public HttpRequest send(InputStream paramInputStream)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      openOutput();
      copy(paramInputStream, this.output);
      return this;
    }
    catch (IOException paramInputStream)
    {
      throw new HttpRequestException(paramInputStream);
    }
  }
  
  public HttpRequest send(final Reader paramReader)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      openOutput();
      final OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(this.output, this.output.encoder.charset());
      (HttpRequest)new FlushOperation(localOutputStreamWriter)
      {
        protected HttpRequest run()
          throws IOException
        {
          return HttpRequest.this.copy(paramReader, localOutputStreamWriter);
        }
      }.call();
    }
    catch (IOException paramReader)
    {
      throw new HttpRequestException(paramReader);
    }
  }
  
  public HttpRequest send(CharSequence paramCharSequence)
    throws HttpRequest.HttpRequestException
  {
    try
    {
      openOutput();
      this.output.write(paramCharSequence.toString());
      return this;
    }
    catch (IOException paramCharSequence)
    {
      throw new HttpRequestException(paramCharSequence);
    }
  }
  
  public HttpRequest send(byte[] paramArrayOfByte)
    throws HttpRequest.HttpRequestException
  {
    return send(new ByteArrayInputStream(paramArrayOfByte));
  }
  
  public String server()
  {
    return header("Server");
  }
  
  public boolean serverError()
    throws HttpRequest.HttpRequestException
  {
    if (500 == code()) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  protected HttpRequest startPart()
    throws IOException
  {
    if (!this.multipart)
    {
      this.multipart = true;
      contentType("multipart/form-data; boundary=00content0boundary00").openOutput();
      this.output.write("--00content0boundary00\r\n");
    }
    for (;;)
    {
      return this;
      this.output.write("\r\n--00content0boundary00\r\n");
    }
  }
  
  public InputStream stream()
    throws HttpRequest.HttpRequestException
  {
    if (code() < 400) {}
    for (;;)
    {
      try
      {
        InputStream localInputStream = getConnection().getInputStream();
        localObject2 = localInputStream;
        if (this.uncompress)
        {
          if ("gzip".equals(contentEncoding())) {
            break label88;
          }
          localObject2 = localInputStream;
        }
        return (InputStream)localObject2;
      }
      catch (IOException localIOException1)
      {
        throw new HttpRequestException(localIOException1);
      }
      Object localObject2 = getConnection().getErrorStream();
      Object localObject1 = localObject2;
      if (localObject2 != null) {
        continue;
      }
      try
      {
        localObject1 = getConnection().getInputStream();
      }
      catch (IOException localIOException2)
      {
        throw new HttpRequestException(localIOException2);
      }
      try
      {
        label88:
        localObject2 = new GZIPInputStream(localIOException2);
      }
      catch (IOException localIOException3)
      {
        throw new HttpRequestException(localIOException3);
      }
    }
  }
  
  public String toString()
  {
    return method() + ' ' + url();
  }
  
  public HttpRequest trustAllCerts()
    throws HttpRequest.HttpRequestException
  {
    return this;
  }
  
  public HttpRequest trustAllHosts()
  {
    return this;
  }
  
  public HttpRequest uncompress(boolean paramBoolean)
  {
    this.uncompress = paramBoolean;
    return this;
  }
  
  public URL url()
  {
    return getConnection().getURL();
  }
  
  public HttpRequest useCaches(boolean paramBoolean)
  {
    getConnection().setUseCaches(paramBoolean);
    return this;
  }
  
  public HttpRequest useProxy(String paramString, int paramInt)
  {
    if (this.connection != null) {
      throw new IllegalStateException("The connection has already been created. This method must be called before reading or writing to the request.");
    }
    this.httpProxyHost = paramString;
    this.httpProxyPort = paramInt;
    return this;
  }
  
  public HttpRequest userAgent(String paramString)
  {
    return header("User-Agent", paramString);
  }
  
  protected HttpRequest writePartHeader(String paramString1, String paramString2)
    throws IOException
  {
    return writePartHeader(paramString1, paramString2, null);
  }
  
  protected HttpRequest writePartHeader(String paramString1, String paramString2, String paramString3)
    throws IOException
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("form-data; name=\"").append(paramString1);
    if (paramString2 != null) {
      localStringBuilder.append("\"; filename=\"").append(paramString2);
    }
    localStringBuilder.append('"');
    partHeader("Content-Disposition", localStringBuilder.toString());
    if (paramString3 != null) {
      partHeader("Content-Type", paramString3);
    }
    return send("\r\n");
  }
  
  public OutputStreamWriter writer()
    throws HttpRequest.HttpRequestException
  {
    try
    {
      openOutput();
      OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(this.output, this.output.encoder.charset());
      return localOutputStreamWriter;
    }
    catch (IOException localIOException)
    {
      throw new HttpRequestException(localIOException);
    }
  }
  
  public static class Base64
  {
    private static final byte EQUALS_SIGN = 61;
    private static final String PREFERRED_ENCODING = "US-ASCII";
    private static final byte[] _STANDARD_ALPHABET = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
    
    public static String encode(String paramString)
    {
      try
      {
        byte[] arrayOfByte = paramString.getBytes("US-ASCII");
        paramString = arrayOfByte;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        for (;;)
        {
          paramString = paramString.getBytes();
        }
      }
      return encodeBytes(paramString);
    }
    
    private static byte[] encode3to4(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, byte[] paramArrayOfByte2, int paramInt3)
    {
      int k = 0;
      byte[] arrayOfByte = _STANDARD_ALPHABET;
      int i;
      int j;
      if (paramInt2 > 0)
      {
        i = paramArrayOfByte1[paramInt1] << 24 >>> 8;
        if (paramInt2 <= 1) {
          break label104;
        }
        j = paramArrayOfByte1[(paramInt1 + 1)] << 24 >>> 16;
        label41:
        if (paramInt2 > 2) {
          k = paramArrayOfByte1[(paramInt1 + 2)] << 24 >>> 24;
        }
        paramInt1 = j | i | k;
        switch (paramInt2)
        {
        }
      }
      for (;;)
      {
        return paramArrayOfByte2;
        i = 0;
        break;
        label104:
        j = 0;
        break label41;
        paramArrayOfByte2[paramInt3] = arrayOfByte[(paramInt1 >>> 18)];
        paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(paramInt1 >>> 12 & 0x3F)];
        paramArrayOfByte2[(paramInt3 + 2)] = arrayOfByte[(paramInt1 >>> 6 & 0x3F)];
        paramArrayOfByte2[(paramInt3 + 3)] = arrayOfByte[(paramInt1 & 0x3F)];
        continue;
        paramArrayOfByte2[paramInt3] = arrayOfByte[(paramInt1 >>> 18)];
        paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(paramInt1 >>> 12 & 0x3F)];
        paramArrayOfByte2[(paramInt3 + 2)] = arrayOfByte[(paramInt1 >>> 6 & 0x3F)];
        paramArrayOfByte2[(paramInt3 + 3)] = 61;
        continue;
        paramArrayOfByte2[paramInt3] = arrayOfByte[(paramInt1 >>> 18)];
        paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(paramInt1 >>> 12 & 0x3F)];
        paramArrayOfByte2[(paramInt3 + 2)] = 61;
        paramArrayOfByte2[(paramInt3 + 3)] = 61;
      }
    }
    
    public static String encodeBytes(byte[] paramArrayOfByte)
    {
      return encodeBytes(paramArrayOfByte, 0, paramArrayOfByte.length);
    }
    
    public static String encodeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      byte[] arrayOfByte = encodeBytesToBytes(paramArrayOfByte, paramInt1, paramInt2);
      try
      {
        paramArrayOfByte = new java/lang/String;
        paramArrayOfByte.<init>(arrayOfByte, "US-ASCII");
        return paramArrayOfByte;
      }
      catch (UnsupportedEncodingException paramArrayOfByte)
      {
        for (;;)
        {
          paramArrayOfByte = new String(arrayOfByte);
        }
      }
    }
    
    public static byte[] encodeBytesToBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      if (paramArrayOfByte == null) {
        throw new NullPointerException("Cannot serialize a null array.");
      }
      if (paramInt1 < 0) {
        throw new IllegalArgumentException("Cannot have negative offset: " + paramInt1);
      }
      if (paramInt2 < 0) {
        throw new IllegalArgumentException("Cannot have length offset: " + paramInt2);
      }
      if (paramInt1 + paramInt2 > paramArrayOfByte.length) {
        throw new IllegalArgumentException(String.format(Locale.ENGLISH, "Cannot have offset of %d and length of %d with array of length %d", new Object[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramArrayOfByte.length) }));
      }
      int j = paramInt2 / 3;
      if (paramInt2 % 3 > 0) {}
      byte[] arrayOfByte;
      for (int i = 4;; i = 0)
      {
        arrayOfByte = new byte[j * 4 + i];
        j = 0;
        for (i = 0; j < paramInt2 - 2; i += 4)
        {
          encode3to4(paramArrayOfByte, j + paramInt1, 3, arrayOfByte, i);
          j += 3;
        }
      }
      int k = i;
      if (j < paramInt2)
      {
        encode3to4(paramArrayOfByte, j + paramInt1, paramInt2 - j, arrayOfByte, i);
        k = i + 4;
      }
      if (k <= arrayOfByte.length - 1)
      {
        paramArrayOfByte = new byte[k];
        System.arraycopy(arrayOfByte, 0, paramArrayOfByte, 0, k);
      }
      for (;;)
      {
        return paramArrayOfByte;
        paramArrayOfByte = arrayOfByte;
      }
    }
  }
  
  protected static abstract class CloseOperation<V>
    extends HttpRequest.Operation<V>
  {
    private final Closeable closeable;
    private final boolean ignoreCloseExceptions;
    
    protected CloseOperation(Closeable paramCloseable, boolean paramBoolean)
    {
      this.closeable = paramCloseable;
      this.ignoreCloseExceptions = paramBoolean;
    }
    
    protected void done()
      throws IOException
    {
      if ((this.closeable instanceof Flushable)) {
        ((Flushable)this.closeable).flush();
      }
      if (this.ignoreCloseExceptions) {}
      try
      {
        this.closeable.close();
        for (;;)
        {
          return;
          this.closeable.close();
        }
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  public static abstract interface ConnectionFactory
  {
    public static final ConnectionFactory DEFAULT = new ConnectionFactory()
    {
      public HttpURLConnection create(URL paramAnonymousURL)
        throws IOException
      {
        return (HttpURLConnection)paramAnonymousURL.openConnection();
      }
      
      public HttpURLConnection create(URL paramAnonymousURL, Proxy paramAnonymousProxy)
        throws IOException
      {
        return (HttpURLConnection)paramAnonymousURL.openConnection(paramAnonymousProxy);
      }
    };
    
    public abstract HttpURLConnection create(URL paramURL)
      throws IOException;
    
    public abstract HttpURLConnection create(URL paramURL, Proxy paramProxy)
      throws IOException;
  }
  
  protected static abstract class FlushOperation<V>
    extends HttpRequest.Operation<V>
  {
    private final Flushable flushable;
    
    protected FlushOperation(Flushable paramFlushable)
    {
      this.flushable = paramFlushable;
    }
    
    protected void done()
      throws IOException
    {
      this.flushable.flush();
    }
  }
  
  public static class HttpRequestException
    extends RuntimeException
  {
    private static final long serialVersionUID = -1170466989781746231L;
    
    protected HttpRequestException(IOException paramIOException)
    {
      super();
    }
    
    public IOException getCause()
    {
      return (IOException)super.getCause();
    }
  }
  
  protected static abstract class Operation<V>
    implements Callable<V>
  {
    /* Error */
    public V call()
      throws HttpRequest.HttpRequestException
    {
      // Byte code:
      //   0: iconst_0
      //   1: istore_1
      //   2: aload_0
      //   3: invokevirtual 24	io/fabric/sdk/android/services/network/HttpRequest$Operation:run	()Ljava/lang/Object;
      //   6: astore_3
      //   7: aload_0
      //   8: invokevirtual 27	io/fabric/sdk/android/services/network/HttpRequest$Operation:done	()V
      //   11: aload_3
      //   12: areturn
      //   13: astore 4
      //   15: iconst_0
      //   16: ifne -5 -> 11
      //   19: new 19	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
      //   22: dup
      //   23: aload 4
      //   25: invokespecial 30	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
      //   28: athrow
      //   29: astore_3
      //   30: iconst_1
      //   31: istore_1
      //   32: aload_3
      //   33: athrow
      //   34: astore_3
      //   35: aload_0
      //   36: invokevirtual 27	io/fabric/sdk/android/services/network/HttpRequest$Operation:done	()V
      //   39: aload_3
      //   40: athrow
      //   41: astore_3
      //   42: iconst_1
      //   43: istore_2
      //   44: iload_2
      //   45: istore_1
      //   46: new 19	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
      //   49: astore 4
      //   51: iload_2
      //   52: istore_1
      //   53: aload 4
      //   55: aload_3
      //   56: invokespecial 30	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
      //   59: iload_2
      //   60: istore_1
      //   61: aload 4
      //   63: athrow
      //   64: astore 4
      //   66: iload_1
      //   67: ifne -28 -> 39
      //   70: new 19	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
      //   73: dup
      //   74: aload 4
      //   76: invokespecial 30	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
      //   79: athrow
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	80	0	this	Operation
      //   1	66	1	i	int
      //   43	17	2	j	int
      //   6	6	3	localObject1	Object
      //   29	4	3	localHttpRequestException1	HttpRequest.HttpRequestException
      //   34	6	3	localObject2	Object
      //   41	15	3	localIOException1	IOException
      //   13	11	4	localIOException2	IOException
      //   49	13	4	localHttpRequestException2	HttpRequest.HttpRequestException
      //   64	11	4	localIOException3	IOException
      // Exception table:
      //   from	to	target	type
      //   7	11	13	java/io/IOException
      //   2	7	29	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
      //   2	7	34	finally
      //   32	34	34	finally
      //   46	51	34	finally
      //   53	59	34	finally
      //   61	64	34	finally
      //   2	7	41	java/io/IOException
      //   35	39	64	java/io/IOException
    }
    
    protected abstract void done()
      throws IOException;
    
    protected abstract V run()
      throws HttpRequest.HttpRequestException, IOException;
  }
  
  public static class RequestOutputStream
    extends BufferedOutputStream
  {
    private final CharsetEncoder encoder;
    
    public RequestOutputStream(OutputStream paramOutputStream, String paramString, int paramInt)
    {
      super(paramInt);
      this.encoder = Charset.forName(HttpRequest.getValidCharset(paramString)).newEncoder();
    }
    
    public RequestOutputStream write(String paramString)
      throws IOException
    {
      paramString = this.encoder.encode(CharBuffer.wrap(paramString));
      super.write(paramString.array(), 0, paramString.limit());
      return this;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\network\HttpRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */