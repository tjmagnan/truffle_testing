package me.zhanghai.android.materialprogressbar;

public abstract interface ShowBackgroundDrawable
{
  public abstract boolean getShowBackground();
  
  public abstract void setShowBackground(boolean paramBoolean);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\ShowBackgroundDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */