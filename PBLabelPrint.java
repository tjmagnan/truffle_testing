package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBLabelPrint
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -295457453615897236L;
  @Expose
  @SerializedName("disabled")
  private Boolean disabled;
  @Expose
  @SerializedName("disabledValue")
  private Object disabledValue;
  @Expose
  @SerializedName("optionName")
  private String optionName;
  @Expose
  @SerializedName("optionType")
  private String optionType;
  
  public PBLabelPrint() {}
  
  public PBLabelPrint(Boolean paramBoolean, Object paramObject, String paramString1, String paramString2)
  {
    this.disabled = paramBoolean;
    this.disabledValue = paramObject;
    this.optionName = paramString1;
    this.optionType = paramString2;
  }
  
  public Boolean getDisabled()
  {
    return this.disabled;
  }
  
  public Object getDisabledValue()
  {
    return this.disabledValue;
  }
  
  public String getOptionName()
  {
    return this.optionName;
  }
  
  public String getOptionType()
  {
    return this.optionType;
  }
  
  public void setDisabled(Boolean paramBoolean)
  {
    this.disabled = paramBoolean;
  }
  
  public void setDisabledValue(Object paramObject)
  {
    this.disabledValue = paramObject;
  }
  
  public void setOptionName(String paramString)
  {
    this.optionName = paramString;
  }
  
  public void setOptionType(String paramString)
  {
    this.optionType = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBLabelPrint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */