package org.joda.time;

import java.io.Serializable;
import org.joda.time.base.BaseInterval;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Interval
  extends BaseInterval
  implements ReadableInterval, Serializable
{
  private static final long serialVersionUID = 4922451897541386752L;
  
  public Interval(long paramLong1, long paramLong2)
  {
    super(paramLong1, paramLong2, null);
  }
  
  public Interval(long paramLong1, long paramLong2, Chronology paramChronology)
  {
    super(paramLong1, paramLong2, paramChronology);
  }
  
  public Interval(long paramLong1, long paramLong2, DateTimeZone paramDateTimeZone)
  {
    super(paramLong1, paramLong2, ISOChronology.getInstance(paramDateTimeZone));
  }
  
  public Interval(Object paramObject)
  {
    super(paramObject, null);
  }
  
  public Interval(Object paramObject, Chronology paramChronology)
  {
    super(paramObject, paramChronology);
  }
  
  public Interval(ReadableDuration paramReadableDuration, ReadableInstant paramReadableInstant)
  {
    super(paramReadableDuration, paramReadableInstant);
  }
  
  public Interval(ReadableInstant paramReadableInstant, ReadableDuration paramReadableDuration)
  {
    super(paramReadableInstant, paramReadableDuration);
  }
  
  public Interval(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    super(paramReadableInstant1, paramReadableInstant2);
  }
  
  public Interval(ReadableInstant paramReadableInstant, ReadablePeriod paramReadablePeriod)
  {
    super(paramReadableInstant, paramReadablePeriod);
  }
  
  public Interval(ReadablePeriod paramReadablePeriod, ReadableInstant paramReadableInstant)
  {
    super(paramReadablePeriod, paramReadableInstant);
  }
  
  public static Interval parse(String paramString)
  {
    return new Interval(paramString);
  }
  
  public static Interval parseWithOffset(String paramString)
  {
    Period localPeriod = null;
    int i = paramString.indexOf('/');
    if (i < 0) {
      throw new IllegalArgumentException("Format requires a '/' separator: " + paramString);
    }
    Object localObject = paramString.substring(0, i);
    if (((String)localObject).length() <= 0) {
      throw new IllegalArgumentException("Format invalid: " + paramString);
    }
    String str = paramString.substring(i + 1);
    if (str.length() <= 0) {
      throw new IllegalArgumentException("Format invalid: " + paramString);
    }
    DateTimeFormatter localDateTimeFormatter = ISODateTimeFormat.dateTimeParser().withOffsetParsed();
    PeriodFormatter localPeriodFormatter = ISOPeriodFormat.standard();
    i = ((String)localObject).charAt(0);
    if ((i == 80) || (i == 112)) {
      localPeriod = localPeriodFormatter.withParseType(PeriodType.standard()).parsePeriod((String)localObject);
    }
    for (localObject = null;; localObject = localDateTimeFormatter.parseDateTime((String)localObject))
    {
      i = str.charAt(0);
      if ((i != 80) && (i != 112)) {
        break label255;
      }
      if (localPeriod == null) {
        break;
      }
      throw new IllegalArgumentException("Interval composed of two durations: " + paramString);
    }
    paramString = new Interval((ReadableInstant)localObject, localPeriodFormatter.withParseType(PeriodType.standard()).parsePeriod(str));
    for (;;)
    {
      return paramString;
      label255:
      paramString = localDateTimeFormatter.parseDateTime(str);
      if (localPeriod != null) {
        paramString = new Interval(localPeriod, paramString);
      } else {
        paramString = new Interval((ReadableInstant)localObject, paramString);
      }
    }
  }
  
  public boolean abuts(ReadableInterval paramReadableInterval)
  {
    boolean bool = false;
    if (paramReadableInterval == null)
    {
      long l = DateTimeUtils.currentTimeMillis();
      if ((getStartMillis() != l) && (getEndMillis() != l)) {}
    }
    for (bool = true;; bool = true) {
      do
      {
        return bool;
      } while ((paramReadableInterval.getEndMillis() != getStartMillis()) && (getEndMillis() != paramReadableInterval.getStartMillis()));
    }
  }
  
  public Interval gap(ReadableInterval paramReadableInterval)
  {
    paramReadableInterval = DateTimeUtils.getReadableInterval(paramReadableInterval);
    long l3 = paramReadableInterval.getStartMillis();
    long l1 = paramReadableInterval.getEndMillis();
    long l2 = getStartMillis();
    long l4 = getEndMillis();
    if (l2 > l1) {
      paramReadableInterval = new Interval(l1, l2, getChronology());
    }
    for (;;)
    {
      return paramReadableInterval;
      if (l3 > l4) {
        paramReadableInterval = new Interval(l4, l3, getChronology());
      } else {
        paramReadableInterval = null;
      }
    }
  }
  
  public Interval overlap(ReadableInterval paramReadableInterval)
  {
    paramReadableInterval = DateTimeUtils.getReadableInterval(paramReadableInterval);
    if (!overlaps(paramReadableInterval)) {}
    for (paramReadableInterval = null;; paramReadableInterval = new Interval(Math.max(getStartMillis(), paramReadableInterval.getStartMillis()), Math.min(getEndMillis(), paramReadableInterval.getEndMillis()), getChronology())) {
      return paramReadableInterval;
    }
  }
  
  public Interval toInterval()
  {
    return this;
  }
  
  public Interval withChronology(Chronology paramChronology)
  {
    if (getChronology() == paramChronology) {}
    for (paramChronology = this;; paramChronology = new Interval(getStartMillis(), getEndMillis(), paramChronology)) {
      return paramChronology;
    }
  }
  
  public Interval withDurationAfterStart(ReadableDuration paramReadableDuration)
  {
    long l1 = DateTimeUtils.getDurationMillis(paramReadableDuration);
    if (l1 == toDurationMillis()) {}
    long l2;
    for (paramReadableDuration = this;; paramReadableDuration = new Interval(l2, paramReadableDuration.add(l2, l1, 1), paramReadableDuration))
    {
      return paramReadableDuration;
      paramReadableDuration = getChronology();
      l2 = getStartMillis();
    }
  }
  
  public Interval withDurationBeforeEnd(ReadableDuration paramReadableDuration)
  {
    long l1 = DateTimeUtils.getDurationMillis(paramReadableDuration);
    if (l1 == toDurationMillis()) {}
    long l2;
    for (paramReadableDuration = this;; paramReadableDuration = new Interval(paramReadableDuration.add(l2, l1, -1), l2, paramReadableDuration))
    {
      return paramReadableDuration;
      paramReadableDuration = getChronology();
      l2 = getEndMillis();
    }
  }
  
  public Interval withEnd(ReadableInstant paramReadableInstant)
  {
    return withEndMillis(DateTimeUtils.getInstantMillis(paramReadableInstant));
  }
  
  public Interval withEndMillis(long paramLong)
  {
    if (paramLong == getEndMillis()) {}
    for (Interval localInterval = this;; localInterval = new Interval(getStartMillis(), paramLong, getChronology())) {
      return localInterval;
    }
  }
  
  public Interval withPeriodAfterStart(ReadablePeriod paramReadablePeriod)
  {
    if (paramReadablePeriod == null) {}
    Chronology localChronology;
    long l;
    for (paramReadablePeriod = withDurationAfterStart(null);; paramReadablePeriod = new Interval(l, localChronology.add(paramReadablePeriod, l, 1), localChronology))
    {
      return paramReadablePeriod;
      localChronology = getChronology();
      l = getStartMillis();
    }
  }
  
  public Interval withPeriodBeforeEnd(ReadablePeriod paramReadablePeriod)
  {
    if (paramReadablePeriod == null) {}
    Chronology localChronology;
    long l;
    for (paramReadablePeriod = withDurationBeforeEnd(null);; paramReadablePeriod = new Interval(localChronology.add(paramReadablePeriod, l, -1), l, localChronology))
    {
      return paramReadablePeriod;
      localChronology = getChronology();
      l = getEndMillis();
    }
  }
  
  public Interval withStart(ReadableInstant paramReadableInstant)
  {
    return withStartMillis(DateTimeUtils.getInstantMillis(paramReadableInstant));
  }
  
  public Interval withStartMillis(long paramLong)
  {
    if (paramLong == getStartMillis()) {}
    for (Interval localInterval = this;; localInterval = new Interval(paramLong, getEndMillis(), getChronology())) {
      return localInterval;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Interval.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */