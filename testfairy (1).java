package com.testfairy;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.Log;
import com.testfairy.p.b;
import java.util.Timer;

public class i
{
  private String a = null;
  private com.testfairy.k.d b;
  private Bitmap c;
  private com.testfairy.d.d d;
  private String e;
  private boolean f = false;
  private boolean g = true;
  private Timer h;
  
  public i a(Bitmap paramBitmap)
  {
    this.c = paramBitmap;
    return this;
  }
  
  public String a()
  {
    return this.a;
  }
  
  public void a(com.testfairy.d.d paramd)
  {
    this.d = paramd;
  }
  
  public void a(String paramString)
  {
    this.e = paramString;
  }
  
  public void a(String paramString1, com.testfairy.k.e parame, String paramString2)
  {
    this.a = paramString1;
    paramString1 = paramString2;
    if (!paramString2.startsWith("http://"))
    {
      paramString1 = paramString2;
      if (!paramString2.startsWith("https://")) {
        paramString1 = "http://" + paramString2 + "/services/";
      }
    }
    Log.d(e.a, "Using " + paramString1 + " as our endpoint for events");
    this.b = parame.a(paramString1);
    this.h = new Timer("testfairy-recorder");
  }
  
  public void a(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public com.testfairy.k.d b()
  {
    return this.b;
  }
  
  public void b(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  public boolean c()
  {
    return this.g;
  }
  
  public boolean d()
  {
    return this.f;
  }
  
  String e()
  {
    return this.e;
  }
  
  public Timer f()
  {
    return this.h;
  }
  
  public Bitmap g()
  {
    Bitmap localBitmap;
    if (this.d != null)
    {
      this.d.a = b.a(this.d.a(), this.d.b(), this.d.c());
      localBitmap = Bitmap.createBitmap(this.d.a, this.d.b(), this.d.c(), Bitmap.Config.ARGB_8888);
    }
    for (;;)
    {
      return localBitmap;
      if (this.c != null) {
        localBitmap = Bitmap.createBitmap(this.c);
      } else {
        localBitmap = null;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */