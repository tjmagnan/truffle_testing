package android.support.v7.view.menu;

abstract interface MenuHelper
{
  public abstract void dismiss();
  
  public abstract void setPresenterCallback(MenuPresenter.Callback paramCallback);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\view\menu\MenuHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */