package org.joda.time.field;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;

public abstract class PreciseDurationDateTimeField
  extends BaseDateTimeField
{
  private static final long serialVersionUID = 5004523158306266035L;
  private final DurationField iUnitField;
  final long iUnitMillis;
  
  public PreciseDurationDateTimeField(DateTimeFieldType paramDateTimeFieldType, DurationField paramDurationField)
  {
    super(paramDateTimeFieldType);
    if (!paramDurationField.isPrecise()) {
      throw new IllegalArgumentException("Unit duration field must be precise");
    }
    this.iUnitMillis = paramDurationField.getUnitMillis();
    if (this.iUnitMillis < 1L) {
      throw new IllegalArgumentException("The unit milliseconds must be at least 1");
    }
    this.iUnitField = paramDurationField;
  }
  
  public DurationField getDurationField()
  {
    return this.iUnitField;
  }
  
  protected int getMaximumValueForSet(long paramLong, int paramInt)
  {
    return getMaximumValue(paramLong);
  }
  
  public int getMinimumValue()
  {
    return 0;
  }
  
  public final long getUnitMillis()
  {
    return this.iUnitMillis;
  }
  
  public boolean isLenient()
  {
    return false;
  }
  
  public long remainder(long paramLong)
  {
    if (paramLong >= 0L) {}
    for (paramLong %= this.iUnitMillis;; paramLong = (paramLong + 1L) % this.iUnitMillis + this.iUnitMillis - 1L) {
      return paramLong;
    }
  }
  
  public long roundCeiling(long paramLong)
  {
    if (paramLong > 0L) {
      paramLong -= 1L;
    }
    for (paramLong = paramLong - paramLong % this.iUnitMillis + this.iUnitMillis;; paramLong -= paramLong % this.iUnitMillis) {
      return paramLong;
    }
  }
  
  public long roundFloor(long paramLong)
  {
    if (paramLong >= 0L) {}
    for (paramLong -= paramLong % this.iUnitMillis;; paramLong = paramLong - paramLong % this.iUnitMillis - this.iUnitMillis)
    {
      return paramLong;
      paramLong = 1L + paramLong;
    }
  }
  
  public long set(long paramLong, int paramInt)
  {
    FieldUtils.verifyValueBounds(this, paramInt, getMinimumValue(), getMaximumValueForSet(paramLong, paramInt));
    return (paramInt - get(paramLong)) * this.iUnitMillis + paramLong;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\field\PreciseDurationDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */