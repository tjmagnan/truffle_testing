package tech.dcube.companion.model;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import tech.dcube.companion.managers.data.DataManager;

public class PBUser
  extends PBObject
  implements Serializable
{
  private static final long INTERVAL_BEFORE_EXPIRY = 180000L;
  private static final DataManager dataManager = ;
  private PBAddress address = new PBAddress();
  private PBUserAuth auth = new PBUserAuth();
  @SerializedName("profile")
  private PBUserProfile profile = new PBUserProfile();
  private double sendProBalance = 0.0D;
  @SerializedName("id")
  private String userId = "";
  
  public static PBUser fromJson(String paramString)
  {
    for (;;)
    {
      try
      {
        Object localObject = new com/google/gson/JsonParser;
        ((JsonParser)localObject).<init>();
        localObject = ((JsonParser)localObject).parse(paramString).getAsJsonObject().getAsJsonObject("_embedded").get("user");
        localObject = (PBUser)gson.fromJson((JsonElement)localObject, PBUser.class);
        if (localObject == null) {
          continue;
        }
        ((PBUser)localObject).auth = ((PBUserAuth)gson.fromJson(paramString, PBUserAuth.class));
        paramString = (String)localObject;
      }
      catch (Exception paramString)
      {
        Log.e("PBUser", paramString.getLocalizedMessage());
        paramString = new PBUser();
        continue;
      }
      return paramString;
      paramString = new PBUser();
    }
  }
  
  public PBAddress getAddress()
  {
    return this.address;
  }
  
  public PBUserAuth getAuth()
  {
    if (this.auth == null) {
      this.auth = new PBUserAuth();
    }
    return this.auth;
  }
  
  public PBUserProfile getProfile()
  {
    return this.profile;
  }
  
  public double getSendProBalance()
  {
    return this.sendProBalance;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public boolean isSessionExpired()
  {
    boolean bool2 = true;
    StringBuilder localStringBuilder = new StringBuilder().append("");
    if (System.currentTimeMillis() < this.auth.getTokenExpiry() - 180000L)
    {
      bool1 = true;
      Log.wtf("Expired", bool1);
      if (System.currentTimeMillis() >= this.auth.getTokenExpiry() - 180000L) {
        break label76;
      }
    }
    label76:
    for (boolean bool1 = bool2;; bool1 = false)
    {
      return bool1;
      bool1 = false;
      break;
    }
  }
  
  public void setAddress(PBAddress paramPBAddress)
  {
    this.address = paramPBAddress;
  }
  
  public void setSendProBalance(double paramDouble)
  {
    this.sendProBalance = paramDouble;
  }
  
  public static class PBUserAuth
    extends PBObject
    implements Serializable
  {
    private String accessTokenSendPro = "";
    private String accessTokenSmartLink = "";
    private String authTokenSendPro = "";
    private String clientIdSendPro = "";
    private String clientIdSmartLink = "";
    private String idTokenSmartLink = "";
    private String refreshToken = "";
    private String sessionTokenSendPro = "";
    private String sessionTokenSmartLink = "";
    private long tokenExpiry = 0L;
    
    public String getAccessTokenSendPro()
    {
      return this.accessTokenSendPro;
    }
    
    public String getAccessTokenSmartLink()
    {
      return this.accessTokenSmartLink;
    }
    
    public String getAuthTokenSendPro()
    {
      return this.authTokenSendPro;
    }
    
    public String getClientIdSendPro()
    {
      return this.clientIdSendPro;
    }
    
    public String getClientIdSmartLink()
    {
      return this.clientIdSmartLink;
    }
    
    public String getIdTokenSmartLink()
    {
      return this.idTokenSmartLink;
    }
    
    public String getRefreshToken()
    {
      return this.refreshToken;
    }
    
    public String getSessionTokenSendPro()
    {
      return this.sessionTokenSendPro;
    }
    
    public String getSessionTokenSmartLink()
    {
      return this.sessionTokenSmartLink;
    }
    
    public long getTokenExpiry()
    {
      return this.tokenExpiry;
    }
    
    public boolean isSendProLoggedIn()
    {
      if ((!this.accessTokenSendPro.isEmpty()) && (!this.authTokenSendPro.isEmpty())) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public boolean isSmartLinkLoggedIn()
    {
      if ((!this.idTokenSmartLink.isEmpty()) && (!this.accessTokenSmartLink.isEmpty())) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public void setAccessTokenSendPro(String paramString)
    {
      this.accessTokenSendPro = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setAccessTokenSmartLink(String paramString)
    {
      this.accessTokenSmartLink = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setAuthTokenSendPro(String paramString)
    {
      this.authTokenSendPro = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setClientIdSendPro(String paramString)
    {
      this.clientIdSendPro = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setClientIdSmartLink(String paramString)
    {
      this.clientIdSmartLink = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setIdTokenSmartLink(String paramString)
    {
      this.idTokenSmartLink = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setRefreshToken(String paramString)
    {
      this.refreshToken = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setSessionTokenSendPro(String paramString)
    {
      this.sessionTokenSendPro = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setSessionTokenSmartLink(String paramString)
    {
      this.sessionTokenSmartLink = paramString;
      PBUser.dataManager.updateUser();
    }
    
    public void setTokenExpiry(long paramLong)
    {
      this.tokenExpiry = paramLong;
      PBUser.dataManager.updateUser();
    }
  }
  
  public static class PBUserProfile
    extends PBObject
    implements Serializable
  {
    @SerializedName("login")
    private String email = "";
    @SerializedName("firstName")
    private String firstName = "";
    @SerializedName("lastName")
    private String lastName = "";
    @SerializedName("locale")
    private String locale = "";
    private String password = "";
    @SerializedName("timeZone")
    private String timeZone = "";
    
    public String getEmail()
    {
      return this.email;
    }
    
    public String getFirstName()
    {
      return this.firstName;
    }
    
    public String getLastName()
    {
      return this.lastName;
    }
    
    public String getLocale()
    {
      return this.locale;
    }
    
    public String getPassword()
    {
      return this.password;
    }
    
    public String getTimeZone()
    {
      return this.timeZone;
    }
    
    public void setEmail(String paramString)
    {
      this.email = paramString;
    }
    
    public void setFirstName(String paramString)
    {
      this.firstName = paramString;
    }
    
    public void setLastName(String paramString)
    {
      this.lastName = paramString;
    }
    
    public void setLocale(String paramString)
    {
      this.locale = paramString;
    }
    
    public void setPassword(String paramString)
    {
      this.password = paramString;
    }
    
    public void setTimeZone(String paramString)
    {
      this.timeZone = paramString;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBUser.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */