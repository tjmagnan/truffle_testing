package org.jsoup.examples;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public class HtmlToPlainText
{
  private static final int timeout = 5000;
  private static final String userAgent = "Mozilla/5.0 (jsoup)";
  
  public static void main(String... paramVarArgs)
    throws IOException
  {
    boolean bool;
    Object localObject1;
    if ((paramVarArgs.length == 1) || (paramVarArgs.length == 2))
    {
      bool = true;
      Validate.isTrue(bool, "usage: java -cp jsoup.jar org.jsoup.examples.HtmlToPlainText url [selector]");
      localObject1 = paramVarArgs[0];
      if (paramVarArgs.length != 2) {
        break label118;
      }
    }
    Object localObject2;
    label118:
    for (paramVarArgs = paramVarArgs[1];; paramVarArgs = null)
    {
      localObject2 = Jsoup.connect((String)localObject1).userAgent("Mozilla/5.0 (jsoup)").timeout(5000).get();
      localObject1 = new HtmlToPlainText();
      if (paramVarArgs == null) {
        break label123;
      }
      paramVarArgs = ((Document)localObject2).select(paramVarArgs).iterator();
      while (paramVarArgs.hasNext())
      {
        localObject2 = ((HtmlToPlainText)localObject1).getPlainText((Element)paramVarArgs.next());
        System.out.println((String)localObject2);
      }
      bool = false;
      break;
    }
    label123:
    paramVarArgs = ((HtmlToPlainText)localObject1).getPlainText((Element)localObject2);
    System.out.println(paramVarArgs);
  }
  
  public String getPlainText(Element paramElement)
  {
    FormattingVisitor localFormattingVisitor = new FormattingVisitor(null);
    new NodeTraversor(localFormattingVisitor).traverse(paramElement);
    return localFormattingVisitor.toString();
  }
  
  private class FormattingVisitor
    implements NodeVisitor
  {
    private static final int maxWidth = 80;
    private StringBuilder accum = new StringBuilder();
    private int width = 0;
    
    private FormattingVisitor() {}
    
    private void append(String paramString)
    {
      if (paramString.startsWith("\n")) {
        this.width = 0;
      }
      if (paramString.equals(" ")) {
        if (this.accum.length() != 0) {
          if (!StringUtil.in(this.accum.substring(this.accum.length() - 1), new String[] { " ", "\n" })) {
            break label70;
          }
        }
      }
      for (;;)
      {
        return;
        label70:
        if (paramString.length() + this.width > 80)
        {
          String[] arrayOfString = paramString.split("\\s+");
          int i = 0;
          label94:
          int j;
          if (i < arrayOfString.length)
          {
            String str = arrayOfString[i];
            if (i != arrayOfString.length - 1) {
              break label188;
            }
            j = 1;
            label118:
            paramString = str;
            if (j == 0) {
              paramString = str + " ";
            }
            if (paramString.length() + this.width <= 80) {
              break label193;
            }
            this.accum.append("\n").append(paramString);
          }
          for (this.width = paramString.length();; this.width += paramString.length())
          {
            i++;
            break label94;
            break;
            label188:
            j = 0;
            break label118;
            label193:
            this.accum.append(paramString);
          }
        }
        this.accum.append(paramString);
        this.width += paramString.length();
      }
    }
    
    public void head(Node paramNode, int paramInt)
    {
      String str = paramNode.nodeName();
      if ((paramNode instanceof TextNode)) {
        append(((TextNode)paramNode).text());
      }
      for (;;)
      {
        return;
        if (str.equals("li")) {
          append("\n * ");
        } else if (str.equals("dt")) {
          append("  ");
        } else if (StringUtil.in(str, new String[] { "p", "h1", "h2", "h3", "h4", "h5", "tr" })) {
          append("\n");
        }
      }
    }
    
    public void tail(Node paramNode, int paramInt)
    {
      String str = paramNode.nodeName();
      if (StringUtil.in(str, new String[] { "br", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5" })) {
        append("\n");
      }
      for (;;)
      {
        return;
        if (str.equals("a")) {
          append(String.format(" <%s>", new Object[] { paramNode.absUrl("href") }));
        }
      }
    }
    
    public String toString()
    {
      return this.accum.toString();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\examples\HtmlToPlainText.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */