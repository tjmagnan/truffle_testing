package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import com.android.volley.VolleyError;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBSendProItem;

public class CatalogAddItemFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  double _height = 0.0D;
  double _length = 0.0D;
  String _productDesc = "";
  String _productName = "";
  String _productUPC = "";
  double _weight = 0.0D;
  double _width = 0.0D;
  private OnAddCatalogItemListener mListener;
  private String mParam2;
  private int mPosition;
  PBSendProItem pbSendProItem;
  EditText productDesc;
  EditText productHeight;
  EditText productLenght;
  EditText productName;
  EditText productUPC;
  EditText productWeight;
  EditText productWidth;
  LinearLayout scanBarCodeButton;
  
  public static CatalogAddItemFragment newInstance(int paramInt, String paramString)
  {
    CatalogAddItemFragment localCatalogAddItemFragment = new CatalogAddItemFragment();
    Bundle localBundle = new Bundle();
    localBundle.putInt("param1", paramInt);
    localBundle.putString("param2", paramString);
    localCatalogAddItemFragment.setArguments(localBundle);
    return localCatalogAddItemFragment;
  }
  
  boolean checkEditInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput()) && (checkLengthInput()) && (checkWidthInput()) && (checkHeightInput()) && (checkWeightInput()) && (checkUPCInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkEditNameInputs()
  {
    if ((checkProductNameInput()) && (checkProductDescInput())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean checkHeightInput()
  {
    bool = false;
    try
    {
      this._height = Double.parseDouble(this.productHeight.getText().toString());
      if (this._height > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._height = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkLengthInput()
  {
    bool = false;
    try
    {
      this._length = Double.parseDouble(this.productLenght.getText().toString());
      if (this._length > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._length = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkProductDescInput()
  {
    this._productDesc = this.productDesc.getText().toString();
    return true;
  }
  
  boolean checkProductNameInput()
  {
    if (!this.productName.getText().toString().equals(""))
    {
      this._productName = this.productName.getText().toString();
      if (!this._productName.equals("")) {
        break label57;
      }
    }
    label57:
    for (boolean bool = false;; bool = true)
    {
      return bool;
      this._productName = "";
      break;
    }
  }
  
  boolean checkUPCInput()
  {
    if (this.productUPC.getText().toString() != "") {
      this._productUPC = this.productUPC.getText().toString();
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      this._productUPC = "";
    }
  }
  
  boolean checkWeightInput()
  {
    bool = false;
    try
    {
      this._weight = Double.parseDouble(this.productWeight.getText().toString());
      if (this._weight > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._weight = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  boolean checkWidthInput()
  {
    bool = false;
    try
    {
      this._width = Double.parseDouble(this.productWidth.getText().toString());
      if (this._width > 0.0D) {
        break label43;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        this._width = 0.0D;
        continue;
        bool = true;
      }
    }
    return bool;
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  void hideKeyboard()
  {
    UtilManager.hideSoftKeyboard(getActivity());
  }
  
  @OnClick({2131624097})
  public void hideSoftKeyboardOnClick()
  {
    hideKeyboard();
  }
  
  @OnFocusChange({2131624097})
  public void hideSoftKeyboardOnFocus(boolean paramBoolean)
  {
    if (paramBoolean) {}
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnAddCatalogItemListener))
    {
      this.mListener = ((OnAddCatalogItemListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnAddCatalogItemListener");
  }
  
  public void onCancelButtonPressed()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onAddCatalogInteraction("action_cancel_button");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    onCancelButtonPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    if (!checkEditNameInputs()) {
      displayToast("Product Name can't be Empty");
    }
    for (;;)
    {
      return;
      if (!checkEditInputs())
      {
        displayToast("Dimensions and Weight can't be Zero");
      }
      else
      {
        this.pbSendProItem = new PBSendProItem(this._productName, "img.jpg", Double.valueOf(this._length), Double.valueOf(this._width), Double.valueOf(this._height), Double.valueOf(this._weight), Integer.valueOf(0), Integer.valueOf(0), this._productUPC);
        try
        {
          ServerManager localServerManager = ServerManager.getInstance();
          Context localContext = getContext();
          PBSendProItem localPBSendProItem = this.pbSendProItem;
          ServerManager.RequestCompletionHandler local2 = new tech/dcube/companion/fragments/CatalogAddItemFragment$2;
          local2.<init>(this);
          localServerManager.saveItem(localContext, localPBSendProItem, local2);
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
        }
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mPosition = getArguments().getInt("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968625, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Add Item");
    setTopBarLeftButtonText("Cancel");
    setTopBarRightButtonText("Save");
    this.scanBarCodeButton = ((LinearLayout)paramLayoutInflater.findViewById(2131624098));
    this.productName = ((EditText)paramLayoutInflater.findViewById(2131624099));
    this.productDesc = ((EditText)paramLayoutInflater.findViewById(2131624100));
    this.productLenght = ((EditText)paramLayoutInflater.findViewById(2131624101));
    this.productWidth = ((EditText)paramLayoutInflater.findViewById(2131624102));
    this.productHeight = ((EditText)paramLayoutInflater.findViewById(2131624103));
    this.productWeight = ((EditText)paramLayoutInflater.findViewById(2131624104));
    this.productUPC = ((EditText)paramLayoutInflater.findViewById(2131624105));
    this.pbSendProItem = new PBSendProItem();
    this.scanBarCodeButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CatalogAddItemFragment.this.onScanBarcodeButtonPressed();
      }
    });
    hideKeyboard();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPause()
  {
    super.onPause();
    hideKeyboard();
  }
  
  public void onResume()
  {
    super.onResume();
    hideKeyboard();
  }
  
  public void onSaveButtonPressed()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onAddCatalogInteraction("action_save_button");
    }
  }
  
  public void onScanBarcodeButtonPressed()
  {
    hideKeyboard();
    if (this.mListener != null) {
      this.mListener.onAddCatalogInteraction("action_scan_barcode_button");
    }
  }
  
  public void updateBarcodeValue(String paramString)
  {
    this.productUPC.setText(paramString);
  }
  
  public void updateBarcodeValue(PBSendProItem paramPBSendProItem, String paramString)
  {
    this.productName.setText(paramPBSendProItem.getItemName());
    this.productLenght.setText(String.format("%.01f", new Object[] { Double.valueOf(paramPBSendProItem.getItemLengthIn()) }));
    this.productWidth.setText(String.format("%.01f", new Object[] { Double.valueOf(paramPBSendProItem.getItemWidthIn()) }));
    this.productHeight.setText(String.format("%.01f", new Object[] { Double.valueOf(paramPBSendProItem.getItemHeightIn()) }));
    this.productWeight.setText(String.format("%.01f", new Object[] { Double.valueOf(paramPBSendProItem.getItemWeightLb()) }));
    this.productUPC.setText(paramString);
  }
  
  public static abstract interface OnAddCatalogItemListener
  {
    public abstract void onAddCatalogInteraction(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\CatalogAddItemFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */