package com.testfairy.p;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Process;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class k
{
  private static Map a = new HashMap(64);
  
  public static int a(int paramInt)
  {
    for (;;)
    {
      try
      {
        if (a.containsKey(Integer.valueOf(paramInt)))
        {
          i = ((Integer)a.get(Integer.valueOf(paramInt))).intValue();
          paramInt = i;
          return paramInt;
        }
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        String str = "/proc/" + paramInt + "/status";
        localObject1 = new java/io/BufferedReader;
        localObject2 = new java/io/FileReader;
        ((FileReader)localObject2).<init>(str);
        ((BufferedReader)localObject1).<init>((Reader)localObject2);
        localObject2 = ((BufferedReader)localObject1).readLine();
        if (localObject2 != null) {
          continue;
        }
        ((BufferedReader)localObject1).close();
      }
      catch (Exception localException)
      {
        int i;
        Object localObject1;
        Object localObject2;
        continue;
      }
      a.put(Integer.valueOf(paramInt), Integer.valueOf(-1));
      paramInt = -1;
      continue;
      if (((String)localObject2).startsWith("Uid:"))
      {
        i = Integer.valueOf(localObject2.split("\\s")[1]).intValue();
        a.put(Integer.valueOf(paramInt), Integer.valueOf(i));
        ((BufferedReader)localObject1).close();
        paramInt = i;
      }
    }
  }
  
  public static int a(String paramString)
  {
    try
    {
      i = a(Integer.valueOf(paramString).intValue());
      return i;
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        int i = -1;
      }
    }
  }
  
  public static List a()
  {
    String[] arrayOfString = new File("/proc/").list();
    LinkedList localLinkedList = new LinkedList();
    for (int i = 0; i < arrayOfString.length; i++) {
      if (c(arrayOfString[i])) {
        localLinkedList.add(arrayOfString[i]);
      }
    }
    return localLinkedList;
  }
  
  public static boolean a(int paramInt1, int paramInt2)
  {
    bool = false;
    try
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      str = "/proc/" + paramInt1 + "/status";
      localObject = new java/io/BufferedReader;
      FileReader localFileReader = new java/io/FileReader;
      localFileReader.<init>(str);
      ((BufferedReader)localObject).<init>(localFileReader);
      j = 0;
      paramInt1 = 0;
      i = 0;
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        Object localObject;
        String str;
        int j;
        int i;
        continue;
        int k = 1;
        int m = paramInt1;
        if (str.startsWith("PPid:"))
        {
          if (Integer.valueOf(str.split("\\s")[1]).intValue() != 1) {
            localThrowable.close();
          } else {
            m = 1;
          }
        }
        else
        {
          int n = i;
          if (str.startsWith("Name:"))
          {
            if (!str.split("\\s")[1].equals("logcat")) {
              localThrowable.close();
            } else {
              n = 1;
            }
          }
          else
          {
            i = n;
            paramInt1 = m;
            j = k;
            if (n != 0)
            {
              i = n;
              paramInt1 = m;
              j = k;
              if (k != 0)
              {
                i = n;
                paramInt1 = m;
                j = k;
                if (m != 0)
                {
                  localThrowable.close();
                  bool = true;
                }
              }
            }
          }
        }
      }
    }
    str = ((BufferedReader)localObject).readLine();
    if (str == null) {
      ((BufferedReader)localObject).close();
    }
    for (;;)
    {
      return bool;
      k = j;
      if (!str.startsWith("Uid:")) {
        break label132;
      }
      if (Integer.valueOf(str.split("\\s")[1]).intValue() == paramInt2) {
        break;
      }
      ((BufferedReader)localObject).close();
    }
  }
  
  public static boolean a(Context paramContext)
  {
    paramContext = ((ActivityManager)paramContext.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
    int i = Process.myPid();
    paramContext = paramContext.iterator();
    do
    {
      if (!paramContext.hasNext()) {
        break;
      }
    } while (((ActivityManager.RunningServiceInfo)paramContext.next()).pid != i);
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static String b(int paramInt)
  {
    String str1 = "";
    Iterator localIterator = a().iterator();
    if (localIterator.hasNext())
    {
      String str2 = (String)localIterator.next();
      if ((a(str2) != paramInt) || (!b(str2).equals("logcat"))) {
        break label80;
      }
      str1 = str1 + "," + str2;
    }
    label80:
    for (;;)
    {
      break;
      return str1;
    }
  }
  
  public static String b(Context paramContext)
  {
    paramContext = ((ActivityManager)paramContext.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
    int i = Process.myPid();
    paramContext = paramContext.iterator();
    ActivityManager.RunningServiceInfo localRunningServiceInfo;
    do
    {
      if (!paramContext.hasNext()) {
        break;
      }
      localRunningServiceInfo = (ActivityManager.RunningServiceInfo)paramContext.next();
    } while (localRunningServiceInfo.pid != i);
    for (paramContext = localRunningServiceInfo.service.getShortClassName();; paramContext = null) {
      return paramContext;
    }
  }
  
  private static String b(String paramString)
  {
    for (;;)
    {
      try
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        paramString = "/proc/" + paramString + "/status";
        localObject = new java/io/BufferedReader;
        FileReader localFileReader = new java/io/FileReader;
        localFileReader.<init>(paramString);
        ((BufferedReader)localObject).<init>(localFileReader);
        paramString = ((BufferedReader)localObject).readLine();
        if (paramString != null) {
          continue;
        }
        ((BufferedReader)localObject).close();
        paramString = "";
      }
      catch (Exception paramString)
      {
        Object localObject;
        paramString = "";
        continue;
      }
      return paramString;
      if (paramString.startsWith("Name:"))
      {
        paramString = paramString.split("\\s")[1];
        ((BufferedReader)localObject).close();
      }
    }
  }
  
  private static boolean c(String paramString)
  {
    try
    {
      Integer.parseInt(paramString);
      bool = true;
    }
    catch (NumberFormatException paramString)
    {
      for (;;)
      {
        boolean bool = false;
      }
    }
    return bool;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */