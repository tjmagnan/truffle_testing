package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadablePartial;

class ReadablePartialConverter
  extends AbstractConverter
  implements PartialConverter
{
  static final ReadablePartialConverter INSTANCE = new ReadablePartialConverter();
  
  public Chronology getChronology(Object paramObject, Chronology paramChronology)
  {
    Chronology localChronology = paramChronology;
    if (paramChronology == null) {
      localChronology = DateTimeUtils.getChronology(((ReadablePartial)paramObject).getChronology());
    }
    return localChronology;
  }
  
  public Chronology getChronology(Object paramObject, DateTimeZone paramDateTimeZone)
  {
    return getChronology(paramObject, (Chronology)null).withZone(paramDateTimeZone);
  }
  
  public int[] getPartialValues(ReadablePartial paramReadablePartial, Object paramObject, Chronology paramChronology)
  {
    paramObject = (ReadablePartial)paramObject;
    int j = paramReadablePartial.size();
    int[] arrayOfInt = new int[j];
    for (int i = 0; i < j; i++) {
      arrayOfInt[i] = ((ReadablePartial)paramObject).get(paramReadablePartial.getFieldType(i));
    }
    paramChronology.validate(paramReadablePartial, arrayOfInt);
    return arrayOfInt;
  }
  
  public Class<?> getSupportedType()
  {
    return ReadablePartial.class;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\ReadablePartialConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */