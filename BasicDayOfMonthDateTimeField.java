package org.joda.time.chrono;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.PreciseDurationDateTimeField;

final class BasicDayOfMonthDateTimeField
  extends PreciseDurationDateTimeField
{
  private static final long serialVersionUID = -4677223814028011723L;
  private final BasicChronology iChronology;
  
  BasicDayOfMonthDateTimeField(BasicChronology paramBasicChronology, DurationField paramDurationField)
  {
    super(DateTimeFieldType.dayOfMonth(), paramDurationField);
    this.iChronology = paramBasicChronology;
  }
  
  private Object readResolve()
  {
    return this.iChronology.dayOfMonth();
  }
  
  public int get(long paramLong)
  {
    return this.iChronology.getDayOfMonth(paramLong);
  }
  
  public int getMaximumValue()
  {
    return this.iChronology.getDaysInMonthMax();
  }
  
  public int getMaximumValue(long paramLong)
  {
    return this.iChronology.getDaysInMonthMax(paramLong);
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial)
  {
    int i;
    if (paramReadablePartial.isSupported(DateTimeFieldType.monthOfYear()))
    {
      i = paramReadablePartial.get(DateTimeFieldType.monthOfYear());
      if (paramReadablePartial.isSupported(DateTimeFieldType.year()))
      {
        int j = paramReadablePartial.get(DateTimeFieldType.year());
        i = this.iChronology.getDaysInYearMonth(j, i);
      }
    }
    for (;;)
    {
      return i;
      i = this.iChronology.getDaysInMonthMax(i);
      continue;
      i = getMaximumValue();
    }
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt)
  {
    int j = 0;
    int k = paramReadablePartial.size();
    int i = 0;
    int m;
    if (i < k) {
      if (paramReadablePartial.getFieldType(i) == DateTimeFieldType.monthOfYear())
      {
        m = paramArrayOfInt[i];
        i = j;
        label40:
        if (i < k) {
          if (paramReadablePartial.getFieldType(i) == DateTimeFieldType.year())
          {
            i = paramArrayOfInt[i];
            i = this.iChronology.getDaysInYearMonth(i, m);
          }
        }
      }
    }
    for (;;)
    {
      return i;
      i++;
      break label40;
      i = this.iChronology.getDaysInMonthMax(m);
      continue;
      i++;
      break;
      i = getMaximumValue();
    }
  }
  
  protected int getMaximumValueForSet(long paramLong, int paramInt)
  {
    return this.iChronology.getDaysInMonthMaxForSet(paramLong, paramInt);
  }
  
  public int getMinimumValue()
  {
    return 1;
  }
  
  public DurationField getRangeDurationField()
  {
    return this.iChronology.months();
  }
  
  public boolean isLeap(long paramLong)
  {
    return this.iChronology.isLeapDay(paramLong);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BasicDayOfMonthDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */