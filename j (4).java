package com.testfairy.j;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.n;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class d
  extends c
{
  protected static final String a = "testfairy-logcat-reader";
  private Pattern b = null;
  private final int j;
  private final String k;
  private InputStreamReader l;
  
  public d(b paramb)
  {
    super(paramb);
    setName("testfairy-logcat-reader");
    this.j = Calendar.getInstance().get(1);
    this.k = String.valueOf(this.i);
  }
  
  private long a(String paramString)
  {
    long l2 = 0L;
    try
    {
      SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
      localSimpleDateFormat.<init>("yyyy-MM-dd HH:mm:ss.SSSSSS");
      l1 = localSimpleDateFormat.parse(paramString).getTime();
      return l1;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        long l1 = l2;
      }
    }
  }
  
  c.a a(String paramString1, String paramString2)
  {
    Object localObject = null;
    c.a locala = new c.a();
    if (this.b == null) {
      this.b = Pattern.compile("^([0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}) (\\w)/([\\s\\w\\.\\-_\\(\\)\\[\\]\\{\\}\\#\\$\\%\\^\\&\\*\\@\\<\\>\\\\\\/\\:]+)\\s*\\(\\s*(\\d+)\\s*\\):\\s?(.*)$");
    }
    Matcher localMatcher = this.b.matcher(paramString1);
    if (!localMatcher.matches()) {
      paramString1 = (String)localObject;
    }
    for (;;)
    {
      return paramString1;
      locala.e = localMatcher.group(4);
      paramString1 = (String)localObject;
      if (locala.e.equals(paramString2))
      {
        locala.a = a(this.j + "-" + localMatcher.group(1));
        locala.b = localMatcher.group(2).trim();
        locala.c = localMatcher.group(3).trim();
        locala.d = localMatcher.group(5);
        paramString1 = locala;
      }
    }
  }
  
  public void a()
  {
    super.a();
  }
  
  public void run()
  {
    try
    {
      Process localProcess = Runtime.getRuntime().exec(n.r);
      Object localObject1 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject1).<init>(localProcess.getInputStream());
      this.l = ((InputStreamReader)localObject1);
      localObject1 = new java/io/BufferedReader;
      ((BufferedReader)localObject1).<init>(this.l);
      if (!this.g)
      {
        localObject2 = ((BufferedReader)localObject1).readLine();
        if (localObject2 != null) {}
      }
      else
      {
        if (!this.e) {
          this.d.a();
        }
        localProcess.destroy();
        if (this.l == null) {}
      }
    }
    catch (Throwable localThrowable1)
    {
      try
      {
        for (;;)
        {
          Object localObject2;
          this.l.close();
          this.l = null;
          return;
          if (!this.f)
          {
            localObject2 = a((String)localObject2, this.k);
            if (localObject2 != null) {
              this.d.a(((c.a)localObject2).a, ((c.a)localObject2).b, ((c.a)localObject2).c, ((c.a)localObject2).d);
            }
          }
          else
          {
            Thread.sleep(1L);
            this.e = true;
          }
        }
        localThrowable1 = localThrowable1;
        Log.e(e.a, "Throwable", localThrowable1);
      }
      catch (Throwable localThrowable2)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\j\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */