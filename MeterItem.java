package tech.dcube.companion.model;

public class MeterItem
{
  public String mMeterAmount;
  public String mMeterName;
  public boolean mShowAlert;
  public boolean mStatus;
  
  public MeterItem(boolean paramBoolean1, boolean paramBoolean2, String paramString1, String paramString2)
  {
    this.mStatus = paramBoolean1;
    this.mShowAlert = paramBoolean2;
    this.mMeterName = paramString1;
    this.mMeterAmount = paramString2;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\MeterItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */