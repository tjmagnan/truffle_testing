package tech.dcube.companion.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.List;
import tech.dcube.companion.listeners.SendProInteractionListener;
import tech.dcube.companion.model.SendProItem;

public class HomeSendProAdapter
  extends RecyclerView.Adapter<HomeSendProViewHolder>
{
  private SendProInteractionListener listener;
  List<SendProItem> mSendProItems;
  
  public HomeSendProAdapter(List<SendProItem> paramList, SendProInteractionListener paramSendProInteractionListener)
  {
    this.mSendProItems = paramList;
    this.listener = paramSendProInteractionListener;
  }
  
  public int getItemCount()
  {
    return this.mSendProItems.size();
  }
  
  public void onBindViewHolder(HomeSendProViewHolder paramHomeSendProViewHolder, int paramInt)
  {
    String str2 = ((SendProItem)this.mSendProItems.get(paramInt)).getSendproName();
    double d = ((SendProItem)this.mSendProItems.get(paramInt)).getSendproAmount();
    new DecimalFormat("0.##").format(d);
    String str1 = String.format("%.02f", new Object[] { Double.valueOf(d) });
    paramHomeSendProViewHolder.mMeterNameTV.setText(str2);
    paramHomeSendProViewHolder.mMeterAmountTV.setText(str1);
    paramHomeSendProViewHolder.mAddFundsIB.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (HomeSendProAdapter.this.listener != null) {
          HomeSendProAdapter.this.listener.onSendProAddFunButtonTapped("send_pro_add_funds");
        }
      }
    });
  }
  
  public HomeSendProViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new HomeSendProViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968708, paramViewGroup, false));
  }
  
  public void refreshView()
  {
    notifyDataSetChanged();
  }
  
  public class HomeSendProViewHolder
    extends RecyclerView.ViewHolder
  {
    public ImageButton mAddFundsIB;
    public TextView mMeterAmountTV;
    public TextView mMeterNameTV;
    public View mView;
    
    public HomeSendProViewHolder(View paramView)
    {
      super();
      this.mView = paramView;
      this.mMeterNameTV = ((TextView)this.mView.findViewById(2131624306));
      this.mMeterAmountTV = ((TextView)this.mView.findViewById(2131624307));
      this.mAddFundsIB = ((ImageButton)this.mView.findViewById(2131624309));
      this.mAddFundsIB.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView) {}
      });
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\adapters\HomeSendProAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */