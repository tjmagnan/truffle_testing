package com.testfairy.p;

import android.os.Bundle;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class f
{
  public static JSONArray a(List paramList)
  {
    JSONArray localJSONArray = new JSONArray();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localJSONArray.put(paramList.next());
    }
    return localJSONArray;
  }
  
  public static JSONArray a(Set paramSet)
  {
    if (paramSet == null)
    {
      paramSet = null;
      return paramSet;
    }
    JSONArray localJSONArray = new JSONArray();
    Iterator localIterator = paramSet.iterator();
    for (;;)
    {
      paramSet = localJSONArray;
      if (!localIterator.hasNext()) {
        break;
      }
      localJSONArray.put(localIterator.next());
    }
  }
  
  public static JSONObject a(Bundle paramBundle)
  {
    if (paramBundle == null) {}
    JSONObject localJSONObject;
    for (paramBundle = null;; paramBundle = localJSONObject)
    {
      return paramBundle;
      localJSONObject = new JSONObject();
      Iterator localIterator = paramBundle.keySet().iterator();
      if (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        Object localObject2 = paramBundle.get(str);
        Object localObject1;
        if ((localObject2 instanceof List)) {
          localObject1 = a((List)localObject2);
        }
        for (;;)
        {
          localJSONObject.put(str, localObject1);
          break;
          if ((localObject2 instanceof Map))
          {
            localObject1 = a((Map)localObject2);
          }
          else
          {
            localObject1 = localObject2;
            if ((localObject2 instanceof Bundle)) {
              localObject1 = a((Bundle)localObject2);
            }
          }
        }
      }
    }
  }
  
  public static JSONObject a(Map paramMap)
  {
    JSONObject localJSONObject = new JSONObject();
    Iterator localIterator = paramMap.keySet().iterator();
    if (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Object localObject2 = paramMap.get(str);
      Object localObject1;
      if ((localObject2 instanceof List)) {
        localObject1 = a((List)localObject2);
      }
      for (;;)
      {
        localJSONObject.put(str, localObject1);
        break;
        if ((localObject2 instanceof Map))
        {
          localObject1 = a((Map)localObject2);
        }
        else
        {
          localObject1 = localObject2;
          if ((localObject2 instanceof Bundle)) {
            localObject1 = a((Bundle)localObject2);
          }
        }
      }
    }
    return localJSONObject;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */