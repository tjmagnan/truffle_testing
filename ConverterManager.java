package org.joda.time.convert;

import org.joda.time.JodaTimePermission;

public final class ConverterManager
{
  private static ConverterManager INSTANCE;
  private ConverterSet iDurationConverters = new ConverterSet(new Converter[] { ReadableDurationConverter.INSTANCE, ReadableIntervalConverter.INSTANCE, StringConverter.INSTANCE, LongConverter.INSTANCE, NullConverter.INSTANCE });
  private ConverterSet iInstantConverters = new ConverterSet(new Converter[] { ReadableInstantConverter.INSTANCE, StringConverter.INSTANCE, CalendarConverter.INSTANCE, DateConverter.INSTANCE, LongConverter.INSTANCE, NullConverter.INSTANCE });
  private ConverterSet iIntervalConverters = new ConverterSet(new Converter[] { ReadableIntervalConverter.INSTANCE, StringConverter.INSTANCE, NullConverter.INSTANCE });
  private ConverterSet iPartialConverters = new ConverterSet(new Converter[] { ReadablePartialConverter.INSTANCE, ReadableInstantConverter.INSTANCE, StringConverter.INSTANCE, CalendarConverter.INSTANCE, DateConverter.INSTANCE, LongConverter.INSTANCE, NullConverter.INSTANCE });
  private ConverterSet iPeriodConverters = new ConverterSet(new Converter[] { ReadableDurationConverter.INSTANCE, ReadablePeriodConverter.INSTANCE, ReadableIntervalConverter.INSTANCE, StringConverter.INSTANCE, NullConverter.INSTANCE });
  
  private void checkAlterDurationConverters()
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("ConverterManager.alterDurationConverters"));
    }
  }
  
  private void checkAlterInstantConverters()
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("ConverterManager.alterInstantConverters"));
    }
  }
  
  private void checkAlterIntervalConverters()
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("ConverterManager.alterIntervalConverters"));
    }
  }
  
  private void checkAlterPartialConverters()
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("ConverterManager.alterPartialConverters"));
    }
  }
  
  private void checkAlterPeriodConverters()
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("ConverterManager.alterPeriodConverters"));
    }
  }
  
  public static ConverterManager getInstance()
  {
    if (INSTANCE == null) {
      INSTANCE = new ConverterManager();
    }
    return INSTANCE;
  }
  
  public DurationConverter addDurationConverter(DurationConverter paramDurationConverter)
    throws SecurityException
  {
    checkAlterDurationConverters();
    if (paramDurationConverter == null) {}
    DurationConverter[] arrayOfDurationConverter;
    for (paramDurationConverter = null;; paramDurationConverter = arrayOfDurationConverter[0])
    {
      return paramDurationConverter;
      arrayOfDurationConverter = new DurationConverter[1];
      this.iDurationConverters = this.iDurationConverters.add(paramDurationConverter, arrayOfDurationConverter);
    }
  }
  
  public InstantConverter addInstantConverter(InstantConverter paramInstantConverter)
    throws SecurityException
  {
    checkAlterInstantConverters();
    if (paramInstantConverter == null) {}
    InstantConverter[] arrayOfInstantConverter;
    for (paramInstantConverter = null;; paramInstantConverter = arrayOfInstantConverter[0])
    {
      return paramInstantConverter;
      arrayOfInstantConverter = new InstantConverter[1];
      this.iInstantConverters = this.iInstantConverters.add(paramInstantConverter, arrayOfInstantConverter);
    }
  }
  
  public IntervalConverter addIntervalConverter(IntervalConverter paramIntervalConverter)
    throws SecurityException
  {
    checkAlterIntervalConverters();
    if (paramIntervalConverter == null) {}
    IntervalConverter[] arrayOfIntervalConverter;
    for (paramIntervalConverter = null;; paramIntervalConverter = arrayOfIntervalConverter[0])
    {
      return paramIntervalConverter;
      arrayOfIntervalConverter = new IntervalConverter[1];
      this.iIntervalConverters = this.iIntervalConverters.add(paramIntervalConverter, arrayOfIntervalConverter);
    }
  }
  
  public PartialConverter addPartialConverter(PartialConverter paramPartialConverter)
    throws SecurityException
  {
    checkAlterPartialConverters();
    if (paramPartialConverter == null) {}
    PartialConverter[] arrayOfPartialConverter;
    for (paramPartialConverter = null;; paramPartialConverter = arrayOfPartialConverter[0])
    {
      return paramPartialConverter;
      arrayOfPartialConverter = new PartialConverter[1];
      this.iPartialConverters = this.iPartialConverters.add(paramPartialConverter, arrayOfPartialConverter);
    }
  }
  
  public PeriodConverter addPeriodConverter(PeriodConverter paramPeriodConverter)
    throws SecurityException
  {
    checkAlterPeriodConverters();
    if (paramPeriodConverter == null) {}
    PeriodConverter[] arrayOfPeriodConverter;
    for (paramPeriodConverter = null;; paramPeriodConverter = arrayOfPeriodConverter[0])
    {
      return paramPeriodConverter;
      arrayOfPeriodConverter = new PeriodConverter[1];
      this.iPeriodConverters = this.iPeriodConverters.add(paramPeriodConverter, arrayOfPeriodConverter);
    }
  }
  
  public DurationConverter getDurationConverter(Object paramObject)
  {
    ConverterSet localConverterSet = this.iDurationConverters;
    if (paramObject == null) {}
    for (Object localObject = null;; localObject = paramObject.getClass())
    {
      localObject = (DurationConverter)localConverterSet.select((Class)localObject);
      if (localObject == null) {
        break;
      }
      return (DurationConverter)localObject;
    }
    localObject = new StringBuilder().append("No duration converter found for type: ");
    if (paramObject == null) {}
    for (paramObject = "null";; paramObject = paramObject.getClass().getName()) {
      throw new IllegalArgumentException((String)paramObject);
    }
  }
  
  public DurationConverter[] getDurationConverters()
  {
    ConverterSet localConverterSet = this.iDurationConverters;
    DurationConverter[] arrayOfDurationConverter = new DurationConverter[localConverterSet.size()];
    localConverterSet.copyInto(arrayOfDurationConverter);
    return arrayOfDurationConverter;
  }
  
  public InstantConverter getInstantConverter(Object paramObject)
  {
    ConverterSet localConverterSet = this.iInstantConverters;
    if (paramObject == null) {}
    for (Object localObject = null;; localObject = paramObject.getClass())
    {
      localObject = (InstantConverter)localConverterSet.select((Class)localObject);
      if (localObject == null) {
        break;
      }
      return (InstantConverter)localObject;
    }
    localObject = new StringBuilder().append("No instant converter found for type: ");
    if (paramObject == null) {}
    for (paramObject = "null";; paramObject = paramObject.getClass().getName()) {
      throw new IllegalArgumentException((String)paramObject);
    }
  }
  
  public InstantConverter[] getInstantConverters()
  {
    ConverterSet localConverterSet = this.iInstantConverters;
    InstantConverter[] arrayOfInstantConverter = new InstantConverter[localConverterSet.size()];
    localConverterSet.copyInto(arrayOfInstantConverter);
    return arrayOfInstantConverter;
  }
  
  public IntervalConverter getIntervalConverter(Object paramObject)
  {
    ConverterSet localConverterSet = this.iIntervalConverters;
    if (paramObject == null) {}
    for (Object localObject = null;; localObject = paramObject.getClass())
    {
      localObject = (IntervalConverter)localConverterSet.select((Class)localObject);
      if (localObject == null) {
        break;
      }
      return (IntervalConverter)localObject;
    }
    localObject = new StringBuilder().append("No interval converter found for type: ");
    if (paramObject == null) {}
    for (paramObject = "null";; paramObject = paramObject.getClass().getName()) {
      throw new IllegalArgumentException((String)paramObject);
    }
  }
  
  public IntervalConverter[] getIntervalConverters()
  {
    ConverterSet localConverterSet = this.iIntervalConverters;
    IntervalConverter[] arrayOfIntervalConverter = new IntervalConverter[localConverterSet.size()];
    localConverterSet.copyInto(arrayOfIntervalConverter);
    return arrayOfIntervalConverter;
  }
  
  public PartialConverter getPartialConverter(Object paramObject)
  {
    ConverterSet localConverterSet = this.iPartialConverters;
    if (paramObject == null) {}
    for (Object localObject = null;; localObject = paramObject.getClass())
    {
      localObject = (PartialConverter)localConverterSet.select((Class)localObject);
      if (localObject == null) {
        break;
      }
      return (PartialConverter)localObject;
    }
    localObject = new StringBuilder().append("No partial converter found for type: ");
    if (paramObject == null) {}
    for (paramObject = "null";; paramObject = paramObject.getClass().getName()) {
      throw new IllegalArgumentException((String)paramObject);
    }
  }
  
  public PartialConverter[] getPartialConverters()
  {
    ConverterSet localConverterSet = this.iPartialConverters;
    PartialConverter[] arrayOfPartialConverter = new PartialConverter[localConverterSet.size()];
    localConverterSet.copyInto(arrayOfPartialConverter);
    return arrayOfPartialConverter;
  }
  
  public PeriodConverter getPeriodConverter(Object paramObject)
  {
    ConverterSet localConverterSet = this.iPeriodConverters;
    if (paramObject == null) {}
    for (Object localObject = null;; localObject = paramObject.getClass())
    {
      localObject = (PeriodConverter)localConverterSet.select((Class)localObject);
      if (localObject == null) {
        break;
      }
      return (PeriodConverter)localObject;
    }
    localObject = new StringBuilder().append("No period converter found for type: ");
    if (paramObject == null) {}
    for (paramObject = "null";; paramObject = paramObject.getClass().getName()) {
      throw new IllegalArgumentException((String)paramObject);
    }
  }
  
  public PeriodConverter[] getPeriodConverters()
  {
    ConverterSet localConverterSet = this.iPeriodConverters;
    PeriodConverter[] arrayOfPeriodConverter = new PeriodConverter[localConverterSet.size()];
    localConverterSet.copyInto(arrayOfPeriodConverter);
    return arrayOfPeriodConverter;
  }
  
  public DurationConverter removeDurationConverter(DurationConverter paramDurationConverter)
    throws SecurityException
  {
    checkAlterDurationConverters();
    if (paramDurationConverter == null) {}
    DurationConverter[] arrayOfDurationConverter;
    for (paramDurationConverter = null;; paramDurationConverter = arrayOfDurationConverter[0])
    {
      return paramDurationConverter;
      arrayOfDurationConverter = new DurationConverter[1];
      this.iDurationConverters = this.iDurationConverters.remove(paramDurationConverter, arrayOfDurationConverter);
    }
  }
  
  public InstantConverter removeInstantConverter(InstantConverter paramInstantConverter)
    throws SecurityException
  {
    checkAlterInstantConverters();
    if (paramInstantConverter == null) {}
    InstantConverter[] arrayOfInstantConverter;
    for (paramInstantConverter = null;; paramInstantConverter = arrayOfInstantConverter[0])
    {
      return paramInstantConverter;
      arrayOfInstantConverter = new InstantConverter[1];
      this.iInstantConverters = this.iInstantConverters.remove(paramInstantConverter, arrayOfInstantConverter);
    }
  }
  
  public IntervalConverter removeIntervalConverter(IntervalConverter paramIntervalConverter)
    throws SecurityException
  {
    checkAlterIntervalConverters();
    if (paramIntervalConverter == null) {}
    IntervalConverter[] arrayOfIntervalConverter;
    for (paramIntervalConverter = null;; paramIntervalConverter = arrayOfIntervalConverter[0])
    {
      return paramIntervalConverter;
      arrayOfIntervalConverter = new IntervalConverter[1];
      this.iIntervalConverters = this.iIntervalConverters.remove(paramIntervalConverter, arrayOfIntervalConverter);
    }
  }
  
  public PartialConverter removePartialConverter(PartialConverter paramPartialConverter)
    throws SecurityException
  {
    checkAlterPartialConverters();
    if (paramPartialConverter == null) {}
    PartialConverter[] arrayOfPartialConverter;
    for (paramPartialConverter = null;; paramPartialConverter = arrayOfPartialConverter[0])
    {
      return paramPartialConverter;
      arrayOfPartialConverter = new PartialConverter[1];
      this.iPartialConverters = this.iPartialConverters.remove(paramPartialConverter, arrayOfPartialConverter);
    }
  }
  
  public PeriodConverter removePeriodConverter(PeriodConverter paramPeriodConverter)
    throws SecurityException
  {
    checkAlterPeriodConverters();
    if (paramPeriodConverter == null) {}
    PeriodConverter[] arrayOfPeriodConverter;
    for (paramPeriodConverter = null;; paramPeriodConverter = arrayOfPeriodConverter[0])
    {
      return paramPeriodConverter;
      arrayOfPeriodConverter = new PeriodConverter[1];
      this.iPeriodConverters = this.iPeriodConverters.remove(paramPeriodConverter, arrayOfPeriodConverter);
    }
  }
  
  public String toString()
  {
    return "ConverterManager[" + this.iInstantConverters.size() + " instant," + this.iPartialConverters.size() + " partial," + this.iDurationConverters.size() + " duration," + this.iPeriodConverters.size() + " period," + this.iIntervalConverters.size() + " interval]";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\ConverterManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */