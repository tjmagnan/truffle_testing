package org.joda.time;

import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Years
  extends BaseSingleFieldPeriod
{
  public static final Years MAX_VALUE = new Years(Integer.MAX_VALUE);
  public static final Years MIN_VALUE = new Years(Integer.MIN_VALUE);
  public static final Years ONE;
  private static final PeriodFormatter PARSER = ISOPeriodFormat.standard().withParseType(PeriodType.years());
  public static final Years THREE;
  public static final Years TWO;
  public static final Years ZERO = new Years(0);
  private static final long serialVersionUID = 87525275727380868L;
  
  static
  {
    ONE = new Years(1);
    TWO = new Years(2);
    THREE = new Years(3);
  }
  
  private Years(int paramInt)
  {
    super(paramInt);
  }
  
  @FromString
  public static Years parseYears(String paramString)
  {
    if (paramString == null) {}
    for (paramString = ZERO;; paramString = years(PARSER.parsePeriod(paramString).getYears())) {
      return paramString;
    }
  }
  
  private Object readResolve()
  {
    return years(getValue());
  }
  
  public static Years years(int paramInt)
  {
    Years localYears;
    switch (paramInt)
    {
    default: 
      localYears = new Years(paramInt);
    }
    for (;;)
    {
      return localYears;
      localYears = ZERO;
      continue;
      localYears = ONE;
      continue;
      localYears = TWO;
      continue;
      localYears = THREE;
      continue;
      localYears = MAX_VALUE;
      continue;
      localYears = MIN_VALUE;
    }
  }
  
  public static Years yearsBetween(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    return years(BaseSingleFieldPeriod.between(paramReadableInstant1, paramReadableInstant2, DurationFieldType.years()));
  }
  
  public static Years yearsBetween(ReadablePartial paramReadablePartial1, ReadablePartial paramReadablePartial2)
  {
    if (((paramReadablePartial1 instanceof LocalDate)) && ((paramReadablePartial2 instanceof LocalDate))) {}
    for (paramReadablePartial1 = years(DateTimeUtils.getChronology(paramReadablePartial1.getChronology()).years().getDifference(((LocalDate)paramReadablePartial2).getLocalMillis(), ((LocalDate)paramReadablePartial1).getLocalMillis()));; paramReadablePartial1 = years(BaseSingleFieldPeriod.between(paramReadablePartial1, paramReadablePartial2, ZERO))) {
      return paramReadablePartial1;
    }
  }
  
  public static Years yearsIn(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (paramReadableInterval = ZERO;; paramReadableInterval = years(BaseSingleFieldPeriod.between(paramReadableInterval.getStart(), paramReadableInterval.getEnd(), DurationFieldType.years()))) {
      return paramReadableInterval;
    }
  }
  
  public Years dividedBy(int paramInt)
  {
    if (paramInt == 1) {}
    for (Years localYears = this;; localYears = years(getValue() / paramInt)) {
      return localYears;
    }
  }
  
  public DurationFieldType getFieldType()
  {
    return DurationFieldType.years();
  }
  
  public PeriodType getPeriodType()
  {
    return PeriodType.years();
  }
  
  public int getYears()
  {
    return getValue();
  }
  
  public boolean isGreaterThan(Years paramYears)
  {
    boolean bool = true;
    if (paramYears == null) {
      if (getValue() <= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() <= paramYears.getValue()) {
        bool = false;
      }
    }
  }
  
  public boolean isLessThan(Years paramYears)
  {
    boolean bool = true;
    if (paramYears == null) {
      if (getValue() >= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() >= paramYears.getValue()) {
        bool = false;
      }
    }
  }
  
  public Years minus(int paramInt)
  {
    return plus(FieldUtils.safeNegate(paramInt));
  }
  
  public Years minus(Years paramYears)
  {
    if (paramYears == null) {}
    for (paramYears = this;; paramYears = minus(paramYears.getValue())) {
      return paramYears;
    }
  }
  
  public Years multipliedBy(int paramInt)
  {
    return years(FieldUtils.safeMultiply(getValue(), paramInt));
  }
  
  public Years negated()
  {
    return years(FieldUtils.safeNegate(getValue()));
  }
  
  public Years plus(int paramInt)
  {
    if (paramInt == 0) {}
    for (Years localYears = this;; localYears = years(FieldUtils.safeAdd(getValue(), paramInt))) {
      return localYears;
    }
  }
  
  public Years plus(Years paramYears)
  {
    if (paramYears == null) {}
    for (paramYears = this;; paramYears = plus(paramYears.getValue())) {
      return paramYears;
    }
  }
  
  @ToString
  public String toString()
  {
    return "P" + String.valueOf(getValue()) + "Y";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Years.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */