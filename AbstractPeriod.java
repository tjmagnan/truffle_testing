package org.joda.time.base;

import org.joda.convert.ToString;
import org.joda.time.DurationFieldType;
import org.joda.time.MutablePeriod;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.ReadablePeriod;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public abstract class AbstractPeriod
  implements ReadablePeriod
{
  public boolean equals(Object paramObject)
  {
    boolean bool2 = true;
    boolean bool1;
    if (this == paramObject) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      if (!(paramObject instanceof ReadablePeriod))
      {
        bool1 = false;
      }
      else
      {
        paramObject = (ReadablePeriod)paramObject;
        if (size() == ((ReadablePeriod)paramObject).size()) {
          break;
        }
        bool1 = false;
      }
    }
    int j = size();
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= j) {
        break;
      }
      if ((getValue(i) != ((ReadablePeriod)paramObject).getValue(i)) || (getFieldType(i) != ((ReadablePeriod)paramObject).getFieldType(i)))
      {
        bool1 = false;
        break;
      }
    }
  }
  
  public int get(DurationFieldType paramDurationFieldType)
  {
    int i = indexOf(paramDurationFieldType);
    if (i == -1) {}
    for (i = 0;; i = getValue(i)) {
      return i;
    }
  }
  
  public DurationFieldType getFieldType(int paramInt)
  {
    return getPeriodType().getFieldType(paramInt);
  }
  
  public DurationFieldType[] getFieldTypes()
  {
    DurationFieldType[] arrayOfDurationFieldType = new DurationFieldType[size()];
    for (int i = 0; i < arrayOfDurationFieldType.length; i++) {
      arrayOfDurationFieldType[i] = getFieldType(i);
    }
    return arrayOfDurationFieldType;
  }
  
  public int[] getValues()
  {
    int[] arrayOfInt = new int[size()];
    for (int i = 0; i < arrayOfInt.length; i++) {
      arrayOfInt[i] = getValue(i);
    }
    return arrayOfInt;
  }
  
  public int hashCode()
  {
    int j = 17;
    int i = 0;
    int k = size();
    while (i < k)
    {
      j = (j * 27 + getValue(i)) * 27 + getFieldType(i).hashCode();
      i++;
    }
    return j;
  }
  
  public int indexOf(DurationFieldType paramDurationFieldType)
  {
    return getPeriodType().indexOf(paramDurationFieldType);
  }
  
  public boolean isSupported(DurationFieldType paramDurationFieldType)
  {
    return getPeriodType().isSupported(paramDurationFieldType);
  }
  
  public int size()
  {
    return getPeriodType().size();
  }
  
  public MutablePeriod toMutablePeriod()
  {
    return new MutablePeriod(this);
  }
  
  public Period toPeriod()
  {
    return new Period(this);
  }
  
  @ToString
  public String toString()
  {
    return ISOPeriodFormat.standard().print(this);
  }
  
  public String toString(PeriodFormatter paramPeriodFormatter)
  {
    if (paramPeriodFormatter == null) {}
    for (paramPeriodFormatter = toString();; paramPeriodFormatter = paramPeriodFormatter.print(this)) {
      return paramPeriodFormatter;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\AbstractPeriod.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */