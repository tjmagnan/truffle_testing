package com.afollestad.materialdialogs.prefs;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.preference.EditTextPreference;
import android.preference.Preference.BaseSavedState;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import com.afollestad.materialdialogs.commons.R.attr;
import com.afollestad.materialdialogs.commons.R.layout;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.util.DialogUtils;

public class MaterialEditTextPreference
  extends EditTextPreference
{
  private int color = 0;
  private MaterialDialog dialog;
  private EditText editText;
  
  public MaterialEditTextPreference(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null);
  }
  
  public MaterialEditTextPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialEditTextPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialEditTextPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet);
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet)
  {
    PrefUtil.setLayoutResource(paramContext, this, paramAttributeSet);
    if (Build.VERSION.SDK_INT >= 21) {}
    for (int i = DialogUtils.resolveColor(paramContext, 16843829);; i = 0)
    {
      i = DialogUtils.resolveColor(paramContext, R.attr.colorAccent, i);
      this.color = DialogUtils.resolveColor(paramContext, R.attr.md_widget_color, i);
      this.editText = new AppCompatEditText(paramContext, paramAttributeSet);
      this.editText.setId(16908291);
      this.editText.setEnabled(true);
      return;
    }
  }
  
  private void requestInputMethod(Dialog paramDialog)
  {
    paramDialog = paramDialog.getWindow();
    if (paramDialog == null) {}
    for (;;)
    {
      return;
      paramDialog.setSoftInputMode(5);
    }
  }
  
  public Dialog getDialog()
  {
    return this.dialog;
  }
  
  public EditText getEditText()
  {
    return this.editText;
  }
  
  public void onActivityDestroy()
  {
    super.onActivityDestroy();
    if ((this.dialog != null) && (this.dialog.isShowing())) {
      this.dialog.dismiss();
    }
  }
  
  protected void onAddEditTextToDialogView(@NonNull View paramView, @NonNull EditText paramEditText)
  {
    ((ViewGroup)paramView).addView(paramEditText, new LinearLayout.LayoutParams(-1, -2));
  }
  
  @SuppressLint({"MissingSuperCall"})
  protected void onBindDialogView(@NonNull View paramView)
  {
    EditText localEditText = this.editText;
    localEditText.setText(getText());
    if (localEditText.getText().length() > 0) {
      localEditText.setSelection(localEditText.length());
    }
    ViewParent localViewParent = localEditText.getParent();
    if (localViewParent != paramView)
    {
      if (localViewParent != null) {
        ((ViewGroup)localViewParent).removeView(localEditText);
      }
      onAddEditTextToDialogView(paramView, localEditText);
    }
  }
  
  protected void onDialogClosed(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      String str = this.editText.getText().toString();
      if (callChangeListener(str)) {
        setText(str);
      }
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    PrefUtil.unregisterOnActivityDestroyListener(this, this);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class))) {
      super.onRestoreInstanceState(paramParcelable);
    }
    for (;;)
    {
      return;
      paramParcelable = (SavedState)paramParcelable;
      super.onRestoreInstanceState(paramParcelable.getSuperState());
      if (paramParcelable.isDialogShowing) {
        showDialog(paramParcelable.dialogBundle);
      }
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Object localObject = super.onSaveInstanceState();
    Dialog localDialog = getDialog();
    if ((localDialog == null) || (!localDialog.isShowing())) {}
    for (;;)
    {
      return (Parcelable)localObject;
      localObject = new SavedState((Parcelable)localObject);
      ((SavedState)localObject).isDialogShowing = true;
      ((SavedState)localObject).dialogBundle = localDialog.onSaveInstanceState();
    }
  }
  
  protected void showDialog(Bundle paramBundle)
  {
    MaterialDialog.Builder localBuilder = new MaterialDialog.Builder(getContext()).title(getDialogTitle()).icon(getDialogIcon()).positiveText(getPositiveButtonText()).negativeText(getNegativeButtonText()).dismissListener(this).onAny(new MaterialDialog.SingleButtonCallback()
    {
      public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
      {
        switch (MaterialEditTextPreference.2.$SwitchMap$com$afollestad$materialdialogs$DialogAction[paramAnonymousDialogAction.ordinal()])
        {
        default: 
          MaterialEditTextPreference.this.onClick(paramAnonymousMaterialDialog, -1);
        }
        for (;;)
        {
          return;
          MaterialEditTextPreference.this.onClick(paramAnonymousMaterialDialog, -3);
          continue;
          MaterialEditTextPreference.this.onClick(paramAnonymousMaterialDialog, -2);
        }
      }
    }).dismissListener(this);
    View localView = LayoutInflater.from(getContext()).inflate(R.layout.md_stub_inputpref, null);
    onBindDialogView(localView);
    MDTintHelper.setTint(this.editText, this.color);
    TextView localTextView = (TextView)localView.findViewById(16908299);
    if ((getDialogMessage() != null) && (getDialogMessage().toString().length() > 0))
    {
      localTextView.setVisibility(0);
      localTextView.setText(getDialogMessage());
    }
    for (;;)
    {
      localBuilder.customView(localView, false);
      PrefUtil.registerOnActivityDestroyListener(this, this);
      this.dialog = localBuilder.build();
      if (paramBundle != null) {
        this.dialog.onRestoreInstanceState(paramBundle);
      }
      requestInputMethod(this.dialog);
      this.dialog.show();
      return;
      localTextView.setVisibility(8);
    }
  }
  
  private static class SavedState
    extends Preference.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public MaterialEditTextPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
      {
        return new MaterialEditTextPreference.SavedState(paramAnonymousParcel);
      }
      
      public MaterialEditTextPreference.SavedState[] newArray(int paramAnonymousInt)
      {
        return new MaterialEditTextPreference.SavedState[paramAnonymousInt];
      }
    };
    Bundle dialogBundle;
    boolean isDialogShowing;
    
    SavedState(Parcel paramParcel)
    {
      super();
      if (paramParcel.readInt() == 1) {}
      for (;;)
      {
        this.isDialogShowing = bool;
        this.dialogBundle = paramParcel.readBundle();
        return;
        bool = false;
      }
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(@NonNull Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      if (this.isDialogShowing) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        paramParcel.writeBundle(this.dialogBundle);
        return;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\prefs\MaterialEditTextPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */