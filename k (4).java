package com.testfairy.k;

import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.h.a;
import com.testfairy.h.c;
import com.testfairy.h.f;
import com.testfairy.n;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;

public class d
{
  public static final int a = 101;
  public static final int b = 100;
  public static final int c = 107;
  public static final int d = 119;
  public static final int e = 2;
  private static final String g = "build";
  private static final String h = "sessionToken";
  private static final String i = "data";
  public String f;
  
  public d(String paramString)
  {
    this.f = paramString;
  }
  
  private String a(String paramString)
  {
    return this.f + "?method=" + paramString;
  }
  
  private String a(String paramString1, String paramString2, String paramString3)
  {
    return this.f + "?method=" + paramString1 + "&" + "build" + "=" + paramString2 + "&" + "sessionToken" + "=" + paramString3;
  }
  
  private void a(String paramString, HttpEntity paramHttpEntity, c paramc)
  {
    new a().a(null, paramString, paramHttpEntity, "application/json", paramc);
  }
  
  private byte[] a(byte[] paramArrayOfByte)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream(paramArrayOfByte.length);
    GZIPOutputStream localGZIPOutputStream = new GZIPOutputStream(localByteArrayOutputStream);
    localGZIPOutputStream.write(paramArrayOfByte);
    localGZIPOutputStream.close();
    paramArrayOfByte = localByteArrayOutputStream.toByteArray();
    localByteArrayOutputStream.close();
    return paramArrayOfByte;
  }
  
  private void b(String paramString, f paramf, c paramc)
  {
    paramf.a("method", paramString);
    new a().a(this.f, paramf, paramc);
  }
  
  public String a()
  {
    return this.f;
  }
  
  public void a(f paramf, c paramc)
  {
    a(n.j, paramf, paramc, Integer.valueOf(30000));
  }
  
  public void a(String paramString, f paramf, c paramc)
  {
    a(paramString, paramf, paramc, null);
  }
  
  public void a(String paramString, f paramf, c paramc, Integer paramInteger)
  {
    a locala = new a();
    if (paramInteger != null) {
      locala.a(paramInteger.intValue());
    }
    locala.b(a(paramString), paramf, paramc);
  }
  
  public void a(String paramString1, String paramString2, c paramc)
  {
    f localf = new f();
    localf.a("sessionToken", paramString2);
    localf.a("data", paramString1);
    c(localf, paramc);
  }
  
  public void a(String paramString1, String paramString2, String paramString3, c paramc)
  {
    String str = a(n.d, paramString1, paramString2);
    try
    {
      byte[] arrayOfByte = a(paramString3.getBytes("UTF-8"));
      ByteArrayEntity localByteArrayEntity = new org/apache/http/entity/ByteArrayEntity;
      localByteArrayEntity.<init>(arrayOfByte);
      a(str, localByteArrayEntity, paramc);
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        f localf = new f();
        localf.a("build", paramString1);
        localf.a("sessionToken", paramString2);
        localf.a("data", paramString3);
        a(str, localf, paramc);
      }
    }
  }
  
  public void a(Map paramMap, byte[] paramArrayOfByte, c paramc)
  {
    paramArrayOfByte = new ByteArrayEntity(paramArrayOfByte);
    Uri.Builder localBuilder = Uri.parse(a(n.e)).buildUpon();
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localBuilder.appendQueryParameter(str, (String)paramMap.get(str));
    }
    paramMap = localBuilder.toString();
    new a().a(null, paramMap, paramArrayOfByte, "image/jpeg", paramc);
  }
  
  public void b(f paramf, c paramc)
  {
    a(n.k, paramf, paramc);
  }
  
  public void c(f paramf, c paramc)
  {
    a(n.h, paramf, paramc);
  }
  
  public void d(f paramf, c paramc)
  {
    a(n.o, paramf, paramc);
  }
  
  public void e(f paramf, c paramc)
  {
    a(n.e, paramf, paramc);
  }
  
  public void f(f paramf, c paramc)
  {
    a(n.f, paramf, paramc);
  }
  
  public void g(f paramf, c paramc)
  {
    Log.d(e.a, "Sending feedback to:  " + this.f);
    a(n.i, paramf, paramc);
  }
  
  public void h(f paramf, c paramc)
  {
    a(n.g, paramf, paramc);
  }
  
  public void i(f paramf, c paramc)
  {
    a(n.l, paramf, paramc);
  }
  
  public void j(f paramf, c paramc)
  {
    a(n.m, paramf, paramc);
  }
  
  public void k(f paramf, c paramc)
  {
    a(n.n, paramf, paramc);
  }
  
  public void l(f paramf, c paramc)
  {
    b(n.p, paramf, paramc);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\k\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */