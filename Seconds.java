package org.joda.time;

import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.base.BaseSingleFieldPeriod;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.ISOPeriodFormat;
import org.joda.time.format.PeriodFormatter;

public final class Seconds
  extends BaseSingleFieldPeriod
{
  public static final Seconds MAX_VALUE = new Seconds(Integer.MAX_VALUE);
  public static final Seconds MIN_VALUE = new Seconds(Integer.MIN_VALUE);
  public static final Seconds ONE;
  private static final PeriodFormatter PARSER = ISOPeriodFormat.standard().withParseType(PeriodType.seconds());
  public static final Seconds THREE;
  public static final Seconds TWO;
  public static final Seconds ZERO = new Seconds(0);
  private static final long serialVersionUID = 87525275727380862L;
  
  static
  {
    ONE = new Seconds(1);
    TWO = new Seconds(2);
    THREE = new Seconds(3);
  }
  
  private Seconds(int paramInt)
  {
    super(paramInt);
  }
  
  @FromString
  public static Seconds parseSeconds(String paramString)
  {
    if (paramString == null) {}
    for (paramString = ZERO;; paramString = seconds(PARSER.parsePeriod(paramString).getSeconds())) {
      return paramString;
    }
  }
  
  private Object readResolve()
  {
    return seconds(getValue());
  }
  
  public static Seconds seconds(int paramInt)
  {
    Seconds localSeconds;
    switch (paramInt)
    {
    default: 
      localSeconds = new Seconds(paramInt);
    }
    for (;;)
    {
      return localSeconds;
      localSeconds = ZERO;
      continue;
      localSeconds = ONE;
      continue;
      localSeconds = TWO;
      continue;
      localSeconds = THREE;
      continue;
      localSeconds = MAX_VALUE;
      continue;
      localSeconds = MIN_VALUE;
    }
  }
  
  public static Seconds secondsBetween(ReadableInstant paramReadableInstant1, ReadableInstant paramReadableInstant2)
  {
    return seconds(BaseSingleFieldPeriod.between(paramReadableInstant1, paramReadableInstant2, DurationFieldType.seconds()));
  }
  
  public static Seconds secondsBetween(ReadablePartial paramReadablePartial1, ReadablePartial paramReadablePartial2)
  {
    if (((paramReadablePartial1 instanceof LocalTime)) && ((paramReadablePartial2 instanceof LocalTime))) {}
    for (paramReadablePartial1 = seconds(DateTimeUtils.getChronology(paramReadablePartial1.getChronology()).seconds().getDifference(((LocalTime)paramReadablePartial2).getLocalMillis(), ((LocalTime)paramReadablePartial1).getLocalMillis()));; paramReadablePartial1 = seconds(BaseSingleFieldPeriod.between(paramReadablePartial1, paramReadablePartial2, ZERO))) {
      return paramReadablePartial1;
    }
  }
  
  public static Seconds secondsIn(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (paramReadableInterval = ZERO;; paramReadableInterval = seconds(BaseSingleFieldPeriod.between(paramReadableInterval.getStart(), paramReadableInterval.getEnd(), DurationFieldType.seconds()))) {
      return paramReadableInterval;
    }
  }
  
  public static Seconds standardSecondsIn(ReadablePeriod paramReadablePeriod)
  {
    return seconds(BaseSingleFieldPeriod.standardPeriodIn(paramReadablePeriod, 1000L));
  }
  
  public Seconds dividedBy(int paramInt)
  {
    if (paramInt == 1) {}
    for (Seconds localSeconds = this;; localSeconds = seconds(getValue() / paramInt)) {
      return localSeconds;
    }
  }
  
  public DurationFieldType getFieldType()
  {
    return DurationFieldType.seconds();
  }
  
  public PeriodType getPeriodType()
  {
    return PeriodType.seconds();
  }
  
  public int getSeconds()
  {
    return getValue();
  }
  
  public boolean isGreaterThan(Seconds paramSeconds)
  {
    boolean bool = true;
    if (paramSeconds == null) {
      if (getValue() <= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() <= paramSeconds.getValue()) {
        bool = false;
      }
    }
  }
  
  public boolean isLessThan(Seconds paramSeconds)
  {
    boolean bool = true;
    if (paramSeconds == null) {
      if (getValue() >= 0) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      if (getValue() >= paramSeconds.getValue()) {
        bool = false;
      }
    }
  }
  
  public Seconds minus(int paramInt)
  {
    return plus(FieldUtils.safeNegate(paramInt));
  }
  
  public Seconds minus(Seconds paramSeconds)
  {
    if (paramSeconds == null) {}
    for (paramSeconds = this;; paramSeconds = minus(paramSeconds.getValue())) {
      return paramSeconds;
    }
  }
  
  public Seconds multipliedBy(int paramInt)
  {
    return seconds(FieldUtils.safeMultiply(getValue(), paramInt));
  }
  
  public Seconds negated()
  {
    return seconds(FieldUtils.safeNegate(getValue()));
  }
  
  public Seconds plus(int paramInt)
  {
    if (paramInt == 0) {}
    for (Seconds localSeconds = this;; localSeconds = seconds(FieldUtils.safeAdd(getValue(), paramInt))) {
      return localSeconds;
    }
  }
  
  public Seconds plus(Seconds paramSeconds)
  {
    if (paramSeconds == null) {}
    for (paramSeconds = this;; paramSeconds = plus(paramSeconds.getValue())) {
      return paramSeconds;
    }
  }
  
  public Days toStandardDays()
  {
    return Days.days(getValue() / 86400);
  }
  
  public Duration toStandardDuration()
  {
    return new Duration(getValue() * 1000L);
  }
  
  public Hours toStandardHours()
  {
    return Hours.hours(getValue() / 3600);
  }
  
  public Minutes toStandardMinutes()
  {
    return Minutes.minutes(getValue() / 60);
  }
  
  public Weeks toStandardWeeks()
  {
    return Weeks.weeks(getValue() / 604800);
  }
  
  @ToString
  public String toString()
  {
    return "PT" + String.valueOf(getValue()) + "S";
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\Seconds.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */