package org.jsoup.parser;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings;
import org.jsoup.nodes.Document.OutputSettings.Syntax;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.nodes.XmlDeclaration;

public class XmlTreeBuilder
  extends TreeBuilder
{
  private void insertNode(Node paramNode)
  {
    currentElement().appendChild(paramNode);
  }
  
  private void popStackToClose(Token.EndTag paramEndTag)
  {
    String str = paramEndTag.name();
    Element localElement = null;
    int i = this.stack.size() - 1;
    paramEndTag = localElement;
    if (i >= 0)
    {
      paramEndTag = (Element)this.stack.get(i);
      if (!paramEndTag.nodeName().equals(str)) {}
    }
    else
    {
      if (paramEndTag != null) {
        break label59;
      }
    }
    label59:
    label103:
    for (;;)
    {
      return;
      i--;
      break;
      for (i = this.stack.size() - 1;; i--)
      {
        if (i < 0) {
          break label103;
        }
        localElement = (Element)this.stack.get(i);
        this.stack.remove(i);
        if (localElement == paramEndTag) {
          break;
        }
      }
    }
  }
  
  ParseSettings defaultSettings()
  {
    return ParseSettings.preserveCase;
  }
  
  protected void initialiseParse(String paramString1, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    super.initialiseParse(paramString1, paramString2, paramParseErrorList, paramParseSettings);
    this.stack.add(this.doc);
    this.doc.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
  }
  
  Element insert(Token.StartTag paramStartTag)
  {
    Tag localTag = Tag.valueOf(paramStartTag.name(), this.settings);
    Element localElement = new Element(localTag, this.baseUri, this.settings.normalizeAttributes(paramStartTag.attributes));
    insertNode(localElement);
    if (paramStartTag.isSelfClosing())
    {
      this.tokeniser.acknowledgeSelfClosingFlag();
      if (!localTag.isKnownTag()) {
        localTag.setSelfClosing();
      }
    }
    for (;;)
    {
      return localElement;
      this.stack.add(localElement);
    }
  }
  
  void insert(Token.Character paramCharacter)
  {
    insertNode(new TextNode(paramCharacter.getData(), this.baseUri));
  }
  
  void insert(Token.Comment paramComment)
  {
    Comment localComment = new Comment(paramComment.getData(), this.baseUri);
    Object localObject1 = localComment;
    Object localObject2 = localObject1;
    if (paramComment.bogus)
    {
      paramComment = localComment.getData();
      localObject2 = localObject1;
      if (paramComment.length() > 1) {
        if (!paramComment.startsWith("!"))
        {
          localObject2 = localObject1;
          if (!paramComment.startsWith("?")) {}
        }
        else
        {
          localObject1 = Jsoup.parse("<" + paramComment.substring(1, paramComment.length() - 1) + ">", this.baseUri, Parser.xmlParser()).child(0);
          localObject2 = new XmlDeclaration(this.settings.normalizeTag(((Element)localObject1).tagName()), localComment.baseUri(), paramComment.startsWith("!"));
          ((Node)localObject2).attributes().addAll(((Element)localObject1).attributes());
        }
      }
    }
    insertNode((Node)localObject2);
  }
  
  void insert(Token.Doctype paramDoctype)
  {
    insertNode(new DocumentType(this.settings.normalizeTag(paramDoctype.getName()), paramDoctype.getPubSysKey(), paramDoctype.getPublicIdentifier(), paramDoctype.getSystemIdentifier(), this.baseUri));
  }
  
  Document parse(String paramString1, String paramString2)
  {
    return parse(paramString1, paramString2, ParseErrorList.noTracking(), ParseSettings.preserveCase);
  }
  
  List<Node> parseFragment(String paramString1, String paramString2, ParseErrorList paramParseErrorList, ParseSettings paramParseSettings)
  {
    initialiseParse(paramString1, paramString2, paramParseErrorList, paramParseSettings);
    runParser();
    return this.doc.childNodes();
  }
  
  protected boolean process(Token paramToken)
  {
    switch (paramToken.type)
    {
    default: 
      Validate.fail("Unexpected token type: " + paramToken.type);
    }
    for (;;)
    {
      return true;
      insert(paramToken.asStartTag());
      continue;
      popStackToClose(paramToken.asEndTag());
      continue;
      insert(paramToken.asComment());
      continue;
      insert(paramToken.asCharacter());
      continue;
      insert(paramToken.asDoctype());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\XmlTreeBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */