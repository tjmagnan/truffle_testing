package tech.dcube.companion.managers.server;

import android.content.Context;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.backend.PBRequest.PBResponse;
import tech.dcube.companion.managers.server.backend.VolleySingleton;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.BarcodeResponse;
import tech.dcube.companion.model.BarcodeResponse.Catalogue;
import tech.dcube.companion.model.BarcodeResponseItem;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBContact;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBParcelPackage;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentPrintLabel;
import tech.dcube.companion.model.PBShipmentRate;
import tech.dcube.companion.model.PBShipmentService;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

class ShipmentManager
{
  private static final Gson gson = new Gson();
  private static final ShipmentManager ourInstance = new ShipmentManager();
  private DataManager dataManager = DataManager.getInstance();
  private Request reqContactsList;
  private Request reqGetAllPackages;
  private Request reqGetPackages;
  private Request reqGetRate;
  private Request reqGetServices;
  private Request reqScanBarcode;
  private Request reqVerfiyAddress;
  
  static ShipmentManager getInstance()
  {
    return ourInstance;
  }
  
  String getAddressBook(final Context paramContext, final ServerManager.RequestCompletionHandler<List<PBAddress>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_ADDRESS_BOOK);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqContactsList = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Object localObject = new JsonParser().parse(paramAnonymousPBResponse.body).getAsJsonObject().get("content");
        paramAnonymousPBResponse = new ArrayList();
        if (localObject != null)
        {
          localObject = PBAddress.fromJsonArray(PBAddress[].class, ((JsonElement)localObject).toString());
          paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          if (localObject != null)
          {
            paramAnonymousPBResponse = ((List)localObject).iterator();
            while (paramAnonymousPBResponse.hasNext()) {
              ((PBAddress)paramAnonymousPBResponse.next()).setAddressType("Recipient");
            }
            ShipmentManager.this.dataManager.getAddresses().clear();
            ShipmentManager.this.dataManager.getAddresses().addAll((Collection)localObject);
            paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqContactsList.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Address Book", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Address Book", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getAddressBook(ShipmentManager.8.this.val$context, ShipmentManager.8.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqContactsList);
    return this.reqContactsList.getTag().toString();
  }
  
  String getAllPackages(final Context paramContext, final ServerManager.RequestCompletionHandler<Map<String, PBParcelPackage>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_ALL_PACKAGES);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    this.reqGetAllPackages = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = PBParcelPackage.fromJsonArray(paramAnonymousPBResponse.body);
        if (paramAnonymousPBResponse != null)
        {
          ShipmentManager.this.dataManager.getParcelPackages().clear();
          ShipmentManager.this.dataManager.getParcelPackages().putAll(paramAnonymousPBResponse);
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqGetAllPackages.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: All Packages", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: All Packages", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getAllPackages(ShipmentManager.10.this.val$context, ShipmentManager.10.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetAllPackages);
    return this.reqGetAllPackages.getTag().toString();
  }
  
  String getAvailablePackages(final Context paramContext, final PBAddress paramPBAddress, final ServerManager.RequestCompletionHandler<Map<PBPackageType, List<PBShipmentPackage>>> paramRequestCompletionHandler)
  {
    String str2 = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_GET_SELECTED_PACKAGES);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    String str1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("cultureCode", "en-US");
    localHashMap2.put("weightUnits", "IMPERIAL");
    localHashMap2.put("dimensionUnits", "IMPERIAL");
    localHashMap2.put("dateOfShipment", str1);
    localHashMap2.put("sender", this.dataManager.getUser().getAddress().toJsonMap().get("address"));
    localHashMap2.put("recipient", paramPBAddress.toJsonMap().get("address"));
    this.reqGetPackages = ServerManager.createRequest(1, str2, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        HashMap localHashMap = new HashMap();
        localHashMap.put(PBPackageType.FLAT_RATE_BOX, new ArrayList());
        localHashMap.put(PBPackageType.FLAT_RATE_ENVELOPE, new ArrayList());
        localHashMap.put(PBPackageType.ENVELOPE, new ArrayList());
        localHashMap.put(PBPackageType.PACKAGE, new ArrayList());
        localHashMap.put(PBPackageType.PACKAGE_LARGE, new ArrayList());
        Iterator localIterator = PBShipmentPackage.fromJsonArray(PBShipmentPackage[].class, paramAnonymousPBResponse.body).iterator();
        while (localIterator.hasNext())
        {
          paramAnonymousPBResponse = (PBShipmentPackage)localIterator.next();
          if (paramAnonymousPBResponse.getRateType().equalsIgnoreCase("COMMERCIAL_BASE"))
          {
            if (paramAnonymousPBResponse.getPackageName().toLowerCase().contains("box")) {
              ((List)localHashMap.get(PBPackageType.FLAT_RATE_BOX)).add(paramAnonymousPBResponse);
            } else {
              ((List)localHashMap.get(PBPackageType.FLAT_RATE_ENVELOPE)).add(paramAnonymousPBResponse);
            }
          }
          else if (paramAnonymousPBResponse.getPackageId().equalsIgnoreCase("LGENV")) {
            ((List)localHashMap.get(PBPackageType.ENVELOPE)).add(paramAnonymousPBResponse);
          } else if (paramAnonymousPBResponse.getPackageId().equalsIgnoreCase("PKG")) {
            ((List)localHashMap.get(PBPackageType.PACKAGE)).add(paramAnonymousPBResponse);
          } else {
            ((List)localHashMap.get(PBPackageType.PACKAGE_LARGE)).add(paramAnonymousPBResponse);
          }
        }
        ShipmentManager.this.dataManager.getShipmentPackages().clear();
        ShipmentManager.this.dataManager.getShipmentPackages().putAll(localHashMap);
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(localHashMap, ShipmentManager.this.reqGetPackages.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Available Packages", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Available Packages", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getAvailablePackages(ShipmentManager.4.this.val$context, ShipmentManager.4.this.val$address, ShipmentManager.4.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetPackages);
    return this.reqGetPackages.getTag().toString();
  }
  
  String getItemFromBarcode(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<BarcodeResponseItem> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_BARCODE_SHIPMENT_SCAN), new Object[] { paramString });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqScanBarcode = ServerManager.createRequest(0, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (BarcodeResponse)ShipmentManager.gson.fromJson(paramAnonymousPBResponse.body, BarcodeResponse.class);
        if ((paramAnonymousPBResponse != null) && (paramAnonymousPBResponse.getCatalogue() != null)) {
          if ((paramAnonymousPBResponse.getMessage().equalsIgnoreCase("success")) && (paramRequestCompletionHandler != null)) {
            paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse.getCatalogue().getItem(), ShipmentManager.this.reqScanBarcode.getTag().toString());
          }
        }
        for (;;)
        {
          return;
          if (paramRequestCompletionHandler != null)
          {
            paramAnonymousPBResponse = new VolleyError("Item not available!");
            paramRequestCompletionHandler.onError(paramAnonymousPBResponse, ShipmentManager.this.reqScanBarcode.getTag().toString());
          }
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Barcode Item", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Barcode Item", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getItemFromBarcode(ShipmentManager.2.this.val$context, ShipmentManager.2.this.val$UPC, ShipmentManager.2.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqScanBarcode);
    return this.reqScanBarcode.getTag().toString();
  }
  
  String getServices(final Context paramContext, final PBAddress paramPBAddress1, final PBAddress paramPBAddress2, final PBShipmentPackage paramPBShipmentPackage, final ServerManager.RequestCompletionHandler<List<PBShipmentService>> paramRequestCompletionHandler)
  {
    String str2 = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_GET_SERVICES);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    String str1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("cultureCode", "en-US");
    localHashMap2.put("weightUnits", "IMPERIAL");
    localHashMap2.put("dimensionUnits", "IMPERIAL");
    localHashMap2.put("dateOfShipment", str1);
    localHashMap2.put("sender", paramPBAddress1.toJsonMap().get("address"));
    localHashMap2.put("recipient", paramPBAddress2.toJsonMap().get("address"));
    localHashMap2.put("inductionPostalCode", paramPBAddress1.getPostalCode());
    localHashMap2.put("serviceOptions", new String[] { paramPBShipmentPackage.getPackageId() });
    localHashMap2.put("dateOfShipmentTimeZone", UtilManager.getSystemTimeZone());
    localHashMap2.put("packageDetails", paramPBShipmentPackage.toJsonMap());
    Log.wtf("Shipment", "Body: " + gson.toJson(localHashMap2));
    this.reqGetServices = ServerManager.createRequest(1, str2, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = PBShipmentService.fromJsonArray(PBShipmentService[].class, paramAnonymousPBResponse.body);
        if (paramAnonymousPBResponse == null) {
          paramAnonymousPBResponse = new ArrayList();
        }
        for (;;)
        {
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqGetServices.getTag().toString());
          }
          return;
          ShipmentManager.this.dataManager.getServices().clear();
          ShipmentManager.this.dataManager.getServices().addAll(paramAnonymousPBResponse);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Services", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Services", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getServices(ShipmentManager.12.this.val$context, ShipmentManager.12.this.val$sender, ShipmentManager.12.this.val$recipient, ShipmentManager.12.this.val$shipmentPackage, ShipmentManager.12.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetServices);
    return this.reqGetServices.getTag().toString();
  }
  
  String getShipmentRate(final Context paramContext, final PBAddress paramPBAddress1, final PBAddress paramPBAddress2, final PBShipmentPackage paramPBShipmentPackage, final PBShipmentService paramPBShipmentService, final ServerManager.RequestCompletionHandler<PBShipmentRate> paramRequestCompletionHandler)
  {
    String str2 = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_SHIPMENT_RATE);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    String str1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("cultureCode", "en-US");
    localHashMap2.put("weightUnits", "IMPERIAL");
    localHashMap2.put("dimensionUnits", "IMPERIAL");
    localHashMap2.put("dateOfShipment", str1);
    localHashMap2.put("sender", paramPBAddress1.toJsonMap().get("address"));
    localHashMap2.put("recipient", paramPBAddress2.toJsonMap().get("address"));
    localHashMap2.put("inductionPostalCode", paramPBAddress1.getPostalCode());
    localHashMap2.put("dateOfShipmentTimeZone", UtilManager.getSystemTimeZone());
    localHashMap2.put("packageDetails", paramPBShipmentPackage.toJsonMap());
    localHashMap2.put("mailClass", paramPBShipmentService.getServiceId());
    localHashMap2.put("specialServices", null);
    localHashMap2.put("includeAndHideSuggestedTrackingService", Boolean.valueOf(true));
    localHashMap2.put("customInfo", null);
    this.reqGetRate = ServerManager.createRequest(1, str2, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (PBShipmentRate)PBShipmentRate.fromJson(PBShipmentRate.class, paramAnonymousPBResponse.body);
        if (paramRequestCompletionHandler != null)
        {
          if (paramAnonymousPBResponse != null) {
            break label47;
          }
          ServerManager.sendError(ShipmentManager.this.reqGetRate.getTag().toString(), "Failed to get Shipment Rate!", paramRequestCompletionHandler);
        }
        for (;;)
        {
          return;
          label47:
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqGetRate.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Shipment Rates", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Shipment Rates", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.getShipmentRate(ShipmentManager.14.this.val$context, ShipmentManager.14.this.val$sender, ShipmentManager.14.this.val$recipient, ShipmentManager.14.this.val$shipmentPackage, ShipmentManager.14.this.val$service, ShipmentManager.14.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetRate);
    return this.reqGetRate.getTag().toString();
  }
  
  String printShipmentLabel(final Context paramContext, final PBAddress paramPBAddress1, final PBAddress paramPBAddress2, final PBShipmentPackage paramPBShipmentPackage, final PBShipmentService paramPBShipmentService, final PBShipmentRate paramPBShipmentRate, final ServerManager.RequestCompletionHandler<PBShipmentPrintLabel> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_SHIPMENT_LABEL);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    Object localObject = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("cultureCode", "en-US");
    localHashMap2.put("weightUnits", "IMPERIAL");
    localHashMap2.put("dimensionUnits", "IMPERIAL");
    localHashMap2.put("dateOfShipment", localObject);
    localHashMap2.put("sender", paramPBAddress1.toJsonMap().get("address"));
    localHashMap2.put("recipient", paramPBAddress2.toJsonMap().get("address"));
    localHashMap2.put("inductionPostalCode", paramPBAddress1.getPostalCode());
    localHashMap2.put("dateOfShipmentTimeZone", UtilManager.getSystemTimeZone());
    localHashMap2.put("packageDetails", paramPBShipmentPackage.toJsonMap());
    localHashMap2.put("mailClass", paramPBShipmentService.getServiceId());
    localHashMap2.put("specialServices", null);
    localHashMap2.put("includeAndHideSuggestedTrackingService", Boolean.valueOf(true));
    localHashMap2.put("customInfo", null);
    localHashMap2.put("totalPackageCharge", paramPBShipmentRate.getTotalPackageCharge());
    localHashMap2.put("stealth", null);
    localHashMap2.put("memo", "");
    localHashMap2.put("senderPhone", paramPBAddress1.getContact().getHomePhone());
    localHashMap2.put("customMessage", null);
    localHashMap2.put("constructValidLastLine", Boolean.valueOf(false));
    localHashMap2.put("deliveryNotificationEmails", new String[] { paramPBAddress1.getContact().getPrimaryEmail() });
    localHashMap2.put("printNotificationEmails", new String[] { paramPBAddress1.getContact().getPrimaryEmail() });
    localHashMap2.put("addToScanForm", Boolean.valueOf(true));
    localObject = new HashMap();
    ((Map)localObject).put("hidePostage", Boolean.valueOf(true));
    ((Map)localObject).put("labelSize", "s8x11");
    ((Map)localObject).put("printReceipt", Boolean.valueOf(true));
    localHashMap2.put("labelDetails", localObject);
    Log.wtf("Shipment", "Body: " + gson.toJson(localHashMap2));
    this.reqGetRate = ServerManager.createRequest(1, str, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (PBShipmentPrintLabel)PBShipmentPrintLabel.fromJson(PBShipmentPrintLabel.class, paramAnonymousPBResponse.body);
        if (paramAnonymousPBResponse == null) {
          ServerManager.sendError(ShipmentManager.this.reqGetRate.getTag().toString(), "Failed to get Shipment Print Label!", paramRequestCompletionHandler);
        }
        for (;;)
        {
          return;
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqGetRate.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Print Label Status", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Print Label Error", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.printShipmentLabel(ShipmentManager.16.this.val$context, ShipmentManager.16.this.val$sender, ShipmentManager.16.this.val$recipient, ShipmentManager.16.this.val$shipmentPackage, ShipmentManager.16.this.val$service, ShipmentManager.16.this.val$rate, ShipmentManager.16.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetRate);
    return this.reqGetRate.getTag().toString();
  }
  
  String verifyAddress(final Context paramContext, final PBAddress paramPBAddress, final ServerManager.RequestCompletionHandler<PBAddress> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_VERIFY_ADDRESS);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqVerfiyAddress = ServerManager.createRequest(1, str, "application/json", localHashMap, paramPBAddress.toJsonMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Map localMap = (Map)ShipmentManager.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        Object localObject = null;
        paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
        if (localMap != null)
        {
          paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
          if (localMap.get("status") != null)
          {
            paramAnonymousPBResponse = localMap.get("status").toString();
            if (!paramAnonymousPBResponse.equalsIgnoreCase("Valid")) {
              break label97;
            }
            paramAnonymousPBResponse = paramPBAddress;
          }
        }
        if (paramRequestCompletionHandler != null)
        {
          if (paramAnonymousPBResponse != null) {
            break label187;
          }
          ServerManager.sendError(ShipmentManager.this.reqVerfiyAddress.getTag().toString(), "Address verification failed!", paramRequestCompletionHandler);
        }
        for (;;)
        {
          return;
          label97:
          if (paramAnonymousPBResponse.equalsIgnoreCase("OriginalOrSuggested"))
          {
            paramAnonymousPBResponse = (PBRequest.PBResponse)localObject;
            if (localMap.get("suggestedAddress") == null) {
              break;
            }
            paramAnonymousPBResponse = (PBAddress)PBAddress.fromJson(PBAddress.class, ShipmentManager.gson.toJson(localMap.get("suggestedAddress")));
            paramAnonymousPBResponse.setContact(paramPBAddress.getContact());
            paramAnonymousPBResponse.setAddressType("Recipient");
            break;
          }
          ServerManager.sendError(ShipmentManager.this.reqVerfiyAddress.getTag().toString(), "Address verification failed!", paramRequestCompletionHandler);
          continue;
          label187:
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, ShipmentManager.this.reqVerfiyAddress.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("SM: Verify Address", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("SM: Verify Address", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            ShipmentManager.ourInstance.verifyAddress(ShipmentManager.6.this.val$context, ShipmentManager.6.this.val$address, ShipmentManager.6.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqVerfiyAddress);
    return this.reqVerfiyAddress.getTag().toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\ShipmentManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */