package io.fabric.sdk.android.services.common;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueueFile
  implements Closeable
{
  static final int HEADER_LENGTH = 16;
  private static final int INITIAL_LENGTH = 4096;
  private static final Logger LOGGER = Logger.getLogger(QueueFile.class.getName());
  private final byte[] buffer = new byte[16];
  private int elementCount;
  int fileLength;
  private Element first;
  private Element last;
  private final RandomAccessFile raf;
  
  public QueueFile(File paramFile)
    throws IOException
  {
    if (!paramFile.exists()) {
      initialize(paramFile);
    }
    this.raf = open(paramFile);
    readHeader();
  }
  
  QueueFile(RandomAccessFile paramRandomAccessFile)
    throws IOException
  {
    this.raf = paramRandomAccessFile;
    readHeader();
  }
  
  private void expandIfNecessary(int paramInt)
    throws IOException
  {
    int m = paramInt + 4;
    paramInt = remainingBytes();
    if (paramInt >= m) {
      return;
    }
    int i = this.fileLength;
    int k;
    int j;
    do
    {
      k = paramInt + i;
      j = i << 1;
      i = j;
      paramInt = k;
    } while (k < m);
    setLength(j);
    paramInt = wrapPosition(this.last.position + 4 + this.last.length);
    if (paramInt < this.first.position)
    {
      FileChannel localFileChannel = this.raf.getChannel();
      localFileChannel.position(this.fileLength);
      paramInt -= 4;
      if (localFileChannel.transferTo(16L, paramInt, localFileChannel) != paramInt) {
        throw new AssertionError("Copied insufficient number of bytes!");
      }
    }
    if (this.last.position < this.first.position)
    {
      paramInt = this.fileLength + this.last.position - 16;
      writeHeader(j, this.elementCount, this.first.position, paramInt);
      this.last = new Element(paramInt, this.last.length);
    }
    for (;;)
    {
      this.fileLength = j;
      break;
      writeHeader(j, this.elementCount, this.first.position, this.last.position);
    }
  }
  
  private static void initialize(File paramFile)
    throws IOException
  {
    File localFile = new File(paramFile.getPath() + ".tmp");
    RandomAccessFile localRandomAccessFile = open(localFile);
    try
    {
      localRandomAccessFile.setLength(4096L);
      localRandomAccessFile.seek(0L);
      byte[] arrayOfByte = new byte[16];
      writeInts(arrayOfByte, new int[] { 4096, 0, 0, 0 });
      localRandomAccessFile.write(arrayOfByte);
      localRandomAccessFile.close();
      if (!localFile.renameTo(paramFile)) {
        throw new IOException("Rename failed!");
      }
    }
    finally
    {
      localRandomAccessFile.close();
    }
  }
  
  private static <T> T nonNull(T paramT, String paramString)
  {
    if (paramT == null) {
      throw new NullPointerException(paramString);
    }
    return paramT;
  }
  
  private static RandomAccessFile open(File paramFile)
    throws FileNotFoundException
  {
    return new RandomAccessFile(paramFile, "rwd");
  }
  
  private Element readElement(int paramInt)
    throws IOException
  {
    if (paramInt == 0) {}
    for (Element localElement = Element.NULL;; localElement = new Element(paramInt, this.raf.readInt()))
    {
      return localElement;
      this.raf.seek(paramInt);
    }
  }
  
  private void readHeader()
    throws IOException
  {
    this.raf.seek(0L);
    this.raf.readFully(this.buffer);
    this.fileLength = readInt(this.buffer, 0);
    if (this.fileLength > this.raf.length()) {
      throw new IOException("File is truncated. Expected length: " + this.fileLength + ", Actual length: " + this.raf.length());
    }
    this.elementCount = readInt(this.buffer, 4);
    int i = readInt(this.buffer, 8);
    int j = readInt(this.buffer, 12);
    this.first = readElement(i);
    this.last = readElement(j);
  }
  
  private static int readInt(byte[] paramArrayOfByte, int paramInt)
  {
    return ((paramArrayOfByte[paramInt] & 0xFF) << 24) + ((paramArrayOfByte[(paramInt + 1)] & 0xFF) << 16) + ((paramArrayOfByte[(paramInt + 2)] & 0xFF) << 8) + (paramArrayOfByte[(paramInt + 3)] & 0xFF);
  }
  
  private int remainingBytes()
  {
    return this.fileLength - usedBytes();
  }
  
  private void ringRead(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
    throws IOException
  {
    paramInt1 = wrapPosition(paramInt1);
    if (paramInt1 + paramInt3 <= this.fileLength)
    {
      this.raf.seek(paramInt1);
      this.raf.readFully(paramArrayOfByte, paramInt2, paramInt3);
    }
    for (;;)
    {
      return;
      int i = this.fileLength - paramInt1;
      this.raf.seek(paramInt1);
      this.raf.readFully(paramArrayOfByte, paramInt2, i);
      this.raf.seek(16L);
      this.raf.readFully(paramArrayOfByte, paramInt2 + i, paramInt3 - i);
    }
  }
  
  private void ringWrite(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
    throws IOException
  {
    int i = wrapPosition(paramInt1);
    if (i + paramInt3 <= this.fileLength)
    {
      this.raf.seek(i);
      this.raf.write(paramArrayOfByte, paramInt2, paramInt3);
    }
    for (;;)
    {
      return;
      paramInt1 = this.fileLength - i;
      this.raf.seek(i);
      this.raf.write(paramArrayOfByte, paramInt2, paramInt1);
      this.raf.seek(16L);
      this.raf.write(paramArrayOfByte, paramInt2 + paramInt1, paramInt3 - paramInt1);
    }
  }
  
  private void setLength(int paramInt)
    throws IOException
  {
    this.raf.setLength(paramInt);
    this.raf.getChannel().force(true);
  }
  
  private int wrapPosition(int paramInt)
  {
    if (paramInt < this.fileLength) {}
    for (;;)
    {
      return paramInt;
      paramInt = paramInt + 16 - this.fileLength;
    }
  }
  
  private void writeHeader(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    throws IOException
  {
    writeInts(this.buffer, new int[] { paramInt1, paramInt2, paramInt3, paramInt4 });
    this.raf.seek(0L);
    this.raf.write(this.buffer);
  }
  
  private static void writeInt(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 >> 24));
    paramArrayOfByte[(paramInt1 + 1)] = ((byte)(paramInt2 >> 16));
    paramArrayOfByte[(paramInt1 + 2)] = ((byte)(paramInt2 >> 8));
    paramArrayOfByte[(paramInt1 + 3)] = ((byte)paramInt2);
  }
  
  private static void writeInts(byte[] paramArrayOfByte, int... paramVarArgs)
  {
    int j = 0;
    int k = paramVarArgs.length;
    for (int i = 0; i < k; i++)
    {
      writeInt(paramArrayOfByte, j, paramVarArgs[i]);
      j += 4;
    }
  }
  
  public void add(byte[] paramArrayOfByte)
    throws IOException
  {
    add(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public void add(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    try
    {
      nonNull(paramArrayOfByte, "buffer");
      if (((paramInt1 | paramInt2) < 0) || (paramInt2 > paramArrayOfByte.length - paramInt1))
      {
        paramArrayOfByte = new java/lang/IndexOutOfBoundsException;
        paramArrayOfByte.<init>();
        throw paramArrayOfByte;
      }
    }
    finally {}
    expandIfNecessary(paramInt2);
    boolean bool = isEmpty();
    int i;
    Element localElement;
    if (bool)
    {
      i = 16;
      localElement = new io/fabric/sdk/android/services/common/QueueFile$Element;
      localElement.<init>(i, paramInt2);
      writeInt(this.buffer, 0, paramInt2);
      ringWrite(localElement.position, this.buffer, 0, 4);
      ringWrite(localElement.position + 4, paramArrayOfByte, paramInt1, paramInt2);
      if (!bool) {
        break label199;
      }
    }
    label199:
    for (paramInt1 = localElement.position;; paramInt1 = this.first.position)
    {
      writeHeader(this.fileLength, this.elementCount + 1, paramInt1, localElement.position);
      this.last = localElement;
      this.elementCount += 1;
      if (bool) {
        this.first = this.last;
      }
      return;
      i = wrapPosition(this.last.position + 4 + this.last.length);
      break;
    }
  }
  
  public void clear()
    throws IOException
  {
    try
    {
      writeHeader(4096, 0, 0, 0);
      this.elementCount = 0;
      this.first = Element.NULL;
      this.last = Element.NULL;
      if (this.fileLength > 4096) {
        setLength(4096);
      }
      this.fileLength = 4096;
      return;
    }
    finally {}
  }
  
  public void close()
    throws IOException
  {
    try
    {
      this.raf.close();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void forEach(ElementReader paramElementReader)
    throws IOException
  {
    try
    {
      int j = this.first.position;
      for (int i = 0; i < this.elementCount; i++)
      {
        Element localElement = readElement(j);
        ElementInputStream localElementInputStream = new io/fabric/sdk/android/services/common/QueueFile$ElementInputStream;
        localElementInputStream.<init>(this, localElement, null);
        paramElementReader.read(localElementInputStream, localElement.length);
        j = wrapPosition(localElement.position + 4 + localElement.length);
      }
      return;
    }
    finally {}
  }
  
  public boolean hasSpaceFor(int paramInt1, int paramInt2)
  {
    if (usedBytes() + 4 + paramInt1 <= paramInt2) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  /* Error */
  public boolean isEmpty()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 144	io/fabric/sdk/android/services/common/QueueFile:elementCount	I
    //   6: istore_1
    //   7: iload_1
    //   8: ifne +9 -> 17
    //   11: iconst_1
    //   12: istore_2
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_2
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_2
    //   19: goto -6 -> 13
    //   22: astore_3
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_3
    //   26: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	27	0	this	QueueFile
    //   6	2	1	i	int
    //   12	7	2	bool	boolean
    //   22	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	22	finally
  }
  
  public void peek(ElementReader paramElementReader)
    throws IOException
  {
    try
    {
      if (this.elementCount > 0)
      {
        ElementInputStream localElementInputStream = new io/fabric/sdk/android/services/common/QueueFile$ElementInputStream;
        localElementInputStream.<init>(this, this.first, null);
        paramElementReader.read(localElementInputStream, this.first.length);
      }
      return;
    }
    finally
    {
      paramElementReader = finally;
      throw paramElementReader;
    }
  }
  
  /* Error */
  public byte[] peek()
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual 264	io/fabric/sdk/android/services/common/QueueFile:isEmpty	()Z
    //   6: istore_2
    //   7: iload_2
    //   8: ifeq +9 -> 17
    //   11: aconst_null
    //   12: astore_3
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_3
    //   16: areturn
    //   17: aload_0
    //   18: getfield 118	io/fabric/sdk/android/services/common/QueueFile:first	Lio/fabric/sdk/android/services/common/QueueFile$Element;
    //   21: getfield 116	io/fabric/sdk/android/services/common/QueueFile$Element:length	I
    //   24: istore_1
    //   25: iload_1
    //   26: newarray <illegal type>
    //   28: astore_3
    //   29: aload_0
    //   30: aload_0
    //   31: getfield 118	io/fabric/sdk/android/services/common/QueueFile:first	Lio/fabric/sdk/android/services/common/QueueFile$Element;
    //   34: getfield 113	io/fabric/sdk/android/services/common/QueueFile$Element:position	I
    //   37: iconst_4
    //   38: iadd
    //   39: aload_3
    //   40: iconst_0
    //   41: iload_1
    //   42: invokespecial 95	io/fabric/sdk/android/services/common/QueueFile:ringRead	(I[BII)V
    //   45: goto -32 -> 13
    //   48: astore_3
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_3
    //   52: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	53	0	this	QueueFile
    //   24	18	1	i	int
    //   6	2	2	bool	boolean
    //   12	28	3	arrayOfByte	byte[]
    //   48	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	7	48	finally
    //   17	45	48	finally
  }
  
  public void remove()
    throws IOException
  {
    try
    {
      if (isEmpty())
      {
        NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
        localNoSuchElementException.<init>();
        throw localNoSuchElementException;
      }
    }
    finally {}
    if (this.elementCount == 1) {
      clear();
    }
    for (;;)
    {
      return;
      int j = wrapPosition(this.first.position + 4 + this.first.length);
      ringRead(j, this.buffer, 0, 4);
      int i = readInt(this.buffer, 0);
      writeHeader(this.fileLength, this.elementCount - 1, j, this.last.position);
      this.elementCount -= 1;
      Element localElement = new io/fabric/sdk/android/services/common/QueueFile$Element;
      localElement.<init>(j, i);
      this.first = localElement;
    }
  }
  
  public int size()
  {
    try
    {
      int i = this.elementCount;
      return i;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName()).append('[');
    localStringBuilder.append("fileLength=").append(this.fileLength);
    localStringBuilder.append(", size=").append(this.elementCount);
    localStringBuilder.append(", first=").append(this.first);
    localStringBuilder.append(", last=").append(this.last);
    localStringBuilder.append(", element lengths=[");
    try
    {
      ElementReader local1 = new io/fabric/sdk/android/services/common/QueueFile$1;
      local1.<init>(this, localStringBuilder);
      forEach(local1);
      localStringBuilder.append("]]");
      return localStringBuilder.toString();
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        LOGGER.log(Level.WARNING, "read error", localIOException);
      }
    }
  }
  
  public int usedBytes()
  {
    int i;
    if (this.elementCount == 0) {
      i = 16;
    }
    for (;;)
    {
      return i;
      if (this.last.position >= this.first.position) {
        i = this.last.position - this.first.position + 4 + this.last.length + 16;
      } else {
        i = this.last.position + 4 + this.last.length + this.fileLength - this.first.position;
      }
    }
  }
  
  static class Element
  {
    static final int HEADER_LENGTH = 4;
    static final Element NULL = new Element(0, 0);
    final int length;
    final int position;
    
    Element(int paramInt1, int paramInt2)
    {
      this.position = paramInt1;
      this.length = paramInt2;
    }
    
    public String toString()
    {
      return getClass().getSimpleName() + "[position = " + this.position + ", length = " + this.length + "]";
    }
  }
  
  private final class ElementInputStream
    extends InputStream
  {
    private int position;
    private int remaining;
    
    private ElementInputStream(QueueFile.Element paramElement)
    {
      this.position = QueueFile.this.wrapPosition(paramElement.position + 4);
      this.remaining = paramElement.length;
    }
    
    public int read()
      throws IOException
    {
      int i;
      if (this.remaining == 0) {
        i = -1;
      }
      for (;;)
      {
        return i;
        QueueFile.this.raf.seek(this.position);
        i = QueueFile.this.raf.read();
        this.position = QueueFile.this.wrapPosition(this.position + 1);
        this.remaining -= 1;
      }
    }
    
    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
      throws IOException
    {
      QueueFile.nonNull(paramArrayOfByte, "buffer");
      if (((paramInt1 | paramInt2) < 0) || (paramInt2 > paramArrayOfByte.length - paramInt1)) {
        throw new ArrayIndexOutOfBoundsException();
      }
      int i;
      if (this.remaining > 0)
      {
        i = paramInt2;
        if (paramInt2 > this.remaining) {
          i = this.remaining;
        }
        QueueFile.this.ringRead(this.position, paramArrayOfByte, paramInt1, i);
        this.position = QueueFile.this.wrapPosition(this.position + i);
        this.remaining -= i;
      }
      for (;;)
      {
        return i;
        i = -1;
      }
    }
  }
  
  public static abstract interface ElementReader
  {
    public abstract void read(InputStream paramInputStream, int paramInt)
      throws IOException;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\common\QueueFile.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */