package org.joda.time.field;

import java.io.Serializable;
import java.util.HashMap;
import org.joda.time.DurationField;
import org.joda.time.DurationFieldType;

public final class UnsupportedDurationField
  extends DurationField
  implements Serializable
{
  private static HashMap<DurationFieldType, UnsupportedDurationField> cCache;
  private static final long serialVersionUID = -6390301302770925357L;
  private final DurationFieldType iType;
  
  private UnsupportedDurationField(DurationFieldType paramDurationFieldType)
  {
    this.iType = paramDurationFieldType;
  }
  
  /* Error */
  public static UnsupportedDurationField getInstance(DurationFieldType paramDurationFieldType)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 27	org/joda/time/field/UnsupportedDurationField:cCache	Ljava/util/HashMap;
    //   6: ifnonnull +48 -> 54
    //   9: new 29	java/util/HashMap
    //   12: astore_1
    //   13: aload_1
    //   14: bipush 7
    //   16: invokespecial 32	java/util/HashMap:<init>	(I)V
    //   19: aload_1
    //   20: putstatic 27	org/joda/time/field/UnsupportedDurationField:cCache	Ljava/util/HashMap;
    //   23: aconst_null
    //   24: astore_1
    //   25: aload_1
    //   26: astore_2
    //   27: aload_1
    //   28: ifnonnull +21 -> 49
    //   31: new 2	org/joda/time/field/UnsupportedDurationField
    //   34: astore_2
    //   35: aload_2
    //   36: aload_0
    //   37: invokespecial 34	org/joda/time/field/UnsupportedDurationField:<init>	(Lorg/joda/time/DurationFieldType;)V
    //   40: getstatic 27	org/joda/time/field/UnsupportedDurationField:cCache	Ljava/util/HashMap;
    //   43: aload_0
    //   44: aload_2
    //   45: invokevirtual 38	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   48: pop
    //   49: ldc 2
    //   51: monitorexit
    //   52: aload_2
    //   53: areturn
    //   54: getstatic 27	org/joda/time/field/UnsupportedDurationField:cCache	Ljava/util/HashMap;
    //   57: aload_0
    //   58: invokevirtual 42	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   61: checkcast 2	org/joda/time/field/UnsupportedDurationField
    //   64: astore_1
    //   65: goto -40 -> 25
    //   68: astore_0
    //   69: ldc 2
    //   71: monitorexit
    //   72: aload_0
    //   73: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	paramDurationFieldType	DurationFieldType
    //   12	53	1	localObject1	Object
    //   26	27	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   3	23	68	finally
    //   31	49	68	finally
    //   54	65	68	finally
  }
  
  private Object readResolve()
  {
    return getInstance(this.iType);
  }
  
  private UnsupportedOperationException unsupported()
  {
    return new UnsupportedOperationException(this.iType + " field is unsupported");
  }
  
  public long add(long paramLong, int paramInt)
  {
    throw unsupported();
  }
  
  public long add(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public int compareTo(DurationField paramDurationField)
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if ((paramObject instanceof UnsupportedDurationField))
      {
        paramObject = (UnsupportedDurationField)paramObject;
        if (((UnsupportedDurationField)paramObject).getName() == null)
        {
          if (getName() != null) {
            bool = false;
          }
        }
        else {
          bool = ((UnsupportedDurationField)paramObject).getName().equals(getName());
        }
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public int getDifference(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public long getDifferenceAsLong(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public long getMillis(int paramInt)
  {
    throw unsupported();
  }
  
  public long getMillis(int paramInt, long paramLong)
  {
    throw unsupported();
  }
  
  public long getMillis(long paramLong)
  {
    throw unsupported();
  }
  
  public long getMillis(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public String getName()
  {
    return this.iType.getName();
  }
  
  public final DurationFieldType getType()
  {
    return this.iType;
  }
  
  public long getUnitMillis()
  {
    return 0L;
  }
  
  public int getValue(long paramLong)
  {
    throw unsupported();
  }
  
  public int getValue(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public long getValueAsLong(long paramLong)
  {
    throw unsupported();
  }
  
  public long getValueAsLong(long paramLong1, long paramLong2)
  {
    throw unsupported();
  }
  
  public int hashCode()
  {
    return getName().hashCode();
  }
  
  public boolean isPrecise()
  {
    return true;
  }
  
  public boolean isSupported()
  {
    return false;
  }
  
  public String toString()
  {
    return "UnsupportedDurationField[" + getName() + ']';
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\field\UnsupportedDurationField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */