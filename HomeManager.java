package tech.dcube.companion.managers.server;

import android.content.Context;
import android.util.Log;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.backend.PBRequest.PBResponse;
import tech.dcube.companion.managers.server.backend.VolleySingleton;
import tech.dcube.companion.model.MeterNickname;
import tech.dcube.companion.model.MeterNicknameResponse;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBCommandStatus;
import tech.dcube.companion.model.PBDevice;
import tech.dcube.companion.model.PBDeviceNotification;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;

class HomeManager
{
  private static final HomeManager ourInstance = new HomeManager();
  private final DataManager dataManager = DataManager.getInstance();
  private Gson gson = new Gson();
  private Request reqAddFundsSendPro;
  private Request reqAddFundsSmartLink;
  private Request reqAddSubscriptionsSmartLink;
  private Request reqDelSubscriptionsSmartLink;
  private Request reqDevicesList;
  private Request reqGetCommandStatusSmartLink;
  private Request reqGetFundsSendPro;
  private Request reqGetNickname;
  private Request reqGetSubscriptionsSmartLink;
  private Request reqMeterWarning;
  private Request reqNotifications;
  private Request reqPostNickname;
  private Request reqPutNickname;
  
  public static HomeManager getInstance()
  {
    return ourInstance;
  }
  
  String addMeterSubscriptions(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_ADD_SUBSCRIPTIONS), new Object[] { paramString });
    Log.wtf(getClass().getName(), "URL: " + str);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", UUID.randomUUID().toString());
    this.reqAddSubscriptionsSmartLink = ServerManager.createRequest(2, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramAnonymousPBResponse.statusCode == 201) {}
        for (boolean bool = true;; bool = false)
        {
          paramAnonymousPBResponse = ((Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class)).get("type").toString();
          DataManager.getInstance().getMeterSubscriptions().put(paramAnonymousPBResponse, Boolean.valueOf(true));
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool), HomeManager.this.reqAddSubscriptionsSmartLink.getTag().toString());
          }
          return;
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Add Sub", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Add Sub", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.addMeterSubscriptions(HomeManager.26.this.val$context, HomeManager.26.this.val$subscriptionType, HomeManager.26.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Add Sub error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAddSubscriptionsSmartLink);
    return this.reqAddSubscriptionsSmartLink.getTag().toString();
  }
  
  String addSendProFunds(final Context paramContext, final Number paramNumber, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_ADD_FUNDS);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap2.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("fundingAmount", paramNumber);
    Log.wtf("SendPro Funds", this.gson.toJson(localHashMap1));
    this.reqAddFundsSendPro = ServerManager.createRequest(1, str, "application/json", localHashMap2, localHashMap1, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Boolean localBoolean = Boolean.valueOf(false);
        Map localMap = (Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        paramAnonymousPBResponse = localBoolean;
        if (localMap != null)
        {
          paramAnonymousPBResponse = localBoolean;
          if (localMap.get("prepayBalance") != null) {
            paramAnonymousPBResponse = Boolean.valueOf(true);
          }
        }
        try
        {
          double d = Double.valueOf(localMap.get("prepayBalance").toString()).doubleValue();
          HomeManager.this.dataManager.getUser().setSendProBalance(d);
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqAddFundsSendPro.getTag().toString());
          }
          return;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Log.e(getClass().getName(), localException.getLocalizedMessage());
          }
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Add SendPro Funds Status", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Add SendPro Funds Error", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.addSendProFunds(HomeManager.8.this.val$context, HomeManager.8.this.val$amount, HomeManager.8.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Add SendPro Funds Error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAddFundsSendPro);
    return this.reqAddFundsSendPro.getTag().toString();
  }
  
  String addSmartLinkFunds(final Context paramContext, final String paramString1, final String paramString2, final Number paramNumber, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler, final ServerManager.PollingHandler paramPollingHandler)
  {
    final String str2 = UUID.randomUUID().toString();
    String str1 = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_ADD_FUNDS), new Object[] { paramString1, paramString2, str2 });
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("Accept", "application/json");
    localHashMap1.put("X-PB-TransactionId", str2);
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    HashMap localHashMap3 = new HashMap();
    localHashMap3.put("refillAmount", paramNumber);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("commandName", "refill");
    localHashMap2.put("commandParams", localHashMap3);
    Log.wtf("SmartLink Body", this.gson.toJson(localHashMap2));
    this.reqAddFundsSmartLink = ServerManager.createRequest(1, str1, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Log.wtf("Success", paramAnonymousPBResponse.body);
        Boolean localBoolean = Boolean.valueOf(false);
        if (paramAnonymousPBResponse.statusCode == 202) {
          localBoolean = Boolean.valueOf(true);
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(localBoolean, HomeManager.this.reqAddFundsSmartLink.getTag().toString());
        }
        HomeManager.ourInstance.getCommandStatus(paramContext, str2, paramString1, paramString2, paramPollingHandler);
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        Log.wtf("Error", new String(paramAnonymousVolleyError.networkResponse.data));
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.addSmartLinkFunds(HomeManager.2.this.val$context, HomeManager.2.this.val$model, HomeManager.2.this.val$serial, HomeManager.2.this.val$amount, HomeManager.2.this.val$completionHandler, HomeManager.2.this.val$pollingHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAddFundsSmartLink);
    return this.reqAddFundsSmartLink.getTag().toString();
  }
  
  String getCommandStatus(final Context paramContext, final String paramString1, final String paramString2, final String paramString3, final ServerManager.PollingHandler paramPollingHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_GET_COMMAND_STATUS), new Object[] { paramString2, paramString3, paramString1 });
    HashMap localHashMap = new HashMap();
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", paramString1);
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    this.reqGetCommandStatusSmartLink = ServerManager.createRequest(0, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Map localMap = (Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        if ((localMap != null) && (localMap.get("successful") != null))
        {
          paramAnonymousPBResponse = (PBCommandStatus)PBCommandStatus.fromJson(PBCommandStatus.class, paramAnonymousPBResponse.body);
          if (paramPollingHandler != null)
          {
            Log.wtf("Command Status Response", paramAnonymousPBResponse.toString());
            paramPollingHandler.onMeterRefillPollingComplete(paramString2, paramString3, paramAnonymousPBResponse);
          }
        }
        for (;;)
        {
          return;
          Log.wtf("Command Status Error", "Trying Again!!!!!!!!!!!!!!!!!!!");
          HomeManager.ourInstance.getCommandStatus(paramContext, paramString1, paramString2, paramString3, paramPollingHandler);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if ((paramAnonymousVolleyError != null) && (paramAnonymousVolleyError.networkResponse != null) && (paramAnonymousVolleyError.networkResponse.statusCode != 401))
        {
          Log.wtf("Command Status Error", paramAnonymousVolleyError);
          HomeManager.ourInstance.getCommandStatus(paramContext, paramString1, paramString2, paramString3, paramPollingHandler);
        }
        for (;;)
        {
          return;
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, null, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.getCommandStatus(HomeManager.4.this.val$context, HomeManager.4.this.val$commandId, HomeManager.4.this.val$model, HomeManager.4.this.val$serial, HomeManager.4.this.val$pollingHandler);
            }
          });
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetCommandStatusSmartLink);
    return this.reqGetCommandStatusSmartLink.getTag().toString();
  }
  
  String getMeterNickname(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<MeterNickname> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_GET_NICKNAME) + paramString;
    HashMap localHashMap = new HashMap();
    localHashMap.put("Accept", "application/json");
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqGetNickname = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (MeterNicknameResponse)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, MeterNicknameResponse.class);
        if (paramAnonymousPBResponse != null) {
          if ((paramAnonymousPBResponse.getMessage().equalsIgnoreCase("success")) && (paramRequestCompletionHandler != null)) {
            paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse.getResults().get(0), HomeManager.this.reqGetNickname.getTag().toString());
          }
        }
        for (;;)
        {
          return;
          paramAnonymousPBResponse = new VolleyError("Nickname not found");
          paramRequestCompletionHandler.onError(paramAnonymousPBResponse, HomeManager.this.reqGetNickname.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Get NickName Status", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Get NickName Error", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.getMeterNickname(HomeManager.14.this.val$context, HomeManager.14.this.val$serialNumber, HomeManager.14.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Get NickName Error Error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetNickname);
    return this.reqGetNickname.getTag().toString();
  }
  
  String getMeterSubscriptions(final Context paramContext, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_GET_SUBSCRIPTIONS), new Object[0]);
    Log.wtf(getClass().getName(), "URL: " + str);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", UUID.randomUUID().toString());
    this.reqGetSubscriptionsSmartLink = ServerManager.createRequest(0, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        int i = 0;
        boolean bool;
        Map localMap;
        if (paramAnonymousPBResponse.statusCode == 200)
        {
          bool = true;
          Map[] arrayOfMap = (Map[])HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map[].class);
          localMap = DataManager.getInstance().getMeterSubscriptions();
          localMap.put("SUPPLY", Boolean.valueOf(false));
          localMap.put("FUNDS", Boolean.valueOf(false));
          localMap.put("DEVICE", Boolean.valueOf(false));
          int j = arrayOfMap.length;
          label90:
          if (i >= j) {
            break label205;
          }
          paramAnonymousPBResponse = arrayOfMap[i];
          if (!paramAnonymousPBResponse.get("type").equals("SUPPLY")) {
            break label172;
          }
          localMap.put("SUPPLY", Boolean.valueOf(true));
        }
        for (;;)
        {
          if (paramAnonymousPBResponse.get("type").equals("DEVICE")) {
            localMap.put("DEVICE", Boolean.valueOf(true));
          }
          i++;
          break label90;
          bool = false;
          break;
          label172:
          if (paramAnonymousPBResponse.get("type").equals("FUNDS")) {
            localMap.put("FUNDS", Boolean.valueOf(true));
          }
        }
        label205:
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool), HomeManager.this.reqGetSubscriptionsSmartLink.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.getMeterSubscriptions(HomeManager.24.this.val$context, HomeManager.24.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetSubscriptionsSmartLink);
    return this.reqGetSubscriptionsSmartLink.getTag().toString();
  }
  
  String getNotifications(final Context paramContext, final ServerManager.RequestCompletionHandler<List<PBDeviceNotification>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_GET_NOTIFICATIONS);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", UUID.randomUUID().toString());
    localHashMap.put("X-UserId", this.dataManager.getUser().getUserId());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    this.reqNotifications = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        HomeManager.this.dataManager.getNotifications().clear();
        paramAnonymousPBResponse = PBDeviceNotification.fromJsonArray(PBDeviceNotification[].class, paramAnonymousPBResponse.body);
        if (paramAnonymousPBResponse != null) {
          HomeManager.this.dataManager.getNotifications().addAll(paramAnonymousPBResponse);
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqNotifications.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Notifications Status", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Notifications Error", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.getNotifications(HomeManager.16.this.val$context, HomeManager.16.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Notifications Error Error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqNotifications);
    return this.reqNotifications.getTag().toString();
  }
  
  String getSendProFunds(final Context paramContext, final ServerManager.RequestCompletionHandler<Double> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_GET_FUNDS);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqGetFundsSendPro = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Boolean localBoolean = Boolean.valueOf(false);
        Map localMap = (Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        double d2 = 0.0D;
        double d1 = d2;
        paramAnonymousPBResponse = localBoolean;
        if (localMap != null)
        {
          d1 = d2;
          paramAnonymousPBResponse = localBoolean;
          if (localMap.get("prepayBalance") != null)
          {
            paramAnonymousPBResponse = Boolean.valueOf(true);
            d1 = d2;
          }
        }
        try
        {
          d2 = Double.valueOf(localMap.get("prepayBalance").toString()).doubleValue();
          d1 = d2;
          HomeManager.this.dataManager.getUser().setSendProBalance(d2);
          d1 = d2;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Log.e(getClass().getName(), localException.getLocalizedMessage());
            continue;
            ServerManager.sendError(HomeManager.this.reqGetFundsSendPro.getTag().toString(), "Failed to get SendPro Funds", paramRequestCompletionHandler);
          }
        }
        if (paramRequestCompletionHandler != null)
        {
          if (paramAnonymousPBResponse.booleanValue()) {
            paramRequestCompletionHandler.onSuccess(Double.valueOf(d1), HomeManager.this.reqGetFundsSendPro.getTag().toString());
          }
        }
        else {
          return;
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Get SendPro Funds Status", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Get SendPro Funds Error", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.getSendProFunds(HomeManager.6.this.val$context, HomeManager.6.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Get SendPro Funds Error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqGetFundsSendPro);
    return this.reqGetFundsSendPro.getTag().toString();
  }
  
  String getUserAddress(final Context paramContext, final ServerManager.RequestCompletionHandler<PBAddress> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_USER_ADDRESS);
    HashMap localHashMap = new HashMap();
    localHashMap.put("authToken", this.dataManager.getUser().getAuth().getAuthTokenSendPro());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    this.reqDevicesList = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        paramAnonymousPBResponse = (PBAddress)PBAddress.fromJson(PBAddress.class, paramAnonymousPBResponse.body);
        HomeManager.this.dataManager.getUser().setAddress(paramAnonymousPBResponse);
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqDevicesList.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.getUserAddress(HomeManager.20.this.val$context, HomeManager.20.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqDevicesList);
    return this.reqDevicesList.getTag().toString();
  }
  
  String listDevices(final Context paramContext, final ServerManager.RequestCompletionHandler<List<PBDevice>> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_GET_DEVICES);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", UUID.randomUUID().toString());
    localHashMap.put("X-UserId", this.dataManager.getUser().getUserId());
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    this.reqDevicesList = ServerManager.createRequest(0, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        HomeManager.this.dataManager.getDevices().clear();
        paramAnonymousPBResponse = PBDevice.fromJsonArray(PBDevice[].class, paramAnonymousPBResponse.body);
        if (paramAnonymousPBResponse != null) {
          HomeManager.this.dataManager.getDevices().addAll(paramAnonymousPBResponse);
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqDevicesList.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.listDevices(HomeManager.18.this.val$context, HomeManager.18.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqDevicesList);
    return this.reqDevicesList.getTag().toString();
  }
  
  String postMeterNickname(final Context paramContext, final String paramString1, final String paramString2, final String paramString3, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_PUT_NICKNAME);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("serial_number", paramString2);
    localHashMap1.put("nickname", paramString1);
    localHashMap1.put("model", paramString3);
    this.reqPostNickname = ServerManager.createRequest(1, str, "application/json", localHashMap2, localHashMap1, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Map localMap = (Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        Boolean localBoolean = Boolean.valueOf(false);
        paramAnonymousPBResponse = localBoolean;
        if (localMap != null)
        {
          paramAnonymousPBResponse = localBoolean;
          if (localMap.get("message").equals("Success")) {
            paramAnonymousPBResponse = Boolean.valueOf(true);
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqPostNickname.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.postMeterNickname(HomeManager.10.this.val$context, HomeManager.10.this.val$nickname, HomeManager.10.this.val$serialNumber, HomeManager.10.this.val$model, HomeManager.10.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqPostNickname);
    return this.reqPostNickname.getTag().toString();
  }
  
  String putMeterNickname(final Context paramContext, final String paramString1, final String paramString2, final String paramString3, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_PUT_NICKNAME) + paramString2;
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("nickname", paramString1);
    localHashMap2.put("model", paramString3);
    this.reqPutNickname = ServerManager.createRequest(2, str, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        Map localMap = (Map)HomeManager.this.gson.fromJson(paramAnonymousPBResponse.body, Map.class);
        Boolean localBoolean = Boolean.valueOf(false);
        paramAnonymousPBResponse = localBoolean;
        if (localMap != null)
        {
          paramAnonymousPBResponse = localBoolean;
          if (localMap.get("message").equals("Success")) {
            paramAnonymousPBResponse = Boolean.valueOf(true);
          }
        }
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onSuccess(paramAnonymousPBResponse, HomeManager.this.reqPutNickname.getTag().toString());
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.putMeterNickname(HomeManager.12.this.val$context, HomeManager.12.this.val$nickname, HomeManager.12.this.val$serialNumber, HomeManager.12.this.val$model, HomeManager.12.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqPutNickname);
    return this.reqPutNickname.getTag().toString();
  }
  
  String removeMeterSubscriptions(final Context paramContext, final String paramString, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_DEL_SUBSCRIPTIONS), new Object[] { paramString });
    Log.wtf(getClass().getName(), "URL: " + str);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSmartLink() }));
    localHashMap.put("Accept", "application/json");
    localHashMap.put("X-PB-TransactionId", UUID.randomUUID().toString());
    this.reqDelSubscriptionsSmartLink = ServerManager.createRequest(3, str, "application/json", localHashMap, new HashMap(), new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramAnonymousPBResponse.statusCode == 204) {}
        for (boolean bool = true;; bool = false)
        {
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool), HomeManager.this.reqDelSubscriptionsSmartLink.getTag().toString());
          }
          return;
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramAnonymousVolleyError.networkResponse != null)
        {
          Log.wtf("Remove Sub", paramAnonymousVolleyError.networkResponse.statusCode + "");
          Log.wtf("Remove Sub", new String(paramAnonymousVolleyError.networkResponse.data));
        }
        for (;;)
        {
          ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
          {
            public void onCompletion()
            {
              HomeManager.ourInstance.removeMeterSubscriptions(HomeManager.28.this.val$context, HomeManager.28.this.val$subscriptionType, HomeManager.28.this.val$completionHandler);
            }
          });
          return;
          Log.wtf("Remove Sub error", paramAnonymousVolleyError.toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqDelSubscriptionsSmartLink);
    return this.reqDelSubscriptionsSmartLink.getTag().toString();
  }
  
  String updateLowFundsWarning(final Context paramContext, final String paramString1, final String paramString2, final String paramString3, final ServerManager.RequestCompletionHandler<Boolean> paramRequestCompletionHandler)
  {
    String str = String.format((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_LOW_FUNDS_WARNING), new Object[] { paramString1, paramString2, UUID.randomUUID().toString() });
    Log.wtf(getClass().getName(), "URL: " + str);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("Authorization", String.format("Bearer %s", new Object[] { this.dataManager.getUser().getAuth().getAccessTokenSendPro() }));
    localHashMap1.put("Accept", "application/json");
    localHashMap1.put("X-PB-TransactionId", UUID.randomUUID().toString());
    HashMap localHashMap3 = new HashMap();
    localHashMap3.put("lowFundWarningLevel", paramString3);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("commandParams", this.gson.toJson(localHashMap3));
    localHashMap2.put("commandName", "setLowFundWarning");
    this.reqMeterWarning = ServerManager.createRequest(1, str, "application/json", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramAnonymousPBResponse.statusCode == 202) {}
        for (boolean bool = true;; bool = false)
        {
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onSuccess(Boolean.valueOf(bool), HomeManager.this.reqMeterWarning.getTag().toString());
          }
          return;
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        ServerManager.handleAuth(paramContext, paramAnonymousVolleyError, paramRequestCompletionHandler, new ServerManager.AuthCompletion()
        {
          public void onCompletion()
          {
            HomeManager.ourInstance.updateLowFundsWarning(HomeManager.22.this.val$context, HomeManager.22.this.val$model, HomeManager.22.this.val$serial, HomeManager.22.this.val$amount, HomeManager.22.this.val$completionHandler);
          }
        });
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqMeterWarning);
    return this.reqMeterWarning.getTag().toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\HomeManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */