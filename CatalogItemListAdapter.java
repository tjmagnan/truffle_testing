package tech.dcube.companion.customListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import tech.dcube.companion.model.PBSendProItem;

public class CatalogItemListAdapter
  extends ArrayAdapter<PBSendProItem>
{
  private List<PBSendProItem> dataSet;
  private int lastPosition = -1;
  Context mContext;
  
  public CatalogItemListAdapter(List<PBSendProItem> paramList, Context paramContext)
  {
    super(paramContext, 2130968651, paramList);
    this.dataSet = paramList;
    this.mContext = paramContext;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    PBSendProItem localPBSendProItem = (PBSendProItem)getItem(paramInt);
    Object localObject2;
    Object localObject1;
    if (paramView == null)
    {
      localObject2 = new ViewHolder(null);
      paramView = LayoutInflater.from(getContext()).inflate(2130968651, paramViewGroup, false);
      ((ViewHolder)localObject2).ProductName = ((TextView)paramView.findViewById(2131624251));
      ((ViewHolder)localObject2).ProductDims = ((TextView)paramView.findViewById(2131624252));
      localObject1 = paramView;
      paramView.setTag(localObject2);
      paramViewGroup = (ViewGroup)localObject2;
      localObject2 = this.mContext;
      if (paramInt <= this.lastPosition) {
        break label217;
      }
    }
    label217:
    for (int i = 2131034132;; i = 2131034129)
    {
      ((View)localObject1).startAnimation(AnimationUtils.loadAnimation((Context)localObject2, i));
      this.lastPosition = paramInt;
      localObject1 = localPBSendProItem.getItemName();
      double d3 = localPBSendProItem.getItemLengthIn();
      double d2 = localPBSendProItem.getItemWidthIn();
      double d1 = localPBSendProItem.getItemHeightIn();
      localPBSendProItem.getItemWeightLb();
      localObject2 = String.format("%.01f\" x %.01f\" x %.01f\"", new Object[] { Double.valueOf(d3), Double.valueOf(d2), Double.valueOf(d1) });
      paramViewGroup.ProductName.setText((CharSequence)localObject1);
      paramViewGroup.ProductDims.setText((CharSequence)localObject2);
      return paramView;
      paramViewGroup = (ViewHolder)paramView.getTag();
      localObject1 = paramView;
      break;
    }
  }
  
  public void refresh()
  {
    notifyDataSetChanged();
  }
  
  public void updateData(List<PBSendProItem> paramList)
  {
    this.dataSet.clear();
    this.dataSet.addAll(paramList);
    notifyDataSetChanged();
  }
  
  private static class ViewHolder
  {
    TextView OrderStatus;
    TextView ProductDims;
    TextView ProductName;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\customListAdapters\CatalogItemListAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */