package com.testfairy.l;

import java.nio.Buffer;
import javax.microedition.khronos.opengles.GL10;

public class a
  extends b
{
  private a a;
  
  public a(GL10 paramGL10, a parama)
  {
    super(paramGL10);
    this.a = parama;
  }
  
  public void glDrawArrays(int paramInt1, int paramInt2, int paramInt3)
  {
    super.glDrawArrays(paramInt1, paramInt2, paramInt3);
    a locala = this.a;
    locala.a += 1;
    if (paramInt1 == 4)
    {
      locala = this.a;
      locala.b += paramInt3 / 3;
    }
  }
  
  public void glDrawElements(int paramInt1, int paramInt2, int paramInt3, Buffer paramBuffer)
  {
    super.glDrawElements(paramInt1, paramInt2, paramInt3, paramBuffer);
    paramBuffer = this.a;
    paramBuffer.a += 1;
    if (paramInt1 == 4)
    {
      paramBuffer = this.a;
      paramBuffer.b += paramInt2 / 3;
    }
  }
  
  public static class a
  {
    public int a;
    public int b;
    
    public void a()
    {
      this.a = 0;
      this.b = 0;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\l\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */