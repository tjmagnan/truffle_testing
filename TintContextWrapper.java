package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class TintContextWrapper
  extends ContextWrapper
{
  private static final Object CACHE_LOCK = new Object();
  private static ArrayList<WeakReference<TintContextWrapper>> sCache;
  private final Resources mResources;
  private final Resources.Theme mTheme;
  
  private TintContextWrapper(@NonNull Context paramContext)
  {
    super(paramContext);
    if (VectorEnabledTintResources.shouldBeUsed())
    {
      this.mResources = new VectorEnabledTintResources(this, paramContext.getResources());
      this.mTheme = this.mResources.newTheme();
      this.mTheme.setTo(paramContext.getTheme());
    }
    for (;;)
    {
      return;
      this.mResources = new TintResources(this, paramContext.getResources());
      this.mTheme = null;
    }
  }
  
  private static boolean shouldWrap(@NonNull Context paramContext)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (!(paramContext instanceof TintContextWrapper))
    {
      bool1 = bool2;
      if (!(paramContext.getResources() instanceof TintResources))
      {
        if (!(paramContext.getResources() instanceof VectorEnabledTintResources)) {
          break label37;
        }
        bool1 = bool2;
      }
    }
    for (;;)
    {
      return bool1;
      label37:
      if (Build.VERSION.SDK_INT >= 21)
      {
        bool1 = bool2;
        if (!VectorEnabledTintResources.shouldBeUsed()) {}
      }
      else
      {
        bool1 = true;
      }
    }
  }
  
  public static Context wrap(@NonNull Context paramContext)
  {
    if (shouldWrap(paramContext)) {}
    for (;;)
    {
      int i;
      synchronized (CACHE_LOCK)
      {
        if (sCache == null)
        {
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          sCache = (ArrayList)localObject1;
          localObject1 = new android/support/v7/widget/TintContextWrapper;
          ((TintContextWrapper)localObject1).<init>(paramContext);
          ArrayList localArrayList = sCache;
          paramContext = new java/lang/ref/WeakReference;
          paramContext.<init>(localObject1);
          localArrayList.add(paramContext);
          return (Context)localObject1;
        }
        i = sCache.size() - 1;
        if (i >= 0)
        {
          localObject1 = (WeakReference)sCache.get(i);
          if ((localObject1 == null) || (((WeakReference)localObject1).get() == null)) {
            sCache.remove(i);
          }
          i--;
          continue;
        }
        i = sCache.size() - 1;
        if (i < 0) {
          continue;
        }
        localObject1 = (WeakReference)sCache.get(i);
        if (localObject1 != null)
        {
          localObject1 = (TintContextWrapper)((WeakReference)localObject1).get();
          if ((localObject1 == null) || (((TintContextWrapper)localObject1).getBaseContext() != paramContext)) {
            break label177;
          }
        }
      }
      Object localObject1 = null;
      continue;
      label177:
      i--;
      continue;
      localObject1 = paramContext;
    }
  }
  
  public AssetManager getAssets()
  {
    return this.mResources.getAssets();
  }
  
  public Resources getResources()
  {
    return this.mResources;
  }
  
  public Resources.Theme getTheme()
  {
    if (this.mTheme == null) {}
    for (Resources.Theme localTheme = super.getTheme();; localTheme = this.mTheme) {
      return localTheme;
    }
  }
  
  public void setTheme(int paramInt)
  {
    if (this.mTheme == null) {
      super.setTheme(paramInt);
    }
    for (;;)
    {
      return;
      this.mTheme.applyStyle(paramInt, true);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\TintContextWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */