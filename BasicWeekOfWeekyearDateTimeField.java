package org.joda.time.chrono;

import org.joda.time.DateTimeFieldType;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.PreciseDurationDateTimeField;

final class BasicWeekOfWeekyearDateTimeField
  extends PreciseDurationDateTimeField
{
  private static final long serialVersionUID = -1587436826395135328L;
  private final BasicChronology iChronology;
  
  BasicWeekOfWeekyearDateTimeField(BasicChronology paramBasicChronology, DurationField paramDurationField)
  {
    super(DateTimeFieldType.weekOfWeekyear(), paramDurationField);
    this.iChronology = paramBasicChronology;
  }
  
  private Object readResolve()
  {
    return this.iChronology.weekOfWeekyear();
  }
  
  public int get(long paramLong)
  {
    return this.iChronology.getWeekOfWeekyear(paramLong);
  }
  
  public int getMaximumValue()
  {
    return 53;
  }
  
  public int getMaximumValue(long paramLong)
  {
    int i = this.iChronology.getWeekyear(paramLong);
    return this.iChronology.getWeeksInYear(i);
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial.isSupported(DateTimeFieldType.weekyear())) {
      i = paramReadablePartial.get(DateTimeFieldType.weekyear());
    }
    for (int i = this.iChronology.getWeeksInYear(i);; i = 53) {
      return i;
    }
  }
  
  public int getMaximumValue(ReadablePartial paramReadablePartial, int[] paramArrayOfInt)
  {
    int j = paramReadablePartial.size();
    int i = 0;
    if (i < j) {
      if (paramReadablePartial.getFieldType(i) == DateTimeFieldType.weekyear()) {
        i = paramArrayOfInt[i];
      }
    }
    for (i = this.iChronology.getWeeksInYear(i);; i = 53)
    {
      return i;
      i++;
      break;
    }
  }
  
  protected int getMaximumValueForSet(long paramLong, int paramInt)
  {
    int i = 52;
    if (paramInt > 52) {
      i = getMaximumValue(paramLong);
    }
    return i;
  }
  
  public int getMinimumValue()
  {
    return 1;
  }
  
  public DurationField getRangeDurationField()
  {
    return this.iChronology.weekyears();
  }
  
  public long remainder(long paramLong)
  {
    return super.remainder(259200000L + paramLong);
  }
  
  public long roundCeiling(long paramLong)
  {
    return super.roundCeiling(paramLong + 259200000L) - 259200000L;
  }
  
  public long roundFloor(long paramLong)
  {
    return super.roundFloor(paramLong + 259200000L) - 259200000L;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BasicWeekOfWeekyearDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */