package com.testfairy;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import com.testfairy.d.q;
import com.testfairy.d.q.b;
import com.testfairy.n.m;
import com.testfairy.p.p;
import com.testfairy.p.s;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class o
  implements com.testfairy.f.b
{
  public static final String a = "TS_DISCARD_TEST";
  static final int k = 0;
  static final int l = 1;
  static final int m = 2;
  static final int n = 3;
  static final int o = 4;
  private static final String p = "testfairy-secure-viewid";
  private static final String q = "testfairy-checkpoint";
  private static final String r = "libc";
  private static final String s = "dalvikvm";
  private static final String t = "memmalloc";
  private static final String u = "NativeCrypto";
  private static final String v = "https://api.testfairy.com/services/";
  private com.testfairy.f.e A;
  private WeakHashMap B = new WeakHashMap();
  private String C;
  private Context D;
  private d E;
  private com.testfairy.n.j F;
  private com.testfairy.n.h G;
  private q H;
  private com.testfairy.n.l I;
  private boolean J = false;
  private a K = new a(new a.a()
  {
    public void a(int paramAnonymousInt)
    {
      if (!o.a(o.this).f()) {
        o.a(o.this, paramAnonymousInt);
      }
    }
  });
  private TimerTask L;
  private Timer M;
  private List N = new ArrayList();
  private long O;
  private i P = new i();
  private com.testfairy.e.a Q;
  private String R = null;
  private boolean S = false;
  private boolean T = false;
  private boolean U = true;
  private TestFairy.LogEventFilter V = null;
  private FeedbackOptions W;
  private final com.testfairy.n.g X = new com.testfairy.n.g();
  private final com.testfairy.a.a Y = new com.testfairy.a.a()
  {
    public boolean a()
    {
      return com.testfairy.f.d.a();
    }
    
    public j b()
    {
      return o.this.g;
    }
    
    public i c()
    {
      return o.i(o.this);
    }
    
    public String d()
    {
      Object localObject = o.j(o.this).getFilesDir();
      if (localObject == null) {}
      for (localObject = null;; localObject = ((File)localObject).getAbsolutePath()) {
        return (String)localObject;
      }
    }
    
    public JSONObject e()
    {
      return o.k(o.this);
    }
    
    public boolean f()
    {
      boolean bool = false;
      if (o.i(o.this).a() == null) {}
      for (;;)
      {
        return bool;
        if ((o.l(o.this).x() == -1L) || (System.currentTimeMillis() <= o.m(o.this) + o.l(o.this).x())) {
          bool = true;
        }
      }
    }
    
    public long g()
    {
      return o.m(o.this);
    }
    
    public String h()
    {
      return c().e();
    }
    
    public String i()
    {
      return o.this.f.a();
    }
    
    public String j()
    {
      return o.n(o.this);
    }
    
    public String k()
    {
      return o.o(o.this);
    }
  };
  private com.testfairy.h.c Z = new com.testfairy.h.c()
  {
    public void a()
    {
      o.a(o.this, true);
    }
    
    public void a(String paramAnonymousString)
    {
      o.this.e(paramAnonymousString);
    }
    
    public void a(Throwable paramAnonymousThrowable, String paramAnonymousString)
    {
      o.g("Could not connect to server endpoint " + o.o(o.this) + ", setting event queue to offline mode");
      o.b(o.this, 1);
      o.this.c.d();
    }
  };
  private c.a aa = new c.a()
  {
    public void a(Map paramAnonymousMap)
    {
      paramAnonymousMap = new g(7, paramAnonymousMap);
      o.this.c.a(paramAnonymousMap);
    }
  };
  com.testfairy.k.d b = this.y.a(this.w);
  com.testfairy.m.b c = new com.testfairy.m.b();
  com.testfairy.j.c d;
  final com.testfairy.j.b e = new f();
  l f = new l(new k()
  {
    public void a(String paramAnonymousString)
    {
      o.this.a();
    }
    
    public void a(String paramAnonymousString1, String paramAnonymousString2)
    {
      o.a(o.this, 20, paramAnonymousString1, paramAnonymousString2);
    }
    
    public void a(HashMap paramAnonymousHashMap)
    {
      Iterator localIterator = paramAnonymousHashMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        paramAnonymousHashMap = (Map.Entry)localIterator.next();
        o.a(o.this, 20, (String)paramAnonymousHashMap.getKey(), paramAnonymousHashMap.getValue());
      }
    }
  });
  j g;
  h h;
  public int i = 180000000;
  com.testfairy.a.c j = new com.testfairy.a.c()
  {
    public void a()
    {
      o.b(o.this, 4);
      o.this.c.a();
      o.b(o.this);
      o.g("backgroundTimer started " + o.this.i + " (ms)");
      o.a(o.this, new Timer("testfairy-background-timer"));
      o.a(o.this, new TimerTask()
      {
        public void run()
        {
          if (o.a(o.this).f()) {
            o.b(o.this, 17);
          }
          o.c(o.this);
          if (o.d(o.this) != null) {
            o.d(o.this).cancel();
          }
        }
      });
      o.d(o.this).schedule(o.e(o.this), o.this.i);
    }
    
    public void a(Activity paramAnonymousActivity)
    {
      o.a(o.this, paramAnonymousActivity);
      if (o.this.h != null) {
        o.this.h.b();
      }
      if (o.h(o.this).b()) {
        o.h(o.this).a();
      }
    }
    
    public void a(Context paramAnonymousContext)
    {
      o.f(o.this);
      o.g(o.this);
      if (o.this.c != null) {
        o.b(o.this, 5);
      }
    }
    
    public void a(String paramAnonymousString)
    {
      o.this.a(paramAnonymousString);
    }
    
    public void b(Activity paramAnonymousActivity)
    {
      o.b(o.this, paramAnonymousActivity);
      if (o.this.h != null) {
        o.this.h.c();
      }
      if (o.h(o.this).b())
      {
        o.g("Recreating NewVersionDialog onActivityResumed");
        o.h(o.this).a(paramAnonymousActivity);
      }
    }
  };
  private String w = "https://api.testfairy.com/services/";
  private com.testfairy.a.b x;
  private com.testfairy.k.e y = new com.testfairy.k.e();
  private String z;
  
  private void A()
  {
    i("Installing hooks");
    Timer localTimer = this.P.f();
    if (localTimer == null)
    {
      i("Can't install hooks, there is no valid session");
      return;
    }
    Object localObject;
    if (this.Q.a())
    {
      if (D())
      {
        localObject = new com.testfairy.j.a(this.e);
        label53:
        this.d = ((com.testfairy.j.c)localObject);
        this.d.start();
      }
    }
    else
    {
      if (this.Q.b())
      {
        localObject = new com.testfairy.n.d(this.c);
        this.I.a((com.testfairy.n.b)localObject);
      }
      if (this.Q.f())
      {
        this.F = new com.testfairy.n.j(this.c);
        this.I.a(this.F);
      }
      if (this.Q.g())
      {
        localObject = new com.testfairy.n.i(this.c, d.f(this.E));
        this.I.a((com.testfairy.n.b)localObject);
      }
      if (this.Q.j())
      {
        if (!c(this.D, "android.permission.READ_PHONE_STATE")) {
          break label590;
        }
        localObject = new com.testfairy.n.k(this.c, d.g(this.E));
        this.I.a((com.testfairy.n.b)localObject);
      }
      label227:
      if (!this.Q.n()) {
        break label599;
      }
      m();
      label243:
      localObject = new m(this.c, d.g(this.E));
      this.I.a((com.testfairy.n.b)localObject);
      if (this.Q.d())
      {
        if (!c(this.D, "android.permission.ACCESS_WIFI_STATE")) {
          break label611;
        }
        localObject = new com.testfairy.n.n(this.c, com.testfairy.k.b.c());
        this.I.a((com.testfairy.n.b)localObject);
      }
    }
    for (;;)
    {
      if (this.Q.y())
      {
        localObject = new com.testfairy.n.a(this.c, this.Y);
        this.I.a((com.testfairy.n.b)localObject);
      }
      this.I.a(new com.testfairy.n.e(this.c));
      this.I.a(new com.testfairy.n.c(this.c, d.b(this.E)));
      localTimer.schedule(this.I, 0L, 1000L);
      localTimer.schedule(new b(null), 0L, 1000L);
      localTimer.schedule(new c(null), 0L, 60000L);
      localTimer.schedule(new a(null), 5000L);
      d(this.D);
      if ((this.Q != null) && (this.Q.k()))
      {
        this.H = new q(this.c, this.Q, this.Y, this.x, new q.b()
        {
          public void a()
          {
            o.q(o.this);
          }
          
          public void a(int paramAnonymousInt)
          {
            if (o.r(o.this) != null) {
              o.r(o.this).a(0L, paramAnonymousInt);
            }
            o.s(o.this);
          }
          
          public void a(long paramAnonymousLong)
          {
            Log.d(e.a, "Screenshot took " + paramAnonymousLong + " Millis, ScreenshotTask is aborted!");
            o.t(o.this).cancel();
            o.b(o.this, 11);
          }
        });
        localTimer.schedule(this.H, 0L, 250L);
      }
      if (this.Q.x() == -1L) {
        break;
      }
      localTimer.schedule(new e(null), this.Q.x());
      break;
      localObject = new com.testfairy.j.d(this.e);
      break label53;
      label590:
      b(10);
      break label227;
      label599:
      n();
      this.h = null;
      break label243;
      label611:
      b(9);
    }
  }
  
  private void B()
  {
    if ((this.Y.f()) && (!this.P.c()))
    {
      this.P.a(true);
      Object localObject = new HashMap(2);
      ((Map)localObject).put("data", "true");
      localObject = new g(15, (Map)localObject);
      this.c.a((g)localObject);
      Log.d(e.a, "Data network is available again");
      this.c.a();
    }
  }
  
  private void C()
  {
    if ((this.Y.f()) && (this.P.c()))
    {
      this.P.a(false);
      Object localObject = new HashMap(2);
      ((Map)localObject).put("data", "false");
      localObject = new g(15, (Map)localObject);
      this.c.a((g)localObject);
      Log.d(e.a, "Data network is unavailable");
    }
  }
  
  private static boolean D()
  {
    return new File("/dev/log/main").exists();
  }
  
  private void E()
  {
    if (this.h != null) {
      this.h.a();
    }
  }
  
  private String a(Context paramContext)
  {
    if (this.g != null) {}
    for (paramContext = this.g.a();; paramContext = paramContext.getApplicationContext().getSharedPreferences("testfairy.preferences", 0).getString("options", null)) {
      return paramContext;
    }
  }
  
  private Map a(Map paramMap)
  {
    HashMap localHashMap = new HashMap();
    paramMap = paramMap.entrySet().iterator();
    while (paramMap.hasNext())
    {
      Object localObject2 = (Map.Entry)paramMap.next();
      Object localObject1 = ((Map.Entry)localObject2).getKey();
      localObject2 = ((Map.Entry)localObject2).getValue();
      if (((localObject1 instanceof String)) && (((localObject2 instanceof String)) || ((localObject2 instanceof Integer)))) {
        localHashMap.put((String)localObject1, localObject2);
      }
    }
    return localHashMap;
  }
  
  private void a(int paramInt)
  {
    if (!this.U) {
      i("Can't call more than 1 startSession in parallel");
    }
    for (;;)
    {
      return;
      this.U = false;
      i("Restarting Session With No Upgrade");
      com.testfairy.h.f localf = j();
      localf.a(n.X, String.valueOf(this.O));
      localf.a(n.Z, z().toString());
      localf.a(n.aa, com.testfairy.p.h.a(this.D));
      Object localObject;
      if (com.testfairy.k.b.a())
      {
        localObject = "on";
        localf.a("wifi", (String)localObject);
        localf.a("identity", l());
      }
      try
      {
        localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>("{'ignore-auto-update':true}");
        localf.a("options", ((JSONObject)localObject).toString());
        this.b.a(localf, this.Z);
        if (this.c == null) {
          continue;
        }
        this.c.a(new com.testfairy.g.a(paramInt));
        continue;
        localObject = "off";
      }
      catch (JSONException localJSONException)
      {
        for (;;) {}
      }
    }
  }
  
  private void a(int paramInt, String paramString, Object paramObject)
  {
    HashMap localHashMap = new HashMap(2);
    localHashMap.put("type", Integer.valueOf(paramInt));
    localHashMap.put(paramString, paramObject);
    paramString = new g(16, localHashMap);
    this.c.a(paramString);
  }
  
  static void a(Integer paramInteger)
  {
    q.a(paramInteger);
  }
  
  private static void a(String paramString, Throwable paramThrowable)
  {
    Log.e(e.a, paramString, paramThrowable);
  }
  
  private boolean a(Activity paramActivity)
  {
    if ((paramActivity == null) || (!paramActivity.getClass().getSimpleName().contentEquals("AutoUpdateActivity"))) {}
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  private int b(Context paramContext)
  {
    if (this.g == null) {}
    for (int i1 = paramContext.getApplicationContext().getSharedPreferences("testfairy.preferences", 0).getInt("testerId", 0);; i1 = this.g.d()) {
      return i1;
    }
  }
  
  private void b(int paramInt)
  {
    Object localObject = new HashMap(1);
    ((HashMap)localObject).put("type", Integer.valueOf(paramInt));
    localObject = new g(16, (Map)localObject);
    this.c.a((g)localObject);
  }
  
  private void c(Context paramContext)
  {
    b(paramContext, null);
  }
  
  private boolean c(Context paramContext, String paramString)
  {
    boolean bool = false;
    if (paramContext == null) {
      i("Can't check if " + paramString + " exist, context is null");
    }
    for (;;)
    {
      return bool;
      if (paramContext.checkCallingOrSelfPermission(paramString) == 0) {
        bool = true;
      }
    }
  }
  
  private void d(Context paramContext)
  {
    try
    {
      if ((this.Q.e()) && (!this.B.containsKey(paramContext)))
      {
        Object localObject = new android/content/IntentFilter;
        ((IntentFilter)localObject).<init>("android.intent.action.BATTERY_CHANGED");
        c localc = new com/testfairy/c;
        localc.<init>();
        localc.a(this.aa);
        paramContext.registerReceiver(localc, (IntentFilter)localObject);
        this.B.put(paramContext, localc);
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        i("Registered battery receiver " + localc + " on " + paramContext);
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  private void e(Context paramContext)
  {
    try
    {
      if (this.Q.e())
      {
        BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)this.B.remove(paramContext);
        if (localBroadcastReceiver != null)
        {
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          i("Unregistering battery receiver " + localBroadcastReceiver + " from context " + paramContext);
          paramContext.unregisterReceiver(localBroadcastReceiver);
        }
      }
      return;
    }
    catch (Throwable paramContext)
    {
      for (;;) {}
    }
  }
  
  private void h(String paramString)
  {
    this.Q = new com.testfairy.e.c(this.D).a(paramString.split(","));
    this.c.a(this.Q);
  }
  
  private void i()
  {
    if (this.L != null) {
      this.L.cancel();
    }
    if (this.M != null)
    {
      this.M.cancel();
      this.M = null;
    }
  }
  
  private static void i(String paramString)
  {
    Log.v(e.a, paramString);
  }
  
  private com.testfairy.h.f j()
  {
    String str = com.testfairy.p.h.e(this.D);
    com.testfairy.h.f localf = new com.testfairy.h.f();
    localf.a(n.W, this.z);
    localf.a(n.ab, "20170625-78d2bc1-1.5.1");
    localf.a(n.ac, String.valueOf(2));
    localf.a("platform", "0");
    localf.a("bundleVersion", String.valueOf(com.testfairy.p.h.b(this.D)));
    localf.a("bundleShortVersion", com.testfairy.p.h.c(this.D));
    localf.a("bundleDisplayName", com.testfairy.p.h.a(d.a(this.E), str));
    localf.a("bundleIdentifier", str);
    int i1 = b(this.D);
    if (i1 > 0)
    {
      str = String.valueOf(i1);
      localf.a("testerId", str);
      if (!com.testfairy.p.k.a(this.D)) {
        break label206;
      }
    }
    label206:
    for (str = "1";; str = "0")
    {
      localf.a("isService", str);
      str = TextUtils.join(",", this.X.a());
      localf.a(n.Y, str);
      localf.a("hashedEmails", com.testfairy.p.a.a(this.D));
      return localf;
      str = null;
      break;
    }
  }
  
  private void k()
  {
    Object localObject = this.P;
    this.C = b();
    i("Stop session: " + this.C);
    localObject = ((i)localObject).f();
    if (localObject != null)
    {
      ((Timer)localObject).cancel();
      ((Timer)localObject).purge();
    }
    if (this.c != null) {
      this.c.c();
    }
    if (this.H != null)
    {
      this.H.a();
      this.H = null;
    }
    if (this.d != null)
    {
      this.d.a();
      this.d = null;
    }
    i();
    this.P = new i();
  }
  
  private String l()
  {
    String str1 = this.f.a("email");
    String str2 = this.f.a();
    if ((str1 != null) || (str2 != null)) {}
    for (;;)
    {
      try
      {
        this.f.a(false);
        JSONObject localJSONObject = new org/json/JSONObject;
        localJSONObject.<init>();
        localJSONObject.put("email", str1);
        localJSONObject.put("correlationId", str2);
        str1 = localJSONObject.toString();
        return str1;
      }
      catch (JSONException localJSONException)
      {
        Log.e(e.a, "Could not serialize identify request");
      }
      Object localObject = null;
    }
  }
  
  private void m()
  {
    if ((this.Q == null) || (this.h != null)) {}
    for (;;)
    {
      return;
      if (this.Q.n())
      {
        this.h = new h(this.x, this.Y, this.D, d.e(this.E));
        this.h.d();
        if (this.W == null) {
          this.W = new FeedbackOptions.Builder().build();
        }
        this.h.a(this.W);
      }
    }
  }
  
  private void n()
  {
    if (this.h != null) {
      this.h.e();
    }
  }
  
  private void o()
  {
    if (this.h != null) {
      this.h.d();
    }
  }
  
  private void p()
  {
    this.S = true;
    if ((this.Q != null) && (!this.Q.q())) {
      t();
    }
    n();
    v();
  }
  
  private void q()
  {
    this.S = false;
    s();
  }
  
  private void r()
  {
    n();
    t();
    v();
  }
  
  private void s()
  {
    if ((this.T) || (this.S)) {}
    for (;;)
    {
      return;
      Activity localActivity = this.x.a();
      if ((!this.Y.f()) && (this.z != null) && (this.D != null) && (!this.K.b()) && (!a(localActivity))) {
        b(this.D, this.C);
      }
      o();
      u();
      w();
    }
  }
  
  private void t()
  {
    if (this.d != null) {
      this.d.b();
    }
    this.I.b();
    p.a();
  }
  
  private void u()
  {
    if (this.d != null) {
      this.d.c();
    }
    this.I.c();
    p.b();
  }
  
  private void v()
  {
    if (this.H != null) {
      this.H.a(true);
    }
  }
  
  private void w()
  {
    if (this.H != null) {
      this.H.a(false);
    }
  }
  
  private void x()
  {
    try
    {
      Class.forName("com.newrelic.agent.android.NewRelic").getMethod("setAttribute", new Class[] { String.class, String.class }).invoke(null, new Object[] { "TestFairy URL", b() });
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  private void y()
  {
    try
    {
      localObject1 = com.testfairy.p.h.f(this.D);
      if (localObject1 == null) {
        break label186;
      }
      localObject3 = ((BitmapDrawable)localObject1).getBitmap();
      localObject1 = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject1).<init>();
      ((Bitmap)localObject3).compress(Bitmap.CompressFormat.PNG, 80, (OutputStream)localObject1);
      localObject1 = ((ByteArrayOutputStream)localObject1).toByteArray();
    }
    catch (Throwable localThrowable)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject3;
        Object localObject5;
        Object localObject4;
        JSONArray localJSONArray;
        continue;
        label186:
        Object localObject2 = null;
      }
    }
    localObject5 = com.testfairy.p.h.g(this.D);
    localObject4 = com.testfairy.p.h.d(this.D);
    localObject3 = new com/testfairy/h/f;
    ((com.testfairy.h.f)localObject3).<init>();
    ((com.testfairy.h.f)localObject3).a("sessionToken", this.Y.c().a());
    localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>((Collection)localObject5);
    ((com.testfairy.h.f)localObject3).a("activities", localJSONArray.toString());
    localObject5 = new org/json/JSONArray;
    ((JSONArray)localObject5).<init>((Collection)localObject4);
    ((com.testfairy.h.f)localObject3).a("permissions", ((JSONArray)localObject5).toString());
    if (localObject1 != null)
    {
      localObject4 = new java/io/ByteArrayInputStream;
      ((ByteArrayInputStream)localObject4).<init>((byte[])localObject1);
      ((com.testfairy.h.f)localObject3).a("icon", (InputStream)localObject4, "icon.jpeg", "image/png");
    }
    localObject4 = this.b;
    localObject1 = new com/testfairy/h/c;
    ((com.testfairy.h.c)localObject1).<init>();
    ((com.testfairy.k.d)localObject4).f((com.testfairy.h.f)localObject3, (com.testfairy.h.c)localObject1);
  }
  
  private JSONObject z()
  {
    JSONObject localJSONObject = new JSONObject();
    try
    {
      f localf = new com/testfairy/f;
      localf.<init>();
      int i1 = localf.b();
      DisplayMetrics localDisplayMetrics = this.D.getResources().getDisplayMetrics();
      localJSONObject.put("product", Build.PRODUCT);
      localJSONObject.put("deviceModel", Build.MODEL);
      localJSONObject.put("brand", Build.BRAND);
      localJSONObject.put("bootloader", Build.BOOTLOADER);
      localJSONObject.put("board", Build.BOARD);
      localJSONObject.put("deviceId", Build.ID);
      localJSONObject.put("device", Build.DEVICE);
      localJSONObject.put("display", Build.DISPLAY);
      localJSONObject.put("manufacturer", Build.MANUFACTURER);
      localJSONObject.put("osRelease", Build.VERSION.RELEASE);
      localJSONObject.put("apiLevel", String.valueOf(Build.VERSION.SDK_INT));
      localJSONObject.put("screenWidth", String.valueOf(localDisplayMetrics.widthPixels));
      localJSONObject.put("screenHeight", String.valueOf(localDisplayMetrics.heightPixels));
      localJSONObject.put("screenDensity", String.valueOf(localDisplayMetrics.densityDpi));
      localJSONObject.put("cpuCores", String.valueOf(i1));
      localJSONObject.put("abi", Build.CPU_ABI);
      localJSONObject.put("abi2", Build.CPU_ABI2);
      localJSONObject.put("memorySize", String.valueOf(localf.a()));
      localJSONObject.put("osVersion", localf.c());
      localJSONObject.put("isRoot", String.valueOf(localf.d()));
      localJSONObject.put("localeCountry", Locale.getDefault().getCountry());
      localJSONObject.put("localeLanguage", Locale.getDefault().getLanguage());
      return localJSONObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public void a()
  {
    if ((!this.f.b()) || (this.f.a() == null) || (!this.Y.f())) {}
    for (;;)
    {
      return;
      Log.d(e.a, "sendUserData: " + this.f.a());
      try
      {
        Object localObject = new org/json/JSONObject;
        ((JSONObject)localObject).<init>();
        ((JSONObject)localObject).put("email", this.f.a("email"));
        ((JSONObject)localObject).put("correlationId", this.f.a());
        com.testfairy.h.f localf = new com/testfairy/h/f;
        localf.<init>();
        localf.a("data", ((JSONObject)localObject).toString());
        localf.a("sessionToken", this.P.a());
        localObject = this.b;
        com.testfairy.h.c localc = new com/testfairy/h/c;
        localc.<init>();
        ((com.testfairy.k.d)localObject).k(localf, localc);
      }
      catch (Throwable localThrowable)
      {
        Log.d(e.a, "sendUserData Throwable ", localThrowable);
      }
    }
  }
  
  void a(Context paramContext, String paramString)
  {
    if (this.D != null) {
      i("begin can be called only once");
    }
    for (;;)
    {
      return;
      if (paramContext.checkCallingOrSelfPermission("android.permission.INTERNET") != 0)
      {
        Log.e(e.a, "INTERNET permission is not granted, cannot use SDK");
      }
      else
      {
        this.z = paramString;
        this.x = new com.testfairy.a.b(paramContext, this.j);
        if (Build.VERSION.SDK_INT >= 14) {
          ((Application)paramContext.getApplicationContext()).registerActivityLifecycleCallbacks(this.x);
        }
        c(paramContext);
      }
    }
  }
  
  void a(Location paramLocation)
  {
    if ((this.Q != null) && (this.G != null) && (this.Q.c()))
    {
      this.G.a(paramLocation);
      this.G.a();
    }
  }
  
  void a(View paramView)
  {
    q.a(paramView);
  }
  
  public void a(FeedbackOptions paramFeedbackOptions)
  {
    this.W = paramFeedbackOptions;
    if (this.h != null) {
      this.h.a(this.W);
    }
  }
  
  void a(SessionStateListener paramSessionStateListener)
  {
    if (!this.N.contains(paramSessionStateListener)) {
      this.N.add(paramSessionStateListener);
    }
  }
  
  public void a(TestFairy.LogEventFilter paramLogEventFilter)
  {
    this.V = paramLogEventFilter;
  }
  
  public void a(com.testfairy.k.e parame)
  {
    this.y = parame;
  }
  
  public void a(String paramString)
  {
    if ((this.R == null) || (!this.R.equals(paramString)))
    {
      this.R = paramString;
      HashMap localHashMap = new HashMap(2);
      localHashMap.put("name", paramString);
      paramString = new g(18, localHashMap);
      this.c.a(paramString);
    }
  }
  
  public void a(String paramString, Map paramMap)
  {
    if (paramMap == null) {}
    for (;;)
    {
      return;
      if (this.J)
      {
        Log.i(e.a, "Testfairy.setCorrelationId() function can be called only once, Please use Testfairy.setUserId() or Testfairy.setAttribute() instead");
      }
      else
      {
        this.J = true;
        Iterator localIterator = paramMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
          paramMap = (Map.Entry)localIterator.next();
          if ((paramMap.getValue() instanceof Serializable)) {
            a((String)paramMap.getKey(), paramMap.getValue().toString());
          }
        }
        if ((paramString != null) && (!paramString.isEmpty())) {
          d(paramString);
        }
      }
    }
  }
  
  public void a(Thread paramThread, Throwable paramThrowable)
  {
    if (this.Y.f()) {
      this.A.a(this.P.a(), paramThrowable);
    }
    for (;;)
    {
      return;
      paramThread = j();
      paramThread.a("stackTrace", com.testfairy.p.l.a(paramThrowable));
      paramThread.a("message", paramThrowable.getMessage());
      paramThread.a("timestamp", String.valueOf(this.O / 1000L));
      paramThread.a("deviceData", z().toString());
      this.b.d(paramThread, null);
    }
  }
  
  public boolean a(String paramString1, String paramString2)
  {
    return this.f.a(paramString1, paramString2);
  }
  
  String b()
  {
    return this.Y.h();
  }
  
  void b(Context paramContext, String paramString)
  {
    if (!this.U)
    {
      i("Can't call more than 1 startSession in parallel");
      return;
    }
    this.U = false;
    i("Starting new session, " + this + ", Previous Session Url = " + paramString + ". " + "1.5.1");
    this.O = System.currentTimeMillis();
    this.c = new com.testfairy.m.b();
    if (paramString != null) {
      a(18, "previousSessionUrl", paramString);
    }
    a(paramContext.getClass().getName());
    this.D = paramContext.getApplicationContext();
    com.testfairy.f.d.a(this);
    this.A = new com.testfairy.f.e(this.b, this.Y.d());
    this.A.a();
    this.E = new d(this.D);
    com.testfairy.k.b.a(d.b(this.E), d.c(this.E));
    this.I = new com.testfairy.n.l();
    this.G = new com.testfairy.n.h(this.c, d.d(this.E));
    this.c.a(this.Y.d());
    paramContext = a(this.D);
    if (paramContext != null)
    {
      Log.d(e.a, "Cached options was found '" + paramContext + "'");
      h(paramContext);
    }
    m();
    paramString = j();
    paramString.a(n.X, String.valueOf(this.O));
    paramString.a(n.Z, z().toString());
    paramString.a(n.aa, com.testfairy.p.h.a(this.D));
    if (com.testfairy.k.b.a()) {}
    for (paramContext = "on";; paramContext = "off")
    {
      paramString.a("wifi", paramContext);
      paramString.a("identity", l());
      this.b.a(paramString, this.Z);
      break;
    }
  }
  
  public void b(com.testfairy.k.e parame)
  {
    this.y = parame;
    this.b = this.y.a(this.w);
  }
  
  void b(String paramString)
  {
    float f1 = (float)(System.currentTimeMillis() - this.O) / 1000.0F;
    com.testfairy.h.f localf = j();
    localf.a("sessionToken", this.P.a());
    localf.a("text", paramString);
    localf.a("timestamp", String.valueOf(f1));
    this.b.g(localf, new com.testfairy.h.c()
    {
      public void a(String paramAnonymousString)
      {
        Log.v(e.a, "Nice! Feedback sent " + paramAnonymousString);
      }
      
      public void a(Throwable paramAnonymousThrowable, String paramAnonymousString)
      {
        Log.e(e.a, "Failed to send user feedback: " + paramAnonymousString, paramAnonymousThrowable);
      }
    });
  }
  
  public void b(String paramString1, String paramString2)
  {
    try
    {
      if ((this.Q != null) && (this.Q.a()) && (this.c != null))
      {
        com.testfairy.m.b localb1 = this.c;
        com.testfairy.g.b localb = new com/testfairy/g/b;
        localb.<init>(System.currentTimeMillis(), "V", paramString1, paramString2);
        localb1.a(localb);
      }
      return;
    }
    catch (Throwable paramString1)
    {
      for (;;) {}
    }
  }
  
  void c()
  {
    if (this.Y.f()) {
      b(19);
    }
    k();
  }
  
  void c(String paramString)
  {
    HashMap localHashMap = new HashMap(1);
    localHashMap.put("name", paramString);
    paramString = new g(25, localHashMap);
    this.c.a(paramString);
  }
  
  public void d()
  {
    this.T = true;
    r();
  }
  
  public void d(String paramString)
  {
    this.f.b(paramString);
  }
  
  public void e()
  {
    this.T = false;
    s();
  }
  
  void e(String paramString)
  {
    i("Received: " + paramString);
    try
    {
      localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      paramString = localJSONObject.getString("status");
      if ((paramString != null) && (paramString.equals("fail")) && (localJSONObject.optInt("code", 0) == 107))
      {
        Log.e(e.a, "Failed to initialize TestFairy, this is probably the wrong SDK App Token. Please use the token from https://app.testfairy.com/settings/#app-token and referer to the documentation for more information");
        return;
      }
    }
    catch (Throwable paramString)
    {
      for (;;)
      {
        JSONObject localJSONObject;
        String str;
        int i1;
        a("Throwable", paramString);
        continue;
        x();
        continue;
        if ((paramString != null) && (paramString.equals("fail")))
        {
          i1 = localJSONObject.getInt("code");
          if (i1 == 101)
          {
            this.K.a(localJSONObject);
            this.K.a(this.x.a());
          }
          else
          {
            paramString = new java/lang/StringBuilder;
            paramString.<init>();
            i("Start session fail (code: " + i1 + ")");
          }
        }
        else
        {
          i("Can't start session");
        }
      }
    }
    if ((paramString != null) && (paramString.equals("ok")))
    {
      paramString = localJSONObject.getString("sessionToken");
      str = localJSONObject.getString("endpointAddress");
      this.P.a(paramString, this.y, str);
      paramString = new com/testfairy/j;
      paramString.<init>(this.D, localJSONObject, this.w);
      this.g = paramString;
      h(this.g.a());
      i1 = this.g.h();
      if ((i1 != -1) && (i1 != this.g.c()))
      {
        a(15, "previousBuildId", Integer.valueOf(i1));
        this.g.a(this.g.c());
      }
      if (localJSONObject.has("credentials"))
      {
        paramString = localJSONObject.getJSONObject("credentials");
        i1 = paramString.getInt("testerId");
        paramString = paramString.getString("secret");
        this.g.a(i1, paramString);
      }
      this.c.b();
      this.c.a(this.P);
      this.c.a(this.P.b());
      this.c.a();
      str = com.testfairy.p.k.b(this.D);
      paramString = this.g.d(str);
      if (paramString != null) {
        this.g.c(paramString);
      }
      this.g.a(this.P.a(), str);
      if (localJSONObject.has("sessionUrl")) {
        this.P.a(localJSONObject.getString("sessionUrl"));
      }
      if (localJSONObject.has("backgroundTimeUntilStopSession")) {
        this.i = localJSONObject.getInt("backgroundTimeUntilStopSession");
      }
      a();
      this.f.c();
      A();
      if (localJSONObject.has("requestBuildInfo")) {
        y();
      }
      paramString = this.N.iterator();
      while (paramString.hasNext()) {
        ((SessionStateListener)paramString.next()).onSessionStarted(b());
      }
    }
  }
  
  void f()
  {
    if (this.H != null) {
      this.H.run();
    }
  }
  
  public void f(String paramString)
  {
    this.w = paramString;
    this.b = this.y.a(paramString);
  }
  
  void g()
  {
    if (this.D != null) {
      ((Application)this.D.getApplicationContext()).unregisterActivityLifecycleCallbacks(this.x);
    }
  }
  
  boolean h()
  {
    return this.U;
  }
  
  private class a
    extends TimerTask
  {
    private a() {}
    
    public void run()
    {
      com.testfairy.f.d.a(o.this);
    }
  }
  
  private class b
    extends TimerTask
  {
    private b() {}
    
    public void run()
    {
      View[] arrayOfView = s.a();
      if (arrayOfView == null) {}
      for (;;)
      {
        return;
        int j = arrayOfView.length;
        for (int i = 0; i < j; i++) {
          p.a(arrayOfView[i], o.this.c, o.l(o.this));
        }
      }
    }
  }
  
  private class c
    extends TimerTask
  {
    private c() {}
    
    public void run()
    {
      if (o.this.c != null)
      {
        g localg = new g(0);
        o.this.c.a(localg);
        o.this.c.a();
      }
    }
  }
  
  private class d
  {
    private final PackageManager b;
    private final ActivityManager c;
    private final TelephonyManager d;
    private final SensorManager e;
    private final LocationManager f;
    private final WifiManager g;
    private final ConnectivityManager h;
    
    public d(Context paramContext)
    {
      this.b = paramContext.getPackageManager();
      this.c = ((ActivityManager)paramContext.getSystemService("activity"));
      this.d = ((TelephonyManager)paramContext.getSystemService("phone"));
      this.e = ((SensorManager)paramContext.getSystemService("sensor"));
      this.f = ((LocationManager)paramContext.getSystemService("location"));
      this.g = ((WifiManager)paramContext.getSystemService("wifi"));
      this.h = ((ConnectivityManager)paramContext.getSystemService("connectivity"));
    }
  }
  
  private class e
    extends TimerTask
  {
    private e() {}
    
    public void run()
    {
      o.g("Session is longer than limit of " + Math.floor(o.l(o.this).x()) + " seconds");
      o.this.c.a(new com.testfairy.g.c());
      o.u(o.this).run();
      o.u(o.this).a();
      o.c(o.this);
    }
  }
  
  class f
    implements com.testfairy.j.b
  {
    double a = 0.0D;
    int b = 0;
    
    f() {}
    
    double a(long paramLong1, long paramLong2)
    {
      paramLong1 = 1000L * (paramLong1 - paramLong2);
      if (this.a == paramLong1)
      {
        this.b += 1;
        paramLong1 += this.b;
      }
      for (;;)
      {
        return paramLong1 / 1000000.0D;
        this.b = 0;
        this.a = paramLong1;
      }
    }
    
    public void a()
    {
      if ((o.this.d instanceof com.testfairy.j.a)) {
        if (o.i(o.this) == null) {
          o.g("Don't start LogcatReader, there is no session!");
        }
      }
      for (;;)
      {
        return;
        o.g("LogFileReader fail, start LogcatReader");
        o.this.d = new com.testfairy.j.d(this);
        o.this.d.start();
        o.g("Started a new log reader " + o.this.d);
        continue;
        o.b(o.this, 3);
        o.this.c.a();
      }
    }
    
    public void a(long paramLong, String paramString1, String paramString2, String paramString3)
    {
      try
      {
        if ("testfairy-secure-viewid".equals(paramString2)) {
          q.a(Integer.valueOf(paramString3));
        }
        for (;;)
        {
          return;
          if ((!paramString2.equals("dalvikvm")) && (!paramString2.equals("libc")) && (!paramString2.equals("memmalloc")) && (!paramString2.equals("NativeCrypto")) && (!paramString2.equals("TS_DISCARD_TEST")))
          {
            Object localObject = o.p(o.this);
            if (((localObject == null) || (((TestFairy.LogEventFilter)localObject).accept(paramString1, paramString2, paramString3))) && (paramLong >= o.a(o.this).g()))
            {
              double d = a(paramLong, o.a(o.this).g());
              com.testfairy.m.b localb = o.this.c;
              localObject = new com/testfairy/g/b;
              ((com.testfairy.g.b)localObject).<init>(d, paramString1, paramString2, paramString3);
              localb.a((g)localObject);
            }
          }
        }
      }
      catch (Throwable paramString1)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */