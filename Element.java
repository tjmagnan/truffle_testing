package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Parser;
import org.jsoup.parser.Tag;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.jsoup.select.Evaluator.AllElements;
import org.jsoup.select.Evaluator.Attribute;
import org.jsoup.select.Evaluator.AttributeStarting;
import org.jsoup.select.Evaluator.AttributeWithValue;
import org.jsoup.select.Evaluator.AttributeWithValueContaining;
import org.jsoup.select.Evaluator.AttributeWithValueEnding;
import org.jsoup.select.Evaluator.AttributeWithValueMatching;
import org.jsoup.select.Evaluator.AttributeWithValueNot;
import org.jsoup.select.Evaluator.AttributeWithValueStarting;
import org.jsoup.select.Evaluator.Class;
import org.jsoup.select.Evaluator.ContainsOwnText;
import org.jsoup.select.Evaluator.ContainsText;
import org.jsoup.select.Evaluator.Id;
import org.jsoup.select.Evaluator.IndexEquals;
import org.jsoup.select.Evaluator.IndexGreaterThan;
import org.jsoup.select.Evaluator.IndexLessThan;
import org.jsoup.select.Evaluator.Matches;
import org.jsoup.select.Evaluator.MatchesOwn;
import org.jsoup.select.Evaluator.Tag;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import org.jsoup.select.QueryParser;
import org.jsoup.select.Selector;

public class Element
  extends Node
{
  private static final Pattern classSplit = Pattern.compile("\\s+");
  private Tag tag;
  
  public Element(String paramString)
  {
    this(Tag.valueOf(paramString), "", new Attributes());
  }
  
  public Element(Tag paramTag, String paramString)
  {
    this(paramTag, paramString, new Attributes());
  }
  
  public Element(Tag paramTag, String paramString, Attributes paramAttributes)
  {
    super(paramString, paramAttributes);
    Validate.notNull(paramTag);
    this.tag = paramTag;
  }
  
  private static void accumulateParents(Element paramElement, Elements paramElements)
  {
    paramElement = paramElement.parent();
    if ((paramElement != null) && (!paramElement.tagName().equals("#root")))
    {
      paramElements.add(paramElement);
      accumulateParents(paramElement, paramElements);
    }
  }
  
  private static void appendNormalisedText(StringBuilder paramStringBuilder, TextNode paramTextNode)
  {
    String str = paramTextNode.getWholeText();
    if (preserveWhitespace(paramTextNode.parentNode)) {
      paramStringBuilder.append(str);
    }
    for (;;)
    {
      return;
      StringUtil.appendNormalisedWhitespace(paramStringBuilder, str, TextNode.lastCharIsWhitespace(paramStringBuilder));
    }
  }
  
  private static void appendWhitespaceIfBr(Element paramElement, StringBuilder paramStringBuilder)
  {
    if ((paramElement.tag.getName().equals("br")) && (!TextNode.lastCharIsWhitespace(paramStringBuilder))) {
      paramStringBuilder.append(" ");
    }
  }
  
  private void html(StringBuilder paramStringBuilder)
  {
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext()) {
      ((Node)localIterator.next()).outerHtml(paramStringBuilder);
    }
  }
  
  private static <E extends Element> Integer indexInList(Element paramElement, List<E> paramList)
  {
    Validate.notNull(paramElement);
    Validate.notNull(paramList);
    int i = 0;
    if (i < paramList.size()) {
      if ((Element)paramList.get(i) != paramElement) {}
    }
    for (paramElement = Integer.valueOf(i);; paramElement = null)
    {
      return paramElement;
      i++;
      break;
    }
  }
  
  private void ownText(StringBuilder paramStringBuilder)
  {
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext())
    {
      Node localNode = (Node)localIterator.next();
      if ((localNode instanceof TextNode)) {
        appendNormalisedText(paramStringBuilder, (TextNode)localNode);
      } else if ((localNode instanceof Element)) {
        appendWhitespaceIfBr((Element)localNode, paramStringBuilder);
      }
    }
  }
  
  static boolean preserveWhitespace(Node paramNode)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramNode != null)
    {
      bool1 = bool2;
      if ((paramNode instanceof Element))
      {
        paramNode = (Element)paramNode;
        if (!paramNode.tag.preserveWhitespace())
        {
          bool1 = bool2;
          if (paramNode.parent() != null)
          {
            bool1 = bool2;
            if (!paramNode.parent().tag.preserveWhitespace()) {}
          }
        }
        else
        {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public Element addClass(String paramString)
  {
    Validate.notNull(paramString);
    Set localSet = classNames();
    localSet.add(paramString);
    classNames(localSet);
    return this;
  }
  
  public Element after(String paramString)
  {
    return (Element)super.after(paramString);
  }
  
  public Element after(Node paramNode)
  {
    return (Element)super.after(paramNode);
  }
  
  public Element append(String paramString)
  {
    Validate.notNull(paramString);
    paramString = Parser.parseFragment(paramString, this, baseUri());
    addChildren((Node[])paramString.toArray(new Node[paramString.size()]));
    return this;
  }
  
  public Element appendChild(Node paramNode)
  {
    Validate.notNull(paramNode);
    reparentChild(paramNode);
    ensureChildNodes();
    this.childNodes.add(paramNode);
    paramNode.setSiblingIndex(this.childNodes.size() - 1);
    return this;
  }
  
  public Element appendElement(String paramString)
  {
    paramString = new Element(Tag.valueOf(paramString), baseUri());
    appendChild(paramString);
    return paramString;
  }
  
  public Element appendText(String paramString)
  {
    Validate.notNull(paramString);
    appendChild(new TextNode(paramString, baseUri()));
    return this;
  }
  
  public Element attr(String paramString1, String paramString2)
  {
    super.attr(paramString1, paramString2);
    return this;
  }
  
  public Element attr(String paramString, boolean paramBoolean)
  {
    this.attributes.put(paramString, paramBoolean);
    return this;
  }
  
  public Element before(String paramString)
  {
    return (Element)super.before(paramString);
  }
  
  public Element before(Node paramNode)
  {
    return (Element)super.before(paramNode);
  }
  
  public Element child(int paramInt)
  {
    return (Element)children().get(paramInt);
  }
  
  public Elements children()
  {
    ArrayList localArrayList = new ArrayList(this.childNodes.size());
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext())
    {
      Node localNode = (Node)localIterator.next();
      if ((localNode instanceof Element)) {
        localArrayList.add((Element)localNode);
      }
    }
    return new Elements(localArrayList);
  }
  
  public String className()
  {
    return attr("class").trim();
  }
  
  public Set<String> classNames()
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet(Arrays.asList(classSplit.split(className())));
    localLinkedHashSet.remove("");
    return localLinkedHashSet;
  }
  
  public Element classNames(Set<String> paramSet)
  {
    Validate.notNull(paramSet);
    this.attributes.put("class", StringUtil.join(paramSet, " "));
    return this;
  }
  
  public Element clone()
  {
    return (Element)super.clone();
  }
  
  public String cssSelector()
  {
    Object localObject;
    if (id().length() > 0) {
      localObject = "#" + id();
    }
    for (;;)
    {
      return (String)localObject;
      localObject = new StringBuilder(tagName().replace(':', '|'));
      String str = StringUtil.join(classNames(), ".");
      if (str.length() > 0) {
        ((StringBuilder)localObject).append('.').append(str);
      }
      if ((parent() == null) || ((parent() instanceof Document)))
      {
        localObject = ((StringBuilder)localObject).toString();
      }
      else
      {
        ((StringBuilder)localObject).insert(0, " > ");
        if (parent().select(((StringBuilder)localObject).toString()).size() > 1) {
          ((StringBuilder)localObject).append(String.format(":nth-child(%d)", new Object[] { Integer.valueOf(elementSiblingIndex().intValue() + 1) }));
        }
        localObject = parent().cssSelector() + ((StringBuilder)localObject).toString();
      }
    }
  }
  
  public String data()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext())
    {
      Node localNode = (Node)localIterator.next();
      if ((localNode instanceof DataNode)) {
        localStringBuilder.append(((DataNode)localNode).getWholeData());
      } else if ((localNode instanceof Comment)) {
        localStringBuilder.append(((Comment)localNode).getData());
      } else if ((localNode instanceof Element)) {
        localStringBuilder.append(((Element)localNode).data());
      }
    }
    return localStringBuilder.toString();
  }
  
  public List<DataNode> dataNodes()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext())
    {
      Node localNode = (Node)localIterator.next();
      if ((localNode instanceof DataNode)) {
        localArrayList.add((DataNode)localNode);
      }
    }
    return Collections.unmodifiableList(localArrayList);
  }
  
  public Map<String, String> dataset()
  {
    return this.attributes.dataset();
  }
  
  public Integer elementSiblingIndex()
  {
    if (parent() == null) {}
    for (Integer localInteger = Integer.valueOf(0);; localInteger = indexInList(this, parent().children())) {
      return localInteger;
    }
  }
  
  public Element empty()
  {
    this.childNodes.clear();
    return this;
  }
  
  public Element firstElementSibling()
  {
    Object localObject = parent().children();
    if (((List)localObject).size() > 1) {}
    for (localObject = (Element)((List)localObject).get(0);; localObject = null) {
      return (Element)localObject;
    }
  }
  
  public Elements getAllElements()
  {
    return Collector.collect(new Evaluator.AllElements(), this);
  }
  
  public Element getElementById(String paramString)
  {
    Validate.notEmpty(paramString);
    paramString = Collector.collect(new Evaluator.Id(paramString), this);
    if (paramString.size() > 0) {}
    for (paramString = (Element)paramString.get(0);; paramString = null) {
      return paramString;
    }
  }
  
  public Elements getElementsByAttribute(String paramString)
  {
    Validate.notEmpty(paramString);
    return Collector.collect(new Evaluator.Attribute(paramString.trim()), this);
  }
  
  public Elements getElementsByAttributeStarting(String paramString)
  {
    Validate.notEmpty(paramString);
    return Collector.collect(new Evaluator.AttributeStarting(paramString.trim()), this);
  }
  
  public Elements getElementsByAttributeValue(String paramString1, String paramString2)
  {
    return Collector.collect(new Evaluator.AttributeWithValue(paramString1, paramString2), this);
  }
  
  public Elements getElementsByAttributeValueContaining(String paramString1, String paramString2)
  {
    return Collector.collect(new Evaluator.AttributeWithValueContaining(paramString1, paramString2), this);
  }
  
  public Elements getElementsByAttributeValueEnding(String paramString1, String paramString2)
  {
    return Collector.collect(new Evaluator.AttributeWithValueEnding(paramString1, paramString2), this);
  }
  
  public Elements getElementsByAttributeValueMatching(String paramString1, String paramString2)
  {
    try
    {
      Pattern localPattern = Pattern.compile(paramString2);
      return getElementsByAttributeValueMatching(paramString1, localPattern);
    }
    catch (PatternSyntaxException paramString1)
    {
      throw new IllegalArgumentException("Pattern syntax error: " + paramString2, paramString1);
    }
  }
  
  public Elements getElementsByAttributeValueMatching(String paramString, Pattern paramPattern)
  {
    return Collector.collect(new Evaluator.AttributeWithValueMatching(paramString, paramPattern), this);
  }
  
  public Elements getElementsByAttributeValueNot(String paramString1, String paramString2)
  {
    return Collector.collect(new Evaluator.AttributeWithValueNot(paramString1, paramString2), this);
  }
  
  public Elements getElementsByAttributeValueStarting(String paramString1, String paramString2)
  {
    return Collector.collect(new Evaluator.AttributeWithValueStarting(paramString1, paramString2), this);
  }
  
  public Elements getElementsByClass(String paramString)
  {
    Validate.notEmpty(paramString);
    return Collector.collect(new Evaluator.Class(paramString), this);
  }
  
  public Elements getElementsByIndexEquals(int paramInt)
  {
    return Collector.collect(new Evaluator.IndexEquals(paramInt), this);
  }
  
  public Elements getElementsByIndexGreaterThan(int paramInt)
  {
    return Collector.collect(new Evaluator.IndexGreaterThan(paramInt), this);
  }
  
  public Elements getElementsByIndexLessThan(int paramInt)
  {
    return Collector.collect(new Evaluator.IndexLessThan(paramInt), this);
  }
  
  public Elements getElementsByTag(String paramString)
  {
    Validate.notEmpty(paramString);
    return Collector.collect(new Evaluator.Tag(paramString.toLowerCase().trim()), this);
  }
  
  public Elements getElementsContainingOwnText(String paramString)
  {
    return Collector.collect(new Evaluator.ContainsOwnText(paramString), this);
  }
  
  public Elements getElementsContainingText(String paramString)
  {
    return Collector.collect(new Evaluator.ContainsText(paramString), this);
  }
  
  public Elements getElementsMatchingOwnText(String paramString)
  {
    try
    {
      Pattern localPattern = Pattern.compile(paramString);
      return getElementsMatchingOwnText(localPattern);
    }
    catch (PatternSyntaxException localPatternSyntaxException)
    {
      throw new IllegalArgumentException("Pattern syntax error: " + paramString, localPatternSyntaxException);
    }
  }
  
  public Elements getElementsMatchingOwnText(Pattern paramPattern)
  {
    return Collector.collect(new Evaluator.MatchesOwn(paramPattern), this);
  }
  
  public Elements getElementsMatchingText(String paramString)
  {
    try
    {
      Pattern localPattern = Pattern.compile(paramString);
      return getElementsMatchingText(localPattern);
    }
    catch (PatternSyntaxException localPatternSyntaxException)
    {
      throw new IllegalArgumentException("Pattern syntax error: " + paramString, localPatternSyntaxException);
    }
  }
  
  public Elements getElementsMatchingText(Pattern paramPattern)
  {
    return Collector.collect(new Evaluator.Matches(paramPattern), this);
  }
  
  public boolean hasClass(String paramString)
  {
    boolean bool2 = false;
    String str = this.attributes.get("class");
    int i1 = str.length();
    int i2 = paramString.length();
    boolean bool1 = bool2;
    if (i1 != 0)
    {
      if (i1 >= i2) {
        break label51;
      }
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      label51:
      if (i1 == i2)
      {
        bool1 = paramString.equalsIgnoreCase(str);
      }
      else
      {
        int n = 0;
        int m = 0;
        int i = 0;
        if (i < i1)
        {
          int k;
          int j;
          if (Character.isWhitespace(str.charAt(i)))
          {
            k = m;
            j = n;
            if (n != 0)
            {
              if ((i - m == i2) && (str.regionMatches(true, m, paramString, 0, i2)))
              {
                bool1 = true;
                continue;
              }
              j = 0;
              k = m;
            }
          }
          for (;;)
          {
            i++;
            m = k;
            n = j;
            break;
            k = m;
            j = n;
            if (n == 0)
            {
              j = 1;
              k = i;
            }
          }
        }
        else
        {
          bool1 = bool2;
          if (n != 0)
          {
            bool1 = bool2;
            if (i1 - m == i2) {
              bool1 = str.regionMatches(true, m, paramString, 0, i2);
            }
          }
        }
      }
    }
  }
  
  public boolean hasText()
  {
    boolean bool = true;
    Iterator localIterator = this.childNodes.iterator();
    Node localNode;
    do
    {
      if (!localIterator.hasNext()) {
        break label70;
      }
      localNode = (Node)localIterator.next();
      if (!(localNode instanceof TextNode)) {
        break;
      }
    } while (((TextNode)localNode).isBlank());
    for (;;)
    {
      return bool;
      if ((!(localNode instanceof Element)) || (!((Element)localNode).hasText())) {
        break;
      }
      continue;
      label70:
      bool = false;
    }
  }
  
  public <T extends Appendable> T html(T paramT)
  {
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext()) {
      ((Node)localIterator.next()).outerHtml(paramT);
    }
    return paramT;
  }
  
  public String html()
  {
    Object localObject = new StringBuilder();
    html((StringBuilder)localObject);
    if (getOutputSettings().prettyPrint()) {}
    for (localObject = ((StringBuilder)localObject).toString().trim();; localObject = ((StringBuilder)localObject).toString()) {
      return (String)localObject;
    }
  }
  
  public Element html(String paramString)
  {
    empty();
    append(paramString);
    return this;
  }
  
  public String id()
  {
    return this.attributes.getIgnoreCase("id");
  }
  
  public Element insertChildren(int paramInt, Collection<? extends Node> paramCollection)
  {
    Validate.notNull(paramCollection, "Children collection to be inserted must not be null.");
    int j = childNodeSize();
    int i = paramInt;
    if (paramInt < 0) {
      i = paramInt + (j + 1);
    }
    if ((i >= 0) && (i <= j)) {}
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool, "Insert position out of bounds.");
      paramCollection = new ArrayList(paramCollection);
      addChildren(i, (Node[])paramCollection.toArray(new Node[paramCollection.size()]));
      return this;
    }
  }
  
  public boolean is(String paramString)
  {
    return is(QueryParser.parse(paramString));
  }
  
  public boolean is(Evaluator paramEvaluator)
  {
    return paramEvaluator.matches((Element)root(), this);
  }
  
  public boolean isBlock()
  {
    return this.tag.isBlock();
  }
  
  public Element lastElementSibling()
  {
    Object localObject = parent().children();
    if (((List)localObject).size() > 1) {}
    for (localObject = (Element)((List)localObject).get(((List)localObject).size() - 1);; localObject = null) {
      return (Element)localObject;
    }
  }
  
  public Element nextElementSibling()
  {
    Element localElement = null;
    if (this.parentNode == null) {}
    for (;;)
    {
      return localElement;
      Elements localElements = parent().children();
      Integer localInteger = indexInList(this, localElements);
      Validate.notNull(localInteger);
      if (localElements.size() > localInteger.intValue() + 1) {
        localElement = (Element)localElements.get(localInteger.intValue() + 1);
      }
    }
  }
  
  public String nodeName()
  {
    return this.tag.getName();
  }
  
  void outerHtmlHead(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    if ((paramOutputSettings.prettyPrint()) && ((this.tag.formatAsBlock()) || ((parent() != null) && (parent().tag().formatAsBlock())) || (paramOutputSettings.outline())))
    {
      if (!(paramAppendable instanceof StringBuilder)) {
        break label148;
      }
      if (((StringBuilder)paramAppendable).length() > 0) {
        indent(paramAppendable, paramInt, paramOutputSettings);
      }
    }
    paramAppendable.append("<").append(tagName());
    this.attributes.html(paramAppendable, paramOutputSettings);
    if ((this.childNodes.isEmpty()) && (this.tag.isSelfClosing())) {
      if ((paramOutputSettings.syntax() == Document.OutputSettings.Syntax.html) && (this.tag.isEmpty())) {
        paramAppendable.append('>');
      }
    }
    for (;;)
    {
      return;
      label148:
      indent(paramAppendable, paramInt, paramOutputSettings);
      break;
      paramAppendable.append(" />");
      continue;
      paramAppendable.append(">");
    }
  }
  
  void outerHtmlTail(Appendable paramAppendable, int paramInt, Document.OutputSettings paramOutputSettings)
    throws IOException
  {
    if ((!this.childNodes.isEmpty()) || (!this.tag.isSelfClosing()))
    {
      if ((paramOutputSettings.prettyPrint()) && (!this.childNodes.isEmpty()) && ((this.tag.formatAsBlock()) || ((paramOutputSettings.outline()) && ((this.childNodes.size() > 1) || ((this.childNodes.size() == 1) && (!(this.childNodes.get(0) instanceof TextNode))))))) {
        indent(paramAppendable, paramInt, paramOutputSettings);
      }
      paramAppendable.append("</").append(tagName()).append(">");
    }
  }
  
  public String ownText()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    ownText(localStringBuilder);
    return localStringBuilder.toString().trim();
  }
  
  public final Element parent()
  {
    return (Element)this.parentNode;
  }
  
  public Elements parents()
  {
    Elements localElements = new Elements();
    accumulateParents(this, localElements);
    return localElements;
  }
  
  public Element prepend(String paramString)
  {
    Validate.notNull(paramString);
    paramString = Parser.parseFragment(paramString, this, baseUri());
    addChildren(0, (Node[])paramString.toArray(new Node[paramString.size()]));
    return this;
  }
  
  public Element prependChild(Node paramNode)
  {
    Validate.notNull(paramNode);
    addChildren(0, new Node[] { paramNode });
    return this;
  }
  
  public Element prependElement(String paramString)
  {
    paramString = new Element(Tag.valueOf(paramString), baseUri());
    prependChild(paramString);
    return paramString;
  }
  
  public Element prependText(String paramString)
  {
    Validate.notNull(paramString);
    prependChild(new TextNode(paramString, baseUri()));
    return this;
  }
  
  public Element previousElementSibling()
  {
    Element localElement = null;
    if (this.parentNode == null) {}
    for (;;)
    {
      return localElement;
      Elements localElements = parent().children();
      Integer localInteger = indexInList(this, localElements);
      Validate.notNull(localInteger);
      if (localInteger.intValue() > 0) {
        localElement = (Element)localElements.get(localInteger.intValue() - 1);
      }
    }
  }
  
  public Element removeClass(String paramString)
  {
    Validate.notNull(paramString);
    Set localSet = classNames();
    localSet.remove(paramString);
    classNames(localSet);
    return this;
  }
  
  public Elements select(String paramString)
  {
    return Selector.select(paramString, this);
  }
  
  public Elements siblingElements()
  {
    if (this.parentNode == null)
    {
      localObject = new Elements(0);
      return (Elements)localObject;
    }
    Object localObject = parent().children();
    Elements localElements = new Elements(((List)localObject).size() - 1);
    Iterator localIterator = ((List)localObject).iterator();
    for (;;)
    {
      localObject = localElements;
      if (!localIterator.hasNext()) {
        break;
      }
      localObject = (Element)localIterator.next();
      if (localObject != this) {
        localElements.add(localObject);
      }
    }
  }
  
  public Tag tag()
  {
    return this.tag;
  }
  
  public String tagName()
  {
    return this.tag.getName();
  }
  
  public Element tagName(String paramString)
  {
    Validate.notEmpty(paramString, "Tag name must not be empty.");
    this.tag = Tag.valueOf(paramString, ParseSettings.preserveCase);
    return this;
  }
  
  public String text()
  {
    final StringBuilder localStringBuilder = new StringBuilder();
    new NodeTraversor(new NodeVisitor()
    {
      public void head(Node paramAnonymousNode, int paramAnonymousInt)
      {
        if ((paramAnonymousNode instanceof TextNode))
        {
          paramAnonymousNode = (TextNode)paramAnonymousNode;
          Element.appendNormalisedText(localStringBuilder, paramAnonymousNode);
        }
        for (;;)
        {
          return;
          if ((paramAnonymousNode instanceof Element))
          {
            paramAnonymousNode = (Element)paramAnonymousNode;
            if ((localStringBuilder.length() > 0) && ((paramAnonymousNode.isBlock()) || (paramAnonymousNode.tag.getName().equals("br"))) && (!TextNode.lastCharIsWhitespace(localStringBuilder))) {
              localStringBuilder.append(" ");
            }
          }
        }
      }
      
      public void tail(Node paramAnonymousNode, int paramAnonymousInt) {}
    }).traverse(this);
    return localStringBuilder.toString().trim();
  }
  
  public Element text(String paramString)
  {
    Validate.notNull(paramString);
    empty();
    appendChild(new TextNode(paramString, this.baseUri));
    return this;
  }
  
  public List<TextNode> textNodes()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.childNodes.iterator();
    while (localIterator.hasNext())
    {
      Node localNode = (Node)localIterator.next();
      if ((localNode instanceof TextNode)) {
        localArrayList.add((TextNode)localNode);
      }
    }
    return Collections.unmodifiableList(localArrayList);
  }
  
  public String toString()
  {
    return outerHtml();
  }
  
  public Element toggleClass(String paramString)
  {
    Validate.notNull(paramString);
    Set localSet = classNames();
    if (localSet.contains(paramString)) {
      localSet.remove(paramString);
    }
    for (;;)
    {
      classNames(localSet);
      return this;
      localSet.add(paramString);
    }
  }
  
  public String val()
  {
    if (tagName().equals("textarea")) {}
    for (String str = text();; str = attr("value")) {
      return str;
    }
  }
  
  public Element val(String paramString)
  {
    if (tagName().equals("textarea")) {
      text(paramString);
    }
    for (;;)
    {
      return this;
      attr("value", paramString);
    }
  }
  
  public Element wrap(String paramString)
  {
    return (Element)super.wrap(paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\nodes\Element.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */