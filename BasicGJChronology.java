package org.joda.time.chrono;

import org.joda.time.Chronology;
import org.joda.time.DateTimeField;

abstract class BasicGJChronology
  extends BasicChronology
{
  private static final long FEB_29 = 5097600000L;
  private static final int[] MAX_DAYS_PER_MONTH_ARRAY;
  private static final long[] MAX_TOTAL_MILLIS_BY_MONTH_ARRAY;
  private static final int[] MIN_DAYS_PER_MONTH_ARRAY;
  private static final long[] MIN_TOTAL_MILLIS_BY_MONTH_ARRAY;
  private static final long serialVersionUID = 538276888268L;
  
  static
  {
    long l1 = 0L;
    MIN_DAYS_PER_MONTH_ARRAY = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    MAX_DAYS_PER_MONTH_ARRAY = new int[] { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    MIN_TOTAL_MILLIS_BY_MONTH_ARRAY = new long[12];
    MAX_TOTAL_MILLIS_BY_MONTH_ARRAY = new long[12];
    int i = 0;
    long l2 = 0L;
    while (i < 11)
    {
      l2 += MIN_DAYS_PER_MONTH_ARRAY[i] * 86400000L;
      MIN_TOTAL_MILLIS_BY_MONTH_ARRAY[(i + 1)] = l2;
      l1 += MAX_DAYS_PER_MONTH_ARRAY[i] * 86400000L;
      MAX_TOTAL_MILLIS_BY_MONTH_ARRAY[(i + 1)] = l1;
      i++;
    }
  }
  
  BasicGJChronology(Chronology paramChronology, Object paramObject, int paramInt)
  {
    super(paramChronology, paramObject, paramInt);
  }
  
  int getDaysInMonthMax(int paramInt)
  {
    return MAX_DAYS_PER_MONTH_ARRAY[(paramInt - 1)];
  }
  
  int getDaysInMonthMaxForSet(long paramLong, int paramInt)
  {
    int i = 28;
    if ((paramInt > 28) || (paramInt < 1)) {
      i = getDaysInMonthMax(paramLong);
    }
    return i;
  }
  
  int getDaysInYearMonth(int paramInt1, int paramInt2)
  {
    if (isLeapYear(paramInt1)) {}
    for (paramInt1 = MAX_DAYS_PER_MONTH_ARRAY[(paramInt2 - 1)];; paramInt1 = MIN_DAYS_PER_MONTH_ARRAY[(paramInt2 - 1)]) {
      return paramInt1;
    }
  }
  
  int getMonthOfYear(long paramLong, int paramInt)
  {
    int i = 1;
    int j = (int)(paramLong - getYearMillis(paramInt) >> 10);
    if (isLeapYear(paramInt)) {
      if (j < 15356250) {
        if (j < 7678125) {
          if (j < 2615625) {
            paramInt = i;
          }
        }
      }
    }
    for (;;)
    {
      return paramInt;
      if (j < 5062500)
      {
        paramInt = 2;
      }
      else
      {
        paramInt = 3;
        continue;
        if (j < 10209375)
        {
          paramInt = 4;
        }
        else if (j < 12825000)
        {
          paramInt = 5;
        }
        else
        {
          paramInt = 6;
          continue;
          if (j < 23118750)
          {
            if (j < 17971875) {
              paramInt = 7;
            } else if (j < 20587500) {
              paramInt = 8;
            } else {
              paramInt = 9;
            }
          }
          else if (j < 25734375)
          {
            paramInt = 10;
          }
          else if (j < 28265625)
          {
            paramInt = 11;
          }
          else
          {
            paramInt = 12;
            continue;
            if (j < 15271875)
            {
              if (j < 7593750)
              {
                paramInt = i;
                if (j >= 2615625) {
                  if (j < 4978125) {
                    paramInt = 2;
                  } else {
                    paramInt = 3;
                  }
                }
              }
              else if (j < 10125000)
              {
                paramInt = 4;
              }
              else if (j < 12740625)
              {
                paramInt = 5;
              }
              else
              {
                paramInt = 6;
              }
            }
            else if (j < 23034375)
            {
              if (j < 17887500) {
                paramInt = 7;
              } else if (j < 20503125) {
                paramInt = 8;
              } else {
                paramInt = 9;
              }
            }
            else if (j < 25650000) {
              paramInt = 10;
            } else if (j < 28181250) {
              paramInt = 11;
            } else {
              paramInt = 12;
            }
          }
        }
      }
    }
  }
  
  long getTotalMillisByYearMonth(int paramInt1, int paramInt2)
  {
    if (isLeapYear(paramInt1)) {}
    for (long l = MAX_TOTAL_MILLIS_BY_MONTH_ARRAY[(paramInt2 - 1)];; l = MIN_TOTAL_MILLIS_BY_MONTH_ARRAY[(paramInt2 - 1)]) {
      return l;
    }
  }
  
  long getYearDifference(long paramLong1, long paramLong2)
  {
    int i = getYear(paramLong1);
    int j = getYear(paramLong2);
    long l = paramLong1 - getYearMillis(i);
    paramLong1 = paramLong2 - getYearMillis(j);
    if (paramLong1 >= 5097600000L) {
      if (isLeapYear(j))
      {
        if (isLeapYear(i)) {
          break label123;
        }
        paramLong2 = l;
        paramLong1 -= 86400000L;
      }
    }
    for (;;)
    {
      j = i - j;
      i = j;
      if (paramLong2 < paramLong1) {
        i = j - 1;
      }
      return i;
      if ((l >= 5097600000L) && (isLeapYear(i))) {
        paramLong2 = l - 86400000L;
      } else {
        label123:
        paramLong2 = l;
      }
    }
  }
  
  boolean isLeapDay(long paramLong)
  {
    if ((dayOfMonth().get(paramLong) == 29) && (monthOfYear().isLeap(paramLong))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  long setYear(long paramLong, int paramInt)
  {
    int m = getYear(paramLong);
    int j = getDayOfYear(paramLong, m);
    int k = getMillisOfDay(paramLong);
    int i = j;
    if (j > 59)
    {
      if (!isLeapYear(m)) {
        break label74;
      }
      i = j;
      if (!isLeapYear(paramInt)) {
        i = j - 1;
      }
    }
    for (;;)
    {
      return getYearMonthDayMillis(paramInt, 1, i) + k;
      label74:
      i = j;
      if (isLeapYear(paramInt)) {
        i = j + 1;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BasicGJChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */