package tech.dcube.companion.managers.server;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.backend.PBRequest.PBResponse;
import tech.dcube.companion.managers.server.backend.VolleySingleton;
import tech.dcube.companion.model.PBUser;
import tech.dcube.companion.model.PBUser.PBUserAuth;
import tech.dcube.companion.model.PBUser.PBUserProfile;

class LoginManager<T>
  implements ServerManager.ResponseHandler
{
  private static final DataManager dataManager = DataManager.getInstance();
  private static final LoginManager ourInstance = new LoginManager();
  private ServerManager.RequestCompletionHandler<PBUser> completionHandler;
  private Request reqAccessToken;
  private Request reqAccessTokenSL;
  private Request reqAccessTokenSP;
  private Request reqAuthTokenSP;
  private Request reqDataTokenSL;
  private Request reqRefreshAccessToken;
  private Request reqSessionToken;
  
  private String getAccessTokenFromHtml(String paramString)
  {
    paramString = Jsoup.parse(paramString);
    if (paramString != null)
    {
      paramString = paramString.body().getElementById("appForm");
      if (paramString != null)
      {
        Iterator localIterator = paramString.children().iterator();
        do
        {
          if (!localIterator.hasNext()) {
            break;
          }
          paramString = (Element)localIterator.next();
        } while (!paramString.attr("name").equalsIgnoreCase("access_token"));
      }
    }
    for (paramString = paramString.attr("value");; paramString = null) {
      return paramString;
    }
  }
  
  private String getIdTokenFromJavascript(String paramString)
  {
    String[] arrayOfString = paramString.substring(paramString.indexOf("<script type=\"text/javascript\">"), paramString.indexOf("</script>")).replace("<script type=\"text/javascript\">", "").split("\n");
    int j = arrayOfString.length;
    int i = 0;
    if (i < j)
    {
      paramString = arrayOfString[i];
      if (!paramString.contains("data.id_token")) {}
    }
    for (paramString = paramString.split("=")[1].trim().replaceAll("'", "").replaceAll(";", "");; paramString = null)
    {
      return paramString;
      i++;
      break;
    }
  }
  
  static LoginManager getInstance()
  {
    return ourInstance;
  }
  
  private void getSendProAccessToken(final Context paramContext, String paramString, final ServerManager.ResponseHandler paramResponseHandler)
  {
    Log.wtf(getClass().getName(), "SP ACCESS");
    this.reqAccessTokenSP = ServerManager.createRequest(0, ((String)ServerManager.URL_MAP.get(ServerManager.Type.SP_ACCESS_TOKEN)).replace("{{sessionToken}}", paramString), "application/json", null, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onResponse(paramContext, LoginManager.this.reqAccessTokenSP, paramAnonymousPBResponse);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onError(paramContext, LoginManager.this.reqAccessTokenSP, paramAnonymousVolleyError);
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAccessTokenSP);
  }
  
  private void getSendProAuthToken(final Context paramContext, String paramString, final ServerManager.ResponseHandler paramResponseHandler)
  {
    Log.wtf(getClass().getName(), "SP AUTH");
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SP_AUTH_TOKEN);
    HashMap localHashMap = new HashMap();
    localHashMap.put("Authorization", String.format("Bearer %s", new Object[] { paramString }));
    this.reqAuthTokenSP = ServerManager.createRequest(1, str, "application/json", localHashMap, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onResponse(paramContext, LoginManager.this.reqAuthTokenSP, paramAnonymousPBResponse);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onError(paramContext, LoginManager.this.reqAuthTokenSP, paramAnonymousVolleyError);
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAuthTokenSP);
  }
  
  private void getSessionToken(Context paramContext, boolean paramBoolean)
  {
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.GET_SESSION_TOKEN);
    HashMap localHashMap = new HashMap();
    localHashMap.put("username", dataManager.getUser().getProfile().getEmail());
    localHashMap.put("password", dataManager.getUser().getProfile().getPassword());
    getSessionToken(paramContext, str, localHashMap, paramBoolean, this.completionHandler);
  }
  
  private void getSmartLinkAccessToken(final Context paramContext, String paramString, final ServerManager.ResponseHandler paramResponseHandler)
  {
    Log.wtf(getClass().getName(), "SL ACCESS TOKEN");
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.SL_ACCESS_TOKEN);
    HashMap localHashMap = new HashMap();
    localHashMap.put("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer");
    localHashMap.put("assertion", paramString);
    this.reqAccessTokenSL = ServerManager.createRequest(1, str, "application/x-www-form-urlencoded", null, localHashMap, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onResponse(paramContext, LoginManager.this.reqAccessTokenSL, paramAnonymousPBResponse);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onError(paramContext, LoginManager.this.reqAccessTokenSL, paramAnonymousVolleyError);
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAccessTokenSL);
  }
  
  private void getSmartLinkDataToken(final Context paramContext, String paramString, final ServerManager.ResponseHandler paramResponseHandler)
  {
    Log.wtf(getClass().getName(), "SL DATA");
    this.reqDataTokenSL = ServerManager.createRequest(0, ((String)ServerManager.URL_MAP.get(ServerManager.Type.SL_DATA_TOKEN)).replace("{{sessionToken}}", paramString), "application/json", null, "", new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onResponse(paramContext, LoginManager.this.reqDataTokenSL, paramAnonymousPBResponse);
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramResponseHandler != null) {
          paramResponseHandler.onError(paramContext, LoginManager.this.reqDataTokenSL, paramAnonymousVolleyError);
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqDataTokenSL);
  }
  
  private void handleAccessTokenSuccess(final Context paramContext, final Request paramRequest, final PBRequest.PBResponse paramPBResponse, final ServerManager.RequestCompletionHandler<PBUser> paramRequestCompletionHandler)
  {
    Map localMap = (Map)new Gson().fromJson(paramPBResponse.body, Map.class);
    if ((localMap != null) && (localMap.get("access_token") != null))
    {
      paramPBResponse = new PBUser();
      final String str2 = localMap.get("access_token").toString();
      String str1 = localMap.get("refresh_token").toString();
      long l1 = 0L;
      Log.wtf("Expiry ", localMap.get("expires_in") + "");
      try
      {
        l2 = Long.valueOf(localMap.get("expires_in").toString()).longValue();
        l1 = l2;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        for (;;)
        {
          long l2;
          Log.e(getClass().getName(), "Invalid Token Expiry");
        }
      }
      l2 = System.currentTimeMillis();
      paramPBResponse.getAuth().setAccessTokenSendPro(str2);
      paramPBResponse.getAuth().setIdTokenSmartLink(str2);
      paramPBResponse.getAuth().setRefreshToken(str1);
      paramPBResponse.getAuth().setTokenExpiry(1000L * l1 + l2);
      dataManager.setUser(paramPBResponse);
      ourInstance.getSendProAuthToken(paramContext, str2, new ServerManager.ResponseHandler()
      {
        private void processRequest()
        {
          LoginManager.ourInstance.getSmartLinkAccessToken(paramContext, str2, new ServerManager.ResponseHandler()
          {
            private void sendResponse()
            {
              int i;
              if ((LoginManager.15.this.val$user.getAuth().isSmartLinkLoggedIn()) || (LoginManager.15.this.val$user.getAuth().isSendProLoggedIn()))
              {
                i = 1;
                if (i == 0) {
                  break label73;
                }
                ServerManager.sendSuccess(LoginManager.15.this.val$request.getTag().toString(), LoginManager.dataManager.getUser(), LoginManager.15.this.val$completionHandler);
              }
              for (;;)
              {
                return;
                i = 0;
                break;
                label73:
                ServerManager.sendError(LoginManager.15.this.val$request.getTag().toString(), "Login Failed!", LoginManager.15.this.val$completionHandler);
              }
            }
            
            public void onError(Context paramAnonymous2Context, Request paramAnonymous2Request, VolleyError paramAnonymous2VolleyError)
            {
              sendResponse();
            }
            
            public void onResponse(Context paramAnonymous2Context, Request paramAnonymous2Request, PBRequest.PBResponse paramAnonymous2PBResponse)
            {
              paramAnonymous2Request = (Map)new Gson().fromJson(paramAnonymous2PBResponse.body, Map.class);
              if ((paramAnonymous2Request != null) && (paramAnonymous2Request.get("access_token") != null))
              {
                paramAnonymous2Context = paramAnonymous2Request.get("access_token").toString();
                paramAnonymous2Request = paramAnonymous2Request.get("clientID").toString();
                LoginManager.dataManager.getUser().getAuth().setAccessTokenSmartLink(paramAnonymous2Context);
                LoginManager.dataManager.getUser().getAuth().setClientIdSmartLink(paramAnonymous2Request);
              }
              sendResponse();
            }
          });
        }
        
        public void onError(Context paramAnonymousContext, Request paramAnonymousRequest, VolleyError paramAnonymousVolleyError)
        {
          processRequest();
        }
        
        public void onResponse(Context paramAnonymousContext, Request paramAnonymousRequest, PBRequest.PBResponse paramAnonymousPBResponse)
        {
          paramAnonymousContext = (String)paramAnonymousPBResponse.headers.get("authToken");
          paramAnonymousRequest = (String)paramAnonymousPBResponse.headers.get("clientid");
          if (paramAnonymousContext != null)
          {
            LoginManager.dataManager.getUser().getAuth().setAuthTokenSendPro(paramAnonymousContext);
            LoginManager.dataManager.getUser().getAuth().setClientIdSendPro(paramAnonymousRequest);
          }
          processRequest();
        }
      });
    }
    for (;;)
    {
      return;
      ServerManager.sendError(paramRequest.getTag().toString(), "Failed to get Access Token", paramRequestCompletionHandler);
    }
  }
  
  String getAccessToken(final Context paramContext, String paramString, final ServerManager.RequestCompletionHandler<PBUser> paramRequestCompletionHandler)
  {
    Log.wtf(getClass().getName(), "ACCESS TOKEN");
    String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.GET_ACCESS_TOKEN);
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("Accept", "application/json");
    localHashMap1.put("Cache-Control", "no-cache");
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("grant_type", "authorization_code");
    localHashMap2.put("redirect_uri", "http://localhost:8080/home/");
    localHashMap2.put("client_secret", "NdwU-tcev0nkHEXJRLcGKGC7-rNQMrjwD8_ZCs5X");
    localHashMap2.put("client_id", "wTvlYFbGmfqgIlMdnKxw");
    localHashMap2.put("code", paramString);
    this.reqAccessToken = ServerManager.createRequest(1, str, "application/x-www-form-urlencoded", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        LoginManager.this.handleAccessTokenSuccess(paramContext, LoginManager.this.reqAccessToken, paramAnonymousPBResponse, paramRequestCompletionHandler);
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        Log.wtf("AT Error", "err", paramAnonymousVolleyError.getCause());
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onError(paramAnonymousVolleyError, LoginManager.this.reqAccessToken.getTag().toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqAccessToken);
    return this.reqAccessToken.getTag().toString();
  }
  
  String getSessionToken(final Context paramContext, String paramString, final Map<String, Object> paramMap, final boolean paramBoolean, final ServerManager.RequestCompletionHandler<PBUser> paramRequestCompletionHandler)
  {
    Log.wtf(getClass().getName(), "LOGIN");
    this.completionHandler = paramRequestCompletionHandler;
    this.reqSessionToken = ServerManager.createRequest(1, paramString, "application/json", null, paramMap, new Response.Listener()new Response.ErrorListener
    {
      public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
      {
        String str = null;
        PBUser localPBUser;
        if (LoginManager.dataManager.getUser() != null) {
          localPBUser = LoginManager.dataManager.getUser();
        }
        for (;;)
        {
          localPBUser.getProfile().setPassword(paramMap.get("password").toString());
          paramAnonymousPBResponse = new JsonParser().parse(paramAnonymousPBResponse.body).getAsJsonObject();
          Object localObject = paramAnonymousPBResponse.get("sessionToken");
          JsonElement localJsonElement = paramAnonymousPBResponse.get("expiresAt");
          if (localObject != null)
          {
            paramAnonymousPBResponse = ((JsonElement)localObject).getAsString();
            if (localObject != null) {
              str = localJsonElement.getAsString();
            }
            localObject = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
          }
          try
          {
            ((DateFormat)localObject).parse(str);
            if (paramBoolean)
            {
              localPBUser.getAuth().setSessionTokenSendPro(paramAnonymousPBResponse);
              LoginManager.this.getSendProAccessToken(paramContext, paramAnonymousPBResponse, LoginManager.this);
              LoginManager.dataManager.setUser(localPBUser);
              return;
              localPBUser = PBUser.fromJson(paramAnonymousPBResponse.body);
              continue;
              paramAnonymousPBResponse = null;
            }
          }
          catch (ParseException localParseException)
          {
            for (;;)
            {
              localParseException.printStackTrace();
              continue;
              localPBUser.getAuth().setSessionTokenSmartLink(paramAnonymousPBResponse);
              LoginManager.this.getSmartLinkDataToken(paramContext, paramAnonymousPBResponse, LoginManager.this);
            }
          }
        }
      }
    }, new Response.ErrorListener()
    {
      public void onErrorResponse(VolleyError paramAnonymousVolleyError)
      {
        if (paramRequestCompletionHandler != null) {
          paramRequestCompletionHandler.onError(paramAnonymousVolleyError, LoginManager.this.reqSessionToken.getTag().toString());
        }
      }
    });
    VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqSessionToken);
    return this.reqSessionToken.getTag().toString();
  }
  
  public void onError(Context paramContext, Request paramRequest, VolleyError paramVolleyError)
  {
    PBUser localPBUser = dataManager.getUser();
    PBUser.PBUserAuth localPBUserAuth = localPBUser.getAuth();
    Log.wtf(getClass().getName(), "Err: " + paramVolleyError.getMessage());
    if (paramRequest.equals(this.reqAccessTokenSP)) {
      getSessionToken(paramContext, false);
    }
    for (;;)
    {
      return;
      if (paramRequest.equals(this.reqAuthTokenSP)) {
        getSessionToken(paramContext, false);
      } else if (paramRequest.equals(this.reqDataTokenSL))
      {
        if ((localPBUserAuth.isSendProLoggedIn()) || (localPBUserAuth.isSmartLinkLoggedIn())) {
          ServerManager.sendSuccess(paramRequest.getTag().toString(), localPBUser, this.completionHandler);
        } else {
          ServerManager.sendError(paramRequest.getTag().toString(), "Unable to Login", this.completionHandler);
        }
      }
      else if (paramRequest.equals(this.reqAccessTokenSL))
      {
        if ((localPBUserAuth.isSendProLoggedIn()) || (localPBUserAuth.isSmartLinkLoggedIn())) {
          ServerManager.sendSuccess(paramRequest.getTag().toString(), localPBUser, this.completionHandler);
        } else {
          ServerManager.sendError(paramRequest.getTag().toString(), "Unable to Login", this.completionHandler);
        }
      }
      else {
        ServerManager.sendError(paramRequest.getTag().toString(), "Unknown Result", this.completionHandler);
      }
    }
  }
  
  public void onResponse(Context paramContext, Request paramRequest, PBRequest.PBResponse paramPBResponse)
  {
    if (paramRequest.equals(this.reqAccessTokenSP))
    {
      paramPBResponse = getAccessTokenFromHtml(paramPBResponse.body);
      if (paramPBResponse != null)
      {
        dataManager.getUser().getAuth().setAccessTokenSendPro(paramPBResponse);
        getSendProAuthToken(paramContext, paramPBResponse, this);
      }
    }
    for (;;)
    {
      return;
      onError(paramContext, paramRequest, new VolleyError("Failed to Get SendPro Access Token"));
      continue;
      Object localObject;
      if (paramRequest.equals(this.reqAuthTokenSP))
      {
        localObject = (String)paramPBResponse.headers.get("authToken");
        paramPBResponse = (String)paramPBResponse.headers.get("clientid");
        if (localObject != null)
        {
          dataManager.getUser().getAuth().setAuthTokenSendPro((String)localObject);
          dataManager.getUser().getAuth().setClientIdSendPro(paramPBResponse);
          getSessionToken(paramContext, false);
        }
        else
        {
          onError(paramContext, paramRequest, new VolleyError("Failed to get SendPro Auth Token"));
        }
      }
      else if (paramRequest.equals(this.reqDataTokenSL))
      {
        paramPBResponse = getIdTokenFromJavascript(paramPBResponse.body);
        if (paramPBResponse != null)
        {
          dataManager.getUser().getAuth().setIdTokenSmartLink(paramPBResponse);
          getSmartLinkAccessToken(paramContext, paramPBResponse, this);
        }
        else
        {
          onError(paramContext, paramRequest, new VolleyError("Failed to get SmartLink Id Token"));
        }
      }
      else if (paramRequest.equals(this.reqAccessTokenSL))
      {
        localObject = new JsonParser().parse(paramPBResponse.body).getAsJsonObject();
        if (localObject != null)
        {
          paramPBResponse = ((JsonObject)localObject).get("access_token");
          if (paramPBResponse != null)
          {
            dataManager.getUser().getAuth().setAccessTokenSmartLink(paramPBResponse.getAsString());
            paramPBResponse = ((JsonObject)localObject).get("clientID");
            if (paramPBResponse != null) {
              dataManager.getUser().getAuth().setClientIdSmartLink(paramPBResponse.getAsString());
            }
            paramPBResponse = dataManager.getUser().getAuth();
            if ((paramPBResponse.isSendProLoggedIn()) || (paramPBResponse.isSmartLinkLoggedIn())) {
              ServerManager.sendSuccess(paramRequest.getTag().toString(), dataManager.getUser(), this.completionHandler);
            } else {
              onError(paramContext, paramRequest, new VolleyError("Unable to Login"));
            }
          }
          else
          {
            onError(paramContext, paramRequest, new VolleyError("Failed to get SmartLink Access Token"));
          }
        }
        else
        {
          onError(paramContext, paramRequest, new VolleyError("Failed to get SmartLink Access Token"));
        }
      }
      else
      {
        ServerManager.sendError(paramRequest.getTag().toString(), "Unknown Result", this.completionHandler);
      }
    }
  }
  
  String refreshAccessToken(final Context paramContext, final ServerManager.RequestCompletionHandler<PBUser> paramRequestCompletionHandler)
  {
    if (dataManager.getUser() == null) {}
    for (paramContext = "";; paramContext = this.reqRefreshAccessToken.getTag().toString())
    {
      return paramContext;
      Log.wtf(getClass().getName(), "ACCESS TOKEN");
      String str = (String)ServerManager.URL_MAP.get(ServerManager.Type.GET_ACCESS_TOKEN);
      HashMap localHashMap1 = new HashMap();
      localHashMap1.put("Accept", "application/json");
      HashMap localHashMap2 = new HashMap();
      localHashMap2.put("grant_type", "refresh_token");
      localHashMap2.put("client_secret", "NdwU-tcev0nkHEXJRLcGKGC7-rNQMrjwD8_ZCs5X");
      localHashMap2.put("client_id", "wTvlYFbGmfqgIlMdnKxw");
      localHashMap2.put("refresh_token", dataManager.getUser().getAuth().getRefreshToken());
      this.reqRefreshAccessToken = ServerManager.createRequest(1, str, "application/x-www-form-urlencoded", localHashMap1, localHashMap2, new Response.Listener()new Response.ErrorListener
      {
        public void onResponse(PBRequest.PBResponse paramAnonymousPBResponse)
        {
          LoginManager.this.handleAccessTokenSuccess(paramContext, LoginManager.this.reqRefreshAccessToken, paramAnonymousPBResponse, paramRequestCompletionHandler);
        }
      }, new Response.ErrorListener()
      {
        public void onErrorResponse(VolleyError paramAnonymousVolleyError)
        {
          Log.wtf("AT Error", "err", paramAnonymousVolleyError.getCause());
          if (paramRequestCompletionHandler != null) {
            paramRequestCompletionHandler.onError(paramAnonymousVolleyError, LoginManager.this.reqRefreshAccessToken.getTag().toString());
          }
        }
      });
      VolleySingleton.getInstance(paramContext).addToRequestQueue(this.reqRefreshAccessToken);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\LoginManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */