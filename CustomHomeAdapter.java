package tech.dcube.companion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.TreeSet;

public class CustomHomeAdapter
  extends BaseAdapter
{
  private static final int TYPE_ITEM = 0;
  private static final int TYPE_SEPARATOR = 1;
  private ArrayList<String> mData = new ArrayList();
  private LayoutInflater mInflater;
  private TreeSet<Integer> sectionHeader = new TreeSet();
  
  public CustomHomeAdapter(Context paramContext)
  {
    this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }
  
  public void addItem(String paramString)
  {
    this.mData.add(paramString);
    notifyDataSetChanged();
  }
  
  public void addSectionHeaderItem(String paramString)
  {
    this.mData.add(paramString);
    this.sectionHeader.add(Integer.valueOf(this.mData.size() - 1));
    notifyDataSetChanged();
  }
  
  public int getCount()
  {
    return this.mData.size();
  }
  
  public String getItem(int paramInt)
  {
    return (String)this.mData.get(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public int getItemViewType(int paramInt)
  {
    if (this.sectionHeader.contains(Integer.valueOf(paramInt))) {}
    for (paramInt = 1;; paramInt = 0) {
      return paramInt;
    }
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramInt = getItemViewType(paramInt);
    if (paramView == null)
    {
      paramViewGroup = new ViewHolder();
      switch (paramInt)
      {
      default: 
        paramView.setTag(paramViewGroup);
      }
    }
    for (;;)
    {
      return paramView;
      paramView = this.mInflater.inflate(2130968653, null);
      paramViewGroup.textView = ((TextView)paramView.findViewById(2131624331));
      break;
      paramView = this.mInflater.inflate(2130968652, null);
      paramViewGroup.textView = ((TextView)paramView.findViewById(2131624253));
      break;
      paramViewGroup = (ViewHolder)paramView.getTag();
    }
  }
  
  public int getViewTypeCount()
  {
    return 2;
  }
  
  public static class ViewHolder
  {
    public TextView textView;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\adapters\CustomHomeAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */