package com.testfairy.m;

import android.util.Log;
import com.testfairy.e;
import com.testfairy.e.a;
import com.testfairy.g;
import com.testfairy.h.c;
import com.testfairy.i;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class b
{
  private static final String a = "testfairy-events";
  private static final int b = 64;
  private static final int c = 10000;
  private static final int d = 4096;
  private static final String e = "events";
  private static final String q = "anonymous-" + System.currentTimeMillis();
  private static final int r = 1000;
  private static final int s = 15000;
  private Vector f = new Vector();
  private final Object g = new Object();
  private a h;
  private String i;
  private List j = new Vector();
  private i k;
  private a l;
  private int m = 0;
  private com.testfairy.k.d n;
  private boolean o = false;
  private boolean p = false;
  private int t = 1000;
  private boolean u = false;
  private boolean v = false;
  private int w = 0;
  
  private List a(int paramInt)
  {
    Vector localVector = new Vector(paramInt);
    Object localObject1 = this.g;
    for (;;)
    {
      if (paramInt > 0) {}
      try
      {
        if (this.f.size() > 0)
        {
          localVector.add((g)this.f.remove(0));
          paramInt--;
          continue;
        }
        return localVector;
      }
      finally {}
    }
  }
  
  private void a(String paramString1, String arg2)
  {
    if (this.i == null) {
      Log.v(e.a, "Cannot save events to disk, path is not defined");
    }
    for (;;)
    {
      return;
      this.m += 1;
      String str1 = this.i + "/" + "testfairy-events" + "." + paramString1 + "." + String.format("%06d", new Object[] { Integer.valueOf(this.m) });
      Log.v(e.a, "Writing " + ???.length() + " bytes to disk at " + str1);
      try
      {
        com.testfairy.p.d.a(str1, ???.getBytes("UTF-8"));
        String str2 = e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.v(str2, "Saved to disk " + ???.getBytes().length + " bytes");
        synchronized (this.g)
        {
          this.j.add(str1);
        }
      }
      catch (Exception ???)
      {
        Log.e(e.a, "Could not save persistent data for token " + paramString1, ???);
      }
    }
  }
  
  private static JSONArray b(List paramList)
  {
    JSONArray localJSONArray = new JSONArray();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localJSONArray.put(((g)paramList.next()).a());
    }
    return localJSONArray;
  }
  
  private static String c(String paramString)
  {
    int i1 = paramString.lastIndexOf("/testfairy-events.");
    int i2;
    if (i1 >= 0)
    {
      i1 = "/testfairy-events.".length() + i1;
      i2 = paramString.indexOf('.', i1);
      if (i2 <= 0) {}
    }
    for (paramString = paramString.substring(i1, i2);; paramString = null)
    {
      return paramString;
      Log.e(e.a, "Could not parse token from " + paramString);
    }
  }
  
  private void g()
  {
    if (this.k != null) {
      this.h.a(this.k.a());
    }
  }
  
  public void a()
  {
    if (this.h != null) {
      this.h.a();
    }
  }
  
  public void a(a parama)
  {
    this.l = parama;
  }
  
  public void a(g paramg)
  {
    if (e()) {}
    for (;;)
    {
      return;
      synchronized (this.g)
      {
        if (this.f.size() < 4096) {
          this.f.add(paramg);
        }
      }
    }
  }
  
  public void a(i parami)
  {
    this.k = parami;
    g();
  }
  
  public void a(com.testfairy.k.d paramd)
  {
    this.n = paramd;
  }
  
  public void a(String paramString)
  {
    this.i = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    this.p = paramBoolean;
  }
  
  public void b()
  {
    this.h = new a();
    this.h.start();
  }
  
  public void c()
  {
    a(true);
    a();
    if (this.h != null) {
      this.h.b();
    }
  }
  
  public void d()
  {
    Log.v(e.a, "Enabling offline-mode for this session");
    this.v = true;
    a();
  }
  
  public boolean e()
  {
    return this.p;
  }
  
  public String toString()
  {
    String str = "EventQueue " + super.toString() + "\n";
    synchronized (this.g)
    {
      Object localObject3 = this.f.iterator();
      while (((Iterator)localObject3).hasNext())
      {
        g localg = (g)((Iterator)localObject3).next();
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        str = str + localg.toString() + "\n";
      }
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      str = str + " (size = " + this.f.size() + ")";
      return str;
    }
  }
  
  private class a
    extends Thread
  {
    private boolean b = false;
    private boolean c = false;
    
    public a()
    {
      super();
    }
    
    private void a(long paramLong)
    {
      try
      {
        Thread.sleep(paramLong);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
    
    private void a(List paramList)
    {
      try
      {
        if (paramList.size() == 0) {}
        for (;;)
        {
          return;
          localObject = b.a(paramList);
          paramList = new org/json/JSONObject;
          paramList.<init>();
          paramList.put("events", localObject);
          paramList = paramList.toString();
          if (b.a(b.this) != null) {
            break;
          }
          String str = e.a;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          Log.v(str, "Session has not been started, saving this batch to disk under token " + b.f());
          b.a(b.this, b.f(), paramList);
        }
      }
      catch (Exception paramList)
      {
        for (;;)
        {
          Log.e(e.a, "Failed to send events over the wire", paramList);
          b.a(b.this, false);
          continue;
          b.a(b.this, true);
          Object localObject = new com/testfairy/m/b$b;
          ((b.b)localObject).<init>(b.this, paramList);
          b.e(b.this).a("20170625-78d2bc1", b.a(b.this).a(), paramList, (c)localObject);
        }
      }
    }
    
    private void b(String paramString)
    {
      try
      {
        String str2 = com.testfairy.p.d.a(paramString);
        String str1 = b.b(paramString);
        b.a(b.this, true);
        com.testfairy.k.d locald = b.e(b.this);
        b.c localc = new com/testfairy/m/b$c;
        localc.<init>(b.this, paramString, null);
        locald.a("20170625-78d2bc1", str1, str2, localc);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          b.a(b.this, false);
          Log.e(e.a, "Exception while reading persistent file " + paramString, localException);
        }
      }
    }
    
    /* Error */
    private void c()
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore_1
      //   2: aload_0
      //   3: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   6: invokestatic 133	com/testfairy/m/b:c	(Lcom/testfairy/m/b;)Ljava/lang/Object;
      //   9: astore_3
      //   10: aload_3
      //   11: monitorenter
      //   12: aload_0
      //   13: getfield 24	com/testfairy/m/b$a:c	Z
      //   16: ifne +18 -> 34
      //   19: aload_0
      //   20: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   23: invokestatic 136	com/testfairy/m/b:f	(Lcom/testfairy/m/b;)Ljava/util/Vector;
      //   26: invokevirtual 139	java/util/Vector:size	()I
      //   29: bipush 64
      //   31: if_icmplt +35 -> 66
      //   34: aload_0
      //   35: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   38: bipush 64
      //   40: invokestatic 142	com/testfairy/m/b:b	(Lcom/testfairy/m/b;I)Ljava/util/List;
      //   43: astore_2
      //   44: aload_2
      //   45: astore_1
      //   46: aload_0
      //   47: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   50: invokestatic 136	com/testfairy/m/b:f	(Lcom/testfairy/m/b;)Ljava/util/Vector;
      //   53: invokevirtual 146	java/util/Vector:isEmpty	()Z
      //   56: ifeq +10 -> 66
      //   59: aload_0
      //   60: iconst_0
      //   61: putfield 24	com/testfairy/m/b$a:c	Z
      //   64: aload_2
      //   65: astore_1
      //   66: aload_3
      //   67: monitorexit
      //   68: aload_1
      //   69: ifnull +59 -> 128
      //   72: aload_1
      //   73: invokestatic 43	com/testfairy/m/b:a	(Ljava/util/List;)Lorg/json/JSONArray;
      //   76: astore_1
      //   77: new 45	org/json/JSONObject
      //   80: astore_2
      //   81: aload_2
      //   82: invokespecial 48	org/json/JSONObject:<init>	()V
      //   85: aload_2
      //   86: ldc 50
      //   88: aload_1
      //   89: invokevirtual 54	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
      //   92: pop
      //   93: aload_2
      //   94: invokevirtual 58	org/json/JSONObject:toString	()Ljava/lang/String;
      //   97: astore_2
      //   98: aload_0
      //   99: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   102: invokestatic 61	com/testfairy/m/b:a	(Lcom/testfairy/m/b;)Lcom/testfairy/i;
      //   105: ifnull +43 -> 148
      //   108: aload_0
      //   109: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   112: invokestatic 61	com/testfairy/m/b:a	(Lcom/testfairy/m/b;)Lcom/testfairy/i;
      //   115: invokevirtual 111	com/testfairy/i:a	()Ljava/lang/String;
      //   118: astore_1
      //   119: aload_0
      //   120: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   123: aload_1
      //   124: aload_2
      //   125: invokestatic 88	com/testfairy/m/b:a	(Lcom/testfairy/m/b;Ljava/lang/String;Ljava/lang/String;)V
      //   128: return
      //   129: astore_1
      //   130: aload_3
      //   131: monitorexit
      //   132: aload_1
      //   133: athrow
      //   134: astore_1
      //   135: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   138: ldc -108
      //   140: aload_1
      //   141: invokestatic 94	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   144: pop
      //   145: goto -17 -> 128
      //   148: invokestatic 78	com/testfairy/m/b:f	()Ljava/lang/String;
      //   151: astore_1
      //   152: goto -33 -> 119
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	155	0	this	a
      //   1	123	1	localObject1	Object
      //   129	4	1	localObject2	Object
      //   134	7	1	localException	Exception
      //   151	1	1	str	String
      //   43	82	2	localObject3	Object
      // Exception table:
      //   from	to	target	type
      //   12	19	129	finally
      //   19	34	129	finally
      //   34	44	129	finally
      //   46	64	129	finally
      //   66	68	129	finally
      //   130	132	129	finally
      //   2	12	134	java/lang/Exception
      //   72	119	134	java/lang/Exception
      //   119	128	134	java/lang/Exception
      //   132	134	134	java/lang/Exception
      //   148	152	134	java/lang/Exception
    }
    
    /* Error */
    private void d()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   4: invokestatic 133	com/testfairy/m/b:c	(Lcom/testfairy/m/b;)Ljava/lang/Object;
      //   7: astore_2
      //   8: aload_2
      //   9: monitorenter
      //   10: aload_0
      //   11: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   14: invokestatic 154	com/testfairy/m/b:d	(Lcom/testfairy/m/b;)Ljava/util/List;
      //   17: invokeinterface 40 1 0
      //   22: ifle +91 -> 113
      //   25: aload_0
      //   26: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   29: invokestatic 136	com/testfairy/m/b:f	(Lcom/testfairy/m/b;)Ljava/util/Vector;
      //   32: invokevirtual 139	java/util/Vector:size	()I
      //   35: bipush 64
      //   37: if_icmplt +76 -> 113
      //   40: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   43: ldc -100
      //   45: invokestatic 85	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
      //   48: pop
      //   49: aload_0
      //   50: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   53: bipush 64
      //   55: invokestatic 142	com/testfairy/m/b:b	(Lcom/testfairy/m/b;I)Ljava/util/List;
      //   58: invokestatic 43	com/testfairy/m/b:a	(Ljava/util/List;)Lorg/json/JSONArray;
      //   61: astore_3
      //   62: new 45	org/json/JSONObject
      //   65: astore_1
      //   66: aload_1
      //   67: invokespecial 48	org/json/JSONObject:<init>	()V
      //   70: aload_1
      //   71: ldc 50
      //   73: aload_3
      //   74: invokevirtual 54	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
      //   77: pop
      //   78: aload_1
      //   79: invokevirtual 58	org/json/JSONObject:toString	()Ljava/lang/String;
      //   82: astore_3
      //   83: aload_0
      //   84: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   87: invokestatic 61	com/testfairy/m/b:a	(Lcom/testfairy/m/b;)Lcom/testfairy/i;
      //   90: ifnull +144 -> 234
      //   93: aload_0
      //   94: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   97: invokestatic 61	com/testfairy/m/b:a	(Lcom/testfairy/m/b;)Lcom/testfairy/i;
      //   100: invokevirtual 111	com/testfairy/i:a	()Ljava/lang/String;
      //   103: astore_1
      //   104: aload_0
      //   105: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   108: aload_1
      //   109: aload_3
      //   110: invokestatic 88	com/testfairy/m/b:a	(Lcom/testfairy/m/b;Ljava/lang/String;Ljava/lang/String;)V
      //   113: aload_2
      //   114: monitorexit
      //   115: aload_0
      //   116: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   119: invokestatic 105	com/testfairy/m/b:e	(Lcom/testfairy/m/b;)Lcom/testfairy/k/d;
      //   122: ifnull +180 -> 302
      //   125: aload_0
      //   126: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   129: invokestatic 133	com/testfairy/m/b:c	(Lcom/testfairy/m/b;)Ljava/lang/Object;
      //   132: astore_2
      //   133: aload_2
      //   134: monitorenter
      //   135: aload_0
      //   136: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   139: invokestatic 154	com/testfairy/m/b:d	(Lcom/testfairy/m/b;)Ljava/util/List;
      //   142: invokeinterface 40 1 0
      //   147: ifle +271 -> 418
      //   150: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   153: astore_1
      //   154: new 68	java/lang/StringBuilder
      //   157: astore_3
      //   158: aload_3
      //   159: invokespecial 69	java/lang/StringBuilder:<init>	()V
      //   162: aload_1
      //   163: aload_3
      //   164: ldc -98
      //   166: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   169: aload_0
      //   170: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   173: invokestatic 154	com/testfairy/m/b:d	(Lcom/testfairy/m/b;)Ljava/util/List;
      //   176: invokeinterface 40 1 0
      //   181: invokevirtual 161	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   184: ldc -93
      //   186: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   189: invokevirtual 79	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   192: invokestatic 165	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   195: pop
      //   196: aload_0
      //   197: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   200: invokestatic 154	com/testfairy/m/b:d	(Lcom/testfairy/m/b;)Ljava/util/List;
      //   203: iconst_0
      //   204: invokeinterface 169 2 0
      //   209: checkcast 171	java/lang/String
      //   212: astore_1
      //   213: aload_1
      //   214: ldc -83
      //   216: invokevirtual 177	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
      //   219: ifeq +41 -> 260
      //   222: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   225: ldc -77
      //   227: invokestatic 165	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   230: pop
      //   231: aload_2
      //   232: monitorexit
      //   233: return
      //   234: invokestatic 78	com/testfairy/m/b:f	()Ljava/lang/String;
      //   237: astore_1
      //   238: goto -134 -> 104
      //   241: astore_1
      //   242: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   245: ldc -75
      //   247: aload_1
      //   248: invokestatic 183	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   251: pop
      //   252: goto -139 -> 113
      //   255: astore_1
      //   256: aload_2
      //   257: monitorexit
      //   258: aload_1
      //   259: athrow
      //   260: aload_0
      //   261: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   264: invokestatic 154	com/testfairy/m/b:d	(Lcom/testfairy/m/b;)Ljava/util/List;
      //   267: iconst_0
      //   268: invokeinterface 186 2 0
      //   273: pop
      //   274: aload_2
      //   275: monitorexit
      //   276: aload_1
      //   277: ifnull +25 -> 302
      //   280: aload_0
      //   281: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   284: iconst_1
      //   285: invokestatic 188	com/testfairy/m/b:b	(Lcom/testfairy/m/b;Z)Z
      //   288: pop
      //   289: aload_0
      //   290: aload_1
      //   291: invokespecial 190	com/testfairy/m/b$a:b	(Ljava/lang/String;)V
      //   294: goto -61 -> 233
      //   297: astore_1
      //   298: aload_2
      //   299: monitorexit
      //   300: aload_1
      //   301: athrow
      //   302: aload_0
      //   303: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   306: invokestatic 133	com/testfairy/m/b:c	(Lcom/testfairy/m/b;)Ljava/lang/Object;
      //   309: astore_3
      //   310: aload_3
      //   311: monitorenter
      //   312: aload_0
      //   313: getfield 24	com/testfairy/m/b$a:c	Z
      //   316: ifne +18 -> 334
      //   319: aload_0
      //   320: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   323: invokestatic 136	com/testfairy/m/b:f	(Lcom/testfairy/m/b;)Ljava/util/Vector;
      //   326: invokevirtual 139	java/util/Vector:size	()I
      //   329: bipush 64
      //   331: if_icmplt +82 -> 413
      //   334: aload_0
      //   335: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   338: bipush 64
      //   340: invokestatic 142	com/testfairy/m/b:b	(Lcom/testfairy/m/b;I)Ljava/util/List;
      //   343: astore_2
      //   344: aload_2
      //   345: astore_1
      //   346: aload_0
      //   347: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   350: invokestatic 136	com/testfairy/m/b:f	(Lcom/testfairy/m/b;)Ljava/util/Vector;
      //   353: invokevirtual 146	java/util/Vector:isEmpty	()Z
      //   356: ifeq +10 -> 366
      //   359: aload_0
      //   360: iconst_0
      //   361: putfield 24	com/testfairy/m/b$a:c	Z
      //   364: aload_2
      //   365: astore_1
      //   366: aload_3
      //   367: monitorexit
      //   368: aload_1
      //   369: ifnull -136 -> 233
      //   372: aload_0
      //   373: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   376: invokestatic 194	com/testfairy/m/b:g	(Lcom/testfairy/m/b;)Z
      //   379: ifeq +21 -> 400
      //   382: aload_0
      //   383: getfield 15	com/testfairy/m/b$a:a	Lcom/testfairy/m/b;
      //   386: iconst_0
      //   387: invokestatic 188	com/testfairy/m/b:b	(Lcom/testfairy/m/b;Z)Z
      //   390: pop
      //   391: getstatic 66	com/testfairy/e:a	Ljava/lang/String;
      //   394: ldc -60
      //   396: invokestatic 165	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   399: pop
      //   400: aload_0
      //   401: aload_1
      //   402: invokespecial 198	com/testfairy/m/b$a:a	(Ljava/util/List;)V
      //   405: goto -172 -> 233
      //   408: astore_1
      //   409: aload_3
      //   410: monitorexit
      //   411: aload_1
      //   412: athrow
      //   413: aconst_null
      //   414: astore_1
      //   415: goto -49 -> 366
      //   418: aconst_null
      //   419: astore_1
      //   420: goto -146 -> 274
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	423	0	this	a
      //   65	173	1	localObject1	Object
      //   241	7	1	localJSONException	org.json.JSONException
      //   255	36	1	str	String
      //   297	4	1	localObject2	Object
      //   345	57	1	localObject3	Object
      //   408	4	1	localObject4	Object
      //   414	6	1	localObject5	Object
      // Exception table:
      //   from	to	target	type
      //   49	104	241	org/json/JSONException
      //   104	113	241	org/json/JSONException
      //   234	238	241	org/json/JSONException
      //   10	49	255	finally
      //   49	104	255	finally
      //   104	113	255	finally
      //   113	115	255	finally
      //   234	238	255	finally
      //   242	252	255	finally
      //   256	258	255	finally
      //   135	233	297	finally
      //   260	274	297	finally
      //   274	276	297	finally
      //   298	300	297	finally
      //   312	334	408	finally
      //   334	344	408	finally
      //   346	364	408	finally
      //   366	368	408	finally
      //   409	411	408	finally
    }
    
    public void a()
    {
      this.c = true;
      synchronized (b.c(b.this))
      {
        String str = e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.d(str, "Forcing a flush of all " + b.f(b.this).size() + " events that are in memory");
        return;
      }
    }
    
    public void a(String paramString)
    {
      Log.v(e.a, "Update saved file names with new token (" + paramString + ")");
      Object localObject1 = b.c(b.this);
      int i = 0;
      try
      {
        while (i < b.d(b.this).size())
        {
          Object localObject2 = (String)b.d(b.this).get(i);
          if (((String)localObject2).contains("anonymous"))
          {
            File localFile = new java/io/File;
            localFile.<init>((String)localObject2);
            Object localObject3 = ((String)localObject2).split("\\.");
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            localObject2 = localFile.getParent() + "/" + "testfairy-events" + "." + paramString + "." + localObject3[(localObject3.length - 1)];
            String str = e.a;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            Log.v(str, "Found matching event file, renaming to " + (String)localObject2);
            localObject3 = new java/io/File;
            ((File)localObject3).<init>((String)localObject2);
            localFile.renameTo((File)localObject3);
            b.d(b.this).remove(i);
            b.d(b.this).add(i, localObject2);
          }
          i++;
        }
        return;
      }
      finally {}
    }
    
    public void b()
    {
      this.b = true;
    }
    
    public void run()
    {
      int i = 0;
      Log.d(e.a, "Starting event uploader background thread");
      if (i == 0)
      {
        if (!b.h(b.this))
        {
          a locala = b.i(b.this);
          if ((!b.j(b.this)) && ((locala == null) || (!locala.m()) || (com.testfairy.k.b.a()))) {
            break label110;
          }
          c();
        }
        for (;;)
        {
          a(b.b(b.this));
          if ((!this.b) || (this.c) || (!b.f(b.this).isEmpty())) {
            break;
          }
          i = 1;
          break;
          label110:
          d();
        }
      }
    }
  }
  
  private class b
    extends c
  {
    private String b;
    
    public b(String paramString)
    {
      this.b = paramString;
    }
    
    public void a()
    {
      super.a();
      b.a(b.this, false);
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      b.a(b.this, 1000);
    }
    
    public void a(Throwable paramThrowable, String paramString)
    {
      super.a(paramThrowable, paramString);
      Log.v(e.a, "Failed to send some events over the wire, saving to disk");
      if (b.a(b.this) != null) {}
      for (paramThrowable = b.a(b.this).a();; paramThrowable = b.f())
      {
        b.a(b.this, paramThrowable, this.b);
        b.a(b.this, Math.min(b.b(b.this) * 2, 15000));
        return;
      }
    }
  }
  
  private class c
    extends c
  {
    private String b;
    
    private c(String paramString)
    {
      this.b = paramString;
    }
    
    public void a()
    {
      super.a();
      b.a(b.this, false);
    }
    
    public void a(String paramString)
    {
      super.a(paramString);
      Log.v(e.a, "Successfully sent " + this.b + " over the wire");
      new File(this.b).delete();
      b.a(b.this, 1000);
    }
    
    public void a(Throwable arg1, String paramString)
    {
      Log.v(e.a, "Failed to send " + this.b + " over the wire, adding back to queue");
      super.a(???, paramString);
      synchronized (b.c(b.this))
      {
        b.d(b.this).add(0, this.b);
        b.a(b.this, Math.min(b.b(b.this) * 2, 15000));
        return;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\m\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */