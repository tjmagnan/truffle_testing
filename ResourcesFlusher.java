package android.support.v7.app;

import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.LongSparseArray;
import java.lang.reflect.Field;
import java.util.Map;

class ResourcesFlusher
{
  private static final String TAG = "ResourcesFlusher";
  private static Field sDrawableCacheField;
  private static boolean sDrawableCacheFieldFetched;
  private static Field sResourcesImplField;
  private static boolean sResourcesImplFieldFetched;
  private static Class sThemedResourceCacheClazz;
  private static boolean sThemedResourceCacheClazzFetched;
  private static Field sThemedResourceCache_mUnthemedEntriesField;
  private static boolean sThemedResourceCache_mUnthemedEntriesFieldFetched;
  
  static boolean flush(@NonNull Resources paramResources)
  {
    int i = Build.VERSION.SDK_INT;
    boolean bool;
    if (i >= 24) {
      bool = flushNougats(paramResources);
    }
    for (;;)
    {
      return bool;
      if (i >= 23) {
        bool = flushMarshmallows(paramResources);
      } else if (i >= 21) {
        bool = flushLollipops(paramResources);
      } else {
        bool = false;
      }
    }
  }
  
  private static boolean flushLollipops(@NonNull Resources paramResources)
  {
    if (!sDrawableCacheFieldFetched) {}
    try
    {
      sDrawableCacheField = Resources.class.getDeclaredField("mDrawableCache");
      sDrawableCacheField.setAccessible(true);
      sDrawableCacheFieldFetched = true;
      if (sDrawableCacheField != null) {
        Object localObject = null;
      }
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      for (;;)
      {
        try
        {
          paramResources = (Map)sDrawableCacheField.get(paramResources);
          if (paramResources != null)
          {
            paramResources.clear();
            bool = true;
            return bool;
            localNoSuchFieldException = localNoSuchFieldException;
            Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", localNoSuchFieldException);
          }
        }
        catch (IllegalAccessException paramResources)
        {
          Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", paramResources);
          paramResources = localNoSuchFieldException;
          continue;
        }
        boolean bool = false;
      }
    }
  }
  
  private static boolean flushMarshmallows(@NonNull Resources paramResources)
  {
    boolean bool1 = false;
    boolean bool2 = true;
    if (!sDrawableCacheFieldFetched) {}
    Object localObject2;
    try
    {
      sDrawableCacheField = Resources.class.getDeclaredField("mDrawableCache");
      sDrawableCacheField.setAccessible(true);
      sDrawableCacheFieldFetched = true;
      localObject3 = null;
      localObject1 = localObject3;
      if (sDrawableCacheField == null) {}
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      try
      {
        Object localObject1 = sDrawableCacheField.get(paramResources);
        if (localObject1 == null)
        {
          return bool1;
          localNoSuchFieldException = localNoSuchFieldException;
          Log.e("ResourcesFlusher", "Could not retrieve Resources#mDrawableCache field", localNoSuchFieldException);
        }
      }
      catch (IllegalAccessException paramResources)
      {
        for (;;)
        {
          Object localObject3;
          Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mDrawableCache", paramResources);
          localObject2 = localObject3;
        }
        if (localObject2 == null) {
          break label102;
        }
      }
    }
    if (flushThemedResourcesCache(localObject2)) {}
    label102:
    for (bool1 = bool2;; bool1 = false) {
      break;
    }
  }
  
  private static boolean flushNougats(@NonNull Resources paramResources)
  {
    boolean bool1 = false;
    boolean bool2 = true;
    if (!sResourcesImplFieldFetched) {}
    for (;;)
    {
      Object localObject1;
      try
      {
        sResourcesImplField = Resources.class.getDeclaredField("mResourcesImpl");
        sResourcesImplField.setAccessible(true);
        sResourcesImplFieldFetched = true;
        if (sResourcesImplField == null) {
          return bool1;
        }
      }
      catch (NoSuchFieldException localNoSuchFieldException1)
      {
        Log.e("ResourcesFlusher", "Could not retrieve Resources#mResourcesImpl field", localNoSuchFieldException1);
        continue;
        localObject1 = null;
      }
      try
      {
        paramResources = sResourcesImplField.get(paramResources);
        if (paramResources == null) {
          continue;
        }
        if (sDrawableCacheFieldFetched) {}
      }
      catch (IllegalAccessException localNoSuchFieldException2)
      {
        try
        {
          sDrawableCacheField = paramResources.getClass().getDeclaredField("mDrawableCache");
          sDrawableCacheField.setAccessible(true);
          sDrawableCacheFieldFetched = true;
          localObject3 = null;
          localObject1 = localObject3;
          if (sDrawableCacheField == null) {}
        }
        catch (NoSuchFieldException localNoSuchFieldException2)
        {
          try
          {
            for (;;)
            {
              localObject1 = sDrawableCacheField.get(paramResources);
              if ((localObject1 == null) || (!flushThemedResourcesCache(localObject1))) {
                break label175;
              }
              bool1 = bool2;
              break;
              paramResources = paramResources;
              Log.e("ResourcesFlusher", "Could not retrieve value from Resources#mResourcesImpl", paramResources);
              paramResources = (Resources)localObject1;
            }
            localNoSuchFieldException2 = localNoSuchFieldException2;
            Log.e("ResourcesFlusher", "Could not retrieve ResourcesImpl#mDrawableCache field", localNoSuchFieldException2);
          }
          catch (IllegalAccessException paramResources)
          {
            for (;;)
            {
              Object localObject3;
              Log.e("ResourcesFlusher", "Could not retrieve value from ResourcesImpl#mDrawableCache", paramResources);
              Object localObject2 = localObject3;
              continue;
              label175:
              bool1 = false;
            }
          }
        }
      }
    }
  }
  
  private static boolean flushThemedResourcesCache(@NonNull Object paramObject)
  {
    if (!sThemedResourceCacheClazzFetched) {}
    try
    {
      sThemedResourceCacheClazz = Class.forName("android.content.res.ThemedResourceCache");
      sThemedResourceCacheClazzFetched = true;
      if (sThemedResourceCacheClazz == null)
      {
        bool = false;
        return bool;
      }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;)
      {
        boolean bool;
        Log.e("ResourcesFlusher", "Could not find ThemedResourceCache class", localClassNotFoundException);
        continue;
        if (!sThemedResourceCache_mUnthemedEntriesFieldFetched) {}
        try
        {
          sThemedResourceCache_mUnthemedEntriesField = sThemedResourceCacheClazz.getDeclaredField("mUnthemedEntries");
          sThemedResourceCache_mUnthemedEntriesField.setAccessible(true);
          sThemedResourceCache_mUnthemedEntriesFieldFetched = true;
          if (sThemedResourceCache_mUnthemedEntriesField == null) {
            bool = false;
          }
        }
        catch (NoSuchFieldException localNoSuchFieldException)
        {
          for (;;)
          {
            Log.e("ResourcesFlusher", "Could not retrieve ThemedResourceCache#mUnthemedEntries field", localNoSuchFieldException);
          }
          Object localObject = null;
          try
          {
            paramObject = (LongSparseArray)sThemedResourceCache_mUnthemedEntriesField.get(paramObject);
            if (paramObject != null)
            {
              ((LongSparseArray)paramObject).clear();
              bool = true;
            }
          }
          catch (IllegalAccessException paramObject)
          {
            for (;;)
            {
              Log.e("ResourcesFlusher", "Could not retrieve value from ThemedResourceCache#mUnthemedEntries", (Throwable)paramObject);
              paramObject = localObject;
            }
            bool = false;
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\app\ResourcesFlusher.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */