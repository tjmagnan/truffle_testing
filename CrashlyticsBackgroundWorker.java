package com.crashlytics.android.core;

import android.os.Looper;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

class CrashlyticsBackgroundWorker
{
  private final ExecutorService executorService;
  
  public CrashlyticsBackgroundWorker(ExecutorService paramExecutorService)
  {
    this.executorService = paramExecutorService;
  }
  
  Future<?> submit(Runnable paramRunnable)
  {
    try
    {
      ExecutorService localExecutorService = this.executorService;
      Runnable local1 = new com/crashlytics/android/core/CrashlyticsBackgroundWorker$1;
      local1.<init>(this, paramRunnable);
      paramRunnable = localExecutorService.submit(local1);
      return paramRunnable;
    }
    catch (RejectedExecutionException paramRunnable)
    {
      for (;;)
      {
        Fabric.getLogger().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
        paramRunnable = null;
      }
    }
  }
  
  <T> Future<T> submit(Callable<T> paramCallable)
  {
    try
    {
      ExecutorService localExecutorService = this.executorService;
      Callable local2 = new com/crashlytics/android/core/CrashlyticsBackgroundWorker$2;
      local2.<init>(this, paramCallable);
      paramCallable = localExecutorService.submit(local2);
      return paramCallable;
    }
    catch (RejectedExecutionException paramCallable)
    {
      for (;;)
      {
        Fabric.getLogger().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
        paramCallable = null;
      }
    }
  }
  
  <T> T submitAndWait(Callable<T> paramCallable)
  {
    Object localObject = null;
    try
    {
      if (Looper.getMainLooper() == Looper.myLooper()) {}
      for (paramCallable = this.executorService.submit(paramCallable).get(4L, TimeUnit.SECONDS);; paramCallable = this.executorService.submit(paramCallable).get()) {
        return paramCallable;
      }
    }
    catch (RejectedExecutionException paramCallable)
    {
      for (;;)
      {
        Fabric.getLogger().d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
        paramCallable = (Callable<T>)localObject;
      }
    }
    catch (Exception paramCallable)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Failed to execute task.", paramCallable);
        paramCallable = (Callable<T>)localObject;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\CrashlyticsBackgroundWorker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */