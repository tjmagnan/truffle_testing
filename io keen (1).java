package io.keen;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "io.keen";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 15;
  public static final String VERSION_NAME = "5.2.0";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\keen\BuildConfig.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */