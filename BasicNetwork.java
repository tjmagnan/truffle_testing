package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.Cache.Entry;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork
  implements Network
{
  protected static final boolean DEBUG = VolleyLog.DEBUG;
  private static int DEFAULT_POOL_SIZE = 4096;
  private static int SLOW_REQUEST_THRESHOLD_MS = 3000;
  protected final HttpStack mHttpStack;
  protected final ByteArrayPool mPool;
  
  public BasicNetwork(HttpStack paramHttpStack)
  {
    this(paramHttpStack, new ByteArrayPool(DEFAULT_POOL_SIZE));
  }
  
  public BasicNetwork(HttpStack paramHttpStack, ByteArrayPool paramByteArrayPool)
  {
    this.mHttpStack = paramHttpStack;
    this.mPool = paramByteArrayPool;
  }
  
  private void addCacheHeaders(Map<String, String> paramMap, Cache.Entry paramEntry)
  {
    if (paramEntry == null) {}
    for (;;)
    {
      return;
      if (paramEntry.etag != null) {
        paramMap.put("If-None-Match", paramEntry.etag);
      }
      if (paramEntry.lastModified > 0L) {
        paramMap.put("If-Modified-Since", DateUtils.formatDate(new Date(paramEntry.lastModified)));
      }
    }
  }
  
  private static void attemptRetryOnException(String paramString, Request<?> paramRequest, VolleyError paramVolleyError)
    throws VolleyError
  {
    RetryPolicy localRetryPolicy = paramRequest.getRetryPolicy();
    int i = paramRequest.getTimeoutMs();
    try
    {
      localRetryPolicy.retry(paramVolleyError);
      paramRequest.addMarker(String.format("%s-retry [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      return;
    }
    catch (VolleyError paramVolleyError)
    {
      paramRequest.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[] { paramString, Integer.valueOf(i) }));
      throw paramVolleyError;
    }
  }
  
  protected static Map<String, String> convertHeaders(Header[] paramArrayOfHeader)
  {
    TreeMap localTreeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
    for (int i = 0; i < paramArrayOfHeader.length; i++) {
      localTreeMap.put(paramArrayOfHeader[i].getName(), paramArrayOfHeader[i].getValue());
    }
    return localTreeMap;
  }
  
  private byte[] entityToBytes(HttpEntity paramHttpEntity)
    throws IOException, ServerError
  {
    PoolingByteArrayOutputStream localPoolingByteArrayOutputStream = new PoolingByteArrayOutputStream(this.mPool, (int)paramHttpEntity.getContentLength());
    Object localObject2 = null;
    Object localObject1 = localObject2;
    Object localObject4;
    try
    {
      localObject4 = paramHttpEntity.getContent();
      if (localObject4 == null)
      {
        localObject1 = localObject2;
        localObject4 = new com/android/volley/ServerError;
        localObject1 = localObject2;
        ((ServerError)localObject4).<init>();
        localObject1 = localObject2;
        throw ((Throwable)localObject4);
      }
    }
    finally {}
    try
    {
      paramHttpEntity.consumeContent();
      this.mPool.returnBuf((byte[])localObject1);
      localPoolingByteArrayOutputStream.close();
      throw ((Throwable)localObject3);
      localObject1 = localObject3;
      byte[] arrayOfByte = this.mPool.getBuf(1024);
      for (;;)
      {
        localObject1 = arrayOfByte;
        int i = ((InputStream)localObject4).read(arrayOfByte);
        if (i == -1) {
          break;
        }
        localObject1 = arrayOfByte;
        localPoolingByteArrayOutputStream.write(arrayOfByte, 0, i);
      }
      localObject1 = arrayOfByte;
      localObject4 = localPoolingByteArrayOutputStream.toByteArray();
      try
      {
        paramHttpEntity.consumeContent();
        this.mPool.returnBuf(arrayOfByte);
        localPoolingByteArrayOutputStream.close();
        return (byte[])localObject4;
      }
      catch (IOException paramHttpEntity)
      {
        for (;;)
        {
          VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
        }
      }
    }
    catch (IOException paramHttpEntity)
    {
      for (;;)
      {
        VolleyLog.v("Error occured when calling consumingContent", new Object[0]);
      }
    }
  }
  
  private void logSlowRequests(long paramLong, Request<?> paramRequest, byte[] paramArrayOfByte, StatusLine paramStatusLine)
  {
    if ((DEBUG) || (paramLong > SLOW_REQUEST_THRESHOLD_MS)) {
      if (paramArrayOfByte == null) {
        break label82;
      }
    }
    label82:
    for (paramArrayOfByte = Integer.valueOf(paramArrayOfByte.length);; paramArrayOfByte = "null")
    {
      VolleyLog.d("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", new Object[] { paramRequest, Long.valueOf(paramLong), paramArrayOfByte, Integer.valueOf(paramStatusLine.getStatusCode()), Integer.valueOf(paramRequest.getRetryPolicy().getCurrentRetryCount()) });
      return;
    }
  }
  
  protected void logError(String paramString1, String paramString2, long paramLong)
  {
    VolleyLog.v("HTTP ERROR(%s) %d ms to fetch %s", new Object[] { paramString1, Long.valueOf(SystemClock.elapsedRealtime() - paramLong), paramString2 });
  }
  
  /* Error */
  public com.android.volley.NetworkResponse performRequest(Request<?> paramRequest)
    throws VolleyError
  {
    // Byte code:
    //   0: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   3: lstore_3
    //   4: aconst_null
    //   5: astore 7
    //   7: invokestatic 243	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   10: astore 8
    //   12: aload 8
    //   14: astore 5
    //   16: aload 7
    //   18: astore 6
    //   20: new 245	java/util/HashMap
    //   23: astore 9
    //   25: aload 8
    //   27: astore 5
    //   29: aload 7
    //   31: astore 6
    //   33: aload 9
    //   35: invokespecial 246	java/util/HashMap:<init>	()V
    //   38: aload 8
    //   40: astore 5
    //   42: aload 7
    //   44: astore 6
    //   46: aload_0
    //   47: aload 9
    //   49: aload_1
    //   50: invokevirtual 250	com/android/volley/Request:getCacheEntry	()Lcom/android/volley/Cache$Entry;
    //   53: invokespecial 252	com/android/volley/toolbox/BasicNetwork:addCacheHeaders	(Ljava/util/Map;Lcom/android/volley/Cache$Entry;)V
    //   56: aload 8
    //   58: astore 5
    //   60: aload 7
    //   62: astore 6
    //   64: aload_0
    //   65: getfield 41	com/android/volley/toolbox/BasicNetwork:mHttpStack	Lcom/android/volley/toolbox/HttpStack;
    //   68: aload_1
    //   69: aload 9
    //   71: invokeinterface 257 3 0
    //   76: astore 7
    //   78: aload 8
    //   80: astore 5
    //   82: aload 7
    //   84: astore 6
    //   86: aload 7
    //   88: invokeinterface 263 1 0
    //   93: astore 10
    //   95: aload 8
    //   97: astore 5
    //   99: aload 7
    //   101: astore 6
    //   103: aload 10
    //   105: invokeinterface 211 1 0
    //   110: istore_2
    //   111: aload 8
    //   113: astore 5
    //   115: aload 7
    //   117: astore 6
    //   119: aload 7
    //   121: invokeinterface 267 1 0
    //   126: invokestatic 269	com/android/volley/toolbox/BasicNetwork:convertHeaders	([Lorg/apache/http/Header;)Ljava/util/Map;
    //   129: astore 8
    //   131: iload_2
    //   132: sipush 304
    //   135: if_icmpne +118 -> 253
    //   138: aload 8
    //   140: astore 5
    //   142: aload 7
    //   144: astore 6
    //   146: aload_1
    //   147: invokevirtual 250	com/android/volley/Request:getCacheEntry	()Lcom/android/volley/Cache$Entry;
    //   150: astore 9
    //   152: aload 9
    //   154: ifnonnull +37 -> 191
    //   157: aload 8
    //   159: astore 5
    //   161: aload 7
    //   163: astore 6
    //   165: new 271	com/android/volley/NetworkResponse
    //   168: dup
    //   169: sipush 304
    //   172: aconst_null
    //   173: aload 8
    //   175: iconst_1
    //   176: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   179: lload_3
    //   180: lsub
    //   181: invokespecial 274	com/android/volley/NetworkResponse:<init>	(I[BLjava/util/Map;ZJ)V
    //   184: astore 7
    //   186: aload 7
    //   188: astore_1
    //   189: aload_1
    //   190: areturn
    //   191: aload 8
    //   193: astore 5
    //   195: aload 7
    //   197: astore 6
    //   199: aload 9
    //   201: getfield 278	com/android/volley/Cache$Entry:responseHeaders	Ljava/util/Map;
    //   204: aload 8
    //   206: invokeinterface 282 2 0
    //   211: aload 8
    //   213: astore 5
    //   215: aload 7
    //   217: astore 6
    //   219: new 271	com/android/volley/NetworkResponse
    //   222: dup
    //   223: sipush 304
    //   226: aload 9
    //   228: getfield 286	com/android/volley/Cache$Entry:data	[B
    //   231: aload 9
    //   233: getfield 278	com/android/volley/Cache$Entry:responseHeaders	Ljava/util/Map;
    //   236: iconst_1
    //   237: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   240: lload_3
    //   241: lsub
    //   242: invokespecial 274	com/android/volley/NetworkResponse:<init>	(I[BLjava/util/Map;ZJ)V
    //   245: astore 7
    //   247: aload 7
    //   249: astore_1
    //   250: goto -61 -> 189
    //   253: aload 8
    //   255: astore 5
    //   257: aload 7
    //   259: astore 6
    //   261: aload 7
    //   263: invokeinterface 290 1 0
    //   268: ifnull +88 -> 356
    //   271: aload 8
    //   273: astore 5
    //   275: aload 7
    //   277: astore 6
    //   279: aload_0
    //   280: aload 7
    //   282: invokeinterface 290 1 0
    //   287: invokespecial 292	com/android/volley/toolbox/BasicNetwork:entityToBytes	(Lorg/apache/http/HttpEntity;)[B
    //   290: astore 9
    //   292: aload 9
    //   294: astore 5
    //   296: aload_0
    //   297: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   300: lload_3
    //   301: lsub
    //   302: aload_1
    //   303: aload 5
    //   305: aload 10
    //   307: invokespecial 294	com/android/volley/toolbox/BasicNetwork:logSlowRequests	(JLcom/android/volley/Request;[BLorg/apache/http/StatusLine;)V
    //   310: iload_2
    //   311: sipush 200
    //   314: if_icmplt +10 -> 324
    //   317: iload_2
    //   318: sipush 299
    //   321: if_icmple +55 -> 376
    //   324: new 145	java/io/IOException
    //   327: astore 6
    //   329: aload 6
    //   331: invokespecial 295	java/io/IOException:<init>	()V
    //   334: aload 6
    //   336: athrow
    //   337: astore 5
    //   339: ldc_w 297
    //   342: aload_1
    //   343: new 299	com/android/volley/TimeoutError
    //   346: dup
    //   347: invokespecial 300	com/android/volley/TimeoutError:<init>	()V
    //   350: invokestatic 302	com/android/volley/toolbox/BasicNetwork:attemptRetryOnException	(Ljava/lang/String;Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    //   353: goto -349 -> 4
    //   356: aload 8
    //   358: astore 5
    //   360: aload 7
    //   362: astore 6
    //   364: iconst_0
    //   365: newarray <illegal type>
    //   367: astore 9
    //   369: aload 9
    //   371: astore 5
    //   373: goto -77 -> 296
    //   376: new 271	com/android/volley/NetworkResponse
    //   379: dup
    //   380: iload_2
    //   381: aload 5
    //   383: aload 8
    //   385: iconst_0
    //   386: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   389: lload_3
    //   390: lsub
    //   391: invokespecial 274	com/android/volley/NetworkResponse:<init>	(I[BLjava/util/Map;ZJ)V
    //   394: astore 6
    //   396: aload 6
    //   398: astore_1
    //   399: goto -210 -> 189
    //   402: astore 5
    //   404: ldc_w 304
    //   407: aload_1
    //   408: new 299	com/android/volley/TimeoutError
    //   411: dup
    //   412: invokespecial 300	com/android/volley/TimeoutError:<init>	()V
    //   415: invokestatic 302	com/android/volley/toolbox/BasicNetwork:attemptRetryOnException	(Ljava/lang/String;Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    //   418: goto -414 -> 4
    //   421: astore 5
    //   423: aload_1
    //   424: invokevirtual 307	com/android/volley/Request:getUrl	()Ljava/lang/String;
    //   427: invokestatic 310	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   430: astore_1
    //   431: aload_1
    //   432: invokevirtual 313	java/lang/String:length	()I
    //   435: ifeq +22 -> 457
    //   438: ldc_w 315
    //   441: aload_1
    //   442: invokevirtual 319	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   445: astore_1
    //   446: new 321	java/lang/RuntimeException
    //   449: dup
    //   450: aload_1
    //   451: aload 5
    //   453: invokespecial 324	java/lang/RuntimeException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   456: athrow
    //   457: new 108	java/lang/String
    //   460: dup
    //   461: ldc_w 315
    //   464: invokespecial 326	java/lang/String:<init>	(Ljava/lang/String;)V
    //   467: astore_1
    //   468: goto -22 -> 446
    //   471: astore 7
    //   473: aconst_null
    //   474: astore 10
    //   476: aload 6
    //   478: astore 8
    //   480: aload 5
    //   482: astore 9
    //   484: aload 7
    //   486: astore 6
    //   488: aload 8
    //   490: ifnull +98 -> 588
    //   493: aload 8
    //   495: invokeinterface 263 1 0
    //   500: invokeinterface 211 1 0
    //   505: istore_2
    //   506: ldc_w 328
    //   509: iconst_2
    //   510: anewarray 4	java/lang/Object
    //   513: dup
    //   514: iconst_0
    //   515: iload_2
    //   516: invokestatic 106	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   519: aastore
    //   520: dup
    //   521: iconst_1
    //   522: aload_1
    //   523: invokevirtual 307	com/android/volley/Request:getUrl	()Ljava/lang/String;
    //   526: aastore
    //   527: invokestatic 331	com/android/volley/VolleyLog:e	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   530: aload 10
    //   532: ifnull +76 -> 608
    //   535: new 271	com/android/volley/NetworkResponse
    //   538: dup
    //   539: iload_2
    //   540: aload 10
    //   542: aload 9
    //   544: iconst_0
    //   545: invokestatic 229	android/os/SystemClock:elapsedRealtime	()J
    //   548: lload_3
    //   549: lsub
    //   550: invokespecial 274	com/android/volley/NetworkResponse:<init>	(I[BLjava/util/Map;ZJ)V
    //   553: astore 5
    //   555: iload_2
    //   556: sipush 401
    //   559: if_icmpeq +10 -> 569
    //   562: iload_2
    //   563: sipush 403
    //   566: if_icmpne +32 -> 598
    //   569: ldc_w 333
    //   572: aload_1
    //   573: new 335	com/android/volley/AuthFailureError
    //   576: dup
    //   577: aload 5
    //   579: invokespecial 338	com/android/volley/AuthFailureError:<init>	(Lcom/android/volley/NetworkResponse;)V
    //   582: invokestatic 302	com/android/volley/toolbox/BasicNetwork:attemptRetryOnException	(Ljava/lang/String;Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    //   585: goto -581 -> 4
    //   588: new 340	com/android/volley/NoConnectionError
    //   591: dup
    //   592: aload 6
    //   594: invokespecial 343	com/android/volley/NoConnectionError:<init>	(Ljava/lang/Throwable;)V
    //   597: athrow
    //   598: new 147	com/android/volley/ServerError
    //   601: dup
    //   602: aload 5
    //   604: invokespecial 344	com/android/volley/ServerError:<init>	(Lcom/android/volley/NetworkResponse;)V
    //   607: athrow
    //   608: new 346	com/android/volley/NetworkError
    //   611: dup
    //   612: aconst_null
    //   613: invokespecial 347	com/android/volley/NetworkError:<init>	(Lcom/android/volley/NetworkResponse;)V
    //   616: athrow
    //   617: astore 6
    //   619: aload 8
    //   621: astore 9
    //   623: aload 5
    //   625: astore 10
    //   627: aload 7
    //   629: astore 8
    //   631: goto -143 -> 488
    //   634: astore 5
    //   636: goto -213 -> 423
    //   639: astore 5
    //   641: goto -237 -> 404
    //   644: astore 5
    //   646: goto -307 -> 339
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	649	0	this	BasicNetwork
    //   0	649	1	paramRequest	Request<?>
    //   110	457	2	i	int
    //   3	546	3	l	long
    //   14	290	5	localObject1	Object
    //   337	1	5	localSocketTimeoutException1	java.net.SocketTimeoutException
    //   358	24	5	localObject2	Object
    //   402	1	5	localConnectTimeoutException1	org.apache.http.conn.ConnectTimeoutException
    //   421	60	5	localMalformedURLException1	java.net.MalformedURLException
    //   553	71	5	localNetworkResponse	com.android.volley.NetworkResponse
    //   634	1	5	localMalformedURLException2	java.net.MalformedURLException
    //   639	1	5	localConnectTimeoutException2	org.apache.http.conn.ConnectTimeoutException
    //   644	1	5	localSocketTimeoutException2	java.net.SocketTimeoutException
    //   18	575	6	localObject3	Object
    //   617	1	6	localIOException1	IOException
    //   5	356	7	localObject4	Object
    //   471	157	7	localIOException2	IOException
    //   10	620	8	localObject5	Object
    //   23	599	9	localObject6	Object
    //   93	533	10	localObject7	Object
    // Exception table:
    //   from	to	target	type
    //   296	310	337	java/net/SocketTimeoutException
    //   324	337	337	java/net/SocketTimeoutException
    //   376	396	337	java/net/SocketTimeoutException
    //   20	25	402	org/apache/http/conn/ConnectTimeoutException
    //   33	38	402	org/apache/http/conn/ConnectTimeoutException
    //   46	56	402	org/apache/http/conn/ConnectTimeoutException
    //   64	78	402	org/apache/http/conn/ConnectTimeoutException
    //   86	95	402	org/apache/http/conn/ConnectTimeoutException
    //   103	111	402	org/apache/http/conn/ConnectTimeoutException
    //   119	131	402	org/apache/http/conn/ConnectTimeoutException
    //   146	152	402	org/apache/http/conn/ConnectTimeoutException
    //   165	186	402	org/apache/http/conn/ConnectTimeoutException
    //   199	211	402	org/apache/http/conn/ConnectTimeoutException
    //   219	247	402	org/apache/http/conn/ConnectTimeoutException
    //   261	271	402	org/apache/http/conn/ConnectTimeoutException
    //   279	292	402	org/apache/http/conn/ConnectTimeoutException
    //   364	369	402	org/apache/http/conn/ConnectTimeoutException
    //   20	25	421	java/net/MalformedURLException
    //   33	38	421	java/net/MalformedURLException
    //   46	56	421	java/net/MalformedURLException
    //   64	78	421	java/net/MalformedURLException
    //   86	95	421	java/net/MalformedURLException
    //   103	111	421	java/net/MalformedURLException
    //   119	131	421	java/net/MalformedURLException
    //   146	152	421	java/net/MalformedURLException
    //   165	186	421	java/net/MalformedURLException
    //   199	211	421	java/net/MalformedURLException
    //   219	247	421	java/net/MalformedURLException
    //   261	271	421	java/net/MalformedURLException
    //   279	292	421	java/net/MalformedURLException
    //   364	369	421	java/net/MalformedURLException
    //   20	25	471	java/io/IOException
    //   33	38	471	java/io/IOException
    //   46	56	471	java/io/IOException
    //   64	78	471	java/io/IOException
    //   86	95	471	java/io/IOException
    //   103	111	471	java/io/IOException
    //   119	131	471	java/io/IOException
    //   146	152	471	java/io/IOException
    //   165	186	471	java/io/IOException
    //   199	211	471	java/io/IOException
    //   219	247	471	java/io/IOException
    //   261	271	471	java/io/IOException
    //   279	292	471	java/io/IOException
    //   364	369	471	java/io/IOException
    //   296	310	617	java/io/IOException
    //   324	337	617	java/io/IOException
    //   376	396	617	java/io/IOException
    //   296	310	634	java/net/MalformedURLException
    //   324	337	634	java/net/MalformedURLException
    //   376	396	634	java/net/MalformedURLException
    //   296	310	639	org/apache/http/conn/ConnectTimeoutException
    //   324	337	639	org/apache/http/conn/ConnectTimeoutException
    //   376	396	639	org/apache/http/conn/ConnectTimeoutException
    //   20	25	644	java/net/SocketTimeoutException
    //   33	38	644	java/net/SocketTimeoutException
    //   46	56	644	java/net/SocketTimeoutException
    //   64	78	644	java/net/SocketTimeoutException
    //   86	95	644	java/net/SocketTimeoutException
    //   103	111	644	java/net/SocketTimeoutException
    //   119	131	644	java/net/SocketTimeoutException
    //   146	152	644	java/net/SocketTimeoutException
    //   165	186	644	java/net/SocketTimeoutException
    //   199	211	644	java/net/SocketTimeoutException
    //   219	247	644	java/net/SocketTimeoutException
    //   261	271	644	java/net/SocketTimeoutException
    //   279	292	644	java/net/SocketTimeoutException
    //   364	369	644	java/net/SocketTimeoutException
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\toolbox\BasicNetwork.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */