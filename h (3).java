package com.testfairy.h;

import android.os.Message;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

public class d
  extends c
{
  private static String[] a = { "image/jpeg", "image/png" };
  
  public d() {}
  
  public d(String[] paramArrayOfString)
  {
    this();
    a = paramArrayOfString;
  }
  
  protected void a(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default: 
      super.a(paramMessage);
    }
    for (;;)
    {
      return;
      c((byte[])paramMessage.obj);
      continue;
      paramMessage = (Object[])paramMessage.obj;
      b((Throwable)paramMessage[0], (byte[])paramMessage[1]);
    }
  }
  
  protected void a(Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    b(a(1, new Object[] { paramThrowable, paramArrayOfByte }));
  }
  
  void a(HttpResponse paramHttpResponse)
  {
    Object localObject1 = null;
    int j = 0;
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    Object localObject2 = paramHttpResponse.getHeaders("Content-Type");
    if (localObject2.length != 1) {
      a(new HttpResponseException(localStatusLine.getStatusCode(), "None, or more than one, Content-Type Header found!"), null);
    }
    for (;;)
    {
      return;
      Object localObject3 = localObject2[0];
      localObject2 = a;
      int k = localObject2.length;
      for (int i = 0; i < k; i++) {
        if (localObject2[i].equals(((Header)localObject3).getValue())) {
          j = 1;
        }
      }
      if (j != 0) {
        break;
      }
      a(new HttpResponseException(localStatusLine.getStatusCode(), "Content-Type not allowed!"), null);
    }
    for (;;)
    {
      try
      {
        localObject2 = paramHttpResponse.getEntity();
        if (localObject2 == null) {
          break label223;
        }
        paramHttpResponse = new org/apache/http/entity/BufferedHttpEntity;
        paramHttpResponse.<init>((HttpEntity)localObject2);
        paramHttpResponse = EntityUtils.toByteArray(paramHttpResponse);
      }
      catch (IOException paramHttpResponse)
      {
        a(paramHttpResponse, (byte[])null);
        paramHttpResponse = (HttpResponse)localObject1;
        continue;
        b(paramHttpResponse);
      }
      if (localStatusLine.getStatusCode() >= 300)
      {
        a(new HttpResponseException(localStatusLine.getStatusCode(), localStatusLine.getReasonPhrase()), paramHttpResponse);
        break;
      }
      break;
      label223:
      paramHttpResponse = null;
    }
  }
  
  public void a(byte[] paramArrayOfByte) {}
  
  protected void b(Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    a(paramThrowable, new String(paramArrayOfByte));
  }
  
  protected void b(byte[] paramArrayOfByte)
  {
    b(a(0, paramArrayOfByte));
  }
  
  protected void c(byte[] paramArrayOfByte)
  {
    a(paramArrayOfByte);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */