package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.TintableBackgroundView;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.attr;
import android.support.v7.appcompat.R.layout;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.menu.ShowableListMenu;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

public class AppCompatSpinner
  extends Spinner
  implements TintableBackgroundView
{
  private static final int[] ATTRS_ANDROID_SPINNERMODE = { 16843505 };
  private static final int MAX_ITEMS_MEASURED = 15;
  private static final int MODE_DIALOG = 0;
  private static final int MODE_DROPDOWN = 1;
  private static final int MODE_THEME = -1;
  private static final String TAG = "AppCompatSpinner";
  private AppCompatBackgroundHelper mBackgroundTintHelper;
  int mDropDownWidth;
  private ForwardingListener mForwardingListener;
  DropdownPopup mPopup;
  private Context mPopupContext;
  private boolean mPopupSet;
  private SpinnerAdapter mTempAdapter;
  final Rect mTempRect = new Rect();
  
  public AppCompatSpinner(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public AppCompatSpinner(Context paramContext, int paramInt)
  {
    this(paramContext, null, R.attr.spinnerStyle, paramInt);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, R.attr.spinnerStyle);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    this(paramContext, paramAttributeSet, paramInt, -1);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    this(paramContext, paramAttributeSet, paramInt1, paramInt2, null);
  }
  
  public AppCompatSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2, final Resources.Theme paramTheme)
  {
    super(paramContext, paramAttributeSet, paramInt1);
    TintTypedArray localTintTypedArray = TintTypedArray.obtainStyledAttributes(paramContext, paramAttributeSet, R.styleable.Spinner, paramInt1, 0);
    this.mBackgroundTintHelper = new AppCompatBackgroundHelper(this);
    int i;
    Object localObject;
    if (paramTheme != null)
    {
      this.mPopupContext = new ContextThemeWrapper(paramContext, paramTheme);
      if (this.mPopupContext != null)
      {
        i = paramInt2;
        if (paramInt2 == -1)
        {
          if (Build.VERSION.SDK_INT < 11) {
            break label452;
          }
          localObject = null;
          paramTheme = null;
        }
      }
    }
    for (;;)
    {
      try
      {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, ATTRS_ANDROID_SPINNERMODE, paramInt1, 0);
        int j = paramInt2;
        paramTheme = localTypedArray;
        localObject = localTypedArray;
        if (localTypedArray.hasValue(0))
        {
          paramTheme = localTypedArray;
          localObject = localTypedArray;
          j = localTypedArray.getInt(0, 0);
        }
        i = j;
        if (localTypedArray != null)
        {
          localTypedArray.recycle();
          i = j;
        }
      }
      catch (Exception localException)
      {
        localObject = paramTheme;
        Log.i("AppCompatSpinner", "Could not read android:spinnerMode", localException);
        i = paramInt2;
        if (paramTheme == null) {
          continue;
        }
        paramTheme.recycle();
        i = paramInt2;
        continue;
      }
      finally
      {
        if (localObject == null) {
          continue;
        }
        ((TypedArray)localObject).recycle();
      }
      if (i == 1)
      {
        paramTheme = new DropdownPopup(this.mPopupContext, paramAttributeSet, paramInt1);
        localObject = TintTypedArray.obtainStyledAttributes(this.mPopupContext, paramAttributeSet, R.styleable.Spinner, paramInt1, 0);
        this.mDropDownWidth = ((TintTypedArray)localObject).getLayoutDimension(R.styleable.Spinner_android_dropDownWidth, -2);
        paramTheme.setBackgroundDrawable(((TintTypedArray)localObject).getDrawable(R.styleable.Spinner_android_popupBackground));
        paramTheme.setPromptText(localTintTypedArray.getString(R.styleable.Spinner_android_prompt));
        ((TintTypedArray)localObject).recycle();
        this.mPopup = paramTheme;
        this.mForwardingListener = new ForwardingListener(this)
        {
          public ShowableListMenu getPopup()
          {
            return paramTheme;
          }
          
          public boolean onForwardingStarted()
          {
            if (!AppCompatSpinner.this.mPopup.isShowing()) {
              AppCompatSpinner.this.mPopup.show();
            }
            return true;
          }
        };
      }
      paramTheme = localTintTypedArray.getTextArray(R.styleable.Spinner_android_entries);
      if (paramTheme != null)
      {
        paramContext = new ArrayAdapter(paramContext, 17367048, paramTheme);
        paramContext.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        setAdapter(paramContext);
      }
      localTintTypedArray.recycle();
      this.mPopupSet = true;
      if (this.mTempAdapter != null)
      {
        setAdapter(this.mTempAdapter);
        this.mTempAdapter = null;
      }
      this.mBackgroundTintHelper.loadFromAttributes(paramAttributeSet, paramInt1);
      return;
      i = localTintTypedArray.getResourceId(R.styleable.Spinner_popupTheme, 0);
      if (i != 0)
      {
        this.mPopupContext = new ContextThemeWrapper(paramContext, i);
        break;
      }
      if (Build.VERSION.SDK_INT < 23)
      {
        paramTheme = paramContext;
        this.mPopupContext = paramTheme;
        break;
      }
      paramTheme = null;
      continue;
      label452:
      i = 1;
    }
  }
  
  int compatMeasureContentWidth(SpinnerAdapter paramSpinnerAdapter, Drawable paramDrawable)
  {
    int j;
    if (paramSpinnerAdapter == null) {
      j = 0;
    }
    for (;;)
    {
      return j;
      int i = 0;
      View localView = null;
      int k = 0;
      int i1 = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
      int i3 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
      j = Math.max(0, getSelectedItemPosition());
      int i2 = Math.min(paramSpinnerAdapter.getCount(), j + 15);
      int m = Math.max(0, j - (15 - (i2 - j)));
      while (m < i2)
      {
        int n = paramSpinnerAdapter.getItemViewType(m);
        j = k;
        if (n != k)
        {
          j = n;
          localView = null;
        }
        localView = paramSpinnerAdapter.getView(m, localView, this);
        if (localView.getLayoutParams() == null) {
          localView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        }
        localView.measure(i1, i3);
        i = Math.max(i, localView.getMeasuredWidth());
        m++;
        k = j;
      }
      j = i;
      if (paramDrawable != null)
      {
        paramDrawable.getPadding(this.mTempRect);
        j = i + (this.mTempRect.left + this.mTempRect.right);
      }
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    if (this.mBackgroundTintHelper != null) {
      this.mBackgroundTintHelper.applySupportBackgroundTint();
    }
  }
  
  public int getDropDownHorizontalOffset()
  {
    int i;
    if (this.mPopup != null) {
      i = this.mPopup.getHorizontalOffset();
    }
    for (;;)
    {
      return i;
      if (Build.VERSION.SDK_INT >= 16) {
        i = super.getDropDownHorizontalOffset();
      } else {
        i = 0;
      }
    }
  }
  
  public int getDropDownVerticalOffset()
  {
    int i;
    if (this.mPopup != null) {
      i = this.mPopup.getVerticalOffset();
    }
    for (;;)
    {
      return i;
      if (Build.VERSION.SDK_INT >= 16) {
        i = super.getDropDownVerticalOffset();
      } else {
        i = 0;
      }
    }
  }
  
  public int getDropDownWidth()
  {
    int i;
    if (this.mPopup != null) {
      i = this.mDropDownWidth;
    }
    for (;;)
    {
      return i;
      if (Build.VERSION.SDK_INT >= 16) {
        i = super.getDropDownWidth();
      } else {
        i = 0;
      }
    }
  }
  
  public Drawable getPopupBackground()
  {
    Drawable localDrawable;
    if (this.mPopup != null) {
      localDrawable = this.mPopup.getBackground();
    }
    for (;;)
    {
      return localDrawable;
      if (Build.VERSION.SDK_INT >= 16) {
        localDrawable = super.getPopupBackground();
      } else {
        localDrawable = null;
      }
    }
  }
  
  public Context getPopupContext()
  {
    Context localContext;
    if (this.mPopup != null) {
      localContext = this.mPopupContext;
    }
    for (;;)
    {
      return localContext;
      if (Build.VERSION.SDK_INT >= 23) {
        localContext = super.getPopupContext();
      } else {
        localContext = null;
      }
    }
  }
  
  public CharSequence getPrompt()
  {
    if (this.mPopup != null) {}
    for (CharSequence localCharSequence = this.mPopup.getHintText();; localCharSequence = super.getPrompt()) {
      return localCharSequence;
    }
  }
  
  @Nullable
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.mBackgroundTintHelper != null) {}
    for (ColorStateList localColorStateList = this.mBackgroundTintHelper.getSupportBackgroundTintList();; localColorStateList = null) {
      return localColorStateList;
    }
  }
  
  @Nullable
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.mBackgroundTintHelper != null) {}
    for (PorterDuff.Mode localMode = this.mBackgroundTintHelper.getSupportBackgroundTintMode();; localMode = null) {
      return localMode;
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if ((this.mPopup != null) && (this.mPopup.isShowing())) {
      this.mPopup.dismiss();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if ((this.mPopup != null) && (View.MeasureSpec.getMode(paramInt1) == Integer.MIN_VALUE)) {
      setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), compatMeasureContentWidth(getAdapter(), getBackground())), View.MeasureSpec.getSize(paramInt1)), getMeasuredHeight());
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((this.mForwardingListener != null) && (this.mForwardingListener.onTouch(this, paramMotionEvent))) {}
    for (boolean bool = true;; bool = super.onTouchEvent(paramMotionEvent)) {
      return bool;
    }
  }
  
  public boolean performClick()
  {
    if (this.mPopup != null) {
      if (!this.mPopup.isShowing()) {
        this.mPopup.show();
      }
    }
    for (boolean bool = true;; bool = super.performClick()) {
      return bool;
    }
  }
  
  public void setAdapter(SpinnerAdapter paramSpinnerAdapter)
  {
    if (!this.mPopupSet) {
      this.mTempAdapter = paramSpinnerAdapter;
    }
    do
    {
      return;
      super.setAdapter(paramSpinnerAdapter);
    } while (this.mPopup == null);
    if (this.mPopupContext == null) {}
    for (Context localContext = getContext();; localContext = this.mPopupContext)
    {
      this.mPopup.setAdapter(new DropDownAdapter(paramSpinnerAdapter, localContext.getTheme()));
      break;
    }
  }
  
  public void setBackgroundDrawable(Drawable paramDrawable)
  {
    super.setBackgroundDrawable(paramDrawable);
    if (this.mBackgroundTintHelper != null) {
      this.mBackgroundTintHelper.onSetBackgroundDrawable(paramDrawable);
    }
  }
  
  public void setBackgroundResource(@DrawableRes int paramInt)
  {
    super.setBackgroundResource(paramInt);
    if (this.mBackgroundTintHelper != null) {
      this.mBackgroundTintHelper.onSetBackgroundResource(paramInt);
    }
  }
  
  public void setDropDownHorizontalOffset(int paramInt)
  {
    if (this.mPopup != null) {
      this.mPopup.setHorizontalOffset(paramInt);
    }
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 16) {
        super.setDropDownHorizontalOffset(paramInt);
      }
    }
  }
  
  public void setDropDownVerticalOffset(int paramInt)
  {
    if (this.mPopup != null) {
      this.mPopup.setVerticalOffset(paramInt);
    }
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 16) {
        super.setDropDownVerticalOffset(paramInt);
      }
    }
  }
  
  public void setDropDownWidth(int paramInt)
  {
    if (this.mPopup != null) {
      this.mDropDownWidth = paramInt;
    }
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 16) {
        super.setDropDownWidth(paramInt);
      }
    }
  }
  
  public void setPopupBackgroundDrawable(Drawable paramDrawable)
  {
    if (this.mPopup != null) {
      this.mPopup.setBackgroundDrawable(paramDrawable);
    }
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 16) {
        super.setPopupBackgroundDrawable(paramDrawable);
      }
    }
  }
  
  public void setPopupBackgroundResource(@DrawableRes int paramInt)
  {
    setPopupBackgroundDrawable(AppCompatResources.getDrawable(getPopupContext(), paramInt));
  }
  
  public void setPrompt(CharSequence paramCharSequence)
  {
    if (this.mPopup != null) {
      this.mPopup.setPromptText(paramCharSequence);
    }
    for (;;)
    {
      return;
      super.setPrompt(paramCharSequence);
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setSupportBackgroundTintList(@Nullable ColorStateList paramColorStateList)
  {
    if (this.mBackgroundTintHelper != null) {
      this.mBackgroundTintHelper.setSupportBackgroundTintList(paramColorStateList);
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode paramMode)
  {
    if (this.mBackgroundTintHelper != null) {
      this.mBackgroundTintHelper.setSupportBackgroundTintMode(paramMode);
    }
  }
  
  private static class DropDownAdapter
    implements ListAdapter, SpinnerAdapter
  {
    private SpinnerAdapter mAdapter;
    private ListAdapter mListAdapter;
    
    public DropDownAdapter(@Nullable SpinnerAdapter paramSpinnerAdapter, @Nullable Resources.Theme paramTheme)
    {
      this.mAdapter = paramSpinnerAdapter;
      if ((paramSpinnerAdapter instanceof ListAdapter)) {
        this.mListAdapter = ((ListAdapter)paramSpinnerAdapter);
      }
      if (paramTheme != null)
      {
        if ((Build.VERSION.SDK_INT < 23) || (!(paramSpinnerAdapter instanceof android.widget.ThemedSpinnerAdapter))) {
          break label66;
        }
        paramSpinnerAdapter = (android.widget.ThemedSpinnerAdapter)paramSpinnerAdapter;
        if (paramSpinnerAdapter.getDropDownViewTheme() != paramTheme) {
          paramSpinnerAdapter.setDropDownViewTheme(paramTheme);
        }
      }
      for (;;)
      {
        return;
        label66:
        if ((paramSpinnerAdapter instanceof ThemedSpinnerAdapter))
        {
          paramSpinnerAdapter = (ThemedSpinnerAdapter)paramSpinnerAdapter;
          if (paramSpinnerAdapter.getDropDownViewTheme() == null) {
            paramSpinnerAdapter.setDropDownViewTheme(paramTheme);
          }
        }
      }
    }
    
    public boolean areAllItemsEnabled()
    {
      ListAdapter localListAdapter = this.mListAdapter;
      if (localListAdapter != null) {}
      for (boolean bool = localListAdapter.areAllItemsEnabled();; bool = true) {
        return bool;
      }
    }
    
    public int getCount()
    {
      if (this.mAdapter == null) {}
      for (int i = 0;; i = this.mAdapter.getCount()) {
        return i;
      }
    }
    
    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (this.mAdapter == null) {}
      for (paramView = null;; paramView = this.mAdapter.getDropDownView(paramInt, paramView, paramViewGroup)) {
        return paramView;
      }
    }
    
    public Object getItem(int paramInt)
    {
      if (this.mAdapter == null) {}
      for (Object localObject = null;; localObject = this.mAdapter.getItem(paramInt)) {
        return localObject;
      }
    }
    
    public long getItemId(int paramInt)
    {
      if (this.mAdapter == null) {}
      for (long l = -1L;; l = this.mAdapter.getItemId(paramInt)) {
        return l;
      }
    }
    
    public int getItemViewType(int paramInt)
    {
      return 0;
    }
    
    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      return getDropDownView(paramInt, paramView, paramViewGroup);
    }
    
    public int getViewTypeCount()
    {
      return 1;
    }
    
    public boolean hasStableIds()
    {
      if ((this.mAdapter != null) && (this.mAdapter.hasStableIds())) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public boolean isEmpty()
    {
      if (getCount() == 0) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public boolean isEnabled(int paramInt)
    {
      ListAdapter localListAdapter = this.mListAdapter;
      if (localListAdapter != null) {}
      for (boolean bool = localListAdapter.isEnabled(paramInt);; bool = true) {
        return bool;
      }
    }
    
    public void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      if (this.mAdapter != null) {
        this.mAdapter.registerDataSetObserver(paramDataSetObserver);
      }
    }
    
    public void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      if (this.mAdapter != null) {
        this.mAdapter.unregisterDataSetObserver(paramDataSetObserver);
      }
    }
  }
  
  private class DropdownPopup
    extends ListPopupWindow
  {
    ListAdapter mAdapter;
    private CharSequence mHintText;
    private final Rect mVisibleRect = new Rect();
    
    public DropdownPopup(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
    {
      super(paramAttributeSet, paramInt);
      setAnchorView(AppCompatSpinner.this);
      setModal(true);
      setPromptPosition(0);
      setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          AppCompatSpinner.this.setSelection(paramAnonymousInt);
          if (AppCompatSpinner.this.getOnItemClickListener() != null) {
            AppCompatSpinner.this.performItemClick(paramAnonymousView, paramAnonymousInt, AppCompatSpinner.DropdownPopup.this.mAdapter.getItemId(paramAnonymousInt));
          }
          AppCompatSpinner.DropdownPopup.this.dismiss();
        }
      });
    }
    
    void computeContentWidth()
    {
      Object localObject = getBackground();
      int i = 0;
      int i2;
      int n;
      int i1;
      if (localObject != null)
      {
        ((Drawable)localObject).getPadding(AppCompatSpinner.this.mTempRect);
        if (ViewUtils.isLayoutRtl(AppCompatSpinner.this))
        {
          i = AppCompatSpinner.this.mTempRect.right;
          i2 = AppCompatSpinner.this.getPaddingLeft();
          n = AppCompatSpinner.this.getPaddingRight();
          i1 = AppCompatSpinner.this.getWidth();
          if (AppCompatSpinner.this.mDropDownWidth != -2) {
            break label245;
          }
          int k = AppCompatSpinner.this.compatMeasureContentWidth((SpinnerAdapter)this.mAdapter, getBackground());
          int m = AppCompatSpinner.this.getContext().getResources().getDisplayMetrics().widthPixels - AppCompatSpinner.this.mTempRect.left - AppCompatSpinner.this.mTempRect.right;
          int j = k;
          if (k > m) {
            j = m;
          }
          setContentWidth(Math.max(j, i1 - i2 - n));
          label172:
          if (!ViewUtils.isLayoutRtl(AppCompatSpinner.this)) {
            break label285;
          }
          i += i1 - n - getWidth();
        }
      }
      for (;;)
      {
        setHorizontalOffset(i);
        return;
        i = -AppCompatSpinner.this.mTempRect.left;
        break;
        localObject = AppCompatSpinner.this.mTempRect;
        AppCompatSpinner.this.mTempRect.right = 0;
        ((Rect)localObject).left = 0;
        break;
        label245:
        if (AppCompatSpinner.this.mDropDownWidth == -1)
        {
          setContentWidth(i1 - i2 - n);
          break label172;
        }
        setContentWidth(AppCompatSpinner.this.mDropDownWidth);
        break label172;
        label285:
        i += i2;
      }
    }
    
    public CharSequence getHintText()
    {
      return this.mHintText;
    }
    
    boolean isVisibleToUser(View paramView)
    {
      if ((ViewCompat.isAttachedToWindow(paramView)) && (paramView.getGlobalVisibleRect(this.mVisibleRect))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public void setAdapter(ListAdapter paramListAdapter)
    {
      super.setAdapter(paramListAdapter);
      this.mAdapter = paramListAdapter;
    }
    
    public void setPromptText(CharSequence paramCharSequence)
    {
      this.mHintText = paramCharSequence;
    }
    
    public void show()
    {
      boolean bool = isShowing();
      computeContentWidth();
      setInputMethodMode(2);
      super.show();
      getListView().setChoiceMode(1);
      setSelection(AppCompatSpinner.this.getSelectedItemPosition());
      if (bool) {}
      for (;;)
      {
        return;
        ViewTreeObserver localViewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
        if (localViewTreeObserver != null)
        {
          final ViewTreeObserver.OnGlobalLayoutListener local2 = new ViewTreeObserver.OnGlobalLayoutListener()
          {
            public void onGlobalLayout()
            {
              if (!AppCompatSpinner.DropdownPopup.this.isVisibleToUser(AppCompatSpinner.this)) {
                AppCompatSpinner.DropdownPopup.this.dismiss();
              }
              for (;;)
              {
                return;
                AppCompatSpinner.DropdownPopup.this.computeContentWidth();
                AppCompatSpinner.DropdownPopup.this.show();
              }
            }
          };
          localViewTreeObserver.addOnGlobalLayoutListener(local2);
          setOnDismissListener(new PopupWindow.OnDismissListener()
          {
            public void onDismiss()
            {
              ViewTreeObserver localViewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
              if (localViewTreeObserver != null) {
                localViewTreeObserver.removeGlobalOnLayoutListener(local2);
              }
            }
          });
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\AppCompatSpinner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */