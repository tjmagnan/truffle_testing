package io.fabric.sdk.android.services.concurrency.internal;

public class DefaultRetryPolicy
  implements RetryPolicy
{
  private final int maxRetries;
  
  public DefaultRetryPolicy()
  {
    this(1);
  }
  
  public DefaultRetryPolicy(int paramInt)
  {
    this.maxRetries = paramInt;
  }
  
  public boolean shouldRetry(int paramInt, Throwable paramThrowable)
  {
    if (paramInt < this.maxRetries) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\concurrency\internal\DefaultRetryPolicy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */