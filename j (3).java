package com.testfairy.j;

import android.os.Process;
import android.util.Log;
import com.testfairy.e;
import com.testfairy.p.k;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

public abstract class c
  extends Thread
{
  protected static final String c = "testfairy-log";
  protected b d;
  protected boolean e = false;
  protected volatile boolean f = false;
  protected volatile boolean g = false;
  protected int h;
  protected int i;
  
  public c(b paramb)
  {
    this.d = paramb;
    setName("testfairy-log");
    this.i = Process.myPid();
    this.h = Process.myUid();
    a(this.h);
  }
  
  private void a(int paramInt)
  {
    Iterator localIterator = k.a().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (k.a(Integer.valueOf(str).intValue(), paramInt)) {
        try
        {
          Runtime localRuntime = Runtime.getRuntime();
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localRuntime.exec("kill -9 " + str);
        }
        catch (IOException localIOException)
        {
          Log.e(e.a, "E", localIOException);
        }
      }
    }
  }
  
  private void b(int paramInt)
  {
    Object localObject = k.b(paramInt);
    if (localObject != null)
    {
      localObject = ((String)localObject).split(",");
      paramInt = 0;
      for (;;)
      {
        if (paramInt < localObject.length) {
          try
          {
            Runtime localRuntime = Runtime.getRuntime();
            StringBuilder localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localRuntime.exec("kill -9 " + localObject[paramInt]);
            paramInt++;
          }
          catch (IOException localIOException)
          {
            for (;;)
            {
              Log.e(e.a, "E", localIOException);
            }
          }
        }
      }
    }
  }
  
  public void a()
  {
    this.g = true;
  }
  
  public void b()
  {
    this.f = true;
  }
  
  public void c()
  {
    this.f = false;
  }
  
  public boolean d()
  {
    return this.f;
  }
  
  static class a
  {
    long a;
    String b;
    String c;
    String d;
    String e;
    
    public String toString()
    {
      String str = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(Long.valueOf(this.a));
      return "LogObject:\n\ttimestemp = " + this.a + " (" + str + ")\n" + "\tlevel = " + this.b + "\n" + "\ttag = " + this.c + "\n" + "\tpid = " + this.e + "\n" + "\ttext = " + this.d;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\j\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */