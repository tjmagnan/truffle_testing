package com.testfairy.h;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.protocol.HttpContext;

class b
  implements Runnable
{
  private final AbstractHttpClient a;
  private final HttpContext b;
  private final HttpUriRequest c;
  private final c d;
  private boolean e;
  private int f;
  
  public b(AbstractHttpClient paramAbstractHttpClient, HttpContext paramHttpContext, HttpUriRequest paramHttpUriRequest, c paramc)
  {
    this.a = paramAbstractHttpClient;
    this.b = paramHttpContext;
    this.c = paramHttpUriRequest;
    this.d = paramc;
    if ((paramc instanceof d)) {
      this.e = true;
    }
  }
  
  private void a()
  {
    if (!Thread.currentThread().isInterrupted())
    {
      HttpResponse localHttpResponse = this.a.execute(this.c, this.b);
      if ((!Thread.currentThread().isInterrupted()) && (this.d != null)) {
        this.d.a(localHttpResponse);
      }
    }
  }
  
  private void b()
  {
    boolean bool = true;
    Object localObject1 = null;
    Object localObject2 = this.a.getHttpRequestRetryHandler();
    IOException localIOException2;
    while (bool) {
      try
      {
        a();
        return;
      }
      catch (UnknownHostException localUnknownHostException)
      {
        for (;;)
        {
          if (this.d != null) {
            this.d.b(localUnknownHostException, "can't resolve host");
          }
        }
      }
      catch (IOException localIOException1)
      {
        i = this.f + 1;
        this.f = i;
        bool = ((HttpRequestRetryHandler)localObject2).retryRequest(localIOException1, i, this.b);
      }
      catch (NullPointerException localNullPointerException)
      {
        localIOException2 = new IOException("NPE in HttpClient" + localNullPointerException.getMessage());
        int i = this.f + 1;
        this.f = i;
        bool = ((HttpRequestRetryHandler)localObject2).retryRequest(localIOException2, i, this.b);
      }
    }
    localObject2 = new ConnectException();
    ((ConnectException)localObject2).initCause(localIOException2);
    throw ((Throwable)localObject2);
  }
  
  public void run()
  {
    try
    {
      if (this.d != null) {
        this.d.c();
      }
      b();
      if (this.d != null) {
        this.d.d();
      }
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        if (this.d != null)
        {
          this.d.d();
          if (this.e) {
            this.d.a(localIOException, (byte[])null);
          } else {
            this.d.b(localIOException, (String)null);
          }
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */