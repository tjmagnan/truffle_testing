package android.support.v7.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.TintContextWrapper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import android.view.View.OnClickListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

class AppCompatViewInflater
{
  private static final String LOG_TAG = "AppCompatViewInflater";
  private static final String[] sClassPrefixList = { "android.widget.", "android.view.", "android.webkit." };
  private static final Map<String, Constructor<? extends View>> sConstructorMap = new ArrayMap();
  private static final Class<?>[] sConstructorSignature = { Context.class, AttributeSet.class };
  private static final int[] sOnClickAttrs = { 16843375 };
  private final Object[] mConstructorArgs = new Object[2];
  
  private void checkOnClickListener(View paramView, AttributeSet paramAttributeSet)
  {
    Object localObject = paramView.getContext();
    if ((!(localObject instanceof ContextWrapper)) || ((Build.VERSION.SDK_INT >= 15) && (!ViewCompat.hasOnClickListeners(paramView)))) {}
    for (;;)
    {
      return;
      localObject = ((Context)localObject).obtainStyledAttributes(paramAttributeSet, sOnClickAttrs);
      paramAttributeSet = ((TypedArray)localObject).getString(0);
      if (paramAttributeSet != null) {
        paramView.setOnClickListener(new DeclaredOnClickListener(paramView, paramAttributeSet));
      }
      ((TypedArray)localObject).recycle();
    }
  }
  
  private View createView(Context paramContext, String paramString1, String paramString2)
    throws ClassNotFoundException, InflateException
  {
    Constructor localConstructor = (Constructor)sConstructorMap.get(paramString1);
    Object localObject = localConstructor;
    if (localConstructor == null) {}
    for (;;)
    {
      try
      {
        localObject = paramContext.getClassLoader();
        if (paramString2 == null) {
          continue;
        }
        paramContext = new java/lang/StringBuilder;
        paramContext.<init>();
        paramContext = paramString2 + paramString1;
        localObject = ((ClassLoader)localObject).loadClass(paramContext).asSubclass(View.class).getConstructor(sConstructorSignature);
        sConstructorMap.put(paramString1, localObject);
        ((Constructor)localObject).setAccessible(true);
        paramContext = (View)((Constructor)localObject).newInstance(this.mConstructorArgs);
      }
      catch (Exception paramContext)
      {
        paramContext = null;
        continue;
      }
      return paramContext;
      paramContext = paramString1;
    }
  }
  
  /* Error */
  private View createViewFromTag(Context paramContext, String paramString, AttributeSet paramAttributeSet)
  {
    // Byte code:
    //   0: aload_2
    //   1: astore 5
    //   3: aload_2
    //   4: ldc -95
    //   6: invokevirtual 165	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   9: ifeq +14 -> 23
    //   12: aload_3
    //   13: aconst_null
    //   14: ldc -89
    //   16: invokeinterface 171 3 0
    //   21: astore 5
    //   23: aload_0
    //   24: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   27: iconst_0
    //   28: aload_1
    //   29: aastore
    //   30: aload_0
    //   31: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   34: iconst_1
    //   35: aload_3
    //   36: aastore
    //   37: iconst_m1
    //   38: aload 5
    //   40: bipush 46
    //   42: invokevirtual 175	java/lang/String:indexOf	(I)I
    //   45: if_icmpne +76 -> 121
    //   48: iconst_0
    //   49: istore 4
    //   51: iload 4
    //   53: getstatic 46	android/support/v7/app/AppCompatViewInflater:sClassPrefixList	[Ljava/lang/String;
    //   56: arraylength
    //   57: if_icmpge +45 -> 102
    //   60: aload_0
    //   61: aload_1
    //   62: aload 5
    //   64: getstatic 46	android/support/v7/app/AppCompatViewInflater:sClassPrefixList	[Ljava/lang/String;
    //   67: iload 4
    //   69: aaload
    //   70: invokespecial 177	android/support/v7/app/AppCompatViewInflater:createView	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    //   73: astore_2
    //   74: aload_2
    //   75: ifnull +21 -> 96
    //   78: aload_0
    //   79: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   82: iconst_0
    //   83: aconst_null
    //   84: aastore
    //   85: aload_0
    //   86: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   89: iconst_1
    //   90: aconst_null
    //   91: aastore
    //   92: aload_2
    //   93: astore_1
    //   94: aload_1
    //   95: areturn
    //   96: iinc 4 1
    //   99: goto -48 -> 51
    //   102: aload_0
    //   103: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   106: iconst_0
    //   107: aconst_null
    //   108: aastore
    //   109: aload_0
    //   110: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   113: iconst_1
    //   114: aconst_null
    //   115: aastore
    //   116: aconst_null
    //   117: astore_1
    //   118: goto -24 -> 94
    //   121: aload_0
    //   122: aload_1
    //   123: aload 5
    //   125: aconst_null
    //   126: invokespecial 177	android/support/v7/app/AppCompatViewInflater:createView	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    //   129: astore_1
    //   130: aload_0
    //   131: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   134: iconst_0
    //   135: aconst_null
    //   136: aastore
    //   137: aload_0
    //   138: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   141: iconst_1
    //   142: aconst_null
    //   143: aastore
    //   144: goto -50 -> 94
    //   147: astore_1
    //   148: aload_0
    //   149: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   152: iconst_0
    //   153: aconst_null
    //   154: aastore
    //   155: aload_0
    //   156: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   159: iconst_1
    //   160: aconst_null
    //   161: aastore
    //   162: aconst_null
    //   163: astore_1
    //   164: goto -70 -> 94
    //   167: astore_1
    //   168: aload_0
    //   169: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   172: iconst_0
    //   173: aconst_null
    //   174: aastore
    //   175: aload_0
    //   176: getfield 57	android/support/v7/app/AppCompatViewInflater:mConstructorArgs	[Ljava/lang/Object;
    //   179: iconst_1
    //   180: aconst_null
    //   181: aastore
    //   182: aload_1
    //   183: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	184	0	this	AppCompatViewInflater
    //   0	184	1	paramContext	Context
    //   0	184	2	paramString	String
    //   0	184	3	paramAttributeSet	AttributeSet
    //   49	48	4	i	int
    //   1	123	5	str	String
    // Exception table:
    //   from	to	target	type
    //   23	48	147	java/lang/Exception
    //   51	74	147	java/lang/Exception
    //   121	130	147	java/lang/Exception
    //   23	48	167	finally
    //   51	74	167	finally
    //   121	130	167	finally
  }
  
  private static Context themifyContext(Context paramContext, AttributeSet paramAttributeSet, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.View, 0, 0);
    int i = 0;
    if (paramBoolean1) {
      i = paramAttributeSet.getResourceId(R.styleable.View_android_theme, 0);
    }
    int j = i;
    if (paramBoolean2)
    {
      j = i;
      if (i == 0)
      {
        i = paramAttributeSet.getResourceId(R.styleable.View_theme, 0);
        j = i;
        if (i != 0)
        {
          Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
          j = i;
        }
      }
    }
    paramAttributeSet.recycle();
    paramAttributeSet = paramContext;
    if (j != 0) {
      if ((paramContext instanceof ContextThemeWrapper))
      {
        paramAttributeSet = paramContext;
        if (((ContextThemeWrapper)paramContext).getThemeResId() == j) {}
      }
      else
      {
        paramAttributeSet = new ContextThemeWrapper(paramContext, j);
      }
    }
    return paramAttributeSet;
  }
  
  public final View createView(View paramView, String paramString, @NonNull Context paramContext, @NonNull AttributeSet paramAttributeSet, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    Object localObject = paramContext;
    if (paramBoolean1)
    {
      localObject = paramContext;
      if (paramView != null) {
        localObject = paramView.getContext();
      }
    }
    if (!paramBoolean2)
    {
      paramView = (View)localObject;
      if (!paramBoolean3) {}
    }
    else
    {
      paramView = themifyContext((Context)localObject, paramAttributeSet, paramBoolean2, paramBoolean3);
    }
    localObject = paramView;
    if (paramBoolean4) {
      localObject = TintContextWrapper.wrap(paramView);
    }
    paramView = null;
    int i = -1;
    switch (paramString.hashCode())
    {
    default: 
      switch (i)
      {
      }
      break;
    }
    for (;;)
    {
      View localView = paramView;
      if (paramView == null)
      {
        localView = paramView;
        if (paramContext != localObject) {
          localView = createViewFromTag((Context)localObject, paramString, paramAttributeSet);
        }
      }
      if (localView != null) {
        checkOnClickListener(localView, paramAttributeSet);
      }
      return localView;
      if (!paramString.equals("TextView")) {
        break;
      }
      i = 0;
      break;
      if (!paramString.equals("ImageView")) {
        break;
      }
      i = 1;
      break;
      if (!paramString.equals("Button")) {
        break;
      }
      i = 2;
      break;
      if (!paramString.equals("EditText")) {
        break;
      }
      i = 3;
      break;
      if (!paramString.equals("Spinner")) {
        break;
      }
      i = 4;
      break;
      if (!paramString.equals("ImageButton")) {
        break;
      }
      i = 5;
      break;
      if (!paramString.equals("CheckBox")) {
        break;
      }
      i = 6;
      break;
      if (!paramString.equals("RadioButton")) {
        break;
      }
      i = 7;
      break;
      if (!paramString.equals("CheckedTextView")) {
        break;
      }
      i = 8;
      break;
      if (!paramString.equals("AutoCompleteTextView")) {
        break;
      }
      i = 9;
      break;
      if (!paramString.equals("MultiAutoCompleteTextView")) {
        break;
      }
      i = 10;
      break;
      if (!paramString.equals("RatingBar")) {
        break;
      }
      i = 11;
      break;
      if (!paramString.equals("SeekBar")) {
        break;
      }
      i = 12;
      break;
      paramView = new AppCompatTextView((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatImageView((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatButton((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatEditText((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatSpinner((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatImageButton((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatCheckBox((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatRadioButton((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatCheckedTextView((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatAutoCompleteTextView((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatMultiAutoCompleteTextView((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatRatingBar((Context)localObject, paramAttributeSet);
      continue;
      paramView = new AppCompatSeekBar((Context)localObject, paramAttributeSet);
    }
  }
  
  private static class DeclaredOnClickListener
    implements View.OnClickListener
  {
    private final View mHostView;
    private final String mMethodName;
    private Context mResolvedContext;
    private Method mResolvedMethod;
    
    public DeclaredOnClickListener(@NonNull View paramView, @NonNull String paramString)
    {
      this.mHostView = paramView;
      this.mMethodName = paramString;
    }
    
    @NonNull
    private void resolveMethod(@Nullable Context paramContext, @NonNull String paramString)
    {
      while (paramContext != null) {
        try
        {
          if (!paramContext.isRestricted())
          {
            paramString = paramContext.getClass().getMethod(this.mMethodName, new Class[] { View.class });
            if (paramString != null)
            {
              this.mResolvedMethod = paramString;
              this.mResolvedContext = paramContext;
              return;
            }
          }
        }
        catch (NoSuchMethodException paramString)
        {
          if ((paramContext instanceof ContextWrapper)) {
            paramContext = ((ContextWrapper)paramContext).getBaseContext();
          } else {
            paramContext = null;
          }
        }
      }
      int i = this.mHostView.getId();
      if (i == -1) {}
      for (paramContext = "";; paramContext = " with id '" + this.mHostView.getContext().getResources().getResourceEntryName(i) + "'") {
        throw new IllegalStateException("Could not find method " + this.mMethodName + "(View) in a parent or ancestor Context for android:onClick " + "attribute defined on view " + this.mHostView.getClass() + paramContext);
      }
    }
    
    public void onClick(@NonNull View paramView)
    {
      if (this.mResolvedMethod == null) {
        resolveMethod(this.mHostView.getContext(), this.mMethodName);
      }
      try
      {
        this.mResolvedMethod.invoke(this.mResolvedContext, new Object[] { paramView });
        return;
      }
      catch (IllegalAccessException paramView)
      {
        throw new IllegalStateException("Could not execute non-public method for android:onClick", paramView);
      }
      catch (InvocationTargetException paramView)
      {
        throw new IllegalStateException("Could not execute method for android:onClick", paramView);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\app\AppCompatViewInflater.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */