package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.android.volley.VolleyError;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBContact;
import tech.dcube.companion.model.PBSendProItem;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBShipmentRate;
import tech.dcube.companion.model.PBShipmentService;

public class PBShipmentSummaryFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  TextView MailClassServiceCost;
  TextView MailClassServiceName;
  AppCompatButton PayWithSendPro;
  TextView RecipientAddress_1;
  TextView RecipientAddress_2_city_state_zipcode;
  TextView RecipientAddress_3_country;
  TextView RecipientName;
  ImageView ShipItemImage;
  TextView ShipItemName;
  private ProgressBar mAmountProgressBar;
  private FrameLayout mFadeFL;
  private OnShipmentSummaryListener mListener;
  private String mParam1;
  private String mParam2;
  private ProgressBar mProgressBar;
  private LinearLayout shipItemLayout;
  
  public static PBShipmentSummaryFragment newInstance(String paramString1, String paramString2)
  {
    PBShipmentSummaryFragment localPBShipmentSummaryFragment = new PBShipmentSummaryFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBShipmentSummaryFragment.setArguments(localBundle);
    return localPBShipmentSummaryFragment;
  }
  
  void callShipmentRateApi()
  {
    showLoadingAmountProgressBar();
    PBAddress localPBAddress1 = DataManager.getInstance().getCurrentShipment().getSenderAddress();
    PBAddress localPBAddress2 = DataManager.getInstance().getCurrentShipment().getReceiverAddress();
    PBShipmentPackage localPBShipmentPackage = DataManager.getInstance().getCurrentShipment().getShipmentPackage();
    PBShipmentService localPBShipmentService = DataManager.getInstance().getCurrentShipment().getShipmentService();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local1 = new tech/dcube/companion/fragments/PBShipmentSummaryFragment$1;
      local1.<init>(this);
      localServerManager.getShipmentRate(localContext, localPBAddress1, localPBAddress2, localPBShipmentPackage, localPBShipmentService, local1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        hideLoadingAmountProgressBar();
      }
    }
  }
  
  public void hideLoadingAmountProgressBar()
  {
    try
    {
      this.mAmountProgressBar.setVisibility(4);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void hideLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(8);
      this.mFadeFL.setVisibility(8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnShipmentSummaryListener))
    {
      this.mListener = ((OnShipmentSummaryListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnShipmentSummaryListener");
  }
  
  public void onBackButtonInteraction()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentSummary("action_back_button");
    }
  }
  
  public void onCancelButtonInteraction()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentSummary("action_cancel_button");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    onCancelButtonInteraction();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968644, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Summary");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Cancel");
    this.shipItemLayout = ((LinearLayout)paramLayoutInflater.findViewById(2131624182));
    if (DataManager.getInstance().getCurrentShipment().isManualShipment()) {
      this.shipItemLayout.setVisibility(8);
    }
    for (;;)
    {
      this.MailClassServiceName = ((TextView)paramLayoutInflater.findViewById(2131624179));
      this.MailClassServiceCost = ((TextView)paramLayoutInflater.findViewById(2131624180));
      this.ShipItemName = ((TextView)paramLayoutInflater.findViewById(2131624184));
      this.ShipItemImage = ((ImageView)paramLayoutInflater.findViewById(2131624183));
      this.RecipientName = ((TextView)paramLayoutInflater.findViewById(2131624185));
      this.RecipientAddress_1 = ((TextView)paramLayoutInflater.findViewById(2131624186));
      this.RecipientAddress_2_city_state_zipcode = ((TextView)paramLayoutInflater.findViewById(2131624187));
      this.RecipientAddress_3_country = ((TextView)paramLayoutInflater.findViewById(2131624188));
      this.mAmountProgressBar = ((ProgressBar)paramLayoutInflater.findViewById(2131624181));
      this.mProgressBar = ((ProgressBar)paramLayoutInflater.findViewById(2131624063));
      this.mFadeFL = ((FrameLayout)paramLayoutInflater.findViewById(2131624062));
      hideLoadingOverlay();
      paramViewGroup = (AppCompatButton)paramLayoutInflater.findViewById(2131624189);
      String str2 = DataManager.getInstance().getCurrentShipment().getItem().getItemName();
      String str3 = DataManager.getInstance().getCurrentShipment().getShipmentService().getServiceName();
      paramBundle = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getContact().getFullName();
      String str1 = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getStreetLine1();
      String str6 = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getCity();
      String str5 = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getState();
      String str4 = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getPostalCode();
      paramViewGroup = DataManager.getInstance().getCurrentShipment().getReceiverAddress().getIsoCountry();
      str4 = str6 + ", " + str5 + " " + str4;
      this.MailClassServiceName.setText(str3);
      this.MailClassServiceCost.setText("");
      this.ShipItemName.setText(str2);
      this.RecipientName.setText(paramBundle);
      this.RecipientAddress_1.setText(str1);
      this.RecipientAddress_2_city_state_zipcode.setText(str4);
      this.RecipientAddress_3_country.setText(paramViewGroup);
      callShipmentRateApi();
      return paramLayoutInflater;
      this.shipItemLayout.setVisibility(0);
    }
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPayWithSendProInteraction()
  {
    if (this.mListener != null) {
      this.mListener.onShipmentSummary("action_pay_with_send_pro");
    }
  }
  
  void onShipmentRateInteraction(PBShipmentRate paramPBShipmentRate)
  {
    if (this.mListener != null) {
      this.mListener.onShipmentRateLoaded(paramPBShipmentRate);
    }
  }
  
  @OnClick({2131624189})
  public void payWithSendPro()
  {
    onPayWithSendProInteraction();
  }
  
  public void showLoadingAmountProgressBar()
  {
    try
    {
      this.mAmountProgressBar.setVisibility(0);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void showLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(0);
      this.mFadeFL.setVisibility(0);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void updateShipmentRate(double paramDouble, String paramString)
  {
    paramString = String.format("$ %.02f", new Object[] { Double.valueOf(paramDouble) });
    this.MailClassServiceCost.setText(paramString);
  }
  
  public static abstract interface OnShipmentSummaryListener
  {
    public abstract void onShipmentRateLoaded(PBShipmentRate paramPBShipmentRate);
    
    public abstract void onShipmentSummary(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBShipmentSummaryFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */