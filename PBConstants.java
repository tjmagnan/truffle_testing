package tech.dcube.companion.model;

public class PBConstants
{
  public static final String CLIENT_ID = "wTvlYFbGmfqgIlMdnKxw";
  public static final String PB_BASE_URL_IMG = "https://sendpro.pitneybowes.com/";
  public static final String PB_DATE_FROMAT = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String PB_URL_SENDPRO = "http://arrakis-dev.us-west-2.elasticbeanstalk.com";
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBConstants.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */