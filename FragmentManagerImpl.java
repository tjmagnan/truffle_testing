package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.support.v4.os.BuildCompat;
import android.support.v4.util.ArraySet;
import android.support.v4.util.DebugUtils;
import android.support.v4.util.LogWriter;
import android.support.v4.util.Pair;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

final class FragmentManagerImpl
  extends FragmentManager
  implements LayoutInflaterFactory
{
  static final Interpolator ACCELERATE_CUBIC = new AccelerateInterpolator(1.5F);
  static final Interpolator ACCELERATE_QUINT;
  static final int ANIM_DUR = 220;
  public static final int ANIM_STYLE_CLOSE_ENTER = 3;
  public static final int ANIM_STYLE_CLOSE_EXIT = 4;
  public static final int ANIM_STYLE_FADE_ENTER = 5;
  public static final int ANIM_STYLE_FADE_EXIT = 6;
  public static final int ANIM_STYLE_OPEN_ENTER = 1;
  public static final int ANIM_STYLE_OPEN_EXIT = 2;
  static boolean DEBUG = false;
  static final Interpolator DECELERATE_CUBIC;
  static final Interpolator DECELERATE_QUINT;
  static final boolean HONEYCOMB;
  static final String TAG = "FragmentManager";
  static final String TARGET_REQUEST_CODE_STATE_TAG = "android:target_req_state";
  static final String TARGET_STATE_TAG = "android:target_state";
  static final String USER_VISIBLE_HINT_TAG = "android:user_visible_hint";
  static final String VIEW_STATE_TAG = "android:view_state";
  static Field sAnimationListenerField;
  ArrayList<Fragment> mActive;
  ArrayList<Fragment> mAdded;
  ArrayList<Integer> mAvailBackStackIndices;
  ArrayList<Integer> mAvailIndices;
  ArrayList<BackStackRecord> mBackStack;
  ArrayList<FragmentManager.OnBackStackChangedListener> mBackStackChangeListeners;
  ArrayList<BackStackRecord> mBackStackIndices;
  FragmentContainer mContainer;
  ArrayList<Fragment> mCreatedMenus;
  int mCurState = 0;
  boolean mDestroyed;
  Runnable mExecCommit = new Runnable()
  {
    public void run()
    {
      FragmentManagerImpl.this.execPendingActions();
    }
  };
  boolean mExecutingActions;
  boolean mHavePendingDeferredStart;
  FragmentHostCallback mHost;
  private CopyOnWriteArrayList<Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean>> mLifecycleCallbacks;
  boolean mNeedMenuInvalidate;
  String mNoTransactionsBecause;
  Fragment mParent;
  ArrayList<OpGenerator> mPendingActions;
  ArrayList<StartEnterTransitionListener> mPostponedTransactions;
  SparseArray<Parcelable> mStateArray = null;
  Bundle mStateBundle = null;
  boolean mStateSaved;
  Runnable[] mTmpActions;
  ArrayList<Fragment> mTmpAddedFragments;
  ArrayList<Boolean> mTmpIsPop;
  ArrayList<BackStackRecord> mTmpRecords;
  
  static
  {
    boolean bool = false;
    DEBUG = false;
    if (Build.VERSION.SDK_INT >= 11) {
      bool = true;
    }
    HONEYCOMB = bool;
    sAnimationListenerField = null;
    DECELERATE_QUINT = new DecelerateInterpolator(2.5F);
    DECELERATE_CUBIC = new DecelerateInterpolator(1.5F);
    ACCELERATE_QUINT = new AccelerateInterpolator(2.5F);
  }
  
  private void addAddedFragments(ArraySet<Fragment> paramArraySet)
  {
    if (this.mCurState < 1) {
      return;
    }
    int k = Math.min(this.mCurState, 4);
    if (this.mAdded == null) {}
    for (int i = 0;; i = this.mAdded.size())
    {
      for (int j = 0; j < i; j++)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(j);
        if (localFragment.mState < k)
        {
          moveToState(localFragment, k, localFragment.getNextAnim(), localFragment.getNextTransition(), false);
          if ((localFragment.mView != null) && (!localFragment.mHidden) && (localFragment.mIsNewlyAdded)) {
            paramArraySet.add(localFragment);
          }
        }
      }
      break;
    }
  }
  
  private void checkStateLoss()
  {
    if (this.mStateSaved) {
      throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
    }
    if (this.mNoTransactionsBecause != null) {
      throw new IllegalStateException("Can not perform this action inside of " + this.mNoTransactionsBecause);
    }
  }
  
  private void cleanupExec()
  {
    this.mExecutingActions = false;
    this.mTmpIsPop.clear();
    this.mTmpRecords.clear();
  }
  
  private void completeExecute(BackStackRecord paramBackStackRecord, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject = new ArrayList(1);
    ArrayList localArrayList = new ArrayList(1);
    ((ArrayList)localObject).add(paramBackStackRecord);
    localArrayList.add(Boolean.valueOf(paramBoolean1));
    executeOps((ArrayList)localObject, localArrayList, 0, 1);
    if (paramBoolean2) {
      FragmentTransition.startTransitions(this, (ArrayList)localObject, localArrayList, 0, 1, true);
    }
    if (paramBoolean3) {
      moveToState(this.mCurState, true);
    }
    if (this.mActive != null)
    {
      int j = this.mActive.size();
      int i = 0;
      if (i < j)
      {
        localObject = (Fragment)this.mActive.get(i);
        if ((localObject != null) && (((Fragment)localObject).mView != null) && (((Fragment)localObject).mIsNewlyAdded) && (paramBackStackRecord.interactsWith(((Fragment)localObject).mContainerId)))
        {
          if ((Build.VERSION.SDK_INT >= 11) && (((Fragment)localObject).mPostponedAlpha > 0.0F)) {
            ((Fragment)localObject).mView.setAlpha(((Fragment)localObject).mPostponedAlpha);
          }
          if (!paramBoolean3) {
            break label196;
          }
          ((Fragment)localObject).mPostponedAlpha = 0.0F;
        }
        for (;;)
        {
          i++;
          break;
          label196:
          ((Fragment)localObject).mPostponedAlpha = -1.0F;
          ((Fragment)localObject).mIsNewlyAdded = false;
        }
      }
    }
  }
  
  private void endAnimatingAwayFragments()
  {
    if (this.mActive == null) {}
    for (int i = 0;; i = this.mActive.size()) {
      for (int j = 0; j < i; j++)
      {
        Fragment localFragment = (Fragment)this.mActive.get(j);
        if ((localFragment != null) && (localFragment.getAnimatingAway() != null))
        {
          int k = localFragment.getStateAfterAnimating();
          Object localObject = localFragment.getAnimatingAway();
          localFragment.setAnimatingAway(null);
          localObject = ((View)localObject).getAnimation();
          if (localObject != null) {
            ((Animation)localObject).cancel();
          }
          moveToState(localFragment, k, 0, 0, false);
        }
      }
    }
  }
  
  private void ensureExecReady(boolean paramBoolean)
  {
    if (this.mExecutingActions) {
      throw new IllegalStateException("FragmentManager is already executing transactions");
    }
    if (Looper.myLooper() != this.mHost.getHandler().getLooper()) {
      throw new IllegalStateException("Must be called from main thread of fragment host");
    }
    if (!paramBoolean) {
      checkStateLoss();
    }
    if (this.mTmpRecords == null)
    {
      this.mTmpRecords = new ArrayList();
      this.mTmpIsPop = new ArrayList();
    }
    this.mExecutingActions = true;
    try
    {
      executePostponedTransaction(null, null);
      return;
    }
    finally
    {
      this.mExecutingActions = false;
    }
  }
  
  private static void executeOps(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2)
  {
    if (paramInt1 < paramInt2)
    {
      BackStackRecord localBackStackRecord = (BackStackRecord)paramArrayList.get(paramInt1);
      boolean bool;
      if (((Boolean)paramArrayList1.get(paramInt1)).booleanValue())
      {
        localBackStackRecord.bumpBackStackNesting(-1);
        if (paramInt1 == paramInt2 - 1)
        {
          bool = true;
          label45:
          localBackStackRecord.executePopOps(bool);
        }
      }
      for (;;)
      {
        paramInt1++;
        break;
        bool = false;
        break label45;
        localBackStackRecord.bumpBackStackNesting(1);
        localBackStackRecord.executeOps();
      }
    }
  }
  
  private void executeOpsTogether(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2)
  {
    boolean bool = ((BackStackRecord)paramArrayList.get(paramInt1)).mAllowOptimization;
    int i = 0;
    label56:
    Object localObject;
    if (this.mTmpAddedFragments == null)
    {
      this.mTmpAddedFragments = new ArrayList();
      if (this.mAdded != null) {
        this.mTmpAddedFragments.addAll(this.mAdded);
      }
      j = paramInt1;
      if (j >= paramInt2) {
        break label148;
      }
      localObject = (BackStackRecord)paramArrayList.get(j);
      if (((Boolean)paramArrayList1.get(j)).booleanValue()) {
        break label130;
      }
      ((BackStackRecord)localObject).expandReplaceOps(this.mTmpAddedFragments);
      label98:
      if ((i == 0) && (!((BackStackRecord)localObject).mAddToBackStack)) {
        break label142;
      }
    }
    label130:
    label142:
    for (i = 1;; i = 0)
    {
      j++;
      break label56;
      this.mTmpAddedFragments.clear();
      break;
      ((BackStackRecord)localObject).trackAddedFragmentsInPop(this.mTmpAddedFragments);
      break label98;
    }
    label148:
    this.mTmpAddedFragments.clear();
    if (!bool) {
      FragmentTransition.startTransitions(this, paramArrayList, paramArrayList1, paramInt1, paramInt2, false);
    }
    executeOps(paramArrayList, paramArrayList1, paramInt1, paramInt2);
    int j = paramInt2;
    if (bool)
    {
      localObject = new ArraySet();
      addAddedFragments((ArraySet)localObject);
      j = postponePostponableTransactions(paramArrayList, paramArrayList1, paramInt1, paramInt2, (ArraySet)localObject);
      makeRemovedFragmentsInvisible((ArraySet)localObject);
    }
    if ((j != paramInt1) && (bool))
    {
      FragmentTransition.startTransitions(this, paramArrayList, paramArrayList1, paramInt1, j, true);
      moveToState(this.mCurState, true);
    }
    while (paramInt1 < paramInt2)
    {
      localObject = (BackStackRecord)paramArrayList.get(paramInt1);
      if ((((Boolean)paramArrayList1.get(paramInt1)).booleanValue()) && (((BackStackRecord)localObject).mIndex >= 0))
      {
        freeBackStackIndex(((BackStackRecord)localObject).mIndex);
        ((BackStackRecord)localObject).mIndex = -1;
      }
      paramInt1++;
    }
    if (i != 0) {
      reportBackStackChanged();
    }
  }
  
  private void executePostponedTransaction(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1)
  {
    int i;
    int k;
    int j;
    label18:
    StartEnterTransitionListener localStartEnterTransitionListener;
    int m;
    if (this.mPostponedTransactions == null)
    {
      i = 0;
      k = 0;
      j = i;
      i = k;
      if (i >= j) {
        return;
      }
      localStartEnterTransitionListener = (StartEnterTransitionListener)this.mPostponedTransactions.get(i);
      if ((paramArrayList == null) || (localStartEnterTransitionListener.mIsBack)) {
        break label116;
      }
      k = paramArrayList.indexOf(localStartEnterTransitionListener.mRecord);
      if ((k == -1) || (!((Boolean)paramArrayList1.get(k)).booleanValue())) {
        break label116;
      }
      localStartEnterTransitionListener.cancelTransaction();
      k = j;
      m = i;
    }
    for (;;)
    {
      i = m + 1;
      j = k;
      break label18;
      i = this.mPostponedTransactions.size();
      break;
      label116:
      if (!localStartEnterTransitionListener.isReady())
      {
        m = i;
        k = j;
        if (paramArrayList != null)
        {
          m = i;
          k = j;
          if (!localStartEnterTransitionListener.mRecord.interactsWith(paramArrayList, 0, paramArrayList.size())) {}
        }
      }
      else
      {
        this.mPostponedTransactions.remove(i);
        m = i - 1;
        k = j - 1;
        if ((paramArrayList != null) && (!localStartEnterTransitionListener.mIsBack))
        {
          i = paramArrayList.indexOf(localStartEnterTransitionListener.mRecord);
          if ((i != -1) && (((Boolean)paramArrayList1.get(i)).booleanValue()))
          {
            localStartEnterTransitionListener.cancelTransaction();
            continue;
          }
        }
        localStartEnterTransitionListener.completeTransaction();
      }
    }
  }
  
  private Fragment findFragmentUnder(Fragment paramFragment)
  {
    ViewGroup localViewGroup = paramFragment.mContainer;
    Object localObject = paramFragment.mView;
    if ((localViewGroup == null) || (localObject == null)) {
      paramFragment = null;
    }
    for (;;)
    {
      return paramFragment;
      for (int i = this.mAdded.indexOf(paramFragment) - 1;; i--)
      {
        if (i < 0) {
          break label75;
        }
        localObject = (Fragment)this.mAdded.get(i);
        if (((Fragment)localObject).mContainer == localViewGroup)
        {
          paramFragment = (Fragment)localObject;
          if (((Fragment)localObject).mView != null) {
            break;
          }
        }
      }
      label75:
      paramFragment = null;
    }
  }
  
  private void forcePostponedTransactions()
  {
    if (this.mPostponedTransactions != null) {
      while (!this.mPostponedTransactions.isEmpty()) {
        ((StartEnterTransitionListener)this.mPostponedTransactions.remove(0)).completeTransaction();
      }
    }
  }
  
  private boolean generateOpsForPendingActions(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1)
  {
    for (;;)
    {
      try
      {
        boolean bool;
        if ((this.mPendingActions == null) || (this.mPendingActions.size() == 0))
        {
          bool = false;
          return bool;
        }
        int j = this.mPendingActions.size();
        int i = 0;
        if (i < j)
        {
          ((OpGenerator)this.mPendingActions.get(i)).generateOps(paramArrayList, paramArrayList1);
          i++;
        }
        else
        {
          this.mPendingActions.clear();
          this.mHost.getHandler().removeCallbacks(this.mExecCommit);
          if (j > 0) {
            bool = true;
          } else {
            bool = false;
          }
        }
      }
      finally {}
    }
  }
  
  static Animation makeFadeAnimation(Context paramContext, float paramFloat1, float paramFloat2)
  {
    paramContext = new AlphaAnimation(paramFloat1, paramFloat2);
    paramContext.setInterpolator(DECELERATE_CUBIC);
    paramContext.setDuration(220L);
    return paramContext;
  }
  
  static Animation makeOpenCloseAnimation(Context paramContext, float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    paramContext = new AnimationSet(false);
    Object localObject = new ScaleAnimation(paramFloat1, paramFloat2, paramFloat1, paramFloat2, 1, 0.5F, 1, 0.5F);
    ((ScaleAnimation)localObject).setInterpolator(DECELERATE_QUINT);
    ((ScaleAnimation)localObject).setDuration(220L);
    paramContext.addAnimation((Animation)localObject);
    localObject = new AlphaAnimation(paramFloat3, paramFloat4);
    ((AlphaAnimation)localObject).setInterpolator(DECELERATE_CUBIC);
    ((AlphaAnimation)localObject).setDuration(220L);
    paramContext.addAnimation((Animation)localObject);
    return paramContext;
  }
  
  private void makeRemovedFragmentsInvisible(ArraySet<Fragment> paramArraySet)
  {
    int j = paramArraySet.size();
    int i = 0;
    if (i < j)
    {
      Fragment localFragment = (Fragment)paramArraySet.valueAt(i);
      View localView;
      if (!localFragment.mAdded)
      {
        localView = localFragment.getView();
        if (Build.VERSION.SDK_INT >= 11) {
          break label60;
        }
        localFragment.getView().setVisibility(4);
      }
      for (;;)
      {
        i++;
        break;
        label60:
        localFragment.mPostponedAlpha = localView.getAlpha();
        localView.setAlpha(0.0F);
      }
    }
  }
  
  static boolean modifiesAlpha(Animation paramAnimation)
  {
    boolean bool2 = true;
    boolean bool1;
    if ((paramAnimation instanceof AlphaAnimation)) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      if ((paramAnimation instanceof AnimationSet))
      {
        paramAnimation = ((AnimationSet)paramAnimation).getAnimations();
        for (int i = 0;; i++)
        {
          if (i >= paramAnimation.size()) {
            break label61;
          }
          bool1 = bool2;
          if ((paramAnimation.get(i) instanceof AlphaAnimation)) {
            break;
          }
        }
      }
      label61:
      bool1 = false;
    }
  }
  
  private void optimizeAndExecuteOps(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1)
  {
    if ((paramArrayList == null) || (paramArrayList.isEmpty())) {}
    for (;;)
    {
      return;
      if ((paramArrayList1 == null) || (paramArrayList.size() != paramArrayList1.size())) {
        throw new IllegalStateException("Internal error with the back stack records");
      }
      executePostponedTransaction(paramArrayList, paramArrayList1);
      int n = paramArrayList.size();
      int k = 0;
      int i = 0;
      while (i < n)
      {
        int m = i;
        int j = k;
        if (!((BackStackRecord)paramArrayList.get(i)).mAllowOptimization)
        {
          if (k != i) {
            executeOpsTogether(paramArrayList, paramArrayList1, k, i);
          }
          k = i + 1;
          j = k;
          if (((Boolean)paramArrayList1.get(i)).booleanValue()) {
            for (;;)
            {
              j = k;
              if (k >= n) {
                break;
              }
              j = k;
              if (!((Boolean)paramArrayList1.get(k)).booleanValue()) {
                break;
              }
              j = k;
              if (((BackStackRecord)paramArrayList.get(k)).mAllowOptimization) {
                break;
              }
              k++;
            }
          }
          executeOpsTogether(paramArrayList, paramArrayList1, i, j);
          i = j;
          m = j - 1;
          j = i;
        }
        i = m + 1;
        k = j;
      }
      if (k != n) {
        executeOpsTogether(paramArrayList, paramArrayList1, k, n);
      }
    }
  }
  
  private boolean popBackStackImmediate(String paramString, int paramInt1, int paramInt2)
  {
    execPendingActions();
    ensureExecReady(true);
    boolean bool = popBackStackState(this.mTmpRecords, this.mTmpIsPop, paramString, paramInt1, paramInt2);
    if (bool) {
      this.mExecutingActions = true;
    }
    try
    {
      optimizeAndExecuteOps(this.mTmpRecords, this.mTmpIsPop);
      cleanupExec();
      doPendingDeferredStart();
      return bool;
    }
    finally
    {
      cleanupExec();
    }
  }
  
  private int postponePostponableTransactions(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2, ArraySet<Fragment> paramArraySet)
  {
    int j = paramInt2;
    int i = paramInt2 - 1;
    if (i >= paramInt1)
    {
      BackStackRecord localBackStackRecord = (BackStackRecord)paramArrayList.get(i);
      boolean bool = ((Boolean)paramArrayList1.get(i)).booleanValue();
      int m;
      label67:
      int k;
      if ((localBackStackRecord.isPostponed()) && (!localBackStackRecord.interactsWith(paramArrayList, i + 1, paramInt2)))
      {
        m = 1;
        k = j;
        if (m != 0)
        {
          if (this.mPostponedTransactions == null) {
            this.mPostponedTransactions = new ArrayList();
          }
          StartEnterTransitionListener localStartEnterTransitionListener = new StartEnterTransitionListener(localBackStackRecord, bool);
          this.mPostponedTransactions.add(localStartEnterTransitionListener);
          localBackStackRecord.setOnStartPostponedListener(localStartEnterTransitionListener);
          if (!bool) {
            break label184;
          }
          localBackStackRecord.executeOps();
        }
      }
      for (;;)
      {
        k = j - 1;
        if (i != k)
        {
          paramArrayList.remove(i);
          paramArrayList.add(k, localBackStackRecord);
        }
        addAddedFragments(paramArraySet);
        i--;
        j = k;
        break;
        m = 0;
        break label67;
        label184:
        localBackStackRecord.executePopOps(false);
      }
    }
    return j;
  }
  
  public static int reverseTransit(int paramInt)
  {
    int i = 0;
    switch (paramInt)
    {
    default: 
      paramInt = i;
    }
    for (;;)
    {
      return paramInt;
      paramInt = 8194;
      continue;
      paramInt = 4097;
      continue;
      paramInt = 4099;
    }
  }
  
  /* Error */
  private void scheduleCommit()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield 410	android/support/v4/app/FragmentManagerImpl:mPostponedTransactions	Ljava/util/ArrayList;
    //   8: ifnull +73 -> 81
    //   11: aload_0
    //   12: getfield 410	android/support/v4/app/FragmentManagerImpl:mPostponedTransactions	Ljava/util/ArrayList;
    //   15: invokevirtual 447	java/util/ArrayList:isEmpty	()Z
    //   18: ifne +63 -> 81
    //   21: iconst_1
    //   22: istore_1
    //   23: aload_0
    //   24: getfield 451	android/support/v4/app/FragmentManagerImpl:mPendingActions	Ljava/util/ArrayList;
    //   27: ifnull +59 -> 86
    //   30: aload_0
    //   31: getfield 451	android/support/v4/app/FragmentManagerImpl:mPendingActions	Ljava/util/ArrayList;
    //   34: invokevirtual 222	java/util/ArrayList:size	()I
    //   37: iconst_1
    //   38: if_icmpne +48 -> 86
    //   41: iload_1
    //   42: ifne +7 -> 49
    //   45: iload_2
    //   46: ifeq +32 -> 78
    //   49: aload_0
    //   50: getfield 336	android/support/v4/app/FragmentManagerImpl:mHost	Landroid/support/v4/app/FragmentHostCallback;
    //   53: invokevirtual 342	android/support/v4/app/FragmentHostCallback:getHandler	()Landroid/os/Handler;
    //   56: aload_0
    //   57: getfield 161	android/support/v4/app/FragmentManagerImpl:mExecCommit	Ljava/lang/Runnable;
    //   60: invokevirtual 458	android/os/Handler:removeCallbacks	(Ljava/lang/Runnable;)V
    //   63: aload_0
    //   64: getfield 336	android/support/v4/app/FragmentManagerImpl:mHost	Landroid/support/v4/app/FragmentHostCallback;
    //   67: invokevirtual 342	android/support/v4/app/FragmentHostCallback:getHandler	()Landroid/os/Handler;
    //   70: aload_0
    //   71: getfield 161	android/support/v4/app/FragmentManagerImpl:mExecCommit	Ljava/lang/Runnable;
    //   74: invokevirtual 563	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   77: pop
    //   78: aload_0
    //   79: monitorexit
    //   80: return
    //   81: iconst_0
    //   82: istore_1
    //   83: goto -60 -> 23
    //   86: iconst_0
    //   87: istore_2
    //   88: goto -47 -> 41
    //   91: astore_3
    //   92: aload_0
    //   93: monitorexit
    //   94: aload_3
    //   95: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	FragmentManagerImpl
    //   22	61	1	i	int
    //   1	87	2	j	int
    //   91	4	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   4	21	91	finally
    //   23	41	91	finally
    //   49	78	91	finally
    //   78	80	91	finally
    //   92	94	91	finally
  }
  
  private void setHWLayerAnimListenerIfAlpha(View paramView, Animation paramAnimation)
  {
    if ((paramView == null) || (paramAnimation == null)) {}
    for (;;)
    {
      return;
      Object localObject;
      if (shouldRunOnHWLayer(paramView, paramAnimation)) {
        localObject = null;
      }
      try
      {
        if (sAnimationListenerField == null)
        {
          sAnimationListenerField = Animation.class.getDeclaredField("mListener");
          sAnimationListenerField.setAccessible(true);
        }
        Animation.AnimationListener localAnimationListener = (Animation.AnimationListener)sAnimationListenerField.get(paramAnimation);
        localObject = localAnimationListener;
      }
      catch (NoSuchFieldException localNoSuchFieldException)
      {
        for (;;)
        {
          Log.e("FragmentManager", "No field with the name mListener is found in Animation class", localNoSuchFieldException);
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        for (;;)
        {
          Log.e("FragmentManager", "Cannot access Animation's mListener field", localIllegalAccessException);
        }
      }
      ViewCompat.setLayerType(paramView, 2, null);
      paramAnimation.setAnimationListener(new AnimateOnHWLayerIfNeededListener(paramView, paramAnimation, (Animation.AnimationListener)localObject));
    }
  }
  
  static boolean shouldRunOnHWLayer(View paramView, Animation paramAnimation)
  {
    if ((Build.VERSION.SDK_INT >= 19) && (ViewCompat.getLayerType(paramView) == 0) && (ViewCompat.hasOverlappingRendering(paramView)) && (modifiesAlpha(paramAnimation))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private void throwException(RuntimeException paramRuntimeException)
  {
    Log.e("FragmentManager", paramRuntimeException.getMessage());
    Log.e("FragmentManager", "Activity state:");
    PrintWriter localPrintWriter = new PrintWriter(new LogWriter("FragmentManager"));
    if (this.mHost != null) {}
    for (;;)
    {
      try
      {
        this.mHost.onDump("  ", null, localPrintWriter, new String[0]);
        throw paramRuntimeException;
      }
      catch (Exception localException1)
      {
        Log.e("FragmentManager", "Failed dumping state", localException1);
        continue;
      }
      try
      {
        dump("  ", null, localException1, new String[0]);
      }
      catch (Exception localException2)
      {
        Log.e("FragmentManager", "Failed dumping state", localException2);
      }
    }
  }
  
  public static int transitToStyleIndex(int paramInt, boolean paramBoolean)
  {
    int i = -1;
    switch (paramInt)
    {
    default: 
      paramInt = i;
      return paramInt;
    case 4097: 
      if (paramBoolean) {}
      for (paramInt = 1;; paramInt = 2) {
        break;
      }
    case 8194: 
      if (paramBoolean) {}
      for (paramInt = 3;; paramInt = 4) {
        break;
      }
    }
    if (paramBoolean) {}
    for (paramInt = 5;; paramInt = 6) {
      break;
    }
  }
  
  void addBackStackState(BackStackRecord paramBackStackRecord)
  {
    if (this.mBackStack == null) {
      this.mBackStack = new ArrayList();
    }
    this.mBackStack.add(paramBackStackRecord);
    reportBackStackChanged();
  }
  
  public void addFragment(Fragment paramFragment, boolean paramBoolean)
  {
    if (this.mAdded == null) {
      this.mAdded = new ArrayList();
    }
    if (DEBUG) {
      Log.v("FragmentManager", "add: " + paramFragment);
    }
    makeActive(paramFragment);
    if (!paramFragment.mDetached)
    {
      if (this.mAdded.contains(paramFragment)) {
        throw new IllegalStateException("Fragment already added: " + paramFragment);
      }
      this.mAdded.add(paramFragment);
      paramFragment.mAdded = true;
      paramFragment.mRemoving = false;
      if (paramFragment.mView == null) {
        paramFragment.mHiddenChanged = false;
      }
      if ((paramFragment.mHasMenu) && (paramFragment.mMenuVisible)) {
        this.mNeedMenuInvalidate = true;
      }
      if (paramBoolean) {
        moveToState(paramFragment);
      }
    }
  }
  
  public void addOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener paramOnBackStackChangedListener)
  {
    if (this.mBackStackChangeListeners == null) {
      this.mBackStackChangeListeners = new ArrayList();
    }
    this.mBackStackChangeListeners.add(paramOnBackStackChangedListener);
  }
  
  /* Error */
  public int allocBackStackIndex(BackStackRecord paramBackStackRecord)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   6: ifnull +13 -> 19
    //   9: aload_0
    //   10: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   13: invokevirtual 222	java/util/ArrayList:size	()I
    //   16: ifgt +88 -> 104
    //   19: aload_0
    //   20: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   23: ifnonnull +16 -> 39
    //   26: new 183	java/util/ArrayList
    //   29: astore_3
    //   30: aload_3
    //   31: invokespecial 352	java/util/ArrayList:<init>	()V
    //   34: aload_0
    //   35: aload_3
    //   36: putfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   39: aload_0
    //   40: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   43: invokevirtual 222	java/util/ArrayList:size	()I
    //   46: istore_2
    //   47: getstatic 119	android/support/v4/app/FragmentManagerImpl:DEBUG	Z
    //   50: ifeq +41 -> 91
    //   53: new 238	java/lang/StringBuilder
    //   56: astore_3
    //   57: aload_3
    //   58: invokespecial 239	java/lang/StringBuilder:<init>	()V
    //   61: ldc 55
    //   63: aload_3
    //   64: ldc_w 715
    //   67: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   70: iload_2
    //   71: invokevirtual 718	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   74: ldc_w 720
    //   77: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: aload_1
    //   81: invokevirtual 672	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   84: invokevirtual 249	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   87: invokestatic 675	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: aload_0
    //   92: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   95: aload_1
    //   96: invokevirtual 263	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   99: pop
    //   100: aload_0
    //   101: monitorexit
    //   102: iload_2
    //   103: ireturn
    //   104: aload_0
    //   105: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   108: aload_0
    //   109: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   112: invokevirtual 222	java/util/ArrayList:size	()I
    //   115: iconst_1
    //   116: isub
    //   117: invokevirtual 434	java/util/ArrayList:remove	(I)Ljava/lang/Object;
    //   120: checkcast 722	java/lang/Integer
    //   123: invokevirtual 725	java/lang/Integer:intValue	()I
    //   126: istore_2
    //   127: getstatic 119	android/support/v4/app/FragmentManagerImpl:DEBUG	Z
    //   130: ifeq +41 -> 171
    //   133: new 238	java/lang/StringBuilder
    //   136: astore_3
    //   137: aload_3
    //   138: invokespecial 239	java/lang/StringBuilder:<init>	()V
    //   141: ldc 55
    //   143: aload_3
    //   144: ldc_w 727
    //   147: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   150: iload_2
    //   151: invokevirtual 718	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   154: ldc_w 729
    //   157: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   160: aload_1
    //   161: invokevirtual 672	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   164: invokevirtual 249	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   167: invokestatic 675	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   170: pop
    //   171: aload_0
    //   172: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   175: iload_2
    //   176: aload_1
    //   177: invokevirtual 733	java/util/ArrayList:set	(ILjava/lang/Object;)Ljava/lang/Object;
    //   180: pop
    //   181: aload_0
    //   182: monitorexit
    //   183: goto -81 -> 102
    //   186: astore_1
    //   187: aload_0
    //   188: monitorexit
    //   189: aload_1
    //   190: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	191	0	this	FragmentManagerImpl
    //   0	191	1	paramBackStackRecord	BackStackRecord
    //   46	130	2	i	int
    //   29	115	3	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	19	186	finally
    //   19	39	186	finally
    //   39	91	186	finally
    //   91	102	186	finally
    //   104	171	186	finally
    //   171	183	186	finally
    //   187	189	186	finally
  }
  
  public void attachController(FragmentHostCallback paramFragmentHostCallback, FragmentContainer paramFragmentContainer, Fragment paramFragment)
  {
    if (this.mHost != null) {
      throw new IllegalStateException("Already attached");
    }
    this.mHost = paramFragmentHostCallback;
    this.mContainer = paramFragmentContainer;
    this.mParent = paramFragment;
  }
  
  public void attachFragment(Fragment paramFragment)
  {
    if (DEBUG) {
      Log.v("FragmentManager", "attach: " + paramFragment);
    }
    if (paramFragment.mDetached)
    {
      paramFragment.mDetached = false;
      if (!paramFragment.mAdded)
      {
        if (this.mAdded == null) {
          this.mAdded = new ArrayList();
        }
        if (this.mAdded.contains(paramFragment)) {
          throw new IllegalStateException("Fragment already added: " + paramFragment);
        }
        if (DEBUG) {
          Log.v("FragmentManager", "add from attach: " + paramFragment);
        }
        this.mAdded.add(paramFragment);
        paramFragment.mAdded = true;
        if ((paramFragment.mHasMenu) && (paramFragment.mMenuVisible)) {
          this.mNeedMenuInvalidate = true;
        }
      }
    }
  }
  
  public FragmentTransaction beginTransaction()
  {
    return new BackStackRecord(this);
  }
  
  void completeShowHideFragment(Fragment paramFragment)
  {
    boolean bool;
    if (paramFragment.mView != null)
    {
      i = paramFragment.getNextTransition();
      if (paramFragment.mHidden) {
        break label150;
      }
      bool = true;
      Animation localAnimation = loadAnimation(paramFragment, i, bool, paramFragment.getNextTransitionStyle());
      if (localAnimation != null)
      {
        setHWLayerAnimListenerIfAlpha(paramFragment.mView, localAnimation);
        paramFragment.mView.startAnimation(localAnimation);
        setHWLayerAnimListenerIfAlpha(paramFragment.mView, localAnimation);
        localAnimation.start();
      }
      if ((!paramFragment.mHidden) || (paramFragment.isHideReplaced())) {
        break label155;
      }
    }
    label150:
    label155:
    for (int i = 8;; i = 0)
    {
      paramFragment.mView.setVisibility(i);
      if (paramFragment.isHideReplaced()) {
        paramFragment.setHideReplaced(false);
      }
      if ((paramFragment.mAdded) && (paramFragment.mHasMenu) && (paramFragment.mMenuVisible)) {
        this.mNeedMenuInvalidate = true;
      }
      paramFragment.mHiddenChanged = false;
      paramFragment.onHiddenChanged(paramFragment.mHidden);
      return;
      bool = false;
      break;
    }
  }
  
  public void detachFragment(Fragment paramFragment)
  {
    if (DEBUG) {
      Log.v("FragmentManager", "detach: " + paramFragment);
    }
    if (!paramFragment.mDetached)
    {
      paramFragment.mDetached = true;
      if (paramFragment.mAdded)
      {
        if (this.mAdded != null)
        {
          if (DEBUG) {
            Log.v("FragmentManager", "remove from detach: " + paramFragment);
          }
          this.mAdded.remove(paramFragment);
        }
        if ((paramFragment.mHasMenu) && (paramFragment.mMenuVisible)) {
          this.mNeedMenuInvalidate = true;
        }
        paramFragment.mAdded = false;
      }
    }
  }
  
  public void dispatchActivityCreated()
  {
    this.mStateSaved = false;
    this.mExecutingActions = true;
    moveToState(2, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchConfigurationChanged(Configuration paramConfiguration)
  {
    if (this.mAdded != null) {
      for (int i = 0; i < this.mAdded.size(); i++)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if (localFragment != null) {
          localFragment.performConfigurationChanged(paramConfiguration);
        }
      }
    }
  }
  
  public boolean dispatchContextItemSelected(MenuItem paramMenuItem)
  {
    int i;
    if (this.mAdded != null)
    {
      i = 0;
      if (i < this.mAdded.size())
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if ((localFragment == null) || (!localFragment.performContextItemSelected(paramMenuItem))) {}
      }
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i++;
      break;
    }
  }
  
  public void dispatchCreate()
  {
    this.mStateSaved = false;
    this.mExecutingActions = true;
    moveToState(1, false);
    this.mExecutingActions = false;
  }
  
  public boolean dispatchCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    boolean bool2 = false;
    boolean bool1 = false;
    Object localObject2 = null;
    Object localObject1 = null;
    int i;
    if (this.mAdded != null)
    {
      i = 0;
      for (;;)
      {
        localObject2 = localObject1;
        bool2 = bool1;
        if (i >= this.mAdded.size()) {
          break;
        }
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        localObject2 = localObject1;
        bool2 = bool1;
        if (localFragment != null)
        {
          localObject2 = localObject1;
          bool2 = bool1;
          if (localFragment.performCreateOptionsMenu(paramMenu, paramMenuInflater))
          {
            bool2 = true;
            localObject2 = localObject1;
            if (localObject1 == null) {
              localObject2 = new ArrayList();
            }
            ((ArrayList)localObject2).add(localFragment);
          }
        }
        i++;
        localObject1 = localObject2;
        bool1 = bool2;
      }
    }
    if (this.mCreatedMenus != null) {
      for (i = 0; i < this.mCreatedMenus.size(); i++)
      {
        paramMenu = (Fragment)this.mCreatedMenus.get(i);
        if ((localObject2 == null) || (!((ArrayList)localObject2).contains(paramMenu))) {
          paramMenu.onDestroyOptionsMenu();
        }
      }
    }
    this.mCreatedMenus = ((ArrayList)localObject2);
    return bool2;
  }
  
  public void dispatchDestroy()
  {
    this.mDestroyed = true;
    execPendingActions();
    this.mExecutingActions = true;
    moveToState(0, false);
    this.mExecutingActions = false;
    this.mHost = null;
    this.mContainer = null;
    this.mParent = null;
  }
  
  public void dispatchDestroyView()
  {
    this.mExecutingActions = true;
    moveToState(1, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchLowMemory()
  {
    if (this.mAdded != null) {
      for (int i = 0; i < this.mAdded.size(); i++)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if (localFragment != null) {
          localFragment.performLowMemory();
        }
      }
    }
  }
  
  public void dispatchMultiWindowModeChanged(boolean paramBoolean)
  {
    if (this.mAdded == null) {}
    for (;;)
    {
      return;
      for (int i = this.mAdded.size() - 1; i >= 0; i--)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if (localFragment != null) {
          localFragment.performMultiWindowModeChanged(paramBoolean);
        }
      }
    }
  }
  
  void dispatchOnFragmentActivityCreated(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentActivityCreated(paramFragment, paramBundle, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentActivityCreated(this, paramFragment, paramBundle);
        }
      }
    }
  }
  
  void dispatchOnFragmentAttached(Fragment paramFragment, Context paramContext, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentAttached(paramFragment, paramContext, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentAttached(this, paramFragment, paramContext);
        }
      }
    }
  }
  
  void dispatchOnFragmentCreated(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentCreated(paramFragment, paramBundle, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentCreated(this, paramFragment, paramBundle);
        }
      }
    }
  }
  
  void dispatchOnFragmentDestroyed(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentDestroyed(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentDestroyed(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentDetached(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentDetached(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentDetached(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentPaused(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentPaused(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentPaused(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentPreAttached(Fragment paramFragment, Context paramContext, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentPreAttached(paramFragment, paramContext, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentPreAttached(this, paramFragment, paramContext);
        }
      }
    }
  }
  
  void dispatchOnFragmentResumed(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentResumed(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentResumed(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentSaveInstanceState(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentSaveInstanceState(paramFragment, paramBundle, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentSaveInstanceState(this, paramFragment, paramBundle);
        }
      }
    }
  }
  
  void dispatchOnFragmentStarted(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentStarted(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentStarted(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentStopped(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentStopped(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentStopped(this, paramFragment);
        }
      }
    }
  }
  
  void dispatchOnFragmentViewCreated(Fragment paramFragment, View paramView, Bundle paramBundle, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentViewCreated(paramFragment, paramView, paramBundle, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      Iterator localIterator = this.mLifecycleCallbacks.iterator();
      while (localIterator.hasNext())
      {
        localObject = (Pair)localIterator.next();
        if ((!paramBoolean) || (((Boolean)((Pair)localObject).second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)((Pair)localObject).first).onFragmentViewCreated(this, paramFragment, paramView, paramBundle);
        }
      }
    }
  }
  
  void dispatchOnFragmentViewDestroyed(Fragment paramFragment, boolean paramBoolean)
  {
    Object localObject;
    if (this.mParent != null)
    {
      localObject = this.mParent.getFragmentManager();
      if ((localObject instanceof FragmentManagerImpl)) {
        ((FragmentManagerImpl)localObject).dispatchOnFragmentViewDestroyed(paramFragment, true);
      }
    }
    if (this.mLifecycleCallbacks == null) {}
    for (;;)
    {
      return;
      localObject = this.mLifecycleCallbacks.iterator();
      while (((Iterator)localObject).hasNext())
      {
        Pair localPair = (Pair)((Iterator)localObject).next();
        if ((!paramBoolean) || (((Boolean)localPair.second).booleanValue())) {
          ((FragmentManager.FragmentLifecycleCallbacks)localPair.first).onFragmentViewDestroyed(this, paramFragment);
        }
      }
    }
  }
  
  public boolean dispatchOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i;
    if (this.mAdded != null)
    {
      i = 0;
      if (i < this.mAdded.size())
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if ((localFragment == null) || (!localFragment.performOptionsItemSelected(paramMenuItem))) {}
      }
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i++;
      break;
    }
  }
  
  public void dispatchOptionsMenuClosed(Menu paramMenu)
  {
    if (this.mAdded != null) {
      for (int i = 0; i < this.mAdded.size(); i++)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if (localFragment != null) {
          localFragment.performOptionsMenuClosed(paramMenu);
        }
      }
    }
  }
  
  public void dispatchPause()
  {
    this.mExecutingActions = true;
    moveToState(4, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchPictureInPictureModeChanged(boolean paramBoolean)
  {
    if (this.mAdded == null) {}
    for (;;)
    {
      return;
      for (int i = this.mAdded.size() - 1; i >= 0; i--)
      {
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        if (localFragment != null) {
          localFragment.performPictureInPictureModeChanged(paramBoolean);
        }
      }
    }
  }
  
  public boolean dispatchPrepareOptionsMenu(Menu paramMenu)
  {
    boolean bool2 = false;
    boolean bool1 = false;
    if (this.mAdded != null)
    {
      int i = 0;
      for (;;)
      {
        bool2 = bool1;
        if (i >= this.mAdded.size()) {
          break;
        }
        Fragment localFragment = (Fragment)this.mAdded.get(i);
        bool2 = bool1;
        if (localFragment != null)
        {
          bool2 = bool1;
          if (localFragment.performPrepareOptionsMenu(paramMenu)) {
            bool2 = true;
          }
        }
        i++;
        bool1 = bool2;
      }
    }
    return bool2;
  }
  
  public void dispatchReallyStop()
  {
    this.mExecutingActions = true;
    moveToState(2, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchResume()
  {
    this.mStateSaved = false;
    this.mExecutingActions = true;
    moveToState(5, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchStart()
  {
    this.mStateSaved = false;
    this.mExecutingActions = true;
    moveToState(4, false);
    this.mExecutingActions = false;
  }
  
  public void dispatchStop()
  {
    this.mStateSaved = true;
    this.mExecutingActions = true;
    moveToState(3, false);
    this.mExecutingActions = false;
  }
  
  void doPendingDeferredStart()
  {
    if (this.mHavePendingDeferredStart)
    {
      boolean bool1 = false;
      int i = 0;
      while (i < this.mActive.size())
      {
        Fragment localFragment = (Fragment)this.mActive.get(i);
        boolean bool2 = bool1;
        if (localFragment != null)
        {
          bool2 = bool1;
          if (localFragment.mLoaderManager != null) {
            bool2 = bool1 | localFragment.mLoaderManager.hasRunningLoaders();
          }
        }
        i++;
        bool1 = bool2;
      }
      if (!bool1)
      {
        this.mHavePendingDeferredStart = false;
        startPendingDeferredFragments();
      }
    }
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    String str = paramString + "    ";
    int j;
    int i;
    Object localObject;
    if (this.mActive != null)
    {
      j = this.mActive.size();
      if (j > 0)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("Active Fragments in ");
        paramPrintWriter.print(Integer.toHexString(System.identityHashCode(this)));
        paramPrintWriter.println(":");
        for (i = 0; i < j; i++)
        {
          localObject = (Fragment)this.mActive.get(i);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("  #");
          paramPrintWriter.print(i);
          paramPrintWriter.print(": ");
          paramPrintWriter.println(localObject);
          if (localObject != null) {
            ((Fragment)localObject).dump(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
          }
        }
      }
    }
    if (this.mAdded != null)
    {
      j = this.mAdded.size();
      if (j > 0)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.println("Added Fragments:");
        for (i = 0; i < j; i++)
        {
          localObject = (Fragment)this.mAdded.get(i);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("  #");
          paramPrintWriter.print(i);
          paramPrintWriter.print(": ");
          paramPrintWriter.println(((Fragment)localObject).toString());
        }
      }
    }
    if (this.mCreatedMenus != null)
    {
      j = this.mCreatedMenus.size();
      if (j > 0)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.println("Fragments Created Menus:");
        for (i = 0; i < j; i++)
        {
          localObject = (Fragment)this.mCreatedMenus.get(i);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("  #");
          paramPrintWriter.print(i);
          paramPrintWriter.print(": ");
          paramPrintWriter.println(((Fragment)localObject).toString());
        }
      }
    }
    if (this.mBackStack != null)
    {
      j = this.mBackStack.size();
      if (j > 0)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.println("Back Stack:");
        for (i = 0; i < j; i++)
        {
          localObject = (BackStackRecord)this.mBackStack.get(i);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("  #");
          paramPrintWriter.print(i);
          paramPrintWriter.print(": ");
          paramPrintWriter.println(((BackStackRecord)localObject).toString());
          ((BackStackRecord)localObject).dump(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
      }
    }
    try
    {
      if (this.mBackStackIndices != null)
      {
        j = this.mBackStackIndices.size();
        if (j > 0)
        {
          paramPrintWriter.print(paramString);
          paramPrintWriter.println("Back Stack Indices:");
          for (i = 0; i < j; i++)
          {
            paramFileDescriptor = (BackStackRecord)this.mBackStackIndices.get(i);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("  #");
            paramPrintWriter.print(i);
            paramPrintWriter.print(": ");
            paramPrintWriter.println(paramFileDescriptor);
          }
        }
      }
      if ((this.mAvailBackStackIndices != null) && (this.mAvailBackStackIndices.size() > 0))
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mAvailBackStackIndices: ");
        paramPrintWriter.println(Arrays.toString(this.mAvailBackStackIndices.toArray()));
      }
      if (this.mPendingActions != null)
      {
        j = this.mPendingActions.size();
        if (j > 0)
        {
          paramPrintWriter.print(paramString);
          paramPrintWriter.println("Pending Actions:");
          for (i = 0; i < j; i++)
          {
            paramFileDescriptor = (OpGenerator)this.mPendingActions.get(i);
            paramPrintWriter.print(paramString);
            paramPrintWriter.print("  #");
            paramPrintWriter.print(i);
            paramPrintWriter.print(": ");
            paramPrintWriter.println(paramFileDescriptor);
          }
        }
      }
      paramPrintWriter.print(paramString);
    }
    finally {}
    paramPrintWriter.println("FragmentManager misc state:");
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("  mHost=");
    paramPrintWriter.println(this.mHost);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("  mContainer=");
    paramPrintWriter.println(this.mContainer);
    if (this.mParent != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("  mParent=");
      paramPrintWriter.println(this.mParent);
    }
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("  mCurState=");
    paramPrintWriter.print(this.mCurState);
    paramPrintWriter.print(" mStateSaved=");
    paramPrintWriter.print(this.mStateSaved);
    paramPrintWriter.print(" mDestroyed=");
    paramPrintWriter.println(this.mDestroyed);
    if (this.mNeedMenuInvalidate)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("  mNeedMenuInvalidate=");
      paramPrintWriter.println(this.mNeedMenuInvalidate);
    }
    if (this.mNoTransactionsBecause != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("  mNoTransactionsBecause=");
      paramPrintWriter.println(this.mNoTransactionsBecause);
    }
    if ((this.mAvailIndices != null) && (this.mAvailIndices.size() > 0))
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("  mAvailIndices: ");
      paramPrintWriter.println(Arrays.toString(this.mAvailIndices.toArray()));
    }
  }
  
  public void enqueueAction(OpGenerator paramOpGenerator, boolean paramBoolean)
  {
    if (!paramBoolean) {
      checkStateLoss();
    }
    try
    {
      if ((this.mDestroyed) || (this.mHost == null))
      {
        paramOpGenerator = new java/lang/IllegalStateException;
        paramOpGenerator.<init>("Activity has been destroyed");
        throw paramOpGenerator;
      }
    }
    finally {}
    if (this.mPendingActions == null)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      this.mPendingActions = localArrayList;
    }
    this.mPendingActions.add(paramOpGenerator);
    scheduleCommit();
  }
  
  /* Error */
  public boolean execPendingActions()
  {
    // Byte code:
    //   0: aload_0
    //   1: iconst_1
    //   2: invokespecial 532	android/support/v4/app/FragmentManagerImpl:ensureExecReady	(Z)V
    //   5: iconst_0
    //   6: istore_1
    //   7: aload_0
    //   8: aload_0
    //   9: getfield 259	android/support/v4/app/FragmentManagerImpl:mTmpRecords	Ljava/util/ArrayList;
    //   12: aload_0
    //   13: getfield 254	android/support/v4/app/FragmentManagerImpl:mTmpIsPop	Ljava/util/ArrayList;
    //   16: invokespecial 1056	android/support/v4/app/FragmentManagerImpl:generateOpsForPendingActions	(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    //   19: ifeq +36 -> 55
    //   22: aload_0
    //   23: iconst_1
    //   24: putfield 252	android/support/v4/app/FragmentManagerImpl:mExecutingActions	Z
    //   27: aload_0
    //   28: aload_0
    //   29: getfield 259	android/support/v4/app/FragmentManagerImpl:mTmpRecords	Ljava/util/ArrayList;
    //   32: aload_0
    //   33: getfield 254	android/support/v4/app/FragmentManagerImpl:mTmpIsPop	Ljava/util/ArrayList;
    //   36: invokespecial 538	android/support/v4/app/FragmentManagerImpl:optimizeAndExecuteOps	(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    //   39: aload_0
    //   40: invokespecial 540	android/support/v4/app/FragmentManagerImpl:cleanupExec	()V
    //   43: iconst_1
    //   44: istore_1
    //   45: goto -38 -> 7
    //   48: astore_2
    //   49: aload_0
    //   50: invokespecial 540	android/support/v4/app/FragmentManagerImpl:cleanupExec	()V
    //   53: aload_2
    //   54: athrow
    //   55: aload_0
    //   56: invokevirtual 543	android/support/v4/app/FragmentManagerImpl:doPendingDeferredStart	()V
    //   59: iload_1
    //   60: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	61	0	this	FragmentManagerImpl
    //   6	54	1	bool	boolean
    //   48	6	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   27	39	48	finally
  }
  
  public void execSingleAction(OpGenerator paramOpGenerator, boolean paramBoolean)
  {
    ensureExecReady(paramBoolean);
    if (paramOpGenerator.generateOps(this.mTmpRecords, this.mTmpIsPop)) {
      this.mExecutingActions = true;
    }
    try
    {
      optimizeAndExecuteOps(this.mTmpRecords, this.mTmpIsPop);
      cleanupExec();
      doPendingDeferredStart();
      return;
    }
    finally
    {
      cleanupExec();
    }
  }
  
  public boolean executePendingTransactions()
  {
    boolean bool = execPendingActions();
    forcePostponedTransactions();
    return bool;
  }
  
  public Fragment findFragmentById(int paramInt)
  {
    int i;
    Object localObject;
    if (this.mAdded != null)
    {
      i = this.mAdded.size() - 1;
      if (i >= 0)
      {
        localObject = (Fragment)this.mAdded.get(i);
        if ((localObject == null) || (((Fragment)localObject).mFragmentId != paramInt)) {}
      }
    }
    for (;;)
    {
      return (Fragment)localObject;
      i--;
      break;
      if (this.mActive != null) {
        for (i = this.mActive.size() - 1;; i--)
        {
          if (i < 0) {
            break label110;
          }
          Fragment localFragment = (Fragment)this.mActive.get(i);
          if (localFragment != null)
          {
            localObject = localFragment;
            if (localFragment.mFragmentId == paramInt) {
              break;
            }
          }
        }
      }
      label110:
      localObject = null;
    }
  }
  
  public Fragment findFragmentByTag(String paramString)
  {
    int i;
    Object localObject;
    if ((this.mAdded != null) && (paramString != null))
    {
      i = this.mAdded.size() - 1;
      if (i >= 0)
      {
        localObject = (Fragment)this.mAdded.get(i);
        if ((localObject == null) || (!paramString.equals(((Fragment)localObject).mTag))) {}
      }
    }
    for (;;)
    {
      return (Fragment)localObject;
      i--;
      break;
      if ((this.mActive != null) && (paramString != null)) {
        for (i = this.mActive.size() - 1;; i--)
        {
          if (i < 0) {
            break label124;
          }
          Fragment localFragment = (Fragment)this.mActive.get(i);
          if (localFragment != null)
          {
            localObject = localFragment;
            if (paramString.equals(localFragment.mTag)) {
              break;
            }
          }
        }
      }
      label124:
      localObject = null;
    }
  }
  
  public Fragment findFragmentByWho(String paramString)
  {
    int i;
    Fragment localFragment;
    if ((this.mActive != null) && (paramString != null))
    {
      i = this.mActive.size() - 1;
      if (i >= 0)
      {
        localFragment = (Fragment)this.mActive.get(i);
        if (localFragment != null)
        {
          localFragment = localFragment.findFragmentByWho(paramString);
          if (localFragment == null) {}
        }
      }
    }
    for (paramString = localFragment;; paramString = null)
    {
      return paramString;
      i--;
      break;
    }
  }
  
  public void freeBackStackIndex(int paramInt)
  {
    try
    {
      this.mBackStackIndices.set(paramInt, null);
      Object localObject1;
      if (this.mAvailBackStackIndices == null)
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        this.mAvailBackStackIndices = ((ArrayList)localObject1);
      }
      if (DEBUG)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        Log.v("FragmentManager", "Freeing back stack index " + paramInt);
      }
      this.mAvailBackStackIndices.add(Integer.valueOf(paramInt));
      return;
    }
    finally {}
  }
  
  public FragmentManager.BackStackEntry getBackStackEntryAt(int paramInt)
  {
    return (FragmentManager.BackStackEntry)this.mBackStack.get(paramInt);
  }
  
  public int getBackStackEntryCount()
  {
    if (this.mBackStack != null) {}
    for (int i = this.mBackStack.size();; i = 0) {
      return i;
    }
  }
  
  public Fragment getFragment(Bundle paramBundle, String paramString)
  {
    int i = paramBundle.getInt(paramString, -1);
    if (i == -1) {
      paramBundle = null;
    }
    for (;;)
    {
      return paramBundle;
      if (i >= this.mActive.size()) {
        throwException(new IllegalStateException("Fragment no longer exists for key " + paramString + ": index " + i));
      }
      Fragment localFragment = (Fragment)this.mActive.get(i);
      paramBundle = localFragment;
      if (localFragment == null)
      {
        throwException(new IllegalStateException("Fragment no longer exists for key " + paramString + ": index " + i));
        paramBundle = localFragment;
      }
    }
  }
  
  public List<Fragment> getFragments()
  {
    return this.mActive;
  }
  
  LayoutInflaterFactory getLayoutInflaterFactory()
  {
    return this;
  }
  
  public void hideFragment(Fragment paramFragment)
  {
    boolean bool = true;
    if (DEBUG) {
      Log.v("FragmentManager", "hide: " + paramFragment);
    }
    if (!paramFragment.mHidden)
    {
      paramFragment.mHidden = true;
      if (paramFragment.mHiddenChanged) {
        break label59;
      }
    }
    for (;;)
    {
      paramFragment.mHiddenChanged = bool;
      return;
      label59:
      bool = false;
    }
  }
  
  public boolean isDestroyed()
  {
    return this.mDestroyed;
  }
  
  boolean isStateAtLeast(int paramInt)
  {
    if (this.mCurState >= paramInt) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  Animation loadAnimation(Fragment paramFragment, int paramInt1, boolean paramBoolean, int paramInt2)
  {
    Animation localAnimation = paramFragment.onCreateAnimation(paramInt1, paramBoolean, paramFragment.getNextAnim());
    if (localAnimation != null) {
      paramFragment = localAnimation;
    }
    for (;;)
    {
      return paramFragment;
      if (paramFragment.getNextAnim() != 0)
      {
        paramFragment = AnimationUtils.loadAnimation(this.mHost.getContext(), paramFragment.getNextAnim());
        if (paramFragment != null) {}
      }
      else if (paramInt1 == 0)
      {
        paramFragment = null;
      }
      else
      {
        paramInt1 = transitToStyleIndex(paramInt1, paramBoolean);
        if (paramInt1 < 0) {
          paramFragment = null;
        } else {
          switch (paramInt1)
          {
          default: 
            paramInt1 = paramInt2;
            if (paramInt2 == 0)
            {
              paramInt1 = paramInt2;
              if (this.mHost.onHasWindowAnimations()) {
                paramInt1 = this.mHost.onGetWindowAnimations();
              }
            }
            if (paramInt1 == 0) {
              paramFragment = null;
            }
            break;
          case 1: 
            paramFragment = makeOpenCloseAnimation(this.mHost.getContext(), 1.125F, 1.0F, 0.0F, 1.0F);
            break;
          case 2: 
            paramFragment = makeOpenCloseAnimation(this.mHost.getContext(), 1.0F, 0.975F, 1.0F, 0.0F);
            break;
          case 3: 
            paramFragment = makeOpenCloseAnimation(this.mHost.getContext(), 0.975F, 1.0F, 0.0F, 1.0F);
            break;
          case 4: 
            paramFragment = makeOpenCloseAnimation(this.mHost.getContext(), 1.0F, 1.075F, 1.0F, 0.0F);
            break;
          case 5: 
            paramFragment = makeFadeAnimation(this.mHost.getContext(), 0.0F, 1.0F);
            break;
          case 6: 
            paramFragment = makeFadeAnimation(this.mHost.getContext(), 1.0F, 0.0F);
            continue;
            paramFragment = null;
          }
        }
      }
    }
  }
  
  void makeActive(Fragment paramFragment)
  {
    if (paramFragment.mIndex >= 0) {}
    label146:
    for (;;)
    {
      return;
      if ((this.mAvailIndices == null) || (this.mAvailIndices.size() <= 0))
      {
        if (this.mActive == null) {
          this.mActive = new ArrayList();
        }
        paramFragment.setIndex(this.mActive.size(), this.mParent);
        this.mActive.add(paramFragment);
      }
      for (;;)
      {
        if (!DEBUG) {
          break label146;
        }
        Log.v("FragmentManager", "Allocated fragment index " + paramFragment);
        break;
        paramFragment.setIndex(((Integer)this.mAvailIndices.remove(this.mAvailIndices.size() - 1)).intValue(), this.mParent);
        this.mActive.set(paramFragment.mIndex, paramFragment);
      }
    }
  }
  
  void makeInactive(Fragment paramFragment)
  {
    if (paramFragment.mIndex < 0) {}
    for (;;)
    {
      return;
      if (DEBUG) {
        Log.v("FragmentManager", "Freeing fragment index " + paramFragment);
      }
      this.mActive.set(paramFragment.mIndex, null);
      if (this.mAvailIndices == null) {
        this.mAvailIndices = new ArrayList();
      }
      this.mAvailIndices.add(Integer.valueOf(paramFragment.mIndex));
      this.mHost.inactivateFragment(paramFragment.mWho);
      paramFragment.initState();
    }
  }
  
  void moveFragmentToExpectedState(Fragment paramFragment)
  {
    if (paramFragment == null) {
      return;
    }
    int j = this.mCurState;
    int i = j;
    label32:
    Object localObject;
    if (paramFragment.mRemoving)
    {
      if (paramFragment.isInBackStack()) {
        i = Math.min(j, 1);
      }
    }
    else
    {
      moveToState(paramFragment, i, paramFragment.getNextTransition(), paramFragment.getNextTransitionStyle(), false);
      if (paramFragment.mView != null)
      {
        localObject = findFragmentUnder(paramFragment);
        if (localObject != null)
        {
          View localView = ((Fragment)localObject).mView;
          localObject = paramFragment.mContainer;
          i = ((ViewGroup)localObject).indexOfChild(localView);
          j = ((ViewGroup)localObject).indexOfChild(paramFragment.mView);
          if (j < i)
          {
            ((ViewGroup)localObject).removeViewAt(j);
            ((ViewGroup)localObject).addView(paramFragment.mView, i);
          }
        }
        if ((paramFragment.mIsNewlyAdded) && (paramFragment.mContainer != null))
        {
          if (Build.VERSION.SDK_INT >= 11) {
            break label222;
          }
          paramFragment.mView.setVisibility(0);
        }
      }
    }
    for (;;)
    {
      paramFragment.mPostponedAlpha = 0.0F;
      paramFragment.mIsNewlyAdded = false;
      localObject = loadAnimation(paramFragment, paramFragment.getNextTransition(), true, paramFragment.getNextTransitionStyle());
      if (localObject != null)
      {
        setHWLayerAnimListenerIfAlpha(paramFragment.mView, (Animation)localObject);
        paramFragment.mView.startAnimation((Animation)localObject);
      }
      if (!paramFragment.mHiddenChanged) {
        break;
      }
      completeShowHideFragment(paramFragment);
      break;
      i = Math.min(j, 0);
      break label32;
      label222:
      if (paramFragment.mPostponedAlpha > 0.0F) {
        paramFragment.mView.setAlpha(paramFragment.mPostponedAlpha);
      }
    }
  }
  
  void moveToState(int paramInt, boolean paramBoolean)
  {
    if ((this.mHost == null) && (paramInt != 0)) {
      throw new IllegalStateException("No activity");
    }
    if ((!paramBoolean) && (paramInt == this.mCurState)) {}
    for (;;)
    {
      return;
      this.mCurState = paramInt;
      if (this.mActive != null)
      {
        paramInt = 0;
        int i = 0;
        Fragment localFragment;
        if (this.mAdded != null)
        {
          k = this.mAdded.size();
          j = 0;
          for (;;)
          {
            paramInt = i;
            if (j >= k) {
              break;
            }
            localFragment = (Fragment)this.mAdded.get(j);
            moveFragmentToExpectedState(localFragment);
            paramInt = i;
            if (localFragment.mLoaderManager != null) {
              paramInt = i | localFragment.mLoaderManager.hasRunningLoaders();
            }
            j++;
            i = paramInt;
          }
        }
        int k = this.mActive.size();
        int j = 0;
        for (i = paramInt; j < k; i = paramInt)
        {
          localFragment = (Fragment)this.mActive.get(j);
          paramInt = i;
          if (localFragment != null) {
            if (!localFragment.mRemoving)
            {
              paramInt = i;
              if (!localFragment.mDetached) {}
            }
            else
            {
              paramInt = i;
              if (!localFragment.mIsNewlyAdded)
              {
                moveFragmentToExpectedState(localFragment);
                paramInt = i;
                if (localFragment.mLoaderManager != null) {
                  paramInt = i | localFragment.mLoaderManager.hasRunningLoaders();
                }
              }
            }
          }
          j++;
        }
        if (i == 0) {
          startPendingDeferredFragments();
        }
        if ((this.mNeedMenuInvalidate) && (this.mHost != null) && (this.mCurState == 5))
        {
          this.mHost.onSupportInvalidateOptionsMenu();
          this.mNeedMenuInvalidate = false;
        }
      }
    }
  }
  
  void moveToState(Fragment paramFragment)
  {
    moveToState(paramFragment, this.mCurState, 0, 0, false);
  }
  
  void moveToState(final Fragment paramFragment, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    int j;
    if (paramFragment.mAdded)
    {
      j = paramInt1;
      if (!paramFragment.mDetached) {}
    }
    else
    {
      j = paramInt1;
      if (paramInt1 > 1) {
        j = 1;
      }
    }
    int i = j;
    if (paramFragment.mRemoving)
    {
      i = j;
      if (j > paramFragment.mState) {
        i = paramFragment.mState;
      }
    }
    paramInt1 = i;
    if (paramFragment.mDeferStart)
    {
      paramInt1 = i;
      if (paramFragment.mState < 4)
      {
        paramInt1 = i;
        if (i > 3) {
          paramInt1 = 3;
        }
      }
    }
    if (paramFragment.mState < paramInt1)
    {
      if ((paramFragment.mFromLayout) && (!paramFragment.mInLayout)) {
        return;
      }
      if (paramFragment.getAnimatingAway() != null)
      {
        paramFragment.setAnimatingAway(null);
        moveToState(paramFragment, paramFragment.getStateAfterAnimating(), 0, 0, true);
      }
      paramInt2 = paramInt1;
      i = paramInt1;
      j = paramInt1;
      paramInt3 = paramInt1;
      switch (paramFragment.mState)
      {
      default: 
        i = paramInt1;
      }
    }
    while (paramFragment.mState != i)
    {
      Log.w("FragmentManager", "moveToState: Fragment state for " + paramFragment + " not updated inline; " + "expected state " + i + " found " + paramFragment.mState);
      paramFragment.mState = i;
      break;
      if (DEBUG) {
        Log.v("FragmentManager", "moveto CREATED: " + paramFragment);
      }
      paramInt3 = paramInt1;
      if (paramFragment.mSavedFragmentState != null)
      {
        paramFragment.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
        paramFragment.mSavedViewState = paramFragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
        paramFragment.mTarget = getFragment(paramFragment.mSavedFragmentState, "android:target_state");
        if (paramFragment.mTarget != null) {
          paramFragment.mTargetRequestCode = paramFragment.mSavedFragmentState.getInt("android:target_req_state", 0);
        }
        paramFragment.mUserVisibleHint = paramFragment.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
        paramInt3 = paramInt1;
        if (!paramFragment.mUserVisibleHint)
        {
          paramFragment.mDeferStart = true;
          paramInt3 = paramInt1;
          if (paramInt1 > 3) {
            paramInt3 = 3;
          }
        }
      }
      paramFragment.mHost = this.mHost;
      paramFragment.mParentFragment = this.mParent;
      if (this.mParent != null) {}
      for (Object localObject1 = this.mParent.mChildFragmentManager;; localObject1 = this.mHost.getFragmentManagerImpl())
      {
        paramFragment.mFragmentManager = ((FragmentManagerImpl)localObject1);
        dispatchOnFragmentPreAttached(paramFragment, this.mHost.getContext(), false);
        paramFragment.mCalled = false;
        paramFragment.onAttach(this.mHost.getContext());
        if (paramFragment.mCalled) {
          break;
        }
        throw new SuperNotCalledException("Fragment " + paramFragment + " did not call through to super.onAttach()");
      }
      label547:
      label585:
      label652:
      ViewGroup localViewGroup;
      if (paramFragment.mParentFragment == null)
      {
        this.mHost.onAttachFragment(paramFragment);
        dispatchOnFragmentAttached(paramFragment, this.mHost.getContext(), false);
        if (paramFragment.mRetaining) {
          break label1247;
        }
        paramFragment.performCreate(paramFragment.mSavedFragmentState);
        dispatchOnFragmentCreated(paramFragment, paramFragment.mSavedFragmentState, false);
        paramFragment.mRetaining = false;
        paramInt2 = paramInt3;
        if (paramFragment.mFromLayout)
        {
          paramFragment.mView = paramFragment.performCreateView(paramFragment.getLayoutInflater(paramFragment.mSavedFragmentState), null, paramFragment.mSavedFragmentState);
          if (paramFragment.mView == null) {
            break label1277;
          }
          paramFragment.mInnerView = paramFragment.mView;
          if (Build.VERSION.SDK_INT < 11) {
            break label1263;
          }
          ViewCompat.setSaveFromParentEnabled(paramFragment.mView, false);
          if (paramFragment.mHidden) {
            paramFragment.mView.setVisibility(8);
          }
          paramFragment.onViewCreated(paramFragment.mView, paramFragment.mSavedFragmentState);
          dispatchOnFragmentViewCreated(paramFragment, paramFragment.mView, paramFragment.mSavedFragmentState, false);
          paramInt2 = paramInt3;
        }
        i = paramInt2;
        if (paramInt2 > 1)
        {
          if (DEBUG) {
            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + paramFragment);
          }
          if (!paramFragment.mFromLayout)
          {
            localObject1 = null;
            if (paramFragment.mContainerId != 0)
            {
              if (paramFragment.mContainerId == -1) {
                throwException(new IllegalArgumentException("Cannot create fragment " + paramFragment + " for a container view with no id"));
              }
              localViewGroup = (ViewGroup)this.mContainer.onFindViewById(paramFragment.mContainerId);
              localObject1 = localViewGroup;
              if (localViewGroup == null)
              {
                localObject1 = localViewGroup;
                if (paramFragment.mRestored) {}
              }
            }
          }
        }
      }
      label1247:
      label1263:
      label1277:
      Object localObject2;
      try
      {
        localObject1 = paramFragment.getResources().getResourceName(paramFragment.mContainerId);
        throwException(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(paramFragment.mContainerId) + " (" + (String)localObject1 + ") for fragment " + paramFragment));
        localObject1 = localViewGroup;
        paramFragment.mContainer = ((ViewGroup)localObject1);
        paramFragment.mView = paramFragment.performCreateView(paramFragment.getLayoutInflater(paramFragment.mSavedFragmentState), (ViewGroup)localObject1, paramFragment.mSavedFragmentState);
        if (paramFragment.mView != null)
        {
          paramFragment.mInnerView = paramFragment.mView;
          if (Build.VERSION.SDK_INT >= 11)
          {
            ViewCompat.setSaveFromParentEnabled(paramFragment.mView, false);
            if (localObject1 != null) {
              ((ViewGroup)localObject1).addView(paramFragment.mView);
            }
            if (paramFragment.mHidden) {
              paramFragment.mView.setVisibility(8);
            }
            paramFragment.onViewCreated(paramFragment.mView, paramFragment.mSavedFragmentState);
            dispatchOnFragmentViewCreated(paramFragment, paramFragment.mView, paramFragment.mSavedFragmentState, false);
            if ((paramFragment.mView.getVisibility() != 0) || (paramFragment.mContainer == null)) {
              break label1312;
            }
            paramBoolean = true;
            paramFragment.mIsNewlyAdded = paramBoolean;
            paramFragment.performActivityCreated(paramFragment.mSavedFragmentState);
            dispatchOnFragmentActivityCreated(paramFragment, paramFragment.mSavedFragmentState, false);
            if (paramFragment.mView != null) {
              paramFragment.restoreViewState(paramFragment.mSavedFragmentState);
            }
            paramFragment.mSavedFragmentState = null;
            i = paramInt2;
            j = i;
            if (i > 2)
            {
              paramFragment.mState = 3;
              j = i;
            }
            paramInt3 = j;
            if (j > 3)
            {
              if (DEBUG) {
                Log.v("FragmentManager", "moveto STARTED: " + paramFragment);
              }
              paramFragment.performStart();
              dispatchOnFragmentStarted(paramFragment, false);
              paramInt3 = j;
            }
            i = paramInt3;
            if (paramInt3 <= 4) {
              continue;
            }
            if (DEBUG) {
              Log.v("FragmentManager", "moveto RESUMED: " + paramFragment);
            }
            paramFragment.performResume();
            dispatchOnFragmentResumed(paramFragment, false);
            paramFragment.mSavedFragmentState = null;
            paramFragment.mSavedViewState = null;
            i = paramInt3;
            continue;
            paramFragment.mParentFragment.onAttachFragment(paramFragment);
            break label547;
            paramFragment.restoreChildFragmentState(paramFragment.mSavedFragmentState);
            paramFragment.mState = 1;
            break label585;
            paramFragment.mView = NoSaveStateFrameLayout.wrap(paramFragment.mView);
            break label652;
            paramFragment.mInnerView = null;
            paramInt2 = paramInt3;
          }
        }
      }
      catch (Resources.NotFoundException localNotFoundException)
      {
        for (;;)
        {
          localObject2 = "unknown";
          continue;
          paramFragment.mView = NoSaveStateFrameLayout.wrap(paramFragment.mView);
          continue;
          label1312:
          paramBoolean = false;
          continue;
          paramFragment.mInnerView = null;
        }
      }
      i = paramInt1;
      if (paramFragment.mState > paramInt1) {
        switch (paramFragment.mState)
        {
        default: 
          i = paramInt1;
          break;
        case 1: 
        case 5: 
        case 4: 
        case 3: 
        case 2: 
          for (;;)
          {
            i = paramInt1;
            if (paramInt1 >= 1) {
              break;
            }
            if ((this.mDestroyed) && (paramFragment.getAnimatingAway() != null))
            {
              localObject2 = paramFragment.getAnimatingAway();
              paramFragment.setAnimatingAway(null);
              ((View)localObject2).clearAnimation();
            }
            if (paramFragment.getAnimatingAway() == null) {
              break label1808;
            }
            paramFragment.setStateAfterAnimating(paramInt1);
            i = 1;
            break;
            if (paramInt1 < 5)
            {
              if (DEBUG) {
                Log.v("FragmentManager", "movefrom RESUMED: " + paramFragment);
              }
              paramFragment.performPause();
              dispatchOnFragmentPaused(paramFragment, false);
            }
            if (paramInt1 < 4)
            {
              if (DEBUG) {
                Log.v("FragmentManager", "movefrom STARTED: " + paramFragment);
              }
              paramFragment.performStop();
              dispatchOnFragmentStopped(paramFragment, false);
            }
            if (paramInt1 < 3)
            {
              if (DEBUG) {
                Log.v("FragmentManager", "movefrom STOPPED: " + paramFragment);
              }
              paramFragment.performReallyStop();
            }
            if (paramInt1 < 2)
            {
              if (DEBUG) {
                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + paramFragment);
              }
              if ((paramFragment.mView != null) && (this.mHost.onShouldSaveFragmentState(paramFragment)) && (paramFragment.mSavedViewState == null)) {
                saveFragmentViewState(paramFragment);
              }
              paramFragment.performDestroyView();
              dispatchOnFragmentViewDestroyed(paramFragment, false);
              if ((paramFragment.mView != null) && (paramFragment.mContainer != null))
              {
                localViewGroup = null;
                localObject2 = localViewGroup;
                if (this.mCurState > 0)
                {
                  localObject2 = localViewGroup;
                  if (!this.mDestroyed)
                  {
                    localObject2 = localViewGroup;
                    if (paramFragment.mView.getVisibility() == 0)
                    {
                      localObject2 = localViewGroup;
                      if (paramFragment.mPostponedAlpha >= 0.0F) {
                        localObject2 = loadAnimation(paramFragment, paramInt2, false, paramInt3);
                      }
                    }
                  }
                }
                paramFragment.mPostponedAlpha = 0.0F;
                if (localObject2 != null)
                {
                  paramFragment.setAnimatingAway(paramFragment.mView);
                  paramFragment.setStateAfterAnimating(paramInt1);
                  ((Animation)localObject2).setAnimationListener(new AnimateOnHWLayerIfNeededListener(paramFragment.mView, (Animation)localObject2)
                  {
                    public void onAnimationEnd(Animation paramAnonymousAnimation)
                    {
                      super.onAnimationEnd(paramAnonymousAnimation);
                      if (paramFragment.getAnimatingAway() != null)
                      {
                        paramFragment.setAnimatingAway(null);
                        FragmentManagerImpl.this.moveToState(paramFragment, paramFragment.getStateAfterAnimating(), 0, 0, false);
                      }
                    }
                  });
                  paramFragment.mView.startAnimation((Animation)localObject2);
                }
                paramFragment.mContainer.removeView(paramFragment.mView);
              }
              paramFragment.mContainer = null;
              paramFragment.mView = null;
              paramFragment.mInnerView = null;
            }
          }
          label1808:
          if (DEBUG) {
            Log.v("FragmentManager", "movefrom CREATED: " + paramFragment);
          }
          if (!paramFragment.mRetaining)
          {
            paramFragment.performDestroy();
            dispatchOnFragmentDestroyed(paramFragment, false);
          }
          for (;;)
          {
            paramFragment.performDetach();
            dispatchOnFragmentDetached(paramFragment, false);
            i = paramInt1;
            if (paramBoolean) {
              break;
            }
            if (paramFragment.mRetaining) {
              break label1901;
            }
            makeInactive(paramFragment);
            i = paramInt1;
            break;
            paramFragment.mState = 0;
          }
          label1901:
          paramFragment.mHost = null;
          paramFragment.mParentFragment = null;
          paramFragment.mFragmentManager = null;
          i = paramInt1;
        }
      }
    }
  }
  
  public void noteStateNotSaved()
  {
    this.mStateSaved = false;
  }
  
  public View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    Object localObject = null;
    if (!"fragment".equals(paramString)) {
      paramString = (String)localObject;
    }
    for (;;)
    {
      return paramString;
      paramString = paramAttributeSet.getAttributeValue(null, "class");
      TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, FragmentTag.Fragment);
      String str1 = paramString;
      if (paramString == null) {
        str1 = localTypedArray.getString(0);
      }
      int k = localTypedArray.getResourceId(1, -1);
      String str2 = localTypedArray.getString(2);
      localTypedArray.recycle();
      paramString = (String)localObject;
      if (Fragment.isSupportFragmentClass(this.mHost.getContext(), str1))
      {
        if (paramView != null) {}
        for (int i = paramView.getId(); (i == -1) && (k == -1) && (str2 == null); i = 0) {
          throw new IllegalArgumentException(paramAttributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + str1);
        }
        int j;
        if (k != -1)
        {
          paramView = findFragmentById(k);
          paramString = paramView;
          if (paramView == null)
          {
            paramString = paramView;
            if (str2 != null) {
              paramString = findFragmentByTag(str2);
            }
          }
          paramView = paramString;
          if (paramString == null)
          {
            paramView = paramString;
            if (i != -1) {
              paramView = findFragmentById(i);
            }
          }
          if (DEBUG) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(k) + " fname=" + str1 + " existing=" + paramView);
          }
          if (paramView != null) {
            break label441;
          }
          paramString = Fragment.instantiate(paramContext, str1);
          paramString.mFromLayout = true;
          if (k == 0) {
            break label434;
          }
          j = k;
          label304:
          paramString.mFragmentId = j;
          paramString.mContainerId = i;
          paramString.mTag = str2;
          paramString.mInLayout = true;
          paramString.mFragmentManager = this;
          paramString.mHost = this.mHost;
          paramString.onInflate(this.mHost.getContext(), paramAttributeSet, paramString.mSavedFragmentState);
          addFragment(paramString, true);
          label363:
          if ((this.mCurState >= 1) || (!paramString.mFromLayout)) {
            break label570;
          }
          moveToState(paramString, 1, 0, 0, false);
        }
        for (;;)
        {
          if (paramString.mView != null) {
            break label578;
          }
          throw new IllegalStateException("Fragment " + str1 + " did not create a view.");
          paramView = null;
          break;
          label434:
          j = i;
          break label304;
          label441:
          if (paramView.mInLayout) {
            throw new IllegalArgumentException(paramAttributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(k) + ", tag " + str2 + ", or parent id 0x" + Integer.toHexString(i) + " with another fragment for " + str1);
          }
          paramView.mInLayout = true;
          paramView.mHost = this.mHost;
          paramString = paramView;
          if (paramView.mRetaining) {
            break label363;
          }
          paramView.onInflate(this.mHost.getContext(), paramAttributeSet, paramView.mSavedFragmentState);
          paramString = paramView;
          break label363;
          label570:
          moveToState(paramString);
        }
        label578:
        if (k != 0) {
          paramString.mView.setId(k);
        }
        if (paramString.mView.getTag() == null) {
          paramString.mView.setTag(str2);
        }
        paramString = paramString.mView;
      }
    }
  }
  
  public void performPendingDeferredStart(Fragment paramFragment)
  {
    if (paramFragment.mDeferStart)
    {
      if (!this.mExecutingActions) {
        break label20;
      }
      this.mHavePendingDeferredStart = true;
    }
    for (;;)
    {
      return;
      label20:
      paramFragment.mDeferStart = false;
      moveToState(paramFragment, this.mCurState, 0, 0, false);
    }
  }
  
  public void popBackStack()
  {
    enqueueAction(new PopBackStackState(null, -1, 0), false);
  }
  
  public void popBackStack(int paramInt1, int paramInt2)
  {
    if (paramInt1 < 0) {
      throw new IllegalArgumentException("Bad id: " + paramInt1);
    }
    enqueueAction(new PopBackStackState(null, paramInt1, paramInt2), false);
  }
  
  public void popBackStack(String paramString, int paramInt)
  {
    enqueueAction(new PopBackStackState(paramString, -1, paramInt), false);
  }
  
  public boolean popBackStackImmediate()
  {
    checkStateLoss();
    return popBackStackImmediate(null, -1, 0);
  }
  
  public boolean popBackStackImmediate(int paramInt1, int paramInt2)
  {
    checkStateLoss();
    execPendingActions();
    if (paramInt1 < 0) {
      throw new IllegalArgumentException("Bad id: " + paramInt1);
    }
    return popBackStackImmediate(null, paramInt1, paramInt2);
  }
  
  public boolean popBackStackImmediate(String paramString, int paramInt)
  {
    checkStateLoss();
    return popBackStackImmediate(paramString, -1, paramInt);
  }
  
  boolean popBackStackState(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, String paramString, int paramInt1, int paramInt2)
  {
    boolean bool2 = false;
    boolean bool1;
    if (this.mBackStack == null) {
      bool1 = bool2;
    }
    do
    {
      return bool1;
      if ((paramString != null) || (paramInt1 >= 0) || ((paramInt2 & 0x1) != 0)) {
        break;
      }
      paramInt1 = this.mBackStack.size() - 1;
      bool1 = bool2;
    } while (paramInt1 < 0);
    paramArrayList.add(this.mBackStack.remove(paramInt1));
    paramArrayList1.add(Boolean.valueOf(true));
    for (;;)
    {
      bool1 = true;
      break;
      int i = -1;
      if ((paramString != null) || (paramInt1 >= 0)) {
        for (int j = this.mBackStack.size() - 1;; j--)
        {
          BackStackRecord localBackStackRecord;
          if (j >= 0)
          {
            localBackStackRecord = (BackStackRecord)this.mBackStack.get(j);
            if ((paramString == null) || (!paramString.equals(localBackStackRecord.getName()))) {
              break label234;
            }
          }
          label234:
          while ((paramInt1 >= 0) && (paramInt1 == localBackStackRecord.mIndex))
          {
            bool1 = bool2;
            if (j < 0) {
              break;
            }
            i = j;
            if ((paramInt2 & 0x1) == 0) {
              break label255;
            }
            for (paramInt2 = j - 1;; paramInt2--)
            {
              i = paramInt2;
              if (paramInt2 < 0) {
                break;
              }
              localBackStackRecord = (BackStackRecord)this.mBackStack.get(paramInt2);
              if ((paramString == null) || (!paramString.equals(localBackStackRecord.getName())))
              {
                i = paramInt2;
                if (paramInt1 < 0) {
                  break;
                }
                i = paramInt2;
                if (paramInt1 != localBackStackRecord.mIndex) {
                  break;
                }
              }
            }
          }
        }
      }
      label255:
      bool1 = bool2;
      if (i == this.mBackStack.size() - 1) {
        break;
      }
      for (paramInt1 = this.mBackStack.size() - 1; paramInt1 > i; paramInt1--)
      {
        paramArrayList.add(this.mBackStack.remove(paramInt1));
        paramArrayList1.add(Boolean.valueOf(true));
      }
    }
  }
  
  public void putFragment(Bundle paramBundle, String paramString, Fragment paramFragment)
  {
    if (paramFragment.mIndex < 0) {
      throwException(new IllegalStateException("Fragment " + paramFragment + " is not currently in the FragmentManager"));
    }
    paramBundle.putInt(paramString, paramFragment.mIndex);
  }
  
  public void registerFragmentLifecycleCallbacks(FragmentManager.FragmentLifecycleCallbacks paramFragmentLifecycleCallbacks, boolean paramBoolean)
  {
    if (this.mLifecycleCallbacks == null) {
      this.mLifecycleCallbacks = new CopyOnWriteArrayList();
    }
    this.mLifecycleCallbacks.add(new Pair(paramFragmentLifecycleCallbacks, Boolean.valueOf(paramBoolean)));
  }
  
  public void removeFragment(Fragment paramFragment)
  {
    if (DEBUG) {
      Log.v("FragmentManager", "remove: " + paramFragment + " nesting=" + paramFragment.mBackStackNesting);
    }
    if (!paramFragment.isInBackStack()) {}
    for (int i = 1;; i = 0)
    {
      if ((!paramFragment.mDetached) || (i != 0))
      {
        if (this.mAdded != null) {
          this.mAdded.remove(paramFragment);
        }
        if ((paramFragment.mHasMenu) && (paramFragment.mMenuVisible)) {
          this.mNeedMenuInvalidate = true;
        }
        paramFragment.mAdded = false;
        paramFragment.mRemoving = true;
      }
      return;
    }
  }
  
  public void removeOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener paramOnBackStackChangedListener)
  {
    if (this.mBackStackChangeListeners != null) {
      this.mBackStackChangeListeners.remove(paramOnBackStackChangedListener);
    }
  }
  
  void reportBackStackChanged()
  {
    if (this.mBackStackChangeListeners != null) {
      for (int i = 0; i < this.mBackStackChangeListeners.size(); i++) {
        ((FragmentManager.OnBackStackChangedListener)this.mBackStackChangeListeners.get(i)).onBackStackChanged();
      }
    }
  }
  
  void restoreAllState(Parcelable paramParcelable, FragmentManagerNonConfig paramFragmentManagerNonConfig)
  {
    if (paramParcelable == null) {}
    for (;;)
    {
      return;
      FragmentManagerState localFragmentManagerState = (FragmentManagerState)paramParcelable;
      if (localFragmentManagerState.mActive != null)
      {
        paramParcelable = null;
        List localList;
        Object localObject;
        int j;
        FragmentState localFragmentState;
        if (paramFragmentManagerNonConfig != null)
        {
          localList = paramFragmentManagerNonConfig.getFragments();
          localObject = paramFragmentManagerNonConfig.getChildNonConfigs();
          if (localList != null) {}
          for (i = localList.size();; i = 0) {
            for (j = 0;; j++)
            {
              paramParcelable = (Parcelable)localObject;
              if (j >= i) {
                break;
              }
              paramParcelable = (Fragment)localList.get(j);
              if (DEBUG) {
                Log.v("FragmentManager", "restoreAllState: re-attaching retained " + paramParcelable);
              }
              localFragmentState = localFragmentManagerState.mActive[paramParcelable.mIndex];
              localFragmentState.mInstance = paramParcelable;
              paramParcelable.mSavedViewState = null;
              paramParcelable.mBackStackNesting = 0;
              paramParcelable.mInLayout = false;
              paramParcelable.mAdded = false;
              paramParcelable.mTarget = null;
              if (localFragmentState.mSavedFragmentState != null)
              {
                localFragmentState.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
                paramParcelable.mSavedViewState = localFragmentState.mSavedFragmentState.getSparseParcelableArray("android:view_state");
                paramParcelable.mSavedFragmentState = localFragmentState.mSavedFragmentState;
              }
            }
          }
        }
        this.mActive = new ArrayList(localFragmentManagerState.mActive.length);
        if (this.mAvailIndices != null) {
          this.mAvailIndices.clear();
        }
        int i = 0;
        if (i < localFragmentManagerState.mActive.length)
        {
          localFragmentState = localFragmentManagerState.mActive[i];
          if (localFragmentState != null)
          {
            localList = null;
            localObject = localList;
            if (paramParcelable != null)
            {
              localObject = localList;
              if (i < paramParcelable.size()) {
                localObject = (FragmentManagerNonConfig)paramParcelable.get(i);
              }
            }
            localObject = localFragmentState.instantiate(this.mHost, this.mParent, (FragmentManagerNonConfig)localObject);
            if (DEBUG) {
              Log.v("FragmentManager", "restoreAllState: active #" + i + ": " + localObject);
            }
            this.mActive.add(localObject);
            localFragmentState.mInstance = null;
          }
          for (;;)
          {
            i++;
            break;
            this.mActive.add(null);
            if (this.mAvailIndices == null) {
              this.mAvailIndices = new ArrayList();
            }
            if (DEBUG) {
              Log.v("FragmentManager", "restoreAllState: avail #" + i);
            }
            this.mAvailIndices.add(Integer.valueOf(i));
          }
        }
        if (paramFragmentManagerNonConfig != null)
        {
          paramFragmentManagerNonConfig = paramFragmentManagerNonConfig.getFragments();
          if (paramFragmentManagerNonConfig != null)
          {
            i = paramFragmentManagerNonConfig.size();
            j = 0;
            label483:
            if (j >= i) {
              break label598;
            }
            paramParcelable = (Fragment)paramFragmentManagerNonConfig.get(j);
            if (paramParcelable.mTargetIndex >= 0) {
              if (paramParcelable.mTargetIndex >= this.mActive.size()) {
                break label551;
              }
            }
          }
          for (paramParcelable.mTarget = ((Fragment)this.mActive.get(paramParcelable.mTargetIndex));; paramParcelable.mTarget = null)
          {
            j++;
            break label483;
            i = 0;
            break;
            label551:
            Log.w("FragmentManager", "Re-attaching retained fragment " + paramParcelable + " target no longer exists: " + paramParcelable.mTargetIndex);
          }
        }
        label598:
        if (localFragmentManagerState.mAdded != null)
        {
          this.mAdded = new ArrayList(localFragmentManagerState.mAdded.length);
          for (i = 0; i < localFragmentManagerState.mAdded.length; i++)
          {
            paramParcelable = (Fragment)this.mActive.get(localFragmentManagerState.mAdded[i]);
            if (paramParcelable == null) {
              throwException(new IllegalStateException("No instantiated fragment for index #" + localFragmentManagerState.mAdded[i]));
            }
            paramParcelable.mAdded = true;
            if (DEBUG) {
              Log.v("FragmentManager", "restoreAllState: added #" + i + ": " + paramParcelable);
            }
            if (this.mAdded.contains(paramParcelable)) {
              throw new IllegalStateException("Already added!");
            }
            this.mAdded.add(paramParcelable);
          }
        }
        this.mAdded = null;
        if (localFragmentManagerState.mBackStack != null)
        {
          this.mBackStack = new ArrayList(localFragmentManagerState.mBackStack.length);
          for (i = 0; i < localFragmentManagerState.mBackStack.length; i++)
          {
            paramParcelable = localFragmentManagerState.mBackStack[i].instantiate(this);
            if (DEBUG)
            {
              Log.v("FragmentManager", "restoreAllState: back stack #" + i + " (index " + paramParcelable.mIndex + "): " + paramParcelable);
              paramFragmentManagerNonConfig = new PrintWriter(new LogWriter("FragmentManager"));
              paramParcelable.dump("  ", paramFragmentManagerNonConfig, false);
              paramFragmentManagerNonConfig.close();
            }
            this.mBackStack.add(paramParcelable);
            if (paramParcelable.mIndex >= 0) {
              setBackStackIndex(paramParcelable.mIndex, paramParcelable);
            }
          }
        }
        else
        {
          this.mBackStack = null;
        }
      }
    }
  }
  
  FragmentManagerNonConfig retainNonConfig()
  {
    Object localObject4 = null;
    Object localObject2 = null;
    Object localObject3 = null;
    Object localObject1 = null;
    if (this.mActive != null)
    {
      int i = 0;
      for (;;)
      {
        localObject3 = localObject1;
        localObject4 = localObject2;
        if (i >= this.mActive.size()) {
          break;
        }
        Fragment localFragment = (Fragment)this.mActive.get(i);
        localObject4 = localObject1;
        Object localObject5 = localObject2;
        if (localFragment != null)
        {
          localObject3 = localObject2;
          if (localFragment.mRetainInstance)
          {
            localObject4 = localObject2;
            if (localObject2 == null) {
              localObject4 = new ArrayList();
            }
            ((ArrayList)localObject4).add(localFragment);
            localFragment.mRetaining = true;
            if (localFragment.mTarget == null) {
              break label253;
            }
          }
          label253:
          for (int j = localFragment.mTarget.mIndex;; j = -1)
          {
            localFragment.mTargetIndex = j;
            localObject3 = localObject4;
            if (DEBUG)
            {
              Log.v("FragmentManager", "retainNonConfig: keeping retained " + localFragment);
              localObject3 = localObject4;
            }
            int k = 0;
            j = k;
            localObject2 = localObject1;
            if (localFragment.mChildFragmentManager == null) {
              break label268;
            }
            localObject4 = localFragment.mChildFragmentManager.retainNonConfig();
            j = k;
            localObject2 = localObject1;
            if (localObject4 == null) {
              break label268;
            }
            localObject2 = localObject1;
            if (localObject1 != null) {
              break;
            }
            localObject1 = new ArrayList();
            for (j = 0;; j++)
            {
              localObject2 = localObject1;
              if (j >= i) {
                break;
              }
              ((ArrayList)localObject1).add(null);
            }
          }
          ((ArrayList)localObject2).add(localObject4);
          j = 1;
          label268:
          localObject4 = localObject2;
          localObject5 = localObject3;
          if (localObject2 != null)
          {
            localObject4 = localObject2;
            localObject5 = localObject3;
            if (j == 0)
            {
              ((ArrayList)localObject2).add(null);
              localObject5 = localObject3;
              localObject4 = localObject2;
            }
          }
        }
        i++;
        localObject1 = localObject4;
        localObject2 = localObject5;
      }
    }
    if ((localObject4 == null) && (localObject3 == null)) {}
    for (localObject1 = null;; localObject1 = new FragmentManagerNonConfig((List)localObject4, (List)localObject3)) {
      return (FragmentManagerNonConfig)localObject1;
    }
  }
  
  Parcelable saveAllState()
  {
    Object localObject2 = null;
    forcePostponedTransactions();
    endAnimatingAwayFragments();
    execPendingActions();
    if (HONEYCOMB) {
      this.mStateSaved = true;
    }
    Object localObject1 = localObject2;
    if (this.mActive != null)
    {
      if (this.mActive.size() > 0) {
        break label55;
      }
      localObject1 = localObject2;
    }
    for (;;)
    {
      return (Parcelable)localObject1;
      label55:
      int m = this.mActive.size();
      FragmentState[] arrayOfFragmentState = new FragmentState[m];
      int j = 0;
      int i = 0;
      Object localObject3;
      if (i < m)
      {
        localObject3 = (Fragment)this.mActive.get(i);
        int k;
        if (localObject3 != null)
        {
          if (((Fragment)localObject3).mIndex < 0) {
            throwException(new IllegalStateException("Failure saving state: active " + localObject3 + " has cleared index: " + ((Fragment)localObject3).mIndex));
          }
          k = 1;
          localObject1 = new FragmentState((Fragment)localObject3);
          arrayOfFragmentState[i] = localObject1;
          if ((((Fragment)localObject3).mState <= 0) || (((FragmentState)localObject1).mSavedFragmentState != null)) {
            break label380;
          }
          ((FragmentState)localObject1).mSavedFragmentState = saveFragmentBasicState((Fragment)localObject3);
          if (((Fragment)localObject3).mTarget != null)
          {
            if (((Fragment)localObject3).mTarget.mIndex < 0) {
              throwException(new IllegalStateException("Failure saving state: " + localObject3 + " has target not in fragment manager: " + ((Fragment)localObject3).mTarget));
            }
            if (((FragmentState)localObject1).mSavedFragmentState == null) {
              ((FragmentState)localObject1).mSavedFragmentState = new Bundle();
            }
            putFragment(((FragmentState)localObject1).mSavedFragmentState, "android:target_state", ((Fragment)localObject3).mTarget);
            if (((Fragment)localObject3).mTargetRequestCode != 0) {
              ((FragmentState)localObject1).mSavedFragmentState.putInt("android:target_req_state", ((Fragment)localObject3).mTargetRequestCode);
            }
          }
        }
        for (;;)
        {
          j = k;
          if (DEBUG)
          {
            Log.v("FragmentManager", "Saved state of " + localObject3 + ": " + ((FragmentState)localObject1).mSavedFragmentState);
            j = k;
          }
          i++;
          break;
          label380:
          ((FragmentState)localObject1).mSavedFragmentState = ((Fragment)localObject3).mSavedFragmentState;
        }
      }
      if (j == 0)
      {
        localObject1 = localObject2;
        if (DEBUG)
        {
          Log.v("FragmentManager", "saveAllState: no fragments!");
          localObject1 = localObject2;
        }
      }
      else
      {
        localObject2 = null;
        localObject3 = null;
        localObject1 = localObject2;
        if (this.mAdded != null)
        {
          j = this.mAdded.size();
          localObject1 = localObject2;
          if (j > 0)
          {
            localObject2 = new int[j];
            for (i = 0;; i++)
            {
              localObject1 = localObject2;
              if (i >= j) {
                break;
              }
              localObject2[i] = ((Fragment)this.mAdded.get(i)).mIndex;
              if (localObject2[i] < 0) {
                throwException(new IllegalStateException("Failure saving state: active " + this.mAdded.get(i) + " has cleared index: " + localObject2[i]));
              }
              if (DEBUG) {
                Log.v("FragmentManager", "saveAllState: adding fragment #" + i + ": " + this.mAdded.get(i));
              }
            }
          }
        }
        localObject2 = localObject3;
        if (this.mBackStack != null)
        {
          j = this.mBackStack.size();
          localObject2 = localObject3;
          if (j > 0)
          {
            localObject3 = new BackStackState[j];
            for (i = 0;; i++)
            {
              localObject2 = localObject3;
              if (i >= j) {
                break;
              }
              localObject3[i] = new BackStackState((BackStackRecord)this.mBackStack.get(i));
              if (DEBUG) {
                Log.v("FragmentManager", "saveAllState: adding back stack #" + i + ": " + this.mBackStack.get(i));
              }
            }
          }
        }
        localObject3 = new FragmentManagerState();
        ((FragmentManagerState)localObject3).mActive = arrayOfFragmentState;
        ((FragmentManagerState)localObject3).mAdded = ((int[])localObject1);
        ((FragmentManagerState)localObject3).mBackStack = ((BackStackState[])localObject2);
        localObject1 = localObject3;
      }
    }
  }
  
  Bundle saveFragmentBasicState(Fragment paramFragment)
  {
    Object localObject2 = null;
    if (this.mStateBundle == null) {
      this.mStateBundle = new Bundle();
    }
    paramFragment.performSaveInstanceState(this.mStateBundle);
    dispatchOnFragmentSaveInstanceState(paramFragment, this.mStateBundle, false);
    if (!this.mStateBundle.isEmpty())
    {
      localObject2 = this.mStateBundle;
      this.mStateBundle = null;
    }
    if (paramFragment.mView != null) {
      saveFragmentViewState(paramFragment);
    }
    Object localObject1 = localObject2;
    if (paramFragment.mSavedViewState != null)
    {
      localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = new Bundle();
      }
      ((Bundle)localObject1).putSparseParcelableArray("android:view_state", paramFragment.mSavedViewState);
    }
    localObject2 = localObject1;
    if (!paramFragment.mUserVisibleHint)
    {
      localObject2 = localObject1;
      if (localObject1 == null) {
        localObject2 = new Bundle();
      }
      ((Bundle)localObject2).putBoolean("android:user_visible_hint", paramFragment.mUserVisibleHint);
    }
    return (Bundle)localObject2;
  }
  
  public Fragment.SavedState saveFragmentInstanceState(Fragment paramFragment)
  {
    Object localObject2 = null;
    if (paramFragment.mIndex < 0) {
      throwException(new IllegalStateException("Fragment " + paramFragment + " is not currently in the FragmentManager"));
    }
    Object localObject1 = localObject2;
    if (paramFragment.mState > 0)
    {
      paramFragment = saveFragmentBasicState(paramFragment);
      localObject1 = localObject2;
      if (paramFragment != null) {
        localObject1 = new Fragment.SavedState(paramFragment);
      }
    }
    return (Fragment.SavedState)localObject1;
  }
  
  void saveFragmentViewState(Fragment paramFragment)
  {
    if (paramFragment.mInnerView == null) {
      return;
    }
    if (this.mStateArray == null) {
      this.mStateArray = new SparseArray();
    }
    for (;;)
    {
      paramFragment.mInnerView.saveHierarchyState(this.mStateArray);
      if (this.mStateArray.size() <= 0) {
        break;
      }
      paramFragment.mSavedViewState = this.mStateArray;
      this.mStateArray = null;
      break;
      this.mStateArray.clear();
    }
  }
  
  /* Error */
  public void setBackStackIndex(int paramInt, BackStackRecord paramBackStackRecord)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   6: ifnonnull +19 -> 25
    //   9: new 183	java/util/ArrayList
    //   12: astore 5
    //   14: aload 5
    //   16: invokespecial 352	java/util/ArrayList:<init>	()V
    //   19: aload_0
    //   20: aload 5
    //   22: putfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   25: aload_0
    //   26: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   29: invokevirtual 222	java/util/ArrayList:size	()I
    //   32: istore 4
    //   34: iload 4
    //   36: istore_3
    //   37: iload_1
    //   38: iload 4
    //   40: if_icmpge +63 -> 103
    //   43: getstatic 119	android/support/v4/app/FragmentManagerImpl:DEBUG	Z
    //   46: ifeq +44 -> 90
    //   49: new 238	java/lang/StringBuilder
    //   52: astore 5
    //   54: aload 5
    //   56: invokespecial 239	java/lang/StringBuilder:<init>	()V
    //   59: ldc 55
    //   61: aload 5
    //   63: ldc_w 715
    //   66: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: iload_1
    //   70: invokevirtual 718	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   73: ldc_w 720
    //   76: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: aload_2
    //   80: invokevirtual 672	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   83: invokevirtual 249	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   86: invokestatic 675	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   89: pop
    //   90: aload_0
    //   91: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   94: iload_1
    //   95: aload_2
    //   96: invokevirtual 733	java/util/ArrayList:set	(ILjava/lang/Object;)Ljava/lang/Object;
    //   99: pop
    //   100: aload_0
    //   101: monitorexit
    //   102: return
    //   103: iload_3
    //   104: iload_1
    //   105: if_icmpge +90 -> 195
    //   108: aload_0
    //   109: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   112: aconst_null
    //   113: invokevirtual 263	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   116: pop
    //   117: aload_0
    //   118: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   121: ifnonnull +19 -> 140
    //   124: new 183	java/util/ArrayList
    //   127: astore 5
    //   129: aload 5
    //   131: invokespecial 352	java/util/ArrayList:<init>	()V
    //   134: aload_0
    //   135: aload 5
    //   137: putfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   140: getstatic 119	android/support/v4/app/FragmentManagerImpl:DEBUG	Z
    //   143: ifeq +34 -> 177
    //   146: new 238	java/lang/StringBuilder
    //   149: astore 5
    //   151: aload 5
    //   153: invokespecial 239	java/lang/StringBuilder:<init>	()V
    //   156: ldc 55
    //   158: aload 5
    //   160: ldc_w 1686
    //   163: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   166: iload_3
    //   167: invokevirtual 718	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   170: invokevirtual 249	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   173: invokestatic 675	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   176: pop
    //   177: aload_0
    //   178: getfield 711	android/support/v4/app/FragmentManagerImpl:mAvailBackStackIndices	Ljava/util/ArrayList;
    //   181: iload_3
    //   182: invokestatic 1081	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   185: invokevirtual 263	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   188: pop
    //   189: iinc 3 1
    //   192: goto -89 -> 103
    //   195: getstatic 119	android/support/v4/app/FragmentManagerImpl:DEBUG	Z
    //   198: ifeq +44 -> 242
    //   201: new 238	java/lang/StringBuilder
    //   204: astore 5
    //   206: aload 5
    //   208: invokespecial 239	java/lang/StringBuilder:<init>	()V
    //   211: ldc 55
    //   213: aload 5
    //   215: ldc_w 727
    //   218: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: iload_1
    //   222: invokevirtual 718	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   225: ldc_w 729
    //   228: invokevirtual 245	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: aload_2
    //   232: invokevirtual 672	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   235: invokevirtual 249	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   238: invokestatic 675	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   241: pop
    //   242: aload_0
    //   243: getfield 713	android/support/v4/app/FragmentManagerImpl:mBackStackIndices	Ljava/util/ArrayList;
    //   246: aload_2
    //   247: invokevirtual 263	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   250: pop
    //   251: goto -151 -> 100
    //   254: astore_2
    //   255: aload_0
    //   256: monitorexit
    //   257: aload_2
    //   258: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	259	0	this	FragmentManagerImpl
    //   0	259	1	paramInt	int
    //   0	259	2	paramBackStackRecord	BackStackRecord
    //   36	154	3	i	int
    //   32	9	4	j	int
    //   12	202	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	25	254	finally
    //   25	34	254	finally
    //   43	90	254	finally
    //   90	100	254	finally
    //   100	102	254	finally
    //   108	140	254	finally
    //   140	177	254	finally
    //   177	189	254	finally
    //   195	242	254	finally
    //   242	251	254	finally
    //   255	257	254	finally
  }
  
  public void showFragment(Fragment paramFragment)
  {
    boolean bool = false;
    if (DEBUG) {
      Log.v("FragmentManager", "show: " + paramFragment);
    }
    if (paramFragment.mHidden)
    {
      paramFragment.mHidden = false;
      if (!paramFragment.mHiddenChanged) {
        bool = true;
      }
      paramFragment.mHiddenChanged = bool;
    }
  }
  
  void startPendingDeferredFragments()
  {
    if (this.mActive == null) {}
    for (;;)
    {
      return;
      for (int i = 0; i < this.mActive.size(); i++)
      {
        Fragment localFragment = (Fragment)this.mActive.get(i);
        if (localFragment != null) {
          performPendingDeferredStart(localFragment);
        }
      }
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    localStringBuilder.append("FragmentManager{");
    localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    localStringBuilder.append(" in ");
    if (this.mParent != null) {
      DebugUtils.buildShortClassTag(this.mParent, localStringBuilder);
    }
    for (;;)
    {
      localStringBuilder.append("}}");
      return localStringBuilder.toString();
      DebugUtils.buildShortClassTag(this.mHost, localStringBuilder);
    }
  }
  
  public void unregisterFragmentLifecycleCallbacks(FragmentManager.FragmentLifecycleCallbacks paramFragmentLifecycleCallbacks)
  {
    if (this.mLifecycleCallbacks == null) {
      return;
    }
    CopyOnWriteArrayList localCopyOnWriteArrayList = this.mLifecycleCallbacks;
    for (int i = 0;; i++) {
      try
      {
        int j = this.mLifecycleCallbacks.size();
        if (i < j)
        {
          if (((Pair)this.mLifecycleCallbacks.get(i)).first == paramFragmentLifecycleCallbacks) {
            this.mLifecycleCallbacks.remove(i);
          }
        }
        else {
          break;
        }
      }
      finally {}
    }
  }
  
  static class AnimateOnHWLayerIfNeededListener
    implements Animation.AnimationListener
  {
    private Animation.AnimationListener mOriginalListener;
    private boolean mShouldRunOnHWLayer;
    View mView;
    
    public AnimateOnHWLayerIfNeededListener(View paramView, Animation paramAnimation)
    {
      if ((paramView == null) || (paramAnimation == null)) {}
      for (;;)
      {
        return;
        this.mView = paramView;
      }
    }
    
    public AnimateOnHWLayerIfNeededListener(View paramView, Animation paramAnimation, Animation.AnimationListener paramAnimationListener)
    {
      if ((paramView == null) || (paramAnimation == null)) {}
      for (;;)
      {
        return;
        this.mOriginalListener = paramAnimationListener;
        this.mView = paramView;
        this.mShouldRunOnHWLayer = true;
      }
    }
    
    @CallSuper
    public void onAnimationEnd(Animation paramAnimation)
    {
      if ((this.mView != null) && (this.mShouldRunOnHWLayer))
      {
        if ((!ViewCompat.isAttachedToWindow(this.mView)) && (!BuildCompat.isAtLeastN())) {
          break label64;
        }
        this.mView.post(new Runnable()
        {
          public void run()
          {
            ViewCompat.setLayerType(FragmentManagerImpl.AnimateOnHWLayerIfNeededListener.this.mView, 0, null);
          }
        });
      }
      for (;;)
      {
        if (this.mOriginalListener != null) {
          this.mOriginalListener.onAnimationEnd(paramAnimation);
        }
        return;
        label64:
        ViewCompat.setLayerType(this.mView, 0, null);
      }
    }
    
    public void onAnimationRepeat(Animation paramAnimation)
    {
      if (this.mOriginalListener != null) {
        this.mOriginalListener.onAnimationRepeat(paramAnimation);
      }
    }
    
    @CallSuper
    public void onAnimationStart(Animation paramAnimation)
    {
      if (this.mOriginalListener != null) {
        this.mOriginalListener.onAnimationStart(paramAnimation);
      }
    }
  }
  
  static class FragmentTag
  {
    public static final int[] Fragment = { 16842755, 16842960, 16842961 };
    public static final int Fragment_id = 1;
    public static final int Fragment_name = 0;
    public static final int Fragment_tag = 2;
  }
  
  static abstract interface OpGenerator
  {
    public abstract boolean generateOps(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1);
  }
  
  private class PopBackStackState
    implements FragmentManagerImpl.OpGenerator
  {
    final int mFlags;
    final int mId;
    final String mName;
    
    PopBackStackState(String paramString, int paramInt1, int paramInt2)
    {
      this.mName = paramString;
      this.mId = paramInt1;
      this.mFlags = paramInt2;
    }
    
    public boolean generateOps(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1)
    {
      return FragmentManagerImpl.this.popBackStackState(paramArrayList, paramArrayList1, this.mName, this.mId, this.mFlags);
    }
  }
  
  static class StartEnterTransitionListener
    implements Fragment.OnStartEnterTransitionListener
  {
    private final boolean mIsBack;
    private int mNumPostponed;
    private final BackStackRecord mRecord;
    
    StartEnterTransitionListener(BackStackRecord paramBackStackRecord, boolean paramBoolean)
    {
      this.mIsBack = paramBoolean;
      this.mRecord = paramBackStackRecord;
    }
    
    public void cancelTransaction()
    {
      this.mRecord.mManager.completeExecute(this.mRecord, this.mIsBack, false, false);
    }
    
    public void completeTransaction()
    {
      boolean bool1 = false;
      if (this.mNumPostponed > 0) {}
      for (int i = 1;; i = 0)
      {
        localObject2 = this.mRecord.mManager;
        int k = ((FragmentManagerImpl)localObject2).mAdded.size();
        for (int j = 0; j < k; j++)
        {
          localObject1 = (Fragment)((FragmentManagerImpl)localObject2).mAdded.get(j);
          ((Fragment)localObject1).setOnStartEnterTransitionListener(null);
          if ((i != 0) && (((Fragment)localObject1).isPostponed())) {
            ((Fragment)localObject1).startPostponedEnterTransition();
          }
        }
      }
      Object localObject1 = this.mRecord.mManager;
      Object localObject2 = this.mRecord;
      boolean bool2 = this.mIsBack;
      if (i == 0) {
        bool1 = true;
      }
      ((FragmentManagerImpl)localObject1).completeExecute((BackStackRecord)localObject2, bool2, bool1, true);
    }
    
    public boolean isReady()
    {
      if (this.mNumPostponed == 0) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public void onStartEnterTransition()
    {
      this.mNumPostponed -= 1;
      if (this.mNumPostponed != 0) {}
      for (;;)
      {
        return;
        this.mRecord.mManager.scheduleCommit();
      }
    }
    
    public void startListening()
    {
      this.mNumPostponed += 1;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\app\FragmentManagerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */