package com.crashlytics.android.core;

import android.content.Context;
import android.util.Log;
import com.crashlytics.android.core.internal.CrashEventDataProvider;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.ApiKey;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.ExecutorUtils;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.DependsOn;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.PriorityCallable;
import io.fabric.sdk.android.services.concurrency.Task;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.HttpRequestFactory;
import io.fabric.sdk.android.services.persistence.FileStore;
import io.fabric.sdk.android.services.persistence.FileStoreImpl;
import io.fabric.sdk.android.services.persistence.PreferenceStore;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;
import io.fabric.sdk.android.services.settings.FeaturesSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.HttpsURLConnection;

@DependsOn({CrashEventDataProvider.class})
public class CrashlyticsCore
  extends Kit<Void>
{
  static final float CLS_DEFAULT_PROCESS_DELAY = 1.0F;
  static final String CRASHLYTICS_REQUIRE_BUILD_ID = "com.crashlytics.RequireBuildId";
  static final boolean CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT = true;
  static final String CRASH_MARKER_FILE_NAME = "crash_marker";
  static final int DEFAULT_MAIN_HANDLER_TIMEOUT_SEC = 4;
  private static final String INITIALIZATION_MARKER_FILE_NAME = "initialization_marker";
  static final int MAX_ATTRIBUTES = 64;
  static final int MAX_ATTRIBUTE_SIZE = 1024;
  private static final String MISSING_BUILD_ID_MSG = "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.";
  private static final String PREFERENCE_STORE_NAME = "com.crashlytics.android.core.CrashlyticsCore";
  public static final String TAG = "CrashlyticsCore";
  private final ConcurrentHashMap<String, String> attributes;
  private CrashlyticsBackgroundWorker backgroundWorker;
  private CrashlyticsController controller;
  private CrashlyticsFileMarker crashMarker;
  private float delay;
  private boolean disabled;
  private CrashEventDataProvider externalCrashEventDataProvider;
  private HttpRequestFactory httpRequestFactory;
  private CrashlyticsFileMarker initializationMarker;
  private CrashlyticsListener listener;
  private final PinningInfoProvider pinningInfo;
  private final long startTime;
  private String userEmail = null;
  private String userId = null;
  private String userName = null;
  
  public CrashlyticsCore()
  {
    this(1.0F, null, null, false);
  }
  
  CrashlyticsCore(float paramFloat, CrashlyticsListener paramCrashlyticsListener, PinningInfoProvider paramPinningInfoProvider, boolean paramBoolean)
  {
    this(paramFloat, paramCrashlyticsListener, paramPinningInfoProvider, paramBoolean, ExecutorUtils.buildSingleThreadExecutorService("Crashlytics Exception Handler"));
  }
  
  CrashlyticsCore(float paramFloat, CrashlyticsListener paramCrashlyticsListener, PinningInfoProvider paramPinningInfoProvider, boolean paramBoolean, ExecutorService paramExecutorService)
  {
    this.delay = paramFloat;
    if (paramCrashlyticsListener != null) {}
    for (;;)
    {
      this.listener = paramCrashlyticsListener;
      this.pinningInfo = paramPinningInfoProvider;
      this.disabled = paramBoolean;
      this.backgroundWorker = new CrashlyticsBackgroundWorker(paramExecutorService);
      this.attributes = new ConcurrentHashMap();
      this.startTime = System.currentTimeMillis();
      return;
      paramCrashlyticsListener = new NoOpListener(null);
    }
  }
  
  private void checkForPreviousCrash()
  {
    Boolean localBoolean = (Boolean)this.backgroundWorker.submitAndWait(new CrashMarkerCheck(this.crashMarker));
    if (!Boolean.TRUE.equals(localBoolean)) {}
    for (;;)
    {
      return;
      try
      {
        this.listener.crashlyticsDidDetectCrashDuringPreviousExecution();
      }
      catch (Exception localException)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", localException);
      }
    }
  }
  
  private void doLog(int paramInt, String paramString1, String paramString2)
  {
    if (this.disabled) {}
    for (;;)
    {
      return;
      if (ensureFabricWithCalled("prior to logging messages."))
      {
        long l2 = System.currentTimeMillis();
        long l1 = this.startTime;
        this.controller.writeToLog(l2 - l1, formatLogMessage(paramInt, paramString1, paramString2));
      }
    }
  }
  
  private static boolean ensureFabricWithCalled(String paramString)
  {
    CrashlyticsCore localCrashlyticsCore = getInstance();
    if ((localCrashlyticsCore == null) || (localCrashlyticsCore.controller == null)) {
      Fabric.getLogger().e("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + paramString, null);
    }
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  private void finishInitSynchronously()
  {
    Object localObject = new PriorityCallable()
    {
      public Void call()
        throws Exception
      {
        return CrashlyticsCore.this.doInBackground();
      }
      
      public Priority getPriority()
      {
        return Priority.IMMEDIATE;
      }
    };
    Iterator localIterator = getDependencies().iterator();
    while (localIterator.hasNext()) {
      ((PriorityCallable)localObject).addDependency((Task)localIterator.next());
    }
    localObject = getFabric().getExecutorService().submit((Callable)localObject);
    Fabric.getLogger().d("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
    try
    {
      ((Future)localObject).get(4L, TimeUnit.SECONDS);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", localInterruptedException);
      }
    }
    catch (ExecutionException localExecutionException)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", localExecutionException);
      }
    }
    catch (TimeoutException localTimeoutException)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Crashlytics timed out during initialization.", localTimeoutException);
      }
    }
  }
  
  private static String formatLogMessage(int paramInt, String paramString1, String paramString2)
  {
    return CommonUtils.logPriorityToString(paramInt) + "/" + paramString1 + " " + paramString2;
  }
  
  public static CrashlyticsCore getInstance()
  {
    return (CrashlyticsCore)Fabric.getKit(CrashlyticsCore.class);
  }
  
  static boolean isBuildIdValid(String paramString, boolean paramBoolean)
  {
    boolean bool = true;
    if (!paramBoolean)
    {
      Fabric.getLogger().d("CrashlyticsCore", "Configured not to require a build ID.");
      paramBoolean = bool;
    }
    for (;;)
    {
      return paramBoolean;
      paramBoolean = bool;
      if (CommonUtils.isNullOrEmpty(paramString))
      {
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", ".     |  | ");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".   \\ |  | /");
        Log.e("CrashlyticsCore", ".    \\    /");
        Log.e("CrashlyticsCore", ".     \\  /");
        Log.e("CrashlyticsCore", ".      \\/");
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", ".      /\\");
        Log.e("CrashlyticsCore", ".     /  \\");
        Log.e("CrashlyticsCore", ".    /    \\");
        Log.e("CrashlyticsCore", ".   / |  | \\");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".");
        paramBoolean = false;
      }
    }
  }
  
  private static String sanitizeAttribute(String paramString)
  {
    String str = paramString;
    if (paramString != null)
    {
      paramString = paramString.trim();
      str = paramString;
      if (paramString.length() > 1024) {
        str = paramString.substring(0, 1024);
      }
    }
    return str;
  }
  
  public void crash()
  {
    new CrashTest().indexOutOfBounds();
  }
  
  void createCrashMarker()
  {
    this.crashMarker.create();
  }
  
  boolean didPreviousInitializationFail()
  {
    ((Boolean)this.backgroundWorker.submitAndWait(new Callable()
    {
      public Boolean call()
        throws Exception
      {
        return Boolean.valueOf(CrashlyticsCore.this.initializationMarker.isPresent());
      }
    })).booleanValue();
  }
  
  protected Void doInBackground()
  {
    markInitializationStarted();
    Object localObject1 = getExternalCrashEventData();
    if (localObject1 != null) {
      this.controller.writeExternalCrashEvent((SessionEventData)localObject1);
    }
    this.controller.cleanInvalidTempFiles();
    for (;;)
    {
      try
      {
        localObject1 = Settings.getInstance().awaitSettingsData();
        if (localObject1 != null) {
          continue;
        }
        Fabric.getLogger().w("CrashlyticsCore", "Received null settings, skipping report submission!");
      }
      catch (Exception localException)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", localException);
        markInitializationComplete();
        continue;
      }
      finally
      {
        markInitializationComplete();
      }
      return null;
      if (!((SettingsData)localObject1).featuresData.collectReports)
      {
        Fabric.getLogger().d("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
        markInitializationComplete();
      }
      else
      {
        if (!this.controller.finalizeSessions(((SettingsData)localObject1).sessionData)) {
          Fabric.getLogger().d("CrashlyticsCore", "Could not finalize previous sessions.");
        }
        this.controller.submitAllReports(this.delay, (SettingsData)localObject1);
        markInitializationComplete();
      }
    }
  }
  
  Map<String, String> getAttributes()
  {
    return Collections.unmodifiableMap(this.attributes);
  }
  
  CrashlyticsController getController()
  {
    return this.controller;
  }
  
  SessionEventData getExternalCrashEventData()
  {
    SessionEventData localSessionEventData = null;
    if (this.externalCrashEventDataProvider != null) {
      localSessionEventData = this.externalCrashEventDataProvider.getCrashEventData();
    }
    return localSessionEventData;
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android.crashlytics-core";
  }
  
  public PinningInfoProvider getPinningInfoProvider()
  {
    if (!this.disabled) {}
    for (PinningInfoProvider localPinningInfoProvider = this.pinningInfo;; localPinningInfoProvider = null) {
      return localPinningInfoProvider;
    }
  }
  
  String getUserEmail()
  {
    if (getIdManager().canCollectUserIds()) {}
    for (String str = this.userEmail;; str = null) {
      return str;
    }
  }
  
  String getUserIdentifier()
  {
    if (getIdManager().canCollectUserIds()) {}
    for (String str = this.userId;; str = null) {
      return str;
    }
  }
  
  String getUserName()
  {
    if (getIdManager().canCollectUserIds()) {}
    for (String str = this.userName;; str = null) {
      return str;
    }
  }
  
  public String getVersion()
  {
    return "2.3.17.dev";
  }
  
  boolean internalVerifyPinning(URL paramURL)
  {
    boolean bool = false;
    if (getPinningInfoProvider() != null)
    {
      paramURL = this.httpRequestFactory.buildHttpRequest(HttpMethod.GET, paramURL.toString());
      ((HttpsURLConnection)paramURL.getConnection()).setInstanceFollowRedirects(false);
      paramURL.code();
      bool = true;
    }
    return bool;
  }
  
  public void log(int paramInt, String paramString1, String paramString2)
  {
    doLog(paramInt, paramString1, paramString2);
    Fabric.getLogger().log(paramInt, "" + paramString1, "" + paramString2, true);
  }
  
  public void log(String paramString)
  {
    doLog(3, "CrashlyticsCore", paramString);
  }
  
  public void logException(Throwable paramThrowable)
  {
    if (this.disabled) {}
    for (;;)
    {
      return;
      if (ensureFabricWithCalled("prior to logging exceptions.")) {
        if (paramThrowable == null) {
          Fabric.getLogger().log(5, "CrashlyticsCore", "Crashlytics is ignoring a request to log a null exception.");
        } else {
          this.controller.writeNonFatalException(Thread.currentThread(), paramThrowable);
        }
      }
    }
  }
  
  void markInitializationComplete()
  {
    this.backgroundWorker.submit(new Callable()
    {
      public Boolean call()
        throws Exception
      {
        try
        {
          boolean bool = CrashlyticsCore.this.initializationMarker.remove();
          Logger localLogger = Fabric.getLogger();
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          localLogger.d("CrashlyticsCore", "Initialization marker file removed: " + bool);
          localObject = Boolean.valueOf(bool);
        }
        catch (Exception localException)
        {
          for (;;)
          {
            Object localObject;
            Fabric.getLogger().e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", localException);
            Boolean localBoolean = Boolean.valueOf(false);
          }
        }
        return (Boolean)localObject;
      }
    });
  }
  
  void markInitializationStarted()
  {
    this.backgroundWorker.submitAndWait(new Callable()
    {
      public Void call()
        throws Exception
      {
        CrashlyticsCore.this.initializationMarker.create();
        Fabric.getLogger().d("CrashlyticsCore", "Initialization marker file created.");
        return null;
      }
    });
  }
  
  protected boolean onPreExecute()
  {
    return onPreExecute(super.getContext());
  }
  
  boolean onPreExecute(Context paramContext)
  {
    boolean bool;
    if (this.disabled) {
      bool = false;
    }
    for (;;)
    {
      return bool;
      Object localObject3 = new ApiKey().getValue(paramContext);
      if (localObject3 == null)
      {
        bool = false;
      }
      else
      {
        Object localObject4 = CommonUtils.resolveBuildId(paramContext);
        if (!isBuildIdValid((String)localObject4, CommonUtils.getBooleanResourceValue(paramContext, "com.crashlytics.RequireBuildId", true))) {
          throw new UnmetDependencyException("This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
        }
        try
        {
          Object localObject2 = Fabric.getLogger();
          Object localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((Logger)localObject2).i("CrashlyticsCore", "Initializing Crashlytics " + getVersion());
          localObject2 = new io/fabric/sdk/android/services/persistence/FileStoreImpl;
          ((FileStoreImpl)localObject2).<init>(this);
          localObject1 = new com/crashlytics/android/core/CrashlyticsFileMarker;
          ((CrashlyticsFileMarker)localObject1).<init>("crash_marker", (FileStore)localObject2);
          this.crashMarker = ((CrashlyticsFileMarker)localObject1);
          localObject1 = new com/crashlytics/android/core/CrashlyticsFileMarker;
          ((CrashlyticsFileMarker)localObject1).<init>("initialization_marker", (FileStore)localObject2);
          this.initializationMarker = ((CrashlyticsFileMarker)localObject1);
          localObject1 = new io/fabric/sdk/android/services/persistence/PreferenceStoreImpl;
          ((PreferenceStoreImpl)localObject1).<init>(getContext(), "com.crashlytics.android.core.CrashlyticsCore");
          PreferenceManager localPreferenceManager = PreferenceManager.create((PreferenceStore)localObject1, this);
          if (this.pinningInfo != null)
          {
            localObject1 = new com/crashlytics/android/core/CrashlyticsPinningInfoProvider;
            ((CrashlyticsPinningInfoProvider)localObject1).<init>(this.pinningInfo);
          }
          for (;;)
          {
            Object localObject5 = new io/fabric/sdk/android/services/network/DefaultHttpRequestFactory;
            ((DefaultHttpRequestFactory)localObject5).<init>(Fabric.getLogger());
            this.httpRequestFactory = ((HttpRequestFactory)localObject5);
            this.httpRequestFactory.setPinningInfoProvider((io.fabric.sdk.android.services.network.PinningInfoProvider)localObject1);
            localObject1 = getIdManager();
            localObject4 = AppData.create(paramContext, (IdManager)localObject1, (String)localObject3, (String)localObject4);
            localObject3 = new com/crashlytics/android/core/ManifestUnityVersionProvider;
            ((ManifestUnityVersionProvider)localObject3).<init>(paramContext, ((AppData)localObject4).packageName);
            Logger localLogger = Fabric.getLogger();
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            localLogger.d("CrashlyticsCore", "Installer package name is: " + ((AppData)localObject4).installerPackageName);
            localObject5 = new com/crashlytics/android/core/CrashlyticsController;
            ((CrashlyticsController)localObject5).<init>(this, this.backgroundWorker, this.httpRequestFactory, (IdManager)localObject1, localPreferenceManager, (FileStore)localObject2, (AppData)localObject4, (UnityVersionProvider)localObject3);
            this.controller = ((CrashlyticsController)localObject5);
            bool = didPreviousInitializationFail();
            checkForPreviousCrash();
            this.controller.enableExceptionHandling(Thread.getDefaultUncaughtExceptionHandler());
            if ((!bool) || (!CommonUtils.canTryConnection(paramContext))) {
              break label409;
            }
            Fabric.getLogger().d("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
            finishInitSynchronously();
            bool = false;
            break;
            localObject1 = null;
          }
        }
        catch (Exception paramContext)
        {
          Fabric.getLogger().e("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", paramContext);
          this.controller = null;
          bool = false;
        }
        label409:
        Fabric.getLogger().d("CrashlyticsCore", "Exception handling initialization successful");
        bool = true;
      }
    }
  }
  
  public void setBool(String paramString, boolean paramBoolean)
  {
    setString(paramString, Boolean.toString(paramBoolean));
  }
  
  public void setDouble(String paramString, double paramDouble)
  {
    setString(paramString, Double.toString(paramDouble));
  }
  
  void setExternalCrashEventDataProvider(CrashEventDataProvider paramCrashEventDataProvider)
  {
    this.externalCrashEventDataProvider = paramCrashEventDataProvider;
  }
  
  public void setFloat(String paramString, float paramFloat)
  {
    setString(paramString, Float.toString(paramFloat));
  }
  
  public void setInt(String paramString, int paramInt)
  {
    setString(paramString, Integer.toString(paramInt));
  }
  
  @Deprecated
  public void setListener(CrashlyticsListener paramCrashlyticsListener)
  {
    try
    {
      Fabric.getLogger().w("CrashlyticsCore", "Use of setListener is deprecated.");
      if (paramCrashlyticsListener == null)
      {
        paramCrashlyticsListener = new java/lang/IllegalArgumentException;
        paramCrashlyticsListener.<init>("listener must not be null.");
        throw paramCrashlyticsListener;
      }
    }
    finally {}
    this.listener = paramCrashlyticsListener;
  }
  
  public void setLong(String paramString, long paramLong)
  {
    setString(paramString, Long.toString(paramLong));
  }
  
  public void setString(String paramString1, String paramString2)
  {
    if (this.disabled) {}
    String str;
    for (;;)
    {
      return;
      if (!ensureFabricWithCalled("prior to setting keys.")) {
        break label139;
      }
      if (paramString1 == null)
      {
        paramString1 = getContext();
        if ((paramString1 != null) && (CommonUtils.isAppDebuggable(paramString1))) {
          throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
        Fabric.getLogger().e("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", null);
      }
      else
      {
        str = sanitizeAttribute(paramString1);
        if ((this.attributes.size() < 64) || (this.attributes.containsKey(str))) {
          break;
        }
        Fabric.getLogger().d("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
      }
    }
    if (paramString2 == null) {}
    for (paramString1 = "";; paramString1 = sanitizeAttribute(paramString2))
    {
      this.attributes.put(str, paramString1);
      this.controller.cacheKeyData(this.attributes);
      break;
      label139:
      break;
    }
  }
  
  public void setUserEmail(String paramString)
  {
    if (this.disabled) {}
    for (;;)
    {
      return;
      if (ensureFabricWithCalled("prior to setting user data."))
      {
        this.userEmail = sanitizeAttribute(paramString);
        this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }
    }
  }
  
  public void setUserIdentifier(String paramString)
  {
    if (this.disabled) {}
    for (;;)
    {
      return;
      if (ensureFabricWithCalled("prior to setting user data."))
      {
        this.userId = sanitizeAttribute(paramString);
        this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }
    }
  }
  
  public void setUserName(String paramString)
  {
    if (this.disabled) {}
    for (;;)
    {
      return;
      if (ensureFabricWithCalled("prior to setting user data."))
      {
        this.userName = sanitizeAttribute(paramString);
        this.controller.cacheUserData(this.userId, this.userName, this.userEmail);
      }
    }
  }
  
  public boolean verifyPinning(URL paramURL)
  {
    try
    {
      bool = internalVerifyPinning(paramURL);
      return bool;
    }
    catch (Exception paramURL)
    {
      for (;;)
      {
        Fabric.getLogger().e("CrashlyticsCore", "Could not verify SSL pinning", paramURL);
        boolean bool = false;
      }
    }
  }
  
  public static class Builder
  {
    private float delay = -1.0F;
    private boolean disabled = false;
    private CrashlyticsListener listener;
    private PinningInfoProvider pinningInfoProvider;
    
    public CrashlyticsCore build()
    {
      if (this.delay < 0.0F) {
        this.delay = 1.0F;
      }
      return new CrashlyticsCore(this.delay, this.listener, this.pinningInfoProvider, this.disabled);
    }
    
    public Builder delay(float paramFloat)
    {
      if (paramFloat <= 0.0F) {
        throw new IllegalArgumentException("delay must be greater than 0");
      }
      if (this.delay > 0.0F) {
        throw new IllegalStateException("delay already set.");
      }
      this.delay = paramFloat;
      return this;
    }
    
    public Builder disabled(boolean paramBoolean)
    {
      this.disabled = paramBoolean;
      return this;
    }
    
    public Builder listener(CrashlyticsListener paramCrashlyticsListener)
    {
      if (paramCrashlyticsListener == null) {
        throw new IllegalArgumentException("listener must not be null.");
      }
      if (this.listener != null) {
        throw new IllegalStateException("listener already set.");
      }
      this.listener = paramCrashlyticsListener;
      return this;
    }
    
    @Deprecated
    public Builder pinningInfo(PinningInfoProvider paramPinningInfoProvider)
    {
      if (paramPinningInfoProvider == null) {
        throw new IllegalArgumentException("pinningInfoProvider must not be null.");
      }
      if (this.pinningInfoProvider != null) {
        throw new IllegalStateException("pinningInfoProvider already set.");
      }
      this.pinningInfoProvider = paramPinningInfoProvider;
      return this;
    }
  }
  
  private static final class CrashMarkerCheck
    implements Callable<Boolean>
  {
    private final CrashlyticsFileMarker crashMarker;
    
    public CrashMarkerCheck(CrashlyticsFileMarker paramCrashlyticsFileMarker)
    {
      this.crashMarker = paramCrashlyticsFileMarker;
    }
    
    public Boolean call()
      throws Exception
    {
      if (!this.crashMarker.isPresent()) {}
      for (Boolean localBoolean = Boolean.FALSE;; localBoolean = Boolean.TRUE)
      {
        return localBoolean;
        Fabric.getLogger().d("CrashlyticsCore", "Found previous crash marker.");
        this.crashMarker.remove();
      }
    }
  }
  
  private static final class NoOpListener
    implements CrashlyticsListener
  {
    public void crashlyticsDidDetectCrashDuringPreviousExecution() {}
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\CrashlyticsCore.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */