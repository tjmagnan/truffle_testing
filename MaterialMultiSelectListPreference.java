package com.afollestad.materialdialogs.prefs;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.preference.MultiSelectListPreference;
import android.preference.Preference.BaseSavedState;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;
import com.afollestad.materialdialogs.MaterialDialog.ListCallbackMultiChoice;
import com.afollestad.materialdialogs.MaterialDialog.SingleButtonCallback;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@TargetApi(11)
public class MaterialMultiSelectListPreference
  extends MultiSelectListPreference
{
  private Context context;
  private MaterialDialog mDialog;
  
  public MaterialMultiSelectListPreference(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null);
  }
  
  public MaterialMultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialMultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet);
  }
  
  @TargetApi(21)
  public MaterialMultiSelectListPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    init(paramContext, paramAttributeSet);
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet)
  {
    this.context = paramContext;
    PrefUtil.setLayoutResource(paramContext, this, paramAttributeSet);
    if (Build.VERSION.SDK_INT <= 10) {
      setWidgetLayoutResource(0);
    }
  }
  
  public Dialog getDialog()
  {
    return this.mDialog;
  }
  
  public void onActivityDestroy()
  {
    super.onActivityDestroy();
    if ((this.mDialog != null) && (this.mDialog.isShowing())) {
      this.mDialog.dismiss();
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    PrefUtil.unregisterOnActivityDestroyListener(this, this);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable == null) || (!paramParcelable.getClass().equals(SavedState.class))) {
      super.onRestoreInstanceState(paramParcelable);
    }
    for (;;)
    {
      return;
      paramParcelable = (SavedState)paramParcelable;
      super.onRestoreInstanceState(paramParcelable.getSuperState());
      if (paramParcelable.isDialogShowing) {
        showDialog(paramParcelable.dialogBundle);
      }
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Object localObject = super.onSaveInstanceState();
    Dialog localDialog = getDialog();
    if ((localDialog == null) || (!localDialog.isShowing())) {}
    for (;;)
    {
      return (Parcelable)localObject;
      localObject = new SavedState((Parcelable)localObject);
      ((SavedState)localObject).isDialogShowing = true;
      ((SavedState)localObject).dialogBundle = localDialog.onSaveInstanceState();
    }
  }
  
  public void setEntries(CharSequence[] paramArrayOfCharSequence)
  {
    super.setEntries(paramArrayOfCharSequence);
    if (this.mDialog != null) {
      this.mDialog.setItems(paramArrayOfCharSequence);
    }
  }
  
  protected void showDialog(Bundle paramBundle)
  {
    Object localObject2 = new ArrayList();
    Iterator localIterator = getValues().iterator();
    while (localIterator.hasNext())
    {
      localObject1 = (String)localIterator.next();
      if (findIndexOfValue((String)localObject1) >= 0) {
        ((List)localObject2).add(Integer.valueOf(findIndexOfValue((String)localObject1)));
      }
    }
    localObject2 = new MaterialDialog.Builder(this.context).title(getDialogTitle()).icon(getDialogIcon()).negativeText(getNegativeButtonText()).positiveText(getPositiveButtonText()).onAny(new MaterialDialog.SingleButtonCallback()
    {
      public void onClick(@NonNull MaterialDialog paramAnonymousMaterialDialog, @NonNull DialogAction paramAnonymousDialogAction)
      {
        switch (MaterialMultiSelectListPreference.3.$SwitchMap$com$afollestad$materialdialogs$DialogAction[paramAnonymousDialogAction.ordinal()])
        {
        default: 
          MaterialMultiSelectListPreference.this.onClick(paramAnonymousMaterialDialog, -1);
        }
        for (;;)
        {
          return;
          MaterialMultiSelectListPreference.this.onClick(paramAnonymousMaterialDialog, -3);
          continue;
          MaterialMultiSelectListPreference.this.onClick(paramAnonymousMaterialDialog, -2);
        }
      }
    }).items(getEntries()).itemsCallbackMultiChoice((Integer[])((List)localObject2).toArray(new Integer[((List)localObject2).size()]), new MaterialDialog.ListCallbackMultiChoice()
    {
      public boolean onSelection(MaterialDialog paramAnonymousMaterialDialog, Integer[] paramAnonymousArrayOfInteger, CharSequence[] paramAnonymousArrayOfCharSequence)
      {
        MaterialMultiSelectListPreference.this.onClick(null, -1);
        paramAnonymousMaterialDialog.dismiss();
        paramAnonymousMaterialDialog = new HashSet();
        int j = paramAnonymousArrayOfInteger.length;
        for (int i = 0; i < j; i++)
        {
          int k = paramAnonymousArrayOfInteger[i].intValue();
          paramAnonymousMaterialDialog.add(MaterialMultiSelectListPreference.this.getEntryValues()[k].toString());
        }
        if (MaterialMultiSelectListPreference.this.callChangeListener(paramAnonymousMaterialDialog)) {
          MaterialMultiSelectListPreference.this.setValues(paramAnonymousMaterialDialog);
        }
        return true;
      }
    }).dismissListener(this);
    Object localObject1 = onCreateDialogView();
    if (localObject1 != null)
    {
      onBindDialogView((View)localObject1);
      ((MaterialDialog.Builder)localObject2).customView((View)localObject1, false);
    }
    for (;;)
    {
      PrefUtil.registerOnActivityDestroyListener(this, this);
      this.mDialog = ((MaterialDialog.Builder)localObject2).build();
      if (paramBundle != null) {
        this.mDialog.onRestoreInstanceState(paramBundle);
      }
      this.mDialog.show();
      return;
      ((MaterialDialog.Builder)localObject2).content(getDialogMessage());
    }
  }
  
  private static class SavedState
    extends Preference.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public MaterialMultiSelectListPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
      {
        return new MaterialMultiSelectListPreference.SavedState(paramAnonymousParcel);
      }
      
      public MaterialMultiSelectListPreference.SavedState[] newArray(int paramAnonymousInt)
      {
        return new MaterialMultiSelectListPreference.SavedState[paramAnonymousInt];
      }
    };
    Bundle dialogBundle;
    boolean isDialogShowing;
    
    SavedState(Parcel paramParcel)
    {
      super();
      if (paramParcel.readInt() == 1) {}
      for (;;)
      {
        this.isDialogShowing = bool;
        this.dialogBundle = paramParcel.readBundle();
        return;
        bool = false;
      }
    }
    
    SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(@NonNull Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      if (this.isDialogShowing) {}
      for (paramInt = 1;; paramInt = 0)
      {
        paramParcel.writeInt(paramInt);
        paramParcel.writeBundle(this.dialogBundle);
        return;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\prefs\MaterialMultiSelectListPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */