package org.joda.time.format;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.DurationField;
import org.joda.time.MutableDateTime;
import org.joda.time.MutableDateTime.Property;
import org.joda.time.ReadablePartial;
import org.joda.time.field.MillisDurationField;
import org.joda.time.field.PreciseDateTimeField;

public class DateTimeFormatterBuilder
{
  private ArrayList<Object> iElementPairs = new ArrayList();
  private Object iFormatter;
  
  private DateTimeFormatterBuilder append0(Object paramObject)
  {
    this.iFormatter = null;
    this.iElementPairs.add(paramObject);
    this.iElementPairs.add(paramObject);
    return this;
  }
  
  private DateTimeFormatterBuilder append0(InternalPrinter paramInternalPrinter, InternalParser paramInternalParser)
  {
    this.iFormatter = null;
    this.iElementPairs.add(paramInternalPrinter);
    this.iElementPairs.add(paramInternalParser);
    return this;
  }
  
  static void appendUnknownString(Appendable paramAppendable, int paramInt)
    throws IOException
  {
    for (;;)
    {
      
      if (paramInt < 0) {
        break;
      }
      paramAppendable.append(65533);
    }
  }
  
  private void checkParser(DateTimeParser paramDateTimeParser)
  {
    if (paramDateTimeParser == null) {
      throw new IllegalArgumentException("No parser supplied");
    }
  }
  
  private void checkPrinter(DateTimePrinter paramDateTimePrinter)
  {
    if (paramDateTimePrinter == null) {
      throw new IllegalArgumentException("No printer supplied");
    }
  }
  
  static boolean csStartsWith(CharSequence paramCharSequence, int paramInt, String paramString)
  {
    boolean bool2 = false;
    int j = paramString.length();
    boolean bool1;
    if (paramCharSequence.length() - paramInt < j) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      for (int i = 0;; i++)
      {
        if (i >= j) {
          break label64;
        }
        bool1 = bool2;
        if (paramCharSequence.charAt(paramInt + i) != paramString.charAt(i)) {
          break;
        }
      }
      label64:
      bool1 = true;
    }
  }
  
  static boolean csStartsWithIgnoreCase(CharSequence paramCharSequence, int paramInt, String paramString)
  {
    boolean bool2 = false;
    int j = paramString.length();
    boolean bool1;
    if (paramCharSequence.length() - paramInt < j) {
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      for (int i = 0;; i++)
      {
        if (i >= j) {
          break label104;
        }
        char c2 = paramCharSequence.charAt(paramInt + i);
        char c1 = paramString.charAt(i);
        if (c2 != c1)
        {
          c2 = Character.toUpperCase(c2);
          c1 = Character.toUpperCase(c1);
          if (c2 != c1)
          {
            bool1 = bool2;
            if (Character.toLowerCase(c2) != Character.toLowerCase(c1)) {
              break;
            }
          }
        }
      }
      label104:
      bool1 = true;
    }
  }
  
  private Object getFormatter()
  {
    Object localObject2 = this.iFormatter;
    Object localObject1 = localObject2;
    Object localObject3;
    Object localObject4;
    if (localObject2 == null)
    {
      localObject1 = localObject2;
      if (this.iElementPairs.size() == 2)
      {
        localObject3 = this.iElementPairs.get(0);
        localObject4 = this.iElementPairs.get(1);
        if (localObject3 == null) {
          break label89;
        }
        if (localObject3 != localObject4)
        {
          localObject1 = localObject2;
          if (localObject4 != null) {
            break label62;
          }
        }
      }
    }
    label62:
    label89:
    for (localObject1 = localObject3;; localObject1 = localObject4)
    {
      localObject2 = localObject1;
      if (localObject1 == null) {
        localObject2 = new Composite(this.iElementPairs);
      }
      this.iFormatter = localObject2;
      localObject1 = localObject2;
      return localObject1;
    }
  }
  
  private boolean isFormatter(Object paramObject)
  {
    if ((isPrinter(paramObject)) || (isParser(paramObject))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private boolean isParser(Object paramObject)
  {
    boolean bool;
    if ((paramObject instanceof InternalParser)) {
      if ((paramObject instanceof Composite)) {
        bool = ((Composite)paramObject).isParser();
      }
    }
    for (;;)
    {
      return bool;
      bool = true;
      continue;
      bool = false;
    }
  }
  
  private boolean isPrinter(Object paramObject)
  {
    boolean bool;
    if ((paramObject instanceof InternalPrinter)) {
      if ((paramObject instanceof Composite)) {
        bool = ((Composite)paramObject).isPrinter();
      }
    }
    for (;;)
    {
      return bool;
      bool = true;
      continue;
      bool = false;
    }
  }
  
  public DateTimeFormatterBuilder append(DateTimeFormatter paramDateTimeFormatter)
  {
    if (paramDateTimeFormatter == null) {
      throw new IllegalArgumentException("No formatter supplied");
    }
    return append0(paramDateTimeFormatter.getPrinter0(), paramDateTimeFormatter.getParser0());
  }
  
  public DateTimeFormatterBuilder append(DateTimeParser paramDateTimeParser)
  {
    checkParser(paramDateTimeParser);
    return append0(null, DateTimeParserInternalParser.of(paramDateTimeParser));
  }
  
  public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter)
  {
    checkPrinter(paramDateTimePrinter);
    return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), null);
  }
  
  public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter, DateTimeParser paramDateTimeParser)
  {
    checkPrinter(paramDateTimePrinter);
    checkParser(paramDateTimeParser);
    return append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), DateTimeParserInternalParser.of(paramDateTimeParser));
  }
  
  public DateTimeFormatterBuilder append(DateTimePrinter paramDateTimePrinter, DateTimeParser[] paramArrayOfDateTimeParser)
  {
    int i = 0;
    if (paramDateTimePrinter != null) {
      checkPrinter(paramDateTimePrinter);
    }
    if (paramArrayOfDateTimeParser == null) {
      throw new IllegalArgumentException("No parsers supplied");
    }
    int j = paramArrayOfDateTimeParser.length;
    if (j == 1) {
      if (paramArrayOfDateTimeParser[0] == null) {
        throw new IllegalArgumentException("No parser supplied");
      }
    }
    InternalParser[] arrayOfInternalParser;
    for (paramDateTimePrinter = append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[0]));; paramDateTimePrinter = append0(DateTimePrinterInternalPrinter.of(paramDateTimePrinter), new MatchingParser(arrayOfInternalParser)))
    {
      return paramDateTimePrinter;
      arrayOfInternalParser = new InternalParser[j];
      while (i < j - 1)
      {
        InternalParser localInternalParser = DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[i]);
        arrayOfInternalParser[i] = localInternalParser;
        if (localInternalParser == null) {
          throw new IllegalArgumentException("Incomplete parser array");
        }
        i++;
      }
      arrayOfInternalParser[i] = DateTimeParserInternalParser.of(paramArrayOfDateTimeParser[i]);
    }
  }
  
  public DateTimeFormatterBuilder appendCenturyOfEra(int paramInt1, int paramInt2)
  {
    return appendSignedDecimal(DateTimeFieldType.centuryOfEra(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendClockhourOfDay(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.clockhourOfDay(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendClockhourOfHalfday(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.clockhourOfHalfday(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendDayOfMonth(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.dayOfMonth(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendDayOfWeek(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.dayOfWeek(), paramInt, 1);
  }
  
  public DateTimeFormatterBuilder appendDayOfWeekShortText()
  {
    return appendShortText(DateTimeFieldType.dayOfWeek());
  }
  
  public DateTimeFormatterBuilder appendDayOfWeekText()
  {
    return appendText(DateTimeFieldType.dayOfWeek());
  }
  
  public DateTimeFormatterBuilder appendDayOfYear(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.dayOfYear(), paramInt, 3);
  }
  
  public DateTimeFormatterBuilder appendDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    int i = paramInt2;
    if (paramInt2 < paramInt1) {
      i = paramInt1;
    }
    if ((paramInt1 < 0) || (i <= 0)) {
      throw new IllegalArgumentException();
    }
    if (paramInt1 <= 1) {}
    for (paramDateTimeFieldType = append0(new UnpaddedNumber(paramDateTimeFieldType, i, false));; paramDateTimeFieldType = append0(new PaddedNumber(paramDateTimeFieldType, i, false, paramInt1))) {
      return paramDateTimeFieldType;
    }
  }
  
  public DateTimeFormatterBuilder appendEraText()
  {
    return appendText(DateTimeFieldType.era());
  }
  
  public DateTimeFormatterBuilder appendFixedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    if (paramInt <= 0) {
      throw new IllegalArgumentException("Illegal number of digits: " + paramInt);
    }
    return append0(new FixedNumber(paramDateTimeFieldType, paramInt, false));
  }
  
  public DateTimeFormatterBuilder appendFixedSignedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    if (paramInt <= 0) {
      throw new IllegalArgumentException("Illegal number of digits: " + paramInt);
    }
    return append0(new FixedNumber(paramDateTimeFieldType, paramInt, true));
  }
  
  public DateTimeFormatterBuilder appendFraction(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    int i = paramInt2;
    if (paramInt2 < paramInt1) {
      i = paramInt1;
    }
    if ((paramInt1 < 0) || (i <= 0)) {
      throw new IllegalArgumentException();
    }
    return append0(new Fraction(paramDateTimeFieldType, paramInt1, i));
  }
  
  public DateTimeFormatterBuilder appendFractionOfDay(int paramInt1, int paramInt2)
  {
    return appendFraction(DateTimeFieldType.dayOfYear(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendFractionOfHour(int paramInt1, int paramInt2)
  {
    return appendFraction(DateTimeFieldType.hourOfDay(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendFractionOfMinute(int paramInt1, int paramInt2)
  {
    return appendFraction(DateTimeFieldType.minuteOfDay(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendFractionOfSecond(int paramInt1, int paramInt2)
  {
    return appendFraction(DateTimeFieldType.secondOfDay(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendHalfdayOfDayText()
  {
    return appendText(DateTimeFieldType.halfdayOfDay());
  }
  
  public DateTimeFormatterBuilder appendHourOfDay(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.hourOfDay(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendHourOfHalfday(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.hourOfHalfday(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendLiteral(char paramChar)
  {
    return append0(new CharacterLiteral(paramChar));
  }
  
  public DateTimeFormatterBuilder appendLiteral(String paramString)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("Literal must not be null");
    }
    DateTimeFormatterBuilder localDateTimeFormatterBuilder = this;
    switch (paramString.length())
    {
    }
    for (localDateTimeFormatterBuilder = append0(new StringLiteral(paramString));; localDateTimeFormatterBuilder = append0(new CharacterLiteral(paramString.charAt(0)))) {
      return localDateTimeFormatterBuilder;
    }
  }
  
  public DateTimeFormatterBuilder appendMillisOfDay(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.millisOfDay(), paramInt, 8);
  }
  
  public DateTimeFormatterBuilder appendMillisOfSecond(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.millisOfSecond(), paramInt, 3);
  }
  
  public DateTimeFormatterBuilder appendMinuteOfDay(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.minuteOfDay(), paramInt, 4);
  }
  
  public DateTimeFormatterBuilder appendMinuteOfHour(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.minuteOfHour(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendMonthOfYear(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.monthOfYear(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendMonthOfYearShortText()
  {
    return appendShortText(DateTimeFieldType.monthOfYear());
  }
  
  public DateTimeFormatterBuilder appendMonthOfYearText()
  {
    return appendText(DateTimeFieldType.monthOfYear());
  }
  
  public DateTimeFormatterBuilder appendOptional(DateTimeParser paramDateTimeParser)
  {
    checkParser(paramDateTimeParser);
    return append0(null, new MatchingParser(new InternalParser[] { DateTimeParserInternalParser.of(paramDateTimeParser), null }));
  }
  
  public DateTimeFormatterBuilder appendPattern(String paramString)
  {
    DateTimeFormat.appendPatternTo(this, paramString);
    return this;
  }
  
  public DateTimeFormatterBuilder appendSecondOfDay(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.secondOfDay(), paramInt, 5);
  }
  
  public DateTimeFormatterBuilder appendSecondOfMinute(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.secondOfMinute(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendShortText(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    return append0(new TextField(paramDateTimeFieldType, true));
  }
  
  public DateTimeFormatterBuilder appendSignedDecimal(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    int i = paramInt2;
    if (paramInt2 < paramInt1) {
      i = paramInt1;
    }
    if ((paramInt1 < 0) || (i <= 0)) {
      throw new IllegalArgumentException();
    }
    if (paramInt1 <= 1) {}
    for (paramDateTimeFieldType = append0(new UnpaddedNumber(paramDateTimeFieldType, i, true));; paramDateTimeFieldType = append0(new PaddedNumber(paramDateTimeFieldType, i, true, paramInt1))) {
      return paramDateTimeFieldType;
    }
  }
  
  public DateTimeFormatterBuilder appendText(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field type must not be null");
    }
    return append0(new TextField(paramDateTimeFieldType, false));
  }
  
  public DateTimeFormatterBuilder appendTimeZoneId()
  {
    return append0(TimeZoneId.INSTANCE, TimeZoneId.INSTANCE);
  }
  
  public DateTimeFormatterBuilder appendTimeZoneName()
  {
    return append0(new TimeZoneName(0, null), null);
  }
  
  public DateTimeFormatterBuilder appendTimeZoneName(Map<String, DateTimeZone> paramMap)
  {
    paramMap = new TimeZoneName(0, paramMap);
    return append0(paramMap, paramMap);
  }
  
  public DateTimeFormatterBuilder appendTimeZoneOffset(String paramString1, String paramString2, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    return append0(new TimeZoneOffset(paramString1, paramString2, paramBoolean, paramInt1, paramInt2));
  }
  
  public DateTimeFormatterBuilder appendTimeZoneOffset(String paramString, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    return append0(new TimeZoneOffset(paramString, paramString, paramBoolean, paramInt1, paramInt2));
  }
  
  public DateTimeFormatterBuilder appendTimeZoneShortName()
  {
    return append0(new TimeZoneName(1, null), null);
  }
  
  public DateTimeFormatterBuilder appendTimeZoneShortName(Map<String, DateTimeZone> paramMap)
  {
    paramMap = new TimeZoneName(1, paramMap);
    return append0(paramMap, paramMap);
  }
  
  public DateTimeFormatterBuilder appendTwoDigitWeekyear(int paramInt)
  {
    return appendTwoDigitWeekyear(paramInt, false);
  }
  
  public DateTimeFormatterBuilder appendTwoDigitWeekyear(int paramInt, boolean paramBoolean)
  {
    return append0(new TwoDigitYear(DateTimeFieldType.weekyear(), paramInt, paramBoolean));
  }
  
  public DateTimeFormatterBuilder appendTwoDigitYear(int paramInt)
  {
    return appendTwoDigitYear(paramInt, false);
  }
  
  public DateTimeFormatterBuilder appendTwoDigitYear(int paramInt, boolean paramBoolean)
  {
    return append0(new TwoDigitYear(DateTimeFieldType.year(), paramInt, paramBoolean));
  }
  
  public DateTimeFormatterBuilder appendWeekOfWeekyear(int paramInt)
  {
    return appendDecimal(DateTimeFieldType.weekOfWeekyear(), paramInt, 2);
  }
  
  public DateTimeFormatterBuilder appendWeekyear(int paramInt1, int paramInt2)
  {
    return appendSignedDecimal(DateTimeFieldType.weekyear(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendYear(int paramInt1, int paramInt2)
  {
    return appendSignedDecimal(DateTimeFieldType.year(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendYearOfCentury(int paramInt1, int paramInt2)
  {
    return appendDecimal(DateTimeFieldType.yearOfCentury(), paramInt1, paramInt2);
  }
  
  public DateTimeFormatterBuilder appendYearOfEra(int paramInt1, int paramInt2)
  {
    return appendDecimal(DateTimeFieldType.yearOfEra(), paramInt1, paramInt2);
  }
  
  public boolean canBuildFormatter()
  {
    return isFormatter(getFormatter());
  }
  
  public boolean canBuildParser()
  {
    return isParser(getFormatter());
  }
  
  public boolean canBuildPrinter()
  {
    return isPrinter(getFormatter());
  }
  
  public void clear()
  {
    this.iFormatter = null;
    this.iElementPairs.clear();
  }
  
  public DateTimeFormatter toFormatter()
  {
    Object localObject = getFormatter();
    if (isPrinter(localObject)) {}
    for (InternalPrinter localInternalPrinter = (InternalPrinter)localObject;; localInternalPrinter = null)
    {
      if (isParser(localObject)) {}
      for (localObject = (InternalParser)localObject;; localObject = null)
      {
        if ((localInternalPrinter != null) || (localObject != null)) {
          return new DateTimeFormatter(localInternalPrinter, (InternalParser)localObject);
        }
        throw new UnsupportedOperationException("Both printing and parsing not supported");
      }
    }
  }
  
  public DateTimeParser toParser()
  {
    Object localObject = getFormatter();
    if (isParser(localObject)) {
      return InternalParserDateTimeParser.of((InternalParser)localObject);
    }
    throw new UnsupportedOperationException("Parsing is not supported");
  }
  
  public DateTimePrinter toPrinter()
  {
    Object localObject = getFormatter();
    if (isPrinter(localObject)) {
      return InternalPrinterDateTimePrinter.of((InternalPrinter)localObject);
    }
    throw new UnsupportedOperationException("Printing is not supported");
  }
  
  static class CharacterLiteral
    implements InternalPrinter, InternalParser
  {
    private final char iValue;
    
    CharacterLiteral(char paramChar)
    {
      this.iValue = paramChar;
    }
    
    public int estimateParsedLength()
    {
      return 1;
    }
    
    public int estimatePrintedLength()
    {
      return 1;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      if (paramInt >= paramCharSequence.length()) {
        paramInt ^= 0xFFFFFFFF;
      }
      for (;;)
      {
        return paramInt;
        char c2 = paramCharSequence.charAt(paramInt);
        char c1 = this.iValue;
        if (c2 != c1)
        {
          c2 = Character.toUpperCase(c2);
          c1 = Character.toUpperCase(c1);
          if ((c2 != c1) && (Character.toLowerCase(c2) != Character.toLowerCase(c1)))
          {
            paramInt ^= 0xFFFFFFFF;
            continue;
          }
        }
        paramInt++;
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      paramAppendable.append(this.iValue);
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      paramAppendable.append(this.iValue);
    }
  }
  
  static class Composite
    implements InternalPrinter, InternalParser
  {
    private final int iParsedLengthEstimate;
    private final InternalParser[] iParsers;
    private final int iPrintedLengthEstimate;
    private final InternalPrinter[] iPrinters;
    
    Composite(List<Object> paramList)
    {
      ArrayList localArrayList2 = new ArrayList();
      ArrayList localArrayList1 = new ArrayList();
      decompose(paramList, localArrayList2, localArrayList1);
      if ((localArrayList2.contains(null)) || (localArrayList2.isEmpty()))
      {
        this.iPrinters = null;
        this.iPrintedLengthEstimate = 0;
        if ((!localArrayList1.contains(null)) && (!localArrayList1.isEmpty())) {
          break label167;
        }
        this.iParsers = null;
      }
      int j;
      for (this.iParsedLengthEstimate = 0;; this.iParsedLengthEstimate = j)
      {
        return;
        int m = localArrayList2.size();
        this.iPrinters = new InternalPrinter[m];
        int i = 0;
        j = 0;
        while (i < m)
        {
          paramList = (InternalPrinter)localArrayList2.get(i);
          j += paramList.estimatePrintedLength();
          this.iPrinters[i] = paramList;
          i++;
        }
        this.iPrintedLengthEstimate = j;
        break;
        label167:
        m = localArrayList1.size();
        this.iParsers = new InternalParser[m];
        j = 0;
        for (i = k; i < m; i++)
        {
          paramList = (InternalParser)localArrayList1.get(i);
          j += paramList.estimateParsedLength();
          this.iParsers[i] = paramList;
        }
      }
    }
    
    private void addArrayToList(List<Object> paramList, Object[] paramArrayOfObject)
    {
      if (paramArrayOfObject != null) {
        for (int i = 0; i < paramArrayOfObject.length; i++) {
          paramList.add(paramArrayOfObject[i]);
        }
      }
    }
    
    private void decompose(List<Object> paramList1, List<Object> paramList2, List<Object> paramList3)
    {
      int j = paramList1.size();
      int i = 0;
      if (i < j)
      {
        Object localObject = paramList1.get(i);
        if ((localObject instanceof Composite))
        {
          addArrayToList(paramList2, ((Composite)localObject).iPrinters);
          label49:
          localObject = paramList1.get(i + 1);
          if (!(localObject instanceof Composite)) {
            break label100;
          }
          addArrayToList(paramList3, ((Composite)localObject).iParsers);
        }
        for (;;)
        {
          i += 2;
          break;
          paramList2.add(localObject);
          break label49;
          label100:
          paramList3.add(localObject);
        }
      }
    }
    
    public int estimateParsedLength()
    {
      return this.iParsedLengthEstimate;
    }
    
    public int estimatePrintedLength()
    {
      return this.iPrintedLengthEstimate;
    }
    
    boolean isParser()
    {
      if (this.iParsers != null) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    boolean isPrinter()
    {
      if (this.iPrinters != null) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      InternalParser[] arrayOfInternalParser = this.iParsers;
      if (arrayOfInternalParser == null) {
        throw new UnsupportedOperationException();
      }
      int j = arrayOfInternalParser.length;
      for (int i = 0; (i < j) && (paramInt >= 0); i++) {
        paramInt = arrayOfInternalParser[i].parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
      }
      return paramInt;
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      InternalPrinter[] arrayOfInternalPrinter = this.iPrinters;
      if (arrayOfInternalPrinter == null) {
        throw new UnsupportedOperationException();
      }
      if (paramLocale == null) {
        paramLocale = Locale.getDefault();
      }
      for (;;)
      {
        int j = arrayOfInternalPrinter.length;
        for (int i = 0; i < j; i++) {
          arrayOfInternalPrinter[i].printTo(paramAppendable, paramLong, paramChronology, paramInt, paramDateTimeZone, paramLocale);
        }
        return;
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      InternalPrinter[] arrayOfInternalPrinter = this.iPrinters;
      if (arrayOfInternalPrinter == null) {
        throw new UnsupportedOperationException();
      }
      Locale localLocale = paramLocale;
      if (paramLocale == null) {
        localLocale = Locale.getDefault();
      }
      int j = arrayOfInternalPrinter.length;
      for (int i = 0; i < j; i++) {
        arrayOfInternalPrinter[i].printTo(paramAppendable, paramReadablePartial, localLocale);
      }
    }
  }
  
  static class FixedNumber
    extends DateTimeFormatterBuilder.PaddedNumber
  {
    protected FixedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
    {
      super(paramInt, paramBoolean, paramInt);
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      int k = super.parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
      int i;
      if (k < 0) {
        i = k;
      }
      for (;;)
      {
        return i;
        int m = this.iMaxParsedDigits + paramInt;
        i = k;
        if (k != m)
        {
          int j = m;
          if (this.iSigned)
          {
            paramInt = paramCharSequence.charAt(paramInt);
            if (paramInt != 45)
            {
              j = m;
              if (paramInt != 43) {}
            }
            else
            {
              j = m + 1;
            }
          }
          if (k > j)
          {
            i = j + 1 ^ 0xFFFFFFFF;
          }
          else
          {
            i = k;
            if (k < j) {
              i = k ^ 0xFFFFFFFF;
            }
          }
        }
      }
    }
  }
  
  static class Fraction
    implements InternalPrinter, InternalParser
  {
    private final DateTimeFieldType iFieldType;
    protected int iMaxDigits;
    protected int iMinDigits;
    
    protected Fraction(DateTimeFieldType paramDateTimeFieldType, int paramInt1, int paramInt2)
    {
      this.iFieldType = paramDateTimeFieldType;
      int i = paramInt2;
      if (paramInt2 > 18) {
        i = 18;
      }
      this.iMinDigits = paramInt1;
      this.iMaxDigits = i;
    }
    
    private long[] getFractionData(long paramLong, DateTimeField paramDateTimeField)
    {
      long l2 = paramDateTimeField.getDurationField().getUnitMillis();
      for (int i = this.iMaxDigits;; i--)
      {
        long l1;
        switch (i)
        {
        default: 
          l1 = 1L;
        }
        while (l2 * l1 / l1 == l2)
        {
          return new long[] { l1 * paramLong / l2, i };
          l1 = 10L;
          continue;
          l1 = 100L;
          continue;
          l1 = 1000L;
          continue;
          l1 = 10000L;
          continue;
          l1 = 100000L;
          continue;
          l1 = 1000000L;
          continue;
          l1 = 10000000L;
          continue;
          l1 = 100000000L;
          continue;
          l1 = 1000000000L;
          continue;
          l1 = 10000000000L;
          continue;
          l1 = 100000000000L;
          continue;
          l1 = 1000000000000L;
          continue;
          l1 = 10000000000000L;
          continue;
          l1 = 100000000000000L;
          continue;
          l1 = 1000000000000000L;
          continue;
          l1 = 10000000000000000L;
          continue;
          l1 = 100000000000000000L;
          continue;
          l1 = 1000000000000000000L;
        }
      }
    }
    
    public int estimateParsedLength()
    {
      return this.iMaxDigits;
    }
    
    public int estimatePrintedLength()
    {
      return this.iMaxDigits;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      DateTimeField localDateTimeField = this.iFieldType.getField(paramDateTimeParserBucket.getChronology());
      int j = Math.min(this.iMaxDigits, paramCharSequence.length() - paramInt);
      long l1 = 0L;
      long l2 = localDateTimeField.getDurationField().getUnitMillis() * 10L;
      int i = 0;
      int k;
      if (i < j)
      {
        k = paramCharSequence.charAt(paramInt + i);
        if ((k >= 48) && (k <= 57)) {}
      }
      else
      {
        l1 /= 10L;
        if (i != 0) {
          break label130;
        }
        paramInt ^= 0xFFFFFFFF;
      }
      for (;;)
      {
        return paramInt;
        i++;
        l2 /= 10L;
        l1 += (k - 48) * l2;
        break;
        label130:
        if (l1 > 2147483647L)
        {
          paramInt ^= 0xFFFFFFFF;
        }
        else
        {
          paramDateTimeParserBucket.saveField(new PreciseDateTimeField(DateTimeFieldType.millisOfSecond(), MillisDurationField.INSTANCE, localDateTimeField.getDurationField()), (int)l1);
          paramInt = i + paramInt;
        }
      }
    }
    
    protected void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology)
      throws IOException
    {
      paramChronology = this.iFieldType.getField(paramChronology);
      int j = this.iMinDigits;
      try
      {
        paramLong = paramChronology.remainder(paramLong);
        if (paramLong == 0L)
        {
          for (;;)
          {
            j--;
            if (j < 0) {
              break;
            }
            paramAppendable.append('0');
          }
          return;
        }
      }
      catch (RuntimeException paramChronology)
      {
        DateTimeFormatterBuilder.appendUnknownString(paramAppendable, j);
      }
      for (;;)
      {
        paramChronology = getFractionData(paramLong, paramChronology);
        paramLong = paramChronology[0];
        int i = (int)paramChronology[1];
        if ((0x7FFFFFFF & paramLong) == paramLong) {}
        int k;
        for (paramChronology = Integer.toString((int)paramLong);; paramChronology = Long.toString(paramLong))
        {
          k = paramChronology.length();
          while (k < i)
          {
            paramAppendable.append('0');
            j--;
            i--;
          }
        }
        if (j < i) {
          for (;;)
          {
            if ((j >= i) || (k <= 1) || (paramChronology.charAt(k - 1) != '0'))
            {
              if (k >= paramChronology.length()) {
                break label221;
              }
              for (i = 0; i < k; i++) {
                paramAppendable.append(paramChronology.charAt(i));
              }
              break;
            }
            i--;
            k--;
          }
        }
        label221:
        paramAppendable.append(paramChronology);
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      printTo(paramAppendable, paramLong, paramChronology);
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      printTo(paramAppendable, paramReadablePartial.getChronology().set(paramReadablePartial, 0L), paramReadablePartial.getChronology());
    }
  }
  
  static class MatchingParser
    implements InternalParser
  {
    private final int iParsedLengthEstimate;
    private final InternalParser[] iParsers;
    
    MatchingParser(InternalParser[] paramArrayOfInternalParser)
    {
      this.iParsers = paramArrayOfInternalParser;
      int i = 0;
      int j = paramArrayOfInternalParser.length;
      j--;
      if (j >= 0)
      {
        InternalParser localInternalParser = paramArrayOfInternalParser[j];
        if (localInternalParser == null) {
          break label58;
        }
        int k = localInternalParser.estimateParsedLength();
        if (k <= i) {
          break label58;
        }
        i = k;
      }
      label58:
      for (;;)
      {
        break;
        this.iParsedLengthEstimate = i;
        return;
      }
    }
    
    public int estimateParsedLength()
    {
      return this.iParsedLengthEstimate;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      InternalParser[] arrayOfInternalParser = this.iParsers;
      int n = arrayOfInternalParser.length;
      Object localObject2 = paramDateTimeParserBucket.saveState();
      Object localObject1 = null;
      int j = 0;
      int k = paramInt;
      int i = paramInt;
      InternalParser localInternalParser;
      if (j < n)
      {
        localInternalParser = arrayOfInternalParser[j];
        if (localInternalParser == null) {
          if (i <= paramInt) {
            label54:
            return paramInt;
          }
        }
      }
      for (j = 1;; j = 0)
      {
        int m;
        if ((i > paramInt) || ((i == paramInt) && (j != 0)))
        {
          if (localObject1 != null) {
            paramDateTimeParserBucket.restoreState(localObject1);
          }
          paramInt = i;
          break label54;
          m = localInternalParser.parseInto(paramDateTimeParserBucket, paramCharSequence, paramInt);
          if (m >= paramInt)
          {
            if (m <= i) {
              break label211;
            }
            if ((m >= paramCharSequence.length()) || (j + 1 >= n) || (arrayOfInternalParser[(j + 1)] == null))
            {
              paramInt = m;
              break label54;
            }
            localObject1 = paramDateTimeParserBucket.saveState();
            i = m;
          }
        }
        label211:
        for (;;)
        {
          paramDateTimeParserBucket.restoreState(localObject2);
          j++;
          break;
          if (m < 0)
          {
            m ^= 0xFFFFFFFF;
            if (m > k)
            {
              k = m;
              continue;
              paramInt = k ^ 0xFFFFFFFF;
              break label54;
            }
          }
        }
      }
    }
  }
  
  static abstract class NumberFormatter
    implements InternalPrinter, InternalParser
  {
    protected final DateTimeFieldType iFieldType;
    protected final int iMaxParsedDigits;
    protected final boolean iSigned;
    
    NumberFormatter(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
    {
      this.iFieldType = paramDateTimeFieldType;
      this.iMaxParsedDigits = paramInt;
      this.iSigned = paramBoolean;
    }
    
    public int estimateParsedLength()
    {
      return this.iMaxParsedDigits;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      int i1 = Math.min(this.iMaxParsedDigits, paramCharSequence.length() - paramInt);
      int k = 0;
      int i = 0;
      int j = 0;
      int m = j;
      int n = i;
      int i2;
      if (k < i1)
      {
        i2 = paramCharSequence.charAt(paramInt + k);
        if ((k == 0) && ((i2 == 45) || (i2 == 43)) && (this.iSigned)) {
          if (i2 == 45)
          {
            i = 1;
            label89:
            if (i2 != 43) {
              break label175;
            }
            j = 1;
            label99:
            m = j;
            n = i;
            if (k + 1 >= i1) {
              break label423;
            }
            i2 = paramCharSequence.charAt(paramInt + k + 1);
            m = j;
            n = i;
            if (i2 < 48) {
              break label423;
            }
            if (i2 <= 57) {
              break label181;
            }
            m = i;
          }
        }
      }
      for (;;)
      {
        if (k == 0)
        {
          i = paramInt ^ 0xFFFFFFFF;
          return i;
          i = 0;
          break label89;
          label175:
          j = 0;
          break label99;
          label181:
          k++;
          i1 = Math.min(i1 + 1, paramCharSequence.length() - paramInt);
          break;
          m = j;
          n = i;
          if (i2 < 48) {
            break label423;
          }
          if (i2 > 57)
          {
            m = i;
            continue;
          }
          k++;
          break;
        }
        if (k >= 9)
        {
          if (j != 0) {
            i = paramInt + k;
          }
          for (paramInt = Integer.parseInt(paramCharSequence.subSequence(paramInt + 1, i).toString());; paramInt = Integer.parseInt(paramCharSequence.subSequence(paramInt, i).toString()))
          {
            label275:
            paramDateTimeParserBucket.saveField(this.iFieldType, paramInt);
            break;
            i = paramInt + k;
          }
        }
        if ((m != 0) || (j != 0)) {}
        for (i = paramInt + 1;; i = paramInt)
        {
          try
          {
            j = paramCharSequence.charAt(i);
            k = paramInt + k;
            j -= 48;
            for (paramInt = i + 1; paramInt < k; paramInt++) {
              j = paramCharSequence.charAt(paramInt) + ((j << 3) + (j << 1)) - 48;
            }
          }
          catch (StringIndexOutOfBoundsException paramDateTimeParserBucket)
          {
            i = paramInt ^ 0xFFFFFFFF;
          }
          paramInt = j;
          i = k;
          if (m == 0) {
            break label275;
          }
          paramInt = -j;
          i = k;
          break label275;
        }
        label423:
        j = m;
        m = n;
      }
    }
  }
  
  static class PaddedNumber
    extends DateTimeFormatterBuilder.NumberFormatter
  {
    protected final int iMinPrintedDigits;
    
    protected PaddedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt1, boolean paramBoolean, int paramInt2)
    {
      super(paramInt1, paramBoolean);
      this.iMinPrintedDigits = paramInt2;
    }
    
    public int estimatePrintedLength()
    {
      return this.iMaxParsedDigits;
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      try
      {
        FormatUtils.appendPaddedInteger(paramAppendable, this.iFieldType.getField(paramChronology).get(paramLong), this.iMinPrintedDigits);
        return;
      }
      catch (RuntimeException paramChronology)
      {
        for (;;)
        {
          DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      if (paramReadablePartial.isSupported(this.iFieldType)) {}
      for (;;)
      {
        try
        {
          FormatUtils.appendPaddedInteger(paramAppendable, paramReadablePartial.get(this.iFieldType), this.iMinPrintedDigits);
          return;
        }
        catch (RuntimeException paramReadablePartial)
        {
          DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
          continue;
        }
        DateTimeFormatterBuilder.appendUnknownString(paramAppendable, this.iMinPrintedDigits);
      }
    }
  }
  
  static class StringLiteral
    implements InternalPrinter, InternalParser
  {
    private final String iValue;
    
    StringLiteral(String paramString)
    {
      this.iValue = paramString;
    }
    
    public int estimateParsedLength()
    {
      return this.iValue.length();
    }
    
    public int estimatePrintedLength()
    {
      return this.iValue.length();
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      if (DateTimeFormatterBuilder.csStartsWithIgnoreCase(paramCharSequence, paramInt, this.iValue)) {
        paramInt = this.iValue.length() + paramInt;
      }
      for (;;)
      {
        return paramInt;
        paramInt ^= 0xFFFFFFFF;
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      paramAppendable.append(this.iValue);
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      paramAppendable.append(this.iValue);
    }
  }
  
  static class TextField
    implements InternalPrinter, InternalParser
  {
    private static Map<Locale, Map<DateTimeFieldType, Object[]>> cParseCache = new ConcurrentHashMap();
    private final DateTimeFieldType iFieldType;
    private final boolean iShort;
    
    TextField(DateTimeFieldType paramDateTimeFieldType, boolean paramBoolean)
    {
      this.iFieldType = paramDateTimeFieldType;
      this.iShort = paramBoolean;
    }
    
    private String print(long paramLong, Chronology paramChronology, Locale paramLocale)
    {
      paramChronology = this.iFieldType.getField(paramChronology);
      if (this.iShort) {}
      for (paramChronology = paramChronology.getAsShortText(paramLong, paramLocale);; paramChronology = paramChronology.getAsText(paramLong, paramLocale)) {
        return paramChronology;
      }
    }
    
    private String print(ReadablePartial paramReadablePartial, Locale paramLocale)
    {
      DateTimeField localDateTimeField;
      if (paramReadablePartial.isSupported(this.iFieldType))
      {
        localDateTimeField = this.iFieldType.getField(paramReadablePartial.getChronology());
        if (this.iShort) {
          paramReadablePartial = localDateTimeField.getAsShortText(paramReadablePartial, paramLocale);
        }
      }
      for (;;)
      {
        return paramReadablePartial;
        paramReadablePartial = localDateTimeField.getAsText(paramReadablePartial, paramLocale);
        continue;
        paramReadablePartial = "�";
      }
    }
    
    public int estimateParsedLength()
    {
      return estimatePrintedLength();
    }
    
    public int estimatePrintedLength()
    {
      if (this.iShort) {}
      for (int i = 6;; i = 20) {
        return i;
      }
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      Locale localLocale = paramDateTimeParserBucket.getLocale();
      Object localObject1 = (Map)cParseCache.get(localLocale);
      if (localObject1 == null)
      {
        localObject1 = new ConcurrentHashMap();
        cParseCache.put(localLocale, localObject1);
      }
      for (;;)
      {
        Object localObject2 = (Object[])((Map)localObject1).get(this.iFieldType);
        MutableDateTime.Property localProperty;
        int i;
        int k;
        if (localObject2 == null)
        {
          localObject2 = new ConcurrentHashMap(32);
          localProperty = new MutableDateTime(0L, DateTimeZone.UTC).property(this.iFieldType);
          i = localProperty.getMinimumValueOverall();
          k = localProperty.getMaximumValueOverall();
          if (k - i > 32) {
            i = paramInt ^ 0xFFFFFFFF;
          }
        }
        for (;;)
        {
          return i;
          int j = localProperty.getMaximumTextLength(localLocale);
          while (i <= k)
          {
            localProperty.set(i);
            ((Map)localObject2).put(localProperty.getAsShortText(localLocale), Boolean.TRUE);
            ((Map)localObject2).put(localProperty.getAsShortText(localLocale).toLowerCase(localLocale), Boolean.TRUE);
            ((Map)localObject2).put(localProperty.getAsShortText(localLocale).toUpperCase(localLocale), Boolean.TRUE);
            ((Map)localObject2).put(localProperty.getAsText(localLocale), Boolean.TRUE);
            ((Map)localObject2).put(localProperty.getAsText(localLocale).toLowerCase(localLocale), Boolean.TRUE);
            ((Map)localObject2).put(localProperty.getAsText(localLocale).toUpperCase(localLocale), Boolean.TRUE);
            i++;
          }
          i = j;
          if ("en".equals(localLocale.getLanguage()))
          {
            i = j;
            if (this.iFieldType == DateTimeFieldType.era())
            {
              ((Map)localObject2).put("BCE", Boolean.TRUE);
              ((Map)localObject2).put("bce", Boolean.TRUE);
              ((Map)localObject2).put("CE", Boolean.TRUE);
              ((Map)localObject2).put("ce", Boolean.TRUE);
              i = 3;
            }
          }
          ((Map)localObject1).put(this.iFieldType, new Object[] { localObject2, Integer.valueOf(i) });
          localObject1 = localObject2;
          label409:
          for (i = Math.min(paramCharSequence.length(), i + paramInt);; i--)
          {
            if (i <= paramInt) {
              break label501;
            }
            localObject2 = paramCharSequence.subSequence(paramInt, i).toString();
            if (((Map)localObject1).containsKey(localObject2))
            {
              paramDateTimeParserBucket.saveField(this.iFieldType, (String)localObject2, localLocale);
              break;
              localObject1 = (Map)localObject2[0];
              i = ((Integer)localObject2[1]).intValue();
              break label409;
            }
          }
          label501:
          i = paramInt ^ 0xFFFFFFFF;
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      try
      {
        paramAppendable.append(print(paramLong, paramChronology, paramLocale));
        return;
      }
      catch (RuntimeException paramChronology)
      {
        for (;;)
        {
          paramAppendable.append(65533);
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      try
      {
        paramAppendable.append(print(paramReadablePartial, paramLocale));
        return;
      }
      catch (RuntimeException paramReadablePartial)
      {
        for (;;)
        {
          paramAppendable.append(65533);
        }
      }
    }
  }
  
  static enum TimeZoneId
    implements InternalPrinter, InternalParser
  {
    private static final List<String> ALL_IDS;
    private static final List<String> BASE_GROUPED_IDS;
    private static final Map<String, List<String>> GROUPED_IDS;
    static final int MAX_LENGTH;
    static final int MAX_PREFIX_LENGTH;
    
    static
    {
      $VALUES = new TimeZoneId[] { INSTANCE };
      BASE_GROUPED_IDS = new ArrayList();
      ALL_IDS = new ArrayList(DateTimeZone.getAvailableIDs());
      Collections.sort(ALL_IDS);
      GROUPED_IDS = new HashMap();
      Iterator localIterator = ALL_IDS.iterator();
      int j = 0;
      int i = 0;
      if (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        int m = str1.indexOf('/');
        if (m >= 0)
        {
          int k = m;
          if (m < str1.length()) {
            k = m + 1;
          }
          j = Math.max(j, k);
          String str2 = str1.substring(0, k + 1);
          String str3 = str1.substring(k);
          if (!GROUPED_IDS.containsKey(str2)) {
            GROUPED_IDS.put(str2, new ArrayList());
          }
          ((List)GROUPED_IDS.get(str2)).add(str3);
        }
        for (;;)
        {
          i = Math.max(i, str1.length());
          break;
          BASE_GROUPED_IDS.add(str1);
        }
      }
      MAX_LENGTH = i;
      MAX_PREFIX_LENGTH = j;
    }
    
    private TimeZoneId() {}
    
    public int estimateParsedLength()
    {
      return MAX_LENGTH;
    }
    
    public int estimatePrintedLength()
    {
      return MAX_LENGTH;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      List localList = BASE_GROUPED_IDS;
      int k = paramCharSequence.length();
      int j = Math.min(k, MAX_PREFIX_LENGTH + paramInt);
      String str1 = "";
      Object localObject1;
      for (int i = paramInt;; i++)
      {
        if (i >= j) {
          break label294;
        }
        if (paramCharSequence.charAt(i) == '/')
        {
          str1 = paramCharSequence.subSequence(paramInt, i + 1).toString();
          j = paramInt + str1.length();
          if (i >= k) {
            break label287;
          }
          localObject1 = str1 + paramCharSequence.charAt(i + 1);
          localList = (List)GROUPED_IDS.get(localObject1);
          if (localList != null) {
            break;
          }
          paramInt ^= 0xFFFFFFFF;
          return paramInt;
        }
      }
      label156:
      label280:
      label287:
      label294:
      for (i = j;; i = paramInt)
      {
        Object localObject2 = null;
        j = 0;
        String str2;
        if (j < localList.size())
        {
          str2 = (String)localList.get(j);
          if (!DateTimeFormatterBuilder.csStartsWith(paramCharSequence, i, str2)) {
            break label280;
          }
          localObject1 = str2;
          if (localObject2 != null) {
            if (str2.length() <= ((String)localObject2).length()) {
              break label280;
            }
          }
        }
        for (localObject1 = str2;; localObject1 = localObject2)
        {
          j++;
          localObject2 = localObject1;
          break label156;
          if (localObject2 != null)
          {
            paramDateTimeParserBucket.setZone(DateTimeZone.forID(str1 + (String)localObject2));
            paramInt = ((String)localObject2).length() + i;
            break;
          }
          paramInt ^= 0xFFFFFFFF;
          break;
        }
        localObject1 = str1;
        break;
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      if (paramDateTimeZone != null) {}
      for (paramChronology = paramDateTimeZone.getID();; paramChronology = "")
      {
        paramAppendable.append(paramChronology);
        return;
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {}
  }
  
  static class TimeZoneName
    implements InternalPrinter, InternalParser
  {
    static final int LONG_NAME = 0;
    static final int SHORT_NAME = 1;
    private final Map<String, DateTimeZone> iParseLookup;
    private final int iType;
    
    TimeZoneName(int paramInt, Map<String, DateTimeZone> paramMap)
    {
      this.iType = paramInt;
      this.iParseLookup = paramMap;
    }
    
    private String print(long paramLong, DateTimeZone paramDateTimeZone, Locale paramLocale)
    {
      if (paramDateTimeZone == null) {
        paramDateTimeZone = "";
      }
      for (;;)
      {
        return paramDateTimeZone;
        switch (this.iType)
        {
        default: 
          paramDateTimeZone = "";
          break;
        case 0: 
          paramDateTimeZone = paramDateTimeZone.getName(paramLong, paramLocale);
          break;
        case 1: 
          paramDateTimeZone = paramDateTimeZone.getShortName(paramLong, paramLocale);
        }
      }
    }
    
    public int estimateParsedLength()
    {
      if (this.iType == 1) {}
      for (int i = 4;; i = 20) {
        return i;
      }
    }
    
    public int estimatePrintedLength()
    {
      if (this.iType == 1) {}
      for (int i = 4;; i = 20) {
        return i;
      }
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      Map localMap = this.iParseLookup;
      Object localObject1;
      label28:
      String str;
      if (localMap != null)
      {
        localObject1 = null;
        Iterator localIterator = localMap.keySet().iterator();
        if (!localIterator.hasNext()) {
          break label101;
        }
        str = (String)localIterator.next();
        if (!DateTimeFormatterBuilder.csStartsWith(paramCharSequence, paramInt, str)) {
          break label139;
        }
        localObject2 = str;
        if (localObject1 != null) {
          if (str.length() <= ((String)localObject1).length()) {
            break label139;
          }
        }
      }
      label101:
      label139:
      for (Object localObject2 = str;; localObject2 = localObject1)
      {
        localObject1 = localObject2;
        break label28;
        localMap = DateTimeUtils.getDefaultTimeZoneNames();
        break;
        if (localObject1 != null)
        {
          paramDateTimeParserBucket.setZone((DateTimeZone)localMap.get(localObject1));
          paramInt = ((String)localObject1).length() + paramInt;
        }
        for (;;)
        {
          return paramInt;
          paramInt ^= 0xFFFFFFFF;
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      paramAppendable.append(print(paramLong - paramInt, paramDateTimeZone, paramLocale));
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {}
  }
  
  static class TimeZoneOffset
    implements InternalPrinter, InternalParser
  {
    private final int iMaxFields;
    private final int iMinFields;
    private final boolean iShowSeparators;
    private final String iZeroOffsetParseText;
    private final String iZeroOffsetPrintText;
    
    TimeZoneOffset(String paramString1, String paramString2, boolean paramBoolean, int paramInt1, int paramInt2)
    {
      this.iZeroOffsetPrintText = paramString1;
      this.iZeroOffsetParseText = paramString2;
      this.iShowSeparators = paramBoolean;
      if ((paramInt1 <= 0) || (paramInt2 < paramInt1)) {
        throw new IllegalArgumentException();
      }
      if (paramInt1 > 4)
      {
        paramInt2 = 4;
        paramInt1 = i;
      }
      for (;;)
      {
        this.iMinFields = paramInt1;
        this.iMaxFields = paramInt2;
        return;
      }
    }
    
    private int digitCount(CharSequence paramCharSequence, int paramInt1, int paramInt2)
    {
      paramInt2 = Math.min(paramCharSequence.length() - paramInt1, paramInt2);
      int i = 0;
      for (;;)
      {
        if (paramInt2 > 0)
        {
          int j = paramCharSequence.charAt(paramInt1 + i);
          if ((j >= 48) && (j <= 57)) {}
        }
        else
        {
          return i;
        }
        i++;
        paramInt2--;
      }
    }
    
    public int estimateParsedLength()
    {
      return estimatePrintedLength();
    }
    
    public int estimatePrintedLength()
    {
      int j = this.iMinFields + 1 << 1;
      int i = j;
      if (this.iShowSeparators) {
        i = j + (this.iMinFields - 1);
      }
      j = i;
      if (this.iZeroOffsetPrintText != null)
      {
        j = i;
        if (this.iZeroOffsetPrintText.length() > i) {
          j = this.iZeroOffsetPrintText.length();
        }
      }
      return j;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      int m = 0;
      int i = paramCharSequence.length() - paramInt;
      int j;
      if (this.iZeroOffsetParseText != null)
      {
        if (this.iZeroOffsetParseText.length() != 0) {
          break label81;
        }
        if (i > 0)
        {
          j = paramCharSequence.charAt(paramInt);
          if ((j != 45) && (j != 43)) {}
        }
      }
      else
      {
        if (i > 1) {
          break label114;
        }
        paramInt ^= 0xFFFFFFFF;
      }
      label81:
      label114:
      int k;
      for (;;)
      {
        return paramInt;
        paramDateTimeParserBucket.setOffset(Integer.valueOf(0));
        continue;
        if (!DateTimeFormatterBuilder.csStartsWithIgnoreCase(paramCharSequence, paramInt, this.iZeroOffsetParseText)) {
          break;
        }
        paramDateTimeParserBucket.setOffset(Integer.valueOf(0));
        paramInt += this.iZeroOffsetParseText.length();
        continue;
        j = paramCharSequence.charAt(paramInt);
        if (j == 45) {}
        for (k = 1;; k = 0)
        {
          j = paramInt + 1;
          if (digitCount(paramCharSequence, j, 2) >= 2) {
            break label178;
          }
          paramInt = j ^ 0xFFFFFFFF;
          break;
          if (j != 43) {
            break label171;
          }
        }
        label171:
        paramInt ^= 0xFFFFFFFF;
        continue;
        label178:
        paramInt = FormatUtils.parseTwoDigits(paramCharSequence, j);
        if (paramInt <= 23) {
          break label199;
        }
        paramInt = j ^ 0xFFFFFFFF;
      }
      label199:
      paramInt *= 3600000;
      int n = i - 1 - 2;
      j += 2;
      if (n <= 0) {
        i = j;
      }
      for (;;)
      {
        label224:
        if (k != 0) {
          paramInt = -paramInt;
        }
        for (;;)
        {
          paramDateTimeParserBucket.setOffset(Integer.valueOf(paramInt));
          paramInt = i;
          break;
          int i1 = paramCharSequence.charAt(j);
          if (i1 == 58)
          {
            i = j + 1;
            n--;
            m = 1;
          }
          do
          {
            j = digitCount(paramCharSequence, i, 2);
            if ((j != 0) || (m != 0)) {
              break label323;
            }
            break;
            if (i1 < 48) {
              break label316;
            }
            i = j;
          } while (i1 <= 57);
          label316:
          i = j;
          break label224;
          label323:
          if (j < 2)
          {
            paramInt = i ^ 0xFFFFFFFF;
            break;
          }
          j = FormatUtils.parseTwoDigits(paramCharSequence, i);
          if (j > 59)
          {
            paramInt = i ^ 0xFFFFFFFF;
            break;
          }
          paramInt += j * 60000;
          i1 = n - 2;
          j = i + 2;
          if (i1 <= 0)
          {
            i = j;
            break label224;
          }
          n = i1;
          i = j;
          if (m != 0)
          {
            if (paramCharSequence.charAt(j) != ':')
            {
              i = j;
              break label224;
            }
            n = i1 - 1;
            i = j + 1;
          }
          j = digitCount(paramCharSequence, i, 2);
          if ((j == 0) && (m == 0)) {
            break label224;
          }
          if (j < 2)
          {
            paramInt = i ^ 0xFFFFFFFF;
            break;
          }
          j = FormatUtils.parseTwoDigits(paramCharSequence, i);
          if (j > 59)
          {
            paramInt = i ^ 0xFFFFFFFF;
            break;
          }
          paramInt += j * 1000;
          j = n - 2;
          i += 2;
          if (j <= 0) {
            break label224;
          }
          j = i;
          if (m != 0)
          {
            if ((paramCharSequence.charAt(i) != '.') && (paramCharSequence.charAt(i) != ',')) {
              break label224;
            }
            j = i + 1;
          }
          n = digitCount(paramCharSequence, j, 3);
          if ((n == 0) && (m == 0))
          {
            i = j;
            break label224;
          }
          if (n < 1)
          {
            paramInt = j ^ 0xFFFFFFFF;
            break;
          }
          i = j + 1;
          j = (paramCharSequence.charAt(j) - '0') * 100 + paramInt;
          if (n <= 1) {
            break label706;
          }
          paramInt = i + 1;
          j = (paramCharSequence.charAt(i) - '0') * 10 + j;
          if (n <= 2) {
            break label697;
          }
          j += paramCharSequence.charAt(paramInt) - '0';
          i = paramInt + 1;
          paramInt = j;
          break label224;
        }
        label697:
        i = paramInt;
        paramInt = j;
        continue;
        label706:
        paramInt = j;
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      if (paramDateTimeZone == null) {}
      for (;;)
      {
        return;
        if ((paramInt != 0) || (this.iZeroOffsetPrintText == null)) {
          break;
        }
        paramAppendable.append(this.iZeroOffsetPrintText);
      }
      if (paramInt >= 0) {
        paramAppendable.append('+');
      }
      for (;;)
      {
        int i = paramInt / 3600000;
        FormatUtils.appendPaddedInteger(paramAppendable, i, 2);
        if (this.iMaxFields == 1) {
          break;
        }
        i = paramInt - i * 3600000;
        if ((i == 0) && (this.iMinFields <= 1)) {
          break;
        }
        paramInt = i / 60000;
        if (this.iShowSeparators) {
          paramAppendable.append(':');
        }
        FormatUtils.appendPaddedInteger(paramAppendable, paramInt, 2);
        if (this.iMaxFields == 2) {
          break;
        }
        i -= paramInt * 60000;
        if ((i == 0) && (this.iMinFields <= 2)) {
          break;
        }
        paramInt = i / 1000;
        if (this.iShowSeparators) {
          paramAppendable.append(':');
        }
        FormatUtils.appendPaddedInteger(paramAppendable, paramInt, 2);
        if (this.iMaxFields == 3) {
          break;
        }
        paramInt = i - paramInt * 1000;
        if ((paramInt == 0) && (this.iMinFields <= 3)) {
          break;
        }
        if (this.iShowSeparators) {
          paramAppendable.append('.');
        }
        FormatUtils.appendPaddedInteger(paramAppendable, paramInt, 3);
        break;
        paramAppendable.append('-');
        paramInt = -paramInt;
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {}
  }
  
  static class TwoDigitYear
    implements InternalPrinter, InternalParser
  {
    private final boolean iLenientParse;
    private final int iPivot;
    private final DateTimeFieldType iType;
    
    TwoDigitYear(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
    {
      this.iType = paramDateTimeFieldType;
      this.iPivot = paramInt;
      this.iLenientParse = paramBoolean;
    }
    
    private int getTwoDigitYear(long paramLong, Chronology paramChronology)
    {
      try
      {
        int j = this.iType.getField(paramChronology).get(paramLong);
        i = j;
        if (j < 0) {
          i = -j;
        }
        i %= 100;
      }
      catch (RuntimeException paramChronology)
      {
        for (;;)
        {
          int i = -1;
        }
      }
      return i;
    }
    
    private int getTwoDigitYear(ReadablePartial paramReadablePartial)
    {
      if (paramReadablePartial.isSupported(this.iType)) {}
      for (;;)
      {
        try
        {
          int j = paramReadablePartial.get(this.iType);
          i = j;
          if (j < 0) {
            i = -j;
          }
          i %= 100;
          return i;
        }
        catch (RuntimeException paramReadablePartial) {}
        int i = -1;
      }
    }
    
    public int estimateParsedLength()
    {
      if (this.iLenientParse) {}
      for (int i = 4;; i = 2) {
        return i;
      }
    }
    
    public int estimatePrintedLength()
    {
      return 2;
    }
    
    public int parseInto(DateTimeParserBucket paramDateTimeParserBucket, CharSequence paramCharSequence, int paramInt)
    {
      int n = 0;
      int m = paramCharSequence.length() - paramInt;
      int j;
      int i;
      if (!this.iLenientParse)
      {
        k = paramInt;
        if (Math.min(2, m) < 2) {
          paramInt ^= 0xFFFFFFFF;
        }
      }
      else
      {
        for (;;)
        {
          return paramInt;
          j = 0;
          i = 0;
          k = 0;
          for (;;)
          {
            if (j < m)
            {
              int i1 = paramCharSequence.charAt(paramInt + j);
              if ((j == 0) && ((i1 == 45) || (i1 == 43)))
              {
                if (i1 == 45) {}
                for (i = 1;; i = 0)
                {
                  if (i == 0) {
                    break label116;
                  }
                  j++;
                  k = 1;
                  break;
                }
                label116:
                paramInt++;
                k = 1;
                m--;
                continue;
              }
              if ((i1 >= 48) && (i1 <= 57)) {}
            }
            else
            {
              if (j != 0) {
                break label160;
              }
              paramInt ^= 0xFFFFFFFF;
              break;
            }
            j++;
          }
          label160:
          if (k == 0)
          {
            k = paramInt;
            if (j == 2) {
              break label322;
            }
          }
          if (j < 9) {
            break;
          }
          j = paramInt + j;
          k = Integer.parseInt(paramCharSequence.subSequence(paramInt, j).toString());
          label204:
          paramDateTimeParserBucket.saveField(this.iType, k);
          paramInt = j;
        }
        if (i == 0) {
          break label495;
        }
      }
      label322:
      label495:
      for (int k = paramInt + 1;; k = paramInt)
      {
        try
        {
          n = paramCharSequence.charAt(k);
          m = paramInt + j;
          paramInt = n - 48;
          for (j = k + 1; j < m; j++) {
            paramInt = paramCharSequence.charAt(j) + ((paramInt << 3) + (paramInt << 1)) - 48;
          }
        }
        catch (StringIndexOutOfBoundsException paramDateTimeParserBucket)
        {
          paramInt ^= 0xFFFFFFFF;
        }
        k = paramInt;
        j = m;
        if (i == 0) {
          break label204;
        }
        k = -paramInt;
        j = m;
        break label204;
        paramInt = paramCharSequence.charAt(k);
        if ((paramInt < 48) || (paramInt > 57))
        {
          paramInt = k ^ 0xFFFFFFFF;
          break;
        }
        i = paramInt - 48;
        paramInt = paramCharSequence.charAt(k + 1);
        if ((paramInt < 48) || (paramInt > 57))
        {
          paramInt = k ^ 0xFFFFFFFF;
          break;
        }
        j = (i << 1) + (i << 3) + paramInt - 48;
        paramInt = this.iPivot;
        if (paramDateTimeParserBucket.getPivotYear() != null) {
          paramInt = paramDateTimeParserBucket.getPivotYear().intValue();
        }
        m = paramInt - 50;
        if (m >= 0) {}
        for (paramInt = m % 100;; paramInt = (m + 1) % 100 + 99)
        {
          i = n;
          if (j < paramInt) {
            i = 100;
          }
          paramDateTimeParserBucket.saveField(this.iType, i + m - paramInt + j);
          paramInt = k + 2;
          break;
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      paramInt = getTwoDigitYear(paramLong, paramChronology);
      if (paramInt < 0)
      {
        paramAppendable.append(65533);
        paramAppendable.append(65533);
      }
      for (;;)
      {
        return;
        FormatUtils.appendPaddedInteger(paramAppendable, paramInt, 2);
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      int i = getTwoDigitYear(paramReadablePartial);
      if (i < 0)
      {
        paramAppendable.append(65533);
        paramAppendable.append(65533);
      }
      for (;;)
      {
        return;
        FormatUtils.appendPaddedInteger(paramAppendable, i, 2);
      }
    }
  }
  
  static class UnpaddedNumber
    extends DateTimeFormatterBuilder.NumberFormatter
  {
    protected UnpaddedNumber(DateTimeFieldType paramDateTimeFieldType, int paramInt, boolean paramBoolean)
    {
      super(paramInt, paramBoolean);
    }
    
    public int estimatePrintedLength()
    {
      return this.iMaxParsedDigits;
    }
    
    public void printTo(Appendable paramAppendable, long paramLong, Chronology paramChronology, int paramInt, DateTimeZone paramDateTimeZone, Locale paramLocale)
      throws IOException
    {
      try
      {
        FormatUtils.appendUnpaddedInteger(paramAppendable, this.iFieldType.getField(paramChronology).get(paramLong));
        return;
      }
      catch (RuntimeException paramChronology)
      {
        for (;;)
        {
          paramAppendable.append(65533);
        }
      }
    }
    
    public void printTo(Appendable paramAppendable, ReadablePartial paramReadablePartial, Locale paramLocale)
      throws IOException
    {
      if (paramReadablePartial.isSupported(this.iFieldType)) {}
      for (;;)
      {
        try
        {
          FormatUtils.appendUnpaddedInteger(paramAppendable, paramReadablePartial.get(this.iFieldType));
          return;
        }
        catch (RuntimeException paramReadablePartial)
        {
          paramAppendable.append(65533);
          continue;
        }
        paramAppendable.append(65533);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\DateTimeFormatterBuilder.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */