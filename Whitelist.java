package org.jsoup.safety;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;

public class Whitelist
{
  private Map<TagName, Set<AttributeKey>> attributes = new HashMap();
  private Map<TagName, Map<AttributeKey, AttributeValue>> enforcedAttributes = new HashMap();
  private boolean preserveRelativeLinks = false;
  private Map<TagName, Map<AttributeKey, Set<Protocol>>> protocols = new HashMap();
  private Set<TagName> tagNames = new HashSet();
  
  public static Whitelist basic()
  {
    return new Whitelist().addTags(new String[] { "a", "b", "blockquote", "br", "cite", "code", "dd", "dl", "dt", "em", "i", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub", "sup", "u", "ul" }).addAttributes("a", new String[] { "href" }).addAttributes("blockquote", new String[] { "cite" }).addAttributes("q", new String[] { "cite" }).addProtocols("a", "href", new String[] { "ftp", "http", "https", "mailto" }).addProtocols("blockquote", "cite", new String[] { "http", "https" }).addProtocols("cite", "cite", new String[] { "http", "https" }).addEnforcedAttribute("a", "rel", "nofollow");
  }
  
  public static Whitelist basicWithImages()
  {
    return basic().addTags(new String[] { "img" }).addAttributes("img", new String[] { "align", "alt", "height", "src", "title", "width" }).addProtocols("img", "src", new String[] { "http", "https" });
  }
  
  private boolean isValidAnchor(String paramString)
  {
    if ((paramString.startsWith("#")) && (!paramString.matches(".*\\s.*"))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static Whitelist none()
  {
    return new Whitelist();
  }
  
  public static Whitelist relaxed()
  {
    return new Whitelist().addTags(new String[] { "a", "b", "blockquote", "br", "caption", "cite", "code", "col", "colgroup", "dd", "div", "dl", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "i", "img", "li", "ol", "p", "pre", "q", "small", "span", "strike", "strong", "sub", "sup", "table", "tbody", "td", "tfoot", "th", "thead", "tr", "u", "ul" }).addAttributes("a", new String[] { "href", "title" }).addAttributes("blockquote", new String[] { "cite" }).addAttributes("col", new String[] { "span", "width" }).addAttributes("colgroup", new String[] { "span", "width" }).addAttributes("img", new String[] { "align", "alt", "height", "src", "title", "width" }).addAttributes("ol", new String[] { "start", "type" }).addAttributes("q", new String[] { "cite" }).addAttributes("table", new String[] { "summary", "width" }).addAttributes("td", new String[] { "abbr", "axis", "colspan", "rowspan", "width" }).addAttributes("th", new String[] { "abbr", "axis", "colspan", "rowspan", "scope", "width" }).addAttributes("ul", new String[] { "type" }).addProtocols("a", "href", new String[] { "ftp", "http", "https", "mailto" }).addProtocols("blockquote", "cite", new String[] { "http", "https" }).addProtocols("cite", "cite", new String[] { "http", "https" }).addProtocols("img", "src", new String[] { "http", "https" }).addProtocols("q", "cite", new String[] { "http", "https" });
  }
  
  public static Whitelist simpleText()
  {
    return new Whitelist().addTags(new String[] { "b", "em", "i", "strong", "u" });
  }
  
  private boolean testValidProtocol(Element paramElement, Attribute paramAttribute, Set<Protocol> paramSet)
  {
    boolean bool = true;
    String str = paramElement.absUrl(paramAttribute.getKey());
    paramElement = str;
    if (str.length() == 0) {
      paramElement = paramAttribute.getValue();
    }
    if (!this.preserveRelativeLinks) {
      paramAttribute.setValue(paramElement);
    }
    paramAttribute = paramSet.iterator();
    do
    {
      if (!paramAttribute.hasNext()) {
        break label126;
      }
      paramSet = ((Protocol)paramAttribute.next()).toString();
      if (!paramSet.equals("#")) {
        break;
      }
    } while (!isValidAnchor(paramElement));
    for (;;)
    {
      return bool;
      paramSet = paramSet + ":";
      if (!paramElement.toLowerCase().startsWith(paramSet)) {
        break;
      }
      continue;
      label126:
      bool = false;
    }
  }
  
  public Whitelist addAttributes(String paramString, String... paramVarArgs)
  {
    int i = 0;
    Validate.notEmpty(paramString);
    Validate.notNull(paramVarArgs);
    if (paramVarArgs.length > 0) {}
    HashSet localHashSet;
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool, "No attribute names supplied.");
      paramString = TagName.valueOf(paramString);
      if (!this.tagNames.contains(paramString)) {
        this.tagNames.add(paramString);
      }
      localHashSet = new HashSet();
      int j = paramVarArgs.length;
      while (i < j)
      {
        String str = paramVarArgs[i];
        Validate.notEmpty(str);
        localHashSet.add(AttributeKey.valueOf(str));
        i++;
      }
    }
    if (this.attributes.containsKey(paramString)) {
      ((Set)this.attributes.get(paramString)).addAll(localHashSet);
    }
    for (;;)
    {
      return this;
      this.attributes.put(paramString, localHashSet);
    }
  }
  
  public Whitelist addEnforcedAttribute(String paramString1, String paramString2, String paramString3)
  {
    Validate.notEmpty(paramString1);
    Validate.notEmpty(paramString2);
    Validate.notEmpty(paramString3);
    paramString1 = TagName.valueOf(paramString1);
    if (!this.tagNames.contains(paramString1)) {
      this.tagNames.add(paramString1);
    }
    paramString2 = AttributeKey.valueOf(paramString2);
    AttributeValue localAttributeValue = AttributeValue.valueOf(paramString3);
    if (this.enforcedAttributes.containsKey(paramString1)) {
      ((Map)this.enforcedAttributes.get(paramString1)).put(paramString2, localAttributeValue);
    }
    for (;;)
    {
      return this;
      paramString3 = new HashMap();
      paramString3.put(paramString2, localAttributeValue);
      this.enforcedAttributes.put(paramString1, paramString3);
    }
  }
  
  public Whitelist addProtocols(String paramString1, String paramString2, String... paramVarArgs)
  {
    Validate.notEmpty(paramString1);
    Validate.notEmpty(paramString2);
    Validate.notNull(paramVarArgs);
    TagName localTagName = TagName.valueOf(paramString1);
    AttributeKey localAttributeKey = AttributeKey.valueOf(paramString2);
    if (this.protocols.containsKey(localTagName))
    {
      paramString1 = (Map)this.protocols.get(localTagName);
      if (!paramString1.containsKey(localAttributeKey)) {
        break label140;
      }
    }
    for (paramString1 = (Set)paramString1.get(localAttributeKey);; paramString1 = paramString2)
    {
      int j = paramVarArgs.length;
      for (int i = 0; i < j; i++)
      {
        paramString2 = paramVarArgs[i];
        Validate.notEmpty(paramString2);
        paramString1.add(Protocol.valueOf(paramString2));
      }
      paramString1 = new HashMap();
      this.protocols.put(localTagName, paramString1);
      break;
      label140:
      paramString2 = new HashSet();
      paramString1.put(localAttributeKey, paramString2);
    }
    return this;
  }
  
  public Whitelist addTags(String... paramVarArgs)
  {
    Validate.notNull(paramVarArgs);
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      String str = paramVarArgs[i];
      Validate.notEmpty(str);
      this.tagNames.add(TagName.valueOf(str));
    }
    return this;
  }
  
  Attributes getEnforcedAttributes(String paramString)
  {
    Attributes localAttributes = new Attributes();
    paramString = TagName.valueOf(paramString);
    if (this.enforcedAttributes.containsKey(paramString))
    {
      Iterator localIterator = ((Map)this.enforcedAttributes.get(paramString)).entrySet().iterator();
      while (localIterator.hasNext())
      {
        paramString = (Map.Entry)localIterator.next();
        localAttributes.put(((AttributeKey)paramString.getKey()).toString(), ((AttributeValue)paramString.getValue()).toString());
      }
    }
    return localAttributes;
  }
  
  protected boolean isSafeAttribute(String paramString, Element paramElement, Attribute paramAttribute)
  {
    boolean bool = true;
    TagName localTagName = TagName.valueOf(paramString);
    Object localObject1 = AttributeKey.valueOf(paramAttribute.getKey());
    Object localObject2 = (Set)this.attributes.get(localTagName);
    if ((localObject2 != null) && (((Set)localObject2).contains(localObject1))) {
      if (this.protocols.containsKey(localTagName))
      {
        paramString = (Map)this.protocols.get(localTagName);
        if ((paramString.containsKey(localObject1)) && (!testValidProtocol(paramElement, paramAttribute, (Set)paramString.get(localObject1)))) {
          break label117;
        }
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      label117:
      bool = false;
      continue;
      if ((Map)this.enforcedAttributes.get(localTagName) != null)
      {
        localObject2 = getEnforcedAttributes(paramString);
        localObject1 = paramAttribute.getKey();
        if (((Attributes)localObject2).hasKeyIgnoreCase((String)localObject1))
        {
          bool = ((Attributes)localObject2).getIgnoreCase((String)localObject1).equals(paramAttribute.getValue());
          continue;
        }
      }
      if ((paramString.equals(":all")) || (!isSafeAttribute(":all", paramElement, paramAttribute))) {
        bool = false;
      }
    }
  }
  
  protected boolean isSafeTag(String paramString)
  {
    return this.tagNames.contains(TagName.valueOf(paramString));
  }
  
  public Whitelist preserveRelativeLinks(boolean paramBoolean)
  {
    this.preserveRelativeLinks = paramBoolean;
    return this;
  }
  
  public Whitelist removeAttributes(String paramString, String... paramVarArgs)
  {
    int i = 0;
    Validate.notEmpty(paramString);
    Validate.notNull(paramVarArgs);
    if (paramVarArgs.length > 0) {}
    TagName localTagName;
    HashSet localHashSet;
    for (boolean bool = true;; bool = false)
    {
      Validate.isTrue(bool, "No attribute names supplied.");
      localTagName = TagName.valueOf(paramString);
      localHashSet = new HashSet();
      int j = paramVarArgs.length;
      while (i < j)
      {
        String str = paramVarArgs[i];
        Validate.notEmpty(str);
        localHashSet.add(AttributeKey.valueOf(str));
        i++;
      }
    }
    if ((this.tagNames.contains(localTagName)) && (this.attributes.containsKey(localTagName)))
    {
      paramVarArgs = (Set)this.attributes.get(localTagName);
      paramVarArgs.removeAll(localHashSet);
      if (paramVarArgs.isEmpty()) {
        this.attributes.remove(localTagName);
      }
    }
    if (paramString.equals(":all"))
    {
      paramVarArgs = this.attributes.keySet().iterator();
      while (paramVarArgs.hasNext())
      {
        localTagName = (TagName)paramVarArgs.next();
        paramString = (Set)this.attributes.get(localTagName);
        paramString.removeAll(localHashSet);
        if (paramString.isEmpty()) {
          this.attributes.remove(localTagName);
        }
      }
    }
    return this;
  }
  
  public Whitelist removeEnforcedAttribute(String paramString1, String paramString2)
  {
    Validate.notEmpty(paramString1);
    Validate.notEmpty(paramString2);
    paramString1 = TagName.valueOf(paramString1);
    if ((this.tagNames.contains(paramString1)) && (this.enforcedAttributes.containsKey(paramString1)))
    {
      AttributeKey localAttributeKey = AttributeKey.valueOf(paramString2);
      paramString2 = (Map)this.enforcedAttributes.get(paramString1);
      paramString2.remove(localAttributeKey);
      if (paramString2.isEmpty()) {
        this.enforcedAttributes.remove(paramString1);
      }
    }
    return this;
  }
  
  public Whitelist removeProtocols(String paramString1, String paramString2, String... paramVarArgs)
  {
    Validate.notEmpty(paramString1);
    Validate.notEmpty(paramString2);
    Validate.notNull(paramVarArgs);
    paramString1 = TagName.valueOf(paramString1);
    AttributeKey localAttributeKey = AttributeKey.valueOf(paramString2);
    Validate.isTrue(this.protocols.containsKey(paramString1), "Cannot remove a protocol that is not set.");
    Map localMap = (Map)this.protocols.get(paramString1);
    Validate.isTrue(localMap.containsKey(localAttributeKey), "Cannot remove a protocol that is not set.");
    paramString2 = (Set)localMap.get(localAttributeKey);
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      String str = paramVarArgs[i];
      Validate.notEmpty(str);
      paramString2.remove(Protocol.valueOf(str));
    }
    if (paramString2.isEmpty())
    {
      localMap.remove(localAttributeKey);
      if (localMap.isEmpty()) {
        this.protocols.remove(paramString1);
      }
    }
    return this;
  }
  
  public Whitelist removeTags(String... paramVarArgs)
  {
    Validate.notNull(paramVarArgs);
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      Object localObject = paramVarArgs[i];
      Validate.notEmpty((String)localObject);
      localObject = TagName.valueOf((String)localObject);
      if (this.tagNames.remove(localObject))
      {
        this.attributes.remove(localObject);
        this.enforcedAttributes.remove(localObject);
        this.protocols.remove(localObject);
      }
    }
    return this;
  }
  
  static class AttributeKey
    extends Whitelist.TypedValue
  {
    AttributeKey(String paramString)
    {
      super();
    }
    
    static AttributeKey valueOf(String paramString)
    {
      return new AttributeKey(paramString);
    }
  }
  
  static class AttributeValue
    extends Whitelist.TypedValue
  {
    AttributeValue(String paramString)
    {
      super();
    }
    
    static AttributeValue valueOf(String paramString)
    {
      return new AttributeValue(paramString);
    }
  }
  
  static class Protocol
    extends Whitelist.TypedValue
  {
    Protocol(String paramString)
    {
      super();
    }
    
    static Protocol valueOf(String paramString)
    {
      return new Protocol(paramString);
    }
  }
  
  static class TagName
    extends Whitelist.TypedValue
  {
    TagName(String paramString)
    {
      super();
    }
    
    static TagName valueOf(String paramString)
    {
      return new TagName(paramString);
    }
  }
  
  static abstract class TypedValue
  {
    private String value;
    
    TypedValue(String paramString)
    {
      Validate.notNull(paramString);
      this.value = paramString;
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool = true;
      if (this == paramObject) {}
      for (;;)
      {
        return bool;
        if (paramObject == null)
        {
          bool = false;
        }
        else if (getClass() != paramObject.getClass())
        {
          bool = false;
        }
        else
        {
          paramObject = (TypedValue)paramObject;
          if (this.value == null)
          {
            if (((TypedValue)paramObject).value != null) {
              bool = false;
            }
          }
          else if (!this.value.equals(((TypedValue)paramObject).value)) {
            bool = false;
          }
        }
      }
    }
    
    public int hashCode()
    {
      if (this.value == null) {}
      for (int i = 0;; i = this.value.hashCode()) {
        return i + 31;
      }
    }
    
    public String toString()
    {
      return this.value;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\safety\Whitelist.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */