package org.joda.time.format;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import org.joda.time.MutablePeriod;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadablePeriod;

public class PeriodFormatter
{
  private final Locale iLocale;
  private final PeriodType iParseType;
  private final PeriodParser iParser;
  private final PeriodPrinter iPrinter;
  
  public PeriodFormatter(PeriodPrinter paramPeriodPrinter, PeriodParser paramPeriodParser)
  {
    this.iPrinter = paramPeriodPrinter;
    this.iParser = paramPeriodParser;
    this.iLocale = null;
    this.iParseType = null;
  }
  
  PeriodFormatter(PeriodPrinter paramPeriodPrinter, PeriodParser paramPeriodParser, Locale paramLocale, PeriodType paramPeriodType)
  {
    this.iPrinter = paramPeriodPrinter;
    this.iParser = paramPeriodParser;
    this.iLocale = paramLocale;
    this.iParseType = paramPeriodType;
  }
  
  private void checkParser()
  {
    if (this.iParser == null) {
      throw new UnsupportedOperationException("Parsing not supported");
    }
  }
  
  private void checkPeriod(ReadablePeriod paramReadablePeriod)
  {
    if (paramReadablePeriod == null) {
      throw new IllegalArgumentException("Period must not be null");
    }
  }
  
  private void checkPrinter()
  {
    if (this.iPrinter == null) {
      throw new UnsupportedOperationException("Printing not supported");
    }
  }
  
  public Locale getLocale()
  {
    return this.iLocale;
  }
  
  public PeriodType getParseType()
  {
    return this.iParseType;
  }
  
  public PeriodParser getParser()
  {
    return this.iParser;
  }
  
  public PeriodPrinter getPrinter()
  {
    return this.iPrinter;
  }
  
  public boolean isParser()
  {
    if (this.iParser != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isPrinter()
  {
    if (this.iPrinter != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public int parseInto(ReadWritablePeriod paramReadWritablePeriod, String paramString, int paramInt)
  {
    checkParser();
    checkPeriod(paramReadWritablePeriod);
    return getParser().parseInto(paramReadWritablePeriod, paramString, paramInt, this.iLocale);
  }
  
  public MutablePeriod parseMutablePeriod(String paramString)
  {
    checkParser();
    MutablePeriod localMutablePeriod = new MutablePeriod(0L, this.iParseType);
    int j = getParser().parseInto(localMutablePeriod, paramString, 0, this.iLocale);
    int i;
    if (j >= 0)
    {
      i = j;
      if (j >= paramString.length()) {
        return localMutablePeriod;
      }
    }
    else
    {
      i = j ^ 0xFFFFFFFF;
    }
    throw new IllegalArgumentException(FormatUtils.createErrorMessage(paramString, i));
  }
  
  public Period parsePeriod(String paramString)
  {
    checkParser();
    return parseMutablePeriod(paramString).toPeriod();
  }
  
  public String print(ReadablePeriod paramReadablePeriod)
  {
    checkPrinter();
    checkPeriod(paramReadablePeriod);
    PeriodPrinter localPeriodPrinter = getPrinter();
    StringBuffer localStringBuffer = new StringBuffer(localPeriodPrinter.calculatePrintedLength(paramReadablePeriod, this.iLocale));
    localPeriodPrinter.printTo(localStringBuffer, paramReadablePeriod, this.iLocale);
    return localStringBuffer.toString();
  }
  
  public void printTo(Writer paramWriter, ReadablePeriod paramReadablePeriod)
    throws IOException
  {
    checkPrinter();
    checkPeriod(paramReadablePeriod);
    getPrinter().printTo(paramWriter, paramReadablePeriod, this.iLocale);
  }
  
  public void printTo(StringBuffer paramStringBuffer, ReadablePeriod paramReadablePeriod)
  {
    checkPrinter();
    checkPeriod(paramReadablePeriod);
    getPrinter().printTo(paramStringBuffer, paramReadablePeriod, this.iLocale);
  }
  
  public PeriodFormatter withLocale(Locale paramLocale)
  {
    PeriodFormatter localPeriodFormatter = this;
    if (paramLocale != getLocale()) {
      if ((paramLocale == null) || (!paramLocale.equals(getLocale()))) {
        break label29;
      }
    }
    label29:
    for (localPeriodFormatter = this;; localPeriodFormatter = new PeriodFormatter(this.iPrinter, this.iParser, paramLocale, this.iParseType)) {
      return localPeriodFormatter;
    }
  }
  
  public PeriodFormatter withParseType(PeriodType paramPeriodType)
  {
    if (paramPeriodType == this.iParseType) {}
    for (paramPeriodType = this;; paramPeriodType = new PeriodFormatter(this.iPrinter, this.iParser, this.iLocale, paramPeriodType)) {
      return paramPeriodType;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\format\PeriodFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */