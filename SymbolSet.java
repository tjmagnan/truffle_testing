package net.sourceforge.zbar;

import java.util.AbstractCollection;
import java.util.Iterator;

public class SymbolSet
  extends AbstractCollection<Symbol>
{
  private long peer;
  
  static
  {
    System.loadLibrary("zbarjni");
    init();
  }
  
  SymbolSet(long paramLong)
  {
    this.peer = paramLong;
  }
  
  private native void destroy(long paramLong);
  
  private native long firstSymbol(long paramLong);
  
  private static native void init();
  
  public void destroy()
  {
    try
    {
      if (this.peer != 0L)
      {
        destroy(this.peer);
        this.peer = 0L;
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  protected void finalize()
  {
    destroy();
  }
  
  public Iterator<Symbol> iterator()
  {
    long l = firstSymbol(this.peer);
    if (l == 0L) {}
    for (SymbolIterator localSymbolIterator = new SymbolIterator(null);; localSymbolIterator = new SymbolIterator(new Symbol(l))) {
      return localSymbolIterator;
    }
  }
  
  public native int size();
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\net\sourceforge\zbar\SymbolSet.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */