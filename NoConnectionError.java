package com.android.volley;

public class NoConnectionError
  extends NetworkError
{
  public NoConnectionError() {}
  
  public NoConnectionError(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\android\volley\NoConnectionError.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */