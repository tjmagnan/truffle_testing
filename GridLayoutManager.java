package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat.CollectionItemInfoCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.Arrays;

public class GridLayoutManager
  extends LinearLayoutManager
{
  private static final boolean DEBUG = false;
  public static final int DEFAULT_SPAN_COUNT = -1;
  private static final String TAG = "GridLayoutManager";
  int[] mCachedBorders;
  final Rect mDecorInsets = new Rect();
  boolean mPendingSpanCountChange = false;
  final SparseIntArray mPreLayoutSpanIndexCache = new SparseIntArray();
  final SparseIntArray mPreLayoutSpanSizeCache = new SparseIntArray();
  View[] mSet;
  int mSpanCount = -1;
  SpanSizeLookup mSpanSizeLookup = new DefaultSpanSizeLookup();
  
  public GridLayoutManager(Context paramContext, int paramInt)
  {
    super(paramContext);
    setSpanCount(paramInt);
  }
  
  public GridLayoutManager(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(paramContext, paramInt2, paramBoolean);
    setSpanCount(paramInt1);
  }
  
  public GridLayoutManager(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    setSpanCount(getProperties(paramContext, paramAttributeSet, paramInt1, paramInt2).spanCount);
  }
  
  private void assignSpans(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int j;
    int i;
    if (paramBoolean)
    {
      j = 0;
      paramInt2 = paramInt1;
      i = 1;
      paramInt1 = j;
    }
    for (;;)
    {
      int k = 0;
      j = paramInt1;
      paramInt1 = k;
      while (j != paramInt2)
      {
        View localView = this.mSet[j];
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        localLayoutParams.mSpanSize = getSpanSize(paramRecycler, paramState, getPosition(localView));
        localLayoutParams.mSpanIndex = paramInt1;
        paramInt1 += localLayoutParams.mSpanSize;
        j += i;
      }
      paramInt1--;
      paramInt2 = -1;
      i = -1;
    }
  }
  
  private void cachePreLayoutSpanMapping()
  {
    int j = getChildCount();
    for (int i = 0; i < j; i++)
    {
      LayoutParams localLayoutParams = (LayoutParams)getChildAt(i).getLayoutParams();
      int k = localLayoutParams.getViewLayoutPosition();
      this.mPreLayoutSpanSizeCache.put(k, localLayoutParams.getSpanSize());
      this.mPreLayoutSpanIndexCache.put(k, localLayoutParams.getSpanIndex());
    }
  }
  
  private void calculateItemBorders(int paramInt)
  {
    this.mCachedBorders = calculateItemBorders(this.mCachedBorders, this.mSpanCount, paramInt);
  }
  
  static int[] calculateItemBorders(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt;
    if ((paramArrayOfInt != null) && (paramArrayOfInt.length == paramInt1 + 1))
    {
      arrayOfInt = paramArrayOfInt;
      if (paramArrayOfInt[(paramArrayOfInt.length - 1)] == paramInt2) {}
    }
    else
    {
      arrayOfInt = new int[paramInt1 + 1];
    }
    arrayOfInt[0] = 0;
    int n = paramInt2 / paramInt1;
    int i2 = paramInt2 % paramInt1;
    int j = 0;
    paramInt2 = 0;
    for (int i = 1; i <= paramInt1; i++)
    {
      int k = n;
      int i1 = paramInt2 + i2;
      paramInt2 = i1;
      int m = k;
      if (i1 > 0)
      {
        paramInt2 = i1;
        m = k;
        if (paramInt1 - i1 < i2)
        {
          m = k + 1;
          paramInt2 = i1 - paramInt1;
        }
      }
      j += m;
      arrayOfInt[i] = j;
    }
    return arrayOfInt;
  }
  
  private void clearPreLayoutSpanMappingCache()
  {
    this.mPreLayoutSpanSizeCache.clear();
    this.mPreLayoutSpanIndexCache.clear();
  }
  
  private void ensureAnchorIsInCorrectSpan(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt)
  {
    int i = 1;
    if (paramInt == 1) {}
    for (;;)
    {
      paramInt = getSpanIndex(paramRecycler, paramState, paramAnchorInfo.mPosition);
      if (i == 0) {
        break;
      }
      while ((paramInt > 0) && (paramAnchorInfo.mPosition > 0))
      {
        paramAnchorInfo.mPosition -= 1;
        paramInt = getSpanIndex(paramRecycler, paramState, paramAnchorInfo.mPosition);
      }
      i = 0;
    }
    int k = paramState.getItemCount();
    int j = paramAnchorInfo.mPosition;
    i = paramInt;
    paramInt = j;
    while (paramInt < k - 1)
    {
      j = getSpanIndex(paramRecycler, paramState, paramInt + 1);
      if (j <= i) {
        break;
      }
      paramInt++;
      i = j;
    }
    paramAnchorInfo.mPosition = paramInt;
  }
  
  private void ensureViewSet()
  {
    if ((this.mSet == null) || (this.mSet.length != this.mSpanCount)) {
      this.mSet = new View[this.mSpanCount];
    }
  }
  
  private int getSpanGroupIndex(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    if (!paramState.isPreLayout()) {
      paramInt = this.mSpanSizeLookup.getSpanGroupIndex(paramInt, this.mSpanCount);
    }
    for (;;)
    {
      return paramInt;
      int i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
      if (i == -1)
      {
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + paramInt);
        paramInt = 0;
      }
      else
      {
        paramInt = this.mSpanSizeLookup.getSpanGroupIndex(i, this.mSpanCount);
      }
    }
  }
  
  private int getSpanIndex(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    int i;
    if (!paramState.isPreLayout()) {
      i = this.mSpanSizeLookup.getCachedSpanIndex(paramInt, this.mSpanCount);
    }
    for (;;)
    {
      return i;
      int j = this.mPreLayoutSpanIndexCache.get(paramInt, -1);
      i = j;
      if (j == -1)
      {
        i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
        if (i == -1)
        {
          Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + paramInt);
          i = 0;
        }
        else
        {
          i = this.mSpanSizeLookup.getCachedSpanIndex(i, this.mSpanCount);
        }
      }
    }
  }
  
  private int getSpanSize(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt)
  {
    int i;
    if (!paramState.isPreLayout()) {
      i = this.mSpanSizeLookup.getSpanSize(paramInt);
    }
    for (;;)
    {
      return i;
      int j = this.mPreLayoutSpanSizeCache.get(paramInt, -1);
      i = j;
      if (j == -1)
      {
        i = paramRecycler.convertPreLayoutPositionToPostLayout(paramInt);
        if (i == -1)
        {
          Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + paramInt);
          i = 1;
        }
        else
        {
          i = this.mSpanSizeLookup.getSpanSize(i);
        }
      }
    }
  }
  
  private void guessMeasurement(float paramFloat, int paramInt)
  {
    calculateItemBorders(Math.max(Math.round(this.mSpanCount * paramFloat), paramInt));
  }
  
  private void measureChild(View paramView, int paramInt, boolean paramBoolean)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    Rect localRect = localLayoutParams.mDecorInsets;
    int i = localRect.top + localRect.bottom + localLayoutParams.topMargin + localLayoutParams.bottomMargin;
    int j = localRect.left + localRect.right + localLayoutParams.leftMargin + localLayoutParams.rightMargin;
    int k = getSpaceForSpanRange(localLayoutParams.mSpanIndex, localLayoutParams.mSpanSize);
    if (this.mOrientation == 1)
    {
      paramInt = getChildMeasureSpec(k, paramInt, j, localLayoutParams.width, false);
      i = getChildMeasureSpec(this.mOrientationHelper.getTotalSpace(), getHeightMode(), i, localLayoutParams.height, true);
    }
    for (;;)
    {
      measureChildWithDecorationsAndMargin(paramView, paramInt, i, paramBoolean);
      return;
      i = getChildMeasureSpec(k, paramInt, i, localLayoutParams.height, false);
      paramInt = getChildMeasureSpec(this.mOrientationHelper.getTotalSpace(), getWidthMode(), j, localLayoutParams.width, true);
    }
  }
  
  private void measureChildWithDecorationsAndMargin(View paramView, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)paramView.getLayoutParams();
    if (paramBoolean) {}
    for (paramBoolean = shouldReMeasureChild(paramView, paramInt1, paramInt2, localLayoutParams);; paramBoolean = shouldMeasureChild(paramView, paramInt1, paramInt2, localLayoutParams))
    {
      if (paramBoolean) {
        paramView.measure(paramInt1, paramInt2);
      }
      return;
    }
  }
  
  private void updateMeasurements()
  {
    if (getOrientation() == 1) {}
    for (int i = getWidth() - getPaddingRight() - getPaddingLeft();; i = getHeight() - getPaddingBottom() - getPaddingTop())
    {
      calculateItemBorders(i);
      return;
    }
  }
  
  public boolean checkLayoutParams(RecyclerView.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  void collectPrefetchPositionsForLayoutState(RecyclerView.State paramState, LinearLayoutManager.LayoutState paramLayoutState, RecyclerView.LayoutManager.LayoutPrefetchRegistry paramLayoutPrefetchRegistry)
  {
    int i = this.mSpanCount;
    for (int j = 0; (j < this.mSpanCount) && (paramLayoutState.hasMore(paramState)) && (i > 0); j++)
    {
      int k = paramLayoutState.mCurrentPosition;
      paramLayoutPrefetchRegistry.addPosition(k, Math.max(0, paramLayoutState.mScrollingOffset));
      i -= this.mSpanSizeLookup.getSpanSize(k);
      paramLayoutState.mCurrentPosition += paramLayoutState.mItemDirection;
    }
  }
  
  View findReferenceChild(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, int paramInt1, int paramInt2, int paramInt3)
  {
    ensureLayoutState();
    Object localObject2 = null;
    Object localObject1 = null;
    int j = this.mOrientationHelper.getStartAfterPadding();
    int k = this.mOrientationHelper.getEndAfterPadding();
    int i;
    View localView;
    Object localObject3;
    Object localObject4;
    if (paramInt2 > paramInt1)
    {
      i = 1;
      if (paramInt1 == paramInt2) {
        break label221;
      }
      localView = getChildAt(paramInt1);
      int m = getPosition(localView);
      localObject3 = localObject2;
      localObject4 = localObject1;
      if (m >= 0)
      {
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (m < paramInt3)
        {
          if (getSpanIndex(paramRecycler, paramState, m) == 0) {
            break label127;
          }
          localObject4 = localObject1;
          localObject3 = localObject2;
        }
      }
    }
    for (;;)
    {
      paramInt1 += i;
      localObject2 = localObject3;
      localObject1 = localObject4;
      break;
      i = -1;
      break;
      label127:
      if (((RecyclerView.LayoutParams)localView.getLayoutParams()).isItemRemoved())
      {
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (localObject2 == null)
        {
          localObject3 = localView;
          localObject4 = localObject1;
        }
      }
      else
      {
        if (this.mOrientationHelper.getDecoratedStart(localView) < k)
        {
          localObject3 = localView;
          if (this.mOrientationHelper.getDecoratedEnd(localView) >= j) {
            break label230;
          }
        }
        localObject3 = localObject2;
        localObject4 = localObject1;
        if (localObject1 == null)
        {
          localObject3 = localObject2;
          localObject4 = localView;
        }
      }
    }
    label221:
    if (localObject1 != null) {}
    for (;;)
    {
      localObject3 = localObject1;
      label230:
      return (View)localObject3;
      localObject1 = localObject2;
    }
  }
  
  public RecyclerView.LayoutParams generateDefaultLayoutParams()
  {
    if (this.mOrientation == 0) {}
    for (LayoutParams localLayoutParams = new LayoutParams(-2, -1);; localLayoutParams = new LayoutParams(-1, -2)) {
      return localLayoutParams;
    }
  }
  
  public RecyclerView.LayoutParams generateLayoutParams(Context paramContext, AttributeSet paramAttributeSet)
  {
    return new LayoutParams(paramContext, paramAttributeSet);
  }
  
  public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {}
    for (paramLayoutParams = new LayoutParams((ViewGroup.MarginLayoutParams)paramLayoutParams);; paramLayoutParams = new LayoutParams(paramLayoutParams)) {
      return paramLayoutParams;
    }
  }
  
  public int getColumnCountForAccessibility(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    int i;
    if (this.mOrientation == 1) {
      i = this.mSpanCount;
    }
    for (;;)
    {
      return i;
      if (paramState.getItemCount() < 1) {
        i = 0;
      } else {
        i = getSpanGroupIndex(paramRecycler, paramState, paramState.getItemCount() - 1) + 1;
      }
    }
  }
  
  public int getRowCountForAccessibility(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    int i;
    if (this.mOrientation == 0) {
      i = this.mSpanCount;
    }
    for (;;)
    {
      return i;
      if (paramState.getItemCount() < 1) {
        i = 0;
      } else {
        i = getSpanGroupIndex(paramRecycler, paramState, paramState.getItemCount() - 1) + 1;
      }
    }
  }
  
  int getSpaceForSpanRange(int paramInt1, int paramInt2)
  {
    if ((this.mOrientation == 1) && (isLayoutRTL())) {}
    for (paramInt1 = this.mCachedBorders[(this.mSpanCount - paramInt1)] - this.mCachedBorders[(this.mSpanCount - paramInt1 - paramInt2)];; paramInt1 = this.mCachedBorders[(paramInt1 + paramInt2)] - this.mCachedBorders[paramInt1]) {
      return paramInt1;
    }
  }
  
  public int getSpanCount()
  {
    return this.mSpanCount;
  }
  
  public SpanSizeLookup getSpanSizeLookup()
  {
    return this.mSpanSizeLookup;
  }
  
  void layoutChunk(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.LayoutState paramLayoutState, LinearLayoutManager.LayoutChunkResult paramLayoutChunkResult)
  {
    int i3 = this.mOrientationHelper.getModeInOther();
    int k;
    int m;
    if (i3 != 1073741824)
    {
      k = 1;
      if (getChildCount() <= 0) {
        break label226;
      }
      m = this.mCachedBorders[this.mSpanCount];
      label38:
      if (k != 0) {
        updateMeasurements();
      }
      if (paramLayoutState.mItemDirection != 1) {
        break label232;
      }
    }
    int i2;
    int i1;
    int i;
    int n;
    int j;
    label226:
    label232:
    for (boolean bool = true;; bool = false)
    {
      i2 = 0;
      i1 = 0;
      i = this.mSpanCount;
      n = i2;
      j = i1;
      if (!bool)
      {
        i = getSpanIndex(paramRecycler, paramState, paramLayoutState.mCurrentPosition) + getSpanSize(paramRecycler, paramState, paramLayoutState.mCurrentPosition);
        j = i1;
        n = i2;
      }
      if ((n >= this.mSpanCount) || (!paramLayoutState.hasMore(paramState)) || (i <= 0)) {
        break label250;
      }
      i2 = paramLayoutState.mCurrentPosition;
      i1 = getSpanSize(paramRecycler, paramState, i2);
      if (i1 <= this.mSpanCount) {
        break label238;
      }
      throw new IllegalArgumentException("Item at position " + i2 + " requires " + i1 + " spans but GridLayoutManager has only " + this.mSpanCount + " spans.");
      k = 0;
      break;
      m = 0;
      break label38;
    }
    label238:
    i -= i1;
    if (i < 0)
    {
      label250:
      if (n != 0) {
        break label296;
      }
      paramLayoutChunkResult.mFinished = true;
    }
    for (;;)
    {
      return;
      Object localObject = paramLayoutState.next(paramRecycler);
      if (localObject == null) {
        break label250;
      }
      j += i1;
      this.mSet[n] = localObject;
      n++;
      break;
      label296:
      i = 0;
      float f2 = 0.0F;
      assignSpans(paramRecycler, paramState, n, j, bool);
      j = 0;
      if (j < n)
      {
        paramRecycler = this.mSet[j];
        if (paramLayoutState.mScrapList == null) {
          if (bool) {
            addView(paramRecycler);
          }
        }
        for (;;)
        {
          calculateItemDecorationsForChild(paramRecycler, this.mDecorInsets);
          measureChild(paramRecycler, i3, false);
          i2 = this.mOrientationHelper.getDecoratedMeasurement(paramRecycler);
          i1 = i;
          if (i2 > i) {
            i1 = i2;
          }
          paramState = (LayoutParams)paramRecycler.getLayoutParams();
          float f3 = 1.0F * this.mOrientationHelper.getDecoratedMeasurementInOther(paramRecycler) / paramState.mSpanSize;
          float f1 = f2;
          if (f3 > f2) {
            f1 = f3;
          }
          j++;
          i = i1;
          f2 = f1;
          break;
          addView(paramRecycler, 0);
          continue;
          if (bool) {
            addDisappearingView(paramRecycler);
          } else {
            addDisappearingView(paramRecycler, 0);
          }
        }
      }
      j = i;
      if (k != 0)
      {
        guessMeasurement(f2, m);
        i = 0;
        k = 0;
        for (;;)
        {
          j = i;
          if (k >= n) {
            break;
          }
          paramRecycler = this.mSet[k];
          measureChild(paramRecycler, 1073741824, true);
          m = this.mOrientationHelper.getDecoratedMeasurement(paramRecycler);
          j = i;
          if (m > i) {
            j = m;
          }
          k++;
          i = j;
        }
      }
      i = 0;
      if (i < n)
      {
        paramState = this.mSet[i];
        if (this.mOrientationHelper.getDecoratedMeasurement(paramState) != j)
        {
          localObject = (LayoutParams)paramState.getLayoutParams();
          paramRecycler = ((LayoutParams)localObject).mDecorInsets;
          m = paramRecycler.top + paramRecycler.bottom + ((LayoutParams)localObject).topMargin + ((LayoutParams)localObject).bottomMargin;
          k = paramRecycler.left + paramRecycler.right + ((LayoutParams)localObject).leftMargin + ((LayoutParams)localObject).rightMargin;
          i1 = getSpaceForSpanRange(((LayoutParams)localObject).mSpanIndex, ((LayoutParams)localObject).mSpanSize);
          if (this.mOrientation != 1) {
            break label728;
          }
          k = getChildMeasureSpec(i1, 1073741824, k, ((LayoutParams)localObject).width, false);
        }
        for (m = View.MeasureSpec.makeMeasureSpec(j - m, 1073741824);; m = getChildMeasureSpec(i1, 1073741824, m, ((LayoutParams)localObject).height, false))
        {
          measureChildWithDecorationsAndMargin(paramState, k, m, true);
          i++;
          break;
          label728:
          k = View.MeasureSpec.makeMeasureSpec(j - k, 1073741824);
        }
      }
      paramLayoutChunkResult.mConsumed = j;
      i = 0;
      k = 0;
      i1 = 0;
      m = 0;
      if (this.mOrientation == 1) {
        if (paramLayoutState.mLayoutDirection == -1)
        {
          m = paramLayoutState.mOffset;
          j = m - j;
          i1 = 0;
          i2 = k;
          k = j;
          label821:
          if (i1 >= n) {
            break label1087;
          }
          paramRecycler = this.mSet[i1];
          paramState = (LayoutParams)paramRecycler.getLayoutParams();
          if (this.mOrientation != 1) {
            break label1051;
          }
          if (!isLayoutRTL()) {
            break label1019;
          }
          j = getPaddingLeft() + this.mCachedBorders[(this.mSpanCount - paramState.mSpanIndex)];
          i = j - this.mOrientationHelper.getDecoratedMeasurementInOther(paramRecycler);
        }
      }
      for (;;)
      {
        layoutDecoratedWithMargins(paramRecycler, i, k, j, m);
        if ((paramState.isItemRemoved()) || (paramState.isItemChanged())) {
          paramLayoutChunkResult.mIgnoreConsumed = true;
        }
        paramLayoutChunkResult.mFocusable |= paramRecycler.hasFocusable();
        i1++;
        i2 = j;
        break label821;
        i1 = paramLayoutState.mOffset;
        m = i1 + j;
        j = i1;
        break;
        if (paramLayoutState.mLayoutDirection == -1)
        {
          k = paramLayoutState.mOffset;
          i = k - j;
          j = i1;
          break;
        }
        i = paramLayoutState.mOffset;
        k = i + j;
        j = i1;
        break;
        label1019:
        i = getPaddingLeft() + this.mCachedBorders[paramState.mSpanIndex];
        j = i + this.mOrientationHelper.getDecoratedMeasurementInOther(paramRecycler);
        continue;
        label1051:
        k = getPaddingTop() + this.mCachedBorders[paramState.mSpanIndex];
        m = k + this.mOrientationHelper.getDecoratedMeasurementInOther(paramRecycler);
        j = i2;
      }
      label1087:
      Arrays.fill(this.mSet, null);
    }
  }
  
  void onAnchorReady(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, LinearLayoutManager.AnchorInfo paramAnchorInfo, int paramInt)
  {
    super.onAnchorReady(paramRecycler, paramState, paramAnchorInfo, paramInt);
    updateMeasurements();
    if ((paramState.getItemCount() > 0) && (!paramState.isPreLayout())) {
      ensureAnchorIsInCorrectSpan(paramRecycler, paramState, paramAnchorInfo, paramInt);
    }
    ensureViewSet();
  }
  
  public View onFocusSearchFailed(View paramView, int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    View localView2 = findContainingItemView(paramView);
    if (localView2 == null) {}
    Object localObject1;
    int i8;
    int i9;
    for (paramView = null;; paramView = null)
    {
      return paramView;
      localObject1 = (LayoutParams)localView2.getLayoutParams();
      i8 = ((LayoutParams)localObject1).mSpanIndex;
      i9 = ((LayoutParams)localObject1).mSpanIndex + ((LayoutParams)localObject1).mSpanSize;
      if (super.onFocusSearchFailed(paramView, paramInt, paramRecycler, paramState) != null) {
        break;
      }
    }
    int i13;
    label75:
    label86:
    int i;
    int j;
    label103:
    int k;
    label121:
    Object localObject2;
    int i2;
    int i3;
    int m;
    int i1;
    int i10;
    int n;
    label152:
    View localView1;
    if (convertFocusDirectionToLayoutDirection(paramInt) == 1)
    {
      i13 = 1;
      if (i13 == this.mShouldReverseLayout) {
        break label201;
      }
      paramInt = 1;
      if (paramInt == 0) {
        break label206;
      }
      paramInt = getChildCount() - 1;
      i = -1;
      j = -1;
      if ((this.mOrientation != 1) || (!isLayoutRTL())) {
        break label220;
      }
      k = 1;
      localObject2 = null;
      i2 = -1;
      i3 = 0;
      localObject1 = null;
      m = -1;
      i1 = 0;
      i10 = getSpanGroupIndex(paramRecycler, paramState, paramInt);
      n = paramInt;
      if (n != j)
      {
        paramInt = getSpanGroupIndex(paramRecycler, paramState, n);
        localView1 = getChildAt(n);
        if (localView1 != localView2) {
          break label226;
        }
      }
      label184:
      if (localObject2 == null) {
        break label661;
      }
    }
    for (;;)
    {
      paramView = (View)localObject2;
      break;
      i13 = 0;
      break label75;
      label201:
      paramInt = 0;
      break label86;
      label206:
      paramInt = 0;
      i = 1;
      j = getChildCount();
      break label103;
      label220:
      k = 0;
      break label121;
      label226:
      int i7;
      int i6;
      Object localObject3;
      int i5;
      int i4;
      if ((localView1.hasFocusable()) && (paramInt != i10))
      {
        if (localObject2 != null) {
          break label184;
        }
        i7 = m;
        i6 = i1;
        localObject3 = localObject1;
        i5 = i2;
        i4 = i3;
        paramView = (View)localObject2;
      }
      for (;;)
      {
        n += i;
        localObject2 = paramView;
        i3 = i4;
        i2 = i5;
        localObject1 = localObject3;
        i1 = i6;
        m = i7;
        break label152;
        LayoutParams localLayoutParams = (LayoutParams)localView1.getLayoutParams();
        int i12 = localLayoutParams.mSpanIndex;
        int i11 = localLayoutParams.mSpanIndex + localLayoutParams.mSpanSize;
        if ((localView1.hasFocusable()) && (i12 == i8))
        {
          paramView = localView1;
          if (i11 == i9) {
            break;
          }
        }
        i5 = 0;
        if (((localView1.hasFocusable()) && (localObject2 == null)) || ((!localView1.hasFocusable()) && (localObject1 == null))) {
          paramInt = 1;
        }
        label578:
        do
        {
          for (;;)
          {
            paramView = (View)localObject2;
            i4 = i3;
            i5 = i2;
            localObject3 = localObject1;
            i6 = i1;
            i7 = m;
            if (paramInt == 0) {
              break;
            }
            if (!localView1.hasFocusable()) {
              break label619;
            }
            paramView = localView1;
            i5 = localLayoutParams.mSpanIndex;
            i4 = Math.min(i11, i9) - Math.max(i12, i8);
            localObject3 = localObject1;
            i6 = i1;
            i7 = m;
            break;
            paramInt = Math.max(i12, i8);
            i4 = Math.min(i11, i9) - paramInt;
            if (localView1.hasFocusable())
            {
              if (i4 > i3)
              {
                paramInt = 1;
              }
              else
              {
                paramInt = i5;
                if (i4 == i3)
                {
                  if (i12 > i2) {}
                  for (i4 = 1;; i4 = 0)
                  {
                    paramInt = i5;
                    if (k != i4) {
                      break;
                    }
                    paramInt = 1;
                    break;
                  }
                }
              }
            }
            else
            {
              paramInt = i5;
              if (localObject2 == null)
              {
                paramInt = i5;
                if (isViewPartiallyVisible(localView1, false, true))
                {
                  if (i4 <= i1) {
                    break label578;
                  }
                  paramInt = 1;
                }
              }
            }
          }
          paramInt = i5;
        } while (i4 != i1);
        if (i12 > m) {}
        for (i4 = 1;; i4 = 0)
        {
          paramInt = i5;
          if (k != i4) {
            break;
          }
          paramInt = 1;
          break;
        }
        label619:
        i7 = localLayoutParams.mSpanIndex;
        i6 = Math.min(i11, i9) - Math.max(i12, i8);
        paramView = (View)localObject2;
        i4 = i3;
        i5 = i2;
        localObject3 = localView1;
      }
      label661:
      localObject2 = localObject1;
    }
  }
  
  public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState, View paramView, AccessibilityNodeInfoCompat paramAccessibilityNodeInfoCompat)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (!(localLayoutParams instanceof LayoutParams))
    {
      super.onInitializeAccessibilityNodeInfoForItem(paramView, paramAccessibilityNodeInfoCompat);
      return;
    }
    paramView = (LayoutParams)localLayoutParams;
    int i = getSpanGroupIndex(paramRecycler, paramState, paramView.getViewLayoutPosition());
    if (this.mOrientation == 0)
    {
      k = paramView.getSpanIndex();
      j = paramView.getSpanSize();
      if ((this.mSpanCount > 1) && (paramView.getSpanSize() == this.mSpanCount)) {}
      for (bool = true;; bool = false)
      {
        paramAccessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(k, j, i, 1, bool, false));
        break;
      }
    }
    int k = paramView.getSpanIndex();
    int j = paramView.getSpanSize();
    if ((this.mSpanCount > 1) && (paramView.getSpanSize() == this.mSpanCount)) {}
    for (boolean bool = true;; bool = false)
    {
      paramAccessibilityNodeInfoCompat.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(i, 1, k, j, bool, false));
      break;
    }
  }
  
  public void onItemsAdded(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsChanged(RecyclerView paramRecyclerView)
  {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsMoved(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3)
  {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsRemoved(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onItemsUpdated(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, Object paramObject)
  {
    this.mSpanSizeLookup.invalidateSpanIndexCache();
  }
  
  public void onLayoutChildren(RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    if (paramState.isPreLayout()) {
      cachePreLayoutSpanMapping();
    }
    super.onLayoutChildren(paramRecycler, paramState);
    clearPreLayoutSpanMappingCache();
  }
  
  public void onLayoutCompleted(RecyclerView.State paramState)
  {
    super.onLayoutCompleted(paramState);
    this.mPendingSpanCountChange = false;
  }
  
  public int scrollHorizontallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    updateMeasurements();
    ensureViewSet();
    return super.scrollHorizontallyBy(paramInt, paramRecycler, paramState);
  }
  
  public int scrollVerticallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    updateMeasurements();
    ensureViewSet();
    return super.scrollVerticallyBy(paramInt, paramRecycler, paramState);
  }
  
  public void setMeasuredDimension(Rect paramRect, int paramInt1, int paramInt2)
  {
    if (this.mCachedBorders == null) {
      super.setMeasuredDimension(paramRect, paramInt1, paramInt2);
    }
    int j = getPaddingLeft() + getPaddingRight();
    int k = getPaddingTop() + getPaddingBottom();
    int i;
    if (this.mOrientation == 1)
    {
      i = chooseSize(paramInt2, paramRect.height() + k, getMinimumHeight());
      paramInt2 = chooseSize(paramInt1, this.mCachedBorders[(this.mCachedBorders.length - 1)] + j, getMinimumWidth());
      paramInt1 = i;
    }
    for (;;)
    {
      setMeasuredDimension(paramInt2, paramInt1);
      return;
      i = chooseSize(paramInt1, paramRect.width() + j, getMinimumWidth());
      paramInt1 = chooseSize(paramInt2, this.mCachedBorders[(this.mCachedBorders.length - 1)] + k, getMinimumHeight());
      paramInt2 = i;
    }
  }
  
  public void setSpanCount(int paramInt)
  {
    if (paramInt == this.mSpanCount) {}
    for (;;)
    {
      return;
      this.mPendingSpanCountChange = true;
      if (paramInt < 1) {
        throw new IllegalArgumentException("Span count should be at least 1. Provided " + paramInt);
      }
      this.mSpanCount = paramInt;
      this.mSpanSizeLookup.invalidateSpanIndexCache();
      requestLayout();
    }
  }
  
  public void setSpanSizeLookup(SpanSizeLookup paramSpanSizeLookup)
  {
    this.mSpanSizeLookup = paramSpanSizeLookup;
  }
  
  public void setStackFromEnd(boolean paramBoolean)
  {
    if (paramBoolean) {
      throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }
    super.setStackFromEnd(false);
  }
  
  public boolean supportsPredictiveItemAnimations()
  {
    if ((this.mPendingSavedState == null) && (!this.mPendingSpanCountChange)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static final class DefaultSpanSizeLookup
    extends GridLayoutManager.SpanSizeLookup
  {
    public int getSpanIndex(int paramInt1, int paramInt2)
    {
      return paramInt1 % paramInt2;
    }
    
    public int getSpanSize(int paramInt)
    {
      return 1;
    }
  }
  
  public static class LayoutParams
    extends RecyclerView.LayoutParams
  {
    public static final int INVALID_SPAN_ID = -1;
    int mSpanIndex = -1;
    int mSpanSize = 0;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(RecyclerView.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
    
    public int getSpanIndex()
    {
      return this.mSpanIndex;
    }
    
    public int getSpanSize()
    {
      return this.mSpanSize;
    }
  }
  
  public static abstract class SpanSizeLookup
  {
    private boolean mCacheSpanIndices = false;
    final SparseIntArray mSpanIndexCache = new SparseIntArray();
    
    int findReferenceIndexFromCache(int paramInt)
    {
      int i = 0;
      int j = this.mSpanIndexCache.size() - 1;
      while (i <= j)
      {
        int k = i + j >>> 1;
        if (this.mSpanIndexCache.keyAt(k) < paramInt) {
          i = k + 1;
        } else {
          j = k - 1;
        }
      }
      paramInt = i - 1;
      if ((paramInt >= 0) && (paramInt < this.mSpanIndexCache.size())) {}
      for (paramInt = this.mSpanIndexCache.keyAt(paramInt);; paramInt = -1) {
        return paramInt;
      }
    }
    
    int getCachedSpanIndex(int paramInt1, int paramInt2)
    {
      int i;
      if (!this.mCacheSpanIndices) {
        i = getSpanIndex(paramInt1, paramInt2);
      }
      for (;;)
      {
        return i;
        int j = this.mSpanIndexCache.get(paramInt1, -1);
        i = j;
        if (j == -1)
        {
          i = getSpanIndex(paramInt1, paramInt2);
          this.mSpanIndexCache.put(paramInt1, i);
        }
      }
    }
    
    public int getSpanGroupIndex(int paramInt1, int paramInt2)
    {
      int i = 0;
      int j = 0;
      int i2 = getSpanSize(paramInt1);
      int m = 0;
      if (m < paramInt1)
      {
        int n = getSpanSize(m);
        int i1 = i + n;
        int k;
        if (i1 == paramInt2)
        {
          i = 0;
          k = j + 1;
        }
        for (;;)
        {
          m++;
          j = k;
          break;
          k = j;
          i = i1;
          if (i1 > paramInt2)
          {
            i = n;
            k = j + 1;
          }
        }
      }
      paramInt1 = j;
      if (i + i2 > paramInt2) {
        paramInt1 = j + 1;
      }
      return paramInt1;
    }
    
    public int getSpanIndex(int paramInt1, int paramInt2)
    {
      int n = getSpanSize(paramInt1);
      if (n == paramInt2) {
        paramInt1 = 0;
      }
      for (;;)
      {
        return paramInt1;
        int m = 0;
        int k = 0;
        int j = m;
        int i = k;
        if (this.mCacheSpanIndices)
        {
          j = m;
          i = k;
          if (this.mSpanIndexCache.size() > 0)
          {
            int i1 = findReferenceIndexFromCache(paramInt1);
            j = m;
            i = k;
            if (i1 >= 0)
            {
              j = this.mSpanIndexCache.get(i1) + getSpanSize(i1);
              i = i1 + 1;
            }
          }
        }
        k = i;
        i = j;
        j = k;
        if (j < paramInt1)
        {
          k = getSpanSize(j);
          m = i + k;
          if (m == paramInt2) {
            i = 0;
          }
          for (;;)
          {
            j++;
            break;
            i = m;
            if (m > paramInt2) {
              i = k;
            }
          }
        }
        paramInt1 = i;
        if (i + n > paramInt2) {
          paramInt1 = 0;
        }
      }
    }
    
    public abstract int getSpanSize(int paramInt);
    
    public void invalidateSpanIndexCache()
    {
      this.mSpanIndexCache.clear();
    }
    
    public boolean isSpanIndexCacheEnabled()
    {
      return this.mCacheSpanIndices;
    }
    
    public void setSpanIndexCacheEnabled(boolean paramBoolean)
    {
      this.mCacheSpanIndices = paramBoolean;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\GridLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */