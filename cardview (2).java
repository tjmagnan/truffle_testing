package android.support.v7.cardview;

public final class R
{
  public static final class attr
  {
    public static final int cardBackgroundColor = 2130772218;
    public static final int cardCornerRadius = 2130772219;
    public static final int cardElevation = 2130772220;
    public static final int cardMaxElevation = 2130772221;
    public static final int cardPreventCornerOverlap = 2130772223;
    public static final int cardUseCompatPadding = 2130772222;
    public static final int contentPadding = 2130772224;
    public static final int contentPaddingBottom = 2130772228;
    public static final int contentPaddingLeft = 2130772225;
    public static final int contentPaddingRight = 2130772226;
    public static final int contentPaddingTop = 2130772227;
  }
  
  public static final class color
  {
    public static final int cardview_dark_background = 2131492885;
    public static final int cardview_light_background = 2131492886;
    public static final int cardview_shadow_end_color = 2131492887;
    public static final int cardview_shadow_start_color = 2131492888;
  }
  
  public static final class dimen
  {
    public static final int cardview_compat_inset_shadow = 2131230876;
    public static final int cardview_default_elevation = 2131230877;
    public static final int cardview_default_radius = 2131230878;
  }
  
  public static final class style
  {
    public static final int Base_CardView = 2131361968;
    public static final int CardView = 2131361953;
    public static final int CardView_Dark = 2131362010;
    public static final int CardView_Light = 2131362011;
  }
  
  public static final class styleable
  {
    public static final int[] CardView = { 16843071, 16843072, 2130772218, 2130772219, 2130772220, 2130772221, 2130772222, 2130772223, 2130772224, 2130772225, 2130772226, 2130772227, 2130772228 };
    public static final int CardView_android_minHeight = 1;
    public static final int CardView_android_minWidth = 0;
    public static final int CardView_cardBackgroundColor = 2;
    public static final int CardView_cardCornerRadius = 3;
    public static final int CardView_cardElevation = 4;
    public static final int CardView_cardMaxElevation = 5;
    public static final int CardView_cardPreventCornerOverlap = 7;
    public static final int CardView_cardUseCompatPadding = 6;
    public static final int CardView_contentPadding = 8;
    public static final int CardView_contentPaddingBottom = 12;
    public static final int CardView_contentPaddingLeft = 9;
    public static final int CardView_contentPaddingRight = 10;
    public static final int CardView_contentPaddingTop = 11;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\cardview\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */