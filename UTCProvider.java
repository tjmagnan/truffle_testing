package org.joda.time.tz;

import java.util.Collections;
import java.util.Set;
import org.joda.time.DateTimeZone;

public final class UTCProvider
  implements Provider
{
  private static final Set<String> AVAILABLE_IDS = Collections.singleton("UTC");
  
  public Set<String> getAvailableIDs()
  {
    return AVAILABLE_IDS;
  }
  
  public DateTimeZone getZone(String paramString)
  {
    if ("UTC".equalsIgnoreCase(paramString)) {}
    for (paramString = DateTimeZone.UTC;; paramString = null) {
      return paramString;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\tz\UTCProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */