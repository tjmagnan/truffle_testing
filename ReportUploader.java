package com.crashlytics.android.core;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.common.BackgroundPriorityRunnable;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ReportUploader
{
  static final Map<String, String> HEADER_INVALID_CLS_FILE = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
  private static final short[] RETRY_INTERVALS = { 10, 20, 30, 60, 120, 300 };
  private final String apiKey;
  private final CreateReportSpiCall createReportCall;
  private final Object fileAccessLock = new Object();
  private final HandlingExceptionCheck handlingExceptionCheck;
  private final ReportFilesProvider reportFilesProvider;
  private Thread uploadThread;
  
  public ReportUploader(String paramString, CreateReportSpiCall paramCreateReportSpiCall, ReportFilesProvider paramReportFilesProvider, HandlingExceptionCheck paramHandlingExceptionCheck)
  {
    if (paramCreateReportSpiCall == null) {
      throw new IllegalArgumentException("createReportCall must not be null.");
    }
    this.createReportCall = paramCreateReportSpiCall;
    this.apiKey = paramString;
    this.reportFilesProvider = paramReportFilesProvider;
    this.handlingExceptionCheck = paramHandlingExceptionCheck;
  }
  
  List<Report> findReports()
  {
    Fabric.getLogger().d("CrashlyticsCore", "Checking for crash reports...");
    int j;
    int i;
    Object localObject4;
    synchronized (this.fileAccessLock)
    {
      localObject3 = this.reportFilesProvider.getCompleteSessionFiles();
      File[] arrayOfFile = this.reportFilesProvider.getInvalidSessionFiles();
      ??? = new LinkedList();
      if (localObject3 != null)
      {
        j = localObject3.length;
        i = 0;
        if (i < j)
        {
          localObject4 = localObject3[i];
          Fabric.getLogger().d("CrashlyticsCore", "Found crash report " + ((File)localObject4).getPath());
          ((List)???).add(new SessionReport((File)localObject4));
          i++;
        }
      }
    }
    Object localObject3 = new HashMap();
    Object localObject5;
    if (localObject2 != null)
    {
      j = localObject2.length;
      for (i = 0; i < j; i++)
      {
        localObject5 = localObject2[i];
        localObject4 = CrashlyticsController.getSessionIdFromSessionFile((File)localObject5);
        if (!((Map)localObject3).containsKey(localObject4)) {
          ((Map)localObject3).put(localObject4, new LinkedList());
        }
        ((List)((Map)localObject3).get(localObject4)).add(localObject5);
      }
    }
    Iterator localIterator = ((Map)localObject3).keySet().iterator();
    while (localIterator.hasNext())
    {
      localObject5 = (String)localIterator.next();
      Fabric.getLogger().d("CrashlyticsCore", "Found invalid session: " + (String)localObject5);
      localObject4 = (List)((Map)localObject3).get(localObject5);
      ((List)???).add(new InvalidSessionReport((String)localObject5, (File[])((List)localObject4).toArray(new File[((List)localObject4).size()])));
    }
    if (((List)???).isEmpty()) {
      Fabric.getLogger().d("CrashlyticsCore", "No reports found.");
    }
    return (List<Report>)???;
  }
  
  boolean forceUpload(Report paramReport)
  {
    boolean bool2 = false;
    synchronized (this.fileAccessLock)
    {
      try
      {
        localObject1 = new com/crashlytics/android/core/CreateReportRequest;
        ((CreateReportRequest)localObject1).<init>(this.apiKey, paramReport);
        boolean bool3 = this.createReportCall.invoke((CreateReportRequest)localObject1);
        Logger localLogger = Fabric.getLogger();
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localStringBuilder = ((StringBuilder)localObject1).append("Crashlytics report upload ");
        if (!bool3) {
          break label120;
        }
        localObject1 = "complete: ";
        localLogger.i("CrashlyticsCore", (String)localObject1 + paramReport.getIdentifier());
        bool1 = bool2;
        if (bool3)
        {
          paramReport.remove();
          bool1 = true;
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          label120:
          Object localObject1 = Fabric.getLogger();
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          ((Logger)localObject1).e("CrashlyticsCore", "Error occurred sending report " + paramReport, localException);
          boolean bool1 = bool2;
        }
      }
      return bool1;
      localObject1 = "FAILED: ";
    }
  }
  
  boolean isUploading()
  {
    if (this.uploadThread != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  /* Error */
  public void uploadReports(float paramFloat, SendCheck paramSendCheck)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 84	com/crashlytics/android/core/ReportUploader:uploadThread	Ljava/lang/Thread;
    //   6: ifnull +19 -> 25
    //   9: invokestatic 96	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   12: ldc 98
    //   14: ldc_w 259
    //   17: invokeinterface 106 3 0
    //   22: aload_0
    //   23: monitorexit
    //   24: return
    //   25: new 18	com/crashlytics/android/core/ReportUploader$Worker
    //   28: astore_3
    //   29: aload_3
    //   30: aload_0
    //   31: fload_1
    //   32: aload_2
    //   33: invokespecial 262	com/crashlytics/android/core/ReportUploader$Worker:<init>	(Lcom/crashlytics/android/core/ReportUploader;FLcom/crashlytics/android/core/ReportUploader$SendCheck;)V
    //   36: new 264	java/lang/Thread
    //   39: astore_2
    //   40: aload_2
    //   41: aload_3
    //   42: ldc_w 266
    //   45: invokespecial 269	java/lang/Thread:<init>	(Ljava/lang/Runnable;Ljava/lang/String;)V
    //   48: aload_0
    //   49: aload_2
    //   50: putfield 84	com/crashlytics/android/core/ReportUploader:uploadThread	Ljava/lang/Thread;
    //   53: aload_0
    //   54: getfield 84	com/crashlytics/android/core/ReportUploader:uploadThread	Ljava/lang/Thread;
    //   57: invokevirtual 272	java/lang/Thread:start	()V
    //   60: goto -38 -> 22
    //   63: astore_2
    //   64: aload_0
    //   65: monitorexit
    //   66: aload_2
    //   67: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	68	0	this	ReportUploader
    //   0	68	1	paramFloat	float
    //   0	68	2	paramSendCheck	SendCheck
    //   28	14	3	localWorker	Worker
    // Exception table:
    //   from	to	target	type
    //   2	22	63	finally
    //   25	60	63	finally
  }
  
  static final class AlwaysSendCheck
    implements ReportUploader.SendCheck
  {
    public boolean canSendReports()
    {
      return true;
    }
  }
  
  static abstract interface HandlingExceptionCheck
  {
    public abstract boolean isHandlingException();
  }
  
  static abstract interface ReportFilesProvider
  {
    public abstract File[] getCompleteSessionFiles();
    
    public abstract File[] getInvalidSessionFiles();
  }
  
  static abstract interface SendCheck
  {
    public abstract boolean canSendReports();
  }
  
  private class Worker
    extends BackgroundPriorityRunnable
  {
    private final float delay;
    private final ReportUploader.SendCheck sendCheck;
    
    Worker(float paramFloat, ReportUploader.SendCheck paramSendCheck)
    {
      this.delay = paramFloat;
      this.sendCheck = paramSendCheck;
    }
    
    private void attemptUploadWithRetry()
    {
      Fabric.getLogger().d("CrashlyticsCore", "Starting report processing in " + this.delay + " second(s)...");
      if (this.delay > 0.0F) {}
      try
      {
        Thread.sleep((this.delay * 1000.0F));
        List localList = ReportUploader.this.findReports();
        if (ReportUploader.this.handlingExceptionCheck.isHandlingException()) {
          return;
        }
      }
      catch (InterruptedException localInterruptedException1)
      {
        for (;;)
        {
          Thread.currentThread().interrupt();
          continue;
          Object localObject1;
          if ((!localInterruptedException1.isEmpty()) && (!this.sendCheck.canSendReports()))
          {
            Fabric.getLogger().d("CrashlyticsCore", "User declined to send. Removing " + localInterruptedException1.size() + " Report(s).");
            localObject1 = localInterruptedException1.iterator();
            while (((Iterator)localObject1).hasNext()) {
              ((Report)((Iterator)localObject1).next()).remove();
            }
          }
          int i = 0;
          while ((!((List)localObject1).isEmpty()) && (!ReportUploader.this.handlingExceptionCheck.isHandlingException()))
          {
            Fabric.getLogger().d("CrashlyticsCore", "Attempting to send " + ((List)localObject1).size() + " report(s)");
            localObject1 = ((List)localObject1).iterator();
            while (((Iterator)localObject1).hasNext())
            {
              localObject2 = (Report)((Iterator)localObject1).next();
              ReportUploader.this.forceUpload((Report)localObject2);
            }
            Object localObject2 = ReportUploader.this.findReports();
            localObject1 = localObject2;
            if (!((List)localObject2).isEmpty())
            {
              long l = ReportUploader.RETRY_INTERVALS[Math.min(i, ReportUploader.RETRY_INTERVALS.length - 1)];
              Fabric.getLogger().d("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + l + " seconds");
              try
              {
                Thread.sleep(1000L * l);
                i++;
                localObject1 = localObject2;
              }
              catch (InterruptedException localInterruptedException2)
              {
                Thread.currentThread().interrupt();
              }
            }
          }
        }
      }
    }
    
    public void onRun()
    {
      try
      {
        attemptUploadWithRetry();
        ReportUploader.access$002(ReportUploader.this, null);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Fabric.getLogger().e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", localException);
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\ReportUploader.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */