package com.crashlytics.android.beta;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import io.fabric.sdk.android.Logger;
import io.fabric.sdk.android.services.cache.MemoryValueCache;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeviceIdentifierProvider;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType;
import io.fabric.sdk.android.services.common.SystemCurrentTimeProvider;
import io.fabric.sdk.android.services.network.DefaultHttpRequestFactory;
import io.fabric.sdk.android.services.persistence.PreferenceStoreImpl;
import io.fabric.sdk.android.services.settings.BetaSettingsData;
import io.fabric.sdk.android.services.settings.Settings;
import io.fabric.sdk.android.services.settings.SettingsData;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Beta
  extends Kit<Boolean>
  implements DeviceIdentifierProvider
{
  private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
  static final String NO_DEVICE_TOKEN = "";
  public static final String TAG = "Beta";
  private final MemoryValueCache<String> deviceTokenCache = new MemoryValueCache();
  private final DeviceTokenLoader deviceTokenLoader = new DeviceTokenLoader();
  private UpdatesController updatesController;
  
  private String getBetaDeviceToken(Context paramContext, String paramString)
  {
    paramString = null;
    for (;;)
    {
      try
      {
        paramContext = (String)this.deviceTokenCache.get(paramContext, this.deviceTokenLoader);
        bool = "".equals(paramContext);
        if (!bool) {
          continue;
        }
        paramContext = null;
      }
      catch (Exception paramContext)
      {
        StringBuilder localStringBuilder;
        Fabric.getLogger().e("Beta", "Failed to load the Beta device token", paramContext);
        paramContext = paramString;
        continue;
        boolean bool = false;
        continue;
      }
      paramString = Fabric.getLogger();
      localStringBuilder = new StringBuilder().append("Beta device token present: ");
      if (TextUtils.isEmpty(paramContext)) {
        continue;
      }
      bool = true;
      paramString.d("Beta", bool);
      return paramContext;
    }
  }
  
  private BetaSettingsData getBetaSettingsData()
  {
    Object localObject = Settings.getInstance().awaitSettingsData();
    if (localObject != null) {}
    for (localObject = ((SettingsData)localObject).betaSettingsData;; localObject = null) {
      return (BetaSettingsData)localObject;
    }
  }
  
  public static Beta getInstance()
  {
    return (Beta)Fabric.getKit(Beta.class);
  }
  
  private BuildProperties loadBuildProperties(Context paramContext)
  {
    Object localObject3 = null;
    Object localObject2 = null;
    Logger localLogger = null;
    StringBuilder localStringBuilder = null;
    Object localObject1 = localLogger;
    for (;;)
    {
      try
      {
        localInputStream = paramContext.getAssets().open("crashlytics-build.properties");
        paramContext = localStringBuilder;
        if (localInputStream != null)
        {
          localObject1 = localLogger;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          paramContext = BuildProperties.fromPropertiesStream(localInputStream);
          localObject1 = paramContext;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          localLogger = Fabric.getLogger();
          localObject1 = paramContext;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          localStringBuilder = new java/lang/StringBuilder;
          localObject1 = paramContext;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          localStringBuilder.<init>();
          localObject1 = paramContext;
          localObject2 = localInputStream;
          localObject3 = localInputStream;
          localLogger.d("Beta", paramContext.packageName + " build properties: " + paramContext.versionName + " (" + paramContext.versionCode + ") - " + paramContext.buildId);
        }
        localObject3 = paramContext;
      }
      catch (Exception paramContext)
      {
        InputStream localInputStream;
        localObject3 = localObject2;
        Fabric.getLogger().e("Beta", "Error reading Beta build properties", paramContext);
        localObject3 = localIOException1;
        if (localObject2 == null) {
          continue;
        }
        try
        {
          ((InputStream)localObject2).close();
          localObject3 = localIOException1;
        }
        catch (IOException paramContext)
        {
          Fabric.getLogger().e("Beta", "Error closing Beta build properties asset", paramContext);
          localObject3 = localIOException1;
        }
        continue;
      }
      finally
      {
        if (localObject3 == null) {
          break label261;
        }
      }
      try
      {
        localInputStream.close();
        localObject3 = paramContext;
      }
      catch (IOException localIOException1)
      {
        Fabric.getLogger().e("Beta", "Error closing Beta build properties asset", localIOException1);
        localObject3 = paramContext;
      }
    }
    return (BuildProperties)localObject3;
    try
    {
      ((InputStream)localObject3).close();
      label261:
      throw paramContext;
    }
    catch (IOException localIOException2)
    {
      for (;;)
      {
        Fabric.getLogger().e("Beta", "Error closing Beta build properties asset", localIOException2);
      }
    }
  }
  
  boolean canCheckForUpdates(BetaSettingsData paramBetaSettingsData, BuildProperties paramBuildProperties)
  {
    if ((paramBetaSettingsData != null) && (!TextUtils.isEmpty(paramBetaSettingsData.updateUrl)) && (paramBuildProperties != null)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  @TargetApi(14)
  UpdatesController createUpdatesController(int paramInt, Application paramApplication)
  {
    if (paramInt >= 14) {}
    for (paramApplication = new ActivityLifecycleCheckForUpdatesController(getFabric().getActivityLifecycleManager(), getFabric().getExecutorService());; paramApplication = new ImmediateCheckForUpdatesController()) {
      return paramApplication;
    }
  }
  
  protected Boolean doInBackground()
  {
    Fabric.getLogger().d("Beta", "Beta kit initializing...");
    Context localContext = getContext();
    IdManager localIdManager = getIdManager();
    if (TextUtils.isEmpty(getBetaDeviceToken(localContext, localIdManager.getInstallerPackageName()))) {
      Fabric.getLogger().d("Beta", "A Beta device token was not found for this app");
    }
    for (Object localObject = Boolean.valueOf(false);; localObject = Boolean.valueOf(true))
    {
      return (Boolean)localObject;
      Fabric.getLogger().d("Beta", "Beta device token is present, checking for app updates.");
      localObject = getBetaSettingsData();
      BuildProperties localBuildProperties = loadBuildProperties(localContext);
      if (canCheckForUpdates((BetaSettingsData)localObject, localBuildProperties)) {
        this.updatesController.initialize(localContext, this, localIdManager, (BetaSettingsData)localObject, localBuildProperties, new PreferenceStoreImpl(this), new SystemCurrentTimeProvider(), new DefaultHttpRequestFactory(Fabric.getLogger()));
      }
    }
  }
  
  public Map<IdManager.DeviceIdentifierType, String> getDeviceIdentifiers()
  {
    Object localObject = getIdManager().getInstallerPackageName();
    String str = getBetaDeviceToken(getContext(), (String)localObject);
    localObject = new HashMap();
    if (!TextUtils.isEmpty(str)) {
      ((Map)localObject).put(IdManager.DeviceIdentifierType.FONT_TOKEN, str);
    }
    return (Map<IdManager.DeviceIdentifierType, String>)localObject;
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android:beta";
  }
  
  String getOverridenSpiEndpoint()
  {
    return CommonUtils.getStringsFileValue(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  public String getVersion()
  {
    return "1.2.5.dev";
  }
  
  @TargetApi(14)
  protected boolean onPreExecute()
  {
    Application localApplication = (Application)getContext().getApplicationContext();
    this.updatesController = createUpdatesController(Build.VERSION.SDK_INT, localApplication);
    return true;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\beta\Beta.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */