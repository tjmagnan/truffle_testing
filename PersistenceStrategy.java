package io.fabric.sdk.android.services.persistence;

public abstract interface PersistenceStrategy<T>
{
  public abstract void clear();
  
  public abstract T restore();
  
  public abstract void save(T paramT);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\persistence\PersistenceStrategy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */