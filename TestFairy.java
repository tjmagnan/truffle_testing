package com.testfairy;

import android.content.Context;
import android.location.Location;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

public class TestFairy
{
  public static final String IDENTITY_TRAIT_AGE = "age";
  public static final String IDENTITY_TRAIT_BIRTHDAY = "birthday";
  public static final String IDENTITY_TRAIT_EMAIL_ADDRESS = "email";
  public static final String IDENTITY_TRAIT_GENDER = "gender";
  public static final String IDENTITY_TRAIT_NAME = "name";
  public static final String IDENTITY_TRAIT_PHONE_NUMBER = "phone_number";
  public static final String IDENTITY_TRAIT_SIGNUP_DATE = "signup_date";
  public static final String IDENTITY_TRAIT_WEBSITE_ADDRESS = "website_address";
  private static o a;
  
  private static void a()
  {
    b().f();
  }
  
  public static void addCheckpoint(String paramString)
  {
    b().c(paramString);
  }
  
  public static void addEvent(String paramString)
  {
    b().c(paramString);
  }
  
  public static void addSessionStateListener(SessionStateListener paramSessionStateListener)
  {
    b().a(paramSessionStateListener);
  }
  
  private static o b()
  {
    try
    {
      if (a == null)
      {
        localo = new com/testfairy/o;
        localo.<init>();
        a = localo;
      }
      o localo = a;
      return localo;
    }
    finally {}
  }
  
  public static void begin(Context paramContext, String paramString)
  {
    b().a(paramContext, paramString);
  }
  
  public static String getSessionUrl()
  {
    return b().b();
  }
  
  public static String getVersion()
  {
    return "1.5.1";
  }
  
  public static void hideView(View paramView)
  {
    b().a(paramView);
  }
  
  public static void hideView(Integer paramInteger)
  {
    o.a(paramInteger);
  }
  
  public static void identify(String paramString)
  {
    b().a(paramString, new HashMap());
  }
  
  public static void identify(String paramString, Map paramMap)
  {
    b().a(paramString, paramMap);
  }
  
  public static void log(String paramString1, String paramString2)
  {
    b().b(paramString1, paramString2);
  }
  
  public static void pause()
  {
    b().d();
  }
  
  public static void resume()
  {
    b().e();
  }
  
  public static void sendUserFeedback(String paramString)
  {
    b().b(paramString);
  }
  
  public static boolean setAttribute(String paramString1, String paramString2)
  {
    return b().a(paramString1, paramString2);
  }
  
  public static void setCorrelationId(String paramString)
  {
    b().a(paramString, new HashMap());
  }
  
  public static void setFeedbackOptions(FeedbackOptions paramFeedbackOptions)
  {
    b().a(paramFeedbackOptions);
  }
  
  public static void setLogEventFilter(LogEventFilter paramLogEventFilter)
  {
    b().a(paramLogEventFilter);
  }
  
  public static void setScreenName(String paramString)
  {
    b().a(paramString);
  }
  
  public static void setServerEndpoint(String paramString)
  {
    b().f(paramString);
  }
  
  public static void setUserId(String paramString)
  {
    b().d(paramString);
  }
  
  public static void stop()
  {
    b().c();
  }
  
  public static void updateLocation(Location paramLocation)
  {
    b().a(paramLocation);
  }
  
  public static abstract interface LogEventFilter
  {
    public abstract boolean accept(String paramString1, String paramString2, String paramString3);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\TestFairy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */