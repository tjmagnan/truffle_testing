package com.testfairy.f;

import android.util.Log;
import com.testfairy.k.c;
import com.testfairy.p.j;
import com.testfairy.p.l;
import java.io.File;
import java.io.FilenameFilter;
import org.json.JSONException;
import org.json.JSONObject;

public class e
{
  private final com.testfairy.k.d a;
  private final String b;
  
  public e(com.testfairy.k.d paramd, String paramString)
  {
    this.a = paramd;
    this.b = paramString;
  }
  
  private static void a(String paramString)
  {
    Log.v(com.testfairy.e.a, paramString);
  }
  
  private void a(String paramString, JSONObject paramJSONObject)
  {
    if (this.b == null) {}
    for (;;)
    {
      return;
      File localFile = new File(this.b + "/" + "testfairy-stacktrace" + "." + paramString + "." + System.currentTimeMillis());
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("sessionToken", paramString);
      localJSONObject.put("data", paramJSONObject);
      com.testfairy.p.d.b(localFile, localJSONObject.toString().getBytes());
      Log.i(com.testfairy.e.a, "Saved stack trace to " + localFile.getAbsolutePath());
    }
  }
  
  private static void b(String paramString, Throwable paramThrowable)
  {
    Log.e(com.testfairy.e.a, paramString, paramThrowable);
  }
  
  private String[] b()
  {
    Object localObject1 = null;
    Log.v(com.testfairy.e.a, "Absolute path: " + this.b);
    if (this.b == null) {}
    for (;;)
    {
      return (String[])localObject1;
      try
      {
        Object localObject2 = new java/io/File;
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        ((File)localObject2).<init>(this.b + "/");
        localObject3 = new com/testfairy/p/j;
        ((j)localObject3).<init>("testfairy-stacktrace");
        localObject2 = ((File)localObject2).list((FilenameFilter)localObject3);
        if ((localObject2 != null) && (localObject2.length > 0))
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          a("Found " + localObject2.length + " traces");
          localObject1 = localObject2;
        }
        else
        {
          a("No previous stack traces found");
        }
      }
      catch (Exception localException)
      {
        b("Can't send old exceptions", localException);
      }
    }
  }
  
  public void a()
  {
    String[] arrayOfString = b();
    if (arrayOfString == null) {
      return;
    }
    int j = arrayOfString.length;
    int i = 0;
    while (i < j)
    {
      String str1 = arrayOfString[i];
      try
      {
        Object localObject1 = com.testfairy.e.a;
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        Log.v((String)localObject1, "Sending " + str1);
        localObject1 = new java/io/File;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        ((File)localObject1).<init>(this.b + "/" + str1);
        localObject2 = com.testfairy.p.d.a((File)localObject1);
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        a("Read this from file: '" + (String)localObject2 + "'");
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        if (!((JSONObject)localObject3).has("sessionToken"))
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          a("NO SESSION TOKEN IN " + str1);
        }
        for (;;)
        {
          i++;
          break;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          a("Sending crash report with sessionToken " + ((JSONObject)localObject3).getString("sessionToken"));
          localObject2 = this.a;
          String str2 = ((JSONObject)localObject3).getString("data");
          localObject3 = ((JSONObject)localObject3).getString("sessionToken");
          c localc = new com/testfairy/k/c;
          localc.<init>(((File)localObject1).getAbsolutePath());
          ((com.testfairy.k.d)localObject2).a(str2, (String)localObject3, localc);
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          b("Could not read stack trace data from " + str1, localException);
        }
      }
    }
  }
  
  public void a(String paramString, Throwable paramThrowable)
  {
    try
    {
      String str = l.a(paramThrowable);
      Object localObject = l.a(str);
      paramThrowable = new org/json/JSONObject;
      paramThrowable.<init>();
      paramThrowable.put("timestamp", System.currentTimeMillis() / 1000L);
      paramThrowable.put("message", localObject);
      paramThrowable.put("stackTrace", str);
      a(paramString, paramThrowable);
      str = com.testfairy.e.a;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      Log.i(str, "Sending crash " + paramThrowable.toString());
      this.a.a(paramThrowable.toString(), paramString, null);
      return;
    }
    catch (JSONException paramString)
    {
      for (;;)
      {
        Log.e(com.testfairy.e.a, "Ouch! JSON Exception", paramString);
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f\e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */