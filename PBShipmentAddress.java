package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBShipmentAddress
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -3638244720488332305L;
  @Expose
  @SerializedName("city")
  private String city;
  @Expose
  @SerializedName("company")
  private String company;
  @Expose
  @SerializedName("countryName")
  private String countryName;
  @Expose
  @SerializedName("email")
  private String email;
  @Expose
  @SerializedName("firstName")
  private String firstName;
  @Expose
  @SerializedName("fullName")
  private String fullName;
  @Expose
  @SerializedName("isoCountry")
  private String isoCountry;
  @Expose
  @SerializedName("lastLineOnly")
  private Boolean lastLineOnly;
  @Expose
  @SerializedName("lastName")
  private String lastName;
  @Expose
  @SerializedName("middleName")
  private String middleName;
  @Expose
  @SerializedName("phone")
  private String phone;
  @Expose
  @SerializedName("postalCode")
  private String postalCode;
  @Expose
  @SerializedName("residential")
  private Boolean residential;
  @Expose
  @SerializedName("state")
  private String state;
  @Expose
  @SerializedName("streetLine1")
  private String streetLine1;
  @Expose
  @SerializedName("streetLine2")
  private String streetLine2;
  @Expose
  @SerializedName("streetLine3")
  private String streetLine3;
  @Expose
  @SerializedName("streetLine4")
  private String streetLine4;
  @Expose
  @SerializedName("type")
  private String type;
  @Expose
  @SerializedName("verified")
  private Boolean verified;
  
  public PBShipmentAddress() {}
  
  public PBShipmentAddress(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Boolean paramBoolean1, String paramString8, String paramString9, String paramString10, String paramString11, Boolean paramBoolean2, String paramString12, String paramString13, String paramString14, String paramString15, String paramString16, String paramString17, Boolean paramBoolean3)
  {
    this.city = paramString1;
    this.company = paramString2;
    this.countryName = paramString3;
    this.email = paramString4;
    this.firstName = paramString5;
    this.fullName = paramString6;
    this.isoCountry = paramString7;
    this.lastLineOnly = paramBoolean1;
    this.lastName = paramString8;
    this.middleName = paramString9;
    this.phone = paramString10;
    this.postalCode = paramString11;
    this.residential = paramBoolean2;
    this.state = paramString12;
    this.streetLine1 = paramString13;
    this.streetLine2 = paramString14;
    this.streetLine3 = paramString15;
    this.streetLine4 = paramString16;
    this.type = paramString17;
    this.verified = paramBoolean3;
  }
  
  public String getCity()
  {
    return this.city;
  }
  
  public String getCompany()
  {
    return this.company;
  }
  
  public String getCountryName()
  {
    return this.countryName;
  }
  
  public String getEmail()
  {
    return this.email;
  }
  
  public String getFirstName()
  {
    return this.firstName;
  }
  
  public String getFullName()
  {
    return this.fullName;
  }
  
  public String getIsoCountry()
  {
    return this.isoCountry;
  }
  
  public Boolean getLastLineOnly()
  {
    return this.lastLineOnly;
  }
  
  public String getLastName()
  {
    return this.lastName;
  }
  
  public String getMiddleName()
  {
    return this.middleName;
  }
  
  public String getPhone()
  {
    return this.phone;
  }
  
  public String getPostalCode()
  {
    return this.postalCode;
  }
  
  public Boolean getResidential()
  {
    return this.residential;
  }
  
  public String getState()
  {
    return this.state;
  }
  
  public String getStreetLine1()
  {
    return this.streetLine1;
  }
  
  public String getStreetLine2()
  {
    return this.streetLine2;
  }
  
  public String getStreetLine3()
  {
    return this.streetLine3;
  }
  
  public String getStreetLine4()
  {
    return this.streetLine4;
  }
  
  public String getType()
  {
    return this.type;
  }
  
  public Boolean getVerified()
  {
    return this.verified;
  }
  
  public void setCity(String paramString)
  {
    this.city = paramString;
  }
  
  public void setCompany(String paramString)
  {
    this.company = paramString;
  }
  
  public void setCountryName(String paramString)
  {
    this.countryName = paramString;
  }
  
  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
  
  public void setFirstName(String paramString)
  {
    this.firstName = paramString;
  }
  
  public void setFullName(String paramString)
  {
    this.fullName = paramString;
  }
  
  public void setIsoCountry(String paramString)
  {
    this.isoCountry = paramString;
  }
  
  public void setLastLineOnly(Boolean paramBoolean)
  {
    this.lastLineOnly = paramBoolean;
  }
  
  public void setLastName(String paramString)
  {
    this.lastName = paramString;
  }
  
  public void setMiddleName(String paramString)
  {
    this.middleName = paramString;
  }
  
  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }
  
  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }
  
  public void setResidential(Boolean paramBoolean)
  {
    this.residential = paramBoolean;
  }
  
  public void setState(String paramString)
  {
    this.state = paramString;
  }
  
  public void setStreetLine1(String paramString)
  {
    this.streetLine1 = paramString;
  }
  
  public void setStreetLine2(String paramString)
  {
    this.streetLine2 = paramString;
  }
  
  public void setStreetLine3(String paramString)
  {
    this.streetLine3 = paramString;
  }
  
  public void setStreetLine4(String paramString)
  {
    this.streetLine4 = paramString;
  }
  
  public void setType(String paramString)
  {
    this.type = paramString;
  }
  
  public void setVerified(Boolean paramBoolean)
  {
    this.verified = paramBoolean;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentAddress.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */