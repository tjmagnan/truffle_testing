package android.support.graphics.drawable;

import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.TintAwareDrawable;
import android.util.AttributeSet;

abstract class VectorDrawableCommon
  extends Drawable
  implements TintAwareDrawable
{
  Drawable mDelegateDrawable;
  
  protected static TypedArray obtainAttributes(Resources paramResources, Resources.Theme paramTheme, AttributeSet paramAttributeSet, int[] paramArrayOfInt)
  {
    if (paramTheme == null) {}
    for (paramResources = paramResources.obtainAttributes(paramAttributeSet, paramArrayOfInt);; paramResources = paramTheme.obtainStyledAttributes(paramAttributeSet, paramArrayOfInt, 0, 0)) {
      return paramResources;
    }
  }
  
  public void applyTheme(Resources.Theme paramTheme)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.applyTheme(this.mDelegateDrawable, paramTheme);
    }
  }
  
  public void clearColorFilter()
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.clearColorFilter();
    }
    for (;;)
    {
      return;
      super.clearColorFilter();
    }
  }
  
  public ColorFilter getColorFilter()
  {
    if (this.mDelegateDrawable != null) {}
    for (ColorFilter localColorFilter = DrawableCompat.getColorFilter(this.mDelegateDrawable);; localColorFilter = null) {
      return localColorFilter;
    }
  }
  
  public Drawable getCurrent()
  {
    if (this.mDelegateDrawable != null) {}
    for (Drawable localDrawable = this.mDelegateDrawable.getCurrent();; localDrawable = super.getCurrent()) {
      return localDrawable;
    }
  }
  
  public int getMinimumHeight()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getMinimumHeight();; i = super.getMinimumHeight()) {
      return i;
    }
  }
  
  public int getMinimumWidth()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getMinimumWidth();; i = super.getMinimumWidth()) {
      return i;
    }
  }
  
  public boolean getPadding(Rect paramRect)
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.getPadding(paramRect);; bool = super.getPadding(paramRect)) {
      return bool;
    }
  }
  
  public int[] getState()
  {
    if (this.mDelegateDrawable != null) {}
    for (int[] arrayOfInt = this.mDelegateDrawable.getState();; arrayOfInt = super.getState()) {
      return arrayOfInt;
    }
  }
  
  public Region getTransparentRegion()
  {
    if (this.mDelegateDrawable != null) {}
    for (Region localRegion = this.mDelegateDrawable.getTransparentRegion();; localRegion = super.getTransparentRegion()) {
      return localRegion;
    }
  }
  
  public void jumpToCurrentState()
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.jumpToCurrentState(this.mDelegateDrawable);
    }
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setBounds(paramRect);
    }
    for (;;)
    {
      return;
      super.onBoundsChange(paramRect);
    }
  }
  
  protected boolean onLevelChange(int paramInt)
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.setLevel(paramInt);; bool = super.onLevelChange(paramInt)) {
      return bool;
    }
  }
  
  public void setChangingConfigurations(int paramInt)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setChangingConfigurations(paramInt);
    }
    for (;;)
    {
      return;
      super.setChangingConfigurations(paramInt);
    }
  }
  
  public void setColorFilter(int paramInt, PorterDuff.Mode paramMode)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setColorFilter(paramInt, paramMode);
    }
    for (;;)
    {
      return;
      super.setColorFilter(paramInt, paramMode);
    }
  }
  
  public void setFilterBitmap(boolean paramBoolean)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setFilterBitmap(paramBoolean);
    }
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.setHotspot(this.mDelegateDrawable, paramFloat1, paramFloat2);
    }
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.setHotspotBounds(this.mDelegateDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  public boolean setState(int[] paramArrayOfInt)
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.setState(paramArrayOfInt);; bool = super.setState(paramArrayOfInt)) {
      return bool;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\graphics\drawable\VectorDrawableCommon.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */