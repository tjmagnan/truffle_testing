package com.testfairy.h;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

class h
  implements HttpEntity
{
  private static final char[] d = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
  ByteArrayOutputStream a = new ByteArrayOutputStream();
  boolean b = false;
  boolean c = false;
  private String e = null;
  
  public h()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    long l = System.currentTimeMillis();
    while (i < 30)
    {
      int j = (int)(2L * l * (i + 7) % d.length);
      localStringBuffer.append(d[j]);
      i++;
    }
    this.e = localStringBuffer.toString();
  }
  
  public void a()
  {
    if (!this.c) {}
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = this.a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localByteArrayOutputStream.write(("--" + this.e + "\r\n").getBytes());
      this.c = true;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
  }
  
  public void a(String paramString, File paramFile, boolean paramBoolean)
  {
    try
    {
      String str = paramFile.getName();
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile);
      a(paramString, str, localFileInputStream, paramBoolean);
      return;
    }
    catch (FileNotFoundException paramString)
    {
      for (;;)
      {
        paramString.printStackTrace();
      }
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    a();
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = this.a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localByteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"\r\n\r\n").getBytes());
      this.a.write(paramString2.getBytes());
      paramString1 = this.a;
      paramString2 = new java/lang/StringBuilder;
      paramString2.<init>();
      paramString1.write(("\r\n--" + this.e + "\r\n").getBytes());
      return;
    }
    catch (IOException paramString1)
    {
      for (;;)
      {
        paramString1.printStackTrace();
      }
    }
  }
  
  public void a(String paramString1, String paramString2, InputStream paramInputStream, String paramString3, boolean paramBoolean)
  {
    a();
    try
    {
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = "Content-Type: " + paramString3 + "\r\n";
      paramString3 = this.a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      paramString3.write(("Content-Disposition: form-data; name=\"" + paramString1 + "\"; filename=\"" + paramString2 + "\"\r\n").getBytes());
      this.a.write(((String)localObject).getBytes());
      this.a.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
      paramString1 = new byte['က'];
      for (;;)
      {
        int i = paramInputStream.read(paramString1);
        if (i == -1) {
          break;
        }
        this.a.write(paramString1, 0, i);
      }
      try
      {
        paramInputStream.close();
        throw paramString1;
      }
      catch (IOException paramString2)
      {
        for (;;)
        {
          paramString2.printStackTrace();
        }
      }
    }
    catch (IOException paramString1)
    {
      paramString1 = paramString1;
      paramString1.printStackTrace();
      try
      {
        paramInputStream.close();
        for (;;)
        {
          return;
          if (!paramBoolean)
          {
            paramString1 = this.a;
            paramString2 = new java/lang/StringBuilder;
            paramString2.<init>();
            paramString1.write(("\r\n--" + this.e + "\r\n").getBytes());
          }
          this.a.flush();
          try
          {
            paramInputStream.close();
          }
          catch (IOException paramString1)
          {
            paramString1.printStackTrace();
          }
        }
      }
      catch (IOException paramString1)
      {
        for (;;)
        {
          paramString1.printStackTrace();
        }
      }
    }
    finally {}
  }
  
  public void a(String paramString1, String paramString2, InputStream paramInputStream, boolean paramBoolean)
  {
    a(paramString1, paramString2, paramInputStream, "application/octet-stream", paramBoolean);
  }
  
  public void b()
  {
    if (this.b) {}
    for (;;)
    {
      return;
      try
      {
        ByteArrayOutputStream localByteArrayOutputStream = this.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localByteArrayOutputStream.write(("\r\n--" + this.e + "--\r\n").getBytes());
        this.b = true;
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localIOException.printStackTrace();
        }
      }
    }
  }
  
  public void consumeContent()
  {
    if (isStreaming()) {
      throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
    }
  }
  
  public InputStream getContent()
  {
    return new ByteArrayInputStream(this.a.toByteArray());
  }
  
  public Header getContentEncoding()
  {
    return null;
  }
  
  public long getContentLength()
  {
    b();
    return this.a.toByteArray().length;
  }
  
  public Header getContentType()
  {
    return new BasicHeader("Content-Type", "multipart/form-data; boundary=" + this.e);
  }
  
  public boolean isChunked()
  {
    return false;
  }
  
  public boolean isRepeatable()
  {
    return false;
  }
  
  public boolean isStreaming()
  {
    return false;
  }
  
  public void writeTo(OutputStream paramOutputStream)
  {
    paramOutputStream.write(this.a.toByteArray());
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */