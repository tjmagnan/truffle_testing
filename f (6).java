package com.testfairy.f;

public class d
{
  private static boolean a = false;
  private static Thread.UncaughtExceptionHandler b = null;
  private static Thread.UncaughtExceptionHandler c = null;
  private static b d;
  private static final Thread.UncaughtExceptionHandler e = new Thread.UncaughtExceptionHandler()
  {
    public void uncaughtException(Thread paramAnonymousThread, Throwable paramAnonymousThrowable)
    {
      d.a(paramAnonymousThread, paramAnonymousThrowable);
    }
  };
  
  public static void a(b paramb)
  {
    d = paramb;
    if (Thread.getDefaultUncaughtExceptionHandler() != e)
    {
      c = b;
      b = Thread.getDefaultUncaughtExceptionHandler();
      Thread.setDefaultUncaughtExceptionHandler(e);
    }
  }
  
  public static boolean a()
  {
    return a;
  }
  
  private static void b(Thread paramThread, Throwable paramThrowable)
  {
    if (a) {
      c.uncaughtException(paramThread, paramThrowable);
    }
    for (;;)
    {
      return;
      a = true;
      if (d != null) {
        d.a(paramThread, paramThrowable);
      }
      b.uncaughtException(paramThread, paramThrowable);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\f\d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */