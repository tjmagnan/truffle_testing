package io.keen.client.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class JsonHelper
{
  static Object fromJson(Object paramObject)
    throws JSONException
  {
    Object localObject;
    if (paramObject == JSONObject.NULL) {
      localObject = null;
    }
    for (;;)
    {
      return localObject;
      if ((paramObject instanceof JSONObject))
      {
        localObject = toMap((JSONObject)paramObject);
      }
      else
      {
        localObject = paramObject;
        if ((paramObject instanceof JSONArray)) {
          localObject = toList((JSONArray)paramObject);
        }
      }
    }
  }
  
  public static Map<String, Object> getMap(JSONObject paramJSONObject, String paramString)
    throws JSONException
  {
    return toMap(paramJSONObject.getJSONObject(paramString));
  }
  
  public static boolean isEmptyObject(JSONObject paramJSONObject)
  {
    if (paramJSONObject.names() == null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static Object toJSON(Object paramObject)
    throws JSONException
  {
    Object localObject;
    Iterator localIterator;
    if ((paramObject instanceof Map))
    {
      localObject = new JSONObject();
      Map localMap = (Map)paramObject;
      localIterator = localMap.keySet().iterator();
      for (;;)
      {
        paramObject = localObject;
        if (!localIterator.hasNext()) {
          break;
        }
        paramObject = localIterator.next();
        ((JSONObject)localObject).put(paramObject.toString(), toJSON(localMap.get(paramObject)));
      }
    }
    if ((paramObject instanceof Iterable))
    {
      localObject = new JSONArray();
      localIterator = ((Iterable)paramObject).iterator();
      for (;;)
      {
        paramObject = localObject;
        if (!localIterator.hasNext()) {
          break;
        }
        ((JSONArray)localObject).put(localIterator.next());
      }
    }
    return paramObject;
  }
  
  public static List<Object> toList(JSONArray paramJSONArray)
    throws JSONException
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramJSONArray.length(); i++) {
      localArrayList.add(fromJson(paramJSONArray.get(i)));
    }
    return localArrayList;
  }
  
  public static Map<String, Object> toMap(JSONObject paramJSONObject)
    throws JSONException
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramJSONObject.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localHashMap.put(str, fromJson(paramJSONObject.get(str)));
    }
    return localHashMap;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\keen\client\android\JsonHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */