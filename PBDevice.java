package tech.dcube.companion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PBDevice
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 4991070482345871793L;
  private List<PBDeviceComponent> components = new ArrayList();
  private String currencyCode;
  private String deviceFamily;
  private String deviceType;
  private String lastOnline;
  private String lastRateUpdate;
  private Object lastSoftwareUpdate;
  private Double lowMeterBalanceWarning;
  private Double lowMeterBalanceWarningMax;
  private Integer lowMeterBalanceWarningMin;
  private Float meterBalance;
  private String modelNumber;
  private String onlineState;
  private String rateVersion;
  private Integer refillDefault;
  private Integer refillMax;
  private Integer refillMin;
  private String serialNumber;
  private String softwareVersion;
  
  public PBDevice() {}
  
  public PBDevice(List<PBDeviceComponent> paramList, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Object paramObject, Double paramDouble1, Double paramDouble2, Integer paramInteger1, Float paramFloat, String paramString6, String paramString7, String paramString8, String paramString9, Integer paramInteger2, Integer paramInteger3, Integer paramInteger4, String paramString10)
  {
    this.components = paramList;
    this.currencyCode = paramString1;
    this.deviceFamily = paramString2;
    this.deviceType = paramString3;
    this.lastOnline = paramString4;
    this.lastRateUpdate = paramString5;
    this.lastSoftwareUpdate = paramObject;
    this.lowMeterBalanceWarning = paramDouble1;
    this.lowMeterBalanceWarningMax = paramDouble2;
    this.lowMeterBalanceWarningMin = paramInteger1;
    this.meterBalance = paramFloat;
    this.modelNumber = paramString6;
    this.serialNumber = paramString7;
    this.onlineState = paramString8;
    this.rateVersion = paramString9;
    this.refillDefault = paramInteger2;
    this.refillMax = paramInteger3;
    this.refillMin = paramInteger4;
    this.softwareVersion = paramString10;
  }
  
  public List<PBDeviceComponent> getComponents()
  {
    return this.components;
  }
  
  public String getCurrencyCode()
  {
    return this.currencyCode;
  }
  
  public String getDeviceFamily()
  {
    return this.deviceFamily;
  }
  
  public String getDeviceType()
  {
    return this.deviceType;
  }
  
  public String getLastOnline()
  {
    return this.lastOnline;
  }
  
  public String getLastRateUpdate()
  {
    return this.lastRateUpdate;
  }
  
  public Object getLastSoftwareUpdate()
  {
    return this.lastSoftwareUpdate;
  }
  
  public Double getLowMeterBalanceWarning()
  {
    return this.lowMeterBalanceWarning;
  }
  
  public Double getLowMeterBalanceWarningMax()
  {
    return this.lowMeterBalanceWarningMax;
  }
  
  public Integer getLowMeterBalanceWarningMin()
  {
    return this.lowMeterBalanceWarningMin;
  }
  
  public Float getMeterBalance()
  {
    return this.meterBalance;
  }
  
  public String getModelNumber()
  {
    return this.modelNumber;
  }
  
  public String getOnlineState()
  {
    return this.onlineState;
  }
  
  public String getRateVersion()
  {
    return this.rateVersion;
  }
  
  public Integer getRefillDefault()
  {
    return this.refillDefault;
  }
  
  public Integer getRefillMax()
  {
    return this.refillMax;
  }
  
  public Integer getRefillMin()
  {
    return this.refillMin;
  }
  
  public String getSerialNumber()
  {
    return this.serialNumber;
  }
  
  public String getSoftwareVersion()
  {
    return this.softwareVersion;
  }
  
  public void setComponents(List<PBDeviceComponent> paramList)
  {
    this.components = paramList;
  }
  
  public void setCurrencyCode(String paramString)
  {
    this.currencyCode = paramString;
  }
  
  public void setDeviceFamily(String paramString)
  {
    this.deviceFamily = paramString;
  }
  
  public void setDeviceType(String paramString)
  {
    this.deviceType = paramString;
  }
  
  public void setLastOnline(String paramString)
  {
    this.lastOnline = paramString;
  }
  
  public void setLastRateUpdate(String paramString)
  {
    this.lastRateUpdate = paramString;
  }
  
  public void setLastSoftwareUpdate(Object paramObject)
  {
    this.lastSoftwareUpdate = paramObject;
  }
  
  public void setLowMeterBalanceWarning(Double paramDouble)
  {
    this.lowMeterBalanceWarning = paramDouble;
  }
  
  public void setLowMeterBalanceWarningMax(Double paramDouble)
  {
    this.lowMeterBalanceWarningMax = paramDouble;
  }
  
  public void setLowMeterBalanceWarningMin(Integer paramInteger)
  {
    this.lowMeterBalanceWarningMin = paramInteger;
  }
  
  public void setMeterBalance(Float paramFloat)
  {
    this.meterBalance = paramFloat;
  }
  
  public void setModelNumber(String paramString)
  {
    this.modelNumber = paramString;
  }
  
  public void setOnlineState(String paramString)
  {
    this.onlineState = paramString;
  }
  
  public void setRateVersion(String paramString)
  {
    this.rateVersion = paramString;
  }
  
  public void setRefillDefault(Integer paramInteger)
  {
    this.refillDefault = paramInteger;
  }
  
  public void setRefillMax(Integer paramInteger)
  {
    this.refillMax = paramInteger;
  }
  
  public void setRefillMin(Integer paramInteger)
  {
    this.refillMin = paramInteger;
  }
  
  public void setSerialNumber(String paramString)
  {
    this.serialNumber = paramString;
  }
  
  public void setSoftwareVersion(String paramString)
  {
    this.softwareVersion = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBDevice.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */