package android.support.v7.widget;

import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

public class LinearSnapHelper
  extends SnapHelper
{
  private static final float INVALID_DISTANCE = 1.0F;
  @Nullable
  private OrientationHelper mHorizontalHelper;
  @Nullable
  private OrientationHelper mVerticalHelper;
  
  private float computeDistancePerChild(RecyclerView.LayoutManager paramLayoutManager, OrientationHelper paramOrientationHelper)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    int i = Integer.MAX_VALUE;
    int k = Integer.MIN_VALUE;
    int i2 = paramLayoutManager.getChildCount();
    float f;
    if (i2 == 0) {
      f = 1.0F;
    }
    for (;;)
    {
      return f;
      int m = 0;
      int j;
      if (m < i2)
      {
        View localView = paramLayoutManager.getChildAt(m);
        int n = paramLayoutManager.getPosition(localView);
        Object localObject3;
        int i1;
        if (n == -1)
        {
          localObject3 = localObject1;
          i1 = k;
        }
        for (;;)
        {
          m++;
          k = i1;
          localObject1 = localObject3;
          break;
          j = i;
          if (n < i)
          {
            j = n;
            localObject1 = localView;
          }
          i1 = k;
          i = j;
          localObject3 = localObject1;
          if (n > k)
          {
            i1 = n;
            localObject2 = localView;
            i = j;
            localObject3 = localObject1;
          }
        }
      }
      if ((localObject1 == null) || (localObject2 == null))
      {
        f = 1.0F;
      }
      else
      {
        j = Math.min(paramOrientationHelper.getDecoratedStart((View)localObject1), paramOrientationHelper.getDecoratedStart((View)localObject2));
        j = Math.max(paramOrientationHelper.getDecoratedEnd((View)localObject1), paramOrientationHelper.getDecoratedEnd((View)localObject2)) - j;
        if (j == 0) {
          f = 1.0F;
        } else {
          f = 1.0F * j / (k - i + 1);
        }
      }
    }
  }
  
  private int distanceToCenter(@NonNull RecyclerView.LayoutManager paramLayoutManager, @NonNull View paramView, OrientationHelper paramOrientationHelper)
  {
    int k = paramOrientationHelper.getDecoratedStart(paramView);
    int j = paramOrientationHelper.getDecoratedMeasurement(paramView) / 2;
    if (paramLayoutManager.getClipToPadding()) {}
    for (int i = paramOrientationHelper.getStartAfterPadding() + paramOrientationHelper.getTotalSpace() / 2;; i = paramOrientationHelper.getEnd() / 2) {
      return k + j - i;
    }
  }
  
  private int estimateNextPositionDiffForFling(RecyclerView.LayoutManager paramLayoutManager, OrientationHelper paramOrientationHelper, int paramInt1, int paramInt2)
  {
    int i = 0;
    int[] arrayOfInt = calculateScrollDistance(paramInt1, paramInt2);
    float f = computeDistancePerChild(paramLayoutManager, paramOrientationHelper);
    if (f <= 0.0F) {
      paramInt1 = i;
    }
    for (;;)
    {
      return paramInt1;
      if (Math.abs(arrayOfInt[0]) > Math.abs(arrayOfInt[1])) {}
      for (paramInt1 = arrayOfInt[0];; paramInt1 = arrayOfInt[1])
      {
        if (paramInt1 <= 0) {
          break label80;
        }
        paramInt1 = (int)Math.floor(paramInt1 / f);
        break;
      }
      label80:
      paramInt1 = (int)Math.ceil(paramInt1 / f);
    }
  }
  
  @Nullable
  private View findCenterView(RecyclerView.LayoutManager paramLayoutManager, OrientationHelper paramOrientationHelper)
  {
    int i1 = paramLayoutManager.getChildCount();
    Object localObject2;
    if (i1 == 0)
    {
      localObject2 = null;
      return (View)localObject2;
    }
    Object localObject1 = null;
    if (paramLayoutManager.getClipToPadding()) {}
    for (int i = paramOrientationHelper.getStartAfterPadding() + paramOrientationHelper.getTotalSpace() / 2;; i = paramOrientationHelper.getEnd() / 2)
    {
      int k = Integer.MAX_VALUE;
      int j = 0;
      for (;;)
      {
        localObject2 = localObject1;
        if (j >= i1) {
          break;
        }
        localObject2 = paramLayoutManager.getChildAt(j);
        int n = Math.abs(paramOrientationHelper.getDecoratedStart((View)localObject2) + paramOrientationHelper.getDecoratedMeasurement((View)localObject2) / 2 - i);
        int m = k;
        if (n < k)
        {
          m = n;
          localObject1 = localObject2;
        }
        j++;
        k = m;
      }
    }
  }
  
  @NonNull
  private OrientationHelper getHorizontalHelper(@NonNull RecyclerView.LayoutManager paramLayoutManager)
  {
    if ((this.mHorizontalHelper == null) || (this.mHorizontalHelper.mLayoutManager != paramLayoutManager)) {
      this.mHorizontalHelper = OrientationHelper.createHorizontalHelper(paramLayoutManager);
    }
    return this.mHorizontalHelper;
  }
  
  @NonNull
  private OrientationHelper getVerticalHelper(@NonNull RecyclerView.LayoutManager paramLayoutManager)
  {
    if ((this.mVerticalHelper == null) || (this.mVerticalHelper.mLayoutManager != paramLayoutManager)) {
      this.mVerticalHelper = OrientationHelper.createVerticalHelper(paramLayoutManager);
    }
    return this.mVerticalHelper;
  }
  
  public int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager paramLayoutManager, @NonNull View paramView)
  {
    int[] arrayOfInt = new int[2];
    if (paramLayoutManager.canScrollHorizontally())
    {
      arrayOfInt[0] = distanceToCenter(paramLayoutManager, paramView, getHorizontalHelper(paramLayoutManager));
      if (!paramLayoutManager.canScrollVertically()) {
        break label55;
      }
      arrayOfInt[1] = distanceToCenter(paramLayoutManager, paramView, getVerticalHelper(paramLayoutManager));
    }
    for (;;)
    {
      return arrayOfInt;
      arrayOfInt[0] = 0;
      break;
      label55:
      arrayOfInt[1] = 0;
    }
  }
  
  public View findSnapView(RecyclerView.LayoutManager paramLayoutManager)
  {
    if (paramLayoutManager.canScrollVertically()) {
      paramLayoutManager = findCenterView(paramLayoutManager, getVerticalHelper(paramLayoutManager));
    }
    for (;;)
    {
      return paramLayoutManager;
      if (paramLayoutManager.canScrollHorizontally()) {
        paramLayoutManager = findCenterView(paramLayoutManager, getHorizontalHelper(paramLayoutManager));
      } else {
        paramLayoutManager = null;
      }
    }
  }
  
  public int findTargetSnapPosition(RecyclerView.LayoutManager paramLayoutManager, int paramInt1, int paramInt2)
  {
    int j = -1;
    int i;
    if (!(paramLayoutManager instanceof RecyclerView.SmoothScroller.ScrollVectorProvider)) {
      i = j;
    }
    int k;
    Object localObject;
    int m;
    do
    {
      do
      {
        do
        {
          do
          {
            return i;
            k = paramLayoutManager.getItemCount();
            i = j;
          } while (k == 0);
          localObject = findSnapView(paramLayoutManager);
          i = j;
        } while (localObject == null);
        m = paramLayoutManager.getPosition((View)localObject);
        i = j;
      } while (m == -1);
      localObject = ((RecyclerView.SmoothScroller.ScrollVectorProvider)paramLayoutManager).computeScrollVectorForPosition(k - 1);
      i = j;
    } while (localObject == null);
    if (paramLayoutManager.canScrollHorizontally())
    {
      i = estimateNextPositionDiffForFling(paramLayoutManager, getHorizontalHelper(paramLayoutManager), paramInt1, 0);
      paramInt1 = i;
      if (((PointF)localObject).x < 0.0F) {
        paramInt1 = -i;
      }
      label128:
      if (!paramLayoutManager.canScrollVertically()) {
        break label219;
      }
      i = estimateNextPositionDiffForFling(paramLayoutManager, getVerticalHelper(paramLayoutManager), 0, paramInt2);
      paramInt2 = i;
      if (((PointF)localObject).y < 0.0F) {
        paramInt2 = -i;
      }
      label166:
      if (!paramLayoutManager.canScrollVertically()) {
        break label224;
      }
      paramInt1 = paramInt2;
    }
    label219:
    label224:
    for (;;)
    {
      i = j;
      if (paramInt1 == 0) {
        break;
      }
      paramInt2 = m + paramInt1;
      paramInt1 = paramInt2;
      if (paramInt2 < 0) {
        paramInt1 = 0;
      }
      i = paramInt1;
      if (paramInt1 < k) {
        break;
      }
      i = k - 1;
      break;
      paramInt1 = 0;
      break label128;
      paramInt2 = 0;
      break label166;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v7\widget\LinearSnapHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */