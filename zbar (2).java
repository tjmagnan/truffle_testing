package me.dm7.barcodescanner.zbar;

public final class R
{
  public static final class attr
  {
    public static final int borderAlpha = 2130772212;
    public static final int borderColor = 2130772205;
    public static final int borderLength = 2130772208;
    public static final int borderWidth = 2130772207;
    public static final int cornerRadius = 2130772210;
    public static final int finderOffset = 2130772213;
    public static final int laserColor = 2130772204;
    public static final int laserEnabled = 2130772203;
    public static final int maskColor = 2130772206;
    public static final int roundedCorner = 2130772209;
    public static final int shouldScaleToFill = 2130772202;
    public static final int squaredFinder = 2130772211;
  }
  
  public static final class color
  {
    public static final int viewfinder_border = 2131492977;
    public static final int viewfinder_laser = 2131492980;
    public static final int viewfinder_mask = 2131492981;
  }
  
  public static final class integer
  {
    public static final int viewfinder_border_length = 2131296256;
    public static final int viewfinder_border_width = 2131296257;
  }
  
  public static final class styleable
  {
    public static final int[] BarcodeScannerView = { 2130772202, 2130772203, 2130772204, 2130772205, 2130772206, 2130772207, 2130772208, 2130772209, 2130772210, 2130772211, 2130772212, 2130772213 };
    public static final int BarcodeScannerView_borderAlpha = 10;
    public static final int BarcodeScannerView_borderColor = 3;
    public static final int BarcodeScannerView_borderLength = 6;
    public static final int BarcodeScannerView_borderWidth = 5;
    public static final int BarcodeScannerView_cornerRadius = 8;
    public static final int BarcodeScannerView_finderOffset = 11;
    public static final int BarcodeScannerView_laserColor = 2;
    public static final int BarcodeScannerView_laserEnabled = 1;
    public static final int BarcodeScannerView_maskColor = 4;
    public static final int BarcodeScannerView_roundedCorner = 7;
    public static final int BarcodeScannerView_shouldScaleToFill = 0;
    public static final int BarcodeScannerView_squaredFinder = 9;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\zbar\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */