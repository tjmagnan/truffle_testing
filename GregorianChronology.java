package org.joda.time.chrono;

import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeZone;

public final class GregorianChronology
  extends BasicGJChronology
{
  private static final int DAYS_0000_TO_1970 = 719527;
  private static final GregorianChronology INSTANCE_UTC = getInstance(DateTimeZone.UTC);
  private static final int MAX_YEAR = 292278993;
  private static final long MILLIS_PER_MONTH = 2629746000L;
  private static final long MILLIS_PER_YEAR = 31556952000L;
  private static final int MIN_YEAR = -292275054;
  private static final ConcurrentHashMap<DateTimeZone, GregorianChronology[]> cCache = new ConcurrentHashMap();
  private static final long serialVersionUID = -861407383323710522L;
  
  private GregorianChronology(Chronology paramChronology, Object paramObject, int paramInt)
  {
    super(paramChronology, paramObject, paramInt);
  }
  
  public static GregorianChronology getInstance()
  {
    return getInstance(DateTimeZone.getDefault(), 4);
  }
  
  public static GregorianChronology getInstance(DateTimeZone paramDateTimeZone)
  {
    return getInstance(paramDateTimeZone, 4);
  }
  
  public static GregorianChronology getInstance(DateTimeZone paramDateTimeZone, int paramInt)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    paramDateTimeZone = (GregorianChronology[])cCache.get(localDateTimeZone);
    if (paramDateTimeZone == null)
    {
      paramDateTimeZone = new GregorianChronology[7];
      Object localObject1 = (GregorianChronology[])cCache.putIfAbsent(localDateTimeZone, paramDateTimeZone);
      if (localObject1 != null) {
        paramDateTimeZone = (DateTimeZone)localObject1;
      }
      for (;;)
      {
        Object localObject3 = paramDateTimeZone[(paramInt - 1)];
        localObject1 = localObject3;
        if (localObject3 == null)
        {
          localObject3 = paramDateTimeZone[(paramInt - 1)];
          localObject1 = localObject3;
          if (localObject3 != null) {}
        }
        try
        {
          if (localDateTimeZone == DateTimeZone.UTC)
          {
            localObject1 = new org/joda/time/chrono/GregorianChronology;
            ((GregorianChronology)localObject1).<init>(null, null, paramInt);
          }
          for (;;)
          {
            paramDateTimeZone[(paramInt - 1)] = localObject1;
            return (GregorianChronology)localObject1;
            localObject1 = new GregorianChronology(ZonedChronology.getInstance(getInstance(DateTimeZone.UTC, paramInt), localDateTimeZone), null, paramInt);
          }
        }
        finally {}
      }
    }
  }
  
  public static GregorianChronology getInstanceUTC()
  {
    return INSTANCE_UTC;
  }
  
  private Object readResolve()
  {
    Object localObject = getBase();
    int j = getMinimumDaysInFirstWeek();
    int i = j;
    if (j == 0) {
      i = 4;
    }
    if (localObject == null) {}
    for (localObject = getInstance(DateTimeZone.UTC, i);; localObject = getInstance(((Chronology)localObject).getZone(), i)) {
      return localObject;
    }
  }
  
  protected void assemble(AssembledChronology.Fields paramFields)
  {
    if (getBase() == null) {
      super.assemble(paramFields);
    }
  }
  
  long calculateFirstDayOfYearMillis(int paramInt)
  {
    int i = paramInt / 100;
    if (paramInt < 0) {
      i = (i + 3 >> 2) + ((paramInt + 3 >> 2) - i) - 1;
    }
    for (;;)
    {
      long l = paramInt;
      return (i - 719527 + l * 365L) * 86400000L;
      int j = (i >> 2) + ((paramInt >> 2) - i);
      i = j;
      if (isLeapYear(paramInt)) {
        i = j - 1;
      }
    }
  }
  
  long getApproxMillisAtEpochDividedByTwo()
  {
    return 31083597720000L;
  }
  
  long getAverageMillisPerMonth()
  {
    return 2629746000L;
  }
  
  long getAverageMillisPerYear()
  {
    return 31556952000L;
  }
  
  long getAverageMillisPerYearDividedByTwo()
  {
    return 15778476000L;
  }
  
  int getMaxYear()
  {
    return 292278993;
  }
  
  int getMinYear()
  {
    return -292275054;
  }
  
  boolean isLeapYear(int paramInt)
  {
    if (((paramInt & 0x3) == 0) && ((paramInt % 100 != 0) || (paramInt % 400 == 0))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Chronology withUTC()
  {
    return INSTANCE_UTC;
  }
  
  public Chronology withZone(DateTimeZone paramDateTimeZone)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    if (localDateTimeZone == getZone()) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = getInstance(localDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\GregorianChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */