package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PBPrintingOptions
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = -3137032031283563816L;
  @Expose
  @SerializedName("s4x6")
  private List<PBLabelPrint> s4x6 = new ArrayList();
  @Expose
  @SerializedName("s8x11")
  private List<PBLabelPrint> s8x11 = new ArrayList();
  
  public PBPrintingOptions() {}
  
  public PBPrintingOptions(List<PBLabelPrint> paramList1, List<PBLabelPrint> paramList2)
  {
    this.s4x6 = paramList1;
    this.s8x11 = paramList2;
  }
  
  public List<PBLabelPrint> getS4x6()
  {
    return this.s4x6;
  }
  
  public List<PBLabelPrint> getS8x11()
  {
    return this.s8x11;
  }
  
  public void setS4x6(List<PBLabelPrint> paramList)
  {
    this.s4x6 = paramList;
  }
  
  public void setS8x11(List<PBLabelPrint> paramList)
  {
    this.s8x11 = paramList;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBPrintingOptions.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */