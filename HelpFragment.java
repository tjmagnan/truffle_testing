package tech.dcube.companion;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.ButterKnife;
import tech.dcube.companion.fragments.PBTopBarFragment;

public class HelpFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private OnHelpFragmentInteractionListener mListener;
  EditText mMessageET;
  private String mParam1;
  private String mParam2;
  
  public static HelpFragment newInstance()
  {
    HelpFragment localHelpFragment = new HelpFragment();
    localHelpFragment.setArguments(new Bundle());
    return localHelpFragment;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnHelpFragmentInteractionListener))
    {
      this.mListener = ((OnHelpFragmentInteractionListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement ");
  }
  
  public void onButtonPressed(Uri paramUri)
  {
    if (this.mListener != null) {
      this.mListener.onHelpInteraction(paramUri.toString());
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    this.mListener.onHelpInteraction("back");
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968628, paramViewGroup, false);
    this.mListener = ((OnHelpFragmentInteractionListener)getActivity());
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Help");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    this.mMessageET = ((EditText)paramLayoutInflater.findViewById(2131624108));
    this.mMessageET.requestFocus();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public static abstract interface OnHelpFragmentInteractionListener
  {
    public abstract void onHelpInteraction(String paramString);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\HelpFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */