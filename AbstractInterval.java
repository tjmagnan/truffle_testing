package org.joda.time.base;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.MutableInterval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.ReadableInstant;
import org.joda.time.ReadableInterval;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public abstract class AbstractInterval
  implements ReadableInterval
{
  protected void checkInterval(long paramLong1, long paramLong2)
  {
    if (paramLong2 < paramLong1) {
      throw new IllegalArgumentException("The end instant must be greater than the start instant");
    }
  }
  
  public boolean contains(long paramLong)
  {
    long l2 = getStartMillis();
    long l1 = getEndMillis();
    if ((paramLong >= l2) && (paramLong < l1)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean contains(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (boolean bool = containsNow();; bool = contains(paramReadableInstant.getMillis())) {
      return bool;
    }
  }
  
  public boolean contains(ReadableInterval paramReadableInterval)
  {
    boolean bool;
    if (paramReadableInterval == null) {
      bool = containsNow();
    }
    for (;;)
    {
      return bool;
      long l1 = paramReadableInterval.getStartMillis();
      long l2 = paramReadableInterval.getEndMillis();
      long l4 = getStartMillis();
      long l3 = getEndMillis();
      if ((l4 <= l1) && (l1 < l3) && (l2 <= l3)) {
        bool = true;
      } else {
        bool = false;
      }
    }
  }
  
  public boolean containsNow()
  {
    return contains(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (!(paramObject instanceof ReadableInterval))
      {
        bool = false;
      }
      else
      {
        paramObject = (ReadableInterval)paramObject;
        if ((getStartMillis() != ((ReadableInterval)paramObject).getStartMillis()) || (getEndMillis() != ((ReadableInterval)paramObject).getEndMillis()) || (!FieldUtils.equals(getChronology(), ((ReadableInterval)paramObject).getChronology()))) {
          bool = false;
        }
      }
    }
  }
  
  public DateTime getEnd()
  {
    return new DateTime(getEndMillis(), getChronology());
  }
  
  public DateTime getStart()
  {
    return new DateTime(getStartMillis(), getChronology());
  }
  
  public int hashCode()
  {
    long l2 = getStartMillis();
    long l1 = getEndMillis();
    return (((int)(l2 ^ l2 >>> 32) + 3007) * 31 + (int)(l1 ^ l1 >>> 32)) * 31 + getChronology().hashCode();
  }
  
  public boolean isAfter(long paramLong)
  {
    if (getStartMillis() > paramLong) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isAfter(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (boolean bool = isAfterNow();; bool = isAfter(paramReadableInstant.getMillis())) {
      return bool;
    }
  }
  
  public boolean isAfter(ReadableInterval paramReadableInterval)
  {
    long l;
    if (paramReadableInterval == null)
    {
      l = DateTimeUtils.currentTimeMillis();
      if (getStartMillis() < l) {
        break label31;
      }
    }
    label31:
    for (boolean bool = true;; bool = false)
    {
      return bool;
      l = paramReadableInterval.getEndMillis();
      break;
    }
  }
  
  public boolean isAfterNow()
  {
    return isAfter(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean isBefore(long paramLong)
  {
    if (getEndMillis() <= paramLong) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean isBefore(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (boolean bool = isBeforeNow();; bool = isBefore(paramReadableInstant.getMillis())) {
      return bool;
    }
  }
  
  public boolean isBefore(ReadableInterval paramReadableInterval)
  {
    if (paramReadableInterval == null) {}
    for (boolean bool = isBeforeNow();; bool = isBefore(paramReadableInterval.getStartMillis())) {
      return bool;
    }
  }
  
  public boolean isBeforeNow()
  {
    return isBefore(DateTimeUtils.currentTimeMillis());
  }
  
  public boolean isEqual(ReadableInterval paramReadableInterval)
  {
    if ((getStartMillis() == paramReadableInterval.getStartMillis()) && (getEndMillis() == paramReadableInterval.getEndMillis())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean overlaps(ReadableInterval paramReadableInterval)
  {
    boolean bool = true;
    long l1 = getStartMillis();
    long l2 = getEndMillis();
    long l3;
    if (paramReadableInterval == null)
    {
      l3 = DateTimeUtils.currentTimeMillis();
      if ((l1 >= l3) || (l3 >= l2)) {}
    }
    for (;;)
    {
      return bool;
      bool = false;
      continue;
      l3 = paramReadableInterval.getStartMillis();
      if ((l1 >= paramReadableInterval.getEndMillis()) || (l3 >= l2)) {
        bool = false;
      }
    }
  }
  
  public Duration toDuration()
  {
    long l = toDurationMillis();
    if (l == 0L) {}
    for (Duration localDuration = Duration.ZERO;; localDuration = new Duration(l)) {
      return localDuration;
    }
  }
  
  public long toDurationMillis()
  {
    return FieldUtils.safeSubtract(getEndMillis(), getStartMillis());
  }
  
  public Interval toInterval()
  {
    return new Interval(getStartMillis(), getEndMillis(), getChronology());
  }
  
  public MutableInterval toMutableInterval()
  {
    return new MutableInterval(getStartMillis(), getEndMillis(), getChronology());
  }
  
  public Period toPeriod()
  {
    return new Period(getStartMillis(), getEndMillis(), getChronology());
  }
  
  public Period toPeriod(PeriodType paramPeriodType)
  {
    return new Period(getStartMillis(), getEndMillis(), paramPeriodType, getChronology());
  }
  
  public String toString()
  {
    DateTimeFormatter localDateTimeFormatter = ISODateTimeFormat.dateTime().withChronology(getChronology());
    StringBuffer localStringBuffer = new StringBuffer(48);
    localDateTimeFormatter.printTo(localStringBuffer, getStartMillis());
    localStringBuffer.append('/');
    localDateTimeFormatter.printTo(localStringBuffer, getEndMillis());
    return localStringBuffer.toString();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\base\AbstractInterval.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */