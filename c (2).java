package com.testfairy.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.testfairy.e;
import com.testfairy.p.t;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class b
{
  private static final int e = 75;
  private int a = -1;
  private Camera b = null;
  private byte[] c = null;
  private boolean d = false;
  private Timer f;
  private SurfaceView g;
  private ViewGroup h;
  private WindowManager i;
  private int j = 0;
  private a k;
  private c l = new c(null);
  private a m = new a(null);
  private int n = 0;
  private int o = 0;
  
  public b(Context paramContext, a parama)
  {
    this.k = parama;
    this.i = ((WindowManager)paramContext.getSystemService("window"));
  }
  
  private int a(int paramInt)
  {
    int i2 = 0;
    int i3 = this.i.getDefaultDisplay().getRotation();
    Log.d(e.a, "Camera surface changed, new rotation = " + i3);
    int i1 = i2;
    Camera.CameraInfo localCameraInfo;
    switch (i3)
    {
    default: 
      i1 = i2;
    case 0: 
      localCameraInfo = new Camera.CameraInfo();
      Camera.getCameraInfo(paramInt, localCameraInfo);
      if (localCameraInfo.facing != 1) {
        break;
      }
    }
    for (paramInt = (360 - (i1 + localCameraInfo.orientation) % 360) % 360;; paramInt = (localCameraInfo.orientation - i1 + 360) % 360)
    {
      return paramInt;
      i1 = 90;
      break;
      i1 = 180;
      break;
      i1 = 270;
      break;
    }
  }
  
  private void b(Context paramContext)
  {
    int i2 = 0;
    int i5 = 75;
    Object localObject5;
    int i1;
    int i4;
    int i3;
    try
    {
      this.a = d();
      localObject1 = e.a;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Log.v((String)localObject1, "Found front camera at " + this.a);
      this.b = Camera.open(this.a);
      localObject1 = this.b.getParameters();
      localObject2 = e.a;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      Log.v((String)localObject2, "Params: " + ((Camera.Parameters)localObject1).getPictureSize().width + " x " + ((Camera.Parameters)localObject1).getPictureSize().height);
      localObject5 = ((Camera.Parameters)localObject1).getSupportedPictureSizes().iterator();
      i1 = 0;
      while (((Iterator)localObject5).hasNext())
      {
        localObject2 = (Camera.Size)((Iterator)localObject5).next();
        if (i1 != 0)
        {
          i4 = i2;
          i3 = i1;
          if (((Camera.Size)localObject2).width * ((Camera.Size)localObject2).height >= i1 * i2) {}
        }
        else
        {
          i3 = ((Camera.Size)localObject2).width;
          i4 = ((Camera.Size)localObject2).height;
        }
        localObject3 = e.a;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        Log.v((String)localObject3, "+ Supported " + ((Camera.Size)localObject2).width + "x" + ((Camera.Size)localObject2).height);
        i2 = i4;
        i1 = i3;
        continue;
        return;
      }
    }
    catch (Exception paramContext)
    {
      Log.e(e.a, "Exception", paramContext);
    }
    this.n = i1;
    this.o = i2;
    Object localObject4 = ((Camera.Parameters)localObject1).getSupportedPreviewFpsRange().iterator();
    while (((Iterator)localObject4).hasNext())
    {
      localObject5 = (int[])((Iterator)localObject4).next();
      localObject3 = e.a;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      Log.v((String)localObject3, "Preview rates: " + localObject5[0] / 1000 + " to " + localObject5[1] / 1000);
    }
    Object localObject2 = e.a;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    Log.v((String)localObject2, "Picked up lowest resolution of " + i1 + "x" + i2);
    ((Camera.Parameters)localObject1).setPreviewSize(i1, i2);
    this.b.setParameters((Camera.Parameters)localObject1);
    Object localObject1 = new android/view/SurfaceView;
    ((SurfaceView)localObject1).<init>(paramContext);
    this.g = ((SurfaceView)localObject1);
    localObject1 = this.g.getHolder();
    ((SurfaceHolder)localObject1).addCallback(this.m);
    ((SurfaceHolder)localObject1).setType(3);
    this.j = a(this.a);
    localObject3 = e.a;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Log.d((String)localObject3, "Using displayOrientation=" + this.j);
    this.b.setDisplayOrientation(this.j);
    if (this.j != 90)
    {
      i4 = i2;
      i3 = i1;
      if (this.j != 270) {}
    }
    else
    {
      i3 = i2;
      i4 = i1;
    }
    float f1;
    if ((i3 > 75) || (i4 > 75))
    {
      f1 = i3 / i4;
      if (f1 >= 1.0D)
      {
        i1 = (int)(75.0F / f1);
        i2 = i5;
      }
    }
    for (;;)
    {
      f1 = paramContext.getResources().getDisplayMetrics().density;
      localObject2 = e.a;
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>();
      Log.d((String)localObject2, "Using " + f1 + " dpi => " + (int)(i2 * f1) + "x" + (int)(i1 * f1));
      ((SurfaceHolder)localObject1).setFixedSize((int)(i2 * f1), (int)(i1 * f1));
      this.c = new byte[i3 * i4 * 3];
      this.b.addCallbackBuffer(this.c);
      paramContext = new com/testfairy/c/b$b;
      paramContext.<init>(this, this.k);
      this.b.setPreviewCallbackWithBuffer(paramContext);
      break;
      i2 = (int)(f1 * 75.0F);
      i1 = 75;
      continue;
      i1 = i4;
      i2 = i3;
    }
  }
  
  private void c()
  {
    if (this.b != null)
    {
      this.b.release();
      this.b = null;
    }
    this.d = false;
  }
  
  private int d()
  {
    Camera.CameraInfo localCameraInfo = new Camera.CameraInfo();
    int i1 = 0;
    if (i1 < Camera.getNumberOfCameras())
    {
      Camera.getCameraInfo(i1, localCameraInfo);
      if (localCameraInfo.facing != 1) {}
    }
    for (;;)
    {
      return i1;
      i1++;
      break;
      i1 = -1;
    }
  }
  
  public void a()
  {
    if ((this.h != null) && (this.g != null)) {
      this.h.removeView(this.g);
    }
    if (this.f != null)
    {
      this.f.cancel();
      this.f = null;
    }
    this.h = null;
  }
  
  public void a(Context paramContext)
  {
    b(paramContext);
  }
  
  public void a(ViewGroup paramViewGroup)
  {
    if (this.h == null)
    {
      this.h = paramViewGroup;
      paramViewGroup.addView(this.g);
    }
    if (this.f == null)
    {
      this.f = new Timer();
      this.f.schedule(this.l, 0L, 1000L);
    }
  }
  
  public void b()
  {
    a();
    c();
  }
  
  private class a
    implements SurfaceHolder.Callback
  {
    private a() {}
    
    public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
    {
      try
      {
        String str = e.a;
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        Log.v(str, "surfaceChanged camera=" + b.a(b.this) + " width=" + paramInt2 + " height=" + paramInt3);
        if (b.a(b.this) == null) {
          Log.e(e.a, "Camera not initialized, returning");
        }
        for (;;)
        {
          return;
          Log.d(e.a, "Starting preview");
          b.a(b.this).setPreviewDisplay(paramSurfaceHolder);
          b.a(b.this).startPreview();
        }
      }
      catch (Exception paramSurfaceHolder)
      {
        for (;;)
        {
          Log.e(e.a, "Exception", paramSurfaceHolder);
        }
      }
    }
    
    public void surfaceCreated(SurfaceHolder paramSurfaceHolder) {}
    
    public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder) {}
  }
  
  private class b
    implements Camera.PreviewCallback
  {
    private int[] b = null;
    private final a c;
    
    public b(a parama)
    {
      this.c = parama;
    }
    
    public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
    {
      if (b.a(b.this) == null) {}
      for (;;)
      {
        return;
        try
        {
          long l = System.currentTimeMillis();
          int i = paramCamera.getParameters().getPreviewSize().width;
          int k = paramCamera.getParameters().getPreviewSize().height;
          int j = i * k;
          if ((this.b == null) || (this.b.length != j)) {
            this.b = new int[j];
          }
          t.a(this.b, paramArrayOfByte, i, k);
          paramCamera = Bitmap.createBitmap(i, k, Bitmap.Config.ARGB_8888);
          paramCamera.setPixels(this.b, 0, i, 0, 0, i, k);
          paramArrayOfByte = paramCamera;
          if (b.b(b.this) != 0) {
            paramArrayOfByte = com.testfairy.p.b.a(paramCamera, 360 - b.b(b.this));
          }
          this.c.a(l, paramArrayOfByte);
        }
        catch (Exception paramArrayOfByte)
        {
          for (;;)
          {
            Log.e(e.a, "Exception", paramArrayOfByte);
          }
        }
        b.a(b.this, true);
      }
    }
  }
  
  private class c
    extends TimerTask
  {
    private c() {}
    
    public void run()
    {
      if ((b.c(b.this)) && (b.a(b.this) != null))
      {
        b.a(b.this, false);
        b.a(b.this).addCallbackBuffer(b.d(b.this));
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\c\b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */