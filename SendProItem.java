package tech.dcube.companion.model;

public class SendProItem
{
  private double SendproAmount;
  private String SendproName;
  
  public SendProItem(String paramString, double paramDouble)
  {
    this.SendproName = paramString;
    this.SendproAmount = paramDouble;
  }
  
  public double getSendproAmount()
  {
    return this.SendproAmount;
  }
  
  public String getSendproName()
  {
    return this.SendproName;
  }
  
  public void setSendproAmount(double paramDouble)
  {
    this.SendproAmount = paramDouble;
  }
  
  public void setSendproName(String paramString)
  {
    this.SendproName = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\SendProItem.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */