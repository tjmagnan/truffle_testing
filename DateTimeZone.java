package org.joda.time;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import org.joda.convert.FromString;
import org.joda.convert.ToString;
import org.joda.time.chrono.BaseChronology;
import org.joda.time.field.FieldUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.FormatUtils;
import org.joda.time.tz.DefaultNameProvider;
import org.joda.time.tz.FixedDateTimeZone;
import org.joda.time.tz.NameProvider;
import org.joda.time.tz.Provider;
import org.joda.time.tz.UTCProvider;
import org.joda.time.tz.ZoneInfoProvider;

public abstract class DateTimeZone
  implements Serializable
{
  private static final int MAX_MILLIS = 86399999;
  public static final DateTimeZone UTC = UTCDateTimeZone.INSTANCE;
  private static final AtomicReference<DateTimeZone> cDefault = new AtomicReference();
  private static final AtomicReference<NameProvider> cNameProvider;
  private static final AtomicReference<Provider> cProvider = new AtomicReference();
  private static final long serialVersionUID = 5546345482340108586L;
  private final String iID;
  
  static
  {
    cNameProvider = new AtomicReference();
  }
  
  protected DateTimeZone(String paramString)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("Id must not be null");
    }
    this.iID = paramString;
  }
  
  private static String convertToAsciiNumber(String paramString)
  {
    paramString = new StringBuilder(paramString);
    for (int i = 0; i < paramString.length(); i++)
    {
      int j = Character.digit(paramString.charAt(i), 10);
      if (j >= 0) {
        paramString.setCharAt(i, (char)(j + 48));
      }
    }
    return paramString.toString();
  }
  
  private static DateTimeZone fixedOffsetZone(String paramString, int paramInt)
  {
    if (paramInt == 0) {}
    for (paramString = UTC;; paramString = new FixedDateTimeZone(paramString, null, paramInt, paramInt)) {
      return paramString;
    }
  }
  
  @FromString
  public static DateTimeZone forID(String paramString)
  {
    Object localObject;
    if (paramString == null) {
      localObject = getDefault();
    }
    for (;;)
    {
      return (DateTimeZone)localObject;
      if (paramString.equals("UTC"))
      {
        localObject = UTC;
      }
      else
      {
        DateTimeZone localDateTimeZone = getProvider().getZone(paramString);
        localObject = localDateTimeZone;
        if (localDateTimeZone == null)
        {
          if ((!paramString.startsWith("+")) && (!paramString.startsWith("-"))) {
            break;
          }
          int i = parseOffset(paramString);
          if (i == 0L) {
            localObject = UTC;
          } else {
            localObject = fixedOffsetZone(printOffset(i), i);
          }
        }
      }
    }
    throw new IllegalArgumentException("The datetime zone id '" + paramString + "' is not recognised");
  }
  
  public static DateTimeZone forOffsetHours(int paramInt)
    throws IllegalArgumentException
  {
    return forOffsetHoursMinutes(paramInt, 0);
  }
  
  public static DateTimeZone forOffsetHoursMinutes(int paramInt1, int paramInt2)
    throws IllegalArgumentException
  {
    DateTimeZone localDateTimeZone;
    if ((paramInt1 == 0) && (paramInt2 == 0))
    {
      localDateTimeZone = UTC;
      return localDateTimeZone;
    }
    if ((paramInt1 < -23) || (paramInt1 > 23)) {
      throw new IllegalArgumentException("Hours out of range: " + paramInt1);
    }
    if ((paramInt2 < -59) || (paramInt2 > 59)) {
      throw new IllegalArgumentException("Minutes out of range: " + paramInt2);
    }
    if ((paramInt1 > 0) && (paramInt2 < 0)) {
      throw new IllegalArgumentException("Positive hours must not have negative minutes: " + paramInt2);
    }
    paramInt1 *= 60;
    if (paramInt1 < 0) {}
    for (;;)
    {
      try
      {
        paramInt1 -= Math.abs(paramInt2);
        paramInt1 = FieldUtils.safeMultiply(paramInt1, 60000);
        localDateTimeZone = forOffsetMillis(paramInt1);
      }
      catch (ArithmeticException localArithmeticException)
      {
        throw new IllegalArgumentException("Offset is too large");
      }
      paramInt1 += paramInt2;
    }
  }
  
  public static DateTimeZone forOffsetMillis(int paramInt)
  {
    if ((paramInt < -86399999) || (paramInt > 86399999)) {
      throw new IllegalArgumentException("Millis out of range: " + paramInt);
    }
    return fixedOffsetZone(printOffset(paramInt), paramInt);
  }
  
  public static DateTimeZone forTimeZone(TimeZone paramTimeZone)
  {
    if (paramTimeZone == null) {
      paramTimeZone = getDefault();
    }
    String str2;
    for (;;)
    {
      return paramTimeZone;
      str2 = paramTimeZone.getID();
      if (str2 == null) {
        throw new IllegalArgumentException("The TimeZone id must not be null");
      }
      if (str2.equals("UTC"))
      {
        paramTimeZone = UTC;
      }
      else
      {
        paramTimeZone = null;
        String str1 = getConvertedId(str2);
        Provider localProvider = getProvider();
        if (str1 != null) {
          paramTimeZone = localProvider.getZone(str1);
        }
        Object localObject = paramTimeZone;
        if (paramTimeZone == null) {
          localObject = localProvider.getZone(str2);
        }
        paramTimeZone = (TimeZone)localObject;
        if (localObject == null)
        {
          if ((str1 != null) || ((!str2.startsWith("GMT+")) && (!str2.startsWith("GMT-")))) {
            break;
          }
          localObject = str2.substring(3);
          paramTimeZone = (TimeZone)localObject;
          if (((String)localObject).length() > 2)
          {
            char c = ((String)localObject).charAt(1);
            paramTimeZone = (TimeZone)localObject;
            if (c > '9')
            {
              paramTimeZone = (TimeZone)localObject;
              if (Character.isDigit(c)) {
                paramTimeZone = convertToAsciiNumber((String)localObject);
              }
            }
          }
          int i = parseOffset(paramTimeZone);
          if (i == 0L) {
            paramTimeZone = UTC;
          } else {
            paramTimeZone = fixedOffsetZone(printOffset(i), i);
          }
        }
      }
    }
    throw new IllegalArgumentException("The datetime zone id '" + str2 + "' is not recognised");
  }
  
  public static Set<String> getAvailableIDs()
  {
    return getProvider().getAvailableIDs();
  }
  
  private static String getConvertedId(String paramString)
  {
    return (String)LazyInit.CONVERSION_MAP.get(paramString);
  }
  
  public static DateTimeZone getDefault()
  {
    Object localObject3 = (DateTimeZone)cDefault.get();
    Object localObject5 = localObject3;
    if (localObject3 == null) {
      localObject1 = localObject3;
    }
    try
    {
      localObject5 = System.getProperty("user.timezone");
      localObject1 = localObject3;
      if (localObject5 != null)
      {
        localObject1 = localObject3;
        localObject5 = forID((String)localObject5);
        localObject1 = localObject5;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        localObject4 = localObject1;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        Object localObject4;
        Object localObject2 = localObject4;
      }
    }
    localObject3 = localObject1;
    if (localObject1 == null) {
      localObject3 = forTimeZone(TimeZone.getDefault());
    }
    localObject1 = localObject3;
    if (localObject3 == null) {
      localObject1 = UTC;
    }
    localObject5 = localObject1;
    if (!cDefault.compareAndSet(null, localObject1)) {
      localObject5 = (DateTimeZone)cDefault.get();
    }
    return (DateTimeZone)localObject5;
  }
  
  private static NameProvider getDefaultNameProvider()
  {
    for (;;)
    {
      try
      {
        localObject1 = System.getProperty("org.joda.time.DateTimeZone.NameProvider");
        if (localObject1 == null) {
          continue;
        }
      }
      catch (SecurityException localSecurityException)
      {
        Object localObject1;
        Object localObject3;
        Object localObject2 = null;
        continue;
        localObject2 = null;
        continue;
      }
      try
      {
        localObject1 = (NameProvider)Class.forName((String)localObject1).newInstance();
        localObject3 = localObject1;
        if (localObject1 == null) {
          localObject3 = new DefaultNameProvider();
        }
        return (NameProvider)localObject3;
      }
      catch (Exception localException)
      {
        localObject1 = new java/lang/RuntimeException;
        ((RuntimeException)localObject1).<init>(localException);
        throw ((Throwable)localObject1);
      }
    }
  }
  
  private static Provider getDefaultProvider()
  {
    for (;;)
    {
      try
      {
        localObject1 = System.getProperty("org.joda.time.DateTimeZone.Provider");
        if (localObject1 == null) {
          continue;
        }
      }
      catch (SecurityException localSecurityException1)
      {
        try
        {
          Object localObject1;
          String str = System.getProperty("org.joda.time.DateTimeZone.Folder");
          if (str != null) {
            try
            {
              localObject2 = new org/joda/time/tz/ZoneInfoProvider;
              File localFile = new java/io/File;
              localFile.<init>(str);
              ((ZoneInfoProvider)localObject2).<init>(localFile);
              localObject2 = validateProvider((Provider)localObject2);
            }
            catch (Exception localException3)
            {
              Object localObject2 = new java/lang/RuntimeException;
              ((RuntimeException)localObject2).<init>(localException3);
              throw ((Throwable)localObject2);
            }
          }
        }
        catch (SecurityException localSecurityException2)
        {
          try
          {
            Object localObject3 = new org/joda/time/tz/ZoneInfoProvider;
            ((ZoneInfoProvider)localObject3).<init>("org/joda/time/tz/data");
            localObject3 = validateProvider((Provider)localObject3);
          }
          catch (Exception localException1)
          {
            localException1.printStackTrace();
            UTCProvider localUTCProvider = new UTCProvider();
          }
        }
        continue;
      }
      try
      {
        localObject1 = validateProvider((Provider)Class.forName((String)localObject1).newInstance());
        return (Provider)localObject1;
      }
      catch (Exception localException2)
      {
        localObject1 = new java/lang/RuntimeException;
        ((RuntimeException)localObject1).<init>(localException2);
        throw ((Throwable)localObject1);
      }
    }
  }
  
  public static NameProvider getNameProvider()
  {
    NameProvider localNameProvider2 = (NameProvider)cNameProvider.get();
    NameProvider localNameProvider1 = localNameProvider2;
    if (localNameProvider2 == null)
    {
      localNameProvider2 = getDefaultNameProvider();
      localNameProvider1 = localNameProvider2;
      if (!cNameProvider.compareAndSet(null, localNameProvider2)) {
        localNameProvider1 = (NameProvider)cNameProvider.get();
      }
    }
    return localNameProvider1;
  }
  
  public static Provider getProvider()
  {
    Provider localProvider2 = (Provider)cProvider.get();
    Provider localProvider1 = localProvider2;
    if (localProvider2 == null)
    {
      localProvider2 = getDefaultProvider();
      localProvider1 = localProvider2;
      if (!cProvider.compareAndSet(null, localProvider2)) {
        localProvider1 = (Provider)cProvider.get();
      }
    }
    return localProvider1;
  }
  
  private static int parseOffset(String paramString)
  {
    return -(int)LazyInit.OFFSET_FORMATTER.parseMillis(paramString);
  }
  
  private static String printOffset(int paramInt)
  {
    Object localObject = new StringBuffer();
    int i;
    if (paramInt >= 0)
    {
      ((StringBuffer)localObject).append('+');
      i = paramInt / 3600000;
      FormatUtils.appendPaddedInteger((StringBuffer)localObject, i, 2);
      i = paramInt - i * 3600000;
      paramInt = i / 60000;
      ((StringBuffer)localObject).append(':');
      FormatUtils.appendPaddedInteger((StringBuffer)localObject, paramInt, 2);
      paramInt = i - paramInt * 60000;
      if (paramInt != 0) {
        break label88;
      }
      localObject = ((StringBuffer)localObject).toString();
    }
    for (;;)
    {
      return (String)localObject;
      ((StringBuffer)localObject).append('-');
      paramInt = -paramInt;
      break;
      label88:
      i = paramInt / 1000;
      ((StringBuffer)localObject).append(':');
      FormatUtils.appendPaddedInteger((StringBuffer)localObject, i, 2);
      paramInt -= i * 1000;
      if (paramInt == 0)
      {
        localObject = ((StringBuffer)localObject).toString();
      }
      else
      {
        ((StringBuffer)localObject).append('.');
        FormatUtils.appendPaddedInteger((StringBuffer)localObject, paramInt, 3);
        localObject = ((StringBuffer)localObject).toString();
      }
    }
  }
  
  public static void setDefault(DateTimeZone paramDateTimeZone)
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("DateTimeZone.setDefault"));
    }
    if (paramDateTimeZone == null) {
      throw new IllegalArgumentException("The datetime zone must not be null");
    }
    cDefault.set(paramDateTimeZone);
  }
  
  public static void setNameProvider(NameProvider paramNameProvider)
    throws SecurityException
  {
    Object localObject = System.getSecurityManager();
    if (localObject != null) {
      ((SecurityManager)localObject).checkPermission(new JodaTimePermission("DateTimeZone.setNameProvider"));
    }
    localObject = paramNameProvider;
    if (paramNameProvider == null) {
      localObject = getDefaultNameProvider();
    }
    cNameProvider.set(localObject);
  }
  
  public static void setProvider(Provider paramProvider)
    throws SecurityException
  {
    SecurityManager localSecurityManager = System.getSecurityManager();
    if (localSecurityManager != null) {
      localSecurityManager.checkPermission(new JodaTimePermission("DateTimeZone.setProvider"));
    }
    if (paramProvider == null) {
      paramProvider = getDefaultProvider();
    }
    for (;;)
    {
      cProvider.set(paramProvider);
      return;
      validateProvider(paramProvider);
    }
  }
  
  private static Provider validateProvider(Provider paramProvider)
  {
    Set localSet = paramProvider.getAvailableIDs();
    if ((localSet == null) || (localSet.size() == 0)) {
      throw new IllegalArgumentException("The provider doesn't have any available ids");
    }
    if (!localSet.contains("UTC")) {
      throw new IllegalArgumentException("The provider doesn't support UTC");
    }
    if (!UTC.equals(paramProvider.getZone("UTC"))) {
      throw new IllegalArgumentException("Invalid UTC zone provided");
    }
    return paramProvider;
  }
  
  public long adjustOffset(long paramLong, boolean paramBoolean)
  {
    long l1 = paramLong - 10800000L;
    long l2 = getOffset(l1);
    long l3 = getOffset(10800000L + paramLong);
    if (l2 <= l3) {
      l1 = paramLong;
    }
    for (;;)
    {
      return l1;
      l2 -= l3;
      long l4 = nextTransition(l1);
      l3 = l4 - l2;
      l1 = paramLong;
      if (paramLong >= l3)
      {
        l1 = paramLong;
        if (paramLong < l4 + l2) {
          if (paramLong - l3 >= l2)
          {
            l1 = paramLong;
            if (!paramBoolean) {
              l1 = paramLong - l2;
            }
          }
          else
          {
            l1 = paramLong;
            if (paramBoolean) {
              l1 = paramLong + l2;
            }
          }
        }
      }
    }
  }
  
  public long convertLocalToUTC(long paramLong, boolean paramBoolean)
  {
    long l2 = Long.MAX_VALUE;
    int i = getOffset(paramLong);
    int j = getOffset(paramLong - i);
    long l3;
    long l1;
    if ((i != j) && ((paramBoolean) || (i < 0)))
    {
      l3 = nextTransition(paramLong - i);
      l1 = l3;
      if (l3 == paramLong - i) {
        l1 = Long.MAX_VALUE;
      }
      l3 = nextTransition(paramLong - j);
      if (l3 != paramLong - j) {
        break label164;
      }
    }
    for (;;)
    {
      if (l1 != l2) {
        if (paramBoolean) {
          throw new IllegalInstantException(paramLong, getID());
        }
      }
      for (;;)
      {
        l1 = paramLong - i;
        if (((paramLong ^ l1) < 0L) && ((i ^ paramLong) < 0L)) {
          throw new ArithmeticException("Subtracting time zone offset caused overflow");
        }
        return l1;
        i = j;
      }
      label164:
      l2 = l3;
    }
  }
  
  public long convertLocalToUTC(long paramLong1, boolean paramBoolean, long paramLong2)
  {
    int i = getOffset(paramLong2);
    paramLong2 = paramLong1 - i;
    if (getOffset(paramLong2) == i) {}
    for (paramLong1 = paramLong2;; paramLong1 = convertLocalToUTC(paramLong1, paramBoolean)) {
      return paramLong1;
    }
  }
  
  public long convertUTCToLocal(long paramLong)
  {
    int i = getOffset(paramLong);
    long l = i + paramLong;
    if (((paramLong ^ l) < 0L) && ((i ^ paramLong) >= 0L)) {
      throw new ArithmeticException("Adding time zone offset caused overflow");
    }
    return l;
  }
  
  public abstract boolean equals(Object paramObject);
  
  @ToString
  public final String getID()
  {
    return this.iID;
  }
  
  public long getMillisKeepLocal(DateTimeZone paramDateTimeZone, long paramLong)
  {
    if (paramDateTimeZone == null) {
      paramDateTimeZone = getDefault();
    }
    for (;;)
    {
      if (paramDateTimeZone == this) {}
      for (;;)
      {
        return paramLong;
        paramLong = paramDateTimeZone.convertLocalToUTC(convertUTCToLocal(paramLong), false, paramLong);
      }
    }
  }
  
  public final String getName(long paramLong)
  {
    return getName(paramLong, null);
  }
  
  public String getName(long paramLong, Locale paramLocale)
  {
    Object localObject = paramLocale;
    if (paramLocale == null) {
      localObject = Locale.getDefault();
    }
    String str = getNameKey(paramLong);
    if (str == null)
    {
      paramLocale = this.iID;
      return paramLocale;
    }
    paramLocale = getNameProvider();
    if ((paramLocale instanceof DefaultNameProvider)) {}
    for (localObject = ((DefaultNameProvider)paramLocale).getName((Locale)localObject, this.iID, str, isStandardOffset(paramLong));; localObject = paramLocale.getName((Locale)localObject, this.iID, str))
    {
      paramLocale = (Locale)localObject;
      if (localObject != null) {
        break;
      }
      paramLocale = printOffset(getOffset(paramLong));
      break;
    }
  }
  
  public abstract String getNameKey(long paramLong);
  
  public abstract int getOffset(long paramLong);
  
  public final int getOffset(ReadableInstant paramReadableInstant)
  {
    if (paramReadableInstant == null) {}
    for (int i = getOffset(DateTimeUtils.currentTimeMillis());; i = getOffset(paramReadableInstant.getMillis())) {
      return i;
    }
  }
  
  public int getOffsetFromLocal(long paramLong)
  {
    long l2 = Long.MAX_VALUE;
    int m = getOffset(paramLong);
    long l1 = paramLong - m;
    int j = getOffset(l1);
    long l3;
    if (m != j)
    {
      if (m - j >= 0) {
        break label150;
      }
      l3 = nextTransition(l1);
      l1 = l3;
      if (l3 == paramLong - m) {
        l1 = Long.MAX_VALUE;
      }
      l3 = nextTransition(paramLong - j);
      if (l3 != paramLong - j) {
        break label156;
      }
    }
    label150:
    label156:
    for (paramLong = l2;; paramLong = l3)
    {
      int i;
      if (l1 != paramLong) {
        i = m;
      }
      for (;;)
      {
        return i;
        if (m >= 0)
        {
          paramLong = previousTransition(l1);
          if (paramLong < l1)
          {
            int k = getOffset(paramLong);
            i = k;
            if (l1 - paramLong <= k - m) {
              continue;
            }
          }
        }
        i = j;
      }
    }
  }
  
  public final String getShortName(long paramLong)
  {
    return getShortName(paramLong, null);
  }
  
  public String getShortName(long paramLong, Locale paramLocale)
  {
    Object localObject = paramLocale;
    if (paramLocale == null) {
      localObject = Locale.getDefault();
    }
    paramLocale = getNameKey(paramLong);
    if (paramLocale == null)
    {
      localObject = this.iID;
      return (String)localObject;
    }
    NameProvider localNameProvider = getNameProvider();
    if ((localNameProvider instanceof DefaultNameProvider)) {}
    for (paramLocale = ((DefaultNameProvider)localNameProvider).getShortName((Locale)localObject, this.iID, paramLocale, isStandardOffset(paramLong));; paramLocale = localNameProvider.getShortName((Locale)localObject, this.iID, paramLocale))
    {
      localObject = paramLocale;
      if (paramLocale != null) {
        break;
      }
      localObject = printOffset(getOffset(paramLong));
      break;
    }
  }
  
  public abstract int getStandardOffset(long paramLong);
  
  public int hashCode()
  {
    return getID().hashCode() + 57;
  }
  
  public abstract boolean isFixed();
  
  public boolean isLocalDateTimeGap(LocalDateTime paramLocalDateTime)
  {
    boolean bool = false;
    if (isFixed()) {}
    for (;;)
    {
      return bool;
      try
      {
        paramLocalDateTime.toDateTime(this);
      }
      catch (IllegalInstantException paramLocalDateTime)
      {
        bool = true;
      }
    }
  }
  
  public boolean isStandardOffset(long paramLong)
  {
    if (getOffset(paramLong) == getStandardOffset(paramLong)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public abstract long nextTransition(long paramLong);
  
  public abstract long previousTransition(long paramLong);
  
  public String toString()
  {
    return getID();
  }
  
  public TimeZone toTimeZone()
  {
    return TimeZone.getTimeZone(this.iID);
  }
  
  protected Object writeReplace()
    throws ObjectStreamException
  {
    return new Stub(this.iID);
  }
  
  static final class LazyInit
  {
    static final Map<String, String> CONVERSION_MAP = ;
    static final DateTimeFormatter OFFSET_FORMATTER = buildFormatter();
    
    private static DateTimeFormatter buildFormatter()
    {
      BaseChronology local1 = new BaseChronology()
      {
        private static final long serialVersionUID = -3128740902654445468L;
        
        public DateTimeZone getZone()
        {
          return null;
        }
        
        public String toString()
        {
          return getClass().getName();
        }
        
        public Chronology withUTC()
        {
          return this;
        }
        
        public Chronology withZone(DateTimeZone paramAnonymousDateTimeZone)
        {
          return this;
        }
      };
      return new DateTimeFormatterBuilder().appendTimeZoneOffset(null, true, 2, 4).toFormatter().withChronology(local1);
    }
    
    private static Map<String, String> buildMap()
    {
      HashMap localHashMap = new HashMap();
      localHashMap.put("GMT", "UTC");
      localHashMap.put("WET", "WET");
      localHashMap.put("CET", "CET");
      localHashMap.put("MET", "CET");
      localHashMap.put("ECT", "CET");
      localHashMap.put("EET", "EET");
      localHashMap.put("MIT", "Pacific/Apia");
      localHashMap.put("HST", "Pacific/Honolulu");
      localHashMap.put("AST", "America/Anchorage");
      localHashMap.put("PST", "America/Los_Angeles");
      localHashMap.put("MST", "America/Denver");
      localHashMap.put("PNT", "America/Phoenix");
      localHashMap.put("CST", "America/Chicago");
      localHashMap.put("EST", "America/New_York");
      localHashMap.put("IET", "America/Indiana/Indianapolis");
      localHashMap.put("PRT", "America/Puerto_Rico");
      localHashMap.put("CNT", "America/St_Johns");
      localHashMap.put("AGT", "America/Argentina/Buenos_Aires");
      localHashMap.put("BET", "America/Sao_Paulo");
      localHashMap.put("ART", "Africa/Cairo");
      localHashMap.put("CAT", "Africa/Harare");
      localHashMap.put("EAT", "Africa/Addis_Ababa");
      localHashMap.put("NET", "Asia/Yerevan");
      localHashMap.put("PLT", "Asia/Karachi");
      localHashMap.put("IST", "Asia/Kolkata");
      localHashMap.put("BST", "Asia/Dhaka");
      localHashMap.put("VST", "Asia/Ho_Chi_Minh");
      localHashMap.put("CTT", "Asia/Shanghai");
      localHashMap.put("JST", "Asia/Tokyo");
      localHashMap.put("ACT", "Australia/Darwin");
      localHashMap.put("AET", "Australia/Sydney");
      localHashMap.put("SST", "Pacific/Guadalcanal");
      localHashMap.put("NST", "Pacific/Auckland");
      return Collections.unmodifiableMap(localHashMap);
    }
  }
  
  private static final class Stub
    implements Serializable
  {
    private static final long serialVersionUID = -6471952376487863581L;
    private transient String iID;
    
    Stub(String paramString)
    {
      this.iID = paramString;
    }
    
    private void readObject(ObjectInputStream paramObjectInputStream)
      throws IOException
    {
      this.iID = paramObjectInputStream.readUTF();
    }
    
    private Object readResolve()
      throws ObjectStreamException
    {
      return DateTimeZone.forID(this.iID);
    }
    
    private void writeObject(ObjectOutputStream paramObjectOutputStream)
      throws IOException
    {
      paramObjectOutputStream.writeUTF(this.iID);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\DateTimeZone.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */