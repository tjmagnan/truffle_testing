package com.testfairy.h;

import android.os.SystemClock;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashSet;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.protocol.HttpContext;

class g
  implements HttpRequestRetryHandler
{
  private static final int a = 1500;
  private static HashSet b = new HashSet();
  private static HashSet c = new HashSet();
  private final int d;
  
  static
  {
    b.add(NoHttpResponseException.class);
    b.add(UnknownHostException.class);
    b.add(SocketException.class);
    c.add(InterruptedIOException.class);
    c.add(SSLHandshakeException.class);
  }
  
  public g(int paramInt)
  {
    this.d = paramInt;
  }
  
  public boolean retryRequest(IOException paramIOException, int paramInt, HttpContext paramHttpContext)
  {
    boolean bool2 = false;
    Boolean localBoolean = (Boolean)paramHttpContext.getAttribute("http.request_sent");
    int i;
    boolean bool1;
    if ((localBoolean != null) && (localBoolean.booleanValue()))
    {
      i = 1;
      if (paramInt <= this.d) {
        break label64;
      }
      bool1 = bool2;
      label44:
      if (!bool1) {
        break label145;
      }
      SystemClock.sleep(1500L);
    }
    for (;;)
    {
      return bool1;
      i = 0;
      break;
      label64:
      bool1 = bool2;
      if (c.contains(paramIOException.getClass())) {
        break label44;
      }
      if (b.contains(paramIOException.getClass()))
      {
        bool1 = true;
        break label44;
      }
      if (i == 0)
      {
        bool1 = true;
        break label44;
      }
      bool1 = bool2;
      if (((HttpUriRequest)paramHttpContext.getAttribute("http.request")).getMethod().equals("POST")) {
        break label44;
      }
      bool1 = true;
      break label44;
      label145:
      paramIOException.printStackTrace();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */