package org.jsoup.helper;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public final class StringUtil
{
  private static final String[] padding = { "", " ", "  ", "   ", "    ", "     ", "      ", "       ", "        ", "         ", "          " };
  
  public static void appendNormalisedWhitespace(StringBuilder paramStringBuilder, String paramString, boolean paramBoolean)
  {
    int n = 0;
    int j = 0;
    int i1 = paramString.length();
    int k = 0;
    if (k < i1)
    {
      int i2 = paramString.codePointAt(k);
      int i;
      int m;
      if (isWhitespace(i2)) {
        if (paramBoolean)
        {
          i = n;
          m = j;
          if (j == 0) {}
        }
        else
        {
          if (n == 0) {
            break label86;
          }
          m = j;
          i = n;
        }
      }
      for (;;)
      {
        k += Character.charCount(i2);
        n = i;
        j = m;
        break;
        label86:
        paramStringBuilder.append(' ');
        i = 1;
        m = j;
        continue;
        paramStringBuilder.appendCodePoint(i2);
        i = 0;
        m = 1;
      }
    }
  }
  
  public static boolean in(String paramString, String... paramVarArgs)
  {
    boolean bool2 = false;
    int j = paramVarArgs.length;
    for (int i = 0;; i++)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (paramVarArgs[i].equals(paramString)) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  public static boolean inSorted(String paramString, String[] paramArrayOfString)
  {
    if (Arrays.binarySearch(paramArrayOfString, paramString) >= 0) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isBlank(String paramString)
  {
    boolean bool2 = true;
    boolean bool1 = bool2;
    if (paramString != null)
    {
      if (paramString.length() == 0) {
        bool1 = bool2;
      }
    }
    else {
      return bool1;
    }
    int j = paramString.length();
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i >= j) {
        break;
      }
      if (!isWhitespace(paramString.codePointAt(i)))
      {
        bool1 = false;
        break;
      }
    }
  }
  
  public static boolean isNumeric(String paramString)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramString != null)
    {
      if (paramString.length() != 0) {
        break label22;
      }
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      label22:
      int j = paramString.length();
      for (int i = 0;; i++)
      {
        if (i >= j) {
          break label54;
        }
        bool1 = bool2;
        if (!Character.isDigit(paramString.codePointAt(i))) {
          break;
        }
      }
      label54:
      bool1 = true;
    }
  }
  
  public static boolean isWhitespace(int paramInt)
  {
    if ((paramInt == 32) || (paramInt == 9) || (paramInt == 10) || (paramInt == 12) || (paramInt == 13)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static String join(Collection paramCollection, String paramString)
  {
    return join(paramCollection.iterator(), paramString);
  }
  
  public static String join(Iterator paramIterator, String paramString)
  {
    Object localObject;
    if (!paramIterator.hasNext()) {
      localObject = "";
    }
    for (;;)
    {
      return (String)localObject;
      String str = paramIterator.next().toString();
      localObject = str;
      if (paramIterator.hasNext())
      {
        localObject = new StringBuilder(64).append(str);
        while (paramIterator.hasNext())
        {
          ((StringBuilder)localObject).append(paramString);
          ((StringBuilder)localObject).append(paramIterator.next());
        }
        localObject = ((StringBuilder)localObject).toString();
      }
    }
  }
  
  public static String normaliseWhitespace(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramString.length());
    appendNormalisedWhitespace(localStringBuilder, paramString, false);
    return localStringBuilder.toString();
  }
  
  public static String padding(int paramInt)
  {
    if (paramInt < 0) {
      throw new IllegalArgumentException("width must be > 0");
    }
    if (paramInt < padding.length) {}
    for (Object localObject = padding[paramInt];; localObject = String.valueOf((char[])localObject))
    {
      return (String)localObject;
      localObject = new char[paramInt];
      for (int i = 0; i < paramInt; i++) {
        localObject[i] = 32;
      }
    }
  }
  
  /* Error */
  public static String resolve(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: new 143	java/net/URL
    //   3: astore_2
    //   4: aload_2
    //   5: aload_0
    //   6: invokespecial 144	java/net/URL:<init>	(Ljava/lang/String;)V
    //   9: aload_2
    //   10: aload_1
    //   11: invokestatic 147	org/jsoup/helper/StringUtil:resolve	(Ljava/net/URL;Ljava/lang/String;)Ljava/net/URL;
    //   14: invokevirtual 150	java/net/URL:toExternalForm	()Ljava/lang/String;
    //   17: astore_0
    //   18: aload_0
    //   19: areturn
    //   20: astore_0
    //   21: new 143	java/net/URL
    //   24: astore_0
    //   25: aload_0
    //   26: aload_1
    //   27: invokespecial 144	java/net/URL:<init>	(Ljava/lang/String;)V
    //   30: aload_0
    //   31: invokevirtual 150	java/net/URL:toExternalForm	()Ljava/lang/String;
    //   34: astore_0
    //   35: goto -17 -> 18
    //   38: astore_0
    //   39: ldc 12
    //   41: astore_0
    //   42: goto -24 -> 18
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	45	0	paramString1	String
    //   0	45	1	paramString2	String
    //   3	7	2	localURL	URL
    // Exception table:
    //   from	to	target	type
    //   0	9	20	java/net/MalformedURLException
    //   9	18	38	java/net/MalformedURLException
    //   21	35	38	java/net/MalformedURLException
  }
  
  public static URL resolve(URL paramURL, String paramString)
    throws MalformedURLException
  {
    String str = paramString;
    if (paramString.startsWith("?")) {
      str = paramURL.getPath() + paramString;
    }
    paramString = paramURL;
    if (str.indexOf('.') == 0)
    {
      paramString = paramURL;
      if (paramURL.getFile().indexOf('/') != 0) {
        paramString = new URL(paramURL.getProtocol(), paramURL.getHost(), paramURL.getPort(), "/" + paramURL.getFile());
      }
    }
    return new URL(paramString, str);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\helper\StringUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */