package me.dm7.barcodescanner.core;

import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class CameraHandlerThread
  extends HandlerThread
{
  private static final String LOG_TAG = "CameraHandlerThread";
  private BarcodeScannerView mScannerView;
  
  public CameraHandlerThread(BarcodeScannerView paramBarcodeScannerView)
  {
    super("CameraHandlerThread");
    this.mScannerView = paramBarcodeScannerView;
    start();
  }
  
  public void startCamera(final int paramInt)
  {
    new Handler(getLooper()).post(new Runnable()
    {
      public void run()
      {
        final Camera localCamera = CameraUtils.getCameraInstance(paramInt);
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {
          public void run()
          {
            CameraHandlerThread.this.mScannerView.setupCameraPreview(CameraWrapper.getWrapper(localCamera, CameraHandlerThread.1.this.val$cameraId));
          }
        });
      }
    });
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\CameraHandlerThread.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */