package tech.dcube.companion.managers.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class UtilManager
{
  private static final String TAG = "UtilManager";
  private static final DecimalFormat[] formats = { null, new DecimalFormat("#.#"), new DecimalFormat("#.##"), new DecimalFormat("#.###"), new DecimalFormat("#.####") };
  
  public static String bitmapToString(Bitmap paramBitmap)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, localByteArrayOutputStream);
    return Base64.encodeToString(localByteArrayOutputStream.toByteArray(), 0);
  }
  
  private static String capitalize(String paramString)
  {
    String str;
    if ((paramString == null) || (paramString.length() == 0)) {
      str = "";
    }
    for (;;)
    {
      return str;
      char c = paramString.charAt(0);
      str = paramString;
      if (!Character.isUpperCase(c)) {
        str = Character.toUpperCase(c) + paramString.substring(1);
      }
    }
  }
  
  public static double dimensionImperialToMetric(double paramDouble)
  {
    return paramDouble * 0.0254D;
  }
  
  public static double dimensionMetricToImperial(double paramDouble)
  {
    return paramDouble / 0.0254D;
  }
  
  public static String formatDoubleToString(double paramDouble, int paramInt)
  {
    long l = paramDouble;
    if (Math.abs(paramDouble - l) < 1.0E-5D) {}
    for (String str = Long.toString(l);; str = formats[paramInt].format(paramDouble)) {
      return str;
    }
  }
  
  private static String getDeviceName()
  {
    String str2 = Build.MANUFACTURER;
    String str1 = Build.MODEL;
    if (str1.startsWith(str2)) {}
    for (str1 = capitalize(str1);; str1 = capitalize(str2) + " " + str1) {
      return str1;
    }
  }
  
  public static AlertDialog getDialog(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = new AlertDialog.Builder(paramContext);
    paramContext.setTitle(paramString1).setMessage(paramString2).setCancelable(false);
    return paramContext.create();
  }
  
  public static int getRandom(int paramInt)
  {
    return new Random().nextInt(paramInt);
  }
  
  public static String getSystemTimeZone()
  {
    long l = Calendar.getInstance().getTimeZone().getOffset(new Date().getTime());
    Date localDate = new Date(Math.abs(l));
    if (l >= 0L) {}
    for (Object localObject = "'GMT'+HH:mm";; localObject = "'GMT'-HH:mm")
    {
      localObject = new SimpleDateFormat((String)localObject);
      ((DateFormat)localObject).setTimeZone(TimeZone.getTimeZone("GMT+0"));
      return ((DateFormat)localObject).format(localDate);
    }
  }
  
  public static void hideSoftKeyboard(Activity paramActivity)
  {
    try
    {
      InputMethodManager localInputMethodManager = (InputMethodManager)paramActivity.getSystemService("input_method");
      if (paramActivity.getCurrentFocus() != null)
      {
        localInputMethodManager.hideSoftInputFromWindow(paramActivity.getCurrentFocus().getWindowToken(), 0);
        paramActivity.getCurrentFocus().clearFocus();
      }
      return;
    }
    catch (Exception paramActivity)
    {
      for (;;) {}
    }
  }
  
  public static boolean isInternetAvailable(Context paramContext)
  {
    paramContext = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((paramContext != null) && (paramContext.isConnected())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean isValidJson(String paramString)
  {
    try
    {
      Gson localGson = new com/google/gson/Gson;
      localGson.<init>();
      localGson.fromJson(paramString, Object.class);
      bool = true;
    }
    catch (JsonSyntaxException paramString)
    {
      for (;;)
      {
        boolean bool = false;
      }
    }
    return bool;
  }
  
  public static void registerAutoHideKeyboardListener(Activity paramActivity, View paramView)
  {
    if (!(paramView instanceof EditText)) {
      paramView.setOnTouchListener(new View.OnTouchListener()
      {
        public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
        {
          UtilManager.hideSoftKeyboard(this.val$activity);
          paramAnonymousView.getRootView().requestFocus();
          return false;
        }
      });
    }
    if ((paramView instanceof ViewGroup)) {
      for (int i = 0; i < ((ViewGroup)paramView).getChildCount(); i++) {
        registerAutoHideKeyboardListener(paramActivity, ((ViewGroup)paramView).getChildAt(i));
      }
    }
  }
  
  public static boolean showGloabalToast(Context paramContext, String paramString)
  {
    boolean bool = false;
    try
    {
      Toast.makeText(paramContext, paramString, 0).show();
      bool = true;
    }
    catch (Exception paramContext)
    {
      for (;;)
      {
        paramContext.printStackTrace();
      }
    }
    return bool;
  }
  
  public static Bitmap stringToBitmap(String paramString)
  {
    try
    {
      paramString = Base64.decode(paramString, 0);
      paramString = BitmapFactory.decodeByteArray(paramString, 0, paramString.length);
      return paramString;
    }
    catch (Exception paramString)
    {
      for (;;)
      {
        paramString.getMessage();
        paramString = null;
      }
    }
  }
  
  public static double weightKGtoLBS(double paramDouble)
  {
    return paramDouble * 2.2046D;
  }
  
  public static double weightLBSplusOZ(double paramDouble1, double paramDouble2)
  {
    return paramDouble1 + (int)paramDouble2 % 16;
  }
  
  public static double weightLBtoKG(double paramDouble)
  {
    return paramDouble / 2.2046D;
  }
  
  public static double weightOZtoKG(double paramDouble)
  {
    return paramDouble / 35.274D;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\util\UtilManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */