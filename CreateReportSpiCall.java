package com.crashlytics.android.core;

abstract interface CreateReportSpiCall
{
  public abstract boolean invoke(CreateReportRequest paramCreateReportRequest);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\core\CreateReportSpiCall.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */