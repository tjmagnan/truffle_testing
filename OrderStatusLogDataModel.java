package tech.dcube.companion.model;

public class OrderStatusLogDataModel
{
  String LogDate = null;
  String LogLocation = null;
  String LogStatus = null;
  
  public OrderStatusLogDataModel()
  {
    this.LogDate = "05/09";
    this.LogStatus = "Waiting for pickup";
    this.LogLocation = "Danbury, CT";
  }
  
  public OrderStatusLogDataModel(String paramString1, String paramString2, String paramString3)
  {
    this.LogDate = paramString1;
    this.LogStatus = paramString2;
    this.LogLocation = paramString3;
  }
  
  public String getLogDate()
  {
    return this.LogDate;
  }
  
  public String getLogLocation()
  {
    return this.LogLocation;
  }
  
  public String getLogStatus()
  {
    return this.LogStatus;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\OrderStatusLogDataModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */