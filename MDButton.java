package com.afollestad.materialdialogs.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.TextView;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.R.dimen;
import com.afollestad.materialdialogs.util.DialogUtils;

@SuppressLint({"AppCompatCustomView"})
public class MDButton
  extends TextView
{
  private Drawable defaultBackground;
  private boolean stacked = false;
  private Drawable stackedBackground;
  private int stackedEndPadding;
  private GravityEnum stackedGravity;
  
  public MDButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public MDButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    this.stackedEndPadding = paramContext.getResources().getDimensionPixelSize(R.dimen.md_dialog_frame_margin);
    this.stackedGravity = GravityEnum.END;
  }
  
  public void setAllCapsCompat(boolean paramBoolean)
  {
    if (Build.VERSION.SDK_INT >= 14) {
      setAllCaps(paramBoolean);
    }
    for (;;)
    {
      return;
      if (paramBoolean) {
        setTransformationMethod(new AllCapsTransformationMethod(getContext()));
      } else {
        setTransformationMethod(null);
      }
    }
  }
  
  public void setDefaultSelector(Drawable paramDrawable)
  {
    this.defaultBackground = paramDrawable;
    if (!this.stacked) {
      setStacked(false, true);
    }
  }
  
  void setStacked(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i;
    if ((this.stacked != paramBoolean1) || (paramBoolean2))
    {
      if (!paramBoolean1) {
        break label103;
      }
      i = this.stackedGravity.getGravityInt() | 0x10;
      setGravity(i);
      if (Build.VERSION.SDK_INT >= 17)
      {
        if (!paramBoolean1) {
          break label109;
        }
        i = this.stackedGravity.getTextAlignment();
        label52:
        setTextAlignment(i);
      }
      if (!paramBoolean1) {
        break label114;
      }
    }
    label103:
    label109:
    label114:
    for (Drawable localDrawable = this.stackedBackground;; localDrawable = this.defaultBackground)
    {
      DialogUtils.setBackgroundCompat(this, localDrawable);
      if (paramBoolean1) {
        setPadding(this.stackedEndPadding, getPaddingTop(), this.stackedEndPadding, getPaddingBottom());
      }
      this.stacked = paramBoolean1;
      return;
      i = 17;
      break;
      i = 4;
      break label52;
    }
  }
  
  public void setStackedGravity(GravityEnum paramGravityEnum)
  {
    this.stackedGravity = paramGravityEnum;
  }
  
  public void setStackedSelector(Drawable paramDrawable)
  {
    this.stackedBackground = paramDrawable;
    if (this.stacked) {
      setStacked(true, true);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\internal\MDButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */