package tech.dcube.companion.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import butterknife.ButterKnife;
import com.android.volley.VolleyError;
import java.util.List;
import tech.dcube.companion.customListAdapters.CatalogItemListAdapter;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBSendProItem;

public class PBCatalogHomeFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  CatalogItemListAdapter adapter;
  ListView lstCatalogItems;
  private FrameLayout mFadeFL;
  private String mParam1;
  private String mParam2;
  private ProgressBar mProgressBar;
  List<PBSendProItem> pbSendProItemList;
  
  public static PBCatalogHomeFragment newInstance(String paramString1, String paramString2)
  {
    PBCatalogHomeFragment localPBCatalogHomeFragment = new PBCatalogHomeFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBCatalogHomeFragment.setArguments(localBundle);
    return localPBCatalogHomeFragment;
  }
  
  void callGetCatalogListApi()
  {
    showLoadingOverlay();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local2 = new tech/dcube/companion/fragments/PBCatalogHomeFragment$2;
      local2.<init>(this);
      localServerManager.getItemsList(localContext, local2);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        hideLoadingOverlay();
      }
    }
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void hideLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(8);
      this.mFadeFL.setVisibility(8);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void launchAddItemFragment()
  {
    FragmentTransaction localFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
    localFragmentTransaction.add(2131624061, CatalogAddItemFragment.newInstance(123, CatalogAddItemFragment.class.getName())).addToBackStack(CatalogAddItemFragment.class.getName());
    localFragmentTransaction.commit();
  }
  
  public void onButtonPressed(Uri paramUri) {}
  
  public void onClickTopBarLeftButton() {}
  
  public void onClickTopBarRightButton()
  {
    launchAddItemFragment();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968631, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    hideTopBarLeftButton();
    setTopBarTitle("Catalog");
    setTopBarRightButtonText("+");
    setTopBarRightButtonTextSize(32.0F);
    this.mProgressBar = ((ProgressBar)paramLayoutInflater.findViewById(2131624063));
    this.mFadeFL = ((FrameLayout)paramLayoutInflater.findViewById(2131624062));
    showLoadingOverlay();
    this.lstCatalogItems = ((ListView)paramLayoutInflater.findViewById(2131624116));
    this.pbSendProItemList = DataManager.getInstance().getItems();
    this.adapter = new CatalogItemListAdapter(this.pbSendProItemList, getActivity().getApplicationContext());
    this.lstCatalogItems.setAdapter(this.adapter);
    this.lstCatalogItems.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        paramAnonymousAdapterView = PBCatalogHomeFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
        paramAnonymousAdapterView.add(2131624061, CatalogItemDetailFragment.newInstance(paramAnonymousInt, CatalogItemDetailFragment.class.getName())).addToBackStack(CatalogItemDetailFragment.class.getName());
        paramAnonymousAdapterView.commit();
      }
    });
    updateCatalogList();
    return paramLayoutInflater;
  }
  
  public void showLoadingOverlay()
  {
    try
    {
      this.mProgressBar.setVisibility(0);
      this.mFadeFL.setVisibility(0);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void updateCatalogList()
  {
    callGetCatalogListApi();
  }
  
  public void updateCatalogListLocally()
  {
    this.adapter.refresh();
    callGetCatalogListApi();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBCatalogHomeFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */