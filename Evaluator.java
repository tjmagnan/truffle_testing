package org.jsoup.select;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Tag;

public abstract class Evaluator
{
  public abstract boolean matches(Element paramElement1, Element paramElement2);
  
  public static final class AllElements
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return true;
    }
    
    public String toString()
    {
      return "*";
    }
  }
  
  public static final class Attribute
    extends Evaluator
  {
    private String key;
    
    public Attribute(String paramString)
    {
      this.key = paramString;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.hasAttr(this.key);
    }
    
    public String toString()
    {
      return String.format("[%s]", new Object[] { this.key });
    }
  }
  
  public static abstract class AttributeKeyPair
    extends Evaluator
  {
    String key;
    String value;
    
    public AttributeKeyPair(String paramString1, String paramString2)
    {
      Validate.notEmpty(paramString1);
      Validate.notEmpty(paramString2);
      this.key = paramString1.trim().toLowerCase();
      if ((!paramString2.startsWith("\"")) || (!paramString2.endsWith("\"")))
      {
        paramString1 = paramString2;
        if (paramString2.startsWith("'"))
        {
          paramString1 = paramString2;
          if (!paramString2.endsWith("'")) {}
        }
      }
      else
      {
        paramString1 = paramString2.substring(1, paramString2.length() - 1);
      }
      this.value = paramString1.trim().toLowerCase();
    }
  }
  
  public static final class AttributeStarting
    extends Evaluator
  {
    private String keyPrefix;
    
    public AttributeStarting(String paramString)
    {
      Validate.notEmpty(paramString);
      this.keyPrefix = paramString.toLowerCase();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      paramElement1 = paramElement2.attributes().asList().iterator();
      do
      {
        if (!paramElement1.hasNext()) {
          break;
        }
      } while (!((Attribute)paramElement1.next()).getKey().toLowerCase().startsWith(this.keyPrefix));
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[^%s]", new Object[] { this.keyPrefix });
    }
  }
  
  public static final class AttributeWithValue
    extends Evaluator.AttributeKeyPair
  {
    public AttributeWithValue(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if ((paramElement2.hasAttr(this.key)) && (this.value.equalsIgnoreCase(paramElement2.attr(this.key).trim()))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s=%s]", new Object[] { this.key, this.value });
    }
  }
  
  public static final class AttributeWithValueContaining
    extends Evaluator.AttributeKeyPair
  {
    public AttributeWithValueContaining(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if ((paramElement2.hasAttr(this.key)) && (paramElement2.attr(this.key).toLowerCase().contains(this.value))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s*=%s]", new Object[] { this.key, this.value });
    }
  }
  
  public static final class AttributeWithValueEnding
    extends Evaluator.AttributeKeyPair
  {
    public AttributeWithValueEnding(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if ((paramElement2.hasAttr(this.key)) && (paramElement2.attr(this.key).toLowerCase().endsWith(this.value))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s$=%s]", new Object[] { this.key, this.value });
    }
  }
  
  public static final class AttributeWithValueMatching
    extends Evaluator
  {
    String key;
    Pattern pattern;
    
    public AttributeWithValueMatching(String paramString, Pattern paramPattern)
    {
      this.key = paramString.trim().toLowerCase();
      this.pattern = paramPattern;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if ((paramElement2.hasAttr(this.key)) && (this.pattern.matcher(paramElement2.attr(this.key)).find())) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s~=%s]", new Object[] { this.key, this.pattern.toString() });
    }
  }
  
  public static final class AttributeWithValueNot
    extends Evaluator.AttributeKeyPair
  {
    public AttributeWithValueNot(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (!this.value.equalsIgnoreCase(paramElement2.attr(this.key))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s!=%s]", new Object[] { this.key, this.value });
    }
  }
  
  public static final class AttributeWithValueStarting
    extends Evaluator.AttributeKeyPair
  {
    public AttributeWithValueStarting(String paramString1, String paramString2)
    {
      super(paramString2);
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if ((paramElement2.hasAttr(this.key)) && (paramElement2.attr(this.key).toLowerCase().startsWith(this.value))) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format("[%s^=%s]", new Object[] { this.key, this.value });
    }
  }
  
  public static final class Class
    extends Evaluator
  {
    private String className;
    
    public Class(String paramString)
    {
      this.className = paramString;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.hasClass(this.className);
    }
    
    public String toString()
    {
      return String.format(".%s", new Object[] { this.className });
    }
  }
  
  public static final class ContainsData
    extends Evaluator
  {
    private String searchText;
    
    public ContainsData(String paramString)
    {
      this.searchText = paramString.toLowerCase();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.data().toLowerCase().contains(this.searchText);
    }
    
    public String toString()
    {
      return String.format(":containsData(%s)", new Object[] { this.searchText });
    }
  }
  
  public static final class ContainsOwnText
    extends Evaluator
  {
    private String searchText;
    
    public ContainsOwnText(String paramString)
    {
      this.searchText = paramString.toLowerCase();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.ownText().toLowerCase().contains(this.searchText);
    }
    
    public String toString()
    {
      return String.format(":containsOwn(%s)", new Object[] { this.searchText });
    }
  }
  
  public static final class ContainsText
    extends Evaluator
  {
    private String searchText;
    
    public ContainsText(String paramString)
    {
      this.searchText = paramString.toLowerCase();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.text().toLowerCase().contains(this.searchText);
    }
    
    public String toString()
    {
      return String.format(":contains(%s)", new Object[] { this.searchText });
    }
  }
  
  public static abstract class CssNthEvaluator
    extends Evaluator
  {
    protected final int a;
    protected final int b;
    
    public CssNthEvaluator(int paramInt)
    {
      this(0, paramInt);
    }
    
    public CssNthEvaluator(int paramInt1, int paramInt2)
    {
      this.a = paramInt1;
      this.b = paramInt2;
    }
    
    protected abstract int calculatePosition(Element paramElement1, Element paramElement2);
    
    protected abstract String getPseudoClass();
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool = true;
      Element localElement = paramElement2.parent();
      if ((localElement == null) || ((localElement instanceof Document))) {
        bool = false;
      }
      for (;;)
      {
        return bool;
        int i = calculatePosition(paramElement1, paramElement2);
        if (this.a == 0)
        {
          if (i != this.b) {
            bool = false;
          }
        }
        else if (((i - this.b) * this.a < 0) || ((i - this.b) % this.a != 0)) {
          bool = false;
        }
      }
    }
    
    public String toString()
    {
      String str;
      if (this.a == 0) {
        str = String.format(":%s(%d)", new Object[] { getPseudoClass(), Integer.valueOf(this.b) });
      }
      for (;;)
      {
        return str;
        if (this.b == 0) {
          str = String.format(":%s(%dn)", new Object[] { getPseudoClass(), Integer.valueOf(this.a) });
        } else {
          str = String.format(":%s(%dn%+d)", new Object[] { getPseudoClass(), Integer.valueOf(this.a), Integer.valueOf(this.b) });
        }
      }
    }
  }
  
  public static final class Id
    extends Evaluator
  {
    private String id;
    
    public Id(String paramString)
    {
      this.id = paramString;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return this.id.equals(paramElement2.id());
    }
    
    public String toString()
    {
      return String.format("#%s", new Object[] { this.id });
    }
  }
  
  public static final class IndexEquals
    extends Evaluator.IndexEvaluator
  {
    public IndexEquals(int paramInt)
    {
      super();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (paramElement2.elementSiblingIndex().intValue() == this.index) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format(":eq(%d)", new Object[] { Integer.valueOf(this.index) });
    }
  }
  
  public static abstract class IndexEvaluator
    extends Evaluator
  {
    int index;
    
    public IndexEvaluator(int paramInt)
    {
      this.index = paramInt;
    }
  }
  
  public static final class IndexGreaterThan
    extends Evaluator.IndexEvaluator
  {
    public IndexGreaterThan(int paramInt)
    {
      super();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (paramElement2.elementSiblingIndex().intValue() > this.index) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format(":gt(%d)", new Object[] { Integer.valueOf(this.index) });
    }
  }
  
  public static final class IndexLessThan
    extends Evaluator.IndexEvaluator
  {
    public IndexLessThan(int paramInt)
    {
      super();
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (paramElement2.elementSiblingIndex().intValue() < this.index) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format(":lt(%d)", new Object[] { Integer.valueOf(this.index) });
    }
  }
  
  public static final class IsEmpty
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      paramElement2 = paramElement2.childNodes().iterator();
      do
      {
        if (!paramElement2.hasNext()) {
          break;
        }
        paramElement1 = (Node)paramElement2.next();
      } while (((paramElement1 instanceof Comment)) || ((paramElement1 instanceof XmlDeclaration)) || ((paramElement1 instanceof DocumentType)));
      for (boolean bool = false;; bool = true) {
        return bool;
      }
    }
    
    public String toString()
    {
      return ":empty";
    }
  }
  
  public static final class IsFirstChild
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      paramElement1 = paramElement2.parent();
      if ((paramElement1 != null) && (!(paramElement1 instanceof Document)) && (paramElement2.elementSiblingIndex().intValue() == 0)) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return ":first-child";
    }
  }
  
  public static final class IsFirstOfType
    extends Evaluator.IsNthOfType
  {
    public IsFirstOfType()
    {
      super(1);
    }
    
    public String toString()
    {
      return ":first-of-type";
    }
  }
  
  public static final class IsLastChild
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      paramElement1 = paramElement2.parent();
      if ((paramElement1 != null) && (!(paramElement1 instanceof Document)) && (paramElement2.elementSiblingIndex().intValue() == paramElement1.children().size() - 1)) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return ":last-child";
    }
  }
  
  public static final class IsLastOfType
    extends Evaluator.IsNthLastOfType
  {
    public IsLastOfType()
    {
      super(1);
    }
    
    public String toString()
    {
      return ":last-of-type";
    }
  }
  
  public static final class IsNthChild
    extends Evaluator.CssNthEvaluator
  {
    public IsNthChild(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    protected int calculatePosition(Element paramElement1, Element paramElement2)
    {
      return paramElement2.elementSiblingIndex().intValue() + 1;
    }
    
    protected String getPseudoClass()
    {
      return "nth-child";
    }
  }
  
  public static final class IsNthLastChild
    extends Evaluator.CssNthEvaluator
  {
    public IsNthLastChild(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    protected int calculatePosition(Element paramElement1, Element paramElement2)
    {
      return paramElement2.parent().children().size() - paramElement2.elementSiblingIndex().intValue();
    }
    
    protected String getPseudoClass()
    {
      return "nth-last-child";
    }
  }
  
  public static class IsNthLastOfType
    extends Evaluator.CssNthEvaluator
  {
    public IsNthLastOfType(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    protected int calculatePosition(Element paramElement1, Element paramElement2)
    {
      int j = 0;
      paramElement1 = paramElement2.parent().children();
      int k = paramElement2.elementSiblingIndex().intValue();
      while (k < paramElement1.size())
      {
        int i = j;
        if (((Element)paramElement1.get(k)).tag().equals(paramElement2.tag())) {
          i = j + 1;
        }
        k++;
        j = i;
      }
      return j;
    }
    
    protected String getPseudoClass()
    {
      return "nth-last-of-type";
    }
  }
  
  public static class IsNthOfType
    extends Evaluator.CssNthEvaluator
  {
    public IsNthOfType(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    protected int calculatePosition(Element paramElement1, Element paramElement2)
    {
      int i = 0;
      Iterator localIterator = paramElement2.parent().children().iterator();
      int j;
      do
      {
        j = i;
        if (!localIterator.hasNext()) {
          break;
        }
        paramElement1 = (Element)localIterator.next();
        j = i;
        if (paramElement1.tag().equals(paramElement2.tag())) {
          j = i + 1;
        }
        i = j;
      } while (paramElement1 != paramElement2);
      return j;
    }
    
    protected String getPseudoClass()
    {
      return "nth-of-type";
    }
  }
  
  public static final class IsOnlyChild
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      paramElement1 = paramElement2.parent();
      if ((paramElement1 != null) && (!(paramElement1 instanceof Document)) && (paramElement2.siblingElements().size() == 0)) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return ":only-child";
    }
  }
  
  public static final class IsOnlyOfType
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool = true;
      paramElement1 = paramElement2.parent();
      if ((paramElement1 == null) || ((paramElement1 instanceof Document))) {
        bool = false;
      }
      for (;;)
      {
        return bool;
        int i = 0;
        paramElement1 = paramElement1.children().iterator();
        while (paramElement1.hasNext()) {
          if (((Element)paramElement1.next()).tag().equals(paramElement2.tag())) {
            i++;
          }
        }
        if (i != 1) {
          bool = false;
        }
      }
    }
    
    public String toString()
    {
      return ":only-of-type";
    }
  }
  
  public static final class IsRoot
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool = false;
      if ((paramElement1 instanceof Document)) {
        paramElement1 = paramElement1.child(0);
      }
      for (;;)
      {
        if (paramElement2 == paramElement1) {
          bool = true;
        }
        return bool;
      }
    }
    
    public String toString()
    {
      return ":root";
    }
  }
  
  public static final class Matches
    extends Evaluator
  {
    private Pattern pattern;
    
    public Matches(Pattern paramPattern)
    {
      this.pattern = paramPattern;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return this.pattern.matcher(paramElement2.text()).find();
    }
    
    public String toString()
    {
      return String.format(":matches(%s)", new Object[] { this.pattern });
    }
  }
  
  public static final class MatchesOwn
    extends Evaluator
  {
    private Pattern pattern;
    
    public MatchesOwn(Pattern paramPattern)
    {
      this.pattern = paramPattern;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return this.pattern.matcher(paramElement2.ownText()).find();
    }
    
    public String toString()
    {
      return String.format(":matchesOwn(%s)", new Object[] { this.pattern });
    }
  }
  
  public static final class Tag
    extends Evaluator
  {
    private String tagName;
    
    public Tag(String paramString)
    {
      this.tagName = paramString;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.tagName().equalsIgnoreCase(this.tagName);
    }
    
    public String toString()
    {
      return String.format("%s", new Object[] { this.tagName });
    }
  }
  
  public static final class TagEndsWith
    extends Evaluator
  {
    private String tagName;
    
    public TagEndsWith(String paramString)
    {
      this.tagName = paramString;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      return paramElement2.tagName().endsWith(this.tagName);
    }
    
    public String toString()
    {
      return String.format("%s", new Object[] { this.tagName });
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\select\Evaluator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */