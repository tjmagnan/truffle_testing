package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class PBWeightRules
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 2978363340086721321L;
  @Expose
  @SerializedName("hiddenWeightKg")
  private double hiddenWeightKg;
  @Expose
  @SerializedName("maxWeightAllowedKg")
  private double maxWeightAllowedKg;
  @Expose
  @SerializedName("minWeightAllowedKg")
  private double minWeightAllowedKg;
  @Expose
  private double weight = 0.1D;
  @Expose
  @SerializedName("weightHidden")
  private Boolean weightHidden;
  
  public PBWeightRules() {}
  
  public PBWeightRules(Integer paramInteger, Double paramDouble1, Double paramDouble2, Boolean paramBoolean)
  {
    this.hiddenWeightKg = paramInteger.intValue();
    this.maxWeightAllowedKg = paramDouble1.doubleValue();
    this.minWeightAllowedKg = paramDouble2.doubleValue();
    this.weightHidden = paramBoolean;
  }
  
  public double getHiddenWeightKg()
  {
    return this.hiddenWeightKg;
  }
  
  public double getMaxWeightAllowedKg()
  {
    return this.maxWeightAllowedKg;
  }
  
  public double getMinWeightAllowedKg()
  {
    return this.minWeightAllowedKg;
  }
  
  public double getWeight()
  {
    return this.weight;
  }
  
  public Boolean getWeightHidden()
  {
    return this.weightHidden;
  }
  
  public void setHiddenWeightKg(double paramDouble)
  {
    this.hiddenWeightKg = paramDouble;
  }
  
  public void setMaxWeightAllowedKg(double paramDouble)
  {
    this.maxWeightAllowedKg = paramDouble;
  }
  
  public void setMinWeightAllowedKg(double paramDouble)
  {
    this.minWeightAllowedKg = paramDouble;
  }
  
  public void setWeight(double paramDouble)
  {
    this.weight = paramDouble;
  }
  
  public void setWeightHidden(Boolean paramBoolean)
  {
    this.weightHidden = paramBoolean;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBWeightRules.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */