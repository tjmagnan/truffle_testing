package com.testfairy.p;

import java.util.concurrent.TimeUnit;

public class o
{
  long a;
  
  private o()
  {
    b();
  }
  
  public static o a()
  {
    return new o();
  }
  
  public long a(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(c(), TimeUnit.MILLISECONDS);
  }
  
  public o b()
  {
    this.a = System.currentTimeMillis();
    return this;
  }
  
  public long c()
  {
    return System.currentTimeMillis() - this.a;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */