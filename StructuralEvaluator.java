package org.jsoup.select;

import java.util.Iterator;
import org.jsoup.nodes.Element;

abstract class StructuralEvaluator
  extends Evaluator
{
  Evaluator evaluator;
  
  static class Has
    extends StructuralEvaluator
  {
    public Has(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      Iterator localIterator = paramElement2.getAllElements().iterator();
      Element localElement;
      do
      {
        if (!localIterator.hasNext()) {
          break;
        }
        localElement = (Element)localIterator.next();
      } while ((localElement == paramElement2) || (!this.evaluator.matches(paramElement1, localElement)));
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format(":has(%s)", new Object[] { this.evaluator });
    }
  }
  
  static class ImmediateParent
    extends StructuralEvaluator
  {
    public ImmediateParent(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramElement1 == paramElement2) {
        bool1 = bool2;
      }
      for (;;)
      {
        return bool1;
        paramElement2 = paramElement2.parent();
        bool1 = bool2;
        if (paramElement2 != null)
        {
          bool1 = bool2;
          if (this.evaluator.matches(paramElement1, paramElement2)) {
            bool1 = true;
          }
        }
      }
    }
    
    public String toString()
    {
      return String.format(":ImmediateParent%s", new Object[] { this.evaluator });
    }
  }
  
  static class ImmediatePreviousSibling
    extends StructuralEvaluator
  {
    public ImmediatePreviousSibling(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramElement1 == paramElement2) {
        bool1 = bool2;
      }
      for (;;)
      {
        return bool1;
        paramElement2 = paramElement2.previousElementSibling();
        bool1 = bool2;
        if (paramElement2 != null)
        {
          bool1 = bool2;
          if (this.evaluator.matches(paramElement1, paramElement2)) {
            bool1 = true;
          }
        }
      }
    }
    
    public String toString()
    {
      return String.format(":prev%s", new Object[] { this.evaluator });
    }
  }
  
  static class Not
    extends StructuralEvaluator
  {
    public Not(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (!this.evaluator.matches(paramElement1, paramElement2)) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
    
    public String toString()
    {
      return String.format(":not%s", new Object[] { this.evaluator });
    }
  }
  
  static class Parent
    extends StructuralEvaluator
  {
    public Parent(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramElement1 == paramElement2)
      {
        bool1 = bool2;
        return bool1;
      }
      for (paramElement2 = paramElement2.parent();; paramElement2 = paramElement2.parent())
      {
        if (this.evaluator.matches(paramElement1, paramElement2))
        {
          bool1 = true;
          break;
        }
        bool1 = bool2;
        if (paramElement2 == paramElement1) {
          break;
        }
      }
    }
    
    public String toString()
    {
      return String.format(":parent%s", new Object[] { this.evaluator });
    }
  }
  
  static class PreviousSibling
    extends StructuralEvaluator
  {
    public PreviousSibling(Evaluator paramEvaluator)
    {
      this.evaluator = paramEvaluator;
    }
    
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      boolean bool2 = false;
      boolean bool1;
      if (paramElement1 == paramElement2)
      {
        bool1 = bool2;
        return bool1;
      }
      for (paramElement2 = paramElement2.previousElementSibling();; paramElement2 = paramElement2.previousElementSibling())
      {
        bool1 = bool2;
        if (paramElement2 == null) {
          break;
        }
        if (this.evaluator.matches(paramElement1, paramElement2))
        {
          bool1 = true;
          break;
        }
      }
    }
    
    public String toString()
    {
      return String.format(":prev*%s", new Object[] { this.evaluator });
    }
  }
  
  static class Root
    extends Evaluator
  {
    public boolean matches(Element paramElement1, Element paramElement2)
    {
      if (paramElement1 == paramElement2) {}
      for (boolean bool = true;; bool = false) {
        return bool;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\select\StructuralEvaluator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */