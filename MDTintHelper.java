package com.afollestad.materialdialogs.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import com.afollestad.materialdialogs.R.attr;
import com.afollestad.materialdialogs.R.drawable;
import com.afollestad.materialdialogs.util.DialogUtils;
import java.lang.reflect.Field;

@SuppressLint({"PrivateResource"})
public class MDTintHelper
{
  private static ColorStateList createEditTextColorStateList(@NonNull Context paramContext, @ColorInt int paramInt)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = { -16842910 };
    arrayOfInt1[0] = DialogUtils.resolveColor(paramContext, R.attr.colorControlNormal);
    int i = 0 + 1;
    arrayOfInt[i] = { -16842919, -16842908 };
    arrayOfInt1[i] = DialogUtils.resolveColor(paramContext, R.attr.colorControlNormal);
    i++;
    arrayOfInt[i] = new int[0];
    arrayOfInt1[i] = paramInt;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private static void setCursorTint(@NonNull EditText paramEditText, @ColorInt int paramInt)
  {
    try
    {
      Object localObject1 = TextView.class.getDeclaredField("mCursorDrawableRes");
      ((Field)localObject1).setAccessible(true);
      int i = ((Field)localObject1).getInt(paramEditText);
      localObject1 = TextView.class.getDeclaredField("mEditor");
      ((Field)localObject1).setAccessible(true);
      Object localObject2 = ((Field)localObject1).get(paramEditText);
      Field localField = localObject2.getClass().getDeclaredField("mCursorDrawable");
      localField.setAccessible(true);
      localObject1 = new Drawable[2];
      localObject1[0] = ContextCompat.getDrawable(paramEditText.getContext(), i);
      localObject1[1] = ContextCompat.getDrawable(paramEditText.getContext(), i);
      localObject1[0].setColorFilter(paramInt, PorterDuff.Mode.SRC_IN);
      localObject1[1].setColorFilter(paramInt, PorterDuff.Mode.SRC_IN);
      localField.set(localObject2, localObject1);
      return;
    }
    catch (NoSuchFieldException paramEditText)
    {
      for (;;)
      {
        Log.d("MDTintHelper", "Device issue with cursor tinting: " + paramEditText.getMessage());
        paramEditText.printStackTrace();
      }
    }
    catch (Exception paramEditText)
    {
      for (;;)
      {
        paramEditText.printStackTrace();
      }
    }
  }
  
  public static void setTint(@NonNull CheckBox paramCheckBox, @ColorInt int paramInt)
  {
    int j = DialogUtils.getDisabledColor(paramCheckBox.getContext());
    int i = DialogUtils.resolveColor(paramCheckBox.getContext(), R.attr.colorControlNormal);
    setTint(paramCheckBox, new ColorStateList(new int[][] { { 16842910, -16842912 }, { 16842910, 16842912 }, { -16842910, -16842912 }, { -16842910, 16842912 } }, new int[] { i, paramInt, j, j }));
  }
  
  public static void setTint(@NonNull CheckBox paramCheckBox, @NonNull ColorStateList paramColorStateList)
  {
    if (Build.VERSION.SDK_INT >= 21) {
      paramCheckBox.setButtonTintList(paramColorStateList);
    }
    for (;;)
    {
      return;
      Drawable localDrawable = DrawableCompat.wrap(ContextCompat.getDrawable(paramCheckBox.getContext(), R.drawable.abc_btn_check_material));
      DrawableCompat.setTintList(localDrawable, paramColorStateList);
      paramCheckBox.setButtonDrawable(localDrawable);
    }
  }
  
  public static void setTint(@NonNull EditText paramEditText, @ColorInt int paramInt)
  {
    ColorStateList localColorStateList = createEditTextColorStateList(paramEditText.getContext(), paramInt);
    if ((paramEditText instanceof AppCompatEditText)) {
      ((AppCompatEditText)paramEditText).setSupportBackgroundTintList(localColorStateList);
    }
    for (;;)
    {
      setCursorTint(paramEditText, paramInt);
      return;
      if (Build.VERSION.SDK_INT >= 21) {
        paramEditText.setBackgroundTintList(localColorStateList);
      }
    }
  }
  
  public static void setTint(@NonNull ProgressBar paramProgressBar, @ColorInt int paramInt)
  {
    setTint(paramProgressBar, paramInt, false);
  }
  
  private static void setTint(@NonNull ProgressBar paramProgressBar, @ColorInt int paramInt, boolean paramBoolean)
  {
    Object localObject = ColorStateList.valueOf(paramInt);
    if (Build.VERSION.SDK_INT >= 21)
    {
      paramProgressBar.setProgressTintList((ColorStateList)localObject);
      paramProgressBar.setSecondaryProgressTintList((ColorStateList)localObject);
      if (!paramBoolean) {
        paramProgressBar.setIndeterminateTintList((ColorStateList)localObject);
      }
    }
    for (;;)
    {
      return;
      localObject = PorterDuff.Mode.SRC_IN;
      if (Build.VERSION.SDK_INT <= 10) {
        localObject = PorterDuff.Mode.MULTIPLY;
      }
      if ((!paramBoolean) && (paramProgressBar.getIndeterminateDrawable() != null)) {
        paramProgressBar.getIndeterminateDrawable().setColorFilter(paramInt, (PorterDuff.Mode)localObject);
      }
      if (paramProgressBar.getProgressDrawable() != null) {
        paramProgressBar.getProgressDrawable().setColorFilter(paramInt, (PorterDuff.Mode)localObject);
      }
    }
  }
  
  public static void setTint(@NonNull RadioButton paramRadioButton, @ColorInt int paramInt)
  {
    int j = DialogUtils.getDisabledColor(paramRadioButton.getContext());
    int[] arrayOfInt1 = { 16842910, -16842912 };
    int[] arrayOfInt2 = { 16842910, 16842912 };
    int[] arrayOfInt3 = { -16842910, 16842912 };
    int i = DialogUtils.resolveColor(paramRadioButton.getContext(), R.attr.colorControlNormal);
    setTint(paramRadioButton, new ColorStateList(new int[][] { arrayOfInt1, arrayOfInt2, { -16842910, -16842912 }, arrayOfInt3 }, new int[] { i, paramInt, j, j }));
  }
  
  public static void setTint(@NonNull RadioButton paramRadioButton, @NonNull ColorStateList paramColorStateList)
  {
    if (Build.VERSION.SDK_INT >= 21) {
      paramRadioButton.setButtonTintList(paramColorStateList);
    }
    for (;;)
    {
      return;
      Drawable localDrawable = DrawableCompat.wrap(ContextCompat.getDrawable(paramRadioButton.getContext(), R.drawable.abc_btn_radio_material));
      DrawableCompat.setTintList(localDrawable, paramColorStateList);
      paramRadioButton.setButtonDrawable(localDrawable);
    }
  }
  
  public static void setTint(@NonNull SeekBar paramSeekBar, @ColorInt int paramInt)
  {
    Object localObject = ColorStateList.valueOf(paramInt);
    if (Build.VERSION.SDK_INT >= 21)
    {
      paramSeekBar.setThumbTintList((ColorStateList)localObject);
      paramSeekBar.setProgressTintList((ColorStateList)localObject);
    }
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT > 10)
      {
        Drawable localDrawable = DrawableCompat.wrap(paramSeekBar.getProgressDrawable());
        paramSeekBar.setProgressDrawable(localDrawable);
        DrawableCompat.setTintList(localDrawable, (ColorStateList)localObject);
        if (Build.VERSION.SDK_INT >= 16)
        {
          localDrawable = DrawableCompat.wrap(paramSeekBar.getThumb());
          DrawableCompat.setTintList(localDrawable, (ColorStateList)localObject);
          paramSeekBar.setThumb(localDrawable);
        }
      }
      else
      {
        localObject = PorterDuff.Mode.SRC_IN;
        if (Build.VERSION.SDK_INT <= 10) {
          localObject = PorterDuff.Mode.MULTIPLY;
        }
        if (paramSeekBar.getIndeterminateDrawable() != null) {
          paramSeekBar.getIndeterminateDrawable().setColorFilter(paramInt, (PorterDuff.Mode)localObject);
        }
        if (paramSeekBar.getProgressDrawable() != null) {
          paramSeekBar.getProgressDrawable().setColorFilter(paramInt, (PorterDuff.Mode)localObject);
        }
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\afollestad\materialdialogs\internal\MDTintHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */