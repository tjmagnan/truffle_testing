package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import com.android.volley.VolleyError;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.server.ServerManager;
import tech.dcube.companion.managers.server.ServerManager.RequestCompletionHandler;
import tech.dcube.companion.model.PBAddress;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBShipment;
import tech.dcube.companion.model.PBShipmentPackage;

public class PBPackageTypeFragment
  extends PBTopBarFragment
  implements View.OnClickListener
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  LinearLayout Envelope;
  LinearLayout LargePackage;
  LinearLayout NormalPackage;
  LinearLayout PriorityMailFlatRateBox;
  LinearLayout PriorityMailFlatRateEnvelope;
  private OnPackageTypeInteractionListener mListener;
  private String mParam1;
  private String mParam2;
  boolean packageTypesLoaded = false;
  
  public static PBPackageTypeFragment newInstance(String paramString1, String paramString2)
  {
    PBPackageTypeFragment localPBPackageTypeFragment = new PBPackageTypeFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBPackageTypeFragment.setArguments(localBundle);
    return localPBPackageTypeFragment;
  }
  
  public void GetAllPackageTypes()
  {
    if (this.mListener != null) {
      this.mListener.onGetAllPackagesV0();
    }
  }
  
  void callGetAvailablePackagesApi()
  {
    PBAddress localPBAddress = DataManager.getInstance().getCurrentShipment().getReceiverAddress();
    try
    {
      ServerManager localServerManager = ServerManager.getInstance();
      Context localContext = getContext();
      ServerManager.RequestCompletionHandler local1 = new tech/dcube/companion/fragments/PBPackageTypeFragment$1;
      local1.<init>(this);
      localServerManager.getAvailablePackages(localContext, localPBAddress, local1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  void hidePackageTypes()
  {
    this.PriorityMailFlatRateBox.setVisibility(8);
    this.PriorityMailFlatRateEnvelope.setVisibility(8);
    this.Envelope.setVisibility(8);
    this.NormalPackage.setVisibility(8);
    this.LargePackage.setVisibility(8);
  }
  
  void launchLargePackageFragment()
  {
    getActivity().getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackageLargePackageFragment(), PBPackageLargePackageFragment.class.getName()).addToBackStack(PBPackageLargePackageFragment.class.getName()).commit();
  }
  
  void launchNormalEnvelopeFragment()
  {
    getActivity().getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackageEnvelopeFragment(), PBPackageEnvelopeFragment.class.getName()).addToBackStack(PBPackageEnvelopeFragment.class.getName()).commit();
  }
  
  void launchNormalPackageFragment()
  {
    getActivity().getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackageNormalPackageFragment(), PBPackageNormalPackageFragment.class.getName()).addToBackStack(PBPackageNormalPackageFragment.class.getName()).commit();
  }
  
  void launchPMFRBoxFragment()
  {
    getActivity().getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackagePMFRBoxFragment(), PBPackagePMFRBoxFragment.class.getName()).addToBackStack(PBPackagePMFRBoxFragment.class.getName()).commit();
  }
  
  void launchPMFREnvelopeFragment()
  {
    getActivity().getSupportFragmentManager().beginTransaction().add(2131624061, new PBPackagePMFREnvelopeFragment(), PBPackagePMFREnvelopeFragment.class.getName()).addToBackStack(PBPackagePMFREnvelopeFragment.class.getName()).commit();
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnPackageTypeInteractionListener))
    {
      this.mListener = ((OnPackageTypeInteractionListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement");
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      onSelectedPackageType(PBPackageType.FLAT_RATE_BOX);
      launchPMFRBoxFragment();
      continue;
      onSelectedPackageType(PBPackageType.FLAT_RATE_ENVELOPE);
      launchPMFREnvelopeFragment();
      continue;
      onSelectedPackageType(PBPackageType.ENVELOPE);
      launchNormalEnvelopeFragment();
      continue;
      onSelectedPackageType(PBPackageType.PACKAGE);
      launchNormalPackageFragment();
      continue;
      onSelectedPackageType(PBPackageType.PACKAGE_LARGE);
      launchLargePackageFragment();
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968638, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Package Type");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    this.PriorityMailFlatRateBox = ((LinearLayout)paramLayoutInflater.findViewById(2131624151));
    this.PriorityMailFlatRateEnvelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624152));
    this.Envelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624153));
    this.NormalPackage = ((LinearLayout)paramLayoutInflater.findViewById(2131624154));
    this.LargePackage = ((LinearLayout)paramLayoutInflater.findViewById(2131624155));
    this.PriorityMailFlatRateBox.setOnClickListener(this);
    this.PriorityMailFlatRateEnvelope.setOnClickListener(this);
    this.Envelope.setOnClickListener(this);
    this.NormalPackage.setOnClickListener(this);
    this.LargePackage.setOnClickListener(this);
    hidePackageTypes();
    GetAllPackageTypes();
    callGetAvailablePackagesApi();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onSelectedPackageType(PBPackageType paramPBPackageType)
  {
    if (this.mListener != null) {
      this.mListener.onPackageTypeSelectInteraction(paramPBPackageType);
    }
  }
  
  void showAvailablePackageType()
  {
    if (!this.packageTypesLoaded) {}
    for (;;)
    {
      return;
      boolean bool4 = DataManager.getInstance().getShipmentPackages().containsKey(PBPackageType.FLAT_RATE_BOX);
      boolean bool2 = DataManager.getInstance().getShipmentPackages().containsKey(PBPackageType.FLAT_RATE_ENVELOPE);
      boolean bool1 = DataManager.getInstance().getShipmentPackages().containsKey(PBPackageType.ENVELOPE);
      boolean bool3 = DataManager.getInstance().getShipmentPackages().containsKey(PBPackageType.PACKAGE);
      boolean bool5 = DataManager.getInstance().getShipmentPackages().containsKey(PBPackageType.PACKAGE_LARGE);
      if (bool4)
      {
        this.PriorityMailFlatRateBox.setVisibility(0);
        label98:
        if (!bool2) {
          break label162;
        }
        this.PriorityMailFlatRateEnvelope.setVisibility(0);
        label110:
        if (!bool1) {
          break label174;
        }
        this.Envelope.setVisibility(0);
        label122:
        if (!bool3) {
          break label186;
        }
        this.NormalPackage.setVisibility(0);
      }
      for (;;)
      {
        if (!bool5) {
          break label198;
        }
        this.LargePackage.setVisibility(0);
        break;
        this.PriorityMailFlatRateBox.setVisibility(8);
        break label98;
        label162:
        this.PriorityMailFlatRateEnvelope.setVisibility(8);
        break label110;
        label174:
        this.Envelope.setVisibility(8);
        break label122;
        label186:
        this.NormalPackage.setVisibility(8);
      }
      label198:
      this.LargePackage.setVisibility(8);
    }
  }
  
  public static abstract interface OnPackageTypeInteractionListener
  {
    public abstract void onGetAllPackagesV0();
    
    public abstract void onPackageTypeSelectInteraction(PBPackageType paramPBPackageType);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBPackageTypeFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */