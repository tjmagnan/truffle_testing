package tech.dcube.companion.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class PBMeterDetailFragment
  extends Fragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private String mParam1;
  private String mParam2;
  
  public static PBMeterDetailFragment newInstance(String paramString1, String paramString2)
  {
    PBMeterDetailFragment localPBMeterDetailFragment = new PBMeterDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBMeterDetailFragment.setArguments(localBundle);
    return localPBMeterDetailFragment;
  }
  
  public void onButtonPressed(Uri paramUri) {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968634, paramViewGroup, false);
    ((Button)paramLayoutInflater.findViewById(2131624133)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        paramAnonymousView = PBMeterDetailFragment.this.getActivity().getSupportFragmentManager().beginTransaction();
        paramAnonymousView.replace(2131624061, new PBHomeFragment(), "tag0");
        paramAnonymousView.commit();
      }
    });
    return paramLayoutInflater;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBMeterDetailFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */