package org.joda.time.chrono;

import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

class GJCacheKey
{
  private final Instant cutoverInstant;
  private final int minDaysInFirstWeek;
  private final DateTimeZone zone;
  
  GJCacheKey(DateTimeZone paramDateTimeZone, Instant paramInstant, int paramInt)
  {
    this.zone = paramDateTimeZone;
    this.cutoverInstant = paramInstant;
    this.minDaysInFirstWeek = paramInt;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool;
      if (paramObject == null)
      {
        bool = false;
      }
      else if (!(paramObject instanceof GJCacheKey))
      {
        bool = false;
      }
      else
      {
        paramObject = (GJCacheKey)paramObject;
        if (this.cutoverInstant == null)
        {
          if (((GJCacheKey)paramObject).cutoverInstant != null) {
            bool = false;
          }
        }
        else if (!this.cutoverInstant.equals(((GJCacheKey)paramObject).cutoverInstant))
        {
          bool = false;
          continue;
        }
        if (this.minDaysInFirstWeek != ((GJCacheKey)paramObject).minDaysInFirstWeek) {
          bool = false;
        } else if (this.zone == null)
        {
          if (((GJCacheKey)paramObject).zone != null) {
            bool = false;
          }
        }
        else if (!this.zone.equals(((GJCacheKey)paramObject).zone)) {
          bool = false;
        }
      }
    }
  }
  
  public int hashCode()
  {
    int j = 0;
    int i;
    int k;
    if (this.cutoverInstant == null)
    {
      i = 0;
      k = this.minDaysInFirstWeek;
      if (this.zone != null) {
        break label49;
      }
    }
    for (;;)
    {
      return ((i + 31) * 31 + k) * 31 + j;
      i = this.cutoverInstant.hashCode();
      break;
      label49:
      j = this.zone.hashCode();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\GJCacheKey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */