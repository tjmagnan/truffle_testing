package com.crashlytics.android.answers;

import java.util.HashSet;
import java.util.Set;

class SamplingEventFilter
  implements EventFilter
{
  static final Set<SessionEvent.Type> EVENTS_TYPE_TO_SAMPLE = new HashSet() {};
  final int samplingRate;
  
  public SamplingEventFilter(int paramInt)
  {
    this.samplingRate = paramInt;
  }
  
  public boolean skipEvent(SessionEvent paramSessionEvent)
  {
    boolean bool = true;
    int i;
    int j;
    if ((EVENTS_TYPE_TO_SAMPLE.contains(paramSessionEvent.type)) && (paramSessionEvent.sessionEventMetadata.betaDeviceToken == null))
    {
      i = 1;
      if (Math.abs(paramSessionEvent.sessionEventMetadata.installationId.hashCode() % this.samplingRate) == 0) {
        break label69;
      }
      j = 1;
      label53:
      if ((i == 0) || (j == 0)) {
        break label74;
      }
    }
    for (;;)
    {
      return bool;
      i = 0;
      break;
      label69:
      j = 0;
      break label53;
      label74:
      bool = false;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\crashlytics\android\answers\SamplingEventFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */