package org.joda.time.chrono;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.field.DividedDateTimeField;
import org.joda.time.field.RemainderDateTimeField;

public final class ISOChronology
  extends AssembledChronology
{
  private static final ISOChronology INSTANCE_UTC;
  private static final ConcurrentHashMap<DateTimeZone, ISOChronology> cCache = new ConcurrentHashMap();
  private static final long serialVersionUID = -6212696554273812441L;
  
  static
  {
    INSTANCE_UTC = new ISOChronology(GregorianChronology.getInstanceUTC());
    cCache.put(DateTimeZone.UTC, INSTANCE_UTC);
  }
  
  private ISOChronology(Chronology paramChronology)
  {
    super(paramChronology, null);
  }
  
  public static ISOChronology getInstance()
  {
    return getInstance(DateTimeZone.getDefault());
  }
  
  public static ISOChronology getInstance(DateTimeZone paramDateTimeZone)
  {
    Object localObject = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localObject = DateTimeZone.getDefault();
    }
    ISOChronology localISOChronology = (ISOChronology)cCache.get(localObject);
    paramDateTimeZone = localISOChronology;
    if (localISOChronology == null)
    {
      paramDateTimeZone = new ISOChronology(ZonedChronology.getInstance(INSTANCE_UTC, (DateTimeZone)localObject));
      localObject = (ISOChronology)cCache.putIfAbsent(localObject, paramDateTimeZone);
      if (localObject == null) {
        break label62;
      }
      paramDateTimeZone = (DateTimeZone)localObject;
    }
    label62:
    for (;;)
    {
      return paramDateTimeZone;
    }
  }
  
  public static ISOChronology getInstanceUTC()
  {
    return INSTANCE_UTC;
  }
  
  private Object writeReplace()
  {
    return new Stub(getZone());
  }
  
  protected void assemble(AssembledChronology.Fields paramFields)
  {
    if (getBase().getZone() == DateTimeZone.UTC)
    {
      paramFields.centuryOfEra = new DividedDateTimeField(ISOYearOfEraDateTimeField.INSTANCE, DateTimeFieldType.centuryOfEra(), 100);
      paramFields.centuries = paramFields.centuryOfEra.getDurationField();
      paramFields.yearOfCentury = new RemainderDateTimeField((DividedDateTimeField)paramFields.centuryOfEra, DateTimeFieldType.yearOfCentury());
      paramFields.weekyearOfCentury = new RemainderDateTimeField((DividedDateTimeField)paramFields.centuryOfEra, paramFields.weekyears, DateTimeFieldType.weekyearOfCentury());
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      if ((paramObject instanceof ISOChronology))
      {
        paramObject = (ISOChronology)paramObject;
        bool = getZone().equals(((ISOChronology)paramObject).getZone());
      }
      else
      {
        bool = false;
      }
    }
  }
  
  public int hashCode()
  {
    return "ISO".hashCode() * 11 + getZone().hashCode();
  }
  
  public String toString()
  {
    String str = "ISOChronology";
    DateTimeZone localDateTimeZone = getZone();
    if (localDateTimeZone != null) {
      str = "ISOChronology" + '[' + localDateTimeZone.getID() + ']';
    }
    return str;
  }
  
  public Chronology withUTC()
  {
    return INSTANCE_UTC;
  }
  
  public Chronology withZone(DateTimeZone paramDateTimeZone)
  {
    DateTimeZone localDateTimeZone = paramDateTimeZone;
    if (paramDateTimeZone == null) {
      localDateTimeZone = DateTimeZone.getDefault();
    }
    if (localDateTimeZone == getZone()) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = getInstance(localDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
  
  private static final class Stub
    implements Serializable
  {
    private static final long serialVersionUID = -6212696554273812441L;
    private transient DateTimeZone iZone;
    
    Stub(DateTimeZone paramDateTimeZone)
    {
      this.iZone = paramDateTimeZone;
    }
    
    private void readObject(ObjectInputStream paramObjectInputStream)
      throws IOException, ClassNotFoundException
    {
      this.iZone = ((DateTimeZone)paramObjectInputStream.readObject());
    }
    
    private Object readResolve()
    {
      return ISOChronology.getInstance(this.iZone);
    }
    
    private void writeObject(ObjectOutputStream paramObjectOutputStream)
      throws IOException
    {
      paramObjectOutputStream.writeObject(this.iZone);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\ISOChronology.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */