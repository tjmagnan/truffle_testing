package com.testfairy.h;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.util.ByteArrayBuffer;

public class c
{
  private static final int b = 4096;
  protected static final int c = 0;
  protected static final int d = 1;
  protected static final int e = 2;
  protected static final int f = 3;
  private static final String g = "gzip";
  private Handler a;
  
  public c()
  {
    if (Looper.myLooper() != null) {
      this.a = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          c.this.a(paramAnonymousMessage);
        }
      };
    }
  }
  
  private String a(HttpEntity paramHttpEntity, String paramString)
  {
    Object localObject2 = paramHttpEntity.getContent();
    if (localObject2 == null)
    {
      paramHttpEntity = null;
      return paramHttpEntity;
    }
    long l = paramHttpEntity.getContentLength();
    if (l > 2147483647L)
    {
      ((InputStream)localObject2).close();
      throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
    }
    paramHttpEntity = paramHttpEntity.getContentEncoding();
    Object localObject1 = localObject2;
    int i;
    if (paramHttpEntity != null)
    {
      HeaderElement[] arrayOfHeaderElement = paramHttpEntity.getElements();
      int j = arrayOfHeaderElement.length;
      i = 0;
      paramHttpEntity = (HttpEntity)localObject2;
      label82:
      localObject1 = paramHttpEntity;
      if (i < j)
      {
        if (!arrayOfHeaderElement[i].getName().equalsIgnoreCase("gzip")) {
          break label230;
        }
        paramHttpEntity = new GZIPInputStream(paramHttpEntity);
      }
    }
    label230:
    for (;;)
    {
      i++;
      break label82;
      if (l < 0L) {
        i = 4096;
      }
      for (;;)
      {
        try
        {
          paramHttpEntity = new org/apache/http/util/ByteArrayBuffer;
          paramHttpEntity.<init>(i);
          localObject2 = new byte['က'];
          i = ((InputStream)localObject1).read((byte[])localObject2);
          if ((i == -1) || (Thread.currentThread().isInterrupted())) {
            break;
          }
          paramHttpEntity.append((byte[])localObject2, 0, i);
          continue;
          i = (int)l;
        }
        catch (OutOfMemoryError paramHttpEntity)
        {
          System.gc();
          ((InputStream)localObject1).close();
          throw new IOException("File too large to fit into available memory");
        }
      }
      ((InputStream)localObject1).close();
      paramHttpEntity = new String(paramHttpEntity.toByteArray(), paramString);
      break;
    }
  }
  
  protected Message a(int paramInt, Object paramObject)
  {
    if (this.a != null) {}
    Message localMessage;
    for (paramObject = this.a.obtainMessage(paramInt, paramObject);; paramObject = localMessage)
    {
      return (Message)paramObject;
      localMessage = new Message();
      localMessage.what = paramInt;
      localMessage.obj = paramObject;
    }
  }
  
  public void a() {}
  
  protected void a(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    }
    for (;;)
    {
      return;
      c((String)paramMessage.obj);
      continue;
      paramMessage = (Object[])paramMessage.obj;
      c((Throwable)paramMessage[0], (String)paramMessage[1]);
      continue;
      b();
      continue;
      a();
    }
  }
  
  public void a(String paramString) {}
  
  public void a(Throwable paramThrowable, String paramString) {}
  
  protected void a(Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    b(a(1, new Object[] { paramThrowable, paramArrayOfByte }));
  }
  
  void a(HttpResponse paramHttpResponse)
  {
    Object localObject2 = null;
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    Object localObject1 = localObject2;
    try
    {
      if (paramHttpResponse.getEntity() != null) {
        localObject1 = a(paramHttpResponse.getEntity(), "UTF-8");
      }
      if (localStatusLine.getStatusCode() >= 300)
      {
        b(new HttpResponseException(localStatusLine.getStatusCode(), localStatusLine.getReasonPhrase()), (String)localObject1);
        return;
      }
    }
    catch (IOException paramHttpResponse)
    {
      for (;;)
      {
        b(paramHttpResponse, (String)null);
        localObject1 = localObject2;
        continue;
        b((String)localObject1);
      }
    }
  }
  
  public void b() {}
  
  protected void b(Message paramMessage)
  {
    if (this.a != null) {
      this.a.sendMessage(paramMessage);
    }
    for (;;)
    {
      return;
      a(paramMessage);
    }
  }
  
  protected void b(String paramString)
  {
    b(a(0, paramString));
  }
  
  protected void b(Throwable paramThrowable, String paramString)
  {
    b(a(1, new Object[] { paramThrowable, paramString }));
  }
  
  protected void c()
  {
    b(a(2, null));
  }
  
  protected void c(String paramString)
  {
    a(paramString);
  }
  
  protected void c(Throwable paramThrowable, String paramString)
  {
    a(paramThrowable, paramString);
  }
  
  protected void d()
  {
    b(a(3, null));
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\h\c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */