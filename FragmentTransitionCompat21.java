package android.support.v4.app;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.support.annotation.RequiresApi;
import android.transition.Transition;
import android.transition.Transition.EpicenterCallback;
import android.transition.Transition.TransitionListener;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@TargetApi(21)
@RequiresApi(21)
class FragmentTransitionCompat21
{
  public static void addTarget(Object paramObject, View paramView)
  {
    if (paramObject != null) {
      ((Transition)paramObject).addTarget(paramView);
    }
  }
  
  public static void addTargets(Object paramObject, ArrayList<View> paramArrayList)
  {
    paramObject = (Transition)paramObject;
    if (paramObject == null) {}
    for (;;)
    {
      return;
      int j;
      int i;
      if ((paramObject instanceof TransitionSet))
      {
        paramObject = (TransitionSet)paramObject;
        j = ((TransitionSet)paramObject).getTransitionCount();
        for (i = 0; i < j; i++) {
          addTargets(((TransitionSet)paramObject).getTransitionAt(i), paramArrayList);
        }
      }
      else if ((!hasSimpleTarget((Transition)paramObject)) && (isNullOrEmpty(((Transition)paramObject).getTargets())))
      {
        j = paramArrayList.size();
        for (i = 0; i < j; i++) {
          ((Transition)paramObject).addTarget((View)paramArrayList.get(i));
        }
      }
    }
  }
  
  public static void beginDelayedTransition(ViewGroup paramViewGroup, Object paramObject)
  {
    TransitionManager.beginDelayedTransition(paramViewGroup, (Transition)paramObject);
  }
  
  private static void bfsAddViewChildren(List<View> paramList, View paramView)
  {
    int k = paramList.size();
    if (containedBeforeIndex(paramList, paramView, k)) {}
    for (;;)
    {
      return;
      paramList.add(paramView);
      for (int i = k; i < paramList.size(); i++)
      {
        paramView = (View)paramList.get(i);
        if ((paramView instanceof ViewGroup))
        {
          ViewGroup localViewGroup = (ViewGroup)paramView;
          int m = localViewGroup.getChildCount();
          for (int j = 0; j < m; j++)
          {
            paramView = localViewGroup.getChildAt(j);
            if (!containedBeforeIndex(paramList, paramView, k)) {
              paramList.add(paramView);
            }
          }
        }
      }
    }
  }
  
  public static void captureTransitioningViews(ArrayList<View> paramArrayList, View paramView)
  {
    if (paramView.getVisibility() == 0)
    {
      if (!(paramView instanceof ViewGroup)) {
        break label60;
      }
      paramView = (ViewGroup)paramView;
      if (!paramView.isTransitionGroup()) {
        break label33;
      }
      paramArrayList.add(paramView);
    }
    for (;;)
    {
      return;
      label33:
      int j = paramView.getChildCount();
      for (int i = 0; i < j; i++) {
        captureTransitioningViews(paramArrayList, paramView.getChildAt(i));
      }
      continue;
      label60:
      paramArrayList.add(paramView);
    }
  }
  
  public static Object cloneTransition(Object paramObject)
  {
    Transition localTransition = null;
    if (paramObject != null) {
      localTransition = ((Transition)paramObject).clone();
    }
    return localTransition;
  }
  
  private static boolean containedBeforeIndex(List<View> paramList, View paramView, int paramInt)
  {
    int i = 0;
    if (i < paramInt) {
      if (paramList.get(i) != paramView) {}
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      i++;
      break;
    }
  }
  
  private static String findKeyForValue(Map<String, String> paramMap, String paramString)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    do
    {
      if (!localIterator.hasNext()) {
        break;
      }
      paramMap = (Map.Entry)localIterator.next();
    } while (!paramString.equals(paramMap.getValue()));
    for (paramMap = (String)paramMap.getKey();; paramMap = null) {
      return paramMap;
    }
  }
  
  public static void findNamedViews(Map<String, View> paramMap, View paramView)
  {
    if (paramView.getVisibility() == 0)
    {
      String str = paramView.getTransitionName();
      if (str != null) {
        paramMap.put(str, paramView);
      }
      if ((paramView instanceof ViewGroup))
      {
        paramView = (ViewGroup)paramView;
        int j = paramView.getChildCount();
        for (int i = 0; i < j; i++) {
          findNamedViews(paramMap, paramView.getChildAt(i));
        }
      }
    }
  }
  
  public static void getBoundsOnScreen(View paramView, Rect paramRect)
  {
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    paramRect.set(arrayOfInt[0], arrayOfInt[1], arrayOfInt[0] + paramView.getWidth(), arrayOfInt[1] + paramView.getHeight());
  }
  
  private static boolean hasSimpleTarget(Transition paramTransition)
  {
    if ((!isNullOrEmpty(paramTransition.getTargetIds())) || (!isNullOrEmpty(paramTransition.getTargetNames())) || (!isNullOrEmpty(paramTransition.getTargetTypes()))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private static boolean isNullOrEmpty(List paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static Object mergeTransitionsInSequence(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    Object localObject = null;
    paramObject1 = (Transition)paramObject1;
    paramObject2 = (Transition)paramObject2;
    paramObject3 = (Transition)paramObject3;
    if ((paramObject1 != null) && (paramObject2 != null))
    {
      paramObject1 = new TransitionSet().addTransition((Transition)paramObject1).addTransition((Transition)paramObject2).setOrdering(1);
      if (paramObject3 == null) {
        break label95;
      }
      paramObject2 = new TransitionSet();
      if (paramObject1 != null) {
        ((TransitionSet)paramObject2).addTransition((Transition)paramObject1);
      }
      ((TransitionSet)paramObject2).addTransition((Transition)paramObject3);
      paramObject1 = paramObject2;
    }
    label95:
    for (;;)
    {
      return paramObject1;
      if (paramObject1 != null) {
        break;
      }
      paramObject1 = localObject;
      if (paramObject2 == null) {
        break;
      }
      paramObject1 = paramObject2;
      break;
    }
  }
  
  public static Object mergeTransitionsTogether(Object paramObject1, Object paramObject2, Object paramObject3)
  {
    TransitionSet localTransitionSet = new TransitionSet();
    if (paramObject1 != null) {
      localTransitionSet.addTransition((Transition)paramObject1);
    }
    if (paramObject2 != null) {
      localTransitionSet.addTransition((Transition)paramObject2);
    }
    if (paramObject3 != null) {
      localTransitionSet.addTransition((Transition)paramObject3);
    }
    return localTransitionSet;
  }
  
  public static ArrayList<String> prepareSetNameOverridesOptimized(ArrayList<View> paramArrayList)
  {
    ArrayList localArrayList = new ArrayList();
    int j = paramArrayList.size();
    for (int i = 0; i < j; i++)
    {
      View localView = (View)paramArrayList.get(i);
      localArrayList.add(localView.getTransitionName());
      localView.setTransitionName(null);
    }
    return localArrayList;
  }
  
  public static void removeTarget(Object paramObject, View paramView)
  {
    if (paramObject != null) {
      ((Transition)paramObject).removeTarget(paramView);
    }
  }
  
  public static void replaceTargets(Object paramObject, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2)
  {
    paramObject = (Transition)paramObject;
    int j;
    int i;
    if ((paramObject instanceof TransitionSet))
    {
      paramObject = (TransitionSet)paramObject;
      j = ((TransitionSet)paramObject).getTransitionCount();
      for (i = 0; i < j; i++) {
        replaceTargets(((TransitionSet)paramObject).getTransitionAt(i), paramArrayList1, paramArrayList2);
      }
    }
    if (!hasSimpleTarget((Transition)paramObject))
    {
      List localList = ((Transition)paramObject).getTargets();
      if ((localList != null) && (localList.size() == paramArrayList1.size()) && (localList.containsAll(paramArrayList1)))
      {
        if (paramArrayList2 == null) {}
        for (i = 0;; i = paramArrayList2.size()) {
          for (j = 0; j < i; j++) {
            ((Transition)paramObject).addTarget((View)paramArrayList2.get(j));
          }
        }
        for (i = paramArrayList1.size() - 1; i >= 0; i--) {
          ((Transition)paramObject).removeTarget((View)paramArrayList1.get(i));
        }
      }
    }
  }
  
  public static void scheduleHideFragmentView(Object paramObject, View paramView, final ArrayList<View> paramArrayList)
  {
    ((Transition)paramObject).addListener(new Transition.TransitionListener()
    {
      public void onTransitionCancel(Transition paramAnonymousTransition) {}
      
      public void onTransitionEnd(Transition paramAnonymousTransition)
      {
        paramAnonymousTransition.removeListener(this);
        this.val$fragmentView.setVisibility(8);
        int j = paramArrayList.size();
        for (int i = 0; i < j; i++) {
          ((View)paramArrayList.get(i)).setVisibility(0);
        }
      }
      
      public void onTransitionPause(Transition paramAnonymousTransition) {}
      
      public void onTransitionResume(Transition paramAnonymousTransition) {}
      
      public void onTransitionStart(Transition paramAnonymousTransition) {}
    });
  }
  
  public static void scheduleNameReset(ViewGroup paramViewGroup, ArrayList<View> paramArrayList, final Map<String, String> paramMap)
  {
    OneShotPreDrawListener.add(paramViewGroup, new Runnable()
    {
      public void run()
      {
        int j = this.val$sharedElementsIn.size();
        for (int i = 0; i < j; i++)
        {
          View localView = (View)this.val$sharedElementsIn.get(i);
          String str = localView.getTransitionName();
          localView.setTransitionName((String)paramMap.get(str));
        }
      }
    });
  }
  
  public static void scheduleRemoveTargets(Object paramObject1, Object paramObject2, final ArrayList<View> paramArrayList1, final Object paramObject3, final ArrayList<View> paramArrayList2, final Object paramObject4, final ArrayList<View> paramArrayList3)
  {
    ((Transition)paramObject1).addListener(new Transition.TransitionListener()
    {
      public void onTransitionCancel(Transition paramAnonymousTransition) {}
      
      public void onTransitionEnd(Transition paramAnonymousTransition) {}
      
      public void onTransitionPause(Transition paramAnonymousTransition) {}
      
      public void onTransitionResume(Transition paramAnonymousTransition) {}
      
      public void onTransitionStart(Transition paramAnonymousTransition)
      {
        if (this.val$enterTransition != null) {
          FragmentTransitionCompat21.replaceTargets(this.val$enterTransition, paramArrayList1, null);
        }
        if (paramObject3 != null) {
          FragmentTransitionCompat21.replaceTargets(paramObject3, paramArrayList2, null);
        }
        if (paramObject4 != null) {
          FragmentTransitionCompat21.replaceTargets(paramObject4, paramArrayList3, null);
        }
      }
    });
  }
  
  public static void setEpicenter(Object paramObject, Rect paramRect)
  {
    if (paramObject != null) {
      ((Transition)paramObject).setEpicenterCallback(new Transition.EpicenterCallback()
      {
        public Rect onGetEpicenter(Transition paramAnonymousTransition)
        {
          if ((this.val$epicenter == null) || (this.val$epicenter.isEmpty())) {}
          for (paramAnonymousTransition = null;; paramAnonymousTransition = this.val$epicenter) {
            return paramAnonymousTransition;
          }
        }
      });
    }
  }
  
  public static void setEpicenter(Object paramObject, View paramView)
  {
    if (paramView != null)
    {
      paramObject = (Transition)paramObject;
      Rect localRect = new Rect();
      getBoundsOnScreen(paramView, localRect);
      ((Transition)paramObject).setEpicenterCallback(new Transition.EpicenterCallback()
      {
        public Rect onGetEpicenter(Transition paramAnonymousTransition)
        {
          return this.val$epicenter;
        }
      });
    }
  }
  
  public static void setNameOverridesOptimized(View paramView, final ArrayList<View> paramArrayList1, final ArrayList<View> paramArrayList2, final ArrayList<String> paramArrayList, Map<String, String> paramMap)
  {
    int k = paramArrayList2.size();
    final ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i < k)
    {
      Object localObject = (View)paramArrayList1.get(i);
      String str = ((View)localObject).getTransitionName();
      localArrayList.add(str);
      if (str == null) {}
      label127:
      for (;;)
      {
        i++;
        break;
        ((View)localObject).setTransitionName(null);
        localObject = (String)paramMap.get(str);
        for (int j = 0;; j++)
        {
          if (j >= k) {
            break label127;
          }
          if (((String)localObject).equals(paramArrayList.get(j)))
          {
            ((View)paramArrayList2.get(j)).setTransitionName(str);
            break;
          }
        }
      }
    }
    OneShotPreDrawListener.add(paramView, new Runnable()
    {
      public void run()
      {
        for (int i = 0; i < this.val$numSharedElements; i++)
        {
          ((View)paramArrayList2.get(i)).setTransitionName((String)paramArrayList.get(i));
          ((View)paramArrayList1.get(i)).setTransitionName((String)localArrayList.get(i));
        }
      }
    });
  }
  
  public static void setNameOverridesUnoptimized(View paramView, ArrayList<View> paramArrayList, final Map<String, String> paramMap)
  {
    OneShotPreDrawListener.add(paramView, new Runnable()
    {
      public void run()
      {
        int j = this.val$sharedElementsIn.size();
        for (int i = 0; i < j; i++)
        {
          View localView = (View)this.val$sharedElementsIn.get(i);
          String str = localView.getTransitionName();
          if (str != null) {
            localView.setTransitionName(FragmentTransitionCompat21.findKeyForValue(paramMap, str));
          }
        }
      }
    });
  }
  
  public static void setSharedElementTargets(Object paramObject, View paramView, ArrayList<View> paramArrayList)
  {
    paramObject = (TransitionSet)paramObject;
    List localList = ((TransitionSet)paramObject).getTargets();
    localList.clear();
    int j = paramArrayList.size();
    for (int i = 0; i < j; i++) {
      bfsAddViewChildren(localList, (View)paramArrayList.get(i));
    }
    localList.add(paramView);
    paramArrayList.add(paramView);
    addTargets(paramObject, paramArrayList);
  }
  
  public static void swapSharedElementTargets(Object paramObject, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2)
  {
    paramObject = (TransitionSet)paramObject;
    if (paramObject != null)
    {
      ((TransitionSet)paramObject).getTargets().clear();
      ((TransitionSet)paramObject).getTargets().addAll(paramArrayList2);
      replaceTargets(paramObject, paramArrayList1, paramArrayList2);
    }
  }
  
  public static Object wrapTransitionInSet(Object paramObject)
  {
    if (paramObject == null) {}
    TransitionSet localTransitionSet;
    for (paramObject = null;; paramObject = localTransitionSet)
    {
      return paramObject;
      localTransitionSet = new TransitionSet();
      localTransitionSet.addTransition((Transition)paramObject);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\app\FragmentTransitionCompat21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */