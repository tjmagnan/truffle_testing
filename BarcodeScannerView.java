package me.dm7.barcodescanner.core;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public abstract class BarcodeScannerView
  extends FrameLayout
  implements Camera.PreviewCallback
{
  private boolean mAutofocusState = true;
  private float mBorderAlpha = 1.0F;
  @ColorInt
  private int mBorderColor = getResources().getColor(R.color.viewfinder_border);
  private int mBorderLength = getResources().getInteger(R.integer.viewfinder_border_length);
  private int mBorderWidth = getResources().getInteger(R.integer.viewfinder_border_width);
  private CameraHandlerThread mCameraHandlerThread;
  private CameraWrapper mCameraWrapper;
  private int mCornerRadius = 0;
  private Boolean mFlashState;
  private Rect mFramingRectInPreview;
  private boolean mIsLaserEnabled = true;
  @ColorInt
  private int mLaserColor = getResources().getColor(R.color.viewfinder_laser);
  private int mMaskColor = getResources().getColor(R.color.viewfinder_mask);
  private CameraPreview mPreview;
  private boolean mRoundedCorner = false;
  private boolean mShouldScaleToFill = true;
  private boolean mSquaredFinder = false;
  private int mViewFinderOffset = 0;
  private IViewFinder mViewFinderView;
  
  public BarcodeScannerView(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public BarcodeScannerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = paramContext.getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.BarcodeScannerView, 0, 0);
    try
    {
      setShouldScaleToFill(paramContext.getBoolean(R.styleable.BarcodeScannerView_shouldScaleToFill, true));
      this.mIsLaserEnabled = paramContext.getBoolean(R.styleable.BarcodeScannerView_laserEnabled, this.mIsLaserEnabled);
      this.mLaserColor = paramContext.getColor(R.styleable.BarcodeScannerView_laserColor, this.mLaserColor);
      this.mBorderColor = paramContext.getColor(R.styleable.BarcodeScannerView_borderColor, this.mBorderColor);
      this.mMaskColor = paramContext.getColor(R.styleable.BarcodeScannerView_maskColor, this.mMaskColor);
      this.mBorderWidth = paramContext.getDimensionPixelSize(R.styleable.BarcodeScannerView_borderWidth, this.mBorderWidth);
      this.mBorderLength = paramContext.getDimensionPixelSize(R.styleable.BarcodeScannerView_borderLength, this.mBorderLength);
      this.mRoundedCorner = paramContext.getBoolean(R.styleable.BarcodeScannerView_roundedCorner, this.mRoundedCorner);
      this.mCornerRadius = paramContext.getDimensionPixelSize(R.styleable.BarcodeScannerView_cornerRadius, this.mCornerRadius);
      this.mSquaredFinder = paramContext.getBoolean(R.styleable.BarcodeScannerView_squaredFinder, this.mSquaredFinder);
      this.mBorderAlpha = paramContext.getFloat(R.styleable.BarcodeScannerView_borderAlpha, this.mBorderAlpha);
      this.mViewFinderOffset = paramContext.getDimensionPixelSize(R.styleable.BarcodeScannerView_finderOffset, this.mViewFinderOffset);
      paramContext.recycle();
      init();
      return;
    }
    finally
    {
      paramContext.recycle();
    }
  }
  
  private void init()
  {
    this.mViewFinderView = createViewFinderView(getContext());
  }
  
  protected IViewFinder createViewFinderView(Context paramContext)
  {
    paramContext = new ViewFinderView(paramContext);
    paramContext.setBorderColor(this.mBorderColor);
    paramContext.setLaserColor(this.mLaserColor);
    paramContext.setLaserEnabled(this.mIsLaserEnabled);
    paramContext.setBorderStrokeWidth(this.mBorderWidth);
    paramContext.setBorderLineLength(this.mBorderLength);
    paramContext.setMaskColor(this.mMaskColor);
    paramContext.setBorderCornerRounded(this.mRoundedCorner);
    paramContext.setBorderCornerRadius(this.mCornerRadius);
    paramContext.setSquareViewFinder(this.mSquaredFinder);
    paramContext.setViewFinderOffset(this.mViewFinderOffset);
    return paramContext;
  }
  
  public boolean getFlash()
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (this.mCameraWrapper != null)
    {
      bool1 = bool2;
      if (CameraUtils.isFlashSupported(this.mCameraWrapper.mCamera))
      {
        bool1 = bool2;
        if (this.mCameraWrapper.mCamera.getParameters().getFlashMode().equals("torch")) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  /* Error */
  public Rect getFramingRectInPreview(int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 266	me/dm7/barcodescanner/core/BarcodeScannerView:mFramingRectInPreview	Landroid/graphics/Rect;
    //   6: ifnonnull +144 -> 150
    //   9: aload_0
    //   10: getfield 192	me/dm7/barcodescanner/core/BarcodeScannerView:mViewFinderView	Lme/dm7/barcodescanner/core/IViewFinder;
    //   13: invokeinterface 272 1 0
    //   18: astore 6
    //   20: aload_0
    //   21: getfield 192	me/dm7/barcodescanner/core/BarcodeScannerView:mViewFinderView	Lme/dm7/barcodescanner/core/IViewFinder;
    //   24: invokeinterface 276 1 0
    //   29: istore_3
    //   30: aload_0
    //   31: getfield 192	me/dm7/barcodescanner/core/BarcodeScannerView:mViewFinderView	Lme/dm7/barcodescanner/core/IViewFinder;
    //   34: invokeinterface 279 1 0
    //   39: istore 4
    //   41: aload 6
    //   43: ifnull +12 -> 55
    //   46: iload_3
    //   47: ifeq +8 -> 55
    //   50: iload 4
    //   52: ifne +11 -> 63
    //   55: aconst_null
    //   56: astore 5
    //   58: aload_0
    //   59: monitorexit
    //   60: aload 5
    //   62: areturn
    //   63: new 281	android/graphics/Rect
    //   66: astore 5
    //   68: aload 5
    //   70: aload 6
    //   72: invokespecial 284	android/graphics/Rect:<init>	(Landroid/graphics/Rect;)V
    //   75: iload_1
    //   76: iload_3
    //   77: if_icmpge +31 -> 108
    //   80: aload 5
    //   82: aload 5
    //   84: getfield 287	android/graphics/Rect:left	I
    //   87: iload_1
    //   88: imul
    //   89: iload_3
    //   90: idiv
    //   91: putfield 287	android/graphics/Rect:left	I
    //   94: aload 5
    //   96: aload 5
    //   98: getfield 290	android/graphics/Rect:right	I
    //   101: iload_1
    //   102: imul
    //   103: iload_3
    //   104: idiv
    //   105: putfield 290	android/graphics/Rect:right	I
    //   108: iload_2
    //   109: iload 4
    //   111: if_icmpge +33 -> 144
    //   114: aload 5
    //   116: aload 5
    //   118: getfield 293	android/graphics/Rect:top	I
    //   121: iload_2
    //   122: imul
    //   123: iload 4
    //   125: idiv
    //   126: putfield 293	android/graphics/Rect:top	I
    //   129: aload 5
    //   131: aload 5
    //   133: getfield 296	android/graphics/Rect:bottom	I
    //   136: iload_2
    //   137: imul
    //   138: iload 4
    //   140: idiv
    //   141: putfield 296	android/graphics/Rect:bottom	I
    //   144: aload_0
    //   145: aload 5
    //   147: putfield 266	me/dm7/barcodescanner/core/BarcodeScannerView:mFramingRectInPreview	Landroid/graphics/Rect;
    //   150: aload_0
    //   151: getfield 266	me/dm7/barcodescanner/core/BarcodeScannerView:mFramingRectInPreview	Landroid/graphics/Rect;
    //   154: astore 5
    //   156: goto -98 -> 58
    //   159: astore 5
    //   161: aload_0
    //   162: monitorexit
    //   163: aload 5
    //   165: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	166	0	this	BarcodeScannerView
    //   0	166	1	paramInt1	int
    //   0	166	2	paramInt2	int
    //   29	76	3	i	int
    //   39	102	4	j	int
    //   56	99	5	localRect1	Rect
    //   159	5	5	localObject	Object
    //   18	53	6	localRect2	Rect
    // Exception table:
    //   from	to	target	type
    //   2	41	159	finally
    //   63	75	159	finally
    //   80	108	159	finally
    //   114	144	159	finally
    //   144	150	159	finally
    //   150	156	159	finally
  }
  
  protected void resumeCameraPreview()
  {
    if (this.mPreview != null) {
      this.mPreview.showCameraPreview();
    }
  }
  
  public void setAutoFocus(boolean paramBoolean)
  {
    this.mAutofocusState = paramBoolean;
    if (this.mPreview != null) {
      this.mPreview.setAutoFocus(paramBoolean);
    }
  }
  
  public void setBorderAlpha(float paramFloat)
  {
    this.mBorderAlpha = paramFloat;
    this.mViewFinderView.setBorderAlpha(this.mBorderAlpha);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setBorderColor(int paramInt)
  {
    this.mBorderColor = paramInt;
    this.mViewFinderView.setBorderColor(this.mBorderColor);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setBorderCornerRadius(int paramInt)
  {
    this.mCornerRadius = paramInt;
    this.mViewFinderView.setBorderCornerRadius(this.mCornerRadius);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setBorderLineLength(int paramInt)
  {
    this.mBorderLength = paramInt;
    this.mViewFinderView.setBorderLineLength(this.mBorderLength);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setBorderStrokeWidth(int paramInt)
  {
    this.mBorderWidth = paramInt;
    this.mViewFinderView.setBorderStrokeWidth(this.mBorderWidth);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setFlash(boolean paramBoolean)
  {
    this.mFlashState = Boolean.valueOf(paramBoolean);
    Camera.Parameters localParameters;
    if ((this.mCameraWrapper != null) && (CameraUtils.isFlashSupported(this.mCameraWrapper.mCamera)))
    {
      localParameters = this.mCameraWrapper.mCamera.getParameters();
      if (!paramBoolean) {
        break label78;
      }
      if (!localParameters.getFlashMode().equals("torch")) {}
    }
    else
    {
      return;
    }
    localParameters.setFlashMode("torch");
    for (;;)
    {
      this.mCameraWrapper.mCamera.setParameters(localParameters);
      break;
      label78:
      if (localParameters.getFlashMode().equals("off")) {
        break;
      }
      localParameters.setFlashMode("off");
    }
  }
  
  public void setIsBorderCornerRounded(boolean paramBoolean)
  {
    this.mRoundedCorner = paramBoolean;
    this.mViewFinderView.setBorderCornerRounded(this.mRoundedCorner);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setLaserColor(int paramInt)
  {
    this.mLaserColor = paramInt;
    this.mViewFinderView.setLaserColor(this.mLaserColor);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setLaserEnabled(boolean paramBoolean)
  {
    this.mIsLaserEnabled = paramBoolean;
    this.mViewFinderView.setLaserEnabled(this.mIsLaserEnabled);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setMaskColor(int paramInt)
  {
    this.mMaskColor = paramInt;
    this.mViewFinderView.setMaskColor(this.mMaskColor);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setShouldScaleToFill(boolean paramBoolean)
  {
    this.mShouldScaleToFill = paramBoolean;
  }
  
  public void setSquareViewFinder(boolean paramBoolean)
  {
    this.mSquaredFinder = paramBoolean;
    this.mViewFinderView.setSquareViewFinder(this.mSquaredFinder);
    this.mViewFinderView.setupViewFinder();
  }
  
  public void setupCameraPreview(CameraWrapper paramCameraWrapper)
  {
    this.mCameraWrapper = paramCameraWrapper;
    if (this.mCameraWrapper != null)
    {
      setupLayout(this.mCameraWrapper);
      this.mViewFinderView.setupViewFinder();
      if (this.mFlashState != null) {
        setFlash(this.mFlashState.booleanValue());
      }
      setAutoFocus(this.mAutofocusState);
    }
  }
  
  public final void setupLayout(CameraWrapper paramCameraWrapper)
  {
    removeAllViews();
    this.mPreview = new CameraPreview(getContext(), paramCameraWrapper, this);
    this.mPreview.setShouldScaleToFill(this.mShouldScaleToFill);
    if (!this.mShouldScaleToFill)
    {
      paramCameraWrapper = new RelativeLayout(getContext());
      paramCameraWrapper.setGravity(17);
      paramCameraWrapper.setBackgroundColor(-16777216);
      paramCameraWrapper.addView(this.mPreview);
      addView(paramCameraWrapper);
    }
    while ((this.mViewFinderView instanceof View))
    {
      addView((View)this.mViewFinderView);
      return;
      addView(this.mPreview);
    }
    throw new IllegalArgumentException("IViewFinder object returned by 'createViewFinderView()' should be instance of android.view.View");
  }
  
  public void startCamera()
  {
    startCamera(CameraUtils.getDefaultCameraId());
  }
  
  public void startCamera(int paramInt)
  {
    if (this.mCameraHandlerThread == null) {
      this.mCameraHandlerThread = new CameraHandlerThread(this);
    }
    this.mCameraHandlerThread.startCamera(paramInt);
  }
  
  public void stopCamera()
  {
    if (this.mCameraWrapper != null)
    {
      this.mPreview.stopCameraPreview();
      this.mPreview.setCamera(null, null);
      this.mCameraWrapper.mCamera.release();
      this.mCameraWrapper = null;
    }
    if (this.mCameraHandlerThread != null)
    {
      this.mCameraHandlerThread.quit();
      this.mCameraHandlerThread = null;
    }
  }
  
  public void stopCameraPreview()
  {
    if (this.mPreview != null) {
      this.mPreview.stopCameraPreview();
    }
  }
  
  public void toggleFlash()
  {
    Camera.Parameters localParameters;
    if ((this.mCameraWrapper != null) && (CameraUtils.isFlashSupported(this.mCameraWrapper.mCamera)))
    {
      localParameters = this.mCameraWrapper.mCamera.getParameters();
      if (!localParameters.getFlashMode().equals("torch")) {
        break label63;
      }
      localParameters.setFlashMode("off");
    }
    for (;;)
    {
      this.mCameraWrapper.mCamera.setParameters(localParameters);
      return;
      label63:
      localParameters.setFlashMode("torch");
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\BarcodeScannerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */