package tech.dcube.companion.fragments;

import android.support.annotation.UiThread;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;

public class ShipItemDetailFragment_ViewBinding
  extends PBTopBarFragment_ViewBinding
{
  private ShipItemDetailFragment target;
  private View view2131624182;
  
  @UiThread
  public ShipItemDetailFragment_ViewBinding(final ShipItemDetailFragment paramShipItemDetailFragment, View paramView)
  {
    super(paramShipItemDetailFragment, paramView);
    this.target = paramShipItemDetailFragment;
    paramView = Utils.findRequiredView(paramView, 2131624182, "method 'hideSoftKeyboardOnClick' and method 'hideSoftKeyboardOnFocus'");
    this.view2131624182 = paramView;
    paramView.setOnClickListener(new DebouncingOnClickListener()
    {
      public void doClick(View paramAnonymousView)
      {
        paramShipItemDetailFragment.hideSoftKeyboardOnClick();
      }
    });
    paramView.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
      public void onFocusChange(View paramAnonymousView, boolean paramAnonymousBoolean)
      {
        paramShipItemDetailFragment.hideSoftKeyboardOnFocus(paramAnonymousBoolean);
      }
    });
  }
  
  public void unbind()
  {
    if (this.target == null) {
      throw new IllegalStateException("Bindings already cleared.");
    }
    this.target = null;
    this.view2131624182.setOnClickListener(null);
    this.view2131624182.setOnFocusChangeListener(null);
    this.view2131624182 = null;
    super.unbind();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\ShipItemDetailFragment_ViewBinding.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */