package org.joda.time.chrono;

import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeUtils;
import org.joda.time.DurationField;
import org.joda.time.ReadablePartial;
import org.joda.time.field.FieldUtils;
import org.joda.time.field.ImpreciseDateTimeField;

class BasicMonthOfYearDateTimeField
  extends ImpreciseDateTimeField
{
  private static final int MIN = 1;
  private static final long serialVersionUID = -8258715387168736L;
  private final BasicChronology iChronology;
  private final int iLeapMonth;
  private final int iMax;
  
  BasicMonthOfYearDateTimeField(BasicChronology paramBasicChronology, int paramInt)
  {
    super(DateTimeFieldType.monthOfYear(), paramBasicChronology.getAverageMillisPerMonth());
    this.iChronology = paramBasicChronology;
    this.iMax = this.iChronology.getMaxMonth();
    this.iLeapMonth = paramInt;
  }
  
  private Object readResolve()
  {
    return this.iChronology.monthOfYear();
  }
  
  public long add(long paramLong, int paramInt)
  {
    if (paramInt == 0) {
      return paramLong;
    }
    long l = this.iChronology.getMillisOfDay(paramLong);
    int j = this.iChronology.getYear(paramLong);
    int n = this.iChronology.getMonthOfYear(paramLong, j);
    int k = n - 1 + paramInt;
    int i;
    if ((n > 0) && (k < 0)) {
      if (Math.signum(this.iMax + paramInt) == Math.signum(paramInt))
      {
        k = j - 1;
        i = this.iMax + paramInt;
        paramInt = k;
        label93:
        k = i + (n - 1);
        i = paramInt;
      }
    }
    for (paramInt = k;; paramInt = k)
    {
      if (paramInt >= 0)
      {
        i += paramInt / this.iMax;
        k = paramInt % this.iMax + 1;
        paramInt = i;
        i = k;
        label140:
        j = this.iChronology.getDayOfMonth(paramLong, j, n);
        k = this.iChronology.getDaysInYearMonth(paramInt, i);
        if (j <= k) {
          break label288;
        }
        j = k;
      }
      label288:
      for (;;)
      {
        paramLong = this.iChronology.getYearMonthDayMillis(paramInt, i, j) + l;
        break;
        k = j + 1;
        i = paramInt - this.iMax;
        paramInt = k;
        break label93;
        k = i + paramInt / this.iMax - 1;
        i = Math.abs(paramInt) % this.iMax;
        paramInt = i;
        if (i == 0) {
          paramInt = this.iMax;
        }
        int m = this.iMax - paramInt + 1;
        i = m;
        paramInt = k;
        if (m != 1) {
          break label140;
        }
        paramInt = k + 1;
        i = m;
        break label140;
      }
      i = j;
    }
  }
  
  public long add(long paramLong1, long paramLong2)
  {
    int i = (int)paramLong2;
    if (i == paramLong2)
    {
      paramLong1 = add(paramLong1, i);
      return paramLong1;
    }
    long l5 = this.iChronology.getMillisOfDay(paramLong1);
    int i1 = this.iChronology.getYear(paramLong1);
    int n = this.iChronology.getMonthOfYear(paramLong1, i1);
    long l2 = n - 1 + paramLong2;
    long l1;
    if (l2 >= 0L)
    {
      l1 = i1 + l2 / this.iMax;
      l2 = l2 % this.iMax + 1L;
    }
    while ((l1 < this.iChronology.getMinYear()) || (l1 > this.iChronology.getMaxYear()))
    {
      throw new IllegalArgumentException("Magnitude of add amount is too large: " + paramLong2);
      long l3 = i1 + l2 / this.iMax - 1L;
      j = (int)(Math.abs(l2) % this.iMax);
      i = j;
      if (j == 0) {
        i = this.iMax;
      }
      long l4 = this.iMax - i + 1;
      l2 = l4;
      l1 = l3;
      if (l4 == 1L)
      {
        l1 = l3 + 1L;
        l2 = l4;
      }
    }
    int k = (int)l1;
    int m = (int)l2;
    int j = this.iChronology.getDayOfMonth(paramLong1, i1, n);
    i = this.iChronology.getDaysInYearMonth(k, m);
    if (j > i) {}
    for (;;)
    {
      paramLong1 = this.iChronology.getYearMonthDayMillis(k, m, i) + l5;
      break;
      i = j;
    }
  }
  
  public int[] add(ReadablePartial paramReadablePartial, int paramInt1, int[] paramArrayOfInt, int paramInt2)
  {
    int i = 0;
    if (paramInt2 == 0) {}
    for (;;)
    {
      return paramArrayOfInt;
      if ((paramReadablePartial.size() > 0) && (paramReadablePartial.getFieldType(0).equals(DateTimeFieldType.monthOfYear())) && (paramInt1 == 0))
      {
        paramArrayOfInt = set(paramReadablePartial, 0, paramArrayOfInt, (paramArrayOfInt[0] - 1 + paramInt2 % 12 + 12) % 12 + 1);
      }
      else if (DateTimeUtils.isContiguous(paramReadablePartial))
      {
        long l = 0L;
        int j = paramReadablePartial.size();
        for (paramInt1 = i; paramInt1 < j; paramInt1++) {
          l = paramReadablePartial.getFieldType(paramInt1).getField(this.iChronology).set(l, paramArrayOfInt[paramInt1]);
        }
        l = add(l, paramInt2);
        paramArrayOfInt = this.iChronology.get(paramReadablePartial, l);
      }
      else
      {
        paramArrayOfInt = super.add(paramReadablePartial, paramInt1, paramArrayOfInt, paramInt2);
      }
    }
  }
  
  public long addWrapField(long paramLong, int paramInt)
  {
    return set(paramLong, FieldUtils.getWrappedValue(get(paramLong), paramInt, 1, this.iMax));
  }
  
  public int get(long paramLong)
  {
    return this.iChronology.getMonthOfYear(paramLong);
  }
  
  public long getDifferenceAsLong(long paramLong1, long paramLong2)
  {
    if (paramLong1 < paramLong2) {
      paramLong2 = -getDifference(paramLong2, paramLong1);
    }
    for (;;)
    {
      return paramLong2;
      int i = this.iChronology.getYear(paramLong1);
      int j = this.iChronology.getMonthOfYear(paramLong1, i);
      int n = this.iChronology.getYear(paramLong2);
      int k = this.iChronology.getMonthOfYear(paramLong2, n);
      long l2 = (i - n) * this.iMax + j - k;
      int m = this.iChronology.getDayOfMonth(paramLong1, i, j);
      long l1 = paramLong2;
      if (m == this.iChronology.getDaysInYearMonth(i, j))
      {
        l1 = paramLong2;
        if (this.iChronology.getDayOfMonth(paramLong2, n, k) > m) {
          l1 = this.iChronology.dayOfMonth().set(paramLong2, m);
        }
      }
      paramLong2 = l2;
      if (paramLong1 - this.iChronology.getYearMonthMillis(i, j) < l1 - this.iChronology.getYearMonthMillis(n, k)) {
        paramLong2 = l2 - 1L;
      }
    }
  }
  
  public int getLeapAmount(long paramLong)
  {
    if (isLeap(paramLong)) {}
    for (int i = 1;; i = 0) {
      return i;
    }
  }
  
  public DurationField getLeapDurationField()
  {
    return this.iChronology.days();
  }
  
  public int getMaximumValue()
  {
    return this.iMax;
  }
  
  public int getMinimumValue()
  {
    return 1;
  }
  
  public DurationField getRangeDurationField()
  {
    return this.iChronology.years();
  }
  
  public boolean isLeap(long paramLong)
  {
    boolean bool2 = false;
    int i = this.iChronology.getYear(paramLong);
    boolean bool1 = bool2;
    if (this.iChronology.isLeapYear(i))
    {
      bool1 = bool2;
      if (this.iChronology.getMonthOfYear(paramLong, i) == this.iLeapMonth) {
        bool1 = true;
      }
    }
    return bool1;
  }
  
  public boolean isLenient()
  {
    return false;
  }
  
  public long remainder(long paramLong)
  {
    return paramLong - roundFloor(paramLong);
  }
  
  public long roundFloor(long paramLong)
  {
    int j = this.iChronology.getYear(paramLong);
    int i = this.iChronology.getMonthOfYear(paramLong, j);
    return this.iChronology.getYearMonthMillis(j, i);
  }
  
  public long set(long paramLong, int paramInt)
  {
    FieldUtils.verifyValueBounds(this, paramInt, 1, this.iMax);
    int k = this.iChronology.getYear(paramLong);
    int i = this.iChronology.getDayOfMonth(paramLong, k);
    int j = this.iChronology.getDaysInYearMonth(k, paramInt);
    if (i > j) {
      i = j;
    }
    for (;;)
    {
      return this.iChronology.getYearMonthDayMillis(k, paramInt, i) + this.iChronology.getMillisOfDay(paramLong);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\chrono\BasicMonthOfYearDateTimeField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */