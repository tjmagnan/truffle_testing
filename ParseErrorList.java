package org.jsoup.parser;

import java.util.ArrayList;

public class ParseErrorList
  extends ArrayList<ParseError>
{
  private static final int INITIAL_CAPACITY = 16;
  private final int maxSize;
  
  ParseErrorList(int paramInt1, int paramInt2)
  {
    super(paramInt1);
    this.maxSize = paramInt2;
  }
  
  public static ParseErrorList noTracking()
  {
    return new ParseErrorList(0, 0);
  }
  
  public static ParseErrorList tracking(int paramInt)
  {
    return new ParseErrorList(16, paramInt);
  }
  
  boolean canAddError()
  {
    if (size() < this.maxSize) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  int getMaxSize()
  {
    return this.maxSize;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\jsoup\parser\ParseErrorList.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */