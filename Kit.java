package io.fabric.sdk.android;

import android.content.Context;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.DependsOn;
import io.fabric.sdk.android.services.concurrency.Task;
import java.io.File;
import java.util.Collection;

public abstract class Kit<Result>
  implements Comparable<Kit>
{
  Context context;
  final DependsOn dependsOnAnnotation = (DependsOn)getClass().getAnnotation(DependsOn.class);
  Fabric fabric;
  IdManager idManager;
  InitializationCallback<Result> initializationCallback;
  InitializationTask<Result> initializationTask = new InitializationTask(this);
  
  public int compareTo(Kit paramKit)
  {
    int i = 1;
    if (containsAnnotatedDependency(paramKit)) {}
    for (;;)
    {
      return i;
      if (paramKit.containsAnnotatedDependency(this)) {
        i = -1;
      } else if ((!hasAnnotatedDependency()) || (paramKit.hasAnnotatedDependency())) {
        if ((!hasAnnotatedDependency()) && (paramKit.hasAnnotatedDependency())) {
          i = -1;
        } else {
          i = 0;
        }
      }
    }
  }
  
  boolean containsAnnotatedDependency(Kit paramKit)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    Class[] arrayOfClass;
    int j;
    if (hasAnnotatedDependency())
    {
      arrayOfClass = this.dependsOnAnnotation.value();
      j = arrayOfClass.length;
    }
    for (int i = 0;; i++)
    {
      bool1 = bool2;
      if (i < j)
      {
        if (arrayOfClass[i].isAssignableFrom(paramKit.getClass())) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  protected abstract Result doInBackground();
  
  public Context getContext()
  {
    return this.context;
  }
  
  protected Collection<Task> getDependencies()
  {
    return this.initializationTask.getDependencies();
  }
  
  public Fabric getFabric()
  {
    return this.fabric;
  }
  
  protected IdManager getIdManager()
  {
    return this.idManager;
  }
  
  public abstract String getIdentifier();
  
  public String getPath()
  {
    return ".Fabric" + File.separator + getIdentifier();
  }
  
  public abstract String getVersion();
  
  boolean hasAnnotatedDependency()
  {
    if (this.dependsOnAnnotation != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  final void initialize()
  {
    this.initializationTask.executeOnExecutor(this.fabric.getExecutorService(), new Void[] { (Void)null });
  }
  
  void injectParameters(Context paramContext, Fabric paramFabric, InitializationCallback<Result> paramInitializationCallback, IdManager paramIdManager)
  {
    this.fabric = paramFabric;
    this.context = new FabricContext(paramContext, getIdentifier(), getPath());
    this.initializationCallback = paramInitializationCallback;
    this.idManager = paramIdManager;
  }
  
  protected void onCancelled(Result paramResult) {}
  
  protected void onPostExecute(Result paramResult) {}
  
  protected boolean onPreExecute()
  {
    return true;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\Kit.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */