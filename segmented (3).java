package info.hoang8f.android.segmented;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.LayoutParams;

public class SegmentedGroup
  extends RadioGroup
{
  private int mCheckedTextColor = -1;
  private Float mCornerRadius = Float.valueOf(getResources().getDimension(R.dimen.radio_button_conner_radius));
  private LayoutSelector mLayoutSelector;
  private int mMarginDp = (int)getResources().getDimension(R.dimen.radio_button_stroke_border);
  private int mTintColor = this.resources.getColor(R.color.radio_button_selected_color);
  private Resources resources = getResources();
  
  public SegmentedGroup(Context paramContext)
  {
    super(paramContext);
    this.mLayoutSelector = new LayoutSelector(this.mCornerRadius.floatValue());
  }
  
  public SegmentedGroup(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initAttrs(paramAttributeSet);
    this.mLayoutSelector = new LayoutSelector(this.mCornerRadius.floatValue());
  }
  
  private void initAttrs(AttributeSet paramAttributeSet)
  {
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.SegmentedGroup, 0, 0);
    try
    {
      this.mMarginDp = ((int)localTypedArray.getDimension(R.styleable.SegmentedGroup_sc_border_width, getResources().getDimension(R.dimen.radio_button_stroke_border)));
      this.mCornerRadius = Float.valueOf(localTypedArray.getDimension(R.styleable.SegmentedGroup_sc_corner_radius, getResources().getDimension(R.dimen.radio_button_conner_radius)));
      this.mTintColor = localTypedArray.getColor(R.styleable.SegmentedGroup_sc_tint_color, getResources().getColor(R.color.radio_button_selected_color));
      this.mCheckedTextColor = localTypedArray.getColor(R.styleable.SegmentedGroup_sc_checked_text_color, getResources().getColor(17170443));
      return;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  private void updateBackground(View paramView)
  {
    int i = this.mLayoutSelector.getSelected();
    int m = this.mLayoutSelector.getUnselected();
    int j = this.mTintColor;
    int k = this.mCheckedTextColor;
    Object localObject = new ColorStateList(new int[][] { { 16842919 }, { -16842919, -16842912 }, { -16842919, 16842912 } }, new int[] { -7829368, j, k });
    ((Button)paramView).setTextColor((ColorStateList)localObject);
    Drawable localDrawable = this.resources.getDrawable(i).mutate();
    localObject = this.resources.getDrawable(m).mutate();
    ((GradientDrawable)localDrawable).setColor(this.mTintColor);
    ((GradientDrawable)localDrawable).setStroke(this.mMarginDp, this.mTintColor);
    ((GradientDrawable)localObject).setStroke(this.mMarginDp, this.mTintColor);
    ((GradientDrawable)localDrawable).setCornerRadii(this.mLayoutSelector.getChildRadii(paramView));
    ((GradientDrawable)localObject).setCornerRadii(this.mLayoutSelector.getChildRadii(paramView));
    StateListDrawable localStateListDrawable = new StateListDrawable();
    localStateListDrawable.addState(new int[] { -16842912 }, (Drawable)localObject);
    localStateListDrawable.addState(new int[] { 16842912 }, localDrawable);
    if (Build.VERSION.SDK_INT >= 16) {
      paramView.setBackground(localStateListDrawable);
    }
    for (;;)
    {
      return;
      paramView.setBackgroundDrawable(localStateListDrawable);
    }
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    updateBackground();
  }
  
  public void setTintColor(int paramInt)
  {
    this.mTintColor = paramInt;
    updateBackground();
  }
  
  public void setTintColor(int paramInt1, int paramInt2)
  {
    this.mTintColor = paramInt1;
    this.mCheckedTextColor = paramInt2;
    updateBackground();
  }
  
  public void updateBackground()
  {
    int j = super.getChildCount();
    int i = 0;
    View localView;
    if (i < j)
    {
      localView = getChildAt(i);
      updateBackground(localView);
      if (i != j - 1) {}
    }
    else
    {
      return;
    }
    RadioGroup.LayoutParams localLayoutParams = (RadioGroup.LayoutParams)localView.getLayoutParams();
    localLayoutParams = new RadioGroup.LayoutParams(localLayoutParams.width, localLayoutParams.height, localLayoutParams.weight);
    if (getOrientation() == 0) {
      localLayoutParams.setMargins(0, 0, -this.mMarginDp, 0);
    }
    for (;;)
    {
      localView.setLayoutParams(localLayoutParams);
      i++;
      break;
      localLayoutParams.setMargins(0, 0, 0, -this.mMarginDp);
    }
  }
  
  private class LayoutSelector
  {
    private final int SELECTED_LAYOUT = R.drawable.radio_checked;
    private final int UNSELECTED_LAYOUT = R.drawable.radio_unchecked;
    private int child = -1;
    private int children = -1;
    private float r;
    private final float r1 = TypedValue.applyDimension(1, 0.1F, SegmentedGroup.this.getResources().getDisplayMetrics());
    private final float[] rBot;
    private final float[] rDefault;
    private final float[] rLeft;
    private final float[] rMiddle;
    private final float[] rRight;
    private final float[] rTop;
    private float[] radii;
    
    public LayoutSelector(float paramFloat)
    {
      this.r = paramFloat;
      this.rLeft = new float[] { this.r, this.r, this.r1, this.r1, this.r1, this.r1, this.r, this.r };
      this.rRight = new float[] { this.r1, this.r1, this.r, this.r, this.r, this.r, this.r1, this.r1 };
      this.rMiddle = new float[] { this.r1, this.r1, this.r1, this.r1, this.r1, this.r1, this.r1, this.r1 };
      this.rDefault = new float[] { this.r, this.r, this.r, this.r, this.r, this.r, this.r, this.r };
      this.rTop = new float[] { this.r, this.r, this.r, this.r, this.r1, this.r1, this.r1, this.r1 };
      this.rBot = new float[] { this.r1, this.r1, this.r1, this.r1, this.r, this.r, this.r, this.r };
    }
    
    private int getChildIndex(View paramView)
    {
      return SegmentedGroup.this.indexOfChild(paramView);
    }
    
    private int getChildren()
    {
      return SegmentedGroup.this.getChildCount();
    }
    
    private void setChildRadii(int paramInt1, int paramInt2)
    {
      if ((this.children == paramInt1) && (this.child == paramInt2)) {}
      for (;;)
      {
        return;
        this.children = paramInt1;
        this.child = paramInt2;
        if (this.children == 1)
        {
          this.radii = this.rDefault;
        }
        else
        {
          float[] arrayOfFloat;
          if (this.child == 0)
          {
            if (SegmentedGroup.this.getOrientation() == 0) {}
            for (arrayOfFloat = this.rLeft;; arrayOfFloat = this.rTop)
            {
              this.radii = arrayOfFloat;
              break;
            }
          }
          if (this.child == this.children - 1)
          {
            if (SegmentedGroup.this.getOrientation() == 0) {}
            for (arrayOfFloat = this.rRight;; arrayOfFloat = this.rBot)
            {
              this.radii = arrayOfFloat;
              break;
            }
          }
          this.radii = this.rMiddle;
        }
      }
    }
    
    public float[] getChildRadii(View paramView)
    {
      setChildRadii(getChildren(), getChildIndex(paramView));
      return this.radii;
    }
    
    public int getSelected()
    {
      return this.SELECTED_LAYOUT;
    }
    
    public int getUnselected()
    {
      return this.UNSELECTED_LAYOUT;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\info\hoang8f\android\segmented\SegmentedGroup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */