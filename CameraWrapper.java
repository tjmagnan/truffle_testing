package me.dm7.barcodescanner.core;

import android.hardware.Camera;
import android.support.annotation.NonNull;

public class CameraWrapper
{
  public final Camera mCamera;
  public final int mCameraId;
  
  private CameraWrapper(@NonNull Camera paramCamera, int paramInt)
  {
    if (paramCamera == null) {
      throw new NullPointerException("Camera cannot be null");
    }
    this.mCamera = paramCamera;
    this.mCameraId = paramInt;
  }
  
  public static CameraWrapper getWrapper(Camera paramCamera, int paramInt)
  {
    if (paramCamera == null) {}
    for (paramCamera = null;; paramCamera = new CameraWrapper(paramCamera, paramInt)) {
      return paramCamera;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\CameraWrapper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */