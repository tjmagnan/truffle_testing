package tech.dcube.companion.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PBShipmentPrintLabel
  extends PBObject
  implements Serializable
{
  private static final long serialVersionUID = 4805667501849246305L;
  @Expose
  @SerializedName("baseCharge")
  private Double baseCharge;
  @Expose
  @SerializedName("carrierCode")
  private String carrierCode;
  @Expose
  @SerializedName("chargesSpecialServices")
  private List<PBSpecialServiceCharges> chargesSpecialServices = new ArrayList();
  @Expose
  @SerializedName("correlationId")
  private String correlationId;
  @Expose
  @SerializedName("deliveryExpectations")
  private PBDeliveryExpectations deliveryExpectations;
  @Expose
  @SerializedName("discounts")
  private Object discounts;
  @Expose
  @SerializedName("labelUrl")
  private String labelUrl;
  @Expose
  @SerializedName("meterInfo")
  private Object meterInfo;
  @Expose
  @SerializedName("packageChargeCurrency")
  private String packageChargeCurrency;
  @Expose
  @SerializedName("packageTrackingId")
  private String packageTrackingId;
  @Expose
  @SerializedName("prepayBalance")
  private Double prepayBalance;
  @Expose
  @SerializedName("serviceCost")
  private Double serviceCost;
  @Expose
  @SerializedName("shipmentId")
  private String shipmentId;
  @Expose
  @SerializedName("size")
  private Object size;
  @Expose
  @SerializedName("surcharges")
  private Object surcharges;
  @Expose
  @SerializedName("taxes")
  private Object taxes;
  @Expose
  @SerializedName("totalPackageCharge")
  private Double totalPackageCharge;
  @Expose
  @SerializedName("totalTaxAmount")
  private Object totalTaxAmount;
  @Expose
  @SerializedName("transactionId")
  private String transactionId;
  
  public PBShipmentPrintLabel() {}
  
  public PBShipmentPrintLabel(Double paramDouble1, String paramString1, List<PBSpecialServiceCharges> paramList, String paramString2, PBDeliveryExpectations paramPBDeliveryExpectations, Object paramObject1, String paramString3, Object paramObject2, String paramString4, String paramString5, Double paramDouble2, Double paramDouble3, String paramString6, Object paramObject3, Object paramObject4, Object paramObject5, Double paramDouble4, Object paramObject6, String paramString7)
  {
    this.baseCharge = paramDouble1;
    this.carrierCode = paramString1;
    this.chargesSpecialServices = paramList;
    this.correlationId = paramString2;
    this.deliveryExpectations = paramPBDeliveryExpectations;
    this.discounts = paramObject1;
    this.labelUrl = paramString3;
    this.meterInfo = paramObject2;
    this.packageChargeCurrency = paramString4;
    this.packageTrackingId = paramString5;
    this.prepayBalance = paramDouble2;
    this.serviceCost = paramDouble3;
    this.shipmentId = paramString6;
    this.size = paramObject3;
    this.surcharges = paramObject4;
    this.taxes = paramObject5;
    this.totalPackageCharge = paramDouble4;
    this.totalTaxAmount = paramObject6;
    this.transactionId = paramString7;
  }
  
  public Double getBaseCharge()
  {
    return this.baseCharge;
  }
  
  public String getCarrierCode()
  {
    return this.carrierCode;
  }
  
  public List<PBSpecialServiceCharges> getChargesSpecialServices()
  {
    return this.chargesSpecialServices;
  }
  
  public String getCorrelationId()
  {
    return this.correlationId;
  }
  
  public PBDeliveryExpectations getDeliveryExpectations()
  {
    return this.deliveryExpectations;
  }
  
  public Object getDiscounts()
  {
    return this.discounts;
  }
  
  public String getLabelUrl()
  {
    return this.labelUrl;
  }
  
  public Object getMeterInfo()
  {
    return this.meterInfo;
  }
  
  public String getPackageChargeCurrency()
  {
    return this.packageChargeCurrency;
  }
  
  public String getPackageTrackingId()
  {
    return this.packageTrackingId;
  }
  
  public Double getPrepayBalance()
  {
    return this.prepayBalance;
  }
  
  public Double getServiceCost()
  {
    return this.serviceCost;
  }
  
  public String getShipmentId()
  {
    return this.shipmentId;
  }
  
  public Object getSize()
  {
    return this.size;
  }
  
  public Object getSurcharges()
  {
    return this.surcharges;
  }
  
  public Object getTaxes()
  {
    return this.taxes;
  }
  
  public Double getTotalPackageCharge()
  {
    return this.totalPackageCharge;
  }
  
  public Object getTotalTaxAmount()
  {
    return this.totalTaxAmount;
  }
  
  public String getTransactionId()
  {
    return this.transactionId;
  }
  
  public void setBaseCharge(Double paramDouble)
  {
    this.baseCharge = paramDouble;
  }
  
  public void setCarrierCode(String paramString)
  {
    this.carrierCode = paramString;
  }
  
  public void setChargesSpecialServices(List<PBSpecialServiceCharges> paramList)
  {
    this.chargesSpecialServices = paramList;
  }
  
  public void setCorrelationId(String paramString)
  {
    this.correlationId = paramString;
  }
  
  public void setDeliveryExpectations(PBDeliveryExpectations paramPBDeliveryExpectations)
  {
    this.deliveryExpectations = paramPBDeliveryExpectations;
  }
  
  public void setDiscounts(Object paramObject)
  {
    this.discounts = paramObject;
  }
  
  public void setLabelUrl(String paramString)
  {
    this.labelUrl = paramString;
  }
  
  public void setMeterInfo(Object paramObject)
  {
    this.meterInfo = paramObject;
  }
  
  public void setPackageChargeCurrency(String paramString)
  {
    this.packageChargeCurrency = paramString;
  }
  
  public void setPackageTrackingId(String paramString)
  {
    this.packageTrackingId = paramString;
  }
  
  public void setPrepayBalance(Double paramDouble)
  {
    this.prepayBalance = paramDouble;
  }
  
  public void setServiceCost(Double paramDouble)
  {
    this.serviceCost = paramDouble;
  }
  
  public void setShipmentId(String paramString)
  {
    this.shipmentId = paramString;
  }
  
  public void setSize(Object paramObject)
  {
    this.size = paramObject;
  }
  
  public void setSurcharges(Object paramObject)
  {
    this.surcharges = paramObject;
  }
  
  public void setTaxes(Object paramObject)
  {
    this.taxes = paramObject;
  }
  
  public void setTotalPackageCharge(Double paramDouble)
  {
    this.totalPackageCharge = paramDouble;
  }
  
  public void setTotalTaxAmount(Object paramObject)
  {
    this.totalTaxAmount = paramObject;
  }
  
  public void setTransactionId(String paramString)
  {
    this.transactionId = paramString;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\model\PBShipmentPrintLabel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */