package io.fabric.sdk.android.services.settings;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Logger;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class Settings
{
  public static final String SETTINGS_CACHE_FILENAME = "com.crashlytics.settings.json";
  private static final String SETTINGS_URL_FORMAT = "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings";
  private boolean initialized = false;
  private SettingsController settingsController;
  private final AtomicReference<SettingsData> settingsData = new AtomicReference();
  private final CountDownLatch settingsDataLatch = new CountDownLatch(1);
  
  public static Settings getInstance()
  {
    return LazyHolder.INSTANCE;
  }
  
  private void setSettingsData(SettingsData paramSettingsData)
  {
    this.settingsData.set(paramSettingsData);
    this.settingsDataLatch.countDown();
  }
  
  public SettingsData awaitSettingsData()
  {
    try
    {
      this.settingsDataLatch.await();
      SettingsData localSettingsData = (SettingsData)this.settingsData.get();
      return localSettingsData;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        Fabric.getLogger().e("Fabric", "Interrupted while waiting for settings data.");
        Object localObject = null;
      }
    }
  }
  
  public void clearSettings()
  {
    this.settingsData.set(null);
  }
  
  /* Error */
  public Settings initialize(io.fabric.sdk.android.Kit paramKit, io.fabric.sdk.android.services.common.IdManager paramIdManager, io.fabric.sdk.android.services.network.HttpRequestFactory paramHttpRequestFactory, String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 46	io/fabric/sdk/android/services/settings/Settings:initialized	Z
    //   6: istore 8
    //   8: iload 8
    //   10: ifeq +7 -> 17
    //   13: aload_0
    //   14: monitorexit
    //   15: aload_0
    //   16: areturn
    //   17: aload_0
    //   18: getfield 97	io/fabric/sdk/android/services/settings/Settings:settingsController	Lio/fabric/sdk/android/services/settings/SettingsController;
    //   21: ifnonnull +230 -> 251
    //   24: aload_1
    //   25: invokevirtual 103	io/fabric/sdk/android/Kit:getContext	()Landroid/content/Context;
    //   28: astore 15
    //   30: aload_2
    //   31: invokevirtual 109	io/fabric/sdk/android/services/common/IdManager:getAppIdentifier	()Ljava/lang/String;
    //   34: astore 14
    //   36: new 111	io/fabric/sdk/android/services/common/ApiKey
    //   39: astore 9
    //   41: aload 9
    //   43: invokespecial 112	io/fabric/sdk/android/services/common/ApiKey:<init>	()V
    //   46: aload 9
    //   48: aload 15
    //   50: invokevirtual 116	io/fabric/sdk/android/services/common/ApiKey:getValue	(Landroid/content/Context;)Ljava/lang/String;
    //   53: astore 13
    //   55: aload_2
    //   56: invokevirtual 119	io/fabric/sdk/android/services/common/IdManager:getInstallerPackageName	()Ljava/lang/String;
    //   59: astore 16
    //   61: new 121	io/fabric/sdk/android/services/common/SystemCurrentTimeProvider
    //   64: astore 11
    //   66: aload 11
    //   68: invokespecial 122	io/fabric/sdk/android/services/common/SystemCurrentTimeProvider:<init>	()V
    //   71: new 124	io/fabric/sdk/android/services/settings/DefaultSettingsJsonTransform
    //   74: astore 9
    //   76: aload 9
    //   78: invokespecial 125	io/fabric/sdk/android/services/settings/DefaultSettingsJsonTransform:<init>	()V
    //   81: new 127	io/fabric/sdk/android/services/settings/DefaultCachedSettingsIo
    //   84: astore 10
    //   86: aload 10
    //   88: aload_1
    //   89: invokespecial 130	io/fabric/sdk/android/services/settings/DefaultCachedSettingsIo:<init>	(Lio/fabric/sdk/android/Kit;)V
    //   92: aload 15
    //   94: invokestatic 135	io/fabric/sdk/android/services/common/CommonUtils:getAppIconHashOrNull	(Landroid/content/Context;)Ljava/lang/String;
    //   97: astore 12
    //   99: getstatic 141	java/util/Locale:US	Ljava/util/Locale;
    //   102: ldc 19
    //   104: iconst_1
    //   105: anewarray 4	java/lang/Object
    //   108: dup
    //   109: iconst_0
    //   110: aload 14
    //   112: aastore
    //   113: invokestatic 147	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   116: astore 17
    //   118: new 149	io/fabric/sdk/android/services/settings/DefaultSettingsSpiCall
    //   121: astore 14
    //   123: aload 14
    //   125: aload_1
    //   126: aload 6
    //   128: aload 17
    //   130: aload_3
    //   131: invokespecial 152	io/fabric/sdk/android/services/settings/DefaultSettingsSpiCall:<init>	(Lio/fabric/sdk/android/Kit;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/HttpRequestFactory;)V
    //   134: aload_2
    //   135: invokevirtual 155	io/fabric/sdk/android/services/common/IdManager:getModelName	()Ljava/lang/String;
    //   138: astore 18
    //   140: aload_2
    //   141: invokevirtual 158	io/fabric/sdk/android/services/common/IdManager:getOsBuildVersionString	()Ljava/lang/String;
    //   144: astore 17
    //   146: aload_2
    //   147: invokevirtual 161	io/fabric/sdk/android/services/common/IdManager:getOsDisplayVersionString	()Ljava/lang/String;
    //   150: astore_3
    //   151: aload_2
    //   152: invokevirtual 164	io/fabric/sdk/android/services/common/IdManager:getAdvertisingId	()Ljava/lang/String;
    //   155: astore 19
    //   157: aload_2
    //   158: invokevirtual 167	io/fabric/sdk/android/services/common/IdManager:getAppInstallIdentifier	()Ljava/lang/String;
    //   161: astore 6
    //   163: aload_2
    //   164: invokevirtual 170	io/fabric/sdk/android/services/common/IdManager:getAndroidId	()Ljava/lang/String;
    //   167: astore_2
    //   168: iconst_1
    //   169: anewarray 143	java/lang/String
    //   172: dup
    //   173: iconst_0
    //   174: aload 15
    //   176: invokestatic 173	io/fabric/sdk/android/services/common/CommonUtils:resolveBuildId	(Landroid/content/Context;)Ljava/lang/String;
    //   179: aastore
    //   180: invokestatic 177	io/fabric/sdk/android/services/common/CommonUtils:createInstanceIdFrom	([Ljava/lang/String;)Ljava/lang/String;
    //   183: astore 20
    //   185: aload 16
    //   187: invokestatic 183	io/fabric/sdk/android/services/common/DeliveryMechanism:determineFrom	(Ljava/lang/String;)Lio/fabric/sdk/android/services/common/DeliveryMechanism;
    //   190: invokevirtual 187	io/fabric/sdk/android/services/common/DeliveryMechanism:getId	()I
    //   193: istore 7
    //   195: new 189	io/fabric/sdk/android/services/settings/SettingsRequest
    //   198: astore 15
    //   200: aload 15
    //   202: aload 13
    //   204: aload 18
    //   206: aload 17
    //   208: aload_3
    //   209: aload 19
    //   211: aload 6
    //   213: aload_2
    //   214: aload 20
    //   216: aload 5
    //   218: aload 4
    //   220: iload 7
    //   222: aload 12
    //   224: invokespecial 192	io/fabric/sdk/android/services/settings/SettingsRequest:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    //   227: new 194	io/fabric/sdk/android/services/settings/DefaultSettingsController
    //   230: astore_2
    //   231: aload_2
    //   232: aload_1
    //   233: aload 15
    //   235: aload 11
    //   237: aload 9
    //   239: aload 10
    //   241: aload 14
    //   243: invokespecial 197	io/fabric/sdk/android/services/settings/DefaultSettingsController:<init>	(Lio/fabric/sdk/android/Kit;Lio/fabric/sdk/android/services/settings/SettingsRequest;Lio/fabric/sdk/android/services/common/CurrentTimeProvider;Lio/fabric/sdk/android/services/settings/SettingsJsonTransform;Lio/fabric/sdk/android/services/settings/CachedSettingsIo;Lio/fabric/sdk/android/services/settings/SettingsSpiCall;)V
    //   246: aload_0
    //   247: aload_2
    //   248: putfield 97	io/fabric/sdk/android/services/settings/Settings:settingsController	Lio/fabric/sdk/android/services/settings/SettingsController;
    //   251: aload_0
    //   252: iconst_1
    //   253: putfield 46	io/fabric/sdk/android/services/settings/Settings:initialized	Z
    //   256: goto -243 -> 13
    //   259: astore_1
    //   260: aload_0
    //   261: monitorexit
    //   262: aload_1
    //   263: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	264	0	this	Settings
    //   0	264	1	paramKit	io.fabric.sdk.android.Kit
    //   0	264	2	paramIdManager	io.fabric.sdk.android.services.common.IdManager
    //   0	264	3	paramHttpRequestFactory	io.fabric.sdk.android.services.network.HttpRequestFactory
    //   0	264	4	paramString1	String
    //   0	264	5	paramString2	String
    //   0	264	6	paramString3	String
    //   193	28	7	i	int
    //   6	3	8	bool	boolean
    //   39	199	9	localObject1	Object
    //   84	156	10	localDefaultCachedSettingsIo	DefaultCachedSettingsIo
    //   64	172	11	localSystemCurrentTimeProvider	io.fabric.sdk.android.services.common.SystemCurrentTimeProvider
    //   97	126	12	str1	String
    //   53	150	13	str2	String
    //   34	208	14	localObject2	Object
    //   28	206	15	localObject3	Object
    //   59	127	16	str3	String
    //   116	91	17	str4	String
    //   138	67	18	str5	String
    //   155	55	19	str6	String
    //   183	32	20	str7	String
    // Exception table:
    //   from	to	target	type
    //   2	8	259	finally
    //   17	251	259	finally
    //   251	256	259	finally
  }
  
  /* Error */
  public boolean loadSettingsData()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 97	io/fabric/sdk/android/services/settings/Settings:settingsController	Lio/fabric/sdk/android/services/settings/SettingsController;
    //   6: invokeinterface 203 1 0
    //   11: astore_2
    //   12: aload_0
    //   13: aload_2
    //   14: invokespecial 205	io/fabric/sdk/android/services/settings/Settings:setSettingsData	(Lio/fabric/sdk/android/services/settings/SettingsData;)V
    //   17: aload_2
    //   18: ifnull +9 -> 27
    //   21: iconst_1
    //   22: istore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: iload_1
    //   26: ireturn
    //   27: iconst_0
    //   28: istore_1
    //   29: goto -6 -> 23
    //   32: astore_2
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_2
    //   36: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	37	0	this	Settings
    //   22	7	1	bool	boolean
    //   11	7	2	localSettingsData	SettingsData
    //   32	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	17	32	finally
  }
  
  /* Error */
  public boolean loadSettingsSkippingCache()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 97	io/fabric/sdk/android/services/settings/Settings:settingsController	Lio/fabric/sdk/android/services/settings/SettingsController;
    //   6: getstatic 212	io/fabric/sdk/android/services/settings/SettingsCacheBehavior:SKIP_CACHE_LOOKUP	Lio/fabric/sdk/android/services/settings/SettingsCacheBehavior;
    //   9: invokeinterface 215 2 0
    //   14: astore_2
    //   15: aload_0
    //   16: aload_2
    //   17: invokespecial 205	io/fabric/sdk/android/services/settings/Settings:setSettingsData	(Lio/fabric/sdk/android/services/settings/SettingsData;)V
    //   20: aload_2
    //   21: ifnonnull +16 -> 37
    //   24: invokestatic 82	io/fabric/sdk/android/Fabric:getLogger	()Lio/fabric/sdk/android/Logger;
    //   27: ldc 84
    //   29: ldc -39
    //   31: aconst_null
    //   32: invokeinterface 220 4 0
    //   37: aload_2
    //   38: ifnull +9 -> 47
    //   41: iconst_1
    //   42: istore_1
    //   43: aload_0
    //   44: monitorexit
    //   45: iload_1
    //   46: ireturn
    //   47: iconst_0
    //   48: istore_1
    //   49: goto -6 -> 43
    //   52: astore_2
    //   53: aload_0
    //   54: monitorexit
    //   55: aload_2
    //   56: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	57	0	this	Settings
    //   42	7	1	bool	boolean
    //   14	24	2	localSettingsData	SettingsData
    //   52	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	20	52	finally
    //   24	37	52	finally
  }
  
  public void setSettingsController(SettingsController paramSettingsController)
  {
    this.settingsController = paramSettingsController;
  }
  
  public <T> T withSettings(SettingsAccess<T> paramSettingsAccess, T paramT)
  {
    SettingsData localSettingsData = (SettingsData)this.settingsData.get();
    if (localSettingsData == null) {}
    for (;;)
    {
      return paramT;
      paramT = paramSettingsAccess.usingSettings(localSettingsData);
    }
  }
  
  static class LazyHolder
  {
    private static final Settings INSTANCE = new Settings(null);
  }
  
  public static abstract interface SettingsAccess<T>
  {
    public abstract T usingSettings(SettingsData paramSettingsData);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\Settings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */