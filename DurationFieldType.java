package org.joda.time;

import java.io.Serializable;

public abstract class DurationFieldType
  implements Serializable
{
  static final byte CENTURIES = 2;
  static final DurationFieldType CENTURIES_TYPE;
  static final byte DAYS = 7;
  static final DurationFieldType DAYS_TYPE;
  static final byte ERAS = 1;
  static final DurationFieldType ERAS_TYPE = new StandardDurationFieldType("eras", (byte)1);
  static final byte HALFDAYS = 8;
  static final DurationFieldType HALFDAYS_TYPE;
  static final byte HOURS = 9;
  static final DurationFieldType HOURS_TYPE;
  static final byte MILLIS = 12;
  static final DurationFieldType MILLIS_TYPE = new StandardDurationFieldType("millis", (byte)12);
  static final byte MINUTES = 10;
  static final DurationFieldType MINUTES_TYPE;
  static final byte MONTHS = 5;
  static final DurationFieldType MONTHS_TYPE;
  static final byte SECONDS = 11;
  static final DurationFieldType SECONDS_TYPE;
  static final byte WEEKS = 6;
  static final DurationFieldType WEEKS_TYPE;
  static final byte WEEKYEARS = 3;
  static final DurationFieldType WEEKYEARS_TYPE;
  static final byte YEARS = 4;
  static final DurationFieldType YEARS_TYPE;
  private static final long serialVersionUID = 8765135187319L;
  private final String iName;
  
  static
  {
    CENTURIES_TYPE = new StandardDurationFieldType("centuries", (byte)2);
    WEEKYEARS_TYPE = new StandardDurationFieldType("weekyears", (byte)3);
    YEARS_TYPE = new StandardDurationFieldType("years", (byte)4);
    MONTHS_TYPE = new StandardDurationFieldType("months", (byte)5);
    WEEKS_TYPE = new StandardDurationFieldType("weeks", (byte)6);
    DAYS_TYPE = new StandardDurationFieldType("days", (byte)7);
    HALFDAYS_TYPE = new StandardDurationFieldType("halfdays", (byte)8);
    HOURS_TYPE = new StandardDurationFieldType("hours", (byte)9);
    MINUTES_TYPE = new StandardDurationFieldType("minutes", (byte)10);
    SECONDS_TYPE = new StandardDurationFieldType("seconds", (byte)11);
  }
  
  protected DurationFieldType(String paramString)
  {
    this.iName = paramString;
  }
  
  public static DurationFieldType centuries()
  {
    return CENTURIES_TYPE;
  }
  
  public static DurationFieldType days()
  {
    return DAYS_TYPE;
  }
  
  public static DurationFieldType eras()
  {
    return ERAS_TYPE;
  }
  
  public static DurationFieldType halfdays()
  {
    return HALFDAYS_TYPE;
  }
  
  public static DurationFieldType hours()
  {
    return HOURS_TYPE;
  }
  
  public static DurationFieldType millis()
  {
    return MILLIS_TYPE;
  }
  
  public static DurationFieldType minutes()
  {
    return MINUTES_TYPE;
  }
  
  public static DurationFieldType months()
  {
    return MONTHS_TYPE;
  }
  
  public static DurationFieldType seconds()
  {
    return SECONDS_TYPE;
  }
  
  public static DurationFieldType weeks()
  {
    return WEEKS_TYPE;
  }
  
  public static DurationFieldType weekyears()
  {
    return WEEKYEARS_TYPE;
  }
  
  public static DurationFieldType years()
  {
    return YEARS_TYPE;
  }
  
  public abstract DurationField getField(Chronology paramChronology);
  
  public String getName()
  {
    return this.iName;
  }
  
  public boolean isSupported(Chronology paramChronology)
  {
    return getField(paramChronology).isSupported();
  }
  
  public String toString()
  {
    return getName();
  }
  
  private static class StandardDurationFieldType
    extends DurationFieldType
  {
    private static final long serialVersionUID = 31156755687123L;
    private final byte iOrdinal;
    
    StandardDurationFieldType(String paramString, byte paramByte)
    {
      super();
      this.iOrdinal = paramByte;
    }
    
    private Object readResolve()
    {
      Object localObject;
      switch (this.iOrdinal)
      {
      default: 
        localObject = this;
      }
      for (;;)
      {
        return localObject;
        localObject = ERAS_TYPE;
        continue;
        localObject = CENTURIES_TYPE;
        continue;
        localObject = WEEKYEARS_TYPE;
        continue;
        localObject = YEARS_TYPE;
        continue;
        localObject = MONTHS_TYPE;
        continue;
        localObject = WEEKS_TYPE;
        continue;
        localObject = DAYS_TYPE;
        continue;
        localObject = HALFDAYS_TYPE;
        continue;
        localObject = HOURS_TYPE;
        continue;
        localObject = MINUTES_TYPE;
        continue;
        localObject = SECONDS_TYPE;
        continue;
        localObject = MILLIS_TYPE;
      }
    }
    
    public boolean equals(Object paramObject)
    {
      boolean bool = true;
      if (this == paramObject) {}
      for (;;)
      {
        return bool;
        if ((paramObject instanceof StandardDurationFieldType))
        {
          if (this.iOrdinal != ((StandardDurationFieldType)paramObject).iOrdinal) {
            bool = false;
          }
        }
        else {
          bool = false;
        }
      }
    }
    
    public DurationField getField(Chronology paramChronology)
    {
      paramChronology = DateTimeUtils.getChronology(paramChronology);
      switch (this.iOrdinal)
      {
      default: 
        throw new InternalError();
      case 1: 
        paramChronology = paramChronology.eras();
      }
      for (;;)
      {
        return paramChronology;
        paramChronology = paramChronology.centuries();
        continue;
        paramChronology = paramChronology.weekyears();
        continue;
        paramChronology = paramChronology.years();
        continue;
        paramChronology = paramChronology.months();
        continue;
        paramChronology = paramChronology.weeks();
        continue;
        paramChronology = paramChronology.days();
        continue;
        paramChronology = paramChronology.halfdays();
        continue;
        paramChronology = paramChronology.hours();
        continue;
        paramChronology = paramChronology.minutes();
        continue;
        paramChronology = paramChronology.seconds();
        continue;
        paramChronology = paramChronology.millis();
      }
    }
    
    public int hashCode()
    {
      return 1 << this.iOrdinal;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\DurationFieldType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */