package android.support.transition;

import android.animation.Animator;
import android.view.ViewGroup;

abstract interface VisibilityImpl
{
  public abstract boolean isVisible(TransitionValues paramTransitionValues);
  
  public abstract Animator onAppear(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, int paramInt1, TransitionValues paramTransitionValues2, int paramInt2);
  
  public abstract Animator onDisappear(ViewGroup paramViewGroup, TransitionValues paramTransitionValues1, int paramInt1, TransitionValues paramTransitionValues2, int paramInt2);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\transition\VisibilityImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */