package tech.dcube.companion.managers.server.backend;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class PBRequest
  extends Request<PBResponse>
{
  private Response.Listener<PBResponse> mListener;
  
  public PBRequest(int paramInt, String paramString, Response.Listener<PBResponse> paramListener, Response.ErrorListener paramErrorListener)
  {
    super(paramInt, paramString, paramErrorListener);
    this.mListener = paramListener;
  }
  
  protected void deliverResponse(PBResponse paramPBResponse)
  {
    this.mListener.onResponse(paramPBResponse);
  }
  
  protected VolleyError parseNetworkError(VolleyError paramVolleyError)
  {
    Throwable localThrowable = paramVolleyError.getCause();
    if (((localThrowable instanceof IOException)) && (localThrowable.getMessage().contains("authentication challenge"))) {
      paramVolleyError = new HashMap();
    }
    for (paramVolleyError = new VolleyError(new NetworkResponse(401, new byte[0], paramVolleyError, false));; paramVolleyError = super.parseNetworkError(paramVolleyError)) {
      return paramVolleyError;
    }
  }
  
  protected Response<PBResponse> parseNetworkResponse(NetworkResponse paramNetworkResponse)
  {
    return Response.success(new PBResponse(paramNetworkResponse), HttpHeaderParser.parseCacheHeaders(paramNetworkResponse));
  }
  
  public static class PBResponse
  {
    public final String body;
    public final Map<String, String> headers;
    public final NetworkResponse response;
    public final int statusCode;
    
    PBResponse(NetworkResponse paramNetworkResponse)
    {
      this.response = paramNetworkResponse;
      this.statusCode = paramNetworkResponse.statusCode;
      this.headers = paramNetworkResponse.headers;
      try
      {
        String str = new java/lang/String;
        str.<init>(paramNetworkResponse.data, HttpHeaderParser.parseCharset(paramNetworkResponse.headers));
        paramNetworkResponse = str;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        for (;;)
        {
          paramNetworkResponse = new String(paramNetworkResponse.data);
        }
      }
      this.body = paramNetworkResponse;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\managers\server\backend\PBRequest.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */