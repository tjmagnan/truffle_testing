package tech.dcube.companion;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost.TabContentFactory;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.adapters.SettingsAdapter;
import tech.dcube.companion.fragments.PBTopBarFragment;
import tech.dcube.companion.fragments.SettingsFragment;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBDevice;

public class MeterSettings
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "MeterSettings: ";
  private OnMeterSettingsListener mListener;
  private String mParam1;
  private String mParam2;
  private TabLayout mTabLayout;
  private HashMap<String, TabInfo> mapTabInfo = new HashMap();
  
  public static MeterSettings newInstance()
  {
    MeterSettings localMeterSettings = new MeterSettings();
    localMeterSettings.setArguments(new Bundle());
    return localMeterSettings;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnMeterSettingsListener))
    {
      this.mListener = ((OnMeterSettingsListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement Listener");
  }
  
  public void onButtonPressed(Uri paramUri)
  {
    if (this.mListener != null) {
      this.mListener.onMeterSettingsInteraction("button");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    this.mListener.onMeterSettingsInteraction("back");
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = paramLayoutInflater.inflate(2130968629, paramViewGroup, false);
    ButterKnife.bind(this, paramBundle);
    setTopBarTitle("Meter Settings");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    Log.v("MeterSettings: ", " on create view called");
    this.mTabLayout = ((TabLayout)paramBundle.findViewById(2131624110));
    ArrayList localArrayList = new ArrayList();
    paramLayoutInflater = DataManager.getInstance().getDevices();
    int i = 0;
    Iterator localIterator = paramLayoutInflater.iterator();
    while (localIterator.hasNext())
    {
      PBDevice localPBDevice = (PBDevice)localIterator.next();
      paramViewGroup = (String)DataManager.getInstance().getMeterSerialToNickName().get(localPBDevice.getSerialNumber());
      paramLayoutInflater = paramViewGroup;
      if (paramViewGroup == "") {
        paramLayoutInflater = localPBDevice.getSerialNumber();
      }
      this.mTabLayout.addTab(this.mTabLayout.newTab().setText("Meter" + (i + 1)));
      localArrayList.add(SettingsFragment.newInstance(i, paramLayoutInflater, localPBDevice.getModelNumber(), localPBDevice.getSerialNumber()));
      i++;
    }
    this.mTabLayout.setTabTextColors(getResources().getColor(2131492939), getResources().getColor(2131492937));
    this.mTabLayout.setTabMode(0);
    paramLayoutInflater = (ViewPager)paramBundle.findViewById(2131624112);
    paramLayoutInflater.setAdapter(new SettingsAdapter(getActivity().getSupportFragmentManager(), localArrayList));
    this.mTabLayout.setupWithViewPager(paramLayoutInflater);
    return paramBundle;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  void updateTabMeterNames()
  {
    int i = 0;
    Iterator localIterator = DataManager.getInstance().getDevices().iterator();
    while (localIterator.hasNext())
    {
      PBDevice localPBDevice = (PBDevice)localIterator.next();
      String str2 = (String)DataManager.getInstance().getMeterSerialToNickName().get(localPBDevice.getSerialNumber());
      String str1 = str2;
      if (str2 == "") {
        str1 = localPBDevice.getSerialNumber();
      }
      this.mTabLayout.getTabAt(i).setText(str1);
      i++;
    }
  }
  
  public static abstract interface OnMeterSettingsListener
  {
    public abstract void onMeterSettingsInteraction(String paramString);
  }
  
  class TabFactory
    implements TabHost.TabContentFactory
  {
    private final Context mContext;
    
    public TabFactory(Context paramContext)
    {
      this.mContext = paramContext;
    }
    
    public View createTabContent(String paramString)
    {
      paramString = new View(this.mContext);
      paramString.setMinimumWidth(0);
      paramString.setMinimumHeight(0);
      return paramString;
    }
  }
  
  private class TabInfo
  {
    private Bundle args;
    private Class<?> clss;
    private Fragment fragment;
    private String tag;
    
    TabInfo(Class<?> paramClass, Bundle paramBundle)
    {
      this.tag = paramClass;
      this.clss = paramBundle;
      Bundle localBundle;
      this.args = localBundle;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\MeterSettings.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */