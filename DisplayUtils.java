package me.dm7.barcodescanner.core;

import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.WindowManager;

public class DisplayUtils
{
  public static int getScreenOrientation(Context paramContext)
  {
    paramContext = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    int i;
    if (paramContext.getWidth() == paramContext.getHeight()) {
      i = 3;
    }
    for (;;)
    {
      return i;
      if (paramContext.getWidth() < paramContext.getHeight()) {
        i = 1;
      } else {
        i = 2;
      }
    }
  }
  
  public static Point getScreenResolution(Context paramContext)
  {
    paramContext = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    Point localPoint = new Point();
    if (Build.VERSION.SDK_INT >= 13) {
      paramContext.getSize(localPoint);
    }
    for (;;)
    {
      return localPoint;
      localPoint.set(paramContext.getWidth(), paramContext.getHeight());
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\dm7\barcodescanner\core\DisplayUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */