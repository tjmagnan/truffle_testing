package io.fabric.sdk.android.services.common;

class AdvertisingInfo
{
  public final String advertisingId;
  public final boolean limitAdTrackingEnabled;
  
  AdvertisingInfo(String paramString, boolean paramBoolean)
  {
    this.advertisingId = paramString;
    this.limitAdTrackingEnabled = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = true;
    if (this == paramObject) {}
    do
    {
      for (;;)
      {
        return bool;
        if ((paramObject == null) || (getClass() != paramObject.getClass()))
        {
          bool = false;
        }
        else
        {
          paramObject = (AdvertisingInfo)paramObject;
          if (this.limitAdTrackingEnabled == ((AdvertisingInfo)paramObject).limitAdTrackingEnabled) {
            break;
          }
          bool = false;
        }
      }
      if (this.advertisingId == null) {
        break;
      }
    } while (this.advertisingId.equals(((AdvertisingInfo)paramObject).advertisingId));
    for (;;)
    {
      bool = false;
      break;
      if (((AdvertisingInfo)paramObject).advertisingId == null) {
        break;
      }
    }
  }
  
  public int hashCode()
  {
    int j = 0;
    if (this.advertisingId != null) {}
    for (int i = this.advertisingId.hashCode();; i = 0)
    {
      if (this.limitAdTrackingEnabled) {
        j = 1;
      }
      return i * 31 + j;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\common\AdvertisingInfo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */