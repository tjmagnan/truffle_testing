package com.testfairy.p;

import java.io.PrintWriter;
import java.io.StringWriter;

public class l
{
  public static String a(String paramString)
  {
    int i = paramString.indexOf('\n');
    String str = paramString;
    if (i >= 0) {
      str = paramString.substring(0, i);
    }
    return str;
  }
  
  public static String a(Throwable paramThrowable)
  {
    StringWriter localStringWriter = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
    return localStringWriter.toString();
  }
  
  public static String a(StackTraceElement[] paramArrayOfStackTraceElement)
  {
    StringWriter localStringWriter = new StringWriter();
    a(paramArrayOfStackTraceElement, new PrintWriter(localStringWriter));
    return localStringWriter.toString();
  }
  
  private static void a(StackTraceElement[] paramArrayOfStackTraceElement, PrintWriter paramPrintWriter)
  {
    int j = paramArrayOfStackTraceElement.length;
    for (int i = 0; i < j; i++) {
      paramPrintWriter.println(paramArrayOfStackTraceElement[i]);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\p\l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */