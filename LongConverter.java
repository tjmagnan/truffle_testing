package org.joda.time.convert;

import org.joda.time.Chronology;

class LongConverter
  extends AbstractConverter
  implements InstantConverter, PartialConverter, DurationConverter
{
  static final LongConverter INSTANCE = new LongConverter();
  
  public long getDurationMillis(Object paramObject)
  {
    return ((Long)paramObject).longValue();
  }
  
  public long getInstantMillis(Object paramObject, Chronology paramChronology)
  {
    return ((Long)paramObject).longValue();
  }
  
  public Class<?> getSupportedType()
  {
    return Long.class;
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\LongConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */