package com.testfairy.n;

import android.os.Handler;
import android.os.Looper;
import com.testfairy.g;
import com.testfairy.p.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class a
  extends b
{
  private static final long a = 4000L;
  private static final String b = "delay";
  private Handler c = null;
  private long d = -1L;
  private final Object e = new Object();
  private final List f = Collections.synchronizedList(new ArrayList());
  private boolean g = false;
  private final com.testfairy.a.a h;
  
  public a(com.testfairy.m.b paramb, com.testfairy.a.a parama)
  {
    super(paramb);
    this.h = parama;
  }
  
  private void a(long paramLong)
  {
    synchronized (this.e)
    {
      this.d = paramLong;
      return;
    }
  }
  
  private void a(long paramLong1, long paramLong2)
  {
    Object localObject = new HashMap(1);
    ((Map)localObject).put("delay", Long.valueOf(paramLong1));
    localObject = new g(20, (Map)localObject);
    ((g)localObject).a(paramLong2);
    b().a((g)localObject);
  }
  
  private Handler c()
  {
    if (this.c == null)
    {
      Looper localLooper = Looper.getMainLooper();
      if (localLooper != null) {
        this.c = new Handler(localLooper);
      }
    }
    return this.c;
  }
  
  private long d()
  {
    synchronized (this.e)
    {
      long l = this.d;
      return l;
    }
  }
  
  private void e()
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localJSONObject.put("type", 7);
      localJSONObject.put("data", f());
      g localg = new com/testfairy/g;
      localg.<init>(16, localJSONObject);
      b().a(localg);
      this.g = true;
      return;
    }
    catch (Throwable localThrowable)
    {
      for (;;) {}
    }
  }
  
  private JSONArray f()
  {
    Object localObject = Thread.getAllStackTraces().keySet();
    JSONArray localJSONArray = new JSONArray();
    Iterator localIterator = ((Set)localObject).iterator();
    while (localIterator.hasNext())
    {
      localObject = (Thread)localIterator.next();
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("name", ((Thread)localObject).getName());
      localJSONObject.put("id", ((Thread)localObject).getId());
      localJSONObject.put("stacktrace", l.a(((Thread)localObject).getStackTrace()));
      localJSONArray.put(localJSONObject);
    }
    return localJSONArray;
  }
  
  public void a()
  {
    long l1 = d();
    if (this.h.a()) {}
    for (;;)
    {
      return;
      if (l1 != -1L)
      {
        long l2 = System.currentTimeMillis();
        this.f.add(Long.valueOf(l2));
        if ((l2 - l1 > 4000L) && (!this.g))
        {
          e();
          b().a();
        }
      }
      else if (c() != null)
      {
        this.c.post(new a(System.currentTimeMillis()));
      }
    }
  }
  
  private class a
    implements Runnable
  {
    private final long b;
    
    public a(long paramLong)
    {
      this.b = paramLong;
      a();
    }
    
    private void a()
    {
      a.a(a.this, this.b);
    }
    
    private void a(long paramLong)
    {
      a.a(a.this, -1L);
      synchronized (a.a(a.this))
      {
        if (!a.a(a.this).isEmpty()) {
          a.a(a.this, paramLong, ((Long)a.a(a.this).get(a.a(a.this).size() - 1)).longValue());
        }
        a.a(a.this).clear();
        a.a(a.this, false);
        return;
      }
    }
    
    public void run()
    {
      long l = System.currentTimeMillis() - this.b;
      a.a(a.this, l, this.b);
      a(l);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\com\testfairy\n\a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */