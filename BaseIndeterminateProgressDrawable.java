package me.zhanghai.android.materialprogressbar;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import me.zhanghai.android.materialprogressbar.internal.ThemeUtils;

abstract class BaseIndeterminateProgressDrawable
  extends BaseProgressDrawable
  implements Animatable
{
  protected Animator[] mAnimators;
  
  @SuppressLint({"NewApi"})
  public BaseIndeterminateProgressDrawable(Context paramContext)
  {
    setTint(ThemeUtils.getColorFromAttrRes(R.attr.colorControlActivated, -16777216, paramContext));
  }
  
  private boolean isStarted()
  {
    boolean bool2 = false;
    Animator[] arrayOfAnimator = this.mAnimators;
    int j = arrayOfAnimator.length;
    for (int i = 0;; i++)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (arrayOfAnimator[i].isStarted()) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    if (isStarted()) {
      invalidateSelf();
    }
  }
  
  public boolean isRunning()
  {
    boolean bool2 = false;
    Animator[] arrayOfAnimator = this.mAnimators;
    int j = arrayOfAnimator.length;
    for (int i = 0;; i++)
    {
      boolean bool1 = bool2;
      if (i < j)
      {
        if (arrayOfAnimator[i].isRunning()) {
          bool1 = true;
        }
      }
      else {
        return bool1;
      }
    }
  }
  
  public void start()
  {
    if (isStarted()) {}
    for (;;)
    {
      return;
      Animator[] arrayOfAnimator = this.mAnimators;
      int j = arrayOfAnimator.length;
      for (int i = 0; i < j; i++) {
        arrayOfAnimator[i].start();
      }
      invalidateSelf();
    }
  }
  
  public void stop()
  {
    Animator[] arrayOfAnimator = this.mAnimators;
    int j = arrayOfAnimator.length;
    for (int i = 0; i < j; i++) {
      arrayOfAnimator[i].end();
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\me\zhanghai\android\materialprogressbar\BaseIndeterminateProgressDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */