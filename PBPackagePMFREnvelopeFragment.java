package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBParcelPackage;
import tech.dcube.companion.model.PBShipmentPackage;

public class PBPackagePMFREnvelopeFragment
  extends PBTopBarFragment
  implements View.OnClickListener
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  public static final String FLAT_PMFR_ENVELOPE = "FRE";
  public static final String LEGAL_PMFR_ENVELOPE = "LGLFRENV";
  public static final String PADDED_PMFR_ENVELOPE = "PFRENV";
  public static final String WINDOWED_PMFR_ENVELOPE = "windowed";
  LinearLayout FlatRateEnvelope;
  LinearLayout LegalFlatRateEnvelope;
  LinearLayout PaddedFlatRateEnvelope;
  TextView Rate_FlatRateEnvelope;
  TextView Rate_LegalFlatRateEnvelope;
  TextView Rate_PaddedFlatRateEnvelope;
  TextView Rate_WindowFlatRateEnvelope;
  LinearLayout WindowFlatRateEnvelope;
  private OnPackagePMFREnvelopeListener mListener;
  private String mParam1;
  private String mParam2;
  
  public static PBPackagePMFREnvelopeFragment newInstance(String paramString1, String paramString2)
  {
    PBPackagePMFREnvelopeFragment localPBPackagePMFREnvelopeFragment = new PBPackagePMFREnvelopeFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBPackagePMFREnvelopeFragment.setArguments(localBundle);
    return localPBPackagePMFREnvelopeFragment;
  }
  
  void hideAllPMFREnvelopes()
  {
    this.FlatRateEnvelope.setVisibility(8);
    this.LegalFlatRateEnvelope.setVisibility(8);
    this.PaddedFlatRateEnvelope.setVisibility(8);
    this.WindowFlatRateEnvelope.setVisibility(8);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnPackagePMFREnvelopeListener))
    {
      this.mListener = ((OnPackagePMFREnvelopeListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnFragmentInteractionListener");
  }
  
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      return;
      onPMFREnvelopeSelected("FRE");
      continue;
      onPMFREnvelopeSelected("LGLFRENV");
      continue;
      onPMFREnvelopeSelected("PFRENV");
      continue;
      onPMFREnvelopeSelected("windowed");
    }
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968637, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Select Packaging");
    setTopBarLeftButtonText("Back");
    hideTopBarRightButton();
    this.FlatRateEnvelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624142));
    this.LegalFlatRateEnvelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624145));
    this.PaddedFlatRateEnvelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624147));
    this.WindowFlatRateEnvelope = ((LinearLayout)paramLayoutInflater.findViewById(2131624149));
    this.Rate_FlatRateEnvelope = ((TextView)paramLayoutInflater.findViewById(2131624144));
    this.Rate_LegalFlatRateEnvelope = ((TextView)paramLayoutInflater.findViewById(2131624146));
    this.Rate_PaddedFlatRateEnvelope = ((TextView)paramLayoutInflater.findViewById(2131624148));
    this.Rate_WindowFlatRateEnvelope = ((TextView)paramLayoutInflater.findViewById(2131624150));
    this.FlatRateEnvelope.setOnClickListener(this);
    this.LegalFlatRateEnvelope.setOnClickListener(this);
    this.PaddedFlatRateEnvelope.setOnClickListener(this);
    this.WindowFlatRateEnvelope.setOnClickListener(this);
    hideAllPMFREnvelopes();
    showAvailablePMFREnvelopes();
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onPMFREnvelopeSelected(String paramString)
  {
    if (this.mListener != null)
    {
      Object localObject2 = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.FLAT_RATE_ENVELOPE);
      Object localObject1 = new PBShipmentPackage();
      Iterator localIterator = ((List)localObject2).iterator();
      while (localIterator.hasNext())
      {
        localObject2 = (PBShipmentPackage)localIterator.next();
        if (((PBShipmentPackage)localObject2).getPackageId().equals(paramString)) {
          localObject1 = localObject2;
        }
      }
      this.mListener.onPMFREnvelopeSelected((PBShipmentPackage)localObject1);
    }
  }
  
  void showAvailablePMFREnvelopes()
  {
    hideAllPMFREnvelopes();
    Object localObject = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.FLAT_RATE_ENVELOPE);
    int m = 0;
    int k = 0;
    int j = 0;
    int i = 0;
    boolean bool = DataManager.getInstance().isPackagesV0Success();
    Iterator localIterator = ((List)localObject).iterator();
    while (localIterator.hasNext())
    {
      localObject = (PBShipmentPackage)localIterator.next();
      if (((PBShipmentPackage)localObject).getPackageId().equals("FRE")) {
        m = 1;
      } else if (((PBShipmentPackage)localObject).getPackageId().equals("LGLFRENV")) {
        k = 1;
      } else if (((PBShipmentPackage)localObject).getPackageId().equals("PFRENV")) {
        j = 1;
      } else if (((PBShipmentPackage)localObject).getPackageId().equals("PFRENV")) {
        i = 1;
      }
    }
    if (m != 0)
    {
      this.FlatRateEnvelope.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("FRE")).getPrice().doubleValue()) });
        this.Rate_FlatRateEnvelope.setText((CharSequence)localObject);
      }
    }
    if (k != 0)
    {
      this.LegalFlatRateEnvelope.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("LGLFRENV")).getPrice().doubleValue()) });
        this.Rate_LegalFlatRateEnvelope.setText((CharSequence)localObject);
      }
    }
    if (j != 0)
    {
      this.PaddedFlatRateEnvelope.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("PFRENV")).getPrice().doubleValue()) });
        this.Rate_PaddedFlatRateEnvelope.setText((CharSequence)localObject);
      }
    }
    if (i != 0)
    {
      this.WindowFlatRateEnvelope.setVisibility(0);
      if (bool)
      {
        localObject = String.format("$ %.02f", new Object[] { Double.valueOf(((PBParcelPackage)DataManager.getInstance().getParcelPackages().get("windowed")).getPrice().doubleValue()) });
        this.Rate_WindowFlatRateEnvelope.setText((CharSequence)localObject);
      }
    }
  }
  
  public static abstract interface OnPackagePMFREnvelopeListener
  {
    public abstract void onPMFREnvelopeSelected(PBShipmentPackage paramPBShipmentPackage);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBPackagePMFREnvelopeFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */