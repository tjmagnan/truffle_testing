package android.support.v13.app;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.support.annotation.RequiresApi;

@TargetApi(24)
@RequiresApi(24)
class FragmentCompatApi24
{
  public static void setUserVisibleHint(Fragment paramFragment, boolean paramBoolean)
  {
    paramFragment.setUserVisibleHint(paramBoolean);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v13\app\FragmentCompatApi24.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */