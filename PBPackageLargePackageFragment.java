package tech.dcube.companion.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.ButterKnife;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import tech.dcube.companion.managers.data.DataManager;
import tech.dcube.companion.managers.util.UtilManager;
import tech.dcube.companion.model.PBDimensionRules;
import tech.dcube.companion.model.PBPackageType;
import tech.dcube.companion.model.PBShipmentPackage;
import tech.dcube.companion.model.PBWeightRules;

public class PBPackageLargePackageFragment
  extends PBTopBarFragment
{
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String LARGE_PACKAGE = "LP";
  EditText Height_in;
  EditText Length_in;
  TextView MaxDimensions;
  TextView MaxWeight;
  TextView MinDimensions;
  TextView MinWeight;
  EditText Weight_LBS;
  EditText Weight_OZ;
  EditText Width_in;
  double mEnvelopeWeight;
  private OnPackageLargePackageSelectListener mListener;
  private String mParam1;
  private String mParam2;
  PBShipmentPackage mShipmentPackage;
  double maxHeight_M;
  double maxLength_M;
  double maxWeight_KG;
  double maxWeight_LBS;
  double maxWidth_M;
  double minHeight_M;
  double minLength_M;
  double minWeight_KG;
  double minWeight_LBS;
  double minWidth_M;
  
  public static PBPackageLargePackageFragment newInstance(String paramString1, String paramString2)
  {
    PBPackageLargePackageFragment localPBPackageLargePackageFragment = new PBPackageLargePackageFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("param1", paramString1);
    localBundle.putString("param2", paramString2);
    localPBPackageLargePackageFragment.setArguments(localBundle);
    return localPBPackageLargePackageFragment;
  }
  
  boolean checkDimensionLimits()
  {
    double d1 = 0.0D;
    double d2 = 0.0D;
    double d3 = 0.0D;
    if (!this.Length_in.getText().toString().equals("")) {
      d1 = Double.parseDouble(this.Length_in.getText().toString());
    }
    if (!this.Width_in.getText().toString().equals("")) {
      d2 = Double.parseDouble(this.Width_in.getText().toString());
    }
    if (!this.Height_in.getText().toString().equals("")) {
      d3 = Double.parseDouble(this.Height_in.getText().toString());
    }
    d1 = UtilManager.dimensionImperialToMetric(d1);
    d2 = UtilManager.dimensionImperialToMetric(d2);
    d3 = UtilManager.dimensionImperialToMetric(d3);
    PBDimensionRules localPBDimensionRules = this.mShipmentPackage.getDimensionRules();
    boolean bool;
    if ((d1 >= this.minLength_M) && (d1 <= this.maxLength_M))
    {
      localPBDimensionRules.setLength(d1);
      if ((d2 < this.minWidth_M) || (d2 > this.maxWidth_M)) {
        break label232;
      }
      localPBDimensionRules.setWidth(d2);
      if ((d3 < this.minHeight_M) || (d3 > this.maxHeight_M)) {
        break label244;
      }
      localPBDimensionRules.setHeight(d3);
      this.mShipmentPackage.setDimensionRules(localPBDimensionRules);
      bool = true;
    }
    for (;;)
    {
      return bool;
      displayToast("Length is not in limits");
      bool = false;
      continue;
      label232:
      displayToast("Width is not in limits");
      bool = false;
      continue;
      label244:
      displayToast("Height is not in limits");
      bool = false;
    }
  }
  
  boolean checkWeightLimits()
  {
    double d1 = 0.0D;
    double d2 = 0.0D;
    if (!this.Weight_LBS.getText().toString().equals("")) {
      d1 = Double.parseDouble(this.Weight_LBS.getText().toString());
    }
    if (!this.Weight_OZ.getText().toString().equals("")) {
      d2 = Double.parseDouble(this.Weight_OZ.getText().toString());
    }
    d1 = UtilManager.weightLBtoKG(d1) + UtilManager.weightOZtoKG(d2);
    PBWeightRules localPBWeightRules = this.mShipmentPackage.getWeightRules();
    if ((d1 >= this.minWeight_KG) && (d1 <= this.maxWeight_KG))
    {
      localPBWeightRules.setWeight(d1);
      this.mShipmentPackage.setWeightRules(localPBWeightRules);
    }
    for (boolean bool = true;; bool = false)
    {
      return bool;
      displayToast("Weight is not in limits");
    }
  }
  
  public void displayToast(String paramString)
  {
    UtilManager.showGloabalToast(getContext(), paramString);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    if ((paramContext instanceof OnPackageLargePackageSelectListener))
    {
      this.mListener = ((OnPackageLargePackageSelectListener)paramContext);
      return;
    }
    throw new RuntimeException(paramContext.toString() + " must implement OnPackageLargePackageSelectListener");
  }
  
  public void onClickTopBarLeftButton()
  {
    getActivity().onBackPressed();
  }
  
  public void onClickTopBarRightButton()
  {
    onNextButtonInteraction();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments() != null)
    {
      this.mParam1 = getArguments().getString("param1");
      this.mParam2 = getArguments().getString("param2");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968641, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    setTopBarTitle("Select Packaging");
    setTopBarLeftButtonText("Back");
    setTopBarRightButtonText("Next");
    this.Length_in = ((EditText)paramLayoutInflater.findViewById(2131624171));
    this.Width_in = ((EditText)paramLayoutInflater.findViewById(2131624172));
    this.Height_in = ((EditText)paramLayoutInflater.findViewById(2131624173));
    this.Weight_LBS = ((EditText)paramLayoutInflater.findViewById(2131624164));
    this.Weight_OZ = ((EditText)paramLayoutInflater.findViewById(2131624165));
    this.MinWeight = ((TextView)paramLayoutInflater.findViewById(2131624166));
    this.MaxWeight = ((TextView)paramLayoutInflater.findViewById(2131624167));
    this.MinDimensions = ((TextView)paramLayoutInflater.findViewById(2131624175));
    this.MaxDimensions = ((TextView)paramLayoutInflater.findViewById(2131624176));
    paramViewGroup = (List)DataManager.getInstance().getShipmentPackages().get(PBPackageType.PACKAGE_LARGE);
    this.mShipmentPackage = new PBShipmentPackage();
    paramViewGroup = paramViewGroup.iterator();
    while (paramViewGroup.hasNext())
    {
      paramBundle = (PBShipmentPackage)paramViewGroup.next();
      if (paramBundle.getPackageId().equals("LP")) {
        this.mShipmentPackage = paramBundle;
      }
    }
    paramViewGroup = this.mShipmentPackage.getWeightRules();
    this.maxWeight_KG = paramViewGroup.getMaxWeightAllowedKg();
    this.minWeight_KG = paramViewGroup.getMinWeightAllowedKg();
    this.minWeight_LBS = UtilManager.weightKGtoLBS(this.minWeight_KG);
    this.maxWeight_LBS = UtilManager.weightKGtoLBS(this.maxWeight_KG);
    this.MinWeight.setText(String.format("min weight: %.02f lbs", new Object[] { Double.valueOf(this.minWeight_LBS) }));
    this.MaxWeight.setText(String.format("max weight: %.02f lbs", new Object[] { Double.valueOf(this.maxWeight_LBS) }));
    paramViewGroup = this.mShipmentPackage.getDimensionRules();
    this.minLength_M = paramViewGroup.getMinLength();
    this.maxLength_M = paramViewGroup.getMaxLength();
    this.minWidth_M = paramViewGroup.getMinWidth();
    this.maxWidth_M = paramViewGroup.getMaxWidth();
    this.minHeight_M = paramViewGroup.getMinHeight();
    this.maxHeight_M = paramViewGroup.getMaxHeight();
    double d4 = UtilManager.dimensionMetricToImperial(this.minLength_M);
    double d1 = UtilManager.dimensionMetricToImperial(this.minWidth_M);
    double d2 = UtilManager.dimensionMetricToImperial(this.minHeight_M);
    double d3 = UtilManager.dimensionMetricToImperial(this.maxLength_M);
    double d6 = UtilManager.dimensionMetricToImperial(this.maxWidth_M);
    double d5 = UtilManager.dimensionMetricToImperial(this.maxHeight_M);
    this.MinDimensions.setText(String.format("min dimensions: %1.02f\" x %2.02f\" x %3.02f\"", new Object[] { Double.valueOf(d4), Double.valueOf(d1), Double.valueOf(d2) }));
    this.MaxDimensions.setText(String.format("max dimensions: %1.02f\" x %2.02f\" x %3.02f\"", new Object[] { Double.valueOf(d3), Double.valueOf(d6), Double.valueOf(d5) }));
    return paramLayoutInflater;
  }
  
  public void onDetach()
  {
    super.onDetach();
    this.mListener = null;
  }
  
  public void onNextButtonInteraction()
  {
    if ((this.mListener != null) && (checkWeightLimits()) && (checkDimensionLimits())) {
      this.mListener.onPackageLargeSelected("action_next_button", this.mShipmentPackage);
    }
  }
  
  public static abstract interface OnPackageLargePackageSelectListener
  {
    public abstract void onPackageLargeSelected(String paramString, PBShipmentPackage paramPBShipmentPackage);
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\tech\dcube\companion\fragments\PBPackageLargePackageFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */