package io.fabric.sdk.android.services.settings;

abstract interface AppSpiCall
{
  public abstract boolean invoke(AppRequestData paramAppRequestData);
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\io\fabric\sdk\android\services\settings\AppSpiCall.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */