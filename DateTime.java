package org.joda.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Locale;
import org.joda.convert.FromString;
import org.joda.time.base.BaseDateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.field.AbstractReadableInstantFieldProperty;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public final class DateTime
  extends BaseDateTime
  implements ReadableDateTime, Serializable
{
  private static final long serialVersionUID = -5171125899451703815L;
  
  public DateTime() {}
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0, 0);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, 0);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, Chronology paramChronology)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramChronology);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, DateTimeZone paramDateTimeZone)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramDateTimeZone);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, Chronology paramChronology)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, 0, paramChronology);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, DateTimeZone paramDateTimeZone)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, 0, paramDateTimeZone);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, Chronology paramChronology)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0, 0, paramChronology);
  }
  
  public DateTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, DateTimeZone paramDateTimeZone)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, 0, 0, paramDateTimeZone);
  }
  
  public DateTime(long paramLong)
  {
    super(paramLong);
  }
  
  public DateTime(long paramLong, Chronology paramChronology)
  {
    super(paramLong, paramChronology);
  }
  
  public DateTime(long paramLong, DateTimeZone paramDateTimeZone)
  {
    super(paramLong, paramDateTimeZone);
  }
  
  public DateTime(Object paramObject)
  {
    super(paramObject, (Chronology)null);
  }
  
  public DateTime(Object paramObject, Chronology paramChronology)
  {
    super(paramObject, DateTimeUtils.getChronology(paramChronology));
  }
  
  public DateTime(Object paramObject, DateTimeZone paramDateTimeZone)
  {
    super(paramObject, paramDateTimeZone);
  }
  
  public DateTime(Chronology paramChronology)
  {
    super(paramChronology);
  }
  
  public DateTime(DateTimeZone paramDateTimeZone)
  {
    super(paramDateTimeZone);
  }
  
  public static DateTime now()
  {
    return new DateTime();
  }
  
  public static DateTime now(Chronology paramChronology)
  {
    if (paramChronology == null) {
      throw new NullPointerException("Chronology must not be null");
    }
    return new DateTime(paramChronology);
  }
  
  public static DateTime now(DateTimeZone paramDateTimeZone)
  {
    if (paramDateTimeZone == null) {
      throw new NullPointerException("Zone must not be null");
    }
    return new DateTime(paramDateTimeZone);
  }
  
  @FromString
  public static DateTime parse(String paramString)
  {
    return parse(paramString, ISODateTimeFormat.dateTimeParser().withOffsetParsed());
  }
  
  public static DateTime parse(String paramString, DateTimeFormatter paramDateTimeFormatter)
  {
    return paramDateTimeFormatter.parseDateTime(paramString);
  }
  
  public Property centuryOfEra()
  {
    return new Property(this, getChronology().centuryOfEra());
  }
  
  public Property dayOfMonth()
  {
    return new Property(this, getChronology().dayOfMonth());
  }
  
  public Property dayOfWeek()
  {
    return new Property(this, getChronology().dayOfWeek());
  }
  
  public Property dayOfYear()
  {
    return new Property(this, getChronology().dayOfYear());
  }
  
  public Property era()
  {
    return new Property(this, getChronology().era());
  }
  
  public Property hourOfDay()
  {
    return new Property(this, getChronology().hourOfDay());
  }
  
  public Property millisOfDay()
  {
    return new Property(this, getChronology().millisOfDay());
  }
  
  public Property millisOfSecond()
  {
    return new Property(this, getChronology().millisOfSecond());
  }
  
  public DateTime minus(long paramLong)
  {
    return withDurationAdded(paramLong, -1);
  }
  
  public DateTime minus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, -1);
  }
  
  public DateTime minus(ReadablePeriod paramReadablePeriod)
  {
    return withPeriodAdded(paramReadablePeriod, -1);
  }
  
  public DateTime minusDays(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().days().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusHours(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().hours().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusMillis(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().millis().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusMinutes(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().minutes().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusMonths(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().months().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusSeconds(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().seconds().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusWeeks(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().weeks().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime minusYears(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().years().subtract(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public Property minuteOfDay()
  {
    return new Property(this, getChronology().minuteOfDay());
  }
  
  public Property minuteOfHour()
  {
    return new Property(this, getChronology().minuteOfHour());
  }
  
  public Property monthOfYear()
  {
    return new Property(this, getChronology().monthOfYear());
  }
  
  public DateTime plus(long paramLong)
  {
    return withDurationAdded(paramLong, 1);
  }
  
  public DateTime plus(ReadableDuration paramReadableDuration)
  {
    return withDurationAdded(paramReadableDuration, 1);
  }
  
  public DateTime plus(ReadablePeriod paramReadablePeriod)
  {
    return withPeriodAdded(paramReadablePeriod, 1);
  }
  
  public DateTime plusDays(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().days().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusHours(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().hours().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusMillis(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().millis().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusMinutes(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().minutes().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusMonths(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().months().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusSeconds(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().seconds().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusWeeks(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().weeks().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime plusYears(int paramInt)
  {
    if (paramInt == 0) {}
    for (DateTime localDateTime = this;; localDateTime = withMillis(getChronology().years().add(getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public Property property(DateTimeFieldType paramDateTimeFieldType)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("The DateTimeFieldType must not be null");
    }
    DateTimeField localDateTimeField = paramDateTimeFieldType.getField(getChronology());
    if (!localDateTimeField.isSupported()) {
      throw new IllegalArgumentException("Field '" + paramDateTimeFieldType + "' is not supported");
    }
    return new Property(this, localDateTimeField);
  }
  
  public Property secondOfDay()
  {
    return new Property(this, getChronology().secondOfDay());
  }
  
  public Property secondOfMinute()
  {
    return new Property(this, getChronology().secondOfMinute());
  }
  
  @Deprecated
  public DateMidnight toDateMidnight()
  {
    return new DateMidnight(getMillis(), getChronology());
  }
  
  public DateTime toDateTime()
  {
    return this;
  }
  
  public DateTime toDateTime(Chronology paramChronology)
  {
    paramChronology = DateTimeUtils.getChronology(paramChronology);
    if (getChronology() == paramChronology) {}
    for (paramChronology = this;; paramChronology = super.toDateTime(paramChronology)) {
      return paramChronology;
    }
  }
  
  public DateTime toDateTime(DateTimeZone paramDateTimeZone)
  {
    paramDateTimeZone = DateTimeUtils.getZone(paramDateTimeZone);
    if (getZone() == paramDateTimeZone) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = super.toDateTime(paramDateTimeZone)) {
      return paramDateTimeZone;
    }
  }
  
  public DateTime toDateTimeISO()
  {
    if (getChronology() == ISOChronology.getInstance()) {}
    for (DateTime localDateTime = this;; localDateTime = super.toDateTimeISO()) {
      return localDateTime;
    }
  }
  
  public LocalDate toLocalDate()
  {
    return new LocalDate(getMillis(), getChronology());
  }
  
  public LocalDateTime toLocalDateTime()
  {
    return new LocalDateTime(getMillis(), getChronology());
  }
  
  public LocalTime toLocalTime()
  {
    return new LocalTime(getMillis(), getChronology());
  }
  
  @Deprecated
  public TimeOfDay toTimeOfDay()
  {
    return new TimeOfDay(getMillis(), getChronology());
  }
  
  @Deprecated
  public YearMonthDay toYearMonthDay()
  {
    return new YearMonthDay(getMillis(), getChronology());
  }
  
  public Property weekOfWeekyear()
  {
    return new Property(this, getChronology().weekOfWeekyear());
  }
  
  public Property weekyear()
  {
    return new Property(this, getChronology().weekyear());
  }
  
  public DateTime withCenturyOfEra(int paramInt)
  {
    return withMillis(getChronology().centuryOfEra().set(getMillis(), paramInt));
  }
  
  public DateTime withChronology(Chronology paramChronology)
  {
    paramChronology = DateTimeUtils.getChronology(paramChronology);
    if (paramChronology == getChronology()) {}
    for (paramChronology = this;; paramChronology = new DateTime(getMillis(), paramChronology)) {
      return paramChronology;
    }
  }
  
  public DateTime withDate(int paramInt1, int paramInt2, int paramInt3)
  {
    Chronology localChronology = getChronology();
    long l = localChronology.withUTC().getDateTimeMillis(paramInt1, paramInt2, paramInt3, getMillisOfDay());
    return withMillis(localChronology.getZone().convertLocalToUTC(l, false, getMillis()));
  }
  
  public DateTime withDate(LocalDate paramLocalDate)
  {
    return withDate(paramLocalDate.getYear(), paramLocalDate.getMonthOfYear(), paramLocalDate.getDayOfMonth());
  }
  
  public DateTime withDayOfMonth(int paramInt)
  {
    return withMillis(getChronology().dayOfMonth().set(getMillis(), paramInt));
  }
  
  public DateTime withDayOfWeek(int paramInt)
  {
    return withMillis(getChronology().dayOfWeek().set(getMillis(), paramInt));
  }
  
  public DateTime withDayOfYear(int paramInt)
  {
    return withMillis(getChronology().dayOfYear().set(getMillis(), paramInt));
  }
  
  public DateTime withDurationAdded(long paramLong, int paramInt)
  {
    DateTime localDateTime = this;
    if (paramLong != 0L) {
      if (paramInt != 0) {
        break label19;
      }
    }
    label19:
    for (localDateTime = this;; localDateTime = withMillis(getChronology().add(getMillis(), paramLong, paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime withDurationAdded(ReadableDuration paramReadableDuration, int paramInt)
  {
    DateTime localDateTime = this;
    if (paramReadableDuration != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localDateTime = this;; localDateTime = withDurationAdded(paramReadableDuration.getMillis(), paramInt)) {
      return localDateTime;
    }
  }
  
  public DateTime withEarlierOffsetAtOverlap()
  {
    return withMillis(getZone().adjustOffset(getMillis(), false));
  }
  
  public DateTime withEra(int paramInt)
  {
    return withMillis(getChronology().era().set(getMillis(), paramInt));
  }
  
  public DateTime withField(DateTimeFieldType paramDateTimeFieldType, int paramInt)
  {
    if (paramDateTimeFieldType == null) {
      throw new IllegalArgumentException("Field must not be null");
    }
    return withMillis(paramDateTimeFieldType.getField(getChronology()).set(getMillis(), paramInt));
  }
  
  public DateTime withFieldAdded(DurationFieldType paramDurationFieldType, int paramInt)
  {
    if (paramDurationFieldType == null) {
      throw new IllegalArgumentException("Field must not be null");
    }
    if (paramInt == 0) {}
    for (paramDurationFieldType = this;; paramDurationFieldType = withMillis(paramDurationFieldType.getField(getChronology()).add(getMillis(), paramInt))) {
      return paramDurationFieldType;
    }
  }
  
  public DateTime withFields(ReadablePartial paramReadablePartial)
  {
    if (paramReadablePartial == null) {}
    for (paramReadablePartial = this;; paramReadablePartial = withMillis(getChronology().set(paramReadablePartial, getMillis()))) {
      return paramReadablePartial;
    }
  }
  
  public DateTime withHourOfDay(int paramInt)
  {
    return withMillis(getChronology().hourOfDay().set(getMillis(), paramInt));
  }
  
  public DateTime withLaterOffsetAtOverlap()
  {
    return withMillis(getZone().adjustOffset(getMillis(), true));
  }
  
  public DateTime withMillis(long paramLong)
  {
    if (paramLong == getMillis()) {}
    for (DateTime localDateTime = this;; localDateTime = new DateTime(paramLong, getChronology())) {
      return localDateTime;
    }
  }
  
  public DateTime withMillisOfDay(int paramInt)
  {
    return withMillis(getChronology().millisOfDay().set(getMillis(), paramInt));
  }
  
  public DateTime withMillisOfSecond(int paramInt)
  {
    return withMillis(getChronology().millisOfSecond().set(getMillis(), paramInt));
  }
  
  public DateTime withMinuteOfHour(int paramInt)
  {
    return withMillis(getChronology().minuteOfHour().set(getMillis(), paramInt));
  }
  
  public DateTime withMonthOfYear(int paramInt)
  {
    return withMillis(getChronology().monthOfYear().set(getMillis(), paramInt));
  }
  
  public DateTime withPeriodAdded(ReadablePeriod paramReadablePeriod, int paramInt)
  {
    DateTime localDateTime = this;
    if (paramReadablePeriod != null) {
      if (paramInt != 0) {
        break label14;
      }
    }
    label14:
    for (localDateTime = this;; localDateTime = withMillis(getChronology().add(paramReadablePeriod, getMillis(), paramInt))) {
      return localDateTime;
    }
  }
  
  public DateTime withSecondOfMinute(int paramInt)
  {
    return withMillis(getChronology().secondOfMinute().set(getMillis(), paramInt));
  }
  
  public DateTime withTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Chronology localChronology = getChronology();
    long l = localChronology.withUTC().getDateTimeMillis(getYear(), getMonthOfYear(), getDayOfMonth(), paramInt1, paramInt2, paramInt3, paramInt4);
    return withMillis(localChronology.getZone().convertLocalToUTC(l, false, getMillis()));
  }
  
  public DateTime withTime(LocalTime paramLocalTime)
  {
    return withTime(paramLocalTime.getHourOfDay(), paramLocalTime.getMinuteOfHour(), paramLocalTime.getSecondOfMinute(), paramLocalTime.getMillisOfSecond());
  }
  
  public DateTime withTimeAtStartOfDay()
  {
    return toLocalDate().toDateTimeAtStartOfDay(getZone());
  }
  
  public DateTime withWeekOfWeekyear(int paramInt)
  {
    return withMillis(getChronology().weekOfWeekyear().set(getMillis(), paramInt));
  }
  
  public DateTime withWeekyear(int paramInt)
  {
    return withMillis(getChronology().weekyear().set(getMillis(), paramInt));
  }
  
  public DateTime withYear(int paramInt)
  {
    return withMillis(getChronology().year().set(getMillis(), paramInt));
  }
  
  public DateTime withYearOfCentury(int paramInt)
  {
    return withMillis(getChronology().yearOfCentury().set(getMillis(), paramInt));
  }
  
  public DateTime withYearOfEra(int paramInt)
  {
    return withMillis(getChronology().yearOfEra().set(getMillis(), paramInt));
  }
  
  public DateTime withZone(DateTimeZone paramDateTimeZone)
  {
    return withChronology(getChronology().withZone(paramDateTimeZone));
  }
  
  public DateTime withZoneRetainFields(DateTimeZone paramDateTimeZone)
  {
    paramDateTimeZone = DateTimeUtils.getZone(paramDateTimeZone);
    DateTimeZone localDateTimeZone = DateTimeUtils.getZone(getZone());
    if (paramDateTimeZone == localDateTimeZone) {}
    for (paramDateTimeZone = this;; paramDateTimeZone = new DateTime(localDateTimeZone.getMillisKeepLocal(paramDateTimeZone, getMillis()), getChronology().withZone(paramDateTimeZone))) {
      return paramDateTimeZone;
    }
  }
  
  public Property year()
  {
    return new Property(this, getChronology().year());
  }
  
  public Property yearOfCentury()
  {
    return new Property(this, getChronology().yearOfCentury());
  }
  
  public Property yearOfEra()
  {
    return new Property(this, getChronology().yearOfEra());
  }
  
  public static final class Property
    extends AbstractReadableInstantFieldProperty
  {
    private static final long serialVersionUID = -6983323811635733510L;
    private DateTimeField iField;
    private DateTime iInstant;
    
    Property(DateTime paramDateTime, DateTimeField paramDateTimeField)
    {
      this.iInstant = paramDateTime;
      this.iField = paramDateTimeField;
    }
    
    private void readObject(ObjectInputStream paramObjectInputStream)
      throws IOException, ClassNotFoundException
    {
      this.iInstant = ((DateTime)paramObjectInputStream.readObject());
      this.iField = ((DateTimeFieldType)paramObjectInputStream.readObject()).getField(this.iInstant.getChronology());
    }
    
    private void writeObject(ObjectOutputStream paramObjectOutputStream)
      throws IOException
    {
      paramObjectOutputStream.writeObject(this.iInstant);
      paramObjectOutputStream.writeObject(this.iField.getType());
    }
    
    public DateTime addToCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.add(this.iInstant.getMillis(), paramInt));
    }
    
    public DateTime addToCopy(long paramLong)
    {
      return this.iInstant.withMillis(this.iField.add(this.iInstant.getMillis(), paramLong));
    }
    
    public DateTime addWrapFieldToCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.addWrapField(this.iInstant.getMillis(), paramInt));
    }
    
    protected Chronology getChronology()
    {
      return this.iInstant.getChronology();
    }
    
    public DateTime getDateTime()
    {
      return this.iInstant;
    }
    
    public DateTimeField getField()
    {
      return this.iField;
    }
    
    protected long getMillis()
    {
      return this.iInstant.getMillis();
    }
    
    public DateTime roundCeilingCopy()
    {
      return this.iInstant.withMillis(this.iField.roundCeiling(this.iInstant.getMillis()));
    }
    
    public DateTime roundFloorCopy()
    {
      return this.iInstant.withMillis(this.iField.roundFloor(this.iInstant.getMillis()));
    }
    
    public DateTime roundHalfCeilingCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfCeiling(this.iInstant.getMillis()));
    }
    
    public DateTime roundHalfEvenCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfEven(this.iInstant.getMillis()));
    }
    
    public DateTime roundHalfFloorCopy()
    {
      return this.iInstant.withMillis(this.iField.roundHalfFloor(this.iInstant.getMillis()));
    }
    
    public DateTime setCopy(int paramInt)
    {
      return this.iInstant.withMillis(this.iField.set(this.iInstant.getMillis(), paramInt));
    }
    
    public DateTime setCopy(String paramString)
    {
      return setCopy(paramString, null);
    }
    
    public DateTime setCopy(String paramString, Locale paramLocale)
    {
      return this.iInstant.withMillis(this.iField.set(this.iInstant.getMillis(), paramString, paramLocale));
    }
    
    public DateTime withMaximumValue()
    {
      try
      {
        DateTime localDateTime1 = setCopy(getMaximumValue());
        return localDateTime1;
      }
      catch (RuntimeException localRuntimeException)
      {
        DateTime localDateTime2;
        while (IllegalInstantException.isIllegalInstant(localRuntimeException)) {
          localDateTime2 = new DateTime(getChronology().getZone().previousTransition(getMillis() + 86400000L), getChronology());
        }
        throw localDateTime2;
      }
    }
    
    public DateTime withMinimumValue()
    {
      try
      {
        DateTime localDateTime1 = setCopy(getMinimumValue());
        return localDateTime1;
      }
      catch (RuntimeException localRuntimeException)
      {
        DateTime localDateTime2;
        while (IllegalInstantException.isIllegalInstant(localRuntimeException)) {
          localDateTime2 = new DateTime(getChronology().getZone().nextTransition(getMillis() - 86400000L), getChronology());
        }
        throw localDateTime2;
      }
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\DateTime.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */