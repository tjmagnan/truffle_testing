package org.joda.time.convert;

import org.joda.time.Chronology;
import org.joda.time.DateTimeUtils;
import org.joda.time.ReadWritableInterval;
import org.joda.time.ReadWritablePeriod;
import org.joda.time.ReadableInterval;

class ReadableIntervalConverter
  extends AbstractConverter
  implements IntervalConverter, DurationConverter, PeriodConverter
{
  static final ReadableIntervalConverter INSTANCE = new ReadableIntervalConverter();
  
  public long getDurationMillis(Object paramObject)
  {
    return ((ReadableInterval)paramObject).toDurationMillis();
  }
  
  public Class<?> getSupportedType()
  {
    return ReadableInterval.class;
  }
  
  public boolean isReadableInterval(Object paramObject, Chronology paramChronology)
  {
    return true;
  }
  
  public void setInto(ReadWritableInterval paramReadWritableInterval, Object paramObject, Chronology paramChronology)
  {
    paramObject = (ReadableInterval)paramObject;
    paramReadWritableInterval.setInterval((ReadableInterval)paramObject);
    if (paramChronology != null) {
      paramReadWritableInterval.setChronology(paramChronology);
    }
    for (;;)
    {
      return;
      paramReadWritableInterval.setChronology(((ReadableInterval)paramObject).getChronology());
    }
  }
  
  public void setInto(ReadWritablePeriod paramReadWritablePeriod, Object paramObject, Chronology paramChronology)
  {
    paramObject = (ReadableInterval)paramObject;
    if (paramChronology != null) {}
    for (;;)
    {
      paramObject = paramChronology.get(paramReadWritablePeriod, ((ReadableInterval)paramObject).getStartMillis(), ((ReadableInterval)paramObject).getEndMillis());
      for (int i = 0; i < paramObject.length; i++) {
        paramReadWritablePeriod.setValue(i, paramObject[i]);
      }
      paramChronology = DateTimeUtils.getIntervalChronology((ReadableInterval)paramObject);
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\org\joda\time\convert\ReadableIntervalConverter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */