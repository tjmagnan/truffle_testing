package android.support.graphics.drawable;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.Build.VERSION;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@SuppressLint({"NewApi"})
public class AnimatedVectorDrawableCompat
  extends VectorDrawableCommon
  implements Animatable2Compat
{
  private static final String ANIMATED_VECTOR = "animated-vector";
  private static final boolean DBG_ANIMATION_VECTOR_DRAWABLE = false;
  private static final String LOGTAG = "AnimatedVDCompat";
  private static final String TARGET = "target";
  private AnimatedVectorDrawableCompatState mAnimatedVectorState;
  private ArrayList<Animatable2Compat.AnimationCallback> mAnimationCallbacks = null;
  private Animator.AnimatorListener mAnimatorListener = null;
  private ArgbEvaluator mArgbEvaluator = null;
  AnimatedVectorDrawableDelegateState mCachedConstantStateDelegate;
  final Drawable.Callback mCallback = new Drawable.Callback()
  {
    public void invalidateDrawable(Drawable paramAnonymousDrawable)
    {
      AnimatedVectorDrawableCompat.this.invalidateSelf();
    }
    
    public void scheduleDrawable(Drawable paramAnonymousDrawable, Runnable paramAnonymousRunnable, long paramAnonymousLong)
    {
      AnimatedVectorDrawableCompat.this.scheduleSelf(paramAnonymousRunnable, paramAnonymousLong);
    }
    
    public void unscheduleDrawable(Drawable paramAnonymousDrawable, Runnable paramAnonymousRunnable)
    {
      AnimatedVectorDrawableCompat.this.unscheduleSelf(paramAnonymousRunnable);
    }
  };
  private Context mContext;
  
  AnimatedVectorDrawableCompat()
  {
    this(null, null, null);
  }
  
  private AnimatedVectorDrawableCompat(@Nullable Context paramContext)
  {
    this(paramContext, null, null);
  }
  
  private AnimatedVectorDrawableCompat(@Nullable Context paramContext, @Nullable AnimatedVectorDrawableCompatState paramAnimatedVectorDrawableCompatState, @Nullable Resources paramResources)
  {
    this.mContext = paramContext;
    if (paramAnimatedVectorDrawableCompatState != null) {}
    for (this.mAnimatedVectorState = paramAnimatedVectorDrawableCompatState;; this.mAnimatedVectorState = new AnimatedVectorDrawableCompatState(paramContext, paramAnimatedVectorDrawableCompatState, this.mCallback, paramResources)) {
      return;
    }
  }
  
  public static void clearAnimationCallbacks(Drawable paramDrawable)
  {
    if ((paramDrawable == null) || (!(paramDrawable instanceof Animatable))) {}
    for (;;)
    {
      return;
      if (Build.VERSION.SDK_INT >= 24) {
        ((AnimatedVectorDrawable)paramDrawable).clearAnimationCallbacks();
      } else {
        ((AnimatedVectorDrawableCompat)paramDrawable).clearAnimationCallbacks();
      }
    }
  }
  
  @Nullable
  public static AnimatedVectorDrawableCompat create(@NonNull Context paramContext, @DrawableRes int paramInt)
  {
    Object localObject;
    if (Build.VERSION.SDK_INT >= 24)
    {
      localObject = new AnimatedVectorDrawableCompat(paramContext);
      ((AnimatedVectorDrawableCompat)localObject).mDelegateDrawable = ResourcesCompat.getDrawable(paramContext.getResources(), paramInt, paramContext.getTheme());
      ((AnimatedVectorDrawableCompat)localObject).mDelegateDrawable.setCallback(((AnimatedVectorDrawableCompat)localObject).mCallback);
      ((AnimatedVectorDrawableCompat)localObject).mCachedConstantStateDelegate = new AnimatedVectorDrawableDelegateState(((AnimatedVectorDrawableCompat)localObject).mDelegateDrawable.getConstantState());
      paramContext = (Context)localObject;
    }
    for (;;)
    {
      return paramContext;
      localObject = paramContext.getResources();
      try
      {
        localObject = ((Resources)localObject).getXml(paramInt);
        localAttributeSet = Xml.asAttributeSet((XmlPullParser)localObject);
        do
        {
          paramInt = ((XmlPullParser)localObject).next();
        } while ((paramInt != 2) && (paramInt != 1));
        if (paramInt != 2)
        {
          paramContext = new org/xmlpull/v1/XmlPullParserException;
          paramContext.<init>("No start tag found");
          throw paramContext;
        }
      }
      catch (XmlPullParserException paramContext)
      {
        AttributeSet localAttributeSet;
        Log.e("AnimatedVDCompat", "parser error", paramContext);
        paramContext = null;
        continue;
        paramContext = createFromXmlInner(paramContext, paramContext.getResources(), (XmlPullParser)localObject, localAttributeSet, paramContext.getTheme());
      }
      catch (IOException paramContext)
      {
        for (;;)
        {
          Log.e("AnimatedVDCompat", "parser error", paramContext);
        }
      }
    }
  }
  
  public static AnimatedVectorDrawableCompat createFromXmlInner(Context paramContext, Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    paramContext = new AnimatedVectorDrawableCompat(paramContext);
    paramContext.inflate(paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    return paramContext;
  }
  
  public static void registerAnimationCallback(Drawable paramDrawable, Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    if ((paramDrawable == null) || (paramAnimationCallback == null)) {}
    for (;;)
    {
      return;
      if ((paramDrawable instanceof Animatable)) {
        if (Build.VERSION.SDK_INT >= 24) {
          registerPlatformCallback((AnimatedVectorDrawable)paramDrawable, paramAnimationCallback);
        } else {
          ((AnimatedVectorDrawableCompat)paramDrawable).registerAnimationCallback(paramAnimationCallback);
        }
      }
    }
  }
  
  private static void registerPlatformCallback(@NonNull AnimatedVectorDrawable paramAnimatedVectorDrawable, @NonNull Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    paramAnimatedVectorDrawable.registerAnimationCallback(paramAnimationCallback.getPlatformCallback());
  }
  
  private void removeAnimatorSetListener()
  {
    if (this.mAnimatorListener != null)
    {
      this.mAnimatedVectorState.mAnimatorSet.removeListener(this.mAnimatorListener);
      this.mAnimatorListener = null;
    }
  }
  
  private void setupAnimatorsForTarget(String paramString, Animator paramAnimator)
  {
    paramAnimator.setTarget(this.mAnimatedVectorState.mVectorDrawable.getTargetByName(paramString));
    if (Build.VERSION.SDK_INT < 21) {
      setupColorAnimator(paramAnimator);
    }
    if (this.mAnimatedVectorState.mAnimators == null)
    {
      AnimatedVectorDrawableCompatState.access$002(this.mAnimatedVectorState, new ArrayList());
      this.mAnimatedVectorState.mTargetNameMap = new ArrayMap();
    }
    this.mAnimatedVectorState.mAnimators.add(paramAnimator);
    this.mAnimatedVectorState.mTargetNameMap.put(paramAnimator, paramString);
  }
  
  private void setupColorAnimator(Animator paramAnimator)
  {
    Object localObject;
    if ((paramAnimator instanceof AnimatorSet))
    {
      localObject = ((AnimatorSet)paramAnimator).getChildAnimations();
      if (localObject != null) {
        for (int i = 0; i < ((List)localObject).size(); i++) {
          setupColorAnimator((Animator)((List)localObject).get(i));
        }
      }
    }
    if ((paramAnimator instanceof ObjectAnimator))
    {
      localObject = (ObjectAnimator)paramAnimator;
      paramAnimator = ((ObjectAnimator)localObject).getPropertyName();
      if (("fillColor".equals(paramAnimator)) || ("strokeColor".equals(paramAnimator)))
      {
        if (this.mArgbEvaluator == null) {
          this.mArgbEvaluator = new ArgbEvaluator();
        }
        ((ObjectAnimator)localObject).setEvaluator(this.mArgbEvaluator);
      }
    }
  }
  
  public static boolean unregisterAnimationCallback(Drawable paramDrawable, Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (paramDrawable != null)
    {
      if (paramAnimationCallback != null) {
        break label16;
      }
      bool1 = bool2;
    }
    for (;;)
    {
      return bool1;
      label16:
      bool1 = bool2;
      if ((paramDrawable instanceof Animatable)) {
        if (Build.VERSION.SDK_INT >= 24) {
          bool1 = unregisterPlatformCallback((AnimatedVectorDrawable)paramDrawable, paramAnimationCallback);
        } else {
          bool1 = ((AnimatedVectorDrawableCompat)paramDrawable).unregisterAnimationCallback(paramAnimationCallback);
        }
      }
    }
  }
  
  private static boolean unregisterPlatformCallback(AnimatedVectorDrawable paramAnimatedVectorDrawable, Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    return paramAnimatedVectorDrawable.unregisterAnimationCallback(paramAnimationCallback.getPlatformCallback());
  }
  
  public void applyTheme(Resources.Theme paramTheme)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.applyTheme(this.mDelegateDrawable, paramTheme);
    }
  }
  
  public boolean canApplyTheme()
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = DrawableCompat.canApplyTheme(this.mDelegateDrawable);; bool = false) {
      return bool;
    }
  }
  
  public void clearAnimationCallbacks()
  {
    if (this.mDelegateDrawable != null) {
      ((AnimatedVectorDrawable)this.mDelegateDrawable).clearAnimationCallbacks();
    }
    for (;;)
    {
      return;
      removeAnimatorSetListener();
      if (this.mAnimationCallbacks != null) {
        this.mAnimationCallbacks.clear();
      }
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.draw(paramCanvas);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.draw(paramCanvas);
      if (this.mAnimatedVectorState.mAnimatorSet.isStarted()) {
        invalidateSelf();
      }
    }
  }
  
  public int getAlpha()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = DrawableCompat.getAlpha(this.mDelegateDrawable);; i = this.mAnimatedVectorState.mVectorDrawable.getAlpha()) {
      return i;
    }
  }
  
  public int getChangingConfigurations()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getChangingConfigurations();; i = super.getChangingConfigurations() | this.mAnimatedVectorState.mChangingConfigurations) {
      return i;
    }
  }
  
  public Drawable.ConstantState getConstantState()
  {
    if (this.mDelegateDrawable != null) {}
    for (AnimatedVectorDrawableDelegateState localAnimatedVectorDrawableDelegateState = new AnimatedVectorDrawableDelegateState(this.mDelegateDrawable.getConstantState());; localAnimatedVectorDrawableDelegateState = null) {
      return localAnimatedVectorDrawableDelegateState;
    }
  }
  
  public int getIntrinsicHeight()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getIntrinsicHeight();; i = this.mAnimatedVectorState.mVectorDrawable.getIntrinsicHeight()) {
      return i;
    }
  }
  
  public int getIntrinsicWidth()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getIntrinsicWidth();; i = this.mAnimatedVectorState.mVectorDrawable.getIntrinsicWidth()) {
      return i;
    }
  }
  
  public int getOpacity()
  {
    if (this.mDelegateDrawable != null) {}
    for (int i = this.mDelegateDrawable.getOpacity();; i = this.mAnimatedVectorState.mVectorDrawable.getOpacity()) {
      return i;
    }
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet)
    throws XmlPullParserException, IOException
  {
    inflate(paramResources, paramXmlPullParser, paramAttributeSet, null);
  }
  
  public void inflate(Resources paramResources, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, Resources.Theme paramTheme)
    throws XmlPullParserException, IOException
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.inflate(this.mDelegateDrawable, paramResources, paramXmlPullParser, paramAttributeSet, paramTheme);
    }
    for (;;)
    {
      return;
      int i = paramXmlPullParser.getEventType();
      int j = paramXmlPullParser.getDepth();
      if ((i != 1) && ((paramXmlPullParser.getDepth() >= j + 1) || (i != 3)))
      {
        Object localObject1;
        Object localObject2;
        if (i == 2)
        {
          localObject1 = paramXmlPullParser.getName();
          if (!"animated-vector".equals(localObject1)) {
            break label182;
          }
          localObject2 = VectorDrawableCommon.obtainAttributes(paramResources, paramTheme, paramAttributeSet, AndroidResources.styleable_AnimatedVectorDrawable);
          i = ((TypedArray)localObject2).getResourceId(0, 0);
          if (i != 0)
          {
            localObject1 = VectorDrawableCompat.create(paramResources, i, paramTheme);
            ((VectorDrawableCompat)localObject1).setAllowCaching(false);
            ((VectorDrawableCompat)localObject1).setCallback(this.mCallback);
            if (this.mAnimatedVectorState.mVectorDrawable != null) {
              this.mAnimatedVectorState.mVectorDrawable.setCallback(null);
            }
            this.mAnimatedVectorState.mVectorDrawable = ((VectorDrawableCompat)localObject1);
          }
          ((TypedArray)localObject2).recycle();
        }
        for (;;)
        {
          i = paramXmlPullParser.next();
          break;
          label182:
          if ("target".equals(localObject1))
          {
            localObject1 = paramResources.obtainAttributes(paramAttributeSet, AndroidResources.styleable_AnimatedVectorDrawableTarget);
            localObject2 = ((TypedArray)localObject1).getString(0);
            i = ((TypedArray)localObject1).getResourceId(1, 0);
            if (i != 0)
            {
              if (this.mContext == null) {
                break label254;
              }
              setupAnimatorsForTarget((String)localObject2, AnimatorInflater.loadAnimator(this.mContext, i));
            }
            ((TypedArray)localObject1).recycle();
          }
        }
        label254:
        ((TypedArray)localObject1).recycle();
        throw new IllegalStateException("Context can't be null when inflating animators");
      }
      this.mAnimatedVectorState.setupAnimatorSet();
    }
  }
  
  public boolean isAutoMirrored()
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = DrawableCompat.isAutoMirrored(this.mDelegateDrawable);; bool = this.mAnimatedVectorState.mVectorDrawable.isAutoMirrored()) {
      return bool;
    }
  }
  
  public boolean isRunning()
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = ((AnimatedVectorDrawable)this.mDelegateDrawable).isRunning();; bool = this.mAnimatedVectorState.mAnimatorSet.isRunning()) {
      return bool;
    }
  }
  
  public boolean isStateful()
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.isStateful();; bool = this.mAnimatedVectorState.mVectorDrawable.isStateful()) {
      return bool;
    }
  }
  
  public Drawable mutate()
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.mutate();
    }
    return this;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setBounds(paramRect);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setBounds(paramRect);
    }
  }
  
  protected boolean onLevelChange(int paramInt)
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.setLevel(paramInt);; bool = this.mAnimatedVectorState.mVectorDrawable.setLevel(paramInt)) {
      return bool;
    }
  }
  
  protected boolean onStateChange(int[] paramArrayOfInt)
  {
    if (this.mDelegateDrawable != null) {}
    for (boolean bool = this.mDelegateDrawable.setState(paramArrayOfInt);; bool = this.mAnimatedVectorState.mVectorDrawable.setState(paramArrayOfInt)) {
      return bool;
    }
  }
  
  public void registerAnimationCallback(@NonNull Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    if (this.mDelegateDrawable != null) {
      registerPlatformCallback((AnimatedVectorDrawable)this.mDelegateDrawable, paramAnimationCallback);
    }
    for (;;)
    {
      return;
      if (paramAnimationCallback != null)
      {
        if (this.mAnimationCallbacks == null) {
          this.mAnimationCallbacks = new ArrayList();
        }
        if (!this.mAnimationCallbacks.contains(paramAnimationCallback))
        {
          this.mAnimationCallbacks.add(paramAnimationCallback);
          if (this.mAnimatorListener == null) {
            this.mAnimatorListener = new AnimatorListenerAdapter()
            {
              public void onAnimationEnd(Animator paramAnonymousAnimator)
              {
                paramAnonymousAnimator = new ArrayList(AnimatedVectorDrawableCompat.this.mAnimationCallbacks);
                int j = paramAnonymousAnimator.size();
                for (int i = 0; i < j; i++) {
                  ((Animatable2Compat.AnimationCallback)paramAnonymousAnimator.get(i)).onAnimationEnd(AnimatedVectorDrawableCompat.this);
                }
              }
              
              public void onAnimationStart(Animator paramAnonymousAnimator)
              {
                paramAnonymousAnimator = new ArrayList(AnimatedVectorDrawableCompat.this.mAnimationCallbacks);
                int j = paramAnonymousAnimator.size();
                for (int i = 0; i < j; i++) {
                  ((Animatable2Compat.AnimationCallback)paramAnonymousAnimator.get(i)).onAnimationStart(AnimatedVectorDrawableCompat.this);
                }
              }
            };
          }
          this.mAnimatedVectorState.mAnimatorSet.addListener(this.mAnimatorListener);
        }
      }
    }
  }
  
  public void setAlpha(int paramInt)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setAlpha(paramInt);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setAlpha(paramInt);
    }
  }
  
  public void setAutoMirrored(boolean paramBoolean)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setAutoMirrored(paramBoolean);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setAutoMirrored(paramBoolean);
    }
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    if (this.mDelegateDrawable != null) {
      this.mDelegateDrawable.setColorFilter(paramColorFilter);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setColorFilter(paramColorFilter);
    }
  }
  
  public void setTint(int paramInt)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.setTint(this.mDelegateDrawable, paramInt);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setTint(paramInt);
    }
  }
  
  public void setTintList(ColorStateList paramColorStateList)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.setTintList(this.mDelegateDrawable, paramColorStateList);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setTintList(paramColorStateList);
    }
  }
  
  public void setTintMode(PorterDuff.Mode paramMode)
  {
    if (this.mDelegateDrawable != null) {
      DrawableCompat.setTintMode(this.mDelegateDrawable, paramMode);
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mVectorDrawable.setTintMode(paramMode);
    }
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.mDelegateDrawable != null) {}
    for (paramBoolean1 = this.mDelegateDrawable.setVisible(paramBoolean1, paramBoolean2);; paramBoolean1 = super.setVisible(paramBoolean1, paramBoolean2))
    {
      return paramBoolean1;
      this.mAnimatedVectorState.mVectorDrawable.setVisible(paramBoolean1, paramBoolean2);
    }
  }
  
  public void start()
  {
    if (this.mDelegateDrawable != null) {
      ((AnimatedVectorDrawable)this.mDelegateDrawable).start();
    }
    for (;;)
    {
      return;
      if (!this.mAnimatedVectorState.mAnimatorSet.isStarted())
      {
        this.mAnimatedVectorState.mAnimatorSet.start();
        invalidateSelf();
      }
    }
  }
  
  public void stop()
  {
    if (this.mDelegateDrawable != null) {
      ((AnimatedVectorDrawable)this.mDelegateDrawable).stop();
    }
    for (;;)
    {
      return;
      this.mAnimatedVectorState.mAnimatorSet.end();
    }
  }
  
  public boolean unregisterAnimationCallback(@NonNull Animatable2Compat.AnimationCallback paramAnimationCallback)
  {
    if (this.mDelegateDrawable != null) {
      unregisterPlatformCallback((AnimatedVectorDrawable)this.mDelegateDrawable, paramAnimationCallback);
    }
    boolean bool1;
    if ((this.mAnimationCallbacks == null) || (paramAnimationCallback == null)) {
      bool1 = false;
    }
    for (;;)
    {
      return bool1;
      boolean bool2 = this.mAnimationCallbacks.remove(paramAnimationCallback);
      bool1 = bool2;
      if (this.mAnimationCallbacks.size() == 0)
      {
        removeAnimatorSetListener();
        bool1 = bool2;
      }
    }
  }
  
  private static class AnimatedVectorDrawableCompatState
    extends Drawable.ConstantState
  {
    AnimatorSet mAnimatorSet;
    private ArrayList<Animator> mAnimators;
    int mChangingConfigurations;
    ArrayMap<Animator, String> mTargetNameMap;
    VectorDrawableCompat mVectorDrawable;
    
    public AnimatedVectorDrawableCompatState(Context paramContext, AnimatedVectorDrawableCompatState paramAnimatedVectorDrawableCompatState, Drawable.Callback paramCallback, Resources paramResources)
    {
      if (paramAnimatedVectorDrawableCompatState != null)
      {
        this.mChangingConfigurations = paramAnimatedVectorDrawableCompatState.mChangingConfigurations;
        if (paramAnimatedVectorDrawableCompatState.mVectorDrawable != null)
        {
          paramContext = paramAnimatedVectorDrawableCompatState.mVectorDrawable.getConstantState();
          if (paramResources == null) {
            break label212;
          }
        }
        label212:
        for (this.mVectorDrawable = ((VectorDrawableCompat)paramContext.newDrawable(paramResources));; this.mVectorDrawable = ((VectorDrawableCompat)paramContext.newDrawable()))
        {
          this.mVectorDrawable = ((VectorDrawableCompat)this.mVectorDrawable.mutate());
          this.mVectorDrawable.setCallback(paramCallback);
          this.mVectorDrawable.setBounds(paramAnimatedVectorDrawableCompatState.mVectorDrawable.getBounds());
          this.mVectorDrawable.setAllowCaching(false);
          if (paramAnimatedVectorDrawableCompatState.mAnimators == null) {
            return;
          }
          int j = paramAnimatedVectorDrawableCompatState.mAnimators.size();
          this.mAnimators = new ArrayList(j);
          this.mTargetNameMap = new ArrayMap(j);
          for (int i = 0; i < j; i++)
          {
            paramCallback = (Animator)paramAnimatedVectorDrawableCompatState.mAnimators.get(i);
            paramContext = paramCallback.clone();
            paramCallback = (String)paramAnimatedVectorDrawableCompatState.mTargetNameMap.get(paramCallback);
            paramContext.setTarget(this.mVectorDrawable.getTargetByName(paramCallback));
            this.mAnimators.add(paramContext);
            this.mTargetNameMap.put(paramContext, paramCallback);
          }
        }
        setupAnimatorSet();
      }
    }
    
    public int getChangingConfigurations()
    {
      return this.mChangingConfigurations;
    }
    
    public Drawable newDrawable()
    {
      throw new IllegalStateException("No constant state support for SDK < 24.");
    }
    
    public Drawable newDrawable(Resources paramResources)
    {
      throw new IllegalStateException("No constant state support for SDK < 24.");
    }
    
    public void setupAnimatorSet()
    {
      if (this.mAnimatorSet == null) {
        this.mAnimatorSet = new AnimatorSet();
      }
      this.mAnimatorSet.playTogether(this.mAnimators);
    }
  }
  
  private static class AnimatedVectorDrawableDelegateState
    extends Drawable.ConstantState
  {
    private final Drawable.ConstantState mDelegateState;
    
    public AnimatedVectorDrawableDelegateState(Drawable.ConstantState paramConstantState)
    {
      this.mDelegateState = paramConstantState;
    }
    
    public boolean canApplyTheme()
    {
      return this.mDelegateState.canApplyTheme();
    }
    
    public int getChangingConfigurations()
    {
      return this.mDelegateState.getChangingConfigurations();
    }
    
    public Drawable newDrawable()
    {
      AnimatedVectorDrawableCompat localAnimatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
      localAnimatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable();
      localAnimatedVectorDrawableCompat.mDelegateDrawable.setCallback(localAnimatedVectorDrawableCompat.mCallback);
      return localAnimatedVectorDrawableCompat;
    }
    
    public Drawable newDrawable(Resources paramResources)
    {
      AnimatedVectorDrawableCompat localAnimatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
      localAnimatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable(paramResources);
      localAnimatedVectorDrawableCompat.mDelegateDrawable.setCallback(localAnimatedVectorDrawableCompat.mCallback);
      return localAnimatedVectorDrawableCompat;
    }
    
    public Drawable newDrawable(Resources paramResources, Resources.Theme paramTheme)
    {
      AnimatedVectorDrawableCompat localAnimatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
      localAnimatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable(paramResources, paramTheme);
      localAnimatedVectorDrawableCompat.mDelegateDrawable.setCallback(localAnimatedVectorDrawableCompat.mCallback);
      return localAnimatedVectorDrawableCompat;
    }
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\graphics\drawable\AnimatedVectorDrawableCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */