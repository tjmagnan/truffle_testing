package android.support.v4.view;

import android.annotation.TargetApi;
import android.support.annotation.RequiresApi;
import android.view.View;

@TargetApi(15)
@RequiresApi(15)
class ViewCompatICSMr1
{
  public static boolean hasOnClickListeners(View paramView)
  {
    return paramView.hasOnClickListeners();
  }
}


/* Location:              C:\Users\TH034MA\OneDrive - Pitney Bowes\Assignments\APK Testing\SendPro Companion\AndroidMCA-dex2jar.jar!\android\support\v4\view\ViewCompatICSMr1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */